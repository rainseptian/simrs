<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PendaftaranModel extends CI_Model {

   

    public function getDokter()
    {            
                $this->db->select('u.*, ug.grup_id, g.nama_grup');
                $this->db->from('user u');
                $this->db->join('user_grup ug','ug.user_id = u.id' , 'left' );
                $this->db->join('grup g','g.id = ug.grup_id', 'left' );
                $this->db->where('g.nama_grup', 'dokter' );
                $this->db->where('u.is_active', '1' );
               
        return $this->db->get();
    }

    public function cek_id()
    {            
                $this->db->select_max('no_rm');
                $this->db->from('pasien');
        return $this->db->get();     
               
               
        return $this->db->get();
    }

    public function getJenisPendaftaran()
    {            
                $this->db->where('is_active', '1' );
        return  $this->db->get('jenis_pendaftaran');
    }

    public function getListPendaftaran()
    {            
                $this->db->select('pp.*, p.nama nama_pasien, p.jk, p.usia, u.nama nama_dokter, jp.nama jenis_pendaftaran' );
                $this->db->from('pendaftaran_pasien pp');
                $this->db->join('pasien p', 'p.id = pp.pasien and p.is_active = 1');
                $this->db->join('user u', 'u.id = pp.dokter and u.is_active = 1');
                $this->db->join('jenis_pendaftaran jp', 'jp.id = pp.jenis_pendaftaran_id and jp.is_active = 1');
                $this->db->where('pp.is_active', '1' );
        return  $this->db->get();
    }
    public function cari($id='')
    {
            $this->db->where('no_rm',$id );
            $this->db->where('is_active', '1');
            return  $this->db->get('pasien');
    }

  

     function cari_kode($keyword)
    {
        $sql = "
            SELECT 
                *
            FROM 
                pasien
            WHERE
                ( 
                    no_rm LIKE '%".$this->db->escape_like_str($keyword)."%' 
                    OR nama LIKE '%".$this->db->escape_like_str($keyword)."%' 
                    OR nik LIKE '%".$this->db->escape_like_str($keyword)."%' 
                )
            ";
        return $this->db->query($sql);
    }


    function getAktifPasien($keyword) {
        $sql = "
            SELECT p.*, pp.jaminan, pp.no_jaminan, MAX(pp.id) as pendaftaran_id
            FROM pendaftaran_pasien pp
            JOIN pemeriksaan pem ON pem.pendaftaran_id = pp.id
            JOIN pasien p ON pem.pasien_id = p.id
            WHERE ( 
                p.no_rm LIKE '%" . $this->db->escape_like_str($keyword) . "%' 
                OR p.nama LIKE '%" . $this->db->escape_like_str($keyword) . "%' 
            )
            AND p.is_active = 1
            AND pem.status != 'selesai'
            AND pem.status != 'sudah_bayar'
            AND pp.pendaftaran_id IS null
            GROUP BY pp.pasien
            ORDER BY pp.id desc
            LIMIT 100
            ";
        return $this->db->query($sql);
    }

    
}