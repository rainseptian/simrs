<?php

class Rekapitulasi extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->load->library('template');
        $this->load->Model('AdministrasiModel');
        $this->load->Model('KasirModel');
        $this->load->Model('RawatInapModel');
        $this->load->Model('RuangBersalinModel');
        $this->load->Model('PasienModel');
    }

    public function kasir_all()
    {
        $jenis = $this->input->get('jenis');
        $from = $this->input->get('from');
        $to = $this->input->get('to');
        $bulan = $this->input->get('bulan');
        $tahun = $this->input->get('tahun');
        $by = $this->input->get('by') ?? 'waktu';
        $id_pasien = $this->input->get('id_pasien');

        $start_date = $end_date = date('Y-m-d');
        if ($jenis == 2) {
            $start_date = $from;
            $end_date = $to;
        }
        if ($jenis == 3) {
            $start_date = date("$tahun-$bulan-01");
            $end_date = date("$tahun-$bulan-t");
        }
        $periode = ['start' => $start_date, 'end' => $end_date];

        $data['jenis'] = $jenis;
        $data['from'] = $from;
        $data['to'] = $to;
        $data['bulan'] = $bulan;
        $data['tahun'] = $tahun;
        $data['by'] = $by;
        $data['id_pasien'] = $id_pasien;
        $data['pasien'] = $id_pasien ? $this->PasienModel->getPasienById($id_pasien)->row() : '';

        $data['listPemeriksaan'] = $this->KasirModel->getListPemeriksaanForRekapKasirAll($by == 'waktu' ? 'waktu' : $id_pasien, $periode);
        foreach ($data['listPemeriksaan'] as $v) {
            $v->bayar = json_decode($v->bayar);
        }
        $data['jaminan'] = $this->config->item('pendaftaran');
        $data['jaminan']['-'] = [
            'class' => 'label-default',
            'label' => '&nbsp;&nbsp;-&nbsp;&nbsp;'
        ];

        $this->template->view('administrasi/rekap_kasir_all', $data);
    }

    public function get_tindakan($bayar_id)
    {
        $r = $this->db->query("
            SELECT item, item_id, harga, jumlah, subtotal FROM detail_bayar
            WHERE bayar_id = $bayar_id
            AND jenis_item = 'tindakan'
        ")->result();

        echo json_encode($r);
    }

    public function get_tindakan_ranap($bayar_id)
    {
        $r = $this->db->query("
            SELECT item, item_id, harga, jumlah, subtotal FROM ri_bayar_detail
            WHERE ri_bayar_id = $bayar_id
            AND (jenis_item = 'tindakan' OR jenis_item = 'makanan' OR jenis_item = 'bed')
        ")->result();

        echo json_encode($r);
    }

    public function get_obat_luar($bayar_id)
    {
        $r = $this->db->query("
            SELECT item, 0 as item_id, harga, jumlah, subtotal FROM detail_bayar_obat_luar
            WHERE bayar_obat_luar_id = $bayar_id
            AND jenis_item = 'obat'
        ")->result();

        echo json_encode($r);
    }

    public function get_obat($bayar_id)
    {
        $r = $this->db->query("
            SELECT item, item_id, harga, jumlah, subtotal FROM detail_bayar
            WHERE bayar_id = $bayar_id
            AND jenis_item = 'obat'
        ")->result();

        echo json_encode($r);
    }

    public function get_obat_ranap($bayar_id)
    {
        $r = $this->db->query("
            SELECT item, item_id, harga, jumlah, subtotal FROM ri_bayar_detail
            WHERE ri_bayar_id = $bayar_id
            AND jenis_item = 'obat'
        ")->result();

        echo json_encode($r);
    }

    public function get_racikan_luar($pemeriksaan_id)
    {
        $racikans = $this->db->query("SELECT * FROM detail_penjualan_obat_racikan_luar WHERE penjualan_obat_luar_id = '$pemeriksaan_id'")->result();
        foreach ($racikans as $k => $v) {
            $racikans[$k]->total = 0;
            $racikans[$k]->obat = $this->db->query("
                SELECT o.*, ora.* FROM obat_racikan_luar ora 
                JOIN obat o ON ora.obat_id = o.id 
                JOIN detail_penjualan_obat_racikan_luar dorp ON ora.detail_penjualan_obat_racikan_luar_id = dorp.id 
                WHERE dorp.id = '$v->id'
            ")->result();
            foreach ($racikans[$k]->obat as $kk => $vv) {
                $racikans[$k]->total += $vv->jumlah_satuan * $vv->harga_jual;
            }
        }

        echo json_encode($racikans);
    }

    public function get_racikan($pemeriksaan_id)
    {
        $racikans = $this->AdministrasiModel->getListRacikanByPemeriksaanId($pemeriksaan_id)->result();
        foreach ($racikans as $k => $v) {
            $racikans[$k]->total = 0;
            $racikans[$k]->obat = $this->AdministrasiModel->getListObatByDetailObatRacikanPemeriksaanId($v->id)->result();
            foreach ($racikans[$k]->obat as $kk => $vv) {
                $racikans[$k]->total += $vv->jumlah_satuan * $vv->harga_jual;
            }
        }

        echo json_encode($racikans);
    }

    public function get_racikan_ranap($is_ranap, $ri_id, $transfer_id, $jenis_ruangan_id)
    {
        $r = [];

        $data['resep'] = $is_ranap ?
            $this->RawatInapModel->getResepByRawatInapId($ri_id) :
            $this->RuangBersalinModel->getResepByTransferId($transfer_id, $jenis_ruangan_id);
        foreach ($data['resep'] as &$d) {
            $d->obat_racik = $this->RawatInapModel->getObatRacikByResepId($d->id);
            foreach ($d->obat_racik as &$dd) {
                $dd->obat = $this->RawatInapModel->getObatRacikDetailByResepObatRacikId($dd->id);
                $dd->total = 0;
                foreach ($dd->obat as $ddd) {
                    $ddd->jumlah_satuan = $ddd->jumlah;
                    $dd->total += $ddd->jumlah * $ddd->harga_jual;
                }
            }
            $r = array_merge($r, $d->obat_racik);
        }

        echo json_encode($r);
    }

    public function get_bhp($bayar_id)
    {
        $r = $this->db->query("
            SELECT item, item_id, harga, jumlah, subtotal FROM detail_bayar
            WHERE bayar_id = $bayar_id
            AND jenis_item = 'bahan habis pakai'
        ")->result();

        echo json_encode($r);
    }

    public function get_bhp_ranap($bayar_id)
    {
        $r = $this->db->query("
            SELECT item, item_id, harga, jumlah, subtotal FROM ri_bayar_detail
            WHERE ri_bayar_id = $bayar_id
            AND jenis_item = 'bahan habis pakai'
        ")->result();

        echo json_encode($r);
    }
}