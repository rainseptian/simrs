<?php


class Api extends CI_Controller
{
    public function poli()
    {
        $this->load->Model('PendaftaranModel');
        $poli = $this->PendaftaranModel->getJenisPendaftaran()->result();

        $this->doSuccess($poli);
    }

    public function caraBayar()
    {
        $data = $this->config->item('pendaftaran');

        $caraBayar = [];

        foreach ($data as $key => $value)
        {
            $caraBayar[] = [
                "id" => $key,
                "name" => $value['label'],
                "deskripsi" => $value['deskripsi']
            ];
        }

        $this->doSuccess($caraBayar);
    }

    public function daftar()
    {
        $this->load->Model('PendaftaranModel');
        $this->load->Model('PasienModel');
        $this->load->Model('MainModel');

        $id_poli = $this->input->post('id_poli');
        $jenis_pendaftaran = $this->PendaftaranModel->getJenisPendaftaranById($id_poli)->row();

        $tanggal = $this->input->post('tanggal_lahir');
        $tanggal_lahir = strtotime($tanggal);
        $sekarang = time(); // Waktu sekarang
        $diff = $sekarang - $tanggal_lahir;
        $umurtahun = floor($diff / (60 * 60 * 24 * 365));
        $sisatahun = $diff % (60 * 60 * 24 * 365);

        $umurbulan = floor($sisatahun / (60 * 60 * 24 * 30));
        $sisabulan = $sisatahun % (60 * 60 * 24 * 30);
        $umurhari = floor($sisabulan / (60 * 60 * 24 * 30));

        $noRm = $this->PasienModel->getNoRmAutoPerMonth();

        $pasien = array(
            'no_rm' => $noRm,
            'nama' => $this->input->post('nama_lengkap'),
            'tanggal_lahir' => $this->input->post('tanggal_lahir'),
            'usia' => $umurtahun . " tahun " . $umurbulan . " bulan " . $umurhari . " hari",
            'tempat_lahir' => $this->input->post('tempat_lahir'),
            'jk' => 'L',
            'alamat' => $this->input->post('alamat'),
            'telepon' => $this->input->post('no_hp'),
            'pekerjaan' => '',
            'agama' => '',
            'tingkat_pendidikan' => '',
            'penanggungjawab' => '',
            'biopsikososial' => '',
            'is_active' => 1,
            'creator' => 1
        );

        $insert_id = $this->MainModel->insert_id($tabel = 'pasien', $pasien);

        $idJenisPendaftaranYangLangsungKePemeriksaan = [19, 25, 44, 45, 40, 41, 42, 43, 59];
        $apakaLangsungKePemeriksaan = in_array($id_poli, $idJenisPendaftaranYangLangsungKePemeriksaan);

        $rm = array(
            'no_rm' => $noRm,
            'pasien' => $insert_id,
            'jenis_pendaftaran_id' => $id_poli,
            'jaminan' => $this->input->post('cara_bayar'),
            'status' => 'pending',
            'is_bpjs' => 0,
            'creator' => 1
        );

        $insert_id2 = $this->MainModel->insert_id($tabel = 'pendaftaran_pasien', $rm);

        if ($apakaLangsungKePemeriksaan)
        {
            $periksa = array(
                'pendaftaran_id' => $insert_id2,
                'pasien_id' => $insert_id,
                'no_rm' => $noRm,
                'nama_pasien' => $this->input->post('nama_lengkap'),
                'keluhan_utama' => '',
                'diagnosa_perawat' => '',
                'asuhan_keperawatan' => '',
                'bmi' => '',
                'td' => '',
                'r' => '',
                'bb' => '',
                'n' => '',
                's' => '',
                'tb' => '',
                'is_bpjs' => 0,
                'jaminan' => $this->input->post('cara_bayar'),
                'status' => 'pending',
                'creator' => 1
            );
            $this->MainModel->insert_id($tabel = 'pemeriksaan', $periksa);
        }

        if ($insert_id2)
        {
            $this->doSuccess([
                "nomor_antrian" => $this->PendaftaranModel->getNoAntrian($insert_id2),
                "jenis_pasien" => $this->input->post('cara_bayar'),
                "nama_pasien" => $this->input->post('nama_lengkap'),
                "kode_booking" => $jenis_pendaftaran->kode,
                "tanggal" => $this->input->post('tanggal'),
                "nama_poli" => $jenis_pendaftaran->nama
            ]);
        }
        else
            $this->doError("Gagal saat menyimpan data");
    }

    public function daftarPasienLama()
    {
        $this->load->Model('PendaftaranModel');
        $this->load->Model('PasienModel');
        $this->load->Model('MainModel');

        $id_poli = $this->input->post('id_poli');
        $jenis_pendaftaran = $this->PendaftaranModel->getJenisPendaftaranById($id_poli)->row();

        $id_pasien = $this->input->post('id_pasien');
        $pasien = $this->PasienModel->getPasienById($id_pasien)->row();

        $rm = array(
            'no_rm' => $pasien->no_rm,
            'pasien' => $id_pasien,
            'jenis_pendaftaran_id' => $id_poli,
            'jaminan' => $this->input->post('cara_bayar'),
            'status' => 'pending',
            'is_bpjs' => 0,
            'creator' => 1
        );

        $insert = $this->MainModel->insert_id($tabel = 'pendaftaran_pasien', $rm);

        $idJenisPendaftaranYangLangsungKePemeriksaan = [19, 25, 44, 45, 40, 41, 42, 43, 59];
        $apakaLangsungKePemeriksaan = in_array($id_poli, $idJenisPendaftaranYangLangsungKePemeriksaan);

        if ($apakaLangsungKePemeriksaan)
        {
            $periksa = array(
                'pendaftaran_id' => $insert,
                'pasien_id' => $id_pasien,
                'no_rm' => $pasien->no_rm,
                'nama_pasien' => $pasien->nama,
                'keluhan_utama' => '',
                'diagnosa_perawat' => '',
                'asuhan_keperawatan' => '',
                'bmi' => '',
                'td' => '',
                'r' => '',
                'bb' => '',
                'n' => '',
                's' => '',
                'tb' => '',
                'is_bpjs' => 0,
                'jaminan' => $this->input->post('cara_bayar'),
                'status' => 'pending',
                'creator' => 1
            );

            $this->MainModel->insert_id($tabel = 'pemeriksaan', $periksa);
        }

        if ($insert)
        {
            $this->doSuccess([
                "nomor_antrian" => $this->PendaftaranModel->getNoAntrian($insert),
                "jenis_pasien" => $this->input->post('cara_bayar'),
                "nama_pasien" => $pasien->nama,
                "kode_booking" => $jenis_pendaftaran->kode,
                "tanggal" => $this->input->post('tanggal'),
                "nama_poli" => $jenis_pendaftaran->nama
            ]);
        }
        else
            $this->doError("Gagal saat menyimpan data");
    }

    public function cariPasien()
    {
        $noRm = $this->input->get('no_rm');
        $tglLahir = $this->input->get('tgl_lahir');

        $this->load->Model('PasienModel');

        $pasien = $this->PasienModel->cariPasien($noRm, $tglLahir);

        if ($pasien)
            $this->doSuccess($pasien);
        else
            $this->doError('Data tidak ditemukan');
    }

    private function doSuccess($data)
    {
        header('Content-Type: application/json');

        echo json_encode([
            "success" => true,
            "message" => "Success",
            "data" => $data
        ]);
    }

    private function doError($message)
    {
        header('Content-Type: application/json');

        echo json_encode([
            "success" => false,
            "message" => $message,
            "data" => null
        ]);
    }
}