<?php


class HistoryBahanHabisPakai extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->load->library('template');
        $this->load->Model('HistoryBahanHabisPakaiModel');
    }

    public function stokAwal()
    {
        $bulan = $this->input->get('bulan');
        $tahun = $this->input->get('tahun');

        if (!$bulan) $bulan = intval(date('m'));
        if (!$tahun) $tahun = intval(date('Y'));

        $data['data'] = $this->HistoryBahanHabisPakaiModel->stokAwal($bulan, $tahun);
        $data['bulan'] = $bulan;
        $data['tahun'] = $tahun;

        $this->template->view('history_bahan_habis_pakai/list_stok_awal_bhp', $data);
    }

    public function stokAkhir()
    {
        $bulan = $this->input->get('bulan');
        $tahun = $this->input->get('tahun');

        if (!$bulan) $bulan = intval(date('m'));
        if (!$tahun) $tahun = intval(date('Y'));

        $data['data'] = $this->HistoryBahanHabisPakaiModel->stokAkhir($bulan, $tahun);
        $data['bulan'] = $bulan;
        $data['tahun'] = $tahun;

        $this->template->view('history_bahan_habis_pakai/list_stok_akhir_bhp', $data);
    }

    public function riwayat()
    {
        $bulan = $this->input->get('bulan');
        $tahun = $this->input->get('tahun');

        if (!$bulan) $bulan = intval(date('m'));
        if (!$tahun) $tahun = intval(date('Y'));

        $data['data'] = $this->HistoryBahanHabisPakaiModel->riwayat($bulan, $tahun);
        $data['bulan'] = $bulan;
        $data['tahun'] = $tahun;

        $this->template->view('history_bahan_habis_pakai/list_riwayat_bhp', $data);
    }
}