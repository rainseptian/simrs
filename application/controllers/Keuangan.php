<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Keuangan extends MY_Controller {

    public function __construct() {
        parent::__construct();
        error_reporting(E_ALL);

        // Display errors in output
        ini_set('display_errors', 1);

        $this->load->library('template');
        $this->load->Model('KeuanganModel');
        $this->load->Model('InsentifModel');
        $this->load->Model('MainModel');
        $this->load->Model('ObatGlobalModel');
        $this->load->Model('AdministrasiModel');
        $this->load->Model('PendaftaranModel');
        $this->load->Model('vehicle_model');
        $this->load->Model('PanggilanAmbulanceModel');
        $this->load->Model('TarifAmbulanceModel');
        $this->load->Model('PengeluaranModel');
    }

    public function index() {
        redirect('Keuangan/listPemasukan');
    }

    public function listPemasukan() {
        $item = $this->input->get('item');
        $jenis_pendaftaran_id = $this->input->get('jp_id');
        $table_id = $this->input->get('table_id');
        $start_date = $this->input->get('from');
        $end_date = $this->input->get('to');

        if ($start_date && $end_date) {
            $date['start'] = $start_date;
            $date['end'] = $end_date;

            if ($item == 'jasa racik')
                $data['detail'] = $this->KeuanganModel->getListPemasukanJasaRacikDetail($item, $jenis_pendaftaran_id, $table_id, $date);
            else
                $data['detail'] = $this->KeuanganModel->getListPemasukanDetail($item, $jenis_pendaftaran_id, $table_id, $date);

            $data['periode'] = date_format(date_create($date['start']), 'd M Y').' sd '.date_format(date_create($date['end']), 'd M Y');
        }
        else {
            if ($item == 'jasa racik')
                $data['detail'] = $this->KeuanganModel->getListPemasukanJasaRacikDetail($item, $jenis_pendaftaran_id, $table_id, 'bulanan');
            else
                $data['detail'] = $this->KeuanganModel->getListPemasukanDetail($item, $jenis_pendaftaran_id, $table_id, 'bulanan');

            $data['periode'] = date_format(date_create(date("Y-m-01")), 'd M Y').' sd '.date_format(date_create(date("Y-m-d")), 'd M Y');
        }

        $data['jpem'] = urldecode($this->input->get('jpem'));
        $data['jp_name'] = urldecode($this->input->get('jp_name'));

        $this->template->view('keuangan/list_laba_v',$data);
    }

    public function printNota() {

        $data['listPemeriksaan'] = [];

        if ($_POST) {
            $data['from'] = $this->input->post('from');
            $data['to'] = $this->input->post('to');
            $data['nama'] = $this->input->post('nama');
            $data['tipe'] = $this->input->post('tipe');

            $data['listPemeriksaan'] = $this->KeuanganModel->getPemeriksaanSelesai($data['from'], $data['to'], $data['nama'], $data['tipe'])->result();
            $data['tindakan'] = $this->AdministrasiModel->getTindakandetail();
            $data['obat'] = $this->AdministrasiModel->getObatPemeriksaandetail();
            $data['racikan'] = $this->AdministrasiModel->getRacikanPemeriksaan();
            $data['penyakit'] = $this->AdministrasiModel->getPenyakitPemeriksaandetail();
            $data['jaminan'] = $this->config->item('pendaftaran');
        }

        $this->template->view('keuangan/print_nota', $data);
    }

    public function printNotaAll() {

        $data['listPemeriksaan'] = [];

        if ($_POST) {
            $data['from'] = $this->input->post('from');
            $data['to'] = $this->input->post('to');
            $data['nama'] = $this->input->post('nama');
            $data['tipe'] = $this->input->post('tipe');

            $bayar_all = $this->KeuanganModel->getBayarAll($data['from'], $data['to'])->result();
            foreach ($bayar_all as &$b) {
                $b->detail = $this->KeuanganModel->getBayarDetailByBayarAllId($b->id, $data['nama'])->row();
                $data['listPemeriksaan'][] = $b;
//                $b->bayar = $this->KeuanganModel->getBayarByBayarAllId($b->id, $data['nama'])->result();
//                $b->ri_bayar = $this->KeuanganModel->getRiBayarByBayarAllId($b->id, $data['nama'])->result();
//
//                if (count($b->bayar) || count($b->ri_bayar)) {
//                    $id1 = []; $id2 = [];
//                    $jenis1 = []; $jenis2 = [];
//                    $jam1 = []; $jam2 = [];
//                    $dok1 = []; $dok2 = [];
//
//                    if (count($b->bayar)) {
//                        $id1 = array_map(function ($v) { return $v->pemeriksaan_id; }, $b->bayar);
//                        $jenis1 = array_map(function ($v) { return $v->jenis; }, $b->bayar);
//                        $jam1 = array_map(function ($v) { return $v->jaminan; }, $b->bayar);
//                        $dok1 = array_map(function ($v) { return $v->nama_dokter; }, $b->bayar);
//                    }
//                    if (count($b->ri_bayar)) {
//                        $id2 = array_map(function ($v) { return $v->rawat_inap_id; }, $b->ri_bayar);
//                        $jenis2 = array_map(function ($v) { return $v->jenis; }, $b->ri_bayar);
//                        $jam2 = array_map(function ($v) { return $v->jaminan; }, $b->ri_bayar);
//                        $dok2 = array_map(function ($v) { return $v->nama_dokter; }, $b->ri_bayar);
//                    }
//
//                    $ids = array_merge($id1, $id2);
//                    $jenis = array_merge($jenis1, $jenis2);
//                    $jam = array_merge($jam1, $jam2);
//                    $dok = array_merge($dok1, $dok2);
//
//                    $o = new stdClass();
//                    $o->id = $b->id;
//                    $o->ids = implode('^^^', $ids);
//                    $o->created_at = $b->created_at;
//                    $o->pasien_id = count($b->bayar) ? $b->bayar[0]->pasien_id : $b->ri_bayar[0]->pasien_id;
//                    $o->no_rm = count($b->bayar) ? $b->bayar[0]->no_rm : $b->ri_bayar[0]->no_rm;
//                    $o->nama_pasien = count($b->bayar) ? $b->bayar[0]->nama_pasien : $b->ri_bayar[0]->nama_pasien;
//                    $o->jenis = implode('^^^', $jenis);
//                    $o->jaminan = implode('^^^', $jam);
//                    $o->nama_dokter = implode('^^^', $dok);
//                    $o->status = 'Selesai';
//
//                    $data['listPemeriksaan'][] = $o;
//                }
            }
        }
//        die(json_encode($data));
        $data['jaminan'] = $this->config->item('pendaftaran');
        $data['jaminan']['-'] = [
            'class' => 'label-default',
            'label' => '&nbsp;&nbsp;-&nbsp;&nbsp;'
        ];

        $this->template->view('keuangan/print_nota_all', $data);
    }

    public function listPemasukanObatLuar() {
        $tipe = $this->input->get('tipe');

        $data['detail'] = $this->KeuanganModel->getListPemasukanObatLuarDetail($tipe);
        $data['tipe'] = str_replace('_', ' ', $tipe);

        $this->template->view('keuangan/list_laba_v',$data);
    }

    public function listTotalPemasukan_OLD() {
        $data['total_tindakan'] = $this->KeuanganModel->getTotallabaTindakan();
        $this->template->view('keuangan/resume_pemasukan',$data);
    }

    public function resume_pemasukan() {
        $start_date = $this->input->get('from');
        $end_date = $this->input->get('to');

        if ($start_date && $end_date) {
            $date['start'] = $start_date;
            $date['end'] = $end_date;

            $data['total_jasa_medis'] = $this->KeuanganModel->getLabaByPendaftaran('tindakan', $date);
            $data['total_obat'] = $this->KeuanganModel->getLabaByPendaftaran('obat', $date);
            $data['total_obat_racikan'] = $this->KeuanganModel->getLabaByPendaftaran('obat racikan', $date);
            $data['total_jasa_racik'] = $this->KeuanganModel->getLabaJasaRacikByPendaftaran($date);
            $data['total_bahan_habis_pakai'] = $this->KeuanganModel->getLabaByPendaftaran('bahan habis pakai', $date);
            $data['total_by_jenis_layanan'] = $this->KeuanganModel->getLabaByLayanan($date);
            $data['total_obat_resep_luar'] = $this->KeuanganModel->getLabaObatLuarByPendaftaran($date);
        }
        else {
            $data['total_jasa_medis'] = $this->KeuanganModel->getLabaByPendaftaran('tindakan', 'bulanan');
            $data['total_obat'] = $this->KeuanganModel->getLabaByPendaftaran('obat', 'bulanan');
            $data['total_obat_racikan'] = $this->KeuanganModel->getLabaByPendaftaran('obat racikan', 'bulanan');
            $data['total_jasa_racik'] = $this->KeuanganModel->getLabaJasaRacikByPendaftaran('bulanan');
            $data['total_bahan_habis_pakai'] = $this->KeuanganModel->getLabaByPendaftaran('bahan habis pakai', 'bulanan');
            $data['total_by_jenis_layanan'] = $this->KeuanganModel->getLabaByLayanan('bulanan');
            $data['total_obat_resep_luar'] = $this->KeuanganModel->getLabaObatLuarByPendaftaran();
        }

        $data['from'] = $start_date;
        $data['to'] = $end_date;

        $this->template->view('keuangan/resume_pemasukan',$data);
    }

    public function resume_pengeluaran()
    {
        $jenis = $this->input->get('jenis');
        $from = $this->input->get('from');
        $to = $this->input->get('to');
        $bulan = $this->input->get('bulan');
        $tahun = $this->input->get('tahun');

        $start_date = $end_date = date('Y-m-d');
        if ($jenis == 2) {
            $start_date = $from;
            $end_date = $to;
        }
        if ($jenis == 3) {
            $start_date = date("$tahun-$bulan-01");
            $end_date = date("$tahun-$bulan-t");
        }

        $data['jenis'] = $jenis;
        $data['from'] = $from;
        $data['to'] = $to;
        $data['bulan'] = $bulan;
        $data['tahun'] = $tahun;

        $data['list'] = $this->PengeluaranModel->get_pengeluaran_rutin_harian_by_periode($start_date, $end_date);
        $this->template->view('keuangan/resume_pengeluaran', $data);
    }

    public function pengeluaran_rutin_harian()
    {
        $data['list'] = $this->PengeluaranModel->get_pengeluaran_rutin_harian();
        $this->template->view('keuangan/pengeluaran_rutin/list', $data);
    }

    public function create_pengeluaran_rutin_harian()
    {
        if ($this->input->post('submit') == 1) {
            $insert = $this->MainModel->insert('pengeluaran_rutin_harian', [
                'd_date' => $this->input->post('d_date'),
                'pengeluaran_item_id' => $this->input->post('pengeluaran_item_id'),
                'keterangan' => $this->input->post('keterangan'),
                'jumlah' => $this->input->post('jumlah'),
            ]);

            if ($insert) {
                $this->session->set_flashdata('success', 'Tambah data berhasil!');
                redirect('Keuangan/pengeluaran_rutin_harian', 'refresh');
            }
            else {
                $this->session->set_flashdata('warning', 'Tambah data gagal!');
                redirect('Keuangan/pengeluaran_rutin_harian', 'refresh');
            }
        }
        else {
            $data['items'] = $this->PengeluaranModel->get_list_items()->result();
            $this->template->view('keuangan/pengeluaran_rutin/create', $data);
        }
    }

    public function edit_pengeluaran_rutin_harian($id)
    {
        if ($this->input->post('submit') == 1) {
            $insert = $this->MainModel->update('pengeluaran_rutin_harian', [
                'd_date' => $this->input->post('d_date'),
                'pengeluaran_item_id' => $this->input->post('pengeluaran_item_id'),
                'keterangan' => $this->input->post('keterangan'),
                'jumlah' => $this->input->post('jumlah'),
            ], $id);

            if ($insert) {
                $this->session->set_flashdata('success', 'Edit data berhasil!');
                redirect('Keuangan/pengeluaran_rutin_harian', 'refresh');
            }
            else {
                $this->session->set_flashdata('warning', 'Edit data gagal!');
                redirect('Keuangan/pengeluaran_rutin_harian', 'refresh');
            }
        }
        else {
            $data['data'] = $this->PengeluaranModel->get_pengeluaran_rutin_harian_by_id($id);
            $data['items'] = $this->PengeluaranModel->get_list_items()->result();
            $this->template->view('keuangan/pengeluaran_rutin/edit', $data);
        }
    }

    public function delete_pengeluaran_rutin_harian($id)
    {
        $this->MainModel->delete($table='pengeluaran_rutin_harian', ['is_active' => '0'], $id);
        $this->session->set_flashdata('success', 'Data berhasil dihapus!');
        redirect('Keuangan/pengeluaran_rutin_harian');
    }

    public function resume_pemasukan_()
    {
        $data['kode_poli'] = $this->config->item('poli');
        $data['jenis_pendaftaran_all'] = $this->PendaftaranModel->getJenisPendaftaran()->result();
        $jns_ruang = $this->db->query("SELECT * FROM jenis_ruangan WHERE is_active = 1 AND nama != 'Poli'")->result();
        $jns_ruang = array_map(function ($v) {
            return (object) [
                'id'=> 'ri-'.$v->id,
                'nama'=> $v->nama,
                'kode'=> $v->kode,
            ];
        }, $jns_ruang);
        $data['jenis_pendaftaran_all'] = array_merge($data['jenis_pendaftaran_all'], $jns_ruang);
        $data['jaminan'] = $this->config->item('pendaftaran');
        $data['tindakan_all'] = $this->db->query("SELECT * FROM tarif_tindakan WHERE is_active = 1")->result();
        $data['tindakan_lab_all'] = $this->db->query("
            SELECT * FROM jenis_layanan_laboratorium 
            WHERE is_active = 1 
        ")->result();
        $data['tindakan_lab_all_merge'] = array_merge($data['tindakan_all'], $data['tindakan_lab_all']);
        $data['dokter_all'] = $this->PendaftaranModel->getDokter();

        $jenis = $this->input->get('jenis');
        $from = $this->input->get('from');
        $to = $this->input->get('to');
        $bulan = $this->input->get('bulan');
        $tahun = $this->input->get('tahun');
        $poli = $this->input->get('poli') ?? '';
        $tindakan = $this->input->get('tindakan') ?? [];
        $jenis_pendaftaran = $this->input->get('jenis_pendaftaran') ?? '';
        $dokter = $this->input->get('dokter') ?? '';

        $start_date = $end_date = date('Y-m-d');
        if ($jenis == 2) {
            $start_date = $from;
            $end_date = $to;
        }
        if ($jenis == 3) {
            $start_date = date("$tahun-$bulan-01");
            $end_date = date("$tahun-$bulan-t");
        }

        $data['jenis'] = $jenis;
        $data['from'] = $from;
        $data['to'] = $to;
        $data['bulan'] = $bulan;
        $data['tahun'] = $tahun;
        $data['poli'] = $poli;
        $data['tindakan'] = $tindakan;
        $data['jenis_pendaftaran'] = $jenis_pendaftaran;
        $data['dokter'] = $dokter;

        if ($poli == 'konsumsi') {
            $data['jasa_medis'] = $this->KeuanganModel->getListPemasukanKonsumsi($start_date, $end_date);
        }
        else {
            $data['jasa_medis'] = $this->KeuanganModel->getListPemasukanTindakan($start_date, $end_date, $poli, $tindakan, $jenis_pendaftaran, $dokter);
        }

        $this->template->view('keuangan/resume_pemasukan_', $data);
    }

    public function resume_frekuensi_tindakan()
    {
        $data['kode_poli'] = $this->config->item('poli');
        $data['jenis_pendaftaran_all'] = $this->PendaftaranModel->getJenisPendaftaran();
        $data['jaminan'] = $this->config->item('pendaftaran');

        $jenis = $this->input->get('jenis');
        $from = $this->input->get('from');
        $to = $this->input->get('to');
        $bulan = $this->input->get('bulan');
        $tahun = $this->input->get('tahun');
        $poli = $this->input->get('poli') ?? '';
        $jenis_pendaftaran = $this->input->get('jenis_pendaftaran') ?? '';

        $start_date = $end_date = date('Y-m-d');
        if ($jenis == 2) {
            $start_date = $from;
            $end_date = $to;
        }
        if ($jenis == 3) {
            $start_date = date("$tahun-$bulan-01");
            $end_date = date("$tahun-$bulan-t");
        }

        $data['jenis'] = $jenis;
        $data['from'] = $from;
        $data['to'] = $to;
        $data['bulan'] = $bulan;
        $data['tahun'] = $tahun;
        $data['poli'] = $poli;
        $data['jenis_pendaftaran'] = $jenis_pendaftaran;

        $data['list'] = $this->KeuanganModel->getFrekuensiTindakan($start_date, $end_date, $poli, $jenis_pendaftaran);

        $this->template->view('keuangan/resume_frekuensi_tindakan', $data);
    }

    public function detail_resume_frekuensi_tindakan($tindakan_id)
    {
        $data['kode_poli'] = $this->config->item('poli');
        $data['jenis_pendaftaran_all'] = $this->PendaftaranModel->getJenisPendaftaran();
        $data['jaminan'] = $this->config->item('pendaftaran');

        $jenis = $this->input->get('jenis');
        $from = $this->input->get('from');
        $to = $this->input->get('to');
        $bulan = $this->input->get('bulan');
        $tahun = $this->input->get('tahun');
        $poli = $this->input->get('poli') ?? '';
        $jenis_pendaftaran = $this->input->get('jenis_pendaftaran') ?? '';

        $start_date = $end_date = date('Y-m-d');
        if ($jenis == 2) {
            $start_date = $from;
            $end_date = $to;
        }
        if ($jenis == 3) {
            $start_date = date("$tahun-$bulan-01");
            $end_date = date("$tahun-$bulan-t");
        }

        $data['jenis'] = $jenis;
        $data['from'] = $from;
        $data['to'] = $to;
        $data['bulan'] = $bulan;
        $data['tahun'] = $tahun;
        $data['poli'] = $poli;
        $data['jenis_pendaftaran'] = $jenis_pendaftaran;

        $data['list'] = $this->KeuanganModel->getListPemasukanTindakan($start_date, $end_date, $poli, [$tindakan_id], $jenis_pendaftaran, null);

        $this->template->view('keuangan/detail_resume_frekuensi_tindakan', $data);
    }

    public function penjualan_obat_global_()
    {
        $jenis = $this->input->get('jenis');
        $from = $this->input->get('from');
        $to = $this->input->get('to');
        $bulan = $this->input->get('bulan');
        $tahun = $this->input->get('tahun');
        $poli = $this->input->get('poli') ?? '';

        $start_date = $end_date = date('Y-m-d');
        if ($jenis == 2) {
            $start_date = $from;
            $end_date = $to;
        }
        if ($jenis == 3) {
            $start_date = date("$tahun-$bulan-01");
            $end_date = date("$tahun-$bulan-t");
        }

        $data['jenis'] = $jenis;
        $data['from'] = $from;
        $data['to'] = $to;
        $data['bulan'] = $bulan;
        $data['tahun'] = $tahun;
        $data['poli'] = $poli;

        $periode = ['start' => $start_date, 'end' => $end_date];
        $obat_luar = !$poli || $poli == 'x' ? $this->ObatGlobalModel->get_obat_luar($periode) : [];
        $obat_racikan_luar = !$poli || $poli == 'x' ? $this->ObatGlobalModel->get_obat_racikan_luar($periode) : [];
        $obat_pemeriksaan = !$poli || $poli != 'x' ? $this->ObatGlobalModel->get_obat_pemeriksaan($periode, $poli == 'x' ? '' : $poli) : [];
        $obat_racikan_pemeriksaan = !$poli || $poli != 'x' ? $this->ObatGlobalModel->get_obat_racikan_pemeriksaan($periode, $poli == 'x' ? '' : $poli) : [];
        $obats = [];

        function searchById($id, $nama, &$array) {
            foreach ($array as $key => $val) {
                if (($val->obat_id != null && $val->obat_id == $id) || $val->nama_obat == $nama) {
                    return $val;
                }
            }
            return null;
        }

        $all = [
            $obat_luar,
            $obat_racikan_luar,
            $obat_pemeriksaan,
            $obat_racikan_pemeriksaan
        ];

        foreach ($all as $o) {
            foreach ($o as $k1 => $o1) {

                $found = searchById($o1->obat_id, $o1->nama_obat, $obats);
                if ($found) {
                    $ext = $o1;
                    $ext->jumlah += $found->jumlah;
                    foreach ($obats as $k => $obat) {
                        if (($obat->obat_id != null && $obat->obat_id == $o1->obat_id) || $obat->nama_obat == $o1->nama_obat) {
                            unset($obats[$k]);
                            break;
                        }
                    }
                    $obats[] = $ext;
                }
                else {
                    $obats[] = $o1;
                }
            }
        }

        $obats = array_filter($obats, function ($var) {
            return ($var->jumlah > 0);
        });

        usort($obats, function ($a, $b) {
            return $b->jumlah - $a->jumlah;
        });

        $data['jenis_pendaftaran_all'] = $this->PendaftaranModel->getJenisPendaftaran();
        $data['detail'] = $obats;

        $this->template->view('keuangan/penjualan_obat_global', $data);
    }

    public function listTotalPiutang() {
        $dari = $this->input->get('from');
        $sampai = $this->input->get('to');

        if (!$dari) $dari = date('Y-m-01');
        if (!$sampai) $sampai = date('Y-m-d');

        $data['total_by_jenis_layanan'] = $this->KeuanganModel->getLabaByLayanan(['start' => $dari, 'end' => $sampai]);
        $data['from'] = $dari;
        $data['to'] = $sampai;

        $this->template->view('keuangan/list_total_piutang_v',$data);
    }

    public function detail_piutang() {
        $jaminan = $this->input->get('jaminan');
        $dari = $this->input->get('from');
        $sampai = $this->input->get('to');

        if (!$dari) $dari = date('Y-m-01');
        if (!$sampai) $sampai = date('Y-m-d');

        $data['list'] = $this->KeuanganModel->getListPiutangByJaminan(['start' => $dari, 'end' => $sampai], $jaminan);
        $data['jaminan'] = $jaminan;
        $data['from'] = $dari;
        $data['to'] = $sampai;

        $this->template->view('keuangan/detail_piutang', $data);
    }

    public function listPengeluaranObat() {
        //$data['tindakan'] = $this->KeuanganModel->getTindakanPemeriksaan();
        $data['obat'] = $this->KeuanganModel->getObatPemeriksaan();
        //print_r($data['tindakan']->result());die();
        $this->template->view('keuangan/list_pengeluaran_obat_v',$data);
    }

    public function listPengeluaranTindakan() {
        $data['tindakan'] = $this->KeuanganModel->getTindakanPemeriksaan();
        //$data['obat'] = $this->KeuanganModel->getObatPemeriksaan();
        //print_r($data['tindakan']->result());die();
        $this->template->view('keuangan/list_pengeluaran_tindakan_v',$data);
    }

    public function listTotalPengeluaran_OLD() {
        $data['tindakan'] = $this->KeuanganModel->getTotalTindakan();
        $data['obat'] = $this->KeuanganModel->getTotalObatPemeriksaan();
        //print_r($data['tindakan']->result());die();
        $this->template->view('keuangan/list_total_pengeluaran_v',$data);
    }

    public function listTotalPengeluaran() {
        $data['dokter'] = $this->InsentifModel->listInsentifDokter(null, date('Y-m-01'), date('Y-m-d'));
		$data['perawat'] = $this->InsentifModel->listInsentifPerawat(null, date('Y-m-01'), date('Y-m-d'));

        $this->template->view('keuangan/list_total_pengeluaran_new_v',$data);
    }

    public function labaRugi_OLD() {
        $data['tindakan'] = $this->KeuanganModel->getTotalTindakan();

        $data['obat'] = $this->KeuanganModel->getTotalObatPemeriksaan();

        $data['total_tindakan'] = $this->KeuanganModel->getTotallabaTindakan();

        $this->template->view('keuangan/laba_rugi_v',$data);
    }

    public function labaRugi() {
		$start_date = $this->input->get('from');
		$end_date = $this->input->get('to');

		if ($start_date && $end_date) {
		    $date['start'] = $start_date;
		    $date['end'] = $end_date;

            $data['total_jasa_medis'] = $this->KeuanganModel->getLabaByPendaftaran('tindakan', $date);
            $data['total_obat'] = $this->KeuanganModel->getLabaByPendaftaran('obat', $date);
            $data['total_obat_racikan'] = $this->KeuanganModel->getLabaByPendaftaran('obat racikan', $date);
            $data['total_jasa_racik'] = $this->KeuanganModel->getLabaByPendaftaran('jasa racik', $date);
            $data['total_bahan_habis_pakai'] = $this->KeuanganModel->getLabaByPendaftaran('bahan habis pakai', $date);
            $data['total_by_jenis_layanan'] = $this->KeuanganModel->getLabaByLayanan($date);
            $data['total_obat_resep_luar'] = $this->KeuanganModel->getLabaObatLuarByPendaftaran($date);

            $data['insentif_dokter'] = $this->InsentifModel->listInsentifDokter('', $start_date, $end_date);
    		$data['insentif_perawat'] = $this->InsentifModel->listInsentifPerawat('', $start_date, $end_date);
		}
		else {
            $data['total_jasa_medis'] = $this->KeuanganModel->getLabaByPendaftaran('tindakan', 'bulanan');
            $data['total_obat'] = $this->KeuanganModel->getLabaByPendaftaran('obat', 'bulanan');
            $data['total_obat_racikan'] = $this->KeuanganModel->getLabaByPendaftaran('obat racikan', 'bulanan');
            $data['total_jasa_racik'] = $this->KeuanganModel->getLabaByPendaftaran('jasa racik', 'bulanan');
            $data['total_bahan_habis_pakai'] = $this->KeuanganModel->getLabaByPendaftaran('bahan habis pakai', 'bulanan');
            $data['total_by_jenis_layanan'] = $this->KeuanganModel->getLabaByLayanan('bulanan');
            $data['total_obat_resep_luar'] = $this->KeuanganModel->getLabaObatLuarByPendaftaran();

            $data['insentif_dokter'] = $this->InsentifModel->listInsentifDokter('', date('Y-m-01'), date('Y-m-d'));
    		$data['insentif_perawat'] = $this->InsentifModel->listInsentifPerawat('', date('Y-m-01'), date('Y-m-d'));
		}

        $this->template->view('keuangan/laba_rugi_v',$data);
    }

    public function rekap_ambulance()
    {
        $jenis = $this->input->get('jenis');
        $from = $this->input->get('from');
        $to = $this->input->get('to');
        $bulan = $this->input->get('bulan');
        $tahun = $this->input->get('tahun');
        $jaminan = $this->input->get('jaminan');

        $data['jenis'] = $jenis;
        $data['from'] = $from;
        $data['to'] = $to;
        $data['bulan'] = $bulan;
        $data['tahun'] = $tahun;
        $data['s_jaminan'] = $jaminan;

        $start_date = $end_date = date('Y-m-d');
        if ($jenis == 2) {
            $start_date = $from;
            $end_date = $to;
        }
        if ($jenis == 3) {
            $start_date = date("$tahun-$bulan-01");
            $end_date = date("$tahun-$bulan-t");
        }
        $periode = ['start' => $start_date, 'end' => $end_date];

        $data['vehiclelist'] = $this->vehicle_model->get();
        $data['list'] = $this->PanggilanAmbulanceModel->getSelesai($periode, $jaminan);
        $data['tarif_ambulance'] = $this->TarifAmbulanceModel->getAll();
        $data['jaminan'] = $this->config->item('pendaftaran');

        $this->template->view('keuangan/rekap_ambulance', $data);
    }
}
