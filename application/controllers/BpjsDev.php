<?php

class BpjsDev extends MY_BpjsController
{
    public function get_signature()
    {
        echo json_encode(parent::signature());
    }

    public function poli($key)
    {
        die($this->ajax_search_poli($key));
    }

    public function decrypt()
    {
        $this->timestamp = $this->input->post('timestamp');
        $decrypted = $this->stringDecrypt($this->input->post('data'));
        $decompressed = $this->decompress($decrypted);
        die(json_encode($decompressed));
    }

    public function antrian_getlisttask($kodebooking)
    {
        $poli = $this->get("{$this->base_url_antrean}ref/poli");
        $r = $this->post("{$this->base_url_antrean}antrean/getlisttask", json_encode(['kodebooking' => $kodebooking]), true);
        die(json_encode($r));
    }

    public function exec()
    {
        $body = [
            "kodebooking" => "17H7EB9ZZ9",
            "jenispasien" => "JKN",
            "nomorkartu" => "0002043221016",
            "nik" => "6403055309810001",
            "nohp" => "q",
            "kodepoli" => "BED",
            "namapoli" => "Poli Bedah",
            "pasienbaru" => 1,
            "norm" => "0000001111",
            "tanggalperiksa" => "2022-11-02",
            "kodedokter" => 6516,
            "namadokter" => "dr. M. ANGGA N. MUKTI, Sp.B., FINACS",
            "jampraktek" => "08:00-16:00",
            "jeniskunjungan" => 1,
            "nomorreferensi" => "133505011022P000002",
            "nomorantrean" => "B003",
            "angkaantrean" => 3,
            "estimasidilayani" => 1667375137,
            "sisakuotajkn" => 27,
            "kuotajkn" => 30,
            "sisakuotanonjkn" => 30,
            "kuotanonjkn" => 30,
            "keterangan" => "Peserta harap 60 menit lebih awal guna pencatatan administrasi."
        ];
//        $r = $this->post("{$this->base_url_antrean}antrean/add", json_encode($body), true);
//        die(json_encode($r));
    }
}