<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class RuangBersalin extends Admin_Controller
{
    // 2 = bersalin
    private $jenis_ruangan_id = 2;

    public function __construct()
    {
        parent::__construct();

        $this->marital_status = $this->config->item('marital_status');
        $this->payment_mode = $this->config->item('payment_mode');
        $this->blood_group = $this->config->item('bloodgroup');
        $this->load->model('RuangBersalinModel');
        $this->load->model('PendaftaranModel');
        $this->load->model('MainModel');
        $this->load->model('ObatModel');
        $this->load->model('AdministrasiModel');
        $this->load->model('PerawatModel');
        $this->load->model('PemeriksaanModel');
        $this->load->model('TarifTindakanModel');
        $this->load->model('PenyakitModel');
        $this->load->model('RawatInapModel');
        $this->load->model('TransferPasienModel');
        $this->load->model('LaporanModel');
        $this->load->model('JenisLayananLaboratoirumModel');

        $this->load->library('Customlib');
        $this->load->library('template');
    }

    private function setDari(&$d)
    {
        if ($d['via_bangsal']) {
            $d['dari'] = $d['bed_name'].' - '.$d['bedgroup'];
        }
        else {
            $d['dari'] = $d['nama_poli'];
        }
    }

    public function hapusPasien($id)
    {
        $this->MainModel->hardDelete('rawat_inap', $id);
        $this->MainModel->hardDeleteWhere('transfer', ['rawat_inap_id', $id]);
        redirect('RuangBersalin/showList');
    }

    public function showList()
    {
        $data['list'] = $this->RuangBersalinModel->getBaru();
        foreach ($data['list'] as &$d) {
            $this->setDari($d);
        }

        $data['is_superadmin'] = $this->session->userdata('logged_in')->id == 1;
        $this->template->view('ruang_bersalin/list', $data);

        
    }

    public function listBilling()
    {
        $data['list'] = $this->RuangBersalinModel->getForBilling();
        foreach ($data['list'] as &$d) {
            $this->setDari($d);
        }
        $this->template->view('ruang_bersalin/list_billing', $data);
    }

    public function listRekapitulasi()
    {
        $data['list'] = $this->RuangBersalinModel->getSelesai();
        foreach ($data['list'] as &$d) {
            $this->setDari($d);
        }
        $this->template->view('ruang_bersalin/list_rekapitulasi', $data);
    }

    public function dataTransfer($jenis)
    {
        $data['jenis'] = $jenis;
        if ($jenis == 'masuk') {
            $data['list'] = $this->RuangBersalinModel->getDataTransferMasuk();
            foreach ($data['list'] as &$d) {
                $this->setDari($d);
            }
        }
        else {
            $data['list'] = $this->RuangBersalinModel->getDataTransferKeluar();
            foreach ($data['list'] as &$d) {
                $this->setDari($d);
            }
        }

        $this->template->view('ruang_bersalin/list_data_transfer', $data);
    }

    public function getDetailBilling(&$data, $sudah_bayar = false)
    {
        $this->getDetailBillingWithTransferId($data, null, $sudah_bayar);
    }

    public function getDetailBillingWithTransferId(&$data, $transfer_id, $sudah_bayar = false)
    {
        $ri_id = $data['rawat_inap']->id;

        if (!$transfer_id) {
            if ($sudah_bayar) {
                $transfer_id = $this->RuangBersalinModel->getLastBersalinTransferByRawatInapId($ri_id)->id;
            }
            else {
                $transfer_id = $data['rawat_inap']->transfer_id;
            }
        }

        $data['bayar'] = $sudah_bayar ? $this->RuangBersalinModel->getBayarByTransferId($transfer_id, $this->jenis_ruangan_id) : '';
        $data['pemeriksaan'] = $this->RuangBersalinModel->getPemeriksaanByTransferId($transfer_id, $this->jenis_ruangan_id);
        $data['diagnosis'] = $this->RuangBersalinModel->getDiagnosisByTransferId($transfer_id, $this->jenis_ruangan_id);
        foreach ($data['diagnosis'] as &$d) {
            $d->penyakit = $this->RuangBersalinModel->getPenyakitByDiagnosisId($d->id);
        }

        $data['resep'] = $this->RuangBersalinModel->getResepByTransferId($transfer_id, $this->jenis_ruangan_id);
        foreach ($data['resep'] as &$d) {
            $d->obat = $this->RuangBersalinModel->getObatByResepId($d->id);
            $d->obat_racik = $this->RuangBersalinModel->getObatRacikByResepId($d->id);
            foreach ($d->obat_racik as &$dd) {
                $dd->detail = $this->RuangBersalinModel->getObatRacikDetailByResepObatRacikId($dd->id);
                $dd->total = 0;
                foreach ($dd->detail as $ddd) {
                    $dd->total += $ddd->jumlah * $ddd->harga_jual;
                }
            }
        }

        $data['bahan_habis_pakai'] = $this->RuangBersalinModel->getBhpByTransferId($transfer_id, $this->jenis_ruangan_id);
        foreach ($data['bahan_habis_pakai'] as &$d) {
            $d->bahan = $this->RuangBersalinModel->getBahanByBhpId($d->id);
        }

        $data['biaya'] = $this->RuangBersalinModel->getBiayaByTransferId($transfer_id, $this->jenis_ruangan_id);
        foreach ($data['biaya'] as &$d) {
            if ($d->jenis_biaya == 'Makanan') {
                $d->makanan = $this->RuangBersalinModel->getMakananById($d->makanan_id);
            }
            else if ($d->jenis_biaya == 'Tindakan') {
                $d->tindakan = $this->RuangBersalinModel->getTindakanByBiayaId($d->id);
            }
        }
        $data['makanan_count_list'] = $this->RuangBersalinModel->getMakananCountByTransferId($transfer_id, $this->jenis_ruangan_id);

        // TODO ini hanya berlaku jika blm ada transfer ke selain bangsal; di bayar juga ya; dan di print
        $tf = $this->RuangBersalinModel->getLastBersalinTransferByRawatInapId($ri_id);
        $bed[] = [
            'start' => $tf->created_at,
            'end' => 'now',
            'bed' => $this->bed_model->bed_listsearch($data['rawat_inap']->tujuan),
        ];

        while ($tf->prev_transfer_id != null) {
            $end = $tf->created_at;
            $tujuan = $tf->tujuan;
            $cur = $tf->current_place_type;

            $tf = $this->RuangBersalinModel->getTransferById($tf->prev_transfer_id);

            if ($cur == 'bangsal') {
                $bed[] = [
                    'start' => $tf->created_at,
                    'end' => $end,
                    'bed' => $this->bed_model->bed_listsearch($tujuan),
                ];
            }
        }
        $data['beds'] = array_reverse($bed);
        $data['beds'] = [];

        $data['all_tindakan'] = $this->LaporanModel->getTindakanById($data['pasien']->id);
        if ($data['rawat_inap']->pendaftaran_id) {
            $data['lab'] = $this->PendaftaranModel->getPemeriksaanByPendaftaranIdAndJenisPendaftaranId($data['rawat_inap']->pendaftaran_id, 19, 'desc');
            foreach ($data['lab'] as &$datum) {
                $datum->layanan = $this->JenisLayananLaboratoirumModel->byIdPemeriksaan($datum->id);
            }
            $data['radio'] = $this->PendaftaranModel->getPemeriksaanByPendaftaranIdAndJenisPendaftaranId($data['rawat_inap']->pendaftaran_id, 59, 'desc');
        }
        else {
            $data['lab'] = [];
            $data['radio'] = [];
        }
    }

    public function detail($id, $transfer_id, $tab = 'pemeriksaan')
    {
        $data['rawat_inap'] = $this->RuangBersalinModel->getById($id);
        $data['transfer_id'] = $transfer_id;
        $data['pasien'] = $this->RuangBersalinModel->getPasienById($data['rawat_inap']->pasien_id);
        $data['dokter'] = $this->PendaftaranModel->getDokter();
        $data['penyakit'] = $this->PenyakitModel->getListRuangBersalin()->result();
        $data['tindakan'] = $this->TarifTindakanModel->getListRuangBersalin()->result();
        $data['listPerawat'] = $this->PerawatModel->listPerawat();
        $data['tipe_makanan'] = $this->RuangBersalinModel->getAllTipeMakanan();
        $data['obat1'] = $this->db->get_where('obat', ['is_active' => 1, 'stok_obat >' => 0])->result();
        $data['bahan'] = $this->db->get_where('bahan_habis_pakai', ['is_active' => 1, 'jumlah >' => 0])->result();
        $data['tab'] = $tab;
        $bed = $this->bed_model->bed_listsearch($data['rawat_inap']->dari);

        if ($data['rawat_inap']->via_bangsal) {
            $data['dari'] = $bed['name'].' - '.$bed['bedgroup'];
        }
        else {
            $data['dari'] = $data['rawat_inap']->nama_poli;
        }

        $this->getDetailBillingWithTransferId($data, $transfer_id);
        $this->template->view('ruang_bersalin/detail', $data);
    }

    public function detailRekamMedis($id, $transfer_id)
    {
        $data['rawat_inap'] = $this->RuangBersalinModel->getById($id);
        $data['pasien'] = $this->RuangBersalinModel->getPasienById($data['rawat_inap']->pasien_id);
        $data['dokter'] = $this->PendaftaranModel->getDokter();
        $data['penyakit'] = $this->PenyakitModel->getListRuangBersalin()->result();
        $data['tindakan'] = $this->TarifTindakanModel->getListRuangBersalin()->result();
        $data['listPerawat'] = $this->PerawatModel->listPerawat();
        $data['tipe_makanan'] = $this->RuangBersalinModel->getAllTipeMakanan();
        $data['obat1'] = $this->db->get_where('obat', ['is_active' => 1, 'stok_obat >' => 0])->result();
        $data['bahan'] = $this->db->get_where('bahan_habis_pakai', ['is_active' => 1, 'jumlah >' => 0])->result();
        $bed = $this->bed_model->bed_listsearch($data['rawat_inap']->dari);
        $data['dari'] = $bed['name'].' - '.$bed['bedgroup'];

        $this->getDetailBillingWithTransferId($data, $transfer_id);
        $this->template->view('ruang_bersalin/detail_rekam_medis', $data);
    }

    public function transfer($jenis, $rawat_inap_id)
    {
        $jenis_transfer = $this->TransferPasienModel->getJenisTransferById($jenis);

        $data['jenis'] = $jenis;
        $data['belum_ada_bangsal'] = $this->input->post('belum_ada_bangsal');
        $data['dari'] = "Ruang Bersalin";
        $data['tujuan'] = $this->TransferPasienModel->getJenisRuanganById($jenis_transfer->ke_jenis_ruangan_id)->nama;
        $data['dokter'] = $this->PendaftaranModel->getDokter();
        $data['rawat_inap'] = $this->RawatInapModel->getById($rawat_inap_id);
        $data['pasien'] = $this->PendaftaranModel->getPasienById($data['rawat_inap']->pasien_id)->row();
        $data['total_biaya'] = $this->input->post('total_biaya');
        $data['redirect_to'] = base_url().'RuangBersalin/insertTransfer/'.$data['rawat_inap']->transfer_id;

        $this->template->view('transfer/add', $data);
    }

    public function insertTransfer()
    {
        $rawat_inap_id = $this->input->post('rawat_inap_id');
        $ri = $this->RuangBersalinModel->getById($rawat_inap_id);
        $jenis = $this->input->post('jenis');
        $jenis_transfer = $this->TransferPasienModel->getJenisTransferById($jenis);

        $this->MainModel->update('transfer', [
            'status' => 'diperiksa',
            'diperiksa_at' => date('Y-m-d H:i:s'),
            'total_biaya' => $this->input->post('total_biaya'),
        ], $ri->transfer_id);

        $transfer = [
            'rawat_inap_id' => $rawat_inap_id,
            'prev_transfer_id' => $ri->transfer_id,
            'jenis' => $jenis,
            'tanggal' => $this->input->post('tanggal'),
            'pukul' => $this->input->post('pukul'),
            'pasien_id' => $ri->pasien_id,
            'dokter_id' => $this->input->post('dokter'),
            'penanggung_jawab' => $ri->penanggung_jawab,
            'jenis_ruangan_id' => $jenis_transfer->ke_jenis_ruangan_id,
            'dari' => $this->input->post('dari'),
            'tujuan' => $this->input->post('tujuan'),
            'current_place_type' => strtolower($this->TransferPasienModel->getJenisRuanganById($jenis_transfer->ke_jenis_ruangan_id)->nama),
            'current_place_id' => $this->input->post('tujuan'),
            'tipe_pasien' => $ri->tipe_pasien,
            'catatan' => $this->input->post('note'),
            'status' => 'ditransfer',
            'meta' => serialize($this->input->post('meta')),
            'transfered_at' => date('Y-m-d H:i:s'),
            'via_bangsal' => $this->input->post('belum_ada_bangsal') ? 0 : 1,
        ];
        $id = $this->MainModel->insert_id('transfer', $transfer);

        $trans = $this->TransferPasienModel->getTransferDetailById($id);

        $rawat_inap = [
            'transfer_id' => $id,
            'pasien_id' => $trans->pasien_id,
            'dokter_id' => $trans->dokter_id,
            'penanggung_jawab' => $trans->penanggung_jawab,
            'dari' => $trans->dari,
            'tujuan' => $trans->tujuan,
            'current_place_type' => $trans->current_place_type,
            'current_place_id' => $trans->current_place_id,
            'tipe_pasien' => unserialize($trans->meta)['status_pasien'],
            'catatan' => $trans->catatan,
            'meta' => $trans->meta,
        ];
        $this->MainModel->update('rawat_inap', $rawat_inap, $rawat_inap_id);
//        die(json_encode([$trans, $this->RuangBersalinModel->getById($rawat_inap_id)]));

        $ke_ruangan = $this->TransferPasienModel->getJenisRuanganById($jenis_transfer->ke_jenis_ruangan_id)->nama;
        $this->session->set_flashdata('success', "Berhasil transfer ke $ke_ruangan!");
        $r = $jenis_transfer->ke_jenis_ruangan_id == 1 ? 'RawatInap' : str_replace(' ', '', $ke_ruangan);
        redirect($r.'/showList');
    }

    // ini dipanggil ketiksa selesai bersalin; entah via bangsal or not; trus ini membuka form
    public function selesai($rawat_inap_id)
    {
        $data['jenis'] = 4; // op 2; ber 4
        $data['belum_ada_bangsal'] = $this->input->post('belum_ada_bangsal');
        $data['dari'] = "Ruang Bersalin";
        $data['dokter'] = $this->PendaftaranModel->getDokter();
        $data['rawat_inap'] = $this->RawatInapModel->getById($rawat_inap_id);
        $data['pasien'] = $this->PendaftaranModel->getPasienById($data['rawat_inap']->pasien_id)->row();
        $data['total_biaya'] = $this->input->post('total_biaya');
        $data['redirect_to'] = base_url().'RuangBersalin/selesaiIsiTransfer/'.$data['rawat_inap']->id;

        if ($data['belum_ada_bangsal']) {
            $data['bed'] = $this->bed_model->bed_listsearch();
        }

        $this->template->view('transfer/add_back_to_bangsal', $data);
    }

    // ini dipanggil ketiksa selesai isi transfer; ntah via bangsal or not, yg jelas dia akan menuju ke bangsal
    public function selesaiIsiTransfer($rawat_inap_id)
    {
        $belum_ada_bangsal = $this->input->post('belum_ada_bangsal');

        if ($belum_ada_bangsal) {
            $ri = $this->RuangBersalinModel->getById($rawat_inap_id);

            $this->MainModel->update('transfer', [
                'status' => 'diperiksa',
                'diperiksa_at' => date('Y-m-d H:i:s'),
                'total_biaya' => $this->input->post('total_biaya'),
            ], $ri->transfer_id);

            $transfer = [
                'rawat_inap_id' => $rawat_inap_id,
                'prev_transfer_id' => $ri->transfer_id,
                'jenis' => 6, // ruang bersalin ke bangsal
                'tanggal' => $this->input->post('tanggal'),
                'pukul' => $this->input->post('pukul'),
                'transfered_at' => date('Y-m-d H:i:s'),
                'pasien_id' => $ri->pasien_id,
                'dokter_id' => $ri->dokter_id,
                'penanggung_jawab' => $ri->penanggung_jawab,
                'jenis_ruangan_id' => 1,
                'dari' => $this->input->post('dari'),
                'tujuan' => $this->input->post('tujuan'),
                'current_place_type' => 'bangsal',
                'current_place_id' => $this->input->post('tujuan'),
                'tipe_pasien' => $ri->tipe_pasien,
                'catatan' => $this->input->post('note'),
                'status' => 'baru',
                'meta' => serialize($this->input->post('meta')),
                'via_bangsal' => 0
            ];
            $tf_id = $this->MainModel->insert_id('transfer', $transfer);

//            $this->MainModel->update('rawat_inap', [
//                'transfer_id' => $tf_id,
//                'dari' => $this->input->post('dari'),
//                'tujuan' => $this->input->post('tujuan'),
//                'status' => 'Masuk',
//                'current_place_type' => 'bangsal',
//                'current_place_id' => $this->input->post('tujuan'),
//                'via_bangsal' => 1
//            ], $rawat_inap_id);

//            $this->MainModel->update('bed', ['is_active' => 'no'], $this->input->post('tujuan'));

            $this->session->set_flashdata('success', 'Persalinan selesai');
            redirect('RuangBersalin/listBilling');
            return;
        }

        // di bawah ini, asalnya ada di submitBayar
        // ini adalah sebelum by-pass
        $ri = $this->RuangBersalinModel->getById($rawat_inap_id);
        $transfer_id = $ri->transfer_id;
        $transfer = $this->RuangBersalinModel->getTransferById($transfer_id);
        $prev_trans = $this->RuangBersalinModel->getTransferById($transfer->prev_transfer_id);

        $this->MainModel->update('rawat_inap', [
            'transfer_id' => $prev_trans->id,
            'dari' => $prev_trans->dari,
            'tujuan' => $prev_trans->tujuan,
            'status' => 'Masuk',
            'current_place_type' => $prev_trans->current_place_type,
            'current_place_id' => $prev_trans->current_place_id,
        ], $rawat_inap_id);
        // end of ini adalah sebelum by-pass

        $this->MainModel->update('transfer', [
            'status' => 'diperiksa',
            'diperiksa_at' => date('Y-m-d H:i:s'),
            'total_biaya' => $this->input->post('total_biaya'),
        ], $transfer_id);

        // INSERT NEW TRANSFER
        $transfer = [
            'rawat_inap_id' => $rawat_inap_id,
            'prev_transfer_id' => $transfer_id,
            'jenis' => 6, // ruang bersalin ke bangsal
            'tanggal' => $this->input->post('tanggal'),
            'pukul' => $this->input->post('pukul'),
            'pasien_id' => $ri->pasien_id,
            'dokter_id' => $ri->dokter_id,
            'penanggung_jawab' => $ri->penanggung_jawab,
            'jenis_ruangan_id' => 1,
            'dari' => $this->input->post('dari'),
            'tujuan' => 'Rawat Inap',
            'current_place_type' => 'from ruang bersalin',
            'current_place_id' => 0,
            'tipe_pasien' => $ri->tipe_pasien,
            'catatan' => $this->input->post('note'),
            'status' => 'selesai',
            'meta' => serialize($this->input->post('meta')),
        ];
        $this->MainModel->insert_id('transfer', $transfer);
        // END ISERT NEW TRANSFER

        $this->session->set_flashdata('success', 'Persalinan selesai');
        redirect('RuangBersalin/listBilling');
    }

    public function selesaiLangsungBayar($rawat_inap_id, $transfer_id)
    {
        $this->MainModel->update(
            'rawat_inap',
            [
                'status' => 'Selesai',
                'tgl_selesai' => date('Y-m-d H:i:s'),
                'total_biaya' => $this->input->post('total_biaya'),
            ],
            $rawat_inap_id
        );

        $this->MainModel->update('transfer', [
            'status' => 'diperiksa',
            'diperiksa_at' => date('Y-m-d H:i:s'),
            'total_biaya' => $this->input->post('total_biaya'),
        ], $transfer_id);

        $this->session->set_flashdata('success', 'Persalinan selesai');
        redirect('RuangBersalin/listBilling');
    }

    public function bayar($id, $transfer_id, $sudah_bayar = false)
    {
        $data['transfer_id'] = $transfer_id;
        $data['sudah_bayar'] = $sudah_bayar;
        $data['rawat_inap'] = $this->RuangBersalinModel->getById($id);
        $data['pasien'] = $this->RuangBersalinModel->getPasienById($data['rawat_inap']->pasien_id);
        $data['dokter'] = $this->PendaftaranModel->getDokter();
        $data['penyakit'] = $this->RuangBersalinModel->getAllPenyakit();
        $data['tindakan'] = $this->RuangBersalinModel->getAllTindakan();
        $data['tipe_makanan'] = $this->RuangBersalinModel->getAllTipeMakanan();
        $data['obat1'] = $this->db->get_where('obat', ['is_active' => 1, 'stok_obat >' => 0])->result();
        $data['bahan'] = $this->db->get_where('bahan_habis_pakai', ['is_active' => 1, 'jumlah >' => 0])->result();

        $this->getDetailBillingWithTransferId($data, $transfer_id, $sudah_bayar);
        $this->template->view('ruang_bersalin/bayar', $data);
    }

    public function submitBayar()
    {
        $ri_id = $this->input->post('ri_id');
        $transfer_id = $this->input->post('transfer_id');

        $bayar = array(
            'rawat_inap_id' => $ri_id,
            'transfer_id' => $transfer_id,
            'jenis_ruangan_id' => $this->jenis_ruangan_id,
            'jasa_racik' => $this->input->post('jasa_racik'),
            'diskon' => $this->input->post('diskon'),
            'total' => $this->input->post('total_input'),
            'bayar' => $this->input->post('bayar'),
            'kembalian' => $this->input->post('kembalian_input'),
        );
        $id_bayar = $this->MainModel->insert_id('ri_bayar', $bayar);

        //tindakan & makanan
        $data['biaya'] = $this->RuangBersalinModel->getBiayaByTransferId($transfer_id, $this->jenis_ruangan_id);
        foreach ($data['biaya'] as &$d) {
            if ($d->jenis_biaya == 'Makanan') {
                $makanan = $this->RuangBersalinModel->getMakananById($d->makanan_id);
                $byr = array(
                    'ri_bayar_id' => $id_bayar,
                    'item' => $makanan->nama,
                    'jenis_item' => 'makanan',
                    'jumlah' => 1,
                    'harga' => $makanan->harga,
                    'subtotal' => $makanan->harga,
                );
                $this->MainModel->insert('ri_bayar_detail', $byr);
            }
            else if ($d->jenis_biaya == 'Tindakan') {
                $tindakan = $this->RuangBersalinModel->getTindakanByBiayaId($d->id);
                foreach ($tindakan as $t) {
                    $byr = array(
                        'ri_bayar_id' => $id_bayar,
                        'item' => $t->nama,
                        'jenis_item' => 'tindakan',
                        'jumlah' => 1,
                        'harga' => $t->tarif_pasien,
                        'subtotal' => $t->tarif_pasien,
                    );
                    $this->MainModel->insert('ri_bayar_detail', $byr);
                }
            }
        }

        //obat & obat racik
        $data['resep'] = $this->RuangBersalinModel->getResepByTransferId($transfer_id, $this->jenis_ruangan_id);
        foreach ($data['resep'] as &$d) {
            $obat = $this->RuangBersalinModel->getObatByResepId($d->id);
            foreach ($obat as $o) {
                $byr = array(
                    'ri_bayar_id' => $id_bayar,
                    'item' => $o->nama,
                    'jenis_item' => 'obat',
                    'jumlah' => $o->jumlah,
                    'harga' => $o->harga_jual,
                    'subtotal' => $o->harga_jual * $o->jumlah,
                );
                $this->MainModel->insert('ri_bayar_detail', $byr);
            }

            $d->obat_racik = $this->RuangBersalinModel->getObatRacikByResepId($d->id);
            foreach ($d->obat_racik as &$dd) {
                $dd->detail = $this->RuangBersalinModel->getObatRacikDetailByResepObatRacikId($dd->id);
                $dd->total = 0;
                foreach ($dd->detail as $ddd) {
                    $dd->total += $ddd->jumlah * $ddd->harga_jual;
                }

                $byr = array(
                    'ri_bayar_id' => $id_bayar,
                    'item' => $dd->nama_racikan,
                    'jenis_item' => 'obat racikan',
                    'jumlah' => 1,
                    'harga' => '',
                    'subtotal' => $dd->total
                );
                $this->MainModel->insert('ri_bayar_detail', $byr);
            }
        }

        // bhp
        $data['bahan_habis_pakai'] = $this->RuangBersalinModel->getBhpByTransferId($transfer_id, $this->jenis_ruangan_id);
        foreach ($data['bahan_habis_pakai'] as &$d) {
            $d->bahan = $this->RuangBersalinModel->getBahanByBhpId($d->id);
            foreach ($d->bahan as $t) {
                $byr = array(
                    'ri_bayar_id' => $id_bayar,
                    'item' => $t->nama,
                    'jenis_item' => 'bahan habis pakai',
                    'jumlah' => $t->jumlah,
                    'harga' => $t->harga_jual,
                    'subtotal' => $t->harga_jual * $t->jumlah,
                );
                $this->MainModel->insert('ri_bayar_detail', $byr);
            }
        }

        // jasa racik
        if ($this->input->post('jasa_racik')) {
            $jasa_racik = array(
                'ri_bayar_id' => $id_bayar,
                'item' => 'Jasa Racik',
                'jenis_item' => 'jasa racik',
                'jumlah' => 0,
                'harga' => $this->input->post('jasa_racik'),
                'subtotal' => $this->input->post('jasa_racik'),
            );
            $this->MainModel->insert('ri_bayar_detail', $jasa_racik);
        }

        $this->MainModel->update('transfer', ['status' => 'selesai', 'selesai_at' => date('Y-m-d H:i:s')], $transfer_id);

        redirect('RuangBersalin/bayar/'.$ri_id.'/'.$transfer_id.'/'.'true');
    }

    public function tambah($what)
    {
        $transfer_id = $this->input->post('transfer_id');
        if ($what == 'pemeriksaan') {
            $pemeriksaan = [
                'rawat_inap_id' => $this->input->post('rawat_inap_id'),
                'transfer_id' => $transfer_id,
                'jenis_ruangan_id' => $this->jenis_ruangan_id,
                'pasien_id' => $this->input->post('pasien_id'),
                'dokter_id' => $this->input->post('dokter_id'),
                'subjektif' => $this->input->post('subjektif'),
                'objektif' => serialize($this->input->post('objektif')),
                'asesmen' => $this->input->post('asesmen'),
                'plan' => $this->input->post('plan')
            ];
            $this->MainModel->insert('ri_pemeriksaan', $pemeriksaan);
        }
        else if ($what == 'diagnosis') {
            $d = [
                'rawat_inap_id' => $this->input->post('rawat_inap_id'),
                'transfer_id' => $transfer_id,
                'jenis_ruangan_id' => $this->jenis_ruangan_id,
                'pasien_id' => $this->input->post('pasien_id'),
                'diagnosis' => $this->input->post('diagnosis')
            ];
            $id = $this->MainModel->insert_id('ri_diagnosis', $d);

            foreach ($this->input->post('penyakit') as $p) {
                $penyakit = [
                    'ri_diagnosis_id' => $id,
                    'penyakit_id' => $p
                ];
                $this->MainModel->insert('ri_diagnosis_penyakit', $penyakit);
            }
        }
        else if ($what == 'resep') {
            $ri_resep_id = $this->MainModel->insert_id('ri_resep', [
                'rawat_inap_id' => $this->input->post('rawat_inap_id'),
                'transfer_id' => $transfer_id,
                'jenis_ruangan_id' => $this->jenis_ruangan_id,
            ]);
            $jumlah_satuan = $this->input->post('jumlah_satuan');
            $signa_obat = $this->input->post('signa_obat');
            $input_obat = $this->input->post('nama_obat');
            $i = 0;
            foreach ($input_obat as $key => $value) {
                if ($value != "") {
                    $obat = array(
                        'ri_resep_id' => $ri_resep_id,
                        'obat_id' => $value,
                        'jumlah' => $jumlah_satuan[$i],
                        'signa' => $signa_obat[$i],
                    );
                    $this->MainModel->insert_id('ri_resep_obat', $obat);
                    $obat = $this->ObatModel->getObatById($value)->row();
                    $stok = array('stok_obat' => ($obat->stok_obat) - $jumlah_satuan[$i]);
                    $this->MainModel->update('obat', $stok, $value);
                }
                $i++;
            }

            for ($n = 1; $n <= 9; $n++) {
                $signa = $this->input->post('signa' . $n);
                $catatan = $this->input->post('catatan' . $n);
                $racikan = array(
                    'ri_resep_id' => $ri_resep_id,
                    'nama_racikan' => 'racikan ' . $n,
                    'signa' => $signa,
                    'catatan' => $catatan ?? '-',
                );
                $jumlah_satuan = $this->input->post('jumlah_satuan_racikan' . $n);

                $j = 0;
                $ri_resep_obat_racik_id = $this->MainModel->insert_id('ri_resep_obat_racik', $racikan);
                $input_obat = $this->input->post('nama_obat_racikan'.$n);

                foreach ($input_obat as $key => $value) {
                    if ($value != '') {
                        $obat_racikan = array(
                            'ri_resep_obat_racik_id' => $ri_resep_obat_racik_id,
                            'obat_id' => $value,
                            'jumlah' => $jumlah_satuan[$j],
                        );

                        $this->MainModel->insert('ri_resep_obat_racik_detail', $obat_racikan);
                        $obat = $this->ObatModel->getObatById($value)->row();
                        $stok = array('stok_obat' => ($obat->stok_obat) - $jumlah_satuan[$j]);
                        $this->MainModel->update('obat', $stok, $value);
                    }
                    $j++;
                }
            }
        }
        else if ($what == 'bahan_habis_pakai') {
            $ri_bhp_id = $this->MainModel->insert_id('ri_bahan_habis_pakai', [
                'rawat_inap_id' => $this->input->post('rawat_inap_id'),
                'transfer_id' => $transfer_id,
                'jenis_ruangan_id' => $this->jenis_ruangan_id,
            ]);
            $x = 0;
            $jumlah_bahan = $_POST['qty'];
            foreach ($_POST['id'] as $value) {
                $this->db->insert('ri_bahan_habis_pakai_detail', [
                    'ri_bahan_habis_pakai_id' => $ri_bhp_id,
                    'bahan_id' => $value,
                    'jumlah' => $jumlah_bahan[$x],
                ]);
                $bahan = $this->ObatModel->getBahanById($value)->row();
                $stok = array('jumlah' => ($bahan->jumlah) - $jumlah_bahan[$x]);
                $this->MainModel->update('bahan_habis_pakai', $stok, $value);
                $x++;
            }
        }
        else if ($what == 'biaya') {
            $jenis_biaya = $this->input->post('jenis_biaya');
            if ($jenis_biaya == '#biaya_1') {
                $biaya = [
                    'rawat_inap_id' => $this->input->post('rawat_inap_id'),
                    'transfer_id' => $transfer_id,
                    'jenis_ruangan_id' => $this->jenis_ruangan_id,
                    'jenis_biaya' => 'Biaya Kamar',
                    'biaya' => $this->input->post('harga_bed')
                ];
                $this->MainModel->insert('ri_biaya', $biaya);
            }
            else if ($jenis_biaya == '#biaya_2') {
                $biaya = [
                    'rawat_inap_id' => $this->input->post('rawat_inap_id'),
                    'transfer_id' => $transfer_id,
                    'jenis_ruangan_id' => $this->jenis_ruangan_id,
                    'jenis_biaya' => 'Makanan',
                    'makanan_id' => $this->input->post('makanan'),
                    'biaya' => $this->input->post('harga_makanan'),
                ];
                $this->MainModel->insert('ri_biaya', $biaya);
            }
            else if ($jenis_biaya == '#biaya_3') {
                $tindakan_id = $this->input->post('tindakan') ?: [];
                $tindakan_anestesi_id = $this->input->post('tindakan_anestesi') ?: [];
                $tindakan_tamu_id = $this->input->post('tindakan_tamu') ?: [];

                $dokter_anestesi = $this->input->post('dokter_anestesi');
                $dokter_tamu = $this->input->post('dokter_tamu') ?: [];
                $perawat = $this->input->post('perawat') ?: [];
                $perawat_tamu = $this->input->post('perawat_tamu') ?: [];

                $harga = $this->RuangBersalinModel->getTindakanHargaByIds(array_merge($tindakan_id, $tindakan_anestesi_id, $tindakan_tamu_id));

                $total_harga = 0;
                foreach ($harga as $h) {
                    $total_harga += $h->tarif_pasien;
                }

                $biaya = [
                    'rawat_inap_id' => $this->input->post('rawat_inap_id'),
                    'transfer_id' => $transfer_id,
                    'jenis_ruangan_id' => $this->jenis_ruangan_id,
                    'jenis_biaya' => 'Tindakan',
                    'biaya' => $total_harga,
                ];
                $biaya_id = $this->MainModel->insert_id('ri_biaya', $biaya);

                foreach ($tindakan_id as $id) {
                    $tarif_perawat = $this->PemeriksaanModel->getTarifTindakanById($id)->tarif_perawat;
                    $tarif_per_perawat = $tarif_perawat / count($perawat);
                    $this->MainModel->insert('ri_biaya_tindakan', [
                        'ri_biaya_id' => $biaya_id,
                        'tindakan_id' => $id,
                        'jumlah_perawat' => count($perawat),
                        'perawat' => implode(',', $perawat) ?? '',
                        'tarif_per_perawat' => is_infinite($tarif_per_perawat) || is_nan($tarif_per_perawat) ? 0 : $tarif_per_perawat,
                        'tipe_tindakan' => 'asli',
                        'perawat_tamu' => implode(',', $perawat_tamu) ?? ''
                    ]);
                }

                foreach ($tindakan_anestesi_id as $id) {
                    $this->MainModel->insert('ri_biaya_tindakan', [
                        'ri_biaya_id' => $biaya_id,
                        'tindakan_id' => $id,
                        'dokter_anestesi' => $dokter_anestesi,
                        'tipe_tindakan' => 'anestesi',
                        'perawat_tamu' => implode(',', $perawat_tamu) ?? ''
                    ]);
                }

                foreach ($tindakan_tamu_id as $id) {
                    $tarif_dokter = $this->PemeriksaanModel->getTarifTindakanById($id)->tarif_dokter;
                    $tarif_per_dokter_tamu = $tarif_dokter / count($dokter_tamu);
                    $this->MainModel->insert('ri_biaya_tindakan', [
                        'ri_biaya_id' => $biaya_id,
                        'tindakan_id' => $id,
                        'jumlah_dokter_tamu' => count($dokter_tamu),
                        'dokter_tamu' => implode(',', $dokter_tamu) ?? '',
                        'tarif_per_dokter_tamu' => is_infinite($tarif_per_dokter_tamu) || is_nan($tarif_per_dokter_tamu) ? 0 : $tarif_per_dokter_tamu,
                        'tipe_tindakan' => 'tamu',
                        'perawat_tamu' => implode(',', $perawat_tamu) ?? ''
                    ]);
                }
            }
        }

        redirect('RuangBersalin/detail/'.$this->input->post('rawat_inap_id').'/'.$transfer_id.'/'.$what);
    }

    public function hapus($what, $rawat_inap_id, $id)
    {
        if ($what == 'pemeriksaan') {
            $this->db->where('id', $id)->delete('ri_pemeriksaan');
        }
        else if ($what == 'diagnosis') {
            $this->db->where('id', $id)->delete('ri_diagnosis');
            $this->db->where('ri_diagnosis_id', $id)->delete('ri_diagnosis_penyakit');
        }
        else if ($what == 'resep') {
            //
        }
        else if ($what == 'bahan_habis_pakai') {
            $ri_bahan_details = $this->db->where('ri_bahan_habis_pakai_id', $id)
                ->get('ri_bahan_habis_pakai_detail')
                ->result();

            foreach ($ri_bahan_details as $d) {
                $bahan = $this->ObatModel->getBahanById($d->bahan_id)->row();
                $stok = ['jumlah' => ($bahan->jumlah) + $d->jumlah];
                $this->MainModel->update('bahan_habis_pakai', $stok, $d->bahan_id);
            }

            $this->db->where('id', $id)->delete('ri_bahan_habis_pakai');
            $this->db->where('ri_bahan_habis_pakai_id', $id)->delete('ri_bahan_habis_pakai_detail');
        }
        else if ($what == 'biaya') {
            if ($this->db->where('id', $id)->get('ri_biaya')->row()->jenis_biaya == 'Tindakan') {
                $this->db->where('ri_biaya_id', $id)->delete('ri_biaya_tindakan');
            }
            $this->db->where('id', $id)->delete('ri_biaya');
        }

        $transfer_id = $this->RuangBersalinModel->getById($rawat_inap_id)->transfer_id;
        redirect('RuangBersalin/detail/'.$rawat_inap_id.'/'.$transfer_id.'/'.$what);
    }

    public function getMakananByTipeId($id)
    {
        echo json_encode($this->RuangBersalinModel->getMakananByTipeId($id));
    }

    public function printPembayaran($bayar_id)
    {
        $id = $this->RuangBersalinModel->getBayarById($bayar_id)->rawat_inap_id;
        $data['rawat_inap'] = $this->RuangBersalinModel->getById($id);

        $transfer_id = $data['rawat_inap']->transfer_id;
        $data['klinik'] = $this->AdministrasiModel->getKlinik()->row();
        $data['bayar'] = $this->RuangBersalinModel->getBayarByTransferId($transfer_id, $this->jenis_ruangan_id);
        $data['sudah_bayar'] = true;
        $data['pasien'] = $this->RuangBersalinModel->getPasienById($data['rawat_inap']->pasien_id);
        $data['dokter'] = $this->PendaftaranModel->getDokter();
        $data['penyakit'] = $this->RuangBersalinModel->getAllPenyakit();
        $data['tindakan'] = $this->RuangBersalinModel->getAllTindakan();
        $data['tipe_makanan'] = $this->RuangBersalinModel->getAllTipeMakanan();
        $data['obat1'] = $this->db->get_where('obat', ['is_active' => 1, 'stok_obat >' => 0])->result();
        $data['bahan'] = $this->db->get_where('bahan_habis_pakai', ['is_active' => 1, 'jumlah >' => 0])->result();

        $this->getDetailBilling($data, true);
        $this->load->view('ruang_bersalin/nota_bayar', $data);
    }
}
