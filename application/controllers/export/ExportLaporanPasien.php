<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH.'controllers/export/BaseExportExcel.php';

class ExportLaporanPasien extends BaseExportExcel
{
    public function __construct() {
        parent::__construct();

        $this->load->library('template');
        $this->load->Model('ObatGlobalModel');
        $this->load->Model('LaporanModel');
        $this->load->Model('PendaftaranModel');
        $this->load->Model('JenisLayananLaboratoirumModel');
    }

    public function jumlahKunjunganTop()
    {
        $start_date         = ($this->input->post('from'))?$this->input->post('from'):date('Y-m-d');
        $end_date           = ($this->input->post('to'))?$this->input->post('to'):date('Y-m-d');
        $jenis_pendaftaran  = $this->input->post('jenis_pendaftaran');

        $jumlah_kunjungan = $this->LaporanModel->getKunjunganPasien($start_date,$end_date)->result();

        $this->row(['Laporan Kunjungan Pasien']);
        if ($jenis_pendaftaran) {
            $j = $this->PendaftaranModel->getJenisPendaftaranById($jenis_pendaftaran)->row()->nama;
            $this->row(["($j)"]);
        }
        if ($start_date && $end_date) {
            $this->row(["($start_date - $end_date)"]);
        }
        $this->row();
        $this->row(['No', 'Keterangan', 'Jumlah']);
        foreach ($jumlah_kunjungan as $k => $row) {
            if ($row->id == $jenis_pendaftaran || $jenis_pendaftaran == '') {
                $this->row([
                    $k + 1,
                    ucwords($row->nama),
                    ucwords($row->jumlah),
                ]);
            }
        }
        $this->printExcel('laporan_jumlah_kunjungan_pasien');
    }

    public function jumlahKunjunganBottom()
    {
        $start_date         = ($this->input->post('from'))?$this->input->post('from'):date('Y-m-d');
        $end_date           = ($this->input->post('to'))?$this->input->post('to'):date('Y-m-d');
        $jenis_pendaftaran  = $this->input->post('jenis_pendaftaran');

        $ListPendaftaran = $this->LaporanModel->getSimpleListPendaftaran($start_date,$end_date,$jenis_pendaftaran)->result();
        foreach ($ListPendaftaran as &$d) {
            if ($d->type == 'Rajal') {
                $d->penyakit = $this->LaporanModel->getSimplePenyakitPemeriksaanByIdPemeriksaan($d->pemeriksaan_id)->result();
            }
            else if ($d->type == 'Ranap') {
                $d->penyakit = $this->LaporanModel->getSimplePenyakitPemeriksaanRiByIdPemeriksaan($d->pemeriksaan_id)->result();
            }
            else {
                $d->penyakit = $this->LaporanModel->getSimplePenyakitPemeriksaanRiByIdPemeriksaan($d->pemeriksaan_id, false)->result();
            }

            if ($d->poli == 'Laboratorium') {
                $l = [];
                foreach ($this->JenisLayananLaboratoirumModel->byIdPemeriksaan($d->pemeriksaan_id) as $v) {
                    $l []= $v->nama_layanan;
                }
                $d->layanan_lab = join(', ', $l);
            }
            else {
                $d->layanan_lab = '';
            }
        }

        $this->row(['Laporan Kunjungan Pasien']);
        if ($jenis_pendaftaran) {
            $j = $this->PendaftaranModel->getJenisPendaftaranById($jenis_pendaftaran)->row()->nama;
            $this->row(["($j)"]);
        }
        if ($start_date && $end_date) {
            $this->row(["($start_date - $end_date)"]);
        }
        $this->row();
        $col = ['No', 'Waktu Kunjungan', 'Nama Pasien', 'Jenis Pendaftaran', 'Tipe Pasien', 'No RM', 'Usia', 'Jenis Kelamin', 'Alamat', 'Nama Dokter', 'Layanan Lab', 'Diagnosis Jenis Penyakit', 'Diagnosis'];
        if ($jenis_pendaftaran == 34)
            $col[]= 'HPHT';
        $this->row($col);
        foreach ($ListPendaftaran as $k => $row) {
            $penyakit = '';
            if ($row->penyakit) {
                foreach ($row->penyakit as $k1 => $row1) {
                    $penyakit .= $row1->kode.' - '.$row1->nama;
                    if ($k1 < sizeof($row->penyakit) - 1) {
                        $penyakit .= ', ';
                    }
                }
            }

            $data = [
                $k + 1,
                $this->DateIndo($row->waktu_pendaftaran),
                ucwords($row->nama_pasien),
                ucwords($row->poli),
                ucwords($row->jaminan),
                ucwords($row->no_rm),
                ucwords($row->usia),
                $row->jk == 'L' ? 'Laki-Laki' : 'Perempuan',
                ucwords($row->alamat),
                ucwords($row->nama_dokter),
                $row->layanan_lab,
                $penyakit,
                $row->diagnosis
            ];
            if ($jenis_pendaftaran == 34)
                $data[]= unserialize($row->meta)['hpht'];
            $this->row($data);
        }
        $this->printExcel('laporan_kunjungan_pasien');
    }

    public function jumlahPasienBottom()
    {
        $start_date         = ($this->input->post('from'))?$this->input->post('from'):date('Y-m-d');
        $end_date           = ($this->input->post('to'))?$this->input->post('to'):date('Y-m-d');
        $jenis_pendaftaran  = $this->input->post('jenis_pendaftaran');

        $data['listjenispendaftaran'] = $this->LaporanModel->getJenisPendaftaran();
        $data['list_jumlah_pasien'] = $this->LaporanModel->listJumlahPasien($start_date,$end_date,$jenis_pendaftaran)->result();

        foreach ($data['list_jumlah_pasien'] as &$d) {
            if ($d->type == 'Rajal') {
                $d->penyakit = $this->LaporanModel->getSimplePenyakitPemeriksaanByIdPemeriksaan($d->pemeriksaan_id)->result();
            }
            else if ($d->type == 'Ranap') {
                $d->penyakit = $this->LaporanModel->getSimplePenyakitPemeriksaanRiByIdPemeriksaan($d->pemeriksaan_id)->result();
            }
            else {
                $d->penyakit = $this->LaporanModel->getSimplePenyakitPemeriksaanRiByIdPemeriksaan($d->pemeriksaan_id, false)->result();
            }
        }

        $this->row(['Laporan Jumlah Pasien']);
        if ($jenis_pendaftaran) {
            $j = $this->PendaftaranModel->getJenisPendaftaranById($jenis_pendaftaran)->row()->nama;
            $this->row(["($j)"]);
        }
        if ($start_date && $end_date) {
            $this->row(["($start_date - $end_date)"]);
        }
        $this->row();
        $this->row(['No', 'Waktu Kunjungan', 'Nama Pasien', 'Jenis Pendaftaran', 'Tipe Pasien', 'No RM', 'Usia', 'Jenis Kelamin', 'Alamat', 'Nama Dokter', 'Diagnosis Jenis Penyakit']);

        foreach ($data['list_jumlah_pasien'] as $k => $row) {
            $penyakit = '';
            if ($row->penyakit) {
                foreach ($row->penyakit as $k1 => $row1) {
                    $penyakit .= $row1->kode.' - '.$row1->nama;
                    if ($k1 < sizeof($row->penyakit) - 1) {
                        $penyakit .= ', ';
                    }
                }
            }

            $this->row([
                $k + 1,
                $this->DateIndo($row->waktu_pendaftaran),
                ucwords($row->nama),
                ucwords($row->poli),
                ucwords($row->jaminan),
                ucwords($row->no_rm),
                ucwords($row->usia),
                $row->jk == 'L' ? 'Laki-Laki' : 'Perempuan',
                ucwords($row->alamat),
                ucwords($row->nama_dokter),
                $penyakit
            ]);
        }

        $this->printExcel('laporan_jumlah_pasien');
    }

    public function jumlahPasienBaruBottom()
    {
        $start_date         = ($this->input->post('from'))?$this->input->post('from'):date('Y-m-d');
        $end_date           = ($this->input->post('to'))?$this->input->post('to'):date('Y-m-d');
        $jenis_pendaftaran  = $this->input->post('jenis_pendaftaran');

        $data['listjenispendaftaran'] = $this->LaporanModel->getJenisPendaftaran();
        $data['list_jumlah_pasien'] = $this->LaporanModel->listJumlahPasienBaru($start_date,$end_date,$jenis_pendaftaran)->result();

        foreach ($data['list_jumlah_pasien'] as &$d) {
            if ($d->type == 'Rajal') {
                $d->penyakit = $this->LaporanModel->getSimplePenyakitPemeriksaanByIdPemeriksaan($d->pemeriksaan_id)->result();
            }
            else if ($d->type == 'Ranap') {
                $d->penyakit = $this->LaporanModel->getSimplePenyakitPemeriksaanRiByIdPemeriksaan($d->pemeriksaan_id)->result();
            }
            else {
                $d->penyakit = $this->LaporanModel->getSimplePenyakitPemeriksaanRiByIdPemeriksaan($d->pemeriksaan_id, false)->result();
            }
        }

        $this->row(['Laporan Jumlah Pasien Baru']);
        if ($jenis_pendaftaran) {
            $j = $this->PendaftaranModel->getJenisPendaftaranById($jenis_pendaftaran)->row()->nama;
            $this->row(["($j)"]);
        }
        if ($start_date && $end_date) {
            $this->row(["($start_date - $end_date)"]);
        }
        $this->row();
        $this->row(['No', 'Waktu Kunjungan', 'Nama Pasien', 'Jenis Pendaftaran', 'Tipe Pasien', 'No RM', 'Usia', 'Jenis Kelamin', 'Alamat', 'Nama Dokter', 'Diagnosis Jenis Penyakit']);

        foreach ($data['list_jumlah_pasien'] as $k => $row) {
            $penyakit = '';
            if ($row->penyakit) {
                foreach ($row->penyakit as $k1 => $row1) {
                    $penyakit .= $row1->kode.' - '.$row1->nama;
                    if ($k1 < sizeof($row->penyakit) - 1) {
                        $penyakit .= ', ';
                    }
                }
            }

            $this->row([
                $k + 1,
                $this->DateIndo($row->waktu_pendaftaran),
                ucwords($row->nama_pasien),
                ucwords($row->poli),
                ucwords($row->jaminan),
                ucwords($row->no_rm),
                ucwords($row->usia),
                $row->jk == 'L' ? 'Laki-Laki' : 'Perempuan',
                ucwords($row->alamat),
                ucwords($row->nama_dokter),
                $penyakit
            ]);
        }

        $this->printExcel('laporan_jumlah_pasien_baru');
    }

    public function jumlahPasienTop20Bottom()
    {
        $start_date         = ($this->input->post('from'))?$this->input->post('from'):date('Y-m-d');
        $end_date           = ($this->input->post('to'))?$this->input->post('to'):date('Y-m-d');
        $jenis_pendaftaran  = $this->input->post('jenis_pendaftaran');

        $data['list_jumlah_pasien20'] = $this->LaporanModel->listJumlahPasien20($start_date,$end_date,$jenis_pendaftaran)->result();

        $this->row(['Laporan Pasien Top 20']);
        if ($jenis_pendaftaran) {
            $j = $this->PendaftaranModel->getJenisPendaftaranById($jenis_pendaftaran)->row()->nama;
            $this->row(["($j)"]);
        }
        if ($start_date && $end_date) {
            $this->row(["($start_date - $end_date)"]);
        }
        $this->row();
        $this->row(['No', 'Nama Pasien', 'Jenis Pendaftaran', 'Tipe Pasien', 'No RM', 'Usia', 'Jenis Kelamin', 'Alamat', 'Total Kunjungan']);

        foreach ($data['list_jumlah_pasien20'] as $k => $row) {
            $this->row([
                $k + 1,
                ucwords($row->nama),
                ucwords($row->poli),
                ucwords($row->jaminan),
                ucwords($row->no_rm),
                ucwords($row->usia),
                $row->jk == 'L' ? 'Laki-Laki' : 'Perempuan',
                ucwords($row->alamat),
                $row->total
            ]);
        }

        $this->printExcel('laporan_pasien_top_20');
    }

    public function laporanPenyakitTop()
    {
        $start_date         = ($this->input->post('from'))?$this->input->post('from'):date('Y-m-d');
        $end_date           = ($this->input->post('to'))?$this->input->post('to'):date('Y-m-d');
        $jenis_pendaftaran  = $this->input->post('jenis_pendaftaran');

        $list = $this->LaporanModel->getPenyakitPemeriksaan($start_date, $end_date, $jenis_pendaftaran)->result();

        $this->row(['Laporan Penyakit']);
        if ($jenis_pendaftaran) {
            $j = $this->PendaftaranModel->getJenisPendaftaranById($jenis_pendaftaran)->row()->nama;
            $this->row(["($j)"]);
        }
        if ($start_date && $end_date) {
            $this->row(["($start_date - $end_date)"]);
        }
        $this->row();
        $this->row(['No', 'Nama Penyakit', 'Jumlah']);

        foreach ($list as $k => $row) {
            $this->row([
                $k + 1,
                ucwords($row->nama),
                $row->jumlah
            ]);
        }

        $this->printExcel('laporan_penyakit');
    }
}
