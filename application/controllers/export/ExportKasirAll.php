<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH.'controllers/export/BaseExportExcel.php';

class ExportKasirAll extends BaseExportExcel
{
    public function __construct()
    {
        parent::__construct();
        $this->load->Model('KasirModel');
        $this->load->Model('PasienModel');
        $this->load->Model('PemeriksaanModel');
    }

    public function export()
    {
        $jenis = $this->input->get('jenis');
        $from = $this->input->get('from');
        $to = $this->input->get('to');
        $bulan = $this->input->get('bulan');
        $tahun = $this->input->get('tahun');
        $by = $this->input->get('by') ?? 'waktu';
        $id_pasien = $this->input->get('id_pasien');

        $start_date = $end_date = date('Y-m-d');
        if ($jenis == 2) {
            $start_date = $from;
            $end_date = $to;
        }
        if ($jenis == 3) {
            $start_date = date("$tahun-$bulan-01");
            $end_date = date("$tahun-$bulan-t");
        }
        $periode = ['start' => $start_date, 'end' => $end_date];

        $data['jenis'] = $jenis;
        $pasien = $id_pasien ? $this->PasienModel->getPasienById($id_pasien)->row() : '';
        $data['listPemeriksaan'] = $this->KasirModel->getListPemeriksaanForRekapKasirAllComplete($by == 'waktu' ? 'waktu' : $id_pasien, $periode);

        $this->row(['Rekapitulasi Kasir All'], true);
        if ($by == 'waktu')
            $this->row(['Periode: ' . date('d-F-Y', strtotime($start_date) . ' sd ' . date('d-F-Y', strtotime($end_date)))]);
        else
            $this->row(['pasien: ' . $pasien->nama]);
        $this->row();
        $this->row(['No', 'Waktu Bayar', 'Pasien', 'Jenis Pendaftaran', '', 'Jasa Medis', 'Detail Jasa Medis', 'Obat', 'Detail Obat', 'Obat Racikan', 'BHP', 'Detail BHP', 'Jasa Racik', 'Diskon', 'Total Bayar'], true, '02A65A', 'FFFFFF');

        foreach ($data['listPemeriksaan'] as $k => $row) {
            $row->bayar = json_decode($row->bayar);

            foreach ($row->bayar as $bayar_index => $bayar) {
                $first = $bayar_index == 0;

                $bayar->detail_jasa_medis = json_decode($bayar->detail_jasa_medis);
                $bayar->detail_obat = json_decode($bayar->detail_obat);
                $bayar->detail_obat_racik = json_decode($bayar->detail_obat_racik);
                $bayar->detail_bhp = json_decode($bayar->detail_bhp);
                $max = max(count($bayar->detail_jasa_medis), count($bayar->detail_obat), count($bayar->detail_obat_racik), count($bayar->detail_bhp));

                for ($i = 0; $i < $max; $i++) {
                    $first_ = $i == 0;

                    $this->row([
                        $first && $first_ ? $k + 1 : '',
                        $first && $first_ ? date('d-M-Y H:i', strtotime($row->created_at)) : '',
                        $first_ ? ucwords($row->bayar[0]->nama_pasien) . ' - ' . $row->bayar[0]->no_rm : '',
                        $first_ ? $bayar->nama_poli : '',
                        $first_ ? ucwords($bayar->jaminan) : '',
                        $first_ ? 'Rp ' . number_format($bayar->jasa_medis, 2, ',', '.') : '',
                        isset($bayar->detail_jasa_medis[$i]) ? $bayar->detail_jasa_medis[$i]->item : '',
                        $first_ ? 'Rp ' . number_format($bayar->obat, 2, ',', '.') : '',
                        isset($bayar->detail_obat[$i]) ? $bayar->detail_obat[$i]->item . ' x ' . $bayar->detail_obat[$i]->jumlah : '',
                        $first_ ? 'Rp ' . number_format($bayar->obat_racik, 2, ',', '.') : '',
                        $first_ ? 'Rp ' . number_format($bayar->bhp, 2, ',', '.') : '',
                        isset($bayar->detail_bhp[$i]) ? $bayar->detail_bhp[$i]->item . ' x ' . $bayar->detail_bhp[$i]->jumlah : '',
                        $first && $first_ ? 'Rp ' . number_format($row->jasa_racik, 2, ',', '.') : '',
                        $first && $first_ ? 'Rp ' . number_format($row->diskon, 2, ',', '.') : '',
                        $first && $first_ ? 'Rp ' . number_format($row->total, 2, ',', '.') : '',
                    ]);
                }
            }
        }

        $this->printExcel('rekap_kasir_all');
    }
}