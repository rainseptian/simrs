<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ambulance extends Admin_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->load->library('Customlib');
        $this->load->library('template');
        $this->load->model('PanggilanAmbulanceModel');
        $this->load->model('TarifAmbulanceModel');
        $this->load->model('MainModel');
        $this->load->model('AdministrasiModel');
        $this->load->helper(['usia']);
    }

    public function index()
    {
        $data['title'] = 'Add Vehicle';
        $listVehicle = $this->vehicle_model->get();
        $data['listVehicle'] = $listVehicle;

        $this->template->view('ambulance/list', $data);
    }

    public function add() {
        $this->form_validation->set_rules('vehicle_no', $this->lang->line('vehicle_no'), 'required');
        $this->form_validation->set_rules('vehicle_model', $this->lang->line('vehicle_model'), 'required');
        $this->form_validation->set_rules('vehicle_type', $this->lang->line('vehicle') . " " . $this->lang->line('type'), 'required');

        if ($this->form_validation->run() == FALSE) {
            $msg = array(
                'vehicle_no' => form_error('vehicle_no'),
                'vehicle_model' => form_error('vehicle_model'),
                'vehicle_type' => form_error('vehicle_type'),
            );
            $array = array('status' => 'fail', 'error' => $msg, 'message' => '');
        } else {
            $manufacture_year = $this->input->post('manufacture_year');
            $data = array(
                'vehicle_no' => $this->input->post('vehicle_no'),
                'vehicle_model' => $this->input->post('vehicle_model'),
                'driver_name' => $this->input->post('driver_name'),
                'driver_licence' => $this->input->post('driver_licence'),
                'driver_contact' => $this->input->post('driver_contact'),
                'vehicle_type' => $this->input->post('vehicle_type'),
                'note' => $this->input->post('note'),
            );
            ($manufacture_year != "") ? $data['manufacture_year'] = $manufacture_year : '';
            $this->vehicle_model->add($data);
            $array = array('status' => 'success', 'error' => '', 'message' => $this->lang->line('success_message'));
        }
        echo json_encode($array);
    }

    public function edit() {
        $id = $this->input->post("id");
        $listVehicle = $this->vehicle_model->getDetails($id);
        echo json_encode($listVehicle);
    }

    public function update() {
        $this->form_validation->set_rules('vehicle_no', 'Vehicle No', 'required');
        $this->form_validation->set_rules('vehicle_model', $this->lang->line('vehicle_model'), 'required');
        $this->form_validation->set_rules('vehicle_type', $this->lang->line('vehicle') . " " . $this->lang->line('type'), 'required');
        if ($this->form_validation->run() == FALSE) {
            $msg = array(
                'vehicle_no' => form_error('vehicle_no'),
                'vehicle_model' => form_error('vehicle_model'),
                'vehicle_type' => form_error('vehicle_type'),
            );
            $array = array('status' => 'fail', 'error' => $msg, 'message' => '');
        } else {
            $id = $this->input->post('id');
            $manufacture_year = $this->input->post('manufacture_year');
            $data = array(
                'id' => $id,
                'vehicle_no' => $this->input->post('vehicle_no'),
                'vehicle_model' => $this->input->post('vehicle_model'),
                'driver_name' => $this->input->post('driver_name'),
                'driver_licence' => $this->input->post('driver_licence'),
                'driver_contact' => $this->input->post('driver_contact'),
                'vehicle_type' => $this->input->post('vehicle_type'),
                'note' => $this->input->post('note'),
            );
            ($manufacture_year != "") ? $data['manufacture_year'] = $manufacture_year : '';
            $this->vehicle_model->add($data);
            $array = array('status' => 'success', 'error' => '', 'message' => 'Record Saved Successfully.');
        }
        echo json_encode($array);
    }

    function delete($id)
    {
        $this->vehicle_model->remove($id);
        redirect('Ambulance');
    }

    public function getCallAmbulance()
    {
        $data['vehiclelist'] = $this->vehicle_model->get();
        $data['list'] = $this->PanggilanAmbulanceModel->getBaru();
        $data['tarif_ambulance'] = $this->TarifAmbulanceModel->getAll();
        $data['jaminan'] = $this->config->item('pendaftaran');

        $this->template->view('ambulance/call_list', $data);
    }

    public function rekapAmbulance()
    {
        $jenis = $this->input->get('jenis');
        $from = $this->input->get('from');
        $to = $this->input->get('to');
        $bulan = $this->input->get('bulan');
        $tahun = $this->input->get('tahun');

        $data['jenis'] = $jenis;
        $data['from'] = $from;
        $data['to'] = $to;
        $data['bulan'] = $bulan;
        $data['tahun'] = $tahun;

        $start_date = $end_date = date('Y-m-d');
        if ($jenis == 2) {
            $start_date = $from;
            $end_date = $to;
        }
        if ($jenis == 3) {
            $start_date = date("$tahun-$bulan-01");
            $end_date = date("$tahun-$bulan-t");
        }
        $periode = ['start' => $start_date, 'end' => $end_date];

        $data['vehiclelist'] = $this->vehicle_model->get();
        $data['list'] = $this->PanggilanAmbulanceModel->getSelesai($periode);
        $data['tarif_ambulance'] = $this->TarifAmbulanceModel->getAll();

        $this->template->view('ambulance/rekap_list', $data);
    }

    public function addCallAmbulance()
    {
        $i = $this->MainModel->insert('panggilan_ambulance', [
            'pasien_id' => $this->input->post('pasien_id'),
            'pasien_baru' => $this->input->post('pasien_baru') == 'baru' ? 1 : 0,
            'nama_pasien' => $this->input->post('nama_pasien'),
            'alamat_pasien' => $this->input->post('alamat_pasien'),
            'no_telp_pasien' => $this->input->post('no_hp_pasien'),
            'vehicle_id' => $this->input->post('vehicle_id'),
            'driver_name' => $this->input->post('driver'),
            'date' => $this->input->post('date'),
            'tarif_ambulance_id' => $this->input->post('tarif_ambulance_id'),
            'jaminan' => $this->input->post('jaminan'),
            'no_jaminan' => $this->input->post('no_jaminan'),
        ]);

        $res = $i ? [
            'status' => 'success',
            'message' => 'Berhasil menambah panggilan ambulance',
        ] : [
            'status' => 'fail',
            'message' => 'Gagal menambah panggilan ambulance'
        ];

        echo json_encode($res);
    }

    public function addPasien()
    {
        $session = $this->session->userdata('logged_in');

        $pasien = array(
            'no_rm' => $this->input->post('no_rm'),
            'nama' => $this->input->post('nama'),
            'tanggal_lahir' => $this->input->post('tanggal_lahir') ?? '0000-00-00',
            'usia' => get_usia($this->input->post('tanggal_lahir')),
            'tempat_lahir' => $this->input->post('tempat_lahir') ?? '',
            'jk' => $this->input->post('jenis_kelamin'),
            'alamat' => $this->input->post('alamat'),
            'telepon' => $this->input->post('telepon'),
            'pekerjaan' => $this->input->post('pekerjaan') ?? '',
            'agama' => $this->input->post('agama') ?? '',
            'tingkat_pendidikan' => $this->input->post('tingkat_pendidikan') ?? '',
            'penanggungjawab' => $this->input->post('penanggungjawab') ?? '',
            'creator' => $session->id
        );

        $i = $this->MainModel->insert_id($tabel = 'pasien', $pasien);

        $res = $i ? [
            'status' => 'success',
            'message' => 'Berhasil menambah pasien',
            'pasien_id' => $i,
            'no_rm' => $this->input->post('no_rm'),
            'nama' => $this->input->post('nama'),
            'alamat' => $this->input->post('alamat'),
            'no_telp' => $this->input->post('no_hp'),
        ] : [
            'status' => 'fail',
            'message' => 'Gagal menambah pasien'
        ];

        echo json_encode($res);
    }

    public function bayarAmbulance()
    {
        $i = $this->MainModel->insert_id('bayar_ambulance', [
            'panggilan_id' => $this->input->post('panggilan_id'),
            'tarif' => $this->input->post('bayar_tarif'),
            'diskon' => $this->input->post('diskon'),
            'total' => $this->input->post('total'),
            'bayar' => $this->input->post('bayar'),
            'kembalian' => $this->input->post('kembalian'),
        ]);

        $res = $i ? [
            'bayar_id' => $i,
            'status' => 'success',
            'message' => 'Pembayaran berhasil'
        ] : [
            'status' => 'fail',
            'message' => 'Pembayaran gagal'
        ];

        if ($i) {
            $this->MainModel->update('panggilan_ambulance', ['status' => 'selesai'], $this->input->post('panggilan_id'));
            $this->session->set_flashdata('success', 'Pembayaran berhasil');
        }

        echo json_encode($res);
    }

    public function printPembayaranAmbulance($bayar_id)
    {
        $data['klinik'] = $this->AdministrasiModel->getKlinik()->row();
        $data['bayar'] = $this->PanggilanAmbulanceModel->getBayarByBayarId($bayar_id);
        $this->load->view('ambulance/print', $data);
    }
}
