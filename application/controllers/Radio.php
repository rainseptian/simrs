<?php


class Radio extends Admin_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->marital_status = $this->config->item('marital_status');
        $this->payment_mode = $this->config->item('payment_mode');
        $this->blood_group = $this->config->item('bloodgroup');

        $this->load->model('MainModel');
        $this->load->model('PemeriksaanModel');
        $this->load->model('DetailPenyakitPemeriksaanModel');
        $this->load->model('Radio_model');
        $this->load->model('Lab_model');
        $this->load->model('PerawatModel');
        $this->load->model('UserModel');
        $this->load->model('LaporanModel');
        $this->load->model('AdministrasiModel');

        $this->load->library('Customlib');
        $this->load->library('template');

        $this->load->helper(array('file', 'php_with_mpdf_helper'));
        $this->load->helper(array('file', 'mpdf'));
    }

    public function index()
    {
        redirect('Radio/listRadio');
    }

    public function search()
    {
        $r = $this->db->select('pem.*, p.nama nama_pasien, p.alamat, p.jk, p.usia, u.nama nama_dokter, jp.nama as jenis_pendaftaran, pp.pendaftaran_id')
            ->select('a.id as antrian_id, a.kode_antrian, a.is_mobile_jkn, a.is_check_in, afo.mulai_layan_poli_at, afo.id as afo_id')
            ->from('pemeriksaan pem')
            ->join('pendaftaran_pasien pp', 'pp.id = pem.pendaftaran_id')
            ->join('jenis_pendaftaran jp', 'jp.id = pp.jenis_pendaftaran_id')
            ->join('pasien p', 'p.id = pem.pasien_id and p.is_active = 1')
            ->join('user u', 'u.id = pem.dokter_id and u.is_active = 1', 'left')
            ->join('antrian a', 'a.pendaftaran_id = pp.id', 'left')
            ->join('antrian_front_office afo', 'afo.pendaftaran_id = pp.id', 'left')
            ->where('pem.is_active', '1')
            ->where('(pem.status', "'sudah_periksa_awal'", FALSE)
            ->or_where("pem.status = 'antri')", null, FALSE)
            ->where('pp.jenis_pendaftaran_id', 59)
            ->order_by('pem.waktu_pemeriksaan', 'asc')
            ->get();

        $data['listPendaftaran'] = $r->result();
        $data['jaminan'] = $this->config->item('pendaftaran');
        $data['charge_category'] = $this->Radio_model->getChargeCategory();
        $data['tindakan'] = $this->PemeriksaanModel->getTindakanByCategory('rad');
        $data['listStaff'] = $this->UserModel->listStaffRadiologi();

        foreach ($data['listPendaftaran'] as &$datum) {
            $datum->tindakan = $this->db->query('SELECT dtp.*, t.nama FROM detail_tindakan_pemeriksaan dtp JOIN tarif_tindakan t ON t.id = dtp.tarif_tindakan_id WHERE dtp.pemeriksaan_id = '.$datum->id)->result();
        }

        $this->template->view('radio/list', $data);
    }

    public function input_layanan($id)
    {
        if ($this->input->post()) {
            $session = $this->session->userdata('logged_in');
            $this->MainModel->hardDeleteWhere('detail_tindakan_pemeriksaan', ['pemeriksaan_id', $id]);
            $input_tindakan = $this->input->post('tindakan');
            foreach ($input_tindakan as $key => $value) {
                $tindakan = array(
                    'pemeriksaan_id' => $id,
                    'tarif_tindakan_id' => $value,
                    'jumlah_perawat' => 0,
                    'perawat' => '',
                    'tarif_per_perawat' => 0,
                    'creator' => $session->id
                );
                $this->MainModel->insert_id('detail_tindakan_pemeriksaan', $tindakan);
            }
//            $this->db->where('id')

            $this->session->set_flashdata('success', 'Berhasil input layanan!');
            redirect('radio/search');
        }
        else {
            $data['pendaftaran'] = $this->PemeriksaanModel->getPendaftaranByIdPemeriksaan($id)->row_array();
            $data['pemeriksaan'] = $this->PemeriksaanModel->getPemeriksaanById($id)->row_array();

            $category = 'umum';
            foreach ($this->config->item('poli') as $k => $v) {
                if (in_array($data['pendaftaran']['kode_daftar'], $v['kode'])) {
                    $category = $k;
                    break;
                }
            }

            $data['s_tindakan'] = $this->LaporanModel->getTindakanByIdPemeriksaan($id)->result();
            $data['tindakan'] = $this->PemeriksaanModel->getTindakanByCategory($category);

            $this->template->view('radio/input_layanan', $data);
        }
    }

    public function input_pemeriksaan($id)
    {
        if ($this->input->post()) {
            $session = $this->session->userdata('logged_in');
            $this->MainModel->update('pemeriksaan', [
                'meta' => serialize($this->input->post('meta')),
                'status' => 'sudah_periksa',
                'sudah_obat' => true
            ], $id);

            $this->MainModel->hardDeleteWhere('detail_tindakan_pemeriksaan', ['pemeriksaan_id', $id]);
            $input_tindakan = $this->input->post('tindakan');
            foreach ($input_tindakan as $key => $value) {
                $tindakan = array(
                    'pemeriksaan_id' => $id,
                    'tarif_tindakan_id' => $value,
                    'jumlah_perawat' => 0,
                    'perawat' => '',
                    'tarif_per_perawat' => 0,
                    'creator' => $session->id
                );
                $this->MainModel->insert_id('detail_tindakan_pemeriksaan', $tindakan);
            }

            $this->session->set_flashdata('success', 'Berhasil input layanan!');
            redirect('radio/search');
        }
        else {
            $data['pendaftaran'] = $this->PemeriksaanModel->getPendaftaranByIdPemeriksaan($id)->row_array();
            $data['pemeriksaan'] = $this->PemeriksaanModel->getPemeriksaanById($id)->row_array();
            $data['pasien'] = $this->db->query("SELECT * FROM pasien WHERE id = {$data['pemeriksaan']['pasien_id']}")->row();

            $category = 'umum';
            foreach ($this->config->item('poli') as $k => $v) {
                if (in_array($data['pendaftaran']['kode_daftar'], $v['kode'])) {
                    $category = $k;
                    break;
                }
            }

            $data['s_tindakan'] = $this->LaporanModel->getTindakanByIdPemeriksaan($id)->result();
            $data['tindakan'] = $this->PemeriksaanModel->getTindakanByCategory($category);
            $data['images'] = $this->db->query("SELECT * FROM pemeriksaan_image WHERE pemeriksaan_id = $id ORDER BY id")->result();

            $this->template->view('radio/input_layanan', $data);
        }
    }

    public function detail($id)
    {
        $data['pendaftaran'] = $this->PemeriksaanModel->getPendaftaranByIdPemeriksaan($id)->row_array();
        $data['pemeriksaan'] = $this->PemeriksaanModel->getPemeriksaanById($id)->row_array();
        $data['pasien'] = $this->db->query("SELECT * FROM pasien WHERE id = {$data['pemeriksaan']['pasien_id']}")->row();

        $category = 'umum';
        foreach ($this->config->item('poli') as $k => $v) {
            if (in_array($data['pendaftaran']['kode_daftar'], $v['kode'])) {
                $category = $k;
                break;
            }
        }

        $data['s_tindakan'] = $this->LaporanModel->getTindakanByIdPemeriksaan($id)->result();
        $data['tindakan'] = $this->PemeriksaanModel->getTindakanByCategory($category);
        $data['images'] = $this->db->query("SELECT * FROM pemeriksaan_image WHERE pemeriksaan_id = $id ORDER BY id")->result();
        $data['is_detail'] = 1;
        $data['meta'] = unserialize($data['pemeriksaan']['meta']);

        $this->template->view('radio/detail', $data);
    }

    private function upload($f_name)
    {
        $s = explode('.', $_FILES[$f_name]['name']);
        $ext = $s[count($s) - 1];
        $final_name = $this->input->post('pemeriksaan_id') . '_' . $f_name . '.' . $ext;

        $config1['upload_path'] = FCPATH . 'assets/img/radiologi';
        $config1['file_name'] = $final_name;
        $config1['overwrite'] = TRUE;
        $config1['max_size'] = 2048000;
        $config1['allowed_types'] = '*';
        $this->upload->initialize($config1);

        if ($this->upload->do_upload($f_name)) {
            return $final_name;
        }
        else {
            $error = array('error' => $this->upload->display_errors());
            die(json_encode($error));
            return '';
        }
    }

    public function periksa()
    {
        $this->load->library('upload');

        try {
            $meta = $this->input->post('meta');

            for ($i = 0; $i < 4; $i++) {
                if ($_FILES['file_hasil_tes_'.$i]['tmp_name']) {
                    $meta['file_hasil_tes_'.$i] = $this->upload('file_hasil_tes_'.$i);
                }
                if ($_FILES['file_hasil_bacaan_'.$i]['tmp_name']) {
                    $meta['file_hasil_bacaan_'.$i] = $this->upload('file_hasil_bacaan_'.$i);
                }
            }

            $meta = serialize($meta);
            $id = $this->input->post('pemeriksaan_id');
            $session = $this->session->userdata('logged_in');
            $periksa = array(
                'meta' => $meta,
                'status' => 'sudah_periksa',
                'sudah_obat' => 1,
                'sudah_periksa_perawat' => 1,
                'sudah_periksa_dokter' => 1,
                'creator' => $session->id
            );

            $this->MainModel->update('pemeriksaan', $periksa, $id);
            $this->MainModel->hardDeleteWhere('detail_tindakan_pemeriksaan', ['pemeriksaan_id', $id]);

            $input_tindakan = $this->input->post('tindakan');
            $input_perawat = $this->input->post('perawat');
            foreach ($input_tindakan as $key => $value) {
                $tarif_perawat = $this->PemeriksaanModel->getTarifTindakanById($value)->tarif_perawat;
                $tindakan = array(
                    'pemeriksaan_id' => $id,
                    'tarif_tindakan_id' => $value,
                    'jumlah_perawat' => count($input_perawat),
                    'perawat' => implode(',', $input_perawat),
                    'tarif_per_perawat' => $tarif_perawat / count($input_perawat),
                    'creator' => $session->id
                );
                $this->MainModel->insert_id('detail_tindakan_pemeriksaan', $tindakan);
            }

            echo json_encode([
                'status' => 'success',
                'message' => 'Pemeriksaan berhasil'
            ]);
        }
        catch (Exception $e) {

            echo json_encode([
                'status' => 'error',
                'message' => $e->getMessage()
            ]);
        }
    }

    public function sudahPeriksa()
    {
        $data['listPemeriksaan'] = $this->PemeriksaanModel->getListPemeriksaanSudahPeriksaByIdJenisPendaftaran(59)->result();
        $data['jaminan'] = $this->config->item('pendaftaran');

        foreach ($data['listPemeriksaan'] as &$datum) {
            $datum->tindakan = $this->db->query('SELECT dtp.*, t.nama FROM detail_tindakan_pemeriksaan dtp JOIN tarif_tindakan t ON t.id = dtp.tarif_tindakan_id WHERE dtp.pemeriksaan_id = '.$datum->id)->result();
        }

        $this->template->view('radio/list_sudah_selesai', $data);
    }

    // --------

    public function listRadio()
    {
        $data['listRadiologi'] = $this->Radio_model->listRadio();
        $this->template->view('master/radio/list_radio_v', $data);
    }

    public function edit($id)
    {
        $data['user'] = $this->UserModel->getUserById($id)->row();
        $data['grup'] = $this->UserModel->getGrup()->result();

        $this->template->view('master/radio/editradio_v', $data);
    }

    public function simpanUpdate()
    {
        $sesi = $this->session->userdata('logged_in');

        $config1['upload_path'] = FCPATH . 'assets/img/profil';
        $config1['allowed_types'] = 'gif|jpg|png';
        //$config1['file_name'] = $this->input->post('username').'.jpg';
        $config1['overwrite'] = TRUE;
        $config1['max_size'] = 2048000;
        $this->load->library('upload', $config1);

        $id = $this->input->post('id');

        if ($this->upload->do_upload('foto')) {

            $user = array(
                'nama' => $this->input->post('nama'),
                'username' => $this->input->post('username'),
                'email' => $this->input->post('email'),
                'password' => md5($this->input->post('password')),
                'password_ori' => $this->input->post('password'),
                'telepon' => $this->input->post('telepon'),
                'foto' => $this->upload->data('file_name'),
                'creator' => $sesi->id
            );
            $a = $this->MainModel->update($tabel = 'user', $user, $id);

            if ($a) {
                $this->session->set_flashdata('success', 'Data Laborat berhasil update!');
                redirect('Radio/listRadio');
            }

        } else {
            $user = array(
                'nama' => $this->input->post('nama'),
                'username' => $this->input->post('username'),
                'email' => $this->input->post('email'),
                'password' => md5($this->input->post('password')),
                'password_ori' => $this->input->post('password'),
                'telepon' => $this->input->post('telepon'),
                'creator' => $sesi->id
            );
            $a = $this->MainModel->update($tabel = 'user', $user, $id);
            if ($a) {
                $this->session->set_flashdata('success', 'Data Laborat berhasil update!');
                redirect('Radio/listRadio');
            }
        }
        $this->session->set_flashdata('warning', 'Data Laborat gagal update!');
        redirect('Radio/listRadio');
    }

    public function hapus($id)
    {
        $this->MainModel->delete('pemeriksaan', ['is_active' => 0], $id);
        $this->MainModel->delete('pendaftaran_pasien', ['is_active' => 0], $id);

        $this->session->set_flashdata('success', 'Berhasil menghapus pemeriksaan');
        redirect('Radio/search', 'refresh');
    }

    public function print($id)
    {
        $data['pemeriksaan'] = $this->AdministrasiModel->getPemeriksaanById($id)->row();
        $data['pasien'] = $this->db->query("SELECT * FROM pasien WHERE id = {$data['pemeriksaan']->pasien_id}")->row();
        $data['klinik'] = $this->AdministrasiModel->getKlinik()->row();
        $data['tindakan'] = $this->db->query('SELECT dtp.*, t.* FROM detail_tindakan_pemeriksaan dtp JOIN tarif_tindakan t ON t.id = dtp.tarif_tindakan_id WHERE dtp.pemeriksaan_id = '.$id)->result();
        $data['meta'] = unserialize($data['pemeriksaan']->meta);
        $data['nama_dokter'] = $this->db->query("SELECT * FROM user WHERE id_jenis_pendaftaran = 59")->row()->nama;

        $this->load->view('radio/print', $data);
    }
}
