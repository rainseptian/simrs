<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Pendaftaran extends MY_BpjsController
{

    public function __construct()
    {
        parent::__construct();

        $this->load->library('template');
        $this->load->Model('PendaftaranModel');
        $this->load->Model('LaporanModel');
        $this->load->Model('MainModel');
        $this->load->Model('PemeriksaanModel');
        $this->load->Model('PasienModel');
        $this->load->Model('PlaceModel');
        $this->load->helper(['kode_booking', 'usia']);
    }

    public function index()
    {
        redirect('pendaftaran/listPendaftaranPasien');
    }

    public function listPendaftaranPasien()
    {
        $data['listPendaftaran'] = $this->PendaftaranModel->getListPasienAntri();
        $data['jaminan'] = $this->config->item('pendaftaran');
        $data['jenis_pendaftaran'] = $this->PendaftaranModel->getJenisPendaftaran();

        $this->template->view('pendaftaran/list_pendaftaran_v', $data);
    }

    public function listKontrol()
    {
        $data['listPendaftaran'] = $this->PendaftaranModel->getListPasienKontrolAntri();
        $data['jaminan'] = $this->config->item('pendaftaran');
        $data['jenis_pendaftaran'] = $this->PendaftaranModel->getJenisPendaftaran();

        $this->template->view('pendaftaran/list_kontrol_v', $data);
    }

    public function pendaftaran_baru($id_antrian = 0)
    {
        if ($this->input->post('submit') == 1) {
            if (strlen($this->input->post('nik')) != 16) {
                $this->session->set_flashdata('warning', 'NIK harus 16 digit');
                return redirect('pendaftaran/pendaftaran_baru');
            }

            $session = $this->session->userdata('logged_in');
            $id_jenis_pendaftaran = $this->input->post('jenis_pendaftaran');
            $data['jenis_pendaftaran'] = $this->PendaftaranModel->getJenisPendaftaran($id_jenis_pendaftaran)->row();

            $pasien = array(
                'no_rm' => $this->getNextNoRm(),
                'no_bpjs' => $this->input->post('no_jaminan') ?? '',
                'nama' => $this->input->post('nama'),
                'nik' => $this->input->post('nik'),
                'tanggal_lahir' => $this->input->post('tanggal_lahir'),
                'usia' => get_usia($this->input->post('tanggal_lahir')),
                'tempat_lahir' => $this->input->post('tempat_lahir'),
                'jk' => $this->input->post('jenis_kelamin'),
                'alamat' => $this->input->post('alamat'),
                'alamat_domisili' => $this->input->post('alamat_domisili'),
                'telepon' => $this->input->post('telepon'),
                'pekerjaan' => $this->input->post('pekerjaan'),
                'agama' => $this->input->post('agama'),
                'tingkat_pendidikan' => $this->input->post('tingkat_pendidikan'),
                'penanggungjawab' => $this->input->post('penanggungjawab'),
                'creator' => $session->id,
                'provinsi_ktp' => $this->input->post('provinsi_ktp') ?? null,
                'kabupaten_ktp' => $this->input->post('kabupaten_ktp') ?? null,
                'kecamatan_ktp' => $this->input->post('kecamatan_ktp') ?? null,
                'desa_ktp' => $this->input->post('desa_ktp') ?? null,
                'provinsi_domisili' => $this->input->post('provinsi_domisili') ?? null,
                'kabupaten_domisili' => $this->input->post('kabupaten_domisili') ?? null,
                'kecamatan_domisili' => $this->input->post('kecamatan_domisili') ?? null,
                'desa_domisili' => $this->input->post('desa_domisili') ?? null,
            );

            if ($this->input->post('jaminan') == 'bpjs' && $this->input->post('no_jaminan')) {
                $pasien['no_bpjs'] = $this->input->post('no_jaminan');
            }

            $insert_id = $this->MainModel->insert_id($tabel = 'pasien', $pasien);

            $idJenisPendaftaranYangLangsungKePemeriksaan = [19, 25, 44, 45, 40, 41, 42, 43, 59];
            $apakaLangsungKePemeriksaan = in_array($this->input->post('jenis_pendaftaran'), $idJenisPendaftaranYangLangsungKePemeriksaan);

            $rm = array(
                'td' => $this->input->post('td'),
                'r' => $this->input->post('r'),
                'bb' => $this->input->post('bb'),
                'n' => $this->input->post('n'),
                's' => $this->input->post('s'),
                'tb' => $this->input->post('tb'),
                'bmi' => $this->input->post('bmi'),
                'no_rm' => $this->input->post('no_rm'),
                'pasien' => $insert_id,
                'penanggungjawab' => $this->input->post('penanggungjawab'),
                'biopsikososial' => $this->input->post('biopsikososial'),
                'jenis_pendaftaran_id' => $this->input->post('jenis_pendaftaran'),
                'jenis_kunjungan' => $this->input->post('jenis_kunjungan'),
                'jaminan' => $this->input->post('jaminan'),
                'no_jaminan' => $this->input->post('no_jaminan'),
                'nomor_rujukan' => $this->input->post('nomor_rujukan') ?? null,
                'dokter' => $this->input->post('dokter'),
                'status' => $apakaLangsungKePemeriksaan ? 'diperiksa' : 'antri',
                'is_bpjs' => (isset($_POST['pasien_bpjs']) ? 1 : 0),
                'creator' => $session->id,
                'waktu_pendaftaran' => $this->input->post('due_date') ?? date('Y-m-d'),
                'created_at' => $this->input->post('due_date') ?? date('Y-m-d'),
            );

            $insert_id2 = $this->MainModel->insert_id($tabel = 'pendaftaran_pasien', $rm);

            if ($apakaLangsungKePemeriksaan || $this->input->post('jenis_pendaftaran') == 58) {
                $periksa = array(
                    'pendaftaran_id' => $insert_id2,
                    'dokter_id' => $this->input->post('dokter'),
                    'pasien_id' => $insert_id,
                    'perawat_id' => '',
                    'no_rm' => $this->input->post('no_rm'),
                    'nama_pasien' => $this->input->post('nama'),
                    'keluhan_utama' => '',
                    'diagnosa_perawat' => '',
                    'asuhan_keperawatan' => '',
                    'bmi' => '',
                    'td' => '',
                    'r' => '',
                    'bb' => '',
                    'n' => '',
                    's' => '',
                    'tb' => '',
                    'is_bpjs' => 0,
                    'jaminan' => $this->input->post('jaminan'),
                    'status' => $this->input->post('jenis_pendaftaran') == 58 ? 'belum' : 'sudah_periksa_awal',
                    'creator' => $this->session->userdata('logged_in')->id
                );
                $pemeriksaan_id = $this->MainModel->insert_id($tabel = 'pemeriksaan', $periksa);
            }
            else {
                $pemeriksaan_id = null;
            }

            $kode_booking = generateKodeBooking();
            $id_antrian = $this->MainModel->insert_id('antrian', [
                'pendaftaran_id' => $insert_id2,
                'pemeriksaan_id' => $pemeriksaan_id,
                'jenis_pendaftaran_id' => $this->input->post('jenis_pendaftaran'),
                'pasien_id' => $insert_id,
                'due_date' => date('Y-m-d'),
                'kode_antrian' => generateKodeAntrian($this->input->post('jenis_pendaftaran'), $this->input->post('due_date') ?? date('Y-m-d')),
                'is_check_in' => false,
                'check_in_at' => date('Y-m-d H:i'),
                'kode_booking' => $kode_booking
            ]);

            $jns = $this->db->query("SELECT * FROM jenis_pendaftaran WHERE id = {$this->input->post('jenis_pendaftaran')}")->row();
            $dok = $this->db->query("SELECT * FROM user WHERE id = {$this->input->post('dokter')}")->row();
            $ant = $this->db->query("SELECT * FROM antrian WHERE id = $id_antrian")->row();
            $due_date = date('Y-m-d');
            $total_antrian_jkn = $this->db->query("SELECT * FROM antrian JOIN pendaftaran_pasien pp ON pp.id = antrian.pendaftaran_id WHERE antrian.jenis_pendaftaran_id = $jns->id AND due_date = '$due_date' AND pp.jaminan = 'bpjs'")->result();
            $total_antrian_non_jkn = $this->db->query("SELECT * FROM antrian JOIN pendaftaran_pasien pp ON pp.id = antrian.pendaftaran_id WHERE antrian.jenis_pendaftaran_id = $jns->id AND due_date = '$due_date' AND pp.jaminan != 'bpjs'")->result();
            $sisa_antrian = $this->db->query("SELECT * FROM antrian WHERE jenis_pendaftaran_id = $jns->id AND due_date = '$due_date' AND is_called = 0")->result();
            $c = count($sisa_antrian);
            $b = ($c - 1) * 15;
            $t = strtotime("+$b minutes");
            $body = [
                "kodebooking" => $kode_booking,
                "jenispasien" => $this->input->post('jaminan') == 'bpjs' ? 'JKN' : 'NON JKN',
                "nomorkartu" => $this->input->post('no_jaminan') ?? '',
                'nik' => $this->input->post('nik'),
                'nohp' => $this->input->post('telepon'),
                'kodepoli' => $jns->kode_bpjs ? explode(',', $jns->kode_bpjs)[0] : '',
                "namapoli" => $jns->nama,
                "pasienbaru" => 1,
                "norm" => $this->input->post('no_rm'),
                "tanggalperiksa" => date('Y-m-d'),
                "kodedokter" => (int)($dok->bpjs_id ?? '0'),
                "namadokter" => $dok->nama,
                "jampraktek" => '08:00-16:00',
                "jeniskunjungan" => $this->input->post('jenis_kunjungan'),
                "nomorreferensi" => $this->input->post('nomor_rujukan') ?? '',
                "nomorantrean" => $ant->kode_antrian,
                "angkaantrean" => (int) preg_replace("/[^0-9]/", "", $ant->kode_antrian),
                "estimasidilayani" => $t,
                "sisakuotajkn" => $jns->kuota_jkn - count($total_antrian_jkn),
                "kuotajkn" => (int) $jns->kuota_jkn,
                "sisakuotanonjkn" => $jns->kuota_non_jkn - count($total_antrian_non_jkn),
                "kuotanonjkn" => (int) $jns->kuota_non_jkn,
                "keterangan" => "Peserta harap 60 menit lebih awal guna pencatatan administrasi.",
            ];
            $res = $this->post("{$this->base_url_antrean}antrean/add", json_encode($body), true);
            $this->MainModel->insert('bpjs_jknmobile_log', [
                'url' => 'antrean/add',
                'header' => 'pend-baru',
                'request' => json_encode($body),
                'response' => json_encode($res),
            ]);

            $id_antrian_fo = $this->input->post('id_antrian');
            if ($id_antrian_fo) {
                $this->db->where('id', $id_antrian_fo)->update('antrian_front_office', [
                    'mulai_tunggu_poli_at' => date('Y-m-d H:i:s'),
                    'pendaftaran_id' => $insert_id2,
                    'pemeriksaan_id' => $pemeriksaan_id,
                ]);
                $a = $this->db->query("SELECT * FROM antrian_front_office WHERE id = $id_antrian_fo")->row();
                if ($a) {
                    foreach ([1,2] as $v) {
                        $waktu = $v == 1 ? $a->mulai_tunggu_admisi_at : ($v == 2 ? $a->mulai_layan_admisi_at : $a->mulai_tunggu_poli_at);
                        $task_body = ['kodebooking' => $kode_booking, 'taskid' => $v, 'waktu' => strtotime($waktu).'000'];
                        $this->MainModel->insert('bpjs_jknmobile_log', [
                            'url' => 'antrean/updatewaktu',
                            'header' => 'task '.$v,
                            'request' => json_encode($task_body),
                            'response' => json_encode($this->post("{$this->base_url_antrean}antrean/updatewaktu", json_encode($task_body), true)),
                        ]);
                    }
                }
            }
            else {
                foreach ([1,2] as $v) {
                    $task_body = ['kodebooking' => $kode_booking, 'taskid' => $v, 'waktu' => time().'000'];
                    $this->MainModel->insert('bpjs_jknmobile_log', [
                        'url' => 'antrean/updatewaktu',
                        'header' => 'task '.$v,
                        'request' => json_encode($task_body),
                        'response' => json_encode($this->post("{$this->base_url_antrean}antrean/updatewaktu", json_encode($task_body), true)),
                    ]);
                }
            }

            if ($insert_id2) {
                if ($this->input->post('jenis_pendaftaran') == 59) {
                    $this->session->set_flashdata('success', 'Pendaftaran pasien berhasil!');
                    redirect('Radio/search', 'refresh');
                } else {
                    $this->session->set_flashdata('success', 'Pendaftaran pasien berhasil!');
                    redirect('pendaftaran/listPendaftaranPasien', 'refresh');
                }
            }

        } else {
            $data['no_rm_auto'] = $this->getNextNoRm();
            $data['jenis_pendaftaran'] = $this->PendaftaranModel->getJenisPendaftaran();
            $data['dokter'] = $this->PendaftaranModel->getDokter();
            $data['jaminan'] = $this->config->item('pendaftaran');
            $data['provinces'] = $this->PlaceModel->getProvinces();
            $data['regencies'] = $this->PlaceModel->getregencies();
            $data['districts'] = $this->PlaceModel->getDistricts();
            $data['base_url_vclaim'] = MY_BpjsController::$base_url_vclaim_;
            $data['id_antrian'] = $id_antrian;

            $this->template->view('pendaftaran/pendaftaran_v', $data);
        }

    }

    public function pendaftaran_lama()
    {
        if ($this->input->post('submit') == 1) {
            $session = $this->session->userdata('logged_in');

            $idJenisPendaftaranYangLangsungKePemeriksaan = [19, 25, 44, 45, 40, 41, 42, 43, 59];
            $apakaLangsungKePemeriksaan = in_array($this->input->post('jenis_pendaftaran'), $idJenisPendaftaranYangLangsungKePemeriksaan);

            $id = $this->input->post('id');
            $user = array(
                'nama' => $this->input->post('nama'),
                'no_bpjs' => $this->input->post('no_jaminan'),
                'nik' => $this->input->post('nik'),
                'tempat_lahir' => $this->input->post('tempat_lahir'),
                'tanggal_lahir' => $this->input->post('tanggal_lahir'),
                'usia' => get_usia($this->input->post('tanggal_lahir')),
                'jk' => $this->input->post('jenis_kelamin'),
                'alamat' => $this->input->post('alamat'),
                'alamat_domisili'          =>$this->input->post('alamat_domisili'),
                'telepon' => $this->input->post('telepon'),
                'pekerjaan' => $this->input->post('pekerjaan'),
                'agama' => $this->input->post('agama'),
                'tingkat_pendidikan' => $this->input->post('tingkat_pendidikan'),
                'penanggungjawab' => $this->input->post('penanggungjawab'),
                'creator' => $session->id
            );

            if ($this->input->post('jaminan') == 'bpjs' && $this->input->post('no_jaminan')) {
                $pasien['no_bpjs'] = $this->input->post('no_jaminan');
            }

            $a = $this->MainModel->update($tabel = 'pasien', $user, $id);

            $rm = array(
                'td' => $this->input->post('td'),
                'r' => $this->input->post('r'),
                'bb' => $this->input->post('bb'),
                'n' => $this->input->post('n'),
                's' => $this->input->post('s'),
                'tb' => $this->input->post('tb'),
                'bmi' => $this->input->post('bmi'),
                'no_rm' => $this->input->post('no_rm'),
                'pasien' => $this->input->post('id'),
                'penanggungjawab' => $this->input->post('penanggungjawab'),
                'biopsikososial' => $this->input->post('biopsikososial'),
                'jenis_pendaftaran_id' => $this->input->post('jenis_pendaftaran'),
                'jenis_kunjungan' => $this->input->post('jenis_kunjungan'),
                'jaminan' => $this->input->post('jaminan'),
                'no_jaminan' => $this->input->post('no_jaminan'),
                'nomor_rujukan' => $this->input->post('nomor_rujukan') ?? null,
                'dokter' => $this->input->post('dokter'),
                'status' => $apakaLangsungKePemeriksaan ? 'diperiksa' : 'antri',
                'is_bpjs' => (isset($_POST['pasien_bpjs']) ? 1 : 0),
                'creator' => $session->id,
                'waktu_pendaftaran' => $this->input->post('due_date') ?? date('Y-m-d'),
                'created_at' => $this->input->post('due_date') ?? date('Y-m-d'),
            );

            $insert = $this->MainModel->insert_id($tabel = 'pendaftaran_pasien', $rm);

            if ($apakaLangsungKePemeriksaan || $this->input->post('jenis_pendaftaran') == 58) {
                $periksa = array(
                    'pendaftaran_id' => $insert,
                    'dokter_id' => $this->input->post('dokter'),
                    'pasien_id' => $this->input->post('id'),
                    'perawat_id' => '',
                    'no_rm' => $this->input->post('no_rm'),
                    'nama_pasien' => $this->input->post('nama'),
                    'keluhan_utama' => '',
                    'diagnosa_perawat' => '',
                    'asuhan_keperawatan' => '',
                    'bmi' => '',
                    'td' => '',
                    'r' => '',
                    'bb' => '',
                    'n' => '',
                    's' => '',
                    'tb' => '',
                    'is_bpjs' => 0,
                    'jaminan' => $this->input->post('jaminan'),
                    'status' => $this->input->post('jenis_pendaftaran') == 58 ? 'belum' : 'sudah_periksa_awal',
                    'creator' => $this->session->userdata('logged_in')->id
                );
                $pemeriksaan_id = $this->MainModel->insert_id($tabel = 'pemeriksaan', $periksa);
            }
            else {
                $pemeriksaan_id = null;
            }

            $kode_booking = generateKodeBooking();
            $id_antrian = $this->MainModel->insert_id('antrian', [
                'pendaftaran_id' => $insert,
                'pemeriksaan_id' => $pemeriksaan_id,
                'jenis_pendaftaran_id' => $this->input->post('jenis_pendaftaran'),
                'pasien_id' => $this->input->post('id'),
                'due_date' => date('Y-m-d'),
                'kode_antrian' => generateKodeAntrian($this->input->post('jenis_pendaftaran'), $this->input->post('due_date') ?? date('Y-m-d')),
                'is_check_in' => false,
                'check_in_at' => date('Y-m-d H:i'),
                'kode_booking' => $kode_booking
            ]);

            $jns = $this->db->query("SELECT * FROM jenis_pendaftaran WHERE id = {$this->input->post('jenis_pendaftaran')}")->row();
            $dok = $this->db->query("SELECT * FROM user WHERE id = {$this->input->post('dokter')}")->row();
            $ant = $this->db->query("SELECT * FROM antrian WHERE id = $id_antrian")->row();
            $due_date = date('Y-m-d');
            $total_antrian_jkn = $this->db->query("SELECT * FROM antrian JOIN pendaftaran_pasien pp ON pp.id = antrian.pendaftaran_id WHERE antrian.jenis_pendaftaran_id = $jns->id AND due_date = '$due_date' AND pp.jaminan = 'bpjs'")->result();
            $total_antrian_non_jkn = $this->db->query("SELECT * FROM antrian JOIN pendaftaran_pasien pp ON pp.id = antrian.pendaftaran_id WHERE antrian.jenis_pendaftaran_id = $jns->id AND due_date = '$due_date' AND pp.jaminan != 'bpjs'")->result();
            $sisa_antrian = $this->db->query("SELECT * FROM antrian WHERE jenis_pendaftaran_id = $jns->id AND due_date = '$due_date' AND is_called = 0")->result();
            $c = count($sisa_antrian);
            $b = ($c - 1) * 15;
            $t = strtotime("+$b minutes");
            $body = [
                "kodebooking" => $kode_booking,
                "jenispasien" => $this->input->post('jaminan') == 'bpjs' ? 'JKN' : 'NON JKN',
                "nomorkartu" => $this->input->post('no_jaminan') ?? '',
                'nik' => $this->input->post('nik'),
                'nohp' => $this->input->post('telepon'),
                'kodepoli' => $jns->kode_bpjs ? explode(',', $jns->kode_bpjs)[0] : '',
                "namapoli" => $jns->nama,
                "pasienbaru" => 0,
                "norm" => $this->input->post('no_rm'),
                "tanggalperiksa" => date('Y-m-d'),
                "kodedokter" => (int)($dok->bpjs_id ?? '0'),
                "namadokter" => $dok->nama,
                "jampraktek" => '08:00-16:00',
                "jeniskunjungan" => $this->input->post('jenis_kunjungan'),
                "nomorreferensi" => $this->input->post('nomor_rujukan') ?? '',
                "nomorantrean" => $ant->kode_antrian,
                "angkaantrean" => (int) preg_replace("/[^0-9]/", "", $ant->kode_antrian),
                "estimasidilayani" => $t,
                "sisakuotajkn" => $jns->kuota_jkn - count($total_antrian_jkn),
                "kuotajkn" => (int) $jns->kuota_jkn,
                "sisakuotanonjkn" => $jns->kuota_non_jkn - count($total_antrian_non_jkn),
                "kuotanonjkn" => (int) $jns->kuota_non_jkn,
                "keterangan" => "Peserta harap 60 menit lebih awal guna pencatatan administrasi.",
            ];
            $res = $this->post("{$this->base_url_antrean}antrean/add", json_encode($body), true);
            $this->MainModel->insert('bpjs_jknmobile_log', [
                'url' => 'antrean/add',
                'header' => 'pend-lama',
                'request' => json_encode($body),
                'response' => json_encode($res),
                'ws_bpjs_request' => '',
                'ws_bpjs_response' => '',
            ]);

            $id_antrian_fo = $this->input->post('id_antrian');
            if ($id_antrian_fo) {
                $this->db->where('id', $id_antrian_fo)->update('antrian_front_office', [
                    'mulai_tunggu_poli_at' => date('Y-m-d H:i:s'),
                    'pendaftaran_id' => $insert,
                    'pemeriksaan_id' => $pemeriksaan_id,
                ]);
            }

            if ($insert) {
                if ($this->input->post('jenis_pendaftaran') == 59) {
                    $this->session->set_flashdata('success', 'Pendaftaran pasien berhasil!');
                    redirect('Radio/search', 'refresh');
                } else {
                    $this->session->set_flashdata('success', 'Pendaftaran pasien berhasil!');
                    redirect('pendaftaran/listPendaftaranPasien', 'refresh');
                }
            } else {
                $this->session->set_flashdata('warning', 'Pendaftaran pasien gagal!');
                redirect('pendaftaran/pendaftaran_lama', 'refresh');
            }


        } else {
            $id = $this->input->post('id_pasien') ?? $this->session->flashdata('id_pasien');
            //print_r($id);die();
            $data['jenis_pendaftaran'] = $this->PendaftaranModel->getJenisPendaftaran();
            $data['dokter'] = $this->PendaftaranModel->getDokter();
            $data['pasien'] = $this->PendaftaranModel->getPasienById($id)->row();
            $data['jaminan'] = $this->config->item('pendaftaran');
            //print_r($data['pasien']);die();
            $this->template->view('pendaftaran/pendaftaran_lama_v', $data);
        }

    }

    public function pendaftaran_lama2($id_antrian = 0)
    {
        $id = $this->input->post('id_pasien') ?? $this->session->flashdata('id_pasien');

        $data['jenis_pendaftaran'] = $this->PendaftaranModel->getJenisPendaftaran();
        $data['dokter'] = $this->PendaftaranModel->getDokter();
        $data['pasien'] = $this->PendaftaranModel->getPasienById($id)->row();
        $data['pemeriksaan'] = $this->PendaftaranModel->getPemeriksaanPasienById($id)->result();

        $data['tindakan'] = $this->LaporanModel->getTindakanById($id);
        $data['obat'] = $this->LaporanModel->getObatPemeriksaanById($id);
        $data['penyakit'] = $this->LaporanModel->getPenyakitPemeriksaanById($id);
        $data['racikan'] = $this->LaporanModel->getRacikanPemeriksaan();
        $data['pendaftaran'] = $this->PendaftaranModel->getPendaftaranByIdPasien($id)->row();
        $data['jaminan'] = $this->config->item('pendaftaran');
        $data['base_url_vclaim'] = MY_BpjsController::$base_url_vclaim_;
        $data['id_antrian'] = $id_antrian;

        $this->template->view('pendaftaran/pendaftaran_lama2_v', $data);
    }

    private function getNextNoRm()
    {
        $m = date('Ym');
        $p = $this->db->query("SELECT * FROM pasien WHERE no_rm like '$m%' ORDER BY no_rm DESC")->row();
        $no = $p && $p->no_rm ? str_pad(((int) substr($p->no_rm, -4)) + 1, 4, '0', STR_PAD_LEFT) : '0001';
        return date('Ym').$no;
    }

    public function cari()
    {
        $no_rm = $this->input->get('no_rm');
        $cari = $this->PendaftaranModel->cari($no_rm)->result();
        echo json_encode($cari);
    }

    public function ajax_kode()
    {
        if ($this->input->is_ajax_request()) {
            $keyword = $this->input->post('keyword');


            $pasien = $this->PendaftaranModel->cari_kode($keyword);

            if ($pasien->num_rows() > 0) {
                $json['status'] = 1;
                $json['datanya'] = "<ul id='daftar-autocomplete'>";
                foreach ($pasien->result() as $b) {
                    $json['datanya'] .= "
						<li>
						    <span id='kodenya' style='display: none;'>$b->id</span>
						    <span id='nohpnya' style='display: none;'>$b->telepon</span>
							<b>NO RM</b> :
							<span id='no_rmnya'>$b->no_rm</span> <br />
							<b>Nama</b> :
							<span id='namanya'>$b->nama</span> <br />
							<b>NIK</b> :
							<span id='niknya'>$b->nik</span> <br />
							<b>Alamat</b> :
							<span id='alamatnya'>$b->alamat</span> <br />

						</li>
					";
                }
                $json['datanya'] .= "</ul>";
            } else {
                $json['status'] = 0;
            }

            echo json_encode($json);
        }
    }

    public function getAktifPasien() {
        if ($this->input->is_ajax_request()) {
            $keyword = $this->input->post('keyword');


            $pasien = $this->PendaftaranModel->getAktifPasien($keyword);

            if ($pasien->num_rows() > 0) {
                $json['status'] = 1;
                $json['datanya'] = "<ul id='daftar-autocomplete'>";
                foreach ($pasien->result() as $b) {
                    $json['datanya'] .= "
						<li>
						    <span id='jaminannya' style='display: none;'>" . $b->jaminan . "</span>
						    <span id='nojaminannya' style='display: none;'>" . $b->no_jaminan . "</span>
						    <span id='pendaftaran_id' style='display: none;'>" . $b->pendaftaran_id . "</span>
						    <span id='kodenya' style='display: none;'>" . $b->id . "</span>
						    <span id='nohpnya' style='display: none;'>" . $b->telepon . "</span>
							<b>NO RM</b> :
							<span id='no_rmnya'>" . $b->no_rm . "</span> <br />
							<b>Nama</b> :
							<span id='namanya'>" . $b->nama . "</span> <br />
							<b>Alamat</b> :
							<span id='alamatnya'>" . $b->alamat . "</span> <br />

						</li>
					";
                }
                $json['datanya'] .= "</ul>";
            } else {
                $json['status'] = 0;
            }

            echo json_encode($json);
        }
    }

    public function barcode_kode()
    {
        if ($this->input->is_ajax_request()) {
            $keyword = $this->input->post('keyword');


            $pasien = $this->PendaftaranModel->cari_kode($keyword);
            if ($pasien->num_rows() > 0) {

                $json['status'] = 1;
                foreach ($pasien->result() as $b) {

                    $json['kode'] = $b->id;
                    $json['no_rm'] = $b->no_rm;
                    $json['nama'] = $b->nama;

                }
            } else {

                $json['pesan'] = "Keyword Yang Dimasukkan Belum Terdaftar";
                $json['status'] = 0;

            }
            echo json_encode($json);
        }
    }

    public function hapus_pendaftaran($id, $kontrol = 0)
    {
        $ant = $this->db->query("SELECT * FROM antrian WHERE pendaftaran_id = $id")->row();
        if ($this->PendaftaranModel->hapusPendaftaran($id)) {
            if ($ant) {
                $task_body = ['kodebooking' => $ant->kode_booking ?? '', 'taskid' => 99, 'waktu' => time().'000'];
                $this->MainModel->insert('bpjs_jknmobile_log', [
                    'url' => 'antrean/updatewaktu',
                    'header' => 'task 99',
                    'request' => json_encode($task_body),
                    'response' => json_encode($this->post("{$this->base_url_antrean}antrean/updatewaktu", json_encode($task_body), true)),
                ]);

                $body = [
                    'kodebooking' => $ant->kode_booking,
                    'keterangan' => 'Dibatalkan petugas'
                ];
                $res = $this->post("{$this->base_url_antrean}antrean/batal", json_encode($body), true);
                $this->MainModel->insert('bpjs_jknmobile_log', [
                    'url' => 'antrean/batal',
                    'header' => '',
                    'request' => json_encode($body),
                    'response' => json_encode($res),
                    'ws_bpjs_request' => '',
                    'ws_bpjs_response' => '',
                ]);
            }

            $this->session->set_flashdata('success', 'Data pendaftaran berhasil dihapus!');
        }
        else {
            $this->session->set_flashdata('warning', 'Data pendaftaran gagal dihapus!');
        }

        if ($kontrol) {
            redirect('pendaftaran/listKontrol');
        }
        else {
            redirect('pendaftaran/listPendaftaranPasien');
        }
    }

    public function villages($district_id) {
        $res = $this->db->query("SELECT * FROM villages WHERE district_id = {$district_id}")->result();
        echo json_encode($res);
    }

    public function get_pemeriksaan($id_pasien)
    {
        echo json_encode([
            'data' => $this->db->query("
                SELECT pemeriksaan.id, pemeriksaan.dokter_id, jp.nama nama_poli, pemeriksaan.created_at, u.nama as nama_dokter 
                FROM pemeriksaan
                JOIN pendaftaran_pasien pp on pemeriksaan.pendaftaran_id = pp.id
                JOIN jenis_pendaftaran jp on pp.jenis_pendaftaran_id = jp.id
                JOIN user u on pemeriksaan.dokter_id = u.id and u.is_active = 1
                WHERE pemeriksaan.pasien_id = $id_pasien ORDER BY pemeriksaan.created_at DESC LIMIT 20
            ")->result()
        ]);
    }

    public function set_jenis($pemeriksaan_id, $jaminan, $jenis)
    {
        if ($jenis == "Poli") {
            $pem = $this->db->query("SELECT * FROM pemeriksaan WHERE id = $pemeriksaan_id")->row();
            $this->MainModel->update('pemeriksaan', ['jaminan' => $jaminan], $pemeriksaan_id);
            $this->MainModel->update('pendaftaran_pasien', ['jaminan' => $jaminan], $pem->pendaftaran_id);
        }
        else if ($jenis == 'Ranap') {
            $this->MainModel->update('rawat_inap', ['tipe_pasien' => $jaminan], $pemeriksaan_id);
        }
        else {
            $this->MainModel->update('transfer', ['tipe_pasien' => $jaminan], $pemeriksaan_id);
        }

        $this->session->set_flashdata('success', 'Berhasil mengubah jenis pendaftaran');
        redirect('Administrasi/kasir_all');
    }

    public function checkin($id, $to = '')
    {
        $ant = $this->db->query("SELECT * FROM antrian WHERE pendaftaran_id = $id")->row();

        $this->MainModel->update('antrian', ['is_check_in' => 1], $ant->id);

        try {
            $task_body = ['kodebooking' => $ant->kode_booking, 'taskid' => 3, 'waktu' => time().'000'];
            $this->MainModel->insert('bpjs_jknmobile_log', [
                'url' => 'antrean/updatewaktu',
                'header' => 'task 3',
                'request' => json_encode($task_body),
                'response' => json_encode($this->post("{$this->base_url_antrean}antrean/updatewaktu", json_encode($task_body), true)),
            ]);
            $this->session->set_flashdata('success', 'Berhasil check in pasien');
        }
        catch (Exception $e) {
            $this->session->set_flashdata('error', 'Gagal check in pasien');
        }

        redirect('pendaftaran/' . $to);
    }

    public function resend_antrian($id, $to = '')
    {
        $ant = $this->db->query("SELECT * FROM antrian WHERE pendaftaran_id = $id")->row();

        if ($this->input->server('REQUEST_METHOD') === 'POST') {
            $log = $this->db->query("SELECT * FROM bpjs_jknmobile_log WHERE kode_booking = '$ant->kode_booking' AND url = 'antrean/add'")->row();
            $body = json_decode($log->request, true);
            $body['nik'] = $this->input->post('nik');
            $body['nomorkartu'] = $this->input->post('nokartu');
            $body['nomorreferensi'] = $this->input->post('nomorreferensi');

            $res = $this->post("{$this->base_url_antrean}antrean/add", json_encode($body), true);
            $this->MainModel->insert_id('bpjs_jknmobile_log', [
                'url' => 'antrean/add',
                'header' => 'pend-lama',
                'request' => json_encode($body),
                'response' => json_encode($res),
                'ws_bpjs_request' => '',
                'ws_bpjs_response' => '',
                'kode_booking' => $ant->kode_booking,
            ]);

            if ($res->success) {
                $this->session->set_flashdata('success', 'Berhasil kirim antrian');
            }
            else {
                $this->session->set_flashdata('error', 'Gagal kirim antrian');
            }
        }

        redirect('pendaftaran/' . $to);
    }
}
