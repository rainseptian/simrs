<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Pasien extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();

        $this->load->library('template');
        $this->load->Model('PasienModel');
        $this->load->Model('UserModel');
        $this->load->Model('MainModel');
    }

    public function index()
    {
        redirect('Pasien/listPasien');
    }

    public function listPasien()
    {
        $data['listPendaftaran'] = $this->PasienModel->getListPasien();

        $this->template->view('master/pasien/list_pasien_v', $data);
    }


    public function edit($id, $to = 0)
    {
        $sesi = $this->session->userdata('logged_in');
        $data['pasien'] = $this->PasienModel->getPasienById($id)->row();
        $data['to'] = $to;

        $this->template->view('master/pasien/pasien_edit_v', $data);
    }

    public function simpanUpdate()
    {
        $sesi = $this->session->userdata('logged_in');

        $id = $this->input->post('id');
        $to = $this->input->post('to');

        $user = array(
            'no_rm' => $this->input->post('no_rm'),
            'no_bpjs' => $this->input->post('no_bpjs'),
            'nama' => $this->input->post('nama'),
            'nik' => $this->input->post('nik'),
            'tempat_lahir' => $this->input->post('tempat_lahir'),
            'tanggal_lahir' => $this->input->post('tanggal_lahir'),
            'jk' => $this->input->post('jenis_kelamin'),
            'alamat' => $this->input->post('alamat'),
            'pekerjaan' => $this->input->post('pekerjaan'),
            'agama' => $this->input->post('agama'),
            'telepon' => $this->input->post('telepon'),
            'penanggungjawab' => $this->input->post('penanggungjawab'),
            'biopsikososial' => $this->input->post('biopsikososial'),
            'creator' => $sesi->id
        );
        $a = $this->MainModel->update($tabel = 'pasien', $user, $id);

        $this->db->query("UPDATE pendaftaran_pasien SET no_rm = '{$this->input->post('no_rm')}' WHERE pasien = {$id}");

        if ($a)
            $this->session->set_flashdata('success', 'Data Pasien berhasil update!');
        else
            $this->session->set_flashdata('warning', 'Data Pasien gagal update!');

        redirect($to == 'pendaftaran' ? 'pendaftaran/listPendaftaranPasien' : 'Pasien/listPasien');
    }

    public function delete_pasien($id)
    {
        $data = array('is_active' => '0');
        $delete = $this->MainModel->delete($table = 'pasien', $data, $id);
        if ($delete) {
            $this->session->set_flashdata('success', 'Data Pasien berhasil dihapus!');
            redirect('Pasien/listPasien');
        }
        $this->session->set_flashdata('warning', 'Data Pasien gagal dihapus!');
        redirect('Pasien/listPasien');

    }


    public function ajax_kode()
    {
        if ($this->input->is_ajax_request()) {
            $keyword = $this->input->post('keyword');


            $pasien = $this->PendaftaranModel->cari_kode($keyword);

            if ($pasien->num_rows() > 0) {
                $json['status'] = 1;
                $json['datanya'] = "<ul id='daftar-autocomplete'>";
                foreach ($pasien->result() as $b) {
                    $json['datanya'] .= "
						<li>
							<b>Kode</b> : 
							<span id='kodenya'>" . $b->id . "</span> <br />
							<span id='no_rmnya'>" . $b->no_rm . "</span> <br />
							<span id='namanya'>" . $b->nama . "</span> <br />
							
						</li>
					";
                    /*$json['datanya'] .= "
                        <li>
                            <b>Kode</b> :
                            <span id='kodenya'>".$b->id."</span> <br />
                            <span id='barangnya'>".$b->nama_usaha."</span> <br />
                            <span id='harganya'>".$b->badan_hukum."</span>
                        </li>
                    ";*/
                }
                $json['datanya'] .= "</ul>";
            } else {
                $json['status'] = 0;
            }

            echo json_encode($json);
        }
    }


}

