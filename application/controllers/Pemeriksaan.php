<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pemeriksaan extends MY_BpjsController {

    public function __construct() {
        parent::__construct();

        $this->load->library('template');
        $this->load->Model('PendaftaranModel');
        $this->load->Model('PemeriksaanModel');
        $this->load->Model('LaporanModel');
        $this->load->Model('ObatModel');
        $this->load->Model('MainModel');
        $this->load->Model('TarifTindakanModel');
        $this->load->Model('PenyakitModel');
        $this->load->Model('DetailPenyakitPemeriksaanModel');
        $this->load->Model('PerawatModel');
        $this->load->Model('DokterModel');
        $this->load->Model('AdministrasiModel');
        $this->load->Model('ObatRacikanModel');
        $this->load->helper('kode_booking');
    }

    public function index() {
        if ($this->input->post('submit')) {
            $insert = $this->MainModel->insert($pasien);
        } else {
            $this->template->view('pemeriksaan/pemeriksaan_v');
        }
    }

    public function rekamedis() {
        $id = $this->input->post('id');

        $data['pasien'] = $this->PendaftaranModel->getPasienById($id)->row();
        $data['pemeriksaan'] = $this->PendaftaranModel->getPemeriksaanPasienById($id)->result();
        $data['tindakan'] = $this->LaporanModel->getTindakanById($id);
        $data['penyakit'] = $this->LaporanModel->getPenyakitPemeriksaanById($id);
        $data['pendaftaran'] = $this->PendaftaranModel->getPendaftaranByIdPasien($id)->row();
        $data['obat'] = $this->LaporanModel->getObatPemeriksaanById($id);

        foreach ($data['pemeriksaan'] as $v) {
            $v->racikans = $this->ObatRacikanModel->getRacikanByIdPemeriksaan($v->id)->result();
            foreach ($v->racikans as &$vv) {
                $vv->racikan = $this->ObatRacikanModel->getObatRacikanByIdDetailObatRacikan($vv->id)->result();
            }

            $racikan_luar = $this->ObatRacikanModel->getRacikanLuarByIdPemeriksaan($v->id)->result();
            foreach ($racikan_luar as &$vv) {
                $vv->racikan = $this->ObatRacikanModel->getObatRacikanLuarByIdDetailObatRacikan($vv->id)->result();
            }
            $v->racikans = array_merge($v->racikans, $racikan_luar);
        }

        $this->load->view('pemeriksaan/rekamedis', $data);
    }

    public function listpemeriksaanPasien() {
        $u = $this->session->get_userdata('logged_in');
        $u = $u['logged_in'];
        $is_dokter = $u->nama_grup == 'dokter';
        $is_laborat = $u->nama_grup == 'laborat';
        $is_ekg = $u->nama_grup == 'ekg';
        $is_spirometri = $u->nama_grup == 'spirometri';

        if ($is_dokter) {
            $data['listPendaftaran'] = $this->PemeriksaanModel->getListPemeriksaanSudahPeriksaAwalByIdJenisPendaftaran($u->id_jenis_pendaftaran);
        }
        else if ($is_laborat || $is_ekg || $is_spirometri) {

            if ($is_laborat) {
                $id = '19';
            } else if ($is_ekg) {
                $id = '40';
            } else if ($is_spirometri) {
                $id = '42';
            }
            $data['listPendaftaran'] = $this->PemeriksaanModel->getListPemeriksaanSudahPeriksaAwalByIdJenisPendaftaran($id);
        } 
        else {
    	    $y = $this->PemeriksaanModel->getJenisPendafataranSudahPeriksaAwal();
            $data['jenis_pendaftaran'] = [];
            foreach($y as &$jp) {
                if ($jp->jenis_pendaftaran_id == 19 || $jp->jenis_pendaftaran_id == 58) {
                    continue;
                }
                $jp->list = $this->PemeriksaanModel->getListPemeriksaanSudahPeriksaAwalByIdJenisPendaftaran($jp->jenis_pendaftaran_id)->result();
                $data['jenis_pendaftaran'][] = $jp;
            }
        }

        $data['jaminan'] = $this->config->item('pendaftaran');

        $this->template->view('pemeriksaan/list_blom_diperiksa_v', $data);
    }

    public function listPasienSelesaiPeriksa() {
        $u = $this->session->get_userdata('logged_in');
        $u = $u['logged_in'];
        $is_dokter = $u->nama_grup == 'dokter';
        $is_laborat = $u->nama_grup == 'laborat';
        $is_ekg = $u->nama_grup == 'ekg';
        $is_spirometri = $u->nama_grup == 'spirometri';

        if ($is_dokter) {
            $data['listPendaftaran'] = $this->PemeriksaanModel->getListPemeriksaanSudahPeriksa($u->id_jenis_pendaftaran)->result();
            foreach ($data['listPendaftaran'] as $datum) {
                $datum->penyakits = $this->DetailPenyakitPemeriksaanModel->getDetailPenyakitByPemeriksaanId($datum->id)->result();
            }
        }
        else if ($is_laborat || $is_ekg || $is_spirometri) {

            if ($is_laborat) {
                $id = '19';
            } else if ($is_ekg) {
                $id = '40';
            } else if ($is_spirometri) {
                $id = '42';
            }
            $data['listPendaftaran'] = $this->PemeriksaanModel->getListPemeriksaanSudahPeriksa($id);
        }
        else {
            $y = $this->PemeriksaanModel->getJenisPendafataranSudahPeriksa();
            $data['jenis_pendaftaran'] = [];
            foreach($y as &$jp) {
                if ($jp->jenis_pendaftaran_id == 19 || $jp->jenis_pendaftaran_id == 58) {
                    continue;
                }
                $jp->list = $this->PemeriksaanModel->getListPemeriksaanSudahPeriksa($jp->jenis_pendaftaran_id)->result();
                foreach ($jp->list as $datum) {
                    $datum->penyakits = $this->DetailPenyakitPemeriksaanModel->getDetailPenyakitByPemeriksaanId($datum->id)->result();
                }
                $data['jenis_pendaftaran'][] = $jp;
            }
        }

//        $data['listPemeriksaan'] = [];
//        $res = $this->PemeriksaanModel->getListPemeriksaanSudahPeriksa()->result();
//        foreach ($res as &$datum) {
//            if ($datum->poli_id == 19 || $datum->poli_id == 58)
//                continue;
//
//            $datum->penyakits = $this->DetailPenyakitPemeriksaanModel->getDetailPenyakitByPemeriksaanId($datum->id)->result();
//            $data['listPemeriksaan'][] = $datum;
//        }

        $data['jaminan'] = $this->config->item('pendaftaran');

        $this->template->view('pemeriksaan/list_pasien_diperiksa_v', $data);
    }

    public function detail() {
        $id = $this->input->get('id_pasien');
        $pemeriksaan = $this->PendaftaranModel->getPemeriksaanPasienById($id)->result();
        $tindakan = $this->LaporanModel->getTindakanById($id);
        $obat = $this->LaporanModel->getObatPemeriksaanById($id);
        $penyakit = $this->LaporanModel->getPenyakitPemeriksaanById($id);
        /* $html='';
         foreach ($pemeriksaan as $key) {
            $html .='<p>'.$key->waktu_pemeriksaan.'</p>';
        }*/
        $html = '<div class="box box-danger">
              <div class="box-header">
                <h3 class="box-title">Riwayat Periksa</h3>&nbsp;&nbsp;

              </div>

              <div class="box-body">
                <table id="example2" class="table table-bordered table-hover">
                  <thead>
                          <tr>
                              <th>No</th>
                              <th>Tanggal Periksa</th>
                              <th>Diagnosis Jenis Penyakit</th>
                              <th>Obat</th>
                              <th>Tindakan</th>
                          </tr>
                      </thead>
                      <tbody>';
        $no = 0;
        foreach ($pemeriksaan as $row) {
            $no++;
            $date = date('d-F-Y', strtotime($row->waktu_pemeriksaan));
            $html .= '<tr>
                              <td>' . $no . '</td>
                              <td>' . $date . '</td>
                              <td> <table style="font-size: 10px">
                                      <thead>
                                          <tr>
                                              <th>Nama Penyakit</th>
                                              <th>Kode</th>
                                          </tr>
                                      </thead>
                                      <tbody>';


            foreach ($penyakit->result() as $row1) {
                if ($row->id == $row1->pemeriksaan_id) {
                    $html .= '<tr>
                                                    <td>' . $row1->nama . '</td>
                                                    <td>' . $row1->kode . '</td>
                                                </tr>';
                }
            }

            $html .= '</tbody>
                                  </table>
                              </td>
                              <td>
                                <table style="font-size: 10px">
                                      <thead>
                                          <tr>

                                              <th>Nama Obat</th>
                                              <th>jumlah</th>
                                          </tr>
                                      </thead>
                                      <tbody>';


            foreach ($obat->result() as $row2) {
                if ($row->id == $row2->pemeriksaan_id) {
                    $html .= '<tr>

                                                    <td>' . $row2->nama . '</td>
                                                     <td>' . $row2->jumlah_satuan . '</td>
                                                </tr>';
                }
                $no++;
            }


            $html .= '</tbody>

                                  </table>
                                </td>
                                <td>
                                  <table style="font-size: 10px">
                                      <thead>
                                          <tr>

                                              <th>Nama Tindakan</th>


                                          </tr>
                                      </thead>
                                      <tbody>';


            foreach ($tindakan->result() as $row3) {
                if ($row->id == $row3->pemeriksaan_id) {
                    $html .= '<tr>

                                                    <td>' . $row3->nama . '</td>

                                                </tr>';
                }
            }

            $html .= '</tbody>
                                  </table>
                                </td>
                          </tr>';

        }


        $html .= '</tbody>

                </table>
              </div>

                </div>';
        echo $html;
    }

    public function periksa($id = "") {

        $session = $this->session->userdata('logged_in');

        if ($this->input->post('submit') == 1 || $this->input->post('rujuk') || $this->input->post('input_resep')) {

            if (isset($_POST['meta'])) {
                if (isset($_POST['kia_hamil'])) {
                    $meta = $this->input->post('meta');
                    if ($_POST['kia_hamil']) {
                        $meta['jenis'][] = 'hamil';
                    }
                    if ($_POST['kia_kb']) {
                        $meta['jenis'][] = 'kb';
                    }
                    $meta = serialize($meta);
                }
                else {
                    $meta = serialize($this->input->post('meta'));
                }
            }
            else {
                $meta = '';
            }

            $hp_lab = $this->input->post('hasil_penunjang_laboratorium');
            $hp_ekg = $this->input->post('hasil_penunjang_ekg');
            $hp_spirometri = $this->input->post('hasil_penunjang_spirometri');

            if ($hp_lab) {
                $hp['laboratorium'] = $hp_lab;
            }
            if ($hp_ekg) {
                $hp['ekg'] = $hp_ekg;
            }
            if ($hp_spirometri) {
                $hp['spirometri'] = $hp_spirometri;
            }
            else {
                $hp = [];
            }

            $input_perawat = $this->input->post('perawat');

            $periksa = array(
                'td' => $this->input->post('td'),
                'r' => $this->input->post('r'),
                'bb' => $this->input->post('bb'),
                'n' => $this->input->post('n'),
                's' => $this->input->post('s'),
                'tb' => $this->input->post('tb'),
                'bmi' => $this->input->post('bmi'),
                'spo2' => $this->input->post('spo2'),
                'detail_perawat_id' => implode(',', $input_perawat) == null ? '' : implode(',', $input_perawat),
                'keluhan_utama' => $this->input->post('keluhan_utama'),
                'catatan_odontogram' => $this->input->post('catatan_odontogram'),
                'meta' => $meta,
                'hd' => isset($_POST['hd']) ? serialize($this->input->post('hd')) : '',
                'amammesia' => $this->input->post('amammesia'),
                'diagnosis' => $this->input->post('diagnosis'),
                'pemeriksaan_fisik' => $this->input->post('pemeriksaan_fisik'),
                'hasil_penunjang' => json_encode($hp),
                'diagnosis_banding' => $this->input->post('diagnosis_banding'),
                'deskripsi_tindakan' => $this->input->post('deskripsi_tindakan'),
                'saran_pemeriksaan' => $this->input->post('saran_pemeriksaan'),
                'creator' => $session->id,
            );

            $rujuk = isset($_POST['rujuk']) && $_POST['rujuk'];
            $input_resep = isset($_POST['input_resep']) && $_POST['input_resep'];
            if (!$rujuk && !$input_resep) {
                $periksa['status'] = 'sudah_periksa';
                $periksa['sudah_periksa_dokter'] = 1;
            }
            if (isset($_POST['asuhan_keperawatan'])) {
                $periksa['asuhan_keperawatan'] = $this->input->post('asuhan_keperawatan');
            }
            if ($this->input->post('is_rujuk_internal')) {
                $periksa['dokter_id'] = $this->input->post('dokter');
                $this->MainModel->update('pendaftaran_pasien', ['dokter' => $this->input->post('dokter')], $this->input->post('pendaftaran_id'));
            }

            $this->MainModel->update('pemeriksaan', $periksa, $id);

            if ($this->input->post('surat')) {
                $this->MainModel->update('pendaftaran_pasien', [
                    'surat' => $this->input->post('surat'),
                    'surat_form' => json_encode($this->input->post('surat_form')),
                ], $this->input->post('pendaftaran_id'));
            }

            $this->MainModel->hardDeleteWhere('detail_tindakan_pemeriksaan', ['pemeriksaan_id', $id]);

            $input_tindakan = $this->input->post('tindakan');
            foreach ($input_tindakan as $key => $value) {
                $tarif_perawat = $this->PemeriksaanModel->getTarifTindakanById($value)->tarif_perawat;
                $tindakan = array(
                    'pemeriksaan_id' => $id,
                    'tarif_tindakan_id' => $value,
                    'jumlah_perawat' => count($input_perawat),
                    'perawat' => implode(',', $input_perawat),
                    'tarif_per_perawat' => $tarif_perawat / count($input_perawat),
                    'creator' => $session->id
                );
                $this->MainModel->insert_id('detail_tindakan_pemeriksaan', $tindakan);
            }

            $this->MainModel->hardDeleteWhere('detail_penyakit_pemeriksaan', ['pemeriksaan_id', $id]);
            $penyakit = $this->input->post('diagnosis_jenis_penyakit');
            foreach ($penyakit as $key => $value) {
                $penyakit = array(
                    'pemeriksaan_id' => $id,
                    'penyakit_id' => $value,
                    'creator	' => $session->id
                );
                $this->MainModel->insert_id('detail_penyakit_pemeriksaan', $penyakit);
            }

            $this->db->delete('detail_bahan_pemeriksaan', array('pemeriksaan_id' => $id));

            if (isset($_POST['id'])) {
                $x=0;
                $jumlah_bahan =$_POST['qty'];
                foreach ($_POST['id'] as $value) {
                    $this->db->insert(
                        'detail_bahan_pemeriksaan',
                        array(
                            'pemeriksaan_id' => $id,
                            'bahan_id' => $value,
                            'jumlah' => $jumlah_bahan[$x],
                            'creator' => $session->id
                        )
                    );
                    $x++;
                }
            }

            if ($this->input->post('is_rujuk_internal')) {
                $this->insertRujukanInternalBpjs($id);
            }

            if (isset($_POST['rujuk']) && $_POST['rujuk']) {
                if ($_POST['rujuk'] == 'lab') {
                    redirect('Permintaan/lab/'.$id, 'refresh');
                }
                else if ($_POST['rujuk'] == 'radio') {
                    redirect('Permintaan/radio/'.$id, 'refresh');
                }
                else if ($_POST['rujuk'] == 'poli') {
                    $this->insertRujukan($id);
                }
            }
            else if (isset($_POST['input_resep']) && $_POST['input_resep']) {
                redirect('apotek/input_resep/' . $id . '/pemeriksaan');
            }
            else {
                $pemeriksaan = $this->db->query("SELECT * FROM pemeriksaan WHERE id = $id")->row();
                $ant = $this->db->query("SELECT * FROM antrian WHERE pendaftaran_id = $pemeriksaan->pendaftaran_id")->row();
                $this->MainModel->update('antrian', ['is_done' => 1], $ant->id);

                $task_body = ['kodebooking' => $ant->kode_booking ?? '', 'taskid' => 5, 'waktu' => time().'000'];
                $this->MainModel->insert('bpjs_jknmobile_log', [
                    'url' => 'antrean/updatewaktu',
                    'header' => 'task 5',
                    'request' => json_encode($task_body),
                    'response' => json_encode($this->post("{$this->base_url_antrean}antrean/updatewaktu", json_encode($task_body), true)),
                ]);

                if (!$this->db->query("SELECT * FROM antrian_farmasi WHERE pemeriksaan_id = $pemeriksaan->id")->row()) {
                    $today = date('Y-m-d');
                    $last = $this->db->query("SELECT * FROM antrian_farmasi WHERE due_date = '$today' ORDER BY id DESC")->row();
                    $next_no_antrian = $last ? 'FAR-' . (1 + ((int)(preg_replace("/[^0-9]/", "", $last->no_antrian)))) : 'FAR-1';
                    $this->MainModel->insert('antrian_farmasi', [
                        'no_antrian' => $next_no_antrian,
                        'due_date' => $today,
                        'pemeriksaan_id' => $pemeriksaan->id,
                        'pendaftaran_id' => $pemeriksaan->pendaftaran_id,
                    ]);
                }

                $this->session->set_flashdata('success', 'Pemeriksaan pasien berhasil!');
                redirect('pemeriksaan/listPasienSelesaiPeriksa');
            }
        } else {
            $data['pendaftaran'] = $this->PemeriksaanModel->getPendaftaranByIdPemeriksaan($id)->row_array();
            $data['pemeriksaan'] = $this->PemeriksaanModel->getPemeriksaanById($id)->row_array();
            $data['images'] = $this->db->query("SELECT * FROM pemeriksaan_image WHERE pemeriksaan_id = $id ORDER BY id")->result();
            $data['jaminan'] = $this->config->item('pendaftaran');
            $data['hasil_penunjang'] = json_decode($data['pemeriksaan']['hasil_penunjang']);
            $data['jenis_pendaftaran'] = $this->PendaftaranModel->getJenisPendaftaran();
            $data['klinik'] = $this->AdministrasiModel->getKlinik()->row();
            $data['pasien'] = $this->db->query("SELECT * FROM pasien WHERE id = {$data['pemeriksaan']['pasien_id']}")->row();

            $data['obat'] = $this->PemeriksaanModel->getObat();
            $data['obat1'] = $this->db->get_where('obat', ['is_active' => 1, 'stok_obat >' => 0])->result();
            $data['bahan'] = $this->db->get_where('bahan_habis_pakai', ['is_active' => 1, 'jumlah >' => 0])->result();
            $data['bahans'] = $this->AdministrasiModel->getBahanHabisPakaiPemeriksaanById($data['pemeriksaan']['id'])->result();

            $category = 'umum';
            foreach ($this->config->item('poli') as $k => $v) {
                if (in_array($data['pendaftaran']['kode_daftar'], $v['kode'])) {
                    $category = $k;
                    break;
                }
            }

            $data['s_tindakan'] = $this->LaporanModel->getTindakanByIdPemeriksaan($id)->result();
            $data['s_penyakit'] = $this->LaporanModel->getPenyakitPemeriksaanByIdPemeriksaan($id)->result();
            $data['tindakan'] = $this->PemeriksaanModel->getTindakanByCategory($category);
            $data['penyakit'] = $this->PemeriksaanModel->getPenyakitByCategory($category);
            $data['listDokter'] = $this->DokterModel->listDokter();
            $data['listPerawat'] = $this->PerawatModel->listPerawat();

            $pemeriksaan = $this->db->query("SELECT * FROM pemeriksaan WHERE id = $id")->row();
            $ant = $this->db->query("SELECT * FROM antrian WHERE pendaftaran_id = $pemeriksaan->pendaftaran_id")->row();
            $cek = $this->db->query("SELECT * FROM bpjs_jknmobile_log WHERE header = 'task 4' AND request like '%$ant->kode_booking%'")->row();
            if (!$cek && !$data['pendaftaran']['pendaftaran_id']) {
                $task_body = ['kodebooking' => $ant->kode_booking ?? '', 'taskid' => 4, 'waktu' => time().'000'];
                $this->MainModel->insert('bpjs_jknmobile_log', [
                    'url' => 'antrean/updatewaktu',
                    'header' => 'task 4',
                    'request' => json_encode($task_body),
                    'response' => json_encode($this->post("{$this->base_url_antrean}antrean/updatewaktu", json_encode($task_body), true)),
                ]);
            }
            $this->MainModel->update('antrian', ['is_called' => true], $ant->id);

            $this->template->view('pemeriksaan/pemeriksaan_v', $data);
        }
    }

    private function insertRujukan($pemeriksaan_id)
    {
        $session = $this->session->userdata('logged_in');
        $pasien_id = $this->input->post('pasien_id');
        $pendaftaran_id = $this->input->post('pendaftaran_id');
        $pendaftaran = $this->db->query("SELECT * FROM pendaftaran_pasien WHERE id = $pendaftaran_id")->row();
        $pem = $this->db->query("SELECT * FROM pemeriksaan WHERE id = $pemeriksaan_id")->row();

        $rm = array(
            'pendaftaran_id' => $pendaftaran_id,
            'td' => $pem->td,
            'r' => $pem->r,
            'bb' => $pem->bb,
            'n' => $pem->n,
            's' => $pem->s,
            'tb' => $pem->tb,
            'bmi' => $pem->bmi,
            'no_rm' => $pendaftaran->no_rm,
            'pasien' => $pendaftaran->pasien,
            'penanggungjawab' => $pendaftaran->penanggungjawab,
            'biopsikososial' => $pendaftaran->biopsikososial,
            'jaminan' => $pendaftaran->jaminan,
            'no_jaminan' => $pendaftaran->no_jaminan,
            'jenis_pendaftaran_id' => $this->input->post('poli-rujuk'),
            'status' => 'antri',
            'is_bpjs' => $pendaftaran->is_bpjs,
            'creator' => $session->id
        );

        $insert = $this->MainModel->insert_id('pendaftaran_pasien', $rm);

        $id_antrian = $this->MainModel->insert('antrian', [
            'pendaftaran_id' => $insert,
            'pemeriksaan_id' => null,
            'jenis_pendaftaran_id' => $this->input->post('poli-rujuk'),
            'pasien_id' => $pasien_id,
            'due_date' => date('Y-m-d'),
            'kode_antrian' => generateKodeAntrian($this->input->post('poli-rujuk'), date('Y-m-d')),
            'is_check_in' => true,
            'check_in_at' => date('Y-m-d H:i'),
            'kode_booking' => generateKodeBooking()
        ]);

        if ($insert)
            $this->session->set_flashdata('success', 'Rujuk pasien berhasil!');
        else
            $this->session->set_flashdata('warning', 'Rujuk pasien gagal!');

        if ($this->input->post('from_igd'))
            redirect('Igd/periksa/'.$pemeriksaan_id, 'refresh');
        else
            redirect('pemeriksaan/periksa/'.$pemeriksaan_id, 'refresh');
    }

    private function insertRujukanInternalBpjs($pemeriksaan_id)
    {
        $pendaftaran_id = $this->input->post('pendaftaran_id');
        $pendaftaran = $this->db->query("SELECT * FROM pendaftaran_pasien WHERE id = $pendaftaran_id")->row();
        $pem = $this->db->query("SELECT * FROM pemeriksaan WHERE id = $pemeriksaan_id")->row();
        $pasien = $this->db->query("SELECT * FROM pasien WHERE id = $pendaftaran->pasien")->row();

        $jns = $this->db->query("SELECT * FROM jenis_pendaftaran WHERE id = {$pendaftaran->jenis_pendaftaran_id}")->row();
        $dok = $this->db->query("SELECT * FROM user WHERE id = {$pendaftaran->dokter}")->row();
        $ant = $this->db->query("SELECT * FROM antrian WHERE pendaftaran_id = $pendaftaran_id")->row();
        $due_date = date('Y-m-d');
        $total_antrian_jkn = $this->db->query("SELECT * FROM antrian JOIN pendaftaran_pasien pp ON pp.id = antrian.pendaftaran_id WHERE antrian.jenis_pendaftaran_id = $jns->id AND due_date = '$due_date' AND pp.jaminan = 'bpjs'")->result();
        $total_antrian_non_jkn = $this->db->query("SELECT * FROM antrian JOIN pendaftaran_pasien pp ON pp.id = antrian.pendaftaran_id WHERE antrian.jenis_pendaftaran_id = $jns->id AND due_date = '$due_date' AND pp.jaminan != 'bpjs'")->result();
        $sisa_antrian = $this->db->query("SELECT * FROM antrian WHERE jenis_pendaftaran_id = $jns->id AND due_date = '$due_date' AND is_called = 0")->result();
        $c = count($sisa_antrian);
        $b = ($c - 1) * 15;
        $t = strtotime("+$b minutes");
        $body = [
            "kodebooking" => $ant->kode_booking,
            "jenispasien" => $pendaftaran->jaminan == 'bpjs' ? 'JKN' : 'NON JKN',
            "nomorkartu" => $pendaftaran->no_jaminan ?? '',
            'nik' => $pasien->nik,
            'nohp' => $pasien->telepon,
            'kodepoli' => $jns->kode_bpjs ? explode(',', $jns->kode_bpjs)[0] : '',
            "namapoli" => $jns->nama,
            "pasienbaru" => 0,
            "norm" => $pasien->no_rm,
            "tanggalperiksa" => date('Y-m-d'),
            "kodedokter" => (int)($dok->bpjs_id ?? '0'),
            "namadokter" => $dok->nama,
            "jampraktek" => '08:00-16:00',
            "jeniskunjungan" => 2,
            "nomorreferensi" => $this->generateRandomString(),
            "nomorantrean" => $ant->kode_antrian,
            "angkaantrean" => (int) preg_replace("/[^0-9]/", "", $ant->kode_antrian),
            "estimasidilayani" => $t,
            "sisakuotajkn" => $jns->kuota_jkn - count($total_antrian_jkn),
            "kuotajkn" => (int) $jns->kuota_jkn,
            "sisakuotanonjkn" => $jns->kuota_non_jkn - count($total_antrian_non_jkn),
            "kuotanonjkn" => (int) $jns->kuota_non_jkn,
            "keterangan" => "Peserta harap 60 menit lebih awal guna pencatatan administrasi.",
        ];
        $res = $this->post("{$this->base_url_antrean}antrean/add", json_encode($body), true);
        $this->MainModel->insert('bpjs_jknmobile_log', [
            'url' => 'antrean/add',
            'header' => 'pend-lama',
            'request' => json_encode($body),
            'response' => json_encode($res),
            'ws_bpjs_request' => '',
            'ws_bpjs_response' => '',
        ]);

        $task_body = ['kodebooking' => $ant->kode_booking, 'taskid' => 3, 'waktu' => strtotime($pendaftaran->created_at).'000'];
        $this->MainModel->insert('bpjs_jknmobile_log', [
            'url' => 'antrean/updatewaktu',
            'header' => 'task 3',
            'request' => json_encode($task_body),
            'response' => json_encode($this->post("{$this->base_url_antrean}antrean/updatewaktu", json_encode($task_body), true)),
        ]);

        $task_body = ['kodebooking' => $ant->kode_booking, 'taskid' => 4, 'waktu' => strtotime($pem->created_at).'000'];
        $this->MainModel->insert('bpjs_jknmobile_log', [
            'url' => 'antrean/updatewaktu',
            'header' => 'task 4',
            'request' => json_encode($task_body),
            'response' => json_encode($this->post("{$this->base_url_antrean}antrean/updatewaktu", json_encode($task_body), true)),
        ]);

        $task_body = ['kodebooking' => $ant->kode_booking, 'taskid' => 5, 'waktu' => strtotime($pem->updated_at).'000'];
        $this->MainModel->insert('bpjs_jknmobile_log', [
            'url' => 'antrean/updatewaktu',
            'header' => 'task 5',
            'request' => json_encode($task_body),
            'response' => json_encode($this->post("{$this->base_url_antrean}antrean/updatewaktu", json_encode($task_body), true)),
        ]);
    }

    private function generateRandomString($length = 19)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[random_int(0, $charactersLength - 1)];
        }
        return $randomString;
    }


    public function hapus($id, $pendaftaran_id)
    {
        $ant = $this->db->query("SELECT * FROM antrian WHERE pendaftaran_id = $id")->row();
        if ($ant) {
            $task_body = ['kodebooking' => $ant->kode_booking ?? '', 'taskid' => 99, 'waktu' => time().'000'];
            $this->MainModel->insert('bpjs_jknmobile_log', [
                'url' => 'antrean/updatewaktu',
                'header' => 'task 99',
                'request' => json_encode($task_body),
                'response' => json_encode($this->post("{$this->base_url_antrean}antrean/updatewaktu", json_encode($task_body), true)),
            ]);

            $body = [
                'kodebooking' => $ant->kode_booking,
                'keterangan' => 'Dibatalkan petugas'
            ];
            $res = $this->post("{$this->base_url_antrean}antrean/batal", json_encode($body), true);
            $this->MainModel->insert('bpjs_jknmobile_log', [
                'url' => 'antrean/batal',
                'header' => '',
                'request' => json_encode($body),
                'response' => json_encode($res),
                'ws_bpjs_request' => '',
                'ws_bpjs_response' => '',
            ]);
        }

        $this->db->query("DELETE FROM antrian WHERE pendaftaran_id = $pendaftaran_id");
        $this->MainModel->delete('pemeriksaan', ['is_active' => 0], $id);
        $this->MainModel->delete('pendaftaran_pasien', ['is_active' => 0], $pendaftaran_id);

        $this->session->set_flashdata('success', 'Berhasil menghapus pemeriksaan');
        redirect('pemeriksaan/listpemeriksaanPasien', 'refresh');
    }

    function base64_to_jpeg($base64_string, $output_file) {
        $ifp = fopen( $output_file, 'wb' );
        $data = explode( ',', $base64_string );
        fwrite( $ifp, base64_decode( $data[ 1 ] ) );
        fclose( $ifp );
        return $output_file;
    }

    public function upload_image($id)
    {
        $get_ext = function () {
            $s = explode(',', $this->input->post('img'));
            $s = explode('/', $s[0]);
            $s = explode(';', $s[1]);
            return $s[0];
        };

        $time = time();
        $ext = $get_ext();
        $file_name = "pemeriksaan-$id-$time.$ext";
        $this->base64_to_jpeg($this->input->post('img'), FCPATH . 'assets/img/hasil_pemeriksaan/' . $file_name);
        $this->MainModel->insert('pemeriksaan_image', [
            'pemeriksaan_id' => $id,
            'image' => $file_name
        ]);

        echo json_encode([
            'success' => true,
            'data' => $this->db->query("SELECT * FROM pemeriksaan_image WHERE pemeriksaan_id = $id ORDER BY id")->result()
        ]);
    }

    public function delete_image($pemeriksaan_id, $id, $file_name)
    {
        $this->MainModel->hardDelete('pemeriksaan_image', $id);
        unlink(FCPATH . 'assets/img/hasil_pemeriksaan/' . $file_name);
        echo json_encode([
            'success' => true,
            'data' => $this->db->query("SELECT * FROM pemeriksaan_image WHERE pemeriksaan_id = $pemeriksaan_id ORDER BY id")->result()
        ]);
    }
}
