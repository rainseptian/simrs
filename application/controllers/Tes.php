<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tes extends MY_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->library('template');
        $this->load->Model('MainModel');
    }

    function index() {

        $this->template->view('template/test');
    }

    function q()
    {
        $p = $this->db->query("
            select p.id as pemeriksaan_id, b.id as bayar_id, td.id as tindakan_id, td.nama as nama_tindakan, td.tarif_pasien as harga, pp.jenis_pendaftaran_id, p.created_at, p.status
            from pemeriksaan p
            JOIN pendaftaran_pasien pp ON p.pendaftaran_id = pp.id
            JOIN bayar b ON b.pemeriksaan_id = p.id
            JOIN detail_tindakan_pemeriksaan_lab dtp on dtp.pemeriksaan_id = p.id
            JOIN jenis_layanan_laboratorium td on td.id = dtp.jenis_layanan_id and td.is_active = 1
            where 1
            and p.created_at > '2021-09-25 19:56:31'
            AND p.status = 'selesai'
            AND pp.jenis_pendaftaran_id = '19'
            order by p.created_at
        ")->result();

        foreach ($p as $v) {
            $this->MainModel->insert('detail_bayar', [
                'bayar_id' => $v->bayar_id,
                'item_id' => $v->tindakan_id,
                'item' => $v->nama_tindakan,
                'jenis_item' => 'tindakan',
                'jumlah' => 1,
                'harga' => $v->harga,
                'subtotal' => $v->harga,
                'creator' => 1,
            ]);
        }
        die('oxe');
    }
}

//class A {
//
//    var $f;
//    var $v = array(
//        array("p" => "p1", "q" => "q1"),
//        array("p" => "p2", "q" => "q2")
//    );
//
//    function __construct() {
////        $this->f = function ($v) {
////            return $v;
////        };
//    }
//
//    function b($f) {
//        $this->f = $f;
//    }
//
//    function c() {
//        foreach ($this->v as $u) {
//            echo is_array($u) ? 'array' : 'no';
//            echo '<br>';
//            echo call_user_func($this->f, $u);
//        }
//    }
//}
//
//
//defined('BASEPATH') or exit('No direct script access allowed');
//
//class RawatInapModel extends CI_Model
//{
//    public function getBaru()
//    {
//        return $this->db
//            ->select('
//                r.*, p.nama as nama_pasien, p.no_rm, p.alamat, u.nama as nama_dokter, jp.nama as nama_poli,
//                bed.name as bed_name, bed_type.name as bed_type_name,floor.name as floor_name, bed_group.name as bedgroup
//            ')
//            ->from('rawat_inap r')
//            ->join('pasien p', 'p.id = r.pasien_id')
//            ->join('user u', 'u.id = r.dokter_id')
//            ->join('jenis_pendaftaran jp', 'jp.id = r.dari', 'left')
//            ->join('bed', 'bed.id = r.tujuan')
//            ->join('bed_type', 'bed.bed_type_id = bed_type.id')
//            ->join('bed_group', 'bed.bed_group_id = bed_group.id')
//            ->join('floor', 'floor.id = bed_group.floor')
//            ->where('r.is_active', '1')
//            ->where('r.status', 'Masuk')
//            ->get()
//            ->result_array();
//    }
//
//    public function getSelesai()
//    {
//        return $this->db
//            ->select('
//                r.*, p.nama as nama_pasien, p.no_rm, p.alamat, u.nama as nama_dokter, jp.nama as nama_poli,
//                bed.name as bed_name, bed_type.name as bed_type_name,floor.name as floor_name, bed_group.name as bedgroup
//            ')
//            ->from('rawat_inap r')
//            ->join('pasien p', 'p.id = r.pasien_id')
//            ->join('user u', 'u.id = r.dokter_id')
//            ->join('jenis_pendaftaran jp', 'jp.id = r.dari', 'left')
//            ->join('bed', 'bed.id = r.tujuan')
//            ->join('bed_type', 'bed.bed_type_id = bed_type.id')
//            ->join('bed_group', 'bed.bed_group_id = bed_group.id')
//            ->join('floor', 'floor.id = bed_group.floor')
//            ->where('r.is_active', '1')
//            ->where('r.status', 'Selesai')
//            ->get()
//            ->result_array();
//    }
//
//    public function getBolehPulang()
//    {
//        return $this->db
//            ->select('
//                r.*, p.nama as nama_pasien, p.no_rm, p.alamat, u.nama as nama_dokter, jp.nama as nama_poli,
//                bed.name as bed_name, bed_type.name as bed_type_name,floor.name as floor_name, bed_group.name as bedgroup
//            ')
//            ->from('rawat_inap r')
//            ->join('pasien p', 'p.id = r.pasien_id')
//            ->join('user u', 'u.id = r.dokter_id')
//            ->join('jenis_pendaftaran jp', 'jp.id = r.dari', 'left')
//            ->join('bed', 'bed.id = r.tujuan')
//            ->join('bed_type', 'bed.bed_type_id = bed_type.id')
//            ->join('bed_group', 'bed.bed_group_id = bed_group.id')
//            ->join('floor', 'floor.id = bed_group.floor')
//            ->where('r.is_active', '1')
//            ->where('r.status', 'Boleh Pulang')
//            ->get()
//            ->result_array();
//    }
//
//    public function getForBilling()
//    {
//        return $this->db
//            ->select('
//                r.*, p.nama as nama_pasien, p.no_rm, p.alamat, u.nama as nama_dokter, jp.nama as nama_poli,
//                bed.name as bed_name, bed_type.name as bed_type_name,floor.name as floor_name, bed_group.name as bedgroup
//            ')
//            ->from('rawat_inap r')
//            ->join('pasien p', 'p.id = r.pasien_id')
//            ->join('user u', 'u.id = r.dokter_id')
//            ->join('jenis_pendaftaran jp', 'jp.id = r.dari', 'left')
//            ->join('bed', 'bed.id = r.tujuan')
//            ->join('bed_type', 'bed.bed_type_id = bed_type.id')
//            ->join('bed_group', 'bed.bed_group_id = bed_group.id')
//            ->join('floor', 'floor.id = bed_group.floor')
//            ->where('r.is_active', '1')
//            ->where("(r.status = 'Boleh Pulang'", null, false)
//            ->or_where("r.status = 'Sudah Bayar')", null, false)
//            ->get()
//            ->result_array();
//    }
//
//    public function getById($id)
//    {
//        return $this->db
//            ->select('
//                r.*, p.nama as nama_pasien, p.no_rm, p.alamat, u.id as dokter_id, u.nama as nama_dokter, jp.nama as nama_poli,
//                bed.name as bed_name, bed_type.name as bed_type_name,floor.name as floor_name, bed_group.name as bedgroup,
//                bed_group.price as bedgroup_price, bed.id as bed_id, t.catatan
//            ')
//            ->from('rawat_inap r')
//            ->join('transfer t', 't.id = r.transfer_id')
//            ->join('pasien p', 'p.id = r.pasien_id')
//            ->join('user u', 'u.id = r.dokter_id')
//            ->join('jenis_pendaftaran jp', 'jp.id = r.dari', 'left')
//            ->join('bed', 'bed.id = r.tujuan')
//            ->join('bed_type', 'bed.bed_type_id = bed_type.id')
//            ->join('bed_group', 'bed.bed_group_id = bed_group.id')
//            ->join('floor', 'floor.id = bed_group.floor')
//            ->where('r.is_active', '1')
//            ->where('r.id', $id)
//            ->get()
//            ->row();
//    }
//
//    public function getPasienById($id)
//    {
//        return $this->db->select('*')->from('pasien')->where('id', $id)->get()->row();
//    }
//
//    public function getPemeriksaanByTransferId($transfer_id, $jenis_ruangan_id)
//    {
//        return $this->db->select('p.*, u.nama as nama_dokter')
//            ->from('ri_pemeriksaan p')
//            ->join('user u', 'u.id = p.dokter_id')
//            ->where('p.transfer_id', $transfer_id)
//            ->where('p.jenis_ruangan_id', $jenis_ruangan_id)
//            ->get()
//            ->result();
//    }
//
//    public function getDiagnosisByTransferId($transfer_id, $jenis_ruangan_id)
//    {
//        return $this->db
//            ->where('r.transfer_id', $transfer_id)
//            ->where('r.jenis_ruangan_id', $jenis_ruangan_id)
//            ->get('ri_diagnosis r')
//            ->result();
//    }
//
//    public function getPenyakitByDiagnosisId($id)
//    {
//        return $this->db
//            ->where('dp.ri_diagnosis_id', $id)
//            ->join('penyakit p', 'p.id = dp.penyakit_id')
//            ->get('ri_diagnosis_penyakit dp')
//            ->result();
//    }
//
//    public function getResepByTransferId($transfer_id, $jenis_ruangan_id)
//    {
//        return $this->db
//            ->where('transfer_id', $transfer_id)
//            ->where('jenis_ruangan_id', $jenis_ruangan_id)
//            ->get('ri_resep')
//            ->result();
//    }
//
//    public function getBhpByTransferId($transfer_id, $jenis_ruangan_id)
//    {
//        return $this->db
//            ->where('transfer_id', $transfer_id)
//            ->where('jenis_ruangan_id', $jenis_ruangan_id)
//            ->get('ri_bahan_habis_pakai')
//            ->result();
//    }
//
//    public function getBiayaByTransferId($transfer_id, $jenis_ruangan_id)
//    {
//        return $this->db
//            ->where('transfer_id', $transfer_id)
//            ->where('jenis_ruangan_id', $jenis_ruangan_id)
//            ->get('ri_biaya')
//            ->result();
//    }
//
//    public function getObatByResepId($id)
//    {
//        return $this->db
//            ->select('ro.id, o.nama, o.harga_jual, ro.jumlah, ro.signa')
//            ->join('obat o', 'o.id = ro.obat_id')
//            ->where('ro.ri_resep_id', $id)
//            ->get('ri_resep_obat ro')
//            ->result();
//    }
//
//    public function getBahanByBhpId($id)
//    {
//        return $this->db
//            ->select('bhp.nama, bhpd.jumlah, bhp.harga_jual')
//            ->join('bahan_habis_pakai bhp', 'bhp.id = bhpd.bahan_id')
//            ->where('bhpd.ri_bahan_habis_pakai_id', $id)
//            ->get('ri_bahan_habis_pakai_detail bhpd')
//            ->result();
//    }
//
//    public function getObatRacikByResepId($id)
//    {
//        return $this->db
//            ->select('ror.id, ror.nama_racikan, ror.signa, ror.catatan')
//            ->where('ror.ri_resep_id', $id)
//            ->get('ri_resep_obat_racik ror')
//            ->result();
//    }
//
//    public function getObatRacikDetailByResepObatRacikId($id)
//    {
//        return $this->db
//            ->select('o.nama, o.harga_jual, rord.jumlah')
//            ->join('obat o', 'o.id = rord.obat_id')
//            ->where('rord.ri_resep_obat_racik_id', $id)
//            ->get('ri_resep_obat_racik_detail rord')
//            ->result();
//    }
//
//    public function getAllPenyakit()
//    {
//        return $this->db->where('is_active', '1')->get('penyakit')->result();
//    }
//
//    public function getAllTindakan()
//    {
//        return $this->db->where('is_active', '1')->get('tarif_tindakan')->result();
//    }
//
//    public function getAllTipeMakanan()
//    {
//        return $this->db->where('is_active', '1')->get('tipe_makanan')->result();
//    }
//
//    public function getMakananById($id)
//    {
//        return $this->db->where('is_active', '1')->where('id', $id)->get('makanan')->row();
//    }
//
//    public function getMakananByTipeId($id)
//    {
//        return $this->db->where('is_active', '1')
//            ->where('tipe_makanan_id', $id)
//            ->get('makanan')
//            ->result();
//    }
//
//    public function getMakananCountByTransferId($transfer_id, $jenis_ruangan_id)
//    {
//        return $this->db
//            ->where('b.jenis_biaya', 'Makanan')
//            ->where('b.transfer_id', $transfer_id)
//            ->where('b.jenis_ruangan_id', $jenis_ruangan_id)
//            ->group_by('m.id')
//            ->join('makanan m', 'm.id = b.makanan_id')
//            ->select('m.nama, m.harga, count(m.id) as jumlah, (count(m.id) * m.harga) as total')
//            ->get('ri_biaya b')
//            ->result();
//    }
//
//    public function getTindakanHargaByIds($id)
//    {
//        return $this->db->where('is_active', '1')
//            ->where_in('id', $id)
//            ->select('tarif_pasien')
//            ->get('tarif_tindakan')
//            ->result();
//    }
//
//    public function getTindakanByBiayaId($id)
//    {
//        return $this->db
//            ->where('bt.ri_biaya_id', $id)
//            ->select('tt.nama, tt.tarif_pasien')
//            ->join('tarif_tindakan tt', 'tt.id = bt.tindakan_id')
//            ->get('ri_biaya_tindakan bt')
//            ->result();
//    }
//
//    public function getBayarByTransferId($transfer_id, $jenis_ruangan_id)
//    {
//        return $this->db
//            ->where('b.transfer_id', $transfer_id)
//            ->where('b.jenis_ruangan_id', $jenis_ruangan_id)
//            ->select('b.*')
//            ->get('ri_bayar b')
//            ->row();
//    }
//
//    public function getBayarById($id)
//    {
//        return $this->db
//            ->where('b.id', $id)
//            ->select('b.*')
//            ->get('ri_bayar b')
//            ->row();
//    }
//
//    public function getTransferById($id)
//    {
//        return $this->db
//            ->where('id', $id)
//            ->get('transfer')
//            ->row();
//    }
//
//    public function getLastTransferByRawatInapId($id)
//    {
//        return $this->db
//            ->limit(1)
//            ->order_by('t.id', 'desc')
//            ->where('ri.id', $id)
//            ->join('transfer t', 't.id = ri.transfer_id')
//            ->select('t.*')
//            ->get('rawat_inap ri')
//            ->row();
//    }
//}
