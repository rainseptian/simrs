<?php


class Igd extends MY_BpjsController
{
    function __construct()
    {
        parent::__construct();
        $this->load->library('template');
        $this->load->Model('MainModel');
        $this->load->Model('IgdModel');
        $this->load->Model('PendaftaranModel');
        $this->load->Model('PemeriksaanModel');
        $this->load->Model('LaporanModel');
        $this->load->Model('PasienModel');
        $this->load->Model('DetailPenyakitPemeriksaanModel');
        $this->load->Model('DetailTindakanPemeriksaanModel');
        $this->load->Model('PerawatModel');
        $this->load->Model('ObatModel');
        $this->load->helper(['kode_booking', 'usia']);
    }

    public function pendaftaran()
    {
        $data['listPendaftaran'] = $this->IgdModel->getListPasienAntri();
        $data['jaminan'] = $this->config->item('pendaftaran');

        $this->template->view('igd/list_pendaftaran', $data);
    }

    public function daftarPasienBaru()
    {
        if ($this->input->post('submit') == 1) {
            $session = $this->session->userdata('logged_in');
            $id_jenis_pendaftaran = $this->input->post('jenis_pendaftaran');
            $data['jenis_pendaftaran'] = $this->PendaftaranModel->getJenisPendaftaran($id_jenis_pendaftaran)->row();

            $pasien = array(
                'no_rm' => $this->input->post('no_rm'),
                "no_bpjs" => $this->input->post('no_jaminan') ?? '',
                'nama' => $this->input->post('nama'),
                'tanggal_lahir' => $this->input->post('tanggal_lahir'),
                'usia' => get_usia($this->input->post('tanggal_lahir')),
                'tempat_lahir' => $this->input->post('tempat_lahir'),
                'jk' => $this->input->post('jenis_kelamin'),
                'alamat' => $this->input->post('alamat'),
                'telepon' => $this->input->post('telepon'),
                'pekerjaan' => $this->input->post('pekerjaan'),
                'agama' => $this->input->post('agama'),
                'tingkat_pendidikan' => $this->input->post('tingkat_pendidikan'),
                'penanggungjawab' => $this->input->post('penanggungjawab'),
                'creator' => $session->id
            );

            $insert_id = $this->MainModel->insert_id($tabel = 'pasien', $pasien);

            $rm = array(
                'td' => $this->input->post('td'),
                'r' => $this->input->post('r'),
                'bb' => $this->input->post('bb'),
                'n' => $this->input->post('n'),
                's' => $this->input->post('s'),
                'tb' => $this->input->post('tb'),
                'bmi' => $this->input->post('bmi'),
                'no_rm' => $this->input->post('no_rm'),
                'pasien' => $insert_id,
                'penanggungjawab' => $this->input->post('penanggungjawab'),
                'biopsikososial' => $this->input->post('biopsikososial'),
                'jenis_pendaftaran_id' => $this->input->post('jenis_pendaftaran'),
                'jaminan' => $this->input->post('jaminan'),
                'no_jaminan' => $this->input->post('no_jaminan'),
                'dokter' => $this->input->post('dokter'),
                'status' => 'antri',
                'is_bpjs' => (isset($_POST['pasien_bpjs']) ? 1 : 0),
                'creator' => $session->id
            );
            $insert_id2 = $this->MainModel->insert_id($tabel = 'pendaftaran_pasien', $rm);

            $periksa = array(
                'pendaftaran_id' => $insert_id2,
                'dokter_id' => $this->input->post('dokter'),
                'pasien_id' => $this->input->post('id'),
                'perawat_id' => '',
                'no_rm' => $this->input->post('no_rm'),
                'nama_pasien' => $this->input->post('nama'),
                'keluhan_utama' => '',
                'diagnosa_perawat' => '',
                'asuhan_keperawatan' => '',
                'bmi' => '',
                'td' => '',
                'r' => '',
                'bb' => '',
                'n' => '',
                's' => '',
                'tb' => '',
                'is_bpjs' => 0,
                'jaminan' => $this->input->post('jaminan'),
                'status' => 'belum',
                'creator' => $this->session->userdata('logged_in')->id
            );
            $periksa_id = $this->MainModel->insert_id($tabel = 'pemeriksaan', $periksa);

            $kode_booking = generateKodeBooking();
            $id_antrian = $this->MainModel->insert_id('antrian', [
                'pendaftaran_id' => $insert_id2,
                'pemeriksaan_id' => $periksa_id,
                'jenis_pendaftaran_id' => $this->input->post('jenis_pendaftaran'),
                'pasien_id' => $insert_id,
                'due_date' => date('Y-m-d'),
                'kode_antrian' => generateKodeAntrian($this->input->post('jenis_pendaftaran'), date('Y-m-d')),
                'is_check_in' => true,
                'check_in_at' => date('Y-m-d H:i'),
                'kode_booking' => $kode_booking
            ]);

            $jns = $this->db->query("SELECT * FROM jenis_pendaftaran WHERE id = {$this->input->post('jenis_pendaftaran')}")->row();
            $dok = $this->db->query("SELECT * FROM user WHERE id = {$this->input->post('dokter')}")->row();
            $ant = $this->db->query("SELECT * FROM antrian WHERE id = $id_antrian")->row();
            $due_date = date('Y-m-d');
            $total_antrian_jkn = $this->db->query("SELECT * FROM antrian JOIN pendaftaran_pasien pp ON pp.id = antrian.pendaftaran_id WHERE antrian.jenis_pendaftaran_id = $jns->id AND due_date = '$due_date' AND pp.jaminan = 'bpjs'")->result();
            $total_antrian_non_jkn = $this->db->query("SELECT * FROM antrian JOIN pendaftaran_pasien pp ON pp.id = antrian.pendaftaran_id WHERE antrian.jenis_pendaftaran_id = $jns->id AND due_date = '$due_date' AND pp.jaminan != 'bpjs'")->result();
            $sisa_antrian = $this->db->query("SELECT * FROM antrian WHERE jenis_pendaftaran_id = $jns->id AND due_date = '$due_date' AND is_called = 0")->result();
            $c = count($sisa_antrian);
            $b = ($c - 1) * 15;
            $t = strtotime("+$b minutes");
            $body = [
                "kodebooking" => $kode_booking,
                "jenispasien" => $this->input->post('jaminan') == 'bpjs' ? 'JKN' : 'NON JKN',
                "nomorkartu" => $this->input->post('no_jaminan') ?? '',
                'nik' => $this->input->post('nik'),
                'nohp' => $this->input->post('telepon'),
                'kodepoli' => $jns->kode_bpjs ? explode(',', $jns->kode_bpjs)[0] : '',
                "namapoli" => $jns->nama,
                "pasienbaru" => 1,
                "norm" => $this->input->post('no_rm'),
                "tanggalperiksa" => date('Y-m-d'),
                "kodedokter" => (int)($dok->bpjs_id ?? '0'),
                "namadokter" => $dok->nama,
                "jampraktek" => '08:00-16:00',
                "jeniskunjungan" => 1,
                "nomorreferensi" => $this->input->post('nomor_rujukan') ?? '',
                "nomorantrean" => $ant->kode_antrian,
                "angkaantrean" => (int)preg_replace("/[^0-9]/", "", $ant->kode_antrian),
                "estimasidilayani" => $t,
                "sisakuotajkn" => $jns->kuota_jkn - count($total_antrian_jkn),
                "kuotajkn" => (int)$jns->kuota_jkn,
                "sisakuotanonjkn" => $jns->kuota_non_jkn - count($total_antrian_non_jkn),
                "kuotanonjkn" => (int)$jns->kuota_non_jkn,
                "keterangan" => "Peserta harap 60 menit lebih awal guna pencatatan administrasi.",
            ];
            $res = $this->post("{$this->base_url_antrean}antrean/add", json_encode($body), true);
            $this->MainModel->insert('bpjs_jknmobile_log', [
                'url' => 'antrean/add',
                'header' => 'pend-lama',
                'request' => json_encode($body),
                'response' => json_encode($res),
                'ws_bpjs_request' => '',
                'ws_bpjs_response' => '',
            ]);

            $task_body = ['kodebooking' => $kode_booking, 'taskid' => 3, 'waktu' => time() . '000'];
            $this->MainModel->insert('bpjs_jknmobile_log', [
                'url' => 'antrean/updatewaktu',
                'header' => 'task 3',
                'request' => json_encode($task_body),
                'response' => json_encode($this->post("{$this->base_url_antrean}antrean/updatewaktu", json_encode($task_body), true)),
            ]);

            if ($insert_id2) {
                $this->session->set_flashdata('success', 'Pendaftaran pasien berhasil!');
                redirect('Igd/pendaftaran', 'refresh');
            }

        } else {
            $data['no_rm_auto'] = $this->PasienModel->getNoRmAuto();
            $data['jenis_pendaftaran'] = $this->IgdModel->getJenisPendaftaranIgd();
            $data['dokter'] = $this->PendaftaranModel->getDokter();
            $data['jaminan'] = $this->config->item('pendaftaran');
            $this->template->view('igd/pendaftaran_baru', $data);
        }
    }

    public function daftarPasienLama()
    {
        if ($this->input->post('submit') == 1) {
            $session = $this->session->userdata('logged_in');

            $id = $this->input->post('id');
            $user = array(
                'nama' => $this->input->post('nama'),
                "no_bpjs" => $this->input->post('no_jaminan') ?? '',
                'tempat_lahir' => $this->input->post('tempat_lahir'),
                'tanggal_lahir' => $this->input->post('tanggal_lahir'),
                'usia' => get_usia($this->input->post('tanggal_lahir')),
                'jk' => $this->input->post('jenis_kelamin'),
                'alamat' => $this->input->post('alamat'),
                'telepon' => $this->input->post('telepon'),
                'pekerjaan' => $this->input->post('pekerjaan'),
                'agama' => $this->input->post('agama'),
                'tingkat_pendidikan' => $this->input->post('tingkat_pendidikan'),
                'penanggungjawab' => $this->input->post('penanggungjawab'),
                'creator' => $session->id
            );
            $a = $this->MainModel->update($tabel = 'pasien', $user, $id);

            $rm = array(
                'td' => $this->input->post('td'),
                'r' => $this->input->post('r'),
                'bb' => $this->input->post('bb'),
                'n' => $this->input->post('n'),
                's' => $this->input->post('s'),
                'tb' => $this->input->post('tb'),
                'bmi' => $this->input->post('bmi'),
                'no_rm' => $this->input->post('no_rm'),
                'pasien' => $this->input->post('id'),
                'penanggungjawab' => $this->input->post('penanggungjawab'),
                'biopsikososial' => $this->input->post('biopsikososial'),
                'jenis_pendaftaran_id' => $this->input->post('jenis_pendaftaran'),
                'jaminan' => $this->input->post('jaminan'),
                'no_jaminan' => $this->input->post('no_jaminan'),
                'dokter' => $this->input->post('dokter'),
                'status' => 'antri',
                'is_bpjs' => (isset($_POST['pasien_bpjs']) ? 1 : 0),
                'creator' => $session->id
            );
            $insert = $this->MainModel->insert_id($tabel = 'pendaftaran_pasien', $rm);

            $periksa = array(
                'pendaftaran_id' => $insert,
                'dokter_id' => $this->input->post('dokter'),
                'pasien_id' => $this->input->post('id'),
                'perawat_id' => '',
                'no_rm' => $this->input->post('no_rm'),
                'nama_pasien' => $this->input->post('nama'),
                'keluhan_utama' => '',
                'diagnosa_perawat' => '',
                'asuhan_keperawatan' => '',
                'bmi' => '',
                'td' => '',
                'r' => '',
                'bb' => '',
                'n' => '',
                's' => '',
                'tb' => '',
                'is_bpjs' => 0,
                'jaminan' => $this->input->post('jaminan'),
                'status' => 'belum',
                'creator' => $this->session->userdata('logged_in')->id
            );
            $periksa_id = $this->MainModel->insert_id($tabel = 'pemeriksaan', $periksa);

            $kode_booking = generateKodeBooking();
            $id_antrian = $this->MainModel->insert_id('antrian', [
                'pendaftaran_id' => $insert,
                'pemeriksaan_id' => $periksa_id,
                'jenis_pendaftaran_id' => $this->input->post('jenis_pendaftaran'),
                'pasien_id' => $this->input->post('id'),
                'due_date' => date('Y-m-d'),
                'kode_antrian' => generateKodeAntrian($this->input->post('jenis_pendaftaran'), date('Y-m-d')),
                'is_check_in' => true,
                'check_in_at' => date('Y-m-d H:i'),
                'kode_booking' => $kode_booking
            ]);

            $jns = $this->db->query("SELECT * FROM jenis_pendaftaran WHERE id = {$this->input->post('jenis_pendaftaran')}")->row();
            $dok = $this->db->query("SELECT * FROM user WHERE id = {$this->input->post('dokter')}")->row();
            $ant = $this->db->query("SELECT * FROM antrian WHERE id = $id_antrian")->row();
            $due_date = date('Y-m-d');
            $total_antrian_jkn = $this->db->query("SELECT * FROM antrian JOIN pendaftaran_pasien pp ON pp.id = antrian.pendaftaran_id WHERE antrian.jenis_pendaftaran_id = $jns->id AND due_date = '$due_date' AND pp.jaminan = 'bpjs'")->result();
            $total_antrian_non_jkn = $this->db->query("SELECT * FROM antrian JOIN pendaftaran_pasien pp ON pp.id = antrian.pendaftaran_id WHERE antrian.jenis_pendaftaran_id = $jns->id AND due_date = '$due_date' AND pp.jaminan != 'bpjs'")->result();
            $sisa_antrian = $this->db->query("SELECT * FROM antrian WHERE jenis_pendaftaran_id = $jns->id AND due_date = '$due_date' AND is_called = 0")->result();
            $c = count($sisa_antrian);
            $b = ($c - 1) * 15;
            $t = strtotime("+$b minutes");
            $body = [
                "kodebooking" => $kode_booking,
                "jenispasien" => $this->input->post('jaminan') == 'bpjs' ? 'JKN' : 'NON JKN',
                "nomorkartu" => $this->input->post('no_jaminan') ?? '',
                'nik' => $this->input->post('nik'),
                'nohp' => $this->input->post('telepon'),
                'kodepoli' => $jns->kode_bpjs ? explode(',', $jns->kode_bpjs)[0] : '',
                "namapoli" => $jns->nama,
                "pasienbaru" => 0,
                "norm" => $this->input->post('no_rm'),
                "tanggalperiksa" => date('Y-m-d'),
                "kodedokter" => (int)($dok->bpjs_id ?? '0'),
                "namadokter" => $dok->nama,
                "jampraktek" => '08:00-16:00',
                "jeniskunjungan" => 1,
                "nomorreferensi" => $this->input->post('nomor_rujukan') ?? '',
                "nomorantrean" => $ant->kode_antrian,
                "angkaantrean" => (int)preg_replace("/[^0-9]/", "", $ant->kode_antrian),
                "estimasidilayani" => $t,
                "sisakuotajkn" => $jns->kuota_jkn - count($total_antrian_jkn),
                "kuotajkn" => (int)$jns->kuota_jkn,
                "sisakuotanonjkn" => $jns->kuota_non_jkn - count($total_antrian_non_jkn),
                "kuotanonjkn" => (int)$jns->kuota_non_jkn,
                "keterangan" => "Peserta harap 60 menit lebih awal guna pencatatan administrasi.",
            ];
            $res = $this->post("{$this->base_url_antrean}antrean/add", json_encode($body), true);
            $this->MainModel->insert('bpjs_jknmobile_log', [
                'url' => 'antrean/add',
                'header' => 'pend-lama',
                'request' => json_encode($body),
                'response' => json_encode($res),
                'ws_bpjs_request' => '',
                'ws_bpjs_response' => '',
            ]);

            $task_body = ['kodebooking' => $kode_booking, 'taskid' => 3, 'waktu' => time() . '000'];
            $this->MainModel->insert('bpjs_jknmobile_log', [
                'url' => 'antrean/updatewaktu',
                'header' => 'task 3',
                'request' => json_encode($task_body),
                'response' => json_encode($this->post("{$this->base_url_antrean}antrean/updatewaktu", json_encode($task_body), true)),
            ]);

            if ($insert) {
                $this->session->set_flashdata('success', 'Pendaftaran pasien berhasil!');
                redirect('Igd/pendaftaran', 'refresh');
            } else {
                $this->session->set_flashdata('warning', 'Pendaftaran pasien gagal!');
                redirect('Igd/daftarPasienLama', 'refresh');
            }
        } else {
            $id = $this->input->post('id_pasien');

            $data['jenis_pendaftaran'] = $this->IgdModel->getJenisPendaftaranIgd();
            $data['dokter'] = $this->PendaftaranModel->getDokter();
            $data['pasien'] = $this->PendaftaranModel->getPasienById($id)->row();
            $data['pemeriksaan'] = $this->PendaftaranModel->getPemeriksaanPasienById($id)->result();

            $data['tindakan'] = $this->LaporanModel->getTindakanById($id);
            $data['obat'] = $this->LaporanModel->getObatPemeriksaanById($id);
            $data['penyakit'] = $this->LaporanModel->getPenyakitPemeriksaanById($id);
            $data['racikan'] = $this->LaporanModel->getRacikanPemeriksaan();
            $data['pendaftaran'] = $this->PendaftaranModel->getPendaftaranByIdPasien($id)->row();
            $data['jaminan'] = $this->config->item('pendaftaran');

            $this->template->view('igd/pendaftaran_lama', $data);
        }
    }

    public function pemeriksaanAwal()
    {
        $data['listPendaftaran'] = $this->IgdModel->getListPasienAntri();
        $data['jaminan'] = $this->config->item('pendaftaran');

        $this->template->view('igd/list_pemeriksaan_awal', $data);
    }

    public function periksaAwal($id)
    {
        $data['pendaftaran'] = $this->PemeriksaanModel->getPendaftaranById($id)->row_array();
        $data['pemeriksaan'] = $this->db->query("SELECT * FROM pemeriksaan WHERE pendaftaran_id = $id")->row_array();
        $data['form'] = unserialize($data['pemeriksaan']['form']);
        $data['jaminan'] = $this->config->item('pendaftaran');
        $this->template->view('igd/pemeriksaan_awal', $data);
    }

    public function submitPeriksaAwal($id)
    {
        $periksa = array(
            'pendaftaran_id' => $this->input->post('pendaftaran_id'),
            'dokter_id' => $this->input->post('dokter_id'),
            'pasien_id' => $id,
            'perawat_id' => $this->session->userdata('logged_in')->id,
            'no_rm' => $this->input->post('no_rm'),
            'nama_pasien' => $this->input->post('nama_pasien'),
            'keluhan_utama' => $this->input->post('keluhan_utama'),
            'diagnosa_perawat' => $this->input->post('diagnosa_perawat'),
            'asuhan_keperawatan' => $this->input->post('asuhan_keperawatan'),
            'bmi' => $this->input->post('bmi'),
            'td' => $this->input->post('td'),
            'r' => $this->input->post('r'),
            'bb' => $this->input->post('bb'),
            'n' => $this->input->post('n'),
            's' => $this->input->post('s'),
            'tb' => $this->input->post('tb'),
            'spo2' => $this->input->post('spo2'),
            'is_bpjs' => $this->input->post('is_bpjs'),
            'jaminan' => $this->input->post('jaminan'),
            'status' => 'sudah_periksa_awal',
            'sudah_periksa_perawat' => 1,
            'form' => serialize($this->input->post('form')),
            'creator' => $this->session->userdata('logged_in')->id
        );

        $id_pem = $this->input->post('pemeriksaan_id');
        $pem = $this->db->query("SELECT * FROM pemeriksaan WHERE id = $id_pem")->row_array();
        if ($pem->sudah_periksa_dokter) {
            $periksa['status'] = 'sudah_periksa';
            $this->MainModel->update('pendaftaran_pasien', array('status' => 'diperiksa'), $this->input->post('pendaftaran_id'));
        }

        $this->MainModel->update('pemeriksaan', $periksa, $this->input->post('pemeriksaan_id'));

        $this->session->set_flashdata('success', 'Pemeriksaan awal pasien berhasil!');
        redirect('Igd/pemeriksaanAwal', 'refresh');
    }

    public function pemeriksaan()
    {
        $data['listPendaftaran'] = $this->IgdModel->getListPemeriksaanSudahPeriksaAwal();
        $data['jaminan'] = $this->config->item('pendaftaran');

        $this->template->view('igd/list_sudah_periksa_awal', $data);
    }

    public function periksa($id = '')
    {
        $session = $this->session->userdata('logged_in');

        if ($this->input->post('submit') == 1 || $this->input->post('rujuk') || $this->input->post('input_resep')) {

            if (isset($_POST['meta'])) {
                if (isset($_POST['kia_hamil'])) {
                    $meta = $this->input->post('meta');
                    if ($_POST['kia_hamil']) {
                        $meta['jenis'][] = 'hamil';
                    }
                    if ($_POST['kia_kb']) {
                        $meta['jenis'][] = 'kb';
                    }
                    $meta = serialize($meta);
                } else {
                    $meta = serialize($this->input->post('meta'));
                }
            } else {
                $meta = '';
            }

            $hd = isset($_POST['hd']) ? serialize($this->input->post('hd')) : '';

            $hp_lab = $this->input->post('hasil_penunjang_laboratorium');
            $hp_ekg = $this->input->post('hasil_penunjang_ekg');
            $hp_spirometri = $this->input->post('hasil_penunjang_spirometri');

            if ($hp_lab) {
                $hp['laboratorium'] = $hp_lab;
            }
            if ($hp_ekg) {
                $hp['ekg'] = $hp_ekg;
            }
            if ($hp_spirometri) {
                $hp['spirometri'] = $hp_spirometri;
            }

            $input_perawat = $this->input->post('perawat');

            $periksa = array(
                'td' => $this->input->post('td'),
                'r' => $this->input->post('r'),
                'bb' => $this->input->post('bb'),
                'n' => $this->input->post('n'),
                's' => $this->input->post('s'),
                'tb' => $this->input->post('tb'),
                'bmi' => $this->input->post('bmi'),
                'spo2' => $this->input->post('spo2'),
                'detail_perawat_id' => implode(',', $input_perawat) == null ? '' : implode(',', $input_perawat),
                'diagnosa_perawat' => $this->input->post('diagnosa_perawat'),
                'keluhan_utama' => $this->input->post('keluhan_utama'),
                'catatan_odontogram' => $this->input->post('catatan_odontogram'),
                'meta' => $meta,
                'hd' => $hd,
                'amammesia' => $this->input->post('amammesia'),
                'diagnosis' => $this->input->post('diagnosis'),
                'pemeriksaan_fisik' => nl2br($this->input->post('pemeriksaan_fisik')),
                'hasil_penunjang' => isset($hp) ? json_encode($hp) : '',
                'diagnosis_banding' => $this->input->post('diagnosis_banding'),
                'deskripsi_tindakan' => $this->input->post('deskripsi_tindakan'),
                'saran_pemeriksaan' => $this->input->post('saran_pemeriksaan'),
                'creator' => $session->id,
                'form' => serialize($this->input->post('form')),
                'status_pasien' => $this->input->post('status_pasien'),
                'sudah_obat' => 1,
                'sudah_periksa_dokter' => 1,
            );

            $rujuk = isset($_POST['rujuk']) && $_POST['rujuk'];
            $input_resep = isset($_POST['input_resep']) && $_POST['input_resep'];
            $pem = $this->db->query("SELECT * FROM pemeriksaan WHERE id = $id")->row_array();
            if (!$rujuk && !$input_resep && $pem['sudah_periksa_perawat'] == '1') {
                $periksa['status'] = 'sudah_periksa';
                $this->MainModel->update('pendaftaran_pasien', array('status' => 'diperiksa'), $this->input->post('pendaftaran_id'));
            }

            if (isset($_POST['asuhan_keperawatan'])) {
                $periksa['asuhan_keperawatan'] = $this->input->post('asuhan_keperawatan');
            }

            $this->MainModel->update('pemeriksaan', $periksa, $id);

            if ($this->input->post('surat')) {
                $this->MainModel->update('pendaftaran_pasien', [
                    'surat' => $this->input->post('surat'),
                    'surat_form' => json_encode($this->input->post('surat_form')),
                ], $this->input->post('pendaftaran_id'));
            }

            $this->MainModel->hardDeleteWhere('detail_tindakan_pemeriksaan', ['pemeriksaan_id', $id]);

            $input_tindakan = $this->input->post('tindakan');

            foreach ($input_tindakan as $key => $value) {
                $tarif_perawat = $this->PemeriksaanModel->getTarifTindakanById($value)->tarif_perawat;
                $tindakan = array(
                    'pemeriksaan_id' => $id,
                    'tarif_tindakan_id' => $value,
                    'jumlah_perawat' => count($input_perawat),
                    'perawat' => implode(',', $input_perawat),
                    'tarif_per_perawat' => $tarif_perawat / count($input_perawat),
                    'creator' => $session->id
                );
                $this->MainModel->insert_id('detail_tindakan_pemeriksaan', $tindakan);
            }

            $input_tindakan_operan = $this->input->post('tindakan_operan');
            $input_perawat_operan = $this->input->post('perawat_operan') ?? [];
            foreach ($input_tindakan_operan as $key => $value) {
                $tarif_perawat = $this->PemeriksaanModel->getTarifTindakanById($value)->tarif_perawat;
                $tindakan = array(
                    'pemeriksaan_id' => $id,
                    'tarif_tindakan_id' => $value,
                    'jumlah_perawat' => count($input_perawat_operan),
                    'perawat' => implode(',', $input_perawat_operan),
                    'tarif_per_perawat' => count($input_perawat_operan) ? $tarif_perawat / count($input_perawat_operan) : 0,
                    'creator' => $session->id,
                    'is_operan' => 1
                );
                $this->MainModel->insert_id('detail_tindakan_pemeriksaan', $tindakan);
            }

            $this->MainModel->hardDeleteWhere('detail_tindakan_konsul', ['pemeriksaan_id', $id]);

            for ($i = 0; $i <= $this->input->post('max_konsul'); $i++) {
                $dokter_id = $this->input->post('dokter_' . $i);
                if ($dokter_id) {
                    foreach ($this->input->post('tindakan_' . $i) as $key => $value) {
                        $tarif_perawat = $this->PemeriksaanModel->getTarifTindakanById($value)->tarif_perawat;
                        $tindakan = array(
                            'pemeriksaan_id' => $id,
                            'tarif_tindakan_id' => $value,
                            'dokter_id' => $dokter_id,
                            'jumlah_perawat' => count($input_perawat),
                            'perawat' => implode(',', $input_perawat),
                            'tarif_per_perawat' => $tarif_perawat / count($input_perawat),
                            'creator' => $session->id
                        );
                        $this->MainModel->insert_id('detail_tindakan_konsul', $tindakan);
                    }
                }
            }

            $this->MainModel->hardDeleteWhere('detail_penyakit_pemeriksaan', ['pemeriksaan_id', $id]);
            $penyakit = $this->input->post('diagnosis_jenis_penyakit') ?? [];
            foreach ($penyakit as $key => $value) {
                $penyakit = array(
                    'pemeriksaan_id' => $id,
                    'penyakit_id' => $value,
                    'creator	' => $session->id
                );
                $this->MainModel->insert_id('detail_penyakit_pemeriksaan', $penyakit);
            }

            $this->db->delete('detail_bahan_pemeriksaan', array('pemeriksaan_id' => $id));

            if (isset($_POST['id'])) {
                $x = 0;
                $jumlah_bahan = $_POST['qty'];
                foreach ($_POST['id'] as $value) {
                    $this->db->insert(
                        'detail_bahan_pemeriksaan',
                        array(
                            'pemeriksaan_id' => $id,
                            'bahan_id' => $value,
                            'jumlah' => $jumlah_bahan[$x],
                            'creator' => $session->id
                        )
                    );
                    $bahan = $this->ObatModel->getBahanById($value)->row();
                    $x++;
                }
            }

            if (isset($_POST['rujuk']) && $_POST['rujuk']) {
                if ($_POST['rujuk'] == 'lab') {
                    redirect('Permintaan/lab/' . $id . '/1', 'refresh');
                }
                else if ($_POST['rujuk'] == 'radio') {
                    redirect('Permintaan/radio/' . $id . '/1', 'refresh');
                }
                else if ($_POST['rujuk'] == 'poli') {
                    die(json_encode($this->input->post()));
                    $this->insertRujukan($id);
                }
            }
            else if (isset($_POST['input_resep']) && $_POST['input_resep']) {
                redirect('apotek/input_resep/' . $id . '/igd');
            }
            else {
                $pemeriksaan = $this->db->query("SELECT * FROM pemeriksaan WHERE id = $id")->row();
                $ant = $this->db->query("SELECT * FROM antrian WHERE pendaftaran_id = $pemeriksaan->pendaftaran_id")->row();
                $this->MainModel->update('antrian', ['is_done' => 1], $ant->id);

                $task_body = ['kodebooking' => $ant->kode_booking ?? '', 'taskid' => 5, 'waktu' => time() . '000'];
                $this->MainModel->insert('bpjs_jknmobile_log', [
                    'url' => 'antrean/updatewaktu',
                    'header' => 'task 5',
                    'request' => json_encode($task_body),
                    'response' => json_encode($this->post("{$this->base_url_antrean}antrean/updatewaktu", json_encode($task_body), true)),
                ]);

                if (!$this->db->query("SELECT * FROM antrian_farmasi WHERE pemeriksaan_id = $pemeriksaan->id")->row()) {
                    $today = date('Y-m-d');
                    $last = $this->db->query("SELECT * FROM antrian_farmasi WHERE due_date = '$today' ORDER BY id DESC")->row();
                    $next_no_antrian = $last ? 'FAR-' . (1 + ((int)(preg_replace("/[^0-9]/", "", $last->no_antrian)))) : 'FAR-1';
                    $this->MainModel->insert('antrian_farmasi', [
                        'no_antrian' => $next_no_antrian,
                        'due_date' => $today,
                        'pemeriksaan_id' => $pemeriksaan->id,
                        'pendaftaran_id' => $pemeriksaan->pendaftaran_id,
                    ]);
                }

                $this->session->set_flashdata('success', 'Pemeriksaan pasien berhasil!');
                redirect('Igd/selesai', 'refresh');
            }
        } else {
            $data['pendaftaran'] = $this->PemeriksaanModel->getPendaftaranByIdPemeriksaan($id)->row_array();
            $data['pemeriksaan'] = $this->PemeriksaanModel->getPemeriksaanById($id)->row_array();
            $data['form'] = unserialize($data['pemeriksaan']['form']);
            $data['jaminan'] = $this->config->item('pendaftaran');
            $data['hasil_penunjang'] = json_decode($data['pemeriksaan']['hasil_penunjang']);
            $data['jenis_pendaftaran'] = $this->PendaftaranModel->getJenisPendaftaran();

            $data['obat'] = $this->PemeriksaanModel->getObat();
            $data['obat1'] = $this->db->get_where('obat', ['is_active' => 1, 'stok_obat >' => 0])->result();
            $data['bahan'] = $this->db->get_where('bahan_habis_pakai', ['is_active' => 1, 'jumlah >' => 0])->result();

            $data['s_tindakan'] = $this->LaporanModel->getTindakanByIdPemeriksaan($id)->result();
            $data['s_penyakit'] = $this->LaporanModel->getPenyakitPemeriksaanByIdPemeriksaan($id)->result();
            $category = 'igd'; // 'igd'
            $data['tindakan'] = $this->PemeriksaanModel->getTindakanByCategory($category);
            $data['penyakit'] = $this->PemeriksaanModel->getPenyakitByCategory($category);
            $data['listPerawat'] = $this->PerawatModel->listPerawat();
            $data['dokter'] = $this->PendaftaranModel->getDokter();
            $data['tindakan_pasien'] = $this->DetailTindakanPemeriksaanModel->getDetailTindakanByPemeriksaanId($data['pemeriksaan']['id']);
            $data['tindakan_konsul'] = $this->db->query("
                SELECT detail_tindakan_konsul.*, tt.nama FROM detail_tindakan_konsul
                JOIN tarif_tindakan tt on detail_tindakan_konsul.tarif_tindakan_id = tt.id
                WHERE pemeriksaan_id = {$data['pemeriksaan']['id']}
            ")->result();

            $pemeriksaan = $this->db->query("SELECT * FROM pemeriksaan WHERE id = $id")->row();
            $ant = $this->db->query("SELECT * FROM antrian WHERE pemeriksaan_id = $pemeriksaan->pendaftaran_id")->row();
            $cek = $this->db->query("SELECT * FROM bpjs_jknmobile_log WHERE header = 'task 4' AND request like '%$ant->kode_booking%'")->row();
            if (!$cek && !$data['pendaftaran']['pendaftaran_id']) {
                $task_body = ['kodebooking' => $ant->kode_booking ?? '', 'taskid' => 4, 'waktu' => time() . '000'];
                $this->MainModel->insert('bpjs_jknmobile_log', [
                    'url' => 'antrean/updatewaktu',
                    'header' => 'task 4',
                    'request' => json_encode($task_body),
                    'response' => json_encode($this->post("{$this->base_url_antrean}antrean/updatewaktu", json_encode($task_body), true)),
                ]);
            }

            $this->template->view('igd/pemeriksaan', $data);
        }
    }

    private function insertRujukan($pemeriksaan_id)
    {
        $session = $this->session->userdata('logged_in');
        $pasien_id = $this->input->post('pasien_id');
        $pendaftaran_id = $this->input->post('pendaftaran_id');
        $pendaftaran = $this->db->query("SELECT * FROM pendaftaran_pasien WHERE id = $pendaftaran_id")->row();
        $pem = $this->db->query("SELECT * FROM pemeriksaan WHERE id = $pemeriksaan_id")->row();

        $rm = array(
            'pendaftaran_id' => $pendaftaran_id,
            'td' => $pem->td,
            'r' => $pem->r,
            'bb' => $pem->bb,
            'n' => $pem->n,
            's' => $pem->s,
            'tb' => $pem->tb,
            'bmi' => $pem->bmi,
            'no_rm' => $pendaftaran->no_rm,
            'pasien' => $pendaftaran->pasien,
            'penanggungjawab' => $pendaftaran->penanggungjawab,
            'biopsikososial' => $pendaftaran->biopsikososial,
            'jaminan' => $pendaftaran->jaminan,
            'no_jaminan' => $pendaftaran->no_jaminan,
            'jenis_pendaftaran_id' => $this->input->post('poli-rujuk'),
            'status' => 'antri',
            'is_bpjs' => $pendaftaran->jaminan,
            'creator' => $session->id
        );

        $insert = $this->MainModel->insert_id('pendaftaran_pasien', $rm);

        $id_antrian = $this->MainModel->insert('antrian', [
            'pendaftaran_id' => $insert,
            'pemeriksaan_id' => null,
            'jenis_pendaftaran_id' => $this->input->post('poli-rujuk'),
            'pasien_id' => $pasien_id,
            'due_date' => date('Y-m-d'),
            'kode_antrian' => generateKodeAntrian($this->input->post('poli-rujuk'), date('Y-m-d')),
            'is_check_in' => true,
            'check_in_at' => date('Y-m-d H:i'),
            'kode_booking' => generateKodeBooking()
        ]);

        if ($insert)
            $this->session->set_flashdata('success', 'Rujuk pasien berhasil!');
        else
            $this->session->set_flashdata('warning', 'Rujuk pasien gagal!');

        if ($this->input->post('from_igd'))
            redirect('Igd/periksa/' . $pemeriksaan_id, 'refresh');
        else
            redirect('pemeriksaan/periksa/' . $pemeriksaan_id, 'refresh');
    }

    public function selesai()
    {
        $data['listPemeriksaan'] = $this->IgdModel->getListPemeriksaanSudahPeriksa()->result();
        foreach ($data['listPemeriksaan'] as &$datum) {
            $datum->penyakits = $this->DetailPenyakitPemeriksaanModel->getDetailPenyakitByPemeriksaanId($datum->id)->result();
            $datum->tindakan_operan = $this->DetailTindakanPemeriksaanModel->getDetailTindakanOperanByPemeriksaanId($datum->id)->result();
            $datum->perawat_operan = $this->PerawatModel->getPerawatOperanByPemeriksaanId($datum->id);
        }

        $data['listPerawat'] = $this->PerawatModel->listPerawat()->result();
        $data['tindakan'] = $this->PemeriksaanModel->getTindakanByCategory('igd')->result();
        $data['jaminan'] = $this->config->item('pendaftaran');

        $this->template->view('igd/list_selesai', $data);
    }

    public function add_operan($pemeriksaan_id)
    {
        $this->db
            ->where('pemeriksaan_id', $pemeriksaan_id)
            ->where('is_operan', 1)
            ->delete('detail_tindakan_pemeriksaan');

        $session = $this->session->userdata('logged_in');
        $input_tindakan_operan = $this->input->post('tindakan_operan');
        $input_perawat_operan = $this->input->post('perawat_operan');

        foreach ($input_tindakan_operan as $key => $value) {
            $tarif_perawat = $this->PemeriksaanModel->getTarifTindakanById($value)->tarif_perawat;
            $tindakan = array(
                'pemeriksaan_id' => $pemeriksaan_id,
                'tarif_tindakan_id' => $value,
                'jumlah_perawat' => count($input_perawat_operan),
                'perawat' => implode(',', $input_perawat_operan),
                'tarif_per_perawat' => $tarif_perawat / count($input_perawat_operan),
                'creator' => $session->id,
                'is_operan' => 1
            );
            $this->MainModel->insert_id('detail_tindakan_pemeriksaan', $tindakan);
        }

        $this->session->set_flashdata('success', 'Berhasil menambah tindakan operan!');
        redirect('Igd/selesai', 'refresh');
    }

    public function hapus($id)
    {
        $this->MainModel->delete('pemeriksaan', ['is_active' => 0], $id);
        $this->MainModel->delete('pendaftaran_pasien', ['is_active' => 0], $id);

        $this->session->set_flashdata('success', 'Berhasil menghapus pemeriksaan');
        redirect('Igd/pemeriksaan', 'refresh');
    }
}
