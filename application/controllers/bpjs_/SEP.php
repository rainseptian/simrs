<?php

class SEP extends MY_BpjsController
{
    public function __construct()
    {
        parent::__construct();

        $this->load->Model('MainModel');
        $this->load->Model('AdministrasiModel');
        $this->load->helper(array('file', 'php_with_mpdf_helper'));
        $this->load->helper(array('file', 'mpdf'));
    }

    public function daftar()
    {
        $list = $this->db->query("
            SELECT sep.*, p.nama as nama_pasien, p.no_rm FROM bpjs_sep sep
            LEFT JOIN pasien p ON (p.no_bpjs = sep.no_bpjs or p.no_rm = sep.no_rm)
            WHERE success = 1 AND date(sep.created_at) >= '2023-10-01'
            ORDER BY id
        ")->result();
//            LIMIT 500

//        $s = $this->db->query("SELECT * FROM bpjs_sep")->result();
//        foreach ($s as $v) {
//            $n = json_decode($v->request);
//            $n = $n->request->t_sep->noMR;
//            $this->MainModel->update('bpjs_sep', ['no_rm' => $n], $v->id);
//        }
//        GROUP BY sep.id

        $this->template->view('bpjs/sep/list', compact('list'));
    }

    public function create()
    {
        $data['base_url_vclaim'] = MY_BpjsController::$base_url_vclaim_;
        $this->template->view('bpjs/sep/create', $data);
    }

    public function create_detail()
    {
        $data = $this->input->post();
        $jenis = $this->input->post('jenis');
        $data['rujukan'] = (json_decode($this->input->post('rujukan')))->rujukan;
        $data['jns_rujukan'] = $this->input->post('jns_rujukan') ?? -1; // 1: rujukan normal; 2: rujukan kontrol
        $data['asalFaskes'] = (json_decode($this->input->post('rujukan')))->asalFaskes;
        $noka = gettype($data['peserta']) == 'string' ? (json_decode($data['peserta']))->noKartu : $data['peserta']->noKartu;
        if (!$noka && isset($data['rujukan'])) {
            $noka = $data['rujukan']->peserta->noKartu;
        }
        $data['pasien'] = $this->db->query("SELECT * FROM pasien WHERE no_bpjs = '$noka' and no_bpjs != ''")->row();

        $from = date('Y-m-d', strtotime('-30 day'));
        $to = date('Y-m-d');
        $history = $this->get("{$this->base_url_vclaim}monitoring/HistoriPelayanan/NoKartu/$noka/tglMulai/$from/tglAkhir/$to");
        $data['history'] = $history->success ? $history->data->histori : [];
        $data['noKartu'] = $noka;
        $data['base_url_vclaim'] = MY_BpjsController::$base_url_vclaim_;
        $data['provinsi'] = json_decode(parent::ajax_search_provinsi())->datanya;

        $kontrol_pertama = false;
        if (count($data['history'])) {
            $h = $data['history'];
            usort($h, function ($a, $b) {
                return strtotime($a->tglPlgSep) < strtotime($b->tglPlgSep);
            });
            if ($h[0]->jnsPelayanan == 1 && $this->input->post('jns_pelayanan') != '1') {
                $data['no_rujukan'] = $h[0]->noSep;
                $kontrol_pertama = true;
            }
        }

        if ($kontrol_pertama) {
            $data['jenis_sep'] = 'Kontrol Pertama';
            $data['detail_view'] = 'kontrol_pertama';
            $data['peserta'] = json_decode($this->input->post('peserta'));
            $data['dpjp'] = [];
            $poli = $this->get("{$this->base_url_antrean}ref/poli");
            $response = $poli->server_output->response;
            $decrypted = $this->stringDecrypt($response);
            $decompressed = $this->decompress($decrypted);
            $poli = json_decode($decompressed);
            $data['poli'] = $poli;
            $data['asal_rujukan'] = 2;
        }
        else if ($jenis == 2) { // igd / ranap
            $data['jenis_sep'] = $this->input->post('jns_pelayanan') == '1' ? 'Rawat Inap' : 'Rawat Jalan';
            $data['detail_view'] = $this->input->post('jns_pelayanan') == '1' ? 'rawat_inap' : 'igd';
            $data['peserta'] = json_decode($this->input->post('peserta'));

            if ($this->input->post('jns_pelayanan') == '1') { // rawat inap
                $data['dpjp'] = [];
            }
            else { // igd
                $data['dpjp'] = json_decode($this->ajax_search_dpjp_param(1, $data['tgl_sep'], 'IGD'), true)['datanya'];
            }
        }
        else { // rujukan
            $data['jenis_sep'] = $data['jns_rujukan'] == 1 ? 'Rujukan' : 'Rujukan Kontrol';
            $data['detail_view'] = $data['jns_rujukan'] == 1 ? 'rujukan' : 'rujukan_kontrol';
            $data['peserta'] = $data['rujukan']->peserta;
            $data['dpjp'] = json_decode($this->ajax_search_dpjp_param(2, $data['tgl_sep'], $data['rujukan']->poliRujukan->kode), true)['datanya'];
        }

        $this->template->view('bpjs/sep/create_detail', $data);
    }

    public function insert()
    {
        $is_inap = $this->input->post('jns_pelayanan') == 1;
        $is_naik_kelas = $this->input->post('naik_kelas');
        $body = [
            'request' => [
                't_sep' => [
                    'noKartu' => $this->input->post('nomor_kartu'),
                    'tglSep' => $this->input->post('tgl_sep'),
                    'ppkPelayanan' => '0207S001', // $this->input->post('ppk_pelayanan'),
                    'jnsPelayanan' => $this->input->post('jns_pelayanan'), // jenis pelayanan = 1. r.inap 2. r.jalan
                    'klsRawat' => [
                        'klsRawatHak' => $this->input->post('kelas_rawat_hak'),
                        'klsRawatNaik' => $is_naik_kelas ? $this->input->post('kelas_rawat_naik') : '',
                        'pembiayaan' => $is_naik_kelas ? $this->input->post('pembiayaan') : '',
                        'penanggungJawab' => $is_naik_kelas ? ($this->input->post('pembiayaan') == 1 ? 'Pribadi' : $this->input->post('penanggungjawab')) : '',
                    ],
                    'noMR' => $this->input->post('no_mr'),
                    'rujukan' => [
                        'asalRujukan' => $this->input->post('asal_rujukan'),
                        'tglRujukan' => $this->input->post('tgl_rujukan'),
                        'noRujukan' => $this->input->post('poli_tujuan') == 'IGD' ? '' : $this->input->post('no_rujukan'),
                        'ppkRujukan' => $this->input->post('ppk_rujukan'),
                    ],
                    'catatan' => $this->input->post('catatan'),
                    'diagAwal' => $this->input->post('diag_awal'),
                    'poli' => [
                        'tujuan' => $is_inap ? '' : $this->input->post('poli_tujuan'),
                        'eksekutif' => $is_inap ? '' : $this->input->post('poli_eksekutif'),
                    ],
                    'cob' => [
                        'cob' => $this->input->post('cob') ? '1' : '0',
                    ],
                    'katarak' => [
                        'katarak' => $this->input->post('katarak') ? '1' : '0',
                    ],
                    'jaminan' => [
                        'lakaLantas' => $this->input->post('laka_lantas') == '-' ? 0 : $this->input->post('laka_lantas'),
                        'penjamin' => [
                            'tglKejadian' => $this->input->post('tgl_kejadian'),
                            'keterangan' => $this->input->post('keterangan'),
                            'suplesi' => [
                                'suplesi' => $this->input->post('suplesi'),
                                'noSepSuplesi' => $this->input->post('no_sep_suplesi'),
                                'lokasiLaka' => [
                                    'kdPropinsi' => $this->input->post('kd_propinsi'),
                                    'kdKabupaten' => $this->input->post('kd_kabupaten') ?? '',
                                    'kdKecamatan' => $this->input->post('kd_kecamatan') ?? '',
                                ]
                            ]
                        ]
                    ],
                    'tujuanKunj' => $this->input->post('tujuan_kunj') ?? '0',
                    'flagProcedure' => ($this->input->post('tujuan_kunj') ?? '0') == '1' ? $this->input->post('flag_procedure') : '',
                    'kdPenunjang' => ($this->input->post('tujuan_kunj') ?? '0') == '1' ? $this->input->post('kd_penunjang') : '',
                    'assesmentPel' => ($this->input->post('tujuan_kunj') ?? '0') == '2' ? $this->input->post('assesment_pel') : '',
                    'skdp' => [
                        'noSurat' => $is_inap ? $this->input->post('no_spri') : ($this->input->post('no_skdp') ?? ''),
                        'kodeDPJP' => $is_inap ? $this->input->post('dpjp_pemberi_spri') : ($this->input->post('dpjp_pemberi_skdp') ?? ''),
                    ],
                    'dpjpLayan' => $is_inap ? '' : $this->input->post('dpjp_layan'),
                    'noTelp' => $this->input->post('no_telp'),
                    'user' => $this->session->userdata('logged_in')->nama,
                ]
            ]
        ];

        $res = $this->post("{$this->base_url_vclaim}SEP/2.0/insert", json_encode($body));

        if ($res->success) {
            $id = $this->MainModel->insert_id('bpjs_sep', [
                'id_pasien' => $this->input->post('id_pasien') ?? 0,
                'no_rm' => $this->input->post('no_mr') ?? '',
                'no_bpjs' => $this->input->post('nomor_kartu'),
                'no_rujukan' => $this->input->post('no_rujukan'),
                'raw_request' => '',
                'request' => json_encode($body),
                'response' => json_encode($res->data),
                'success' => 1,
                'type' => $this->input->post('jenis') == '1' ? ($this->input->post('jns_rujukan') == 1 ? 'rujukan' : 'rujukan kontrol') : ($is_inap ? 'rawat inap' : 'igd'),
                'no_sep' => $res->data->sep->noSep
            ]);
            $this->session->set_flashdata('id', $id);
            $this->session->set_flashdata('success', 'Berhasil membuat SEP! Nomor: ' . $res->data->sep->noSep);
        }
        else {
            $resp = $res->server_output;
            if (($resp->metaData->message ?? '') == 'tujuanKunj tidak sesuai') {
                $this->insert_sep_kontrol($body, $is_inap, 1);
            }
            else {
                $this->MainModel->insert('bpjs_sep', [
                    'id_pasien' => $this->input->post('id_pasien') ?? 0,
                    'no_rm' => $this->input->post('no_mr') ?? '',
                    'no_bpjs' => $this->input->post('nomor_kartu'),
                    'no_rujukan' => $this->input->post('no_rujukan'),
                    'raw_request' => '',
                    'request' => json_encode($body),
                    'response' => json_encode($res->server_output),
                    'success' => 0,
                    'type' => $this->input->post('jenis') == '1' ? ($this->input->post('jns_rujukan') == 1 ? 'rujukan' : 'rujukan kontrol') : ($is_inap ? 'rawat inap' : 'igd'),
                ]);
                $this->session->set_flashdata('warning', 'Gagal membuat SEP! : ' . ($resp->metaData->message ?? ''));
            }
        }
        redirect('bpjs/sep/create', 'refresh');
    }

    private function insert_sep_kontrol($body, $is_inap, $tujuanKunj)
    {
        $body['request']['t_sep']['tujuanKunj'] = "$tujuanKunj";
        $res = $this->post("{$this->base_url_vclaim}SEP/2.0/insert", json_encode($body));
        if ($res->success) {
            $id = $this->MainModel->insert_id('bpjs_sep', [
                'id_pasien' => $this->input->post('id_pasien') ?? 0,
                'no_rm' => $this->input->post('no_mr') ?? '',
                'no_bpjs' => $this->input->post('nomor_kartu'),
                'no_rujukan' => $this->input->post('no_rujukan'),
                'raw_request' => "tujuanKunj: $tujuanKunj",
                'request' => json_encode($body),
                'response' => json_encode($res->data),
                'success' => 1,
                'type' => $this->input->post('jenis') == '1' ? 'rujukan' : ($is_inap ? 'rawat inap' : 'igd'),
                'no_sep' => $res->data->sep->noSep
            ]);
            $this->session->set_flashdata('id', $id);
            $this->session->set_flashdata('success', 'Berhasil membuat SEP! Nomor: ' . $res->data->sep->noSep);
        }
        else {
            if ($tujuanKunj == 1) {
                $this->insert_sep_kontrol($body, $is_inap, 2);
            }
            $resp = $res->server_output;
            $this->MainModel->insert('bpjs_sep', [
                'id_pasien' => $this->input->post('id_pasien') ?? 0,
                'no_rm' => $this->input->post('no_mr') ?? '',
                'no_bpjs' => $this->input->post('nomor_kartu'),
                'no_rujukan' => $this->input->post('no_rujukan'),
                'raw_request' => "tujuanKunj: $tujuanKunj",
                'request' => json_encode($body),
                'response' => json_encode($res->server_output),
                'success' => 0,
                'type' => $this->input->post('jenis') == '1' ? 'rujukan' : ($is_inap ? 'rawat inap' : 'igd'),
            ]);
            $this->session->set_flashdata('warning', 'Gagal membuat SEP! : ' . ($resp->metaData->message ?? ''));
        }
    }

    public function print_sep($id = 0, $no_sep = 0)
    {
        if ($id == 0) {
            $id = $this->db->query("SELECT * FROM bpjs_sep ORDER BY id DESC")->row()->id;
        }
        else {
            $this->db->query("UPDATE bpjs_sep SET print_ke = print_ke + 1 WHERE id = $id");
        }

        $data['data'] = $this->db->query("SELECT sep.* FROM bpjs_sep sep WHERE sep.id = $id")->row();
        $data['response'] = json_decode($data['data']->response);
        $data['response'] = $data['response']->sep;
        $data['diagnosa'] = $data['response']->diagnosa;
        $data['request'] = json_decode($data['data']->request);
        $data['request'] = $data['request']->request->t_sep;
        $data['sep'] = $this->search_sep($data['response']->noSep)['datanya'];
        $data['pasien'] = $this->db->query("SELECT * FROM pasien WHERE no_rm = '{$data['sep']->peserta->noMr}'")->row();
        $data['dpjp'] = $data['sep']->jnsPelayanan == 'Rawat Inap' ? $data['sep']->kontrol->nmDokter : $data['sep']->dpjp->nmDPJP;

        if ($no_sep) {
            $data['sep'] = $this->search_sep($no_sep)['datanya'];
            $data['dpjp'] = $data['sep']->jnsPelayanan == 'Rawat Inap' ? $data['sep']->kontrol->nmDokter : $data['sep']->dpjp->nmDPJP;
        }
        if ($data['sep']->noRujukan && $data['sep']->jnsPelayanan != 'Rawat Inap' && !$data['diagnosa']) {
            $rujukan = json_decode($this->ajax_search_rujukan($data['sep']->noRujukan), true)['datanya'];
            $data['diagnosa'] = "{$rujukan['rujukan']['diagnosa']['kode']} - {$rujukan['rujukan']['diagnosa']['nama']}";
        }

        $data['klinik'] = $this->AdministrasiModel->getKlinik()->row();
        $this->load->view('bpjs/sep/print', $data);
    }

    public function detail($no_sep)
    {
        $sep = $this->search_sep($no_sep);
        if (!($sep['status'] == 1 && $sep['datanya'] != [])) {
            $this->session->set_flashdata('warning', 'Nomor SEP tidak ditemukan');
            redirect('bpjs/sep/create', 'refresh');
            return;
        }

        $data['sep'] = $sep['datanya'];
        $data['peserta'] = $this->search_peserta_by_no_bpjs($data['sep']->peserta->noKartu, $data['sep']->tglSep)['datanya']->peserta;
        $data['bpjs_sep'] = $this->db->query("SELECT * FROM bpjs_sep WHERE no_sep = '$no_sep'")->row();
        $data['request'] = json_decode($data['bpjs_sep']->request)->request->t_sep;
        $data['jenis_sep'] = $data['sep']->jnsPelayanan;
        $data['detail_view'] = $data['sep']->jnsPelayanan == 'Rawat Jalan' ? ($data['sep']->poli == 'INSTALASI GAWAT DARURAT' ? 'igd' : 'rujukan') : 'rawat_inap';
        $data['provinsi'] = json_decode(parent::ajax_search_provinsi())->datanya;

        $kode_poli = json_decode($data['bpjs_sep']->request)->request->t_sep->poli->tujuan;
        $kode_dpjp = json_decode($data['bpjs_sep']->request)->request->t_sep->dpjpLayan;
        $data['dpjp'] = json_decode($this->ajax_search_dpjp_param(2, $data['sep']->tglSep, $kode_poli), true)['datanya'] ?? $this->get_dpjp($kode_dpjp);

        if ($data['sep']->jnsPelayanan == 'Rawat Jalan') {
            $data['dpjp'] = json_decode($this->ajax_search_dpjp_param(1, $data['sep']->tglSep, 'IGD'), true)['datanya'];
            $data['rujukan'] = json_decode($this->ajax_search_rujukan($data['sep']->noRujukan), true)['datanya'];
        }
        else if ($data['sep']->jnsPelayanan == 2) {
            if ($data['sep']->jnsPelayanan == 1) { // rawat inap
                $data['dpjp'] = [];
            }
            else { // igd
                $data['dpjp'] = json_decode($this->ajax_search_dpjp_param(1, $data['tgl_sep'], 'IGD'), true)['datanya'];
            }
        }
        else { // rujukan
            $data['dpjp'] = json_decode($this->ajax_search_dpjp_param(2, $data['tgl_sep'], $data['rujukan']->poliRujukan->kode), true)['datanya'];
        }

        $from = date('Y-m-d', strtotime('-90 day'));
        $to = date('Y-m-d');
        $history = $this->get("{$this->base_url_vclaim}monitoring/HistoriPelayanan/NoKartu/{$data['sep']->peserta->noKartu}/tglMulai/$from/tglAkhir/$to");
        $data['history'] = $history->success ? $history->data->histori : [];
        $data['base_url_vclaim'] = MY_BpjsController::$base_url_vclaim_;

        $this->template->view('bpjs/sep/detail/detail', $data);
    }

    public function delete_sep($sep)
    {
        $res = $this->destroy_sep($sep);
        $this->db->delete('bpjs_sep', array('no_sep' => $sep));

        $this->session->set_flashdata('success', 'Berhasil menghapus SEP');
        $this->session->set_flashdata('no-print', 'yes');
        redirect('bpjs/sep/create', 'refresh');
    }

    public function update()
    {
        $is_inap = $this->input->post('jns_pelayanan') == 'Rawat Inap';
        $is_naik_kelas = $this->input->post('naik_kelas');
        $body = [
            'request' => [
                't_sep' => [
                    'noSep' => $this->input->post('nomor_sep'),
                    'klsRawat' => [
                        'klsRawatHak' => $this->input->post('kelas_rawat_hak'),
                        'klsRawatNaik' => $is_naik_kelas ? $this->input->post('kelas_rawat_naik') : '',
                        'pembiayaan' => $is_naik_kelas ? $this->input->post('pembiayaan') : '',
                        'penanggungJawab' => $is_naik_kelas ? ($this->input->post('pembiayaan') == 1 ? 'Pribadi' : $this->input->post('penanggungjawab')) : '',
                    ],
                    'noMR' => $this->input->post('no_mr'),
                    'catatan' => $this->input->post('catatan'),
                    'diagAwal' => $this->input->post('diag_awal'),
                    'poli' => [
                        'tujuan' => $this->input->post('poli_tujuan'),
                        'eksekutif' => $this->input->post('poli_eksekutif'),
                    ],
                    'cob' => [
                        'cob' => $this->input->post('cob') ? '1' : '0',
                    ],
                    'katarak' => [
                        'katarak' => $this->input->post('katarak') ? '1' : '0',
                    ],
                    'jaminan' => [
                        'lakaLantas' => $this->input->post('laka_lantas'),
                        'penjamin' => [
                            'tglKejadian' => $this->input->post('tgl_kejadian'),
                            'keterangan' => $this->input->post('keterangan'),
                            'suplesi' => [
                                'suplesi' => $this->input->post('suplesi'),
                                'noSepSuplesi' => $this->input->post('no_sep_suplesi'),
                                'lokasiLaka' => [
                                    'kdPropinsi' => $this->input->post('kd_propinsi'),
                                    'kdKabupaten' => $this->input->post('kd_kabupaten') ?? '',
                                    'kdKecamatan' => $this->input->post('kd_kecamatan') ?? '',
                                ]
                            ]
                        ]
                    ],
                    'dpjpLayan' => $is_inap ? '' : $this->input->post('dpjp_layan'),
                    'noTelp' => $this->input->post('no_telp'),
                    'user' => $this->session->userdata('logged_in')->nama,
                ]
            ]
        ];
//        die(json_encode($body));
        $res = $this->put("{$this->base_url_vclaim}SEP/2.0/update", json_encode($body));

        if ($res->success) {
            $no_sep = $this->input->post('nomor_sep');
            $d = $this->db->query("SELECT * FROM bpjs_sep WHERE no_sep = '$no_sep'")->row();
            $b_req = json_decode($d->request);

            $b_req->request->t_sep->noMR = $this->input->post('no_mr');
            $b_req->request->t_sep->diagAwal = $this->input->post('diag_awal');
            $b_req->request->t_sep->noTelp = $this->input->post('no_telp');
            $b_req->request->t_sep->catatan = $this->input->post('catatan');
            $b_req->request->t_sep->jaminan->lakaLantas = $this->input->post('laka_lantas');

            $b_req->request->t_sep->klsRawat->klsRawatNaik = $is_naik_kelas ? $this->input->post('kelas_rawat_naik') : '';
            $b_req->request->t_sep->klsRawat->pembiayaan = $is_naik_kelas ? $this->input->post('pembiayaan') : '';
            $b_req->request->t_sep->klsRawat->penanggungJawab = $is_naik_kelas ? ($this->input->post('pembiayaan') == 1 ? 'Pribadi' : $this->input->post('penanggungjawab')) : '';

            $this->MainModel->update('bpjs_sep', [
                'id_pasien' => $this->input->post('id_pasien') ?? 0,
                'no_bpjs' => $this->input->post('nomor_kartu'),
                'request' => json_encode($b_req),
                'response' => json_encode($res->data),
                'success' => 1,
                'type' => $this->input->post('type'),
            ], $d->id);

            $this->session->set_flashdata('id', $d->id);
            $this->session->set_flashdata('success', 'Berhasil mengedit SEP! ');
            redirect('bpjs/sep/create', 'refresh');
        }
        else {
            $resp = $res->server_output;
            $this->session->set_flashdata('warning', 'Gagal mengedit SEP! : ' . ($resp->metaData->message ?? ''));
            redirect('bpjs/sep/create', 'refresh');
        }
    }

    public function persetujuan()
    {
        if ($this->input->post()) {
            $body = [
                'request' => [
                    't_sep' => [
                        'noKartu' => $this->input->post('no_kartu'),
                        'tglSep' => $this->input->post('tgl_sep'),
                        'jnsPelayanan' => $this->input->post('jns_pelayanan'),
                        'jnsPengajuan' => $this->input->post('jns_pengajuan'),
                        'keterangan' => $this->input->post('keterangan'),
                        'user' => $this->session->userdata('logged_in')->nama,
                    ]
                ]
            ];

            $res = $this->post("{$this->base_url_vclaim}Sep/pengajuanSEP", json_encode($body));

            if ($res->success) {
                $id = $this->MainModel->insert_id('bpjs_sep_persetujuan', [
                    'no_kartu' => $this->input->post('no_kartu'),
                    'nama_peserta' => $this->input->post('nama_peserta'),
                    'tgl_sep' => $this->input->post('tgl_sep'),
                    'ri_rj' => $this->input->post('jns_pelayanan') == 1 ? 'Rawat Inap' : 'Rawat Jalan',
                    'status' => 'Baru',
                    'persetujuan' => $this->input->post('jns_pengajuan'),
                    'request' => json_encode($body),
                    'response' => json_encode($res->data),
                    'success' => 1,
                ]);
                $this->session->set_flashdata('id', $id);
                $this->session->set_flashdata('success', 'Berhasil membuat pengajuan SEP!');
            }
            else {
                $this->MainModel->insert('bpjs_sep_persetujuan', [
                    'no_kartu' => $this->input->post('no_kartu'),
                    'nama_peserta' => $this->input->post('nama_peserta'),
                    'tgl_sep' => $this->input->post('tgl_sep'),
                    'ri_rj' => $this->input->post('jns_pelayanan') == 1 ? 'Rawat Inap' : 'Rawat Jalan',
                    'status' => 'Gagal',
                    'persetujuan' => $this->input->post('jns_pengajuan'),
                    'request' => json_encode($body),
                    'response' => json_encode($res->server_output),
                    'success' => 0,
                ]);
                $resp = $res->server_output;
                $this->session->set_flashdata('warning', 'Gagal membuat pengajuan SEP! : ' . ($resp->metaData->message ?? ''));
            }
            redirect('bpjs/sep/persetujuan', 'refresh');
        }
        else {
            $list = $this->db->query("
                SELECT * FROM bpjs_sep_persetujuan
                ORDER BY id DESC
            ")->result();
            $this->template->view('bpjs/sep_persetujuan/list', compact('list'));
        }
    }

    public function persetujuan_setuju($id)
    {
        $data = $this->db->query("SELECT * FROM bpjs_sep_persetujuan WHERE id = $id")->row();
        $res = $this->post("{$this->base_url_vclaim}Sep/aprovalSEP", $data->request);
        if ($res->success) {
            $this->MainModel->update('bpjs_sep_persetujuan', ['status' => 'Disetujui'], $id);
            $this->session->set_flashdata('success', 'Berhasil menyetujui pengajuan SEP!');
        }
        else {
            $resp = $res->server_output;
            $this->session->set_flashdata('warning', 'Gagal menyetujui pengajuan SEP! : ' . ($resp->metaData->message ?? ''));
        }
        redirect('bpjs/sep/persetujuan', 'refresh');
    }

    public function update_pulang()
    {
        $body = [
            'request' => [
                't_sep' => [
                    'noSep' => $this->input->post('no_sep'),
                    'statusPulang' => $this->input->post('status_pulang'),
                    'noSuratMeninggal' => $this->input->post('status_pulang') != 4 ? '' : $this->input->post('no_surat'),
                    'tglMeninggal' => $this->input->post('status_pulang') != 4 ? '' : $this->input->post('tgl_meninggal'),
                    'tglPulang' => $this->input->post('tgl_pulang'),
                    'noLPManual' => $this->input->post('noLPManual') ?? '',
                    'user' => $this->session->userdata('logged_in')->nama,
                ]
            ]
        ];

        $res = $this->put("{$this->base_url_vclaim}SEP/2.0/updtglplg", json_encode($body));

        if ($res->success) {
            $this->MainModel->update('bpjs_sep', [
                'pulang_request' => json_encode($body),
                'pulang_response' => json_encode($res->data),
                'pulang_success' => 1,
            ], $this->input->post('id'));
            $this->session->set_flashdata('success', 'Berhasil update pulang SEP!');
        }
        else {
            $this->MainModel->update('bpjs_sep', [
                'pulang_request' => json_encode($body),
                'pulang_response' => json_encode($res->server_output),
                'pulang_success' => 0,
            ], $this->input->post('id'));
            $resp = $res->server_output;
            $this->session->set_flashdata('warning', 'Gagal update pulang SEP! : ' . ($resp->metaData->message ?? ''));
        }
        redirect('bpjs/sep/daftar', 'refresh');
    }
}