<?php

class SPRI extends MY_BpjsController
{
    public function index()
    {
        $data['list'] = $this->db->query("
            SELECT rencana_kontrol.*, p.nama, p.no_rm FROM bpjs_rencana_kontrol rencana_kontrol
            JOIN pasien p ON p.id = rencana_kontrol.id_pasien AND p.is_active = 1
            WHERE rencana_kontrol.type = 'spri'
            ORDER BY rencana_kontrol.id DESC
        ")->result();
        $data['dpjp'] = $this->get_dpjp();
        $this->template->view('bpjs/spri/list', $data);
    }

    public function create()
    {
        if ($this->input->method() == 'post') {
            $body = [
                'request' => [
                    'noKartu' => $this->input->post('nomor_bjps'),
                    'kodeDokter' => $this->input->post('kode_dokter'),
                    'poliKontrol' => $this->input->post('poli_kontrol'),
                    'tglRencanaKontrol' => $this->input->post('tgl_spri'),
                    'user' => $this->session->userdata('logged_in')->nama,
                ]
            ];

            $res = $this->post("{$this->base_url_vclaim}RencanaKontrol/InsertSPRI", json_encode($body));

            if ($res->success) {
                $this->MainModel->insert('bpjs_rencana_kontrol', [
                    'id_pasien' => $this->input->post('id_pasien'),
                    'no_sep' => $this->input->post('nomor_sep'),
                    'no_bpjs' => $this->input->post('no_kartu'),
                    'request' => json_encode($body),
                    'response' => json_encode($res->data),
                    'success' => 1,
                    'type' => 'spri'
                ]);
                $this->session->set_flashdata('success', 'Berhasil menambahkan Rencana Kontrol!');
                redirect('bpjs/spri/index', 'refresh');
            }
            else {
                $this->MainModel->insert('bpjs_rencana_kontrol', [
                    'id_pasien' => $this->input->post('id_pasien'),
                    'no_sep' => $this->input->post('nomor_sep'),
                    'no_bpjs' => $this->input->post('no_kartu'),
                    'request' => json_encode($body),
                    'response' => json_encode($res->server_output),
                    'success' => 0,
                    'type' => 'spri'
                ]);
                $this->session->set_flashdata('warning', 'Gagal menambahkan Rencana Kontrol!');
                redirect('bpjs/spri/index', 'refresh');
            }
        }
        else {
            $data['dpjp'] = $this->get_dpjp();
            $data['user_id'] = $this->session->userdata('logged_in')->id;
//            $data['diagnosa_spri'] = json_decode(parent::ajax_search_diagnosa_spri())->datanya;
            $this->template->view('bpjs/spri/create', $data);
        }
    }

    public function get_poli()
    {
        $jenis_kontrol = 1;
        $nomor = urlencode($this->input->post('nomor'));
        $tgl = urlencode($this->input->post('tgl'));
        $res = $this->get("{$this->base_url_vclaim}RencanaKontrol/ListSpesialistik/JnsKontrol/$jenis_kontrol/nomor/$nomor/TglRencanaKontrol/$tgl");
        if ($res->success) {
            $json['status'] = 1;
            $json['datanya'] = $res->data->list;
        }
        else {
            $json['status'] = 0;
            $json['server_output'] = $res->server_output;
        }

        echo json_encode($json);
    }

    public function get_dokter()
    {
        $poli = urlencode($this->input->post('poli_kontrol'));
        $tgl = urlencode($this->input->post('tgl'));
        $res = $this->get("{$this->base_url_vclaim}RencanaKontrol/JadwalPraktekDokter/JnsKontrol/1/KdPoli/$poli/TglRencanaKontrol/$tgl");
        if ($res->success) {
            $json['status'] = 1;
            $json['datanya'] = $res->data->list;
        }
        else {
            $json['status'] = 0;
            $json['server_output'] = $res->server_output;
        }

        echo json_encode($json);
    }

    public function ajax_nomor_sep()
    {
        $keyword = $this->input->post('keyword');
        $pasien = $this->db->query("
            SELECT p.no_rm, p.no_bpjs as no_jaminan, p.id, p.nama, p.alamat, sep.no_sep
            FROM pasien p
            WHERE (sep.no_sep LIKE '%$keyword%' OR p.nama LIKE '%$keyword%' OR p.no_bpjs LIKE '%$keyword%')
            AND sep.no_sep IS NOT NULL
            AND sep.no_sep != ''
            AND p.is_active = 1
            AND p.no_bpjs != ''
            AND p.no_bpjs IS NOT NULL 
            LIMIT 10
        ")->result();

        if (count($pasien) > 0) {
            $json['status'] = 1;
            $json['datanya'] = "<ul class='daftar-autocomplete'>";
            foreach ($pasien as $b) {
                $json['datanya'] .= " 
						<li>
						    <span id='pasien_id' style='display: none;'>" . $b->id . "</span>
						    <span id='alamat' style='display: none;'>" . $b->alamat . "</span>
						    <span id='no_bpjs' style='display: none;'>" . $b->no_jaminan . "</span>
							<b>NO RM</b> :
							<span id='no_rm'>" . $b->no_rm . "</span> <br />
							<b>Nama</b> :
							<span id='nama'>" . $b->nama . "</span> <br />
							<b>No SEP</b> :
							<span id='no_sep'>" . $b->no_sep . "</span> <br />
						</li>
					";
            }
            $json['datanya'] .= "</ul>";
        }
        else {
            $json['status'] = 0;
            $json['count'] = count($pasien);
        }

        echo json_encode($json);
    }
}