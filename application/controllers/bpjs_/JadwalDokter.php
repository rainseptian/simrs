<?php

class JadwalDokter extends MY_RestController
{
    public function index()
    {
        $data['base_url_antrean'] = $this->base_url_antrean;
        $poli = $this->get("{$this->base_url_antrean}ref/poli");
        $data['poli'] = $poli->success ? $poli->data : (object) [
            [
                'nmpoli' => 'ANAK',
                'nmsubspesialis' => 'ANAK',
                'kdsubspesialis' => 'ANA',
                'kdpoli' => 'ANA',
            ],
        ];

        $this->template->view('bpjs/jadwal_dokter/list', $data);
    }

    public function update_jadwal()
    {
        $dokter = $this->get("{$this->base_url_antrean}ref/dokter");
        $data['dokter'] = $dokter->success ? $dokter->response->list : array_map(function ($v) {
            return [
                'namadokter' => $v['nama'],
                'kodedokter' => $v['kode'],
            ];
        }, $this->get_dpjp());

        $this->template->view('bpjs/jadwal_dokter/update', $data);
    }

    public function test()
    {
        $r = $this->get_dpjp();
        die(json_encode($r));
    }

    public function raw()
    {
        echo parent::raw();
    }
}