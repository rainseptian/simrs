<?php

require APPPATH . '/libraries/JWT.php';
require APPPATH . '/libraries/ExpiredException.php';
require APPPATH . '/libraries/BeforeValidException.php';
require APPPATH . '/libraries/SignatureInvalidException.php';
require APPPATH . '/libraries/JWK.php';

class JknMobile extends MY_RestController
{
    public function token()
    {
        $username = $this->input->request_headers()['x-username'];
        $password = $this->input->request_headers()['x-password'];

        $this->db->where('username', $username);
        $this->db->where('password', $password);
        $this->db->from('bpjs_user');
        $query = $this->db->get();
        $res = $query->result();
        if (count($res)) {
            $isitoken = [
                'user' => $username,
                'pass' => $password,
                'tipe' => 'Antrian mobile JKN BPJS Kesehatan',
                'timestamp' => time()
            ];
            $CI = &get_instance();
            $token = JWT::encode($isitoken, $CI->config->item('jwt_key'));
            return $this->send_success_response([
                'token' => $token,
            ]);
        }
        else {
            return $this->send_failed_response(201, 'Username/Password tidak sesuai');
        }
    }

    public function status_antrian()
    {
        $check = $this->check_credential();
        if (!$check['has_access']) {
            return $this->send_failed_response($check['code'], $check['message']);
        }

        $poli = $this->get("{$this->base_url_antrean}ref/poli");
        if ($poli->success) {
            $r = array_filter($poli->data, function ($v) {
                return $v->kdpoli == $this->input->post('kodepoli');
            });
            if (!count($r)) {
                return $this->send_failed_response(201, 'Poli Tidak Ditemukan');
            }
        }
        if (!preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/", $this->input->post('tanggalperiksa'))) {
            return $this->send_failed_response(201, 'Format Tanggal Tidak Sesuai, format yang benar adalah yyyy-mm-dd');
        }
        if (new DateTime($this->input->post('tanggalperiksa') . ' 00:00:00') < new DateTime(date('Y-m-d') . ' 00:00:00')) {
            return $this->send_failed_response(201, 'Tanggal Periksa Tidak Berlaku');
        }

        $kode_poli = $this->input->post('kodepoli');
        $kode_dokter = $this->input->post('kodedokter');
        $jns_pdt = $this->db->query("SELECT * FROM jenis_pendaftaran WHERE kode_bpjs LIKE '%$kode_poli%'")->row();
        $due_date = $this->input->post('tanggalperiksa');
        $dpjp = '-';
        foreach ($this->get_dpjp()->data as $v) {
            if ($v->kodedokter == $kode_dokter) {
                $dpjp = $v->namadokter;
                break;
            }
        }

        $total_antrian = $this->db->query("SELECT * FROM antrian WHERE jenis_pendaftaran_id = $jns_pdt->id AND due_date = '$due_date'")->result();
        $sisa_antrian = $this->db->query("SELECT * FROM antrian WHERE jenis_pendaftaran_id = $jns_pdt->id AND due_date = '$due_date' AND is_called = 0")->result();
        $last_antrian_terpanggil = $this->db->query("SELECT * FROM antrian WHERE jenis_pendaftaran_id = $jns_pdt->id AND due_date = '$due_date' AND is_called = 1 ORDER BY id DESC")->row();
        $total_antrian_jkn = $this->db->query("SELECT * FROM antrian JOIN pendaftaran_pasien pp ON pp.id = antrian.pendaftaran_id WHERE antrian.jenis_pendaftaran_id = $jns_pdt->id AND due_date = '$due_date' AND pp.jaminan = 'bpjs'")->result();
        $total_antrian_non_jkn = $this->db->query("SELECT * FROM antrian JOIN pendaftaran_pasien pp ON pp.id = antrian.pendaftaran_id WHERE antrian.jenis_pendaftaran_id = $jns_pdt->id AND due_date = '$due_date' AND pp.jaminan != 'bpjs'")->result();

        $response = [
            "namapoli" => $jns_pdt->nama,
            "namadokter" => $dpjp,
            "totalantrean" => count($total_antrian),
            "sisaantrean" => count($sisa_antrian),
            "antreanpanggil" => $last_antrian_terpanggil ? $last_antrian_terpanggil->kode_antrian : '',
            "sisakuotajkn" => $jns_pdt->kuota_jkn - count($total_antrian_jkn),
            "kuotajkn" => (int) $jns_pdt->kuota_jkn,
            "sisakuotanonjkn" => $jns_pdt->kuota_non_jkn - count($total_antrian_non_jkn),
            "kuotanonjkn" => (int) $jns_pdt->kuota_non_jkn,
            "keterangan" => "Datanglah minimal 30 menit, jika no antrian anda terlambat, silahkan konfirmasi ke bagian pendaftaran atau perawat poli, terimakasih",
        ];

        $this->MainModel->insert('bpjs_jknmobile_log', [
            'url' => 'status_antrian',
            'header' => json_encode($this->input->request_headers()),
            'request' => json_encode($this->input->post()),
            'response' => json_encode($response),
        ]);

        return $this->send_success_response($response);
    }

    private function get_next_kode_antrian($jenis_pendaftaran_id, $due_date)
    {
        $r = $this->db->query("
            SELECT * FROM antrian 
            WHERE date(due_date) = '$due_date' 
            AND jenis_pendaftaran_id = $jenis_pendaftaran_id 
            ORDER BY id DESC
        ")->row();
        $num = $r == null ? 0 : (int) preg_replace("/[^0-9]/", "", $r->kode_antrian);
        $num = str_pad(++$num, 3, "0", STR_PAD_LEFT);

        $j = $this->db->query("SELECT * FROM jenis_pendaftaran WHERE id = $jenis_pendaftaran_id")->row();
        return "$j->kode_antrian$num";
    }

    public function antrian()
    {
        $check = $this->check_credential();
        if (!$check['has_access']) {
            return $this->send_failed_response($check['code'], $check['message']);
        }

        $due_date = $this->input->post('tanggalperiksa');
        $no_bpjs = $this->input->post('nomorkartu');
        $kode_poli = $this->input->post('kodepoli');
        $jns_pdt = $this->db->query("SELECT * FROM jenis_pendaftaran WHERE kode_bpjs LIKE '%$kode_poli%'")->row();

        $kode_dokter = $this->input->post('kodedokter');
        $dpjp = 250;
        $nama_dpjp = 'dr. Zakky Sukmajaya, Sp.OG';
        $dok = $this->db->query("SELECT * FROM user WHERE bpjs_id = $kode_dokter")->row();
        if ($dok) {
            $dpjp = $dok->id;
            $nama_dpjp = $dok->nama;
        }

        $exist = $this->db->query("
            SELECT * FROM antrian a 
            JOIN pasien p ON p.id = a.pasien_id
            WHERE p.no_bpjs = '$no_bpjs' AND a.jenis_pendaftaran_id = $jns_pdt->id AND due_date = '$due_date'
        ")->row();
        if ($exist) {
            return $this->send_failed_response(201, 'Nomor Antrean Hanya Dapat Diambil 1 Kali Pada Tanggal Yang Sama');
        }

        $jadwal = $this->get("{$this->base_url_antrean}jadwaldokter/kodepoli/$kode_poli/tanggal/$due_date");
        if (!$jadwal->success) {
            return $this->send_failed_response(201, 'Pendaftaran ke Poli Ini Sedang Tutup');
        }
        else {
            $jad = false;
            foreach ($jadwal->data as $v) {
                if ($v->kodedokter == $kode_dokter) {
                    $jad = $v;
                    break;
                }
            }

            if (!$jad) {
                return $this->send_failed_response(201, "Jadwal Dokter $nama_dpjp Tersebut Belum Tersedia, Silahkan Reschedule Tanggal dan Jam Praktek Lainnya");
            }

            $jad = explode('-', $jad->jadwal);
            if (strtotime(date('Y-m-d H:i')) > strtotime(date('Y-m-d') . " $jad[1]")) {
                return $this->send_failed_response(201, "Pendaftaran Ke $jns_pdt->nama Sudah Tutup Jam $jad[1]");
            }
        }

        $nik = $this->input->post('nik');
        $no_rm = $this->input->post('norm');
        $q = 'SELECT * FROM pasien WHERE is_active = 1 ';
        $w = [];
        if ($no_bpjs)
            $w []= "no_bpjs = '$no_bpjs'";
        if ($nik)
            $w []= "nik = '$nik'";
        if ($no_rm)
            $w []= "no_rm = '$no_rm'";
        if (count($w)) {
            $j = implode(' OR ', $w);
            $q .= "AND ($j)";
        }
        $pasien = $this->db->query($q)->row();

        if (!$pasien) {
            return $this->send_failed_response(202, "Data pasien ini tidak ditemukan, silahkan Melakukan Registrasi Pasien Baru");
        }

        $is_pasien_new = false;
        $no_rm = $pasien->no_rm;
        $pasien_id = $pasien->id;

        $idJenisPendaftaranYangLangsungKePemeriksaan = [19, 25, 44, 45, 40, 41, 42, 43, 59, 58];
        $apakaLangsungKePemeriksaan = in_array($jns_pdt->id, $idJenisPendaftaranYangLangsungKePemeriksaan);
        $jaminan = 'bpjs';

        $pdf_id = $this->MainModel->insert_id('pendaftaran_pasien', [
            'td' => 0,
            'r' => 0,
            'bb' => 0,
            'n' => 0,
            's' => 0,
            'tb' => 0,
            'bmi' => 0,
            'no_rm' => $no_rm,
            'pasien' => $pasien_id,
            'penanggungjawab' => 'JKN Mobile',
            'biopsikososial' => '',
            'jenis_pendaftaran_id' => $jns_pdt->id,
            'jaminan' => $jaminan,
            'no_jaminan' => $no_bpjs,
            'dokter' => $dpjp,
            'status' => $apakaLangsungKePemeriksaan ? 'diperiksa' : 'antri',
            'is_bpjs' => 1,
            'creator' => 1,
            'created_at' => $due_date,
            'waktu_pendaftaran' => $due_date,
            'tipe_daftar' => 'mjkn'
        ]);

        $pemeriksaan_id = null;
        if ($apakaLangsungKePemeriksaan) {
            $pemeriksaan_id = $this->MainModel->insert_id('pemeriksaan', [
                'pendaftaran_id' => $pdf_id,
                'dokter_id' => $dpjp,
                'pasien_id' => $pasien_id,
                'perawat_id' => '',
                'no_rm' => $no_rm,
                'nama_pasien' => '',
                'keluhan_utama' => '',
                'diagnosa_perawat' => '',
                'asuhan_keperawatan' => '',
                'bmi' => '',
                'td' => '',
                'r' => '',
                'bb' => '',
                'n' => '',
                's' => '',
                'tb' => '',
                'is_bpjs' => 1,
                'jaminan' => $jaminan,
                'status' => 'sudah_periksa_awal',
                'creator' => 1
            ]);
        }

        $kode_booking = generateKodeBooking();

        $antrian_id = $this->MainModel->insert_id('antrian', [
            'pendaftaran_id' => $pdf_id,
            'pemeriksaan_id' => $pemeriksaan_id,
            'jenis_pendaftaran_id' => $jns_pdt->id,
            'pasien_id' => $pasien_id,
            'due_date' => $due_date,
            'kode_antrian' => $this->get_next_kode_antrian($jns_pdt->id, $due_date),
            'is_mobile_jkn' => 1,
            'bpjs_jam_praktek' => $this->input->post('jampraktek'),
            'bpjs_jenis_kunjungan' => $this->input->post('jeniskunjungan'),
            'bpjs_nomor_referensi' => $this->input->post('nomorreferensi'),
            'request' => json_encode($this->input->post()),
            'is_new_pasien' => $is_pasien_new ? 1 : 0,
            'kode_booking' => $kode_booking
        ]);

        $ant = $this->db->query("SELECT * FROM antrian WHERE id = $antrian_id")->row();
        $total_antrian_jkn = $this->db->query("SELECT * FROM antrian JOIN pendaftaran_pasien pp ON pp.id = antrian.pendaftaran_id WHERE antrian.jenis_pendaftaran_id = $jns_pdt->id AND due_date = '$due_date' AND pp.jaminan = 'bpjs'")->result();
        $total_antrian_non_jkn = $this->db->query("SELECT * FROM antrian JOIN pendaftaran_pasien pp ON pp.id = antrian.pendaftaran_id WHERE antrian.jenis_pendaftaran_id = $jns_pdt->id AND due_date = '$due_date' AND pp.jaminan != 'bpjs'")->result();
        $sisa_antrian = $this->db->query("SELECT * FROM antrian WHERE jenis_pendaftaran_id = $jns_pdt->id AND due_date = '$due_date' AND is_called = 0")->result();

        if ($due_date == date('Y-m-d')) {
            $c = count($sisa_antrian);
            $b = ($c - 1) * 15;
            $t = strtotime("+$b minutes");
        }
        else {
            $a = (int) preg_replace("/[^0-9]/", "", $ant->kode_antrian);
            $b = ($a - 1) * 15;
            $t = strtotime("+$b minutes", strtotime($due_date . ' 08:00'));
        }

        $response = [
            "nomorantrean" => $ant->kode_antrian,
            "angkaantrean" => (int) preg_replace("/[^0-9]/", "", $ant->kode_antrian),
            "kodebooking" => $kode_booking,
            "norm" => $no_rm,
            "namapoli" => $jns_pdt->nama,
            "namadokter" => $nama_dpjp,
            "estimasidilayani" => "{$t}000",
            "sisakuotajkn" => $jns_pdt->kuota_jkn - count($total_antrian_jkn),
            "kuotajkn" => (int) $jns_pdt->kuota_jkn,
            "sisakuotanonjkn" => $jns_pdt->kuota_non_jkn - count($total_antrian_non_jkn),
            "kuotanonjkn" => (int) $jns_pdt->kuota_non_jkn,
            "keterangan" => "Peserta harap 60 menit lebih awal guna pencatatan administrasi.",
        ];
        $this->MainModel->insert('bpjs_jknmobile_log', [
            'url' => 'antrian',
            'header' => json_encode($this->input->request_headers()),
            'request' => json_encode($this->input->post()),
            'response' => json_encode($response),
            'ws_bpjs_request' => '',
            'ws_bpjs_response' => '',
        ]);

        return $this->send_success_response($response, $is_pasien_new ? 202 : 200);
    }

    public function sisa_antrian()
    {
        $check = $this->check_credential();
        if (!$check['has_access']) {
            return $this->send_failed_response($check['code'], $check['message']);
        }

        $kode_booking = $this->input->post('kodebooking');
        $antrian = $this->db->query("
            SELECT a.*, jp.nama as nama_poli, u.nama as nama_dokter FROM antrian a
            JOIN jenis_pendaftaran jp ON jp.id = a.jenis_pendaftaran_id 
            JOIN pendaftaran_pasien pp ON pp.id = a.pendaftaran_id
            JOIN user u ON u.id = pp.dokter
            WHERE a.kode_booking = '$kode_booking'
        ")->row();
        if (!$antrian) {
            return $this->send_failed_response(201, 'Antrean Tidak Ditemukan');
        }

        $last_antrian_terpanggil = $this->db->query("
            SELECT * FROM antrian 
            WHERE jenis_pendaftaran_id = $antrian->jenis_pendaftaran_id 
            AND due_date = '$antrian->due_date' 
            AND is_called = 1
            ORDER BY id
        ")->row();

        $kode_antrian_terpanggil = $last_antrian_terpanggil ? $last_antrian_terpanggil->kode_antrian : '-';
        $kode_antrian_terpanggil_num = !$kode_antrian_terpanggil ? 0 : (int) preg_replace("/[^0-9]/", "", $kode_antrian_terpanggil);
        $kode_antrian_num = (int) preg_replace("/[^0-9]/", "", $antrian->kode_antrian);
        $sisa_antrian = $kode_antrian_num - $kode_antrian_terpanggil_num;

        $response = [
            "nomorantrean" => $antrian->kode_antrian,
            "namapoli" => $antrian->nama_poli,
            "namadokter" => $antrian->nama_dokter,
            "sisaantrean" => $sisa_antrian,
            "antreanpanggil" => $kode_antrian_terpanggil,
            "waktutunggu" => ($sisa_antrian - 1) * 15 * 60,
            "keterangan" => '',
        ];
        $this->MainModel->insert('bpjs_jknmobile_log', [
            'url' => 'sisa_antrian',
            'header' => json_encode($this->input->request_headers()),
            'request' => json_encode($this->input->post()),
            'response' => json_encode($response),
        ]);
        return $this->send_success_response($response, 200);
    }

    public function batal_antrian()
    {
        $check = $this->check_credential();
        if (!$check['has_access']) {
            return $this->send_failed_response($check['code'], $check['message']);
        }

        $kode_booking = $this->input->post('kodebooking');
        $antrian = $this->db->query("
            SELECT a.*, jp.nama as nama_poli, u.nama as nama_dokter FROM antrian a
            JOIN jenis_pendaftaran jp ON jp.id = a.jenis_pendaftaran_id 
            JOIN pendaftaran_pasien pp ON pp.id = a.pendaftaran_id
            JOIN user u ON u.id = pp.dokter
            WHERE a.kode_booking = '$kode_booking'
        ")->row();
        if (!$antrian) {
            return $this->send_failed_response(201, 'Antrean Tidak Ditemukan atau Sudah Dibatalkan');
        }

        $pdftrn = $this->db->query("SELECT * FROM pendaftaran_pasien WHERE id = $antrian->pendaftaran_id")->row();
        if ($pdftrn->status != 'antri') {
            return $this->send_failed_response(201, 'Pasien Sudah Dilayani, Antrean Tidak Dapat Dibatalkan');
        }

        $w = $this->post("{$this->base_url_antrean}antrean/batal", json_encode([
            'kodebooking' => $kode_booking,
            'keterangan' => $this->input->post('keterangan'),
        ]));

        $this->db->query("UPDATE pendaftaran_pasien SET is_active = '0' WHERE id = $antrian->pendaftaran_id");
        $this->db->query("DELETE FROM antrian WHERE id = $antrian->id");

        $this->MainModel->insert('bpjs_jknmobile_log', [
            'url' => 'batal_antrian',
            'header' => json_encode($this->input->request_headers()),
            'request' => json_encode($this->input->post()),
            'response' => '',
            'ws_bpjs_request' => json_encode([
                'kodebooking' => $kode_booking,
                'keterangan' => $this->input->post('keterangan'),
            ]),
            'ws_bpjs_response' => json_encode($w)
        ]);
        return $this->send_success_response('');
    }

    public function checkin()
    {
        $check = $this->check_credential();
        if (!$check['has_access']) {
            return $this->send_failed_response($check['code'], $check['message']);
        }

        $kode_booking = $this->input->post('kodebooking');
        $antrian = $this->db->query("
            SELECT a.*, jp.nama as nama_poli, u.nama as nama_dokter FROM antrian a
            JOIN jenis_pendaftaran jp ON jp.id = a.jenis_pendaftaran_id 
            JOIN pendaftaran_pasien pp ON pp.id = a.pendaftaran_id
            JOIN user u ON u.id = pp.dokter
            WHERE a.kode_booking = '$kode_booking'
        ")->row();

        $tgl = date('Y-m-d H:i', $this->input->post('waktu') / 1000);
        $this->MainModel->update('antrian', [
            'is_check_in' => 1,
            'check_in_at' => $tgl
        ], $antrian->id);

        $this->MainModel->insert('bpjs_jknmobile_log', [
            'url' => 'checkin',
            'header' => json_encode($this->input->request_headers()),
            'request' => json_encode($this->input->post()),
            'response' => '',
        ]);

        try {
            $task_body = ['kodebooking' => $kode_booking, 'taskid' => 3, 'waktu' => time().'000'];
            $this->MainModel->insert('bpjs_jknmobile_log', [
                'url' => 'antrean/updatewaktu',
                'header' => 'task 3',
                'request' => json_encode($task_body),
                'response' => json_encode($this->post("{$this->base_url_antrean}antrean/updatewaktu", json_encode($task_body), true)),
            ]);
        }
        catch (Exception $e) {

        }

        return $this->send_success_response('');
    }

    public function pasien_baru()
    {
        $check = $this->check_credential();
        if (!$check['has_access']) {
            return $this->send_failed_response($check['code'], $check['message']);
        }

        $no_bpjs = $this->input->post('nomorkartu');
        $nik = $this->input->post('nik');
        $kk = $this->input->post('nomorkk');
        $nama = $this->input->post('nama');
        $jk = $this->input->post('jeniskelamin');
        $tanggal_lahir = $this->input->post('tanggallahir');
        $alamat = $this->input->post('alamat');
        $kodeprop = $this->input->post('kodeprop');
        $namaprop = $this->input->post('namaprop');
        $kodedati2 = $this->input->post('kodedati2');
        $namadati2 = $this->input->post('namadati2');
        $kodekec = $this->input->post('kodekec');
        $namakec = $this->input->post('namakec');
        $kodekel = $this->input->post('kodekel');
        $namakel = $this->input->post('namakel');
        $rw = $this->input->post('rw');
        $rt = $this->input->post('rt');

        if (!$no_bpjs)
            return $this->send_failed_response(201, "Nomor Kartu Belum Diisi");
        if (!is_numeric($no_bpjs) || strlen($no_bpjs) != 13)
            return $this->send_failed_response(201, "Format Nomor Kartu Tidak Sesuai");
        if (!$nik)
            return $this->send_failed_response(201, "NIK Belum Diisi");
        if (!is_numeric($nik) || strlen($nik) != 16)
            return $this->send_failed_response(201, "Format NIK hanya numeric dan 16 digit");
        if (!$kk)
            return $this->send_failed_response(201, "Nomor KK Belum Diisi");
        if (!$nama)
            return $this->send_failed_response(201, "Nama Belum Diisi");
        if (!$jk)
            return $this->send_failed_response(201, "Jenis Kelamin Belum Dipilih");
        if (!$tanggal_lahir)
            return $this->send_failed_response(201, "Tanggal Lahir Belum Diisi");
        if (!$alamat)
            return $this->send_failed_response(201, "Alamat Belum Diisi");
        if (!$kodeprop)
            return $this->send_failed_response(201, "Kode Propinsi Belum Diisi");
        if (!$namaprop)
            return $this->send_failed_response(201, "Nama Propinsi Belum Diisi");
        if (!$kodedati2)
            return $this->send_failed_response(201, "Kode Dati 2 Belum Diisi");
        if (!$namadati2)
            return $this->send_failed_response(201, "Dati 2 Belum Diisi");
        if (!$kodekec)
            return $this->send_failed_response(201, "Kode Kecamatan Belum Diisi");
        if (!$namakec)
            return $this->send_failed_response(201, "Kecamatan Belum Diisi");
        if (!$kodekel)
            return $this->send_failed_response(201, "Kode Kelurahan Belum Diisi");
        if (!$namakel)
            return $this->send_failed_response(201, "Kelurahan Belum Diisi");
        if (!$rw)
            return $this->send_failed_response(201, "RW Belum Diisi");
        if (!$rt)
            return $this->send_failed_response(201, "RT Belum Diisi");
        if (!preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/", $tanggal_lahir) || new DateTime($tanggal_lahir . ' 00:00:00') > new DateTime(date('Y-m-d') . ' 23:59:59'))
            return $this->send_failed_response(201, 'Format Tanggal Lahir Tidak Sesuai');

        $p = $this->db->query("SELECT * FROM pasien WHERE no_bpjs = '$no_bpjs'")->row();
        if ($p) {
            return $this->send_failed_response(201, "Data Peserta Sudah Pernah Dientrikan");
        }
        else {
            $norm = $this->PasienModel->getNoRmAuto();

            $this->MainModel->insert('pasien', [
                'no_rm' => $norm,
                'no_bpjs' => $no_bpjs,
                'nama' => $nama,
                'nik' => $nik,
                'kk' => $kk,
                'tanggal_lahir' => $this->input->post('tanggallahir'),
                'tempat_lahir' => $namadati2,
                'usia' => get_usia($this->input->post('tanggallahir')),
                'jk' => $jk,
                'alamat' => $alamat,
                'alamat_domisili' => $alamat,
                'telepon' => $this->input->post('nohp'),
                'pekerjaan' => '',
                'agama' => '',
                'tingkat_pendidikan' => '',
                'penanggungjawab' => 'MJKN',
                'provinsi_bpjs' => $kodeprop,
                'kabupaten_bpjs' => $kodedati2,
                'kecamatan_bpjs' => $kodekec,
                'desa_bpjs' => $kodekel,
                'rw_bpjs' => $rw,
                'rt_bpjs' => $rt,
                'creator' => 1,
            ]);
        }

        return $this->send_success_response(['norm' => $norm], 200, 'Harap datang ke admisi untuk melengkapi data rekam medis');
    }

    public function list_kode_booking_operasi()
    {
        $check = $this->check_credential();
        if (!$check['has_access']) {
            return $this->send_failed_response($check['code'], $check['message']);
        }

        $tanggalawal = $this->input->post('tanggalawal');
        $tanggalakhir = $this->input->post('tanggalakhir');

        if (new DateTime($tanggalawal . ' 00:00:00') > new DateTime($tanggalakhir . ' 00:00:00')) {
            return $this->send_failed_response(201, 'Tanggal Akhir Tidak Boleh Lebih Kecil dari Tanggal Awal');
        }

        $data = $this->db
            ->select('
                p.no_bpjs, pp.no_jaminan, pp.pendaftaran_id as prev_pendaftaran_id,
                jp.nama as nama_poli, jp.kode_bpjs, jp.id as jp_id, a.kode_booking,
                transfered_at, diperiksa_at, date(operasi_at) as operasi_at, selesai_at, tt.nama as nama_tindakan
            ')
            ->from('transfer t')
            ->join('rawat_inap r', 'r.id = t.rawat_inap_id')
            ->join('pasien p', 'p.id = r.pasien_id')
            ->join('pendaftaran_pasien pp', 'pp.id = r.pendaftaran_id', 'left')
            ->join('jenis_pendaftaran jp', 'jp.id = pp.jenis_pendaftaran_id', 'left')
            ->join('antrian a', 'pp.id = a.pendaftaran_id', 'left')

            ->join('ri_biaya by', 'by.rawat_inap_id = r.id', 'left')
            ->join('ri_biaya_tindakan byt', 'byt.ri_biaya_id = by.id', 'left')
            ->join('tarif_tindakan tt', 'tt.id = byt.tindakan_id', 'left')
            ->group_by('t.id')

            ->where('r.is_active', '1')
            ->where('t.is_active', '1')
            ->where('t.current_place_type', 'ruang operasi')
            ->where('date(t.operasi_at) >=', $tanggalawal)
            ->where('date(t.operasi_at) <=', $tanggalakhir)
            ->order_by('t.operasi_at')
            ->get()
            ->result();

        $res = [];
        foreach ($data as $v) {
            if (!$v->kode_booking) {
                continue;
            }

            if ($v->jp_id == 19 && $v->prev_pendaftaran_id) { // lab
                $jp = $this->db->select("SELECT * FROM jenis_pendaftaran WHERE id = $v->prev_pendaftaran_id")->row();
                $kodepoli = !$jp->kode_bpjs ? '-' : explode(',', $jp->kode_bpjs)[0];
                $namapoli = $jp->nama_poli;
            }
            else {
                $kodepoli = !$v->kode_bpjs ? '-' : explode(',', $v->kode_bpjs)[0];
                $namapoli = $v->nama_poli;
            }

            $res []= [
                'kodebooking' => $v->kode_booking,
                'tanggaloperasi' => $v->operasi_at,
                'jenistindakan' => $v->nama_tindakan ?? '-',
                'kodepoli' => $kodepoli,
                'namapoli' => $namapoli,
                'terlaksana' => ($v->diperiksa_at && $v->operasi_at <= date('Y-m-d')) ? 1 : 0,
                'nopeserta' => $v->no_bpjs ?: ($v->no_jaminan ?: ''),
                'lastupdate' => strtotime($v->selesai_at ?? $v->diperiksa_at ?? $v->transfered_at)
            ];
        }

        return $this->send_success_response([
            'list' => $res,
        ]);
    }

    public function list_jadwal_operasi()
    {
        $check = $this->check_credential();
        if (!$check['has_access']) {
            return $this->send_failed_response($check['code'], $check['message']);
        }

        $no_bpjs = $this->input->post('nopeserta');
        if (!is_numeric($no_bpjs) || strlen($no_bpjs) != 13)
            return $this->send_failed_response(201, "Jumlah nomor kartu adalah 13 digit (Numeric)");

        $data = $this->db
            ->select('
                r.id, r.tipe_pasien, t.id as transfer_id, p.nama as nama_pasien, p.no_bpjs, pp.no_jaminan, p.alamat, pp.pendaftaran_id as prev_pendaftaran_id,
                u.nama as nama_dokter, jp.nama as nama_poli, jp.kode_bpjs, jp.id as jp_id, t.via_bangsal, t.jenis, a.kode_booking,
                transfered_at, diperiksa_at, selesai_at, date(operasi_at) as operasi_at, tt.nama as nama_tindakan
            ')
            ->from('transfer t')
            ->join('rawat_inap r', 'r.id = t.rawat_inap_id')
            ->join('pasien p', 'p.id = r.pasien_id')
            ->join('user u', 'u.id = r.dokter_id')
            ->join('pendaftaran_pasien pp', 'pp.id = r.pendaftaran_id', 'left')
            ->join('jenis_pendaftaran jp', 'jp.id = pp.jenis_pendaftaran_id', 'left')
            ->join('antrian a', 'pp.id = a.pendaftaran_id', 'left')

            ->join('ri_biaya by', 'by.rawat_inap_id = r.id', 'left')
            ->join('ri_biaya_tindakan byt', 'byt.ri_biaya_id = by.id', 'left')
            ->join('tarif_tindakan tt', 'tt.id = byt.tindakan_id', 'left')
            ->group_by('t.id')

            ->where('r.is_active', '1')
            ->where('t.is_active', '1')
            ->where('t.current_place_type', 'ruang operasi')
            ->where('(p.no_bpjs =', $this->input->post('nopeserta'), FALSE)
            ->or_where("pp.no_jaminan = {$this->input->post('nopeserta')})", NULL, FALSE)
            ->order_by('t.operasi_at')
            ->get()
            ->result();

        $res = [];
        foreach ($data as $v) {
            if ($v->jp_id == 19 && $v->prev_pendaftaran_id) { // lab
                $jp = $this->db->select("SELECT * FROM jenis_pendaftaran WHERE id = $v->prev_pendaftaran_id")->row();
                $kodepoli = !$jp->kode_bpjs ? '-' : explode(',', $jp->kode_bpjs)[0];
                $namapoli = $jp->nama_poli;
            }
            else {
                $kodepoli = !$v->kode_bpjs ? '-' : explode(',', $v->kode_bpjs)[0];
                $namapoli = $v->nama_poli;
            }

            $res []= [
                'kodebooking' => $v->kode_booking ?? '',
                'tanggaloperasi' => $v->operasi_at,
                'jenistindakan' => $v->nama_tindakan ?? '-',
                'kodepoli' => $kodepoli ?? '',
                'namapoli' => $namapoli ?? '',
                'terlaksana' => ($v->diperiksa_at && $v->operasi_at <= date('Y-m-d')) ? 1 : 0
            ];
        }

        return $this->send_success_response([
            'list' => $res,
        ]);
    }
}