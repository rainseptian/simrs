<?php

class Laporan extends MY_BpjsController
{
    public function kunjungan()
    {
        $this->template->view('bpjs/laporan/kunjungan');
    }

    public function klaim()
    {
        $this->template->view('bpjs/laporan/klaim');
    }

    public function history_pelayanan_peserta()
    {
        $this->template->view('bpjs/laporan/history_pelayanan_peserta');
    }

    public function klaim_jaminan_jasa_raharja()
    {
        $this->template->view('bpjs/laporan/klaim_jaminan_jasa_raharja');
    }

    public function ajax_laporan_kunjungan()
    {
        echo parent::ajax_laporan_kunjungan();
    }

    public function ajax_laporan_klaim()
    {
        echo parent::ajax_laporan_klaim();
    }

    public function ajax_laporan_history_pelayanan_peserta()
    {
        echo parent::ajax_laporan_history_pelayanan_peserta();
    }

    public function ajax_laporan_klaim_jaminan_jasa_raharja()
    {
        echo parent::ajax_laporan_klaim_jaminan_jasa_raharja();
    }
}