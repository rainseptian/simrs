<?php

class RencanaKontrol extends MY_BpjsController
{
    public function __construct()
    {
        parent::__construct();

        $this->load->Model('AdministrasiModel');
        $this->load->helper(['kode_booking', 'usia']);
    }

    public function index()
    {
        $this->template->view('bpjs/rencana_kontrol/index');
    }

    public function ajax_search_spesialistik_rencana_kontrol()
    {
         parent::ajax_search_spesialistik_rencana_kontrol();
    }

    public function ajax_search_jadwal_dokter_rencana_kontrol()
    {
         parent::ajax_search_jadwal_dokter_rencana_kontrol();
    }

    public function daftar()
    {
        $list = $this->db->query("
            SELECT rencana_kontrol.*, p.nama, p.no_rm FROM bpjs_rencana_kontrol rencana_kontrol
            LEFT JOIN pasien p ON p.id = rencana_kontrol.id_pasien AND p.is_active = 1
            ORDER BY rencana_kontrol.id DESC
        ")->result();

        $this->template->view('bpjs/rencana_kontrol/list', compact('list'));
    }

    public function create_kontrol()
    {
        $data['sep'] = json_decode($this->input->post('sep'));
        $data['spesialistik'] = json_decode($this->input->post('spesialistik'));
        $poli = urlencode($data['sep']->poli);
        $res = $this->ajax_search("{$this->base_url_vclaim}referensi/poli/$poli", 'poli');
        $poli = json_decode($res);
        $data['poli'] = $poli->datanya[0];
        $data['jenis_kontrol'] = 2; // Jenis kontrol --> 1: SPRI, 2: Rencana Kontrol
        $data['nomor'] = $data['sep']->noSep; // Nomor --> jika jenis kontrol = 1, maka diisi nomor kartu; jika jenis kontrol = 2, maka diisi nomor SEP

        $this->template->view('bpjs/rencana_kontrol/create_kontrol', $data);
    }

    public function insert_kontrol()
    {
        $body = [
            'request' => [
                'noSEP' => $this->input->post('noSEP'),
                'kodeDokter' => $this->input->post('kode_dokter'),
                'poliKontrol' => $this->input->post('poli_kontrol'),
                'tglRencanaKontrol' => $this->input->post('tglRencanaKontrol'),
                'user' => $this->session->userdata('logged_in')->nama,
            ]
        ];

        $res = $this->post("{$this->base_url_vclaim}RencanaKontrol/insert", json_encode($body));

        if ($res->success) {
            $id = $this->MainModel->insert_id('bpjs_rencana_kontrol', [
                'id_pasien' => $this->input->post('id_pasien') ?? 0,
                'no_sep' => $this->input->post('noSEP'),
                'request' => json_encode($body),
                'response' => json_encode($res->data),
                'success' => 1,
            ]);
            $insert_antrian = $this->insert_kunjungan(
                $res->data->noSuratKontrol,
                $this->input->post('poli_kontrol'),
                $this->input->post('kode_dokter')
            );
            $this->MainModel->update('bpjs_rencana_kontrol', ['insert_antrian' => $insert_antrian], $id);
            $this->session->set_flashdata('success', 'Berhasil menambahkan Rencana Kontrol!');
            redirect('bpjs/rencana_kontrol/index', 'refresh');
        }
        else {
            $this->MainModel->insert('bpjs_rencana_kontrol', [
                'id_pasien' => $this->input->post('id_pasien') ?? 0,
                'no_sep' => $this->input->post('noSEP'),
                'request' => json_encode($body),
                'response' => json_encode($res->server_output),
                'success' => 0
            ]);
            $this->session->set_flashdata('warning', $res->server_output->metaData->message ?? 'Gagal menambahkan Rencana Kontrol!');
            redirect('bpjs/rencana_kontrol/index', 'refresh');
        }
    }

    private function insert_kunjungan($noSuratKontrol, $poli_kontrol, $kode_dokter)
    {
        try {
            $no_rm = $this->input->post('no_rm');
            $pasien = $this->db->query("SELECT * FROM pasien WHERE no_rm = $no_rm")->row();
            $pendaftaran = $this->db->query("
                SELECT * FROM pendaftaran_pasien 
                WHERE pasien = $pasien->id 
                AND pendaftaran_id IS NULL
                ORDER BY id DESC 
            ")->row();

            $jp = $this->db->query("SELECT * FROM jenis_pendaftaran WHERE is_active = 1 And status = 1")->result();
            $jp_id = null;
            foreach ($jp as $v) {
                $kd = explode(',', $v->kode_bpjs);
                foreach ($kd as $k) {
                    if ($k == $poli_kontrol) {
                        $jp_id = $v->id;
                        break;
                    }
                }
                if ($jp_id) {
                    break;
                }
            }
            if (!$jp_id) {
                $jp_id = $pendaftaran->jenis_pendaftaran_id;
            }

            $ref_dokter = $this->db->query("SELECT * FROM user WHERE is_active = 1")->result();
            $dokter_id = null;
            foreach ($ref_dokter as $v) {
                if ($v->bpjs_id == $kode_dokter) {
                    $dokter_id = $v->id;
                    break;
                }
            }
            if (!$dokter_id) {
                $dokter_id = $pendaftaran->dokter;
            }

            if ($pasien && $pendaftaran) {
                $rm = array(
                    'td' => 0,
                    'r' => 0,
                    'bb' => 0,
                    'n' => 0,
                    's' => 0,
                    'tb' => 0,
                    'bmi' => 0,
                    'no_rm' => $no_rm,
                    'pasien' => $pasien->id,
                    'penanggungjawab' => 'Rencana Kontrol',
                    'biopsikososial' => '',
                    'jenis_pendaftaran_id' => $jp_id,
                    'jaminan' => 'bpjs',
                    'no_jaminan' => $pendaftaran->no_jaminan,
                    'dokter' => $dokter_id,
                    'status' => 'antri',
                    'is_bpjs' => 1,
                    'creator' => 1,
                    'created_at' => $this->input->post('tglRencanaKontrol'),
                    'waktu_pendaftaran' => $this->input->post('tglRencanaKontrol'),
                    'tipe_daftar' => 'kontrol'
                );
                $insert = $this->MainModel->insert_id('pendaftaran_pasien', $rm);

                $kode_booking = generateKodeBooking();
                $id_antrian = $this->MainModel->insert_id('antrian', [
                    'pendaftaran_id' => $insert,
                    'pemeriksaan_id' => null,
                    'jenis_pendaftaran_id' => $jp_id,
                    'pasien_id' => $pasien->id,
                    'due_date' => $this->input->post('tglRencanaKontrol'),
                    'kode_antrian' => generateKodeAntrian($jp_id, $this->input->post('tglRencanaKontrol')),
                    'is_check_in' => false,
                    'kode_booking' => $kode_booking
                ]);

                $jns = $this->db->query("SELECT * FROM jenis_pendaftaran WHERE id = {$jp_id}")->row();
                $dok = $this->db->query("SELECT * FROM user WHERE id = {$dokter_id}")->row();
                $ant = $this->db->query("SELECT * FROM antrian WHERE id = $id_antrian")->row();
                $due_date = $this->input->post('tglRencanaKontrol');
                $total_antrian_jkn = $this->db->query("SELECT * FROM antrian JOIN pendaftaran_pasien pp ON pp.id = antrian.pendaftaran_id WHERE antrian.jenis_pendaftaran_id = $jns->id AND due_date = '$due_date' AND pp.jaminan = 'bpjs'")->result();
                $total_antrian_non_jkn = $this->db->query("SELECT * FROM antrian JOIN pendaftaran_pasien pp ON pp.id = antrian.pendaftaran_id WHERE antrian.jenis_pendaftaran_id = $jns->id AND due_date = '$due_date' AND pp.jaminan != 'bpjs'")->result();
                $sisa_antrian = $this->db->query("SELECT * FROM antrian WHERE jenis_pendaftaran_id = $jns->id AND due_date = '$due_date' AND is_called = 0")->result();
                $c = count($sisa_antrian);
                $b = ($c - 1) * 15;
                $t = strtotime("+$b minutes");
                $body = [
                    "kodebooking" => $kode_booking,
                    "jenispasien" => 'JKN',
                    "nomorkartu" => $pendaftaran->no_jaminan,
                    'nik' => $pasien->nik,
                    'nohp' => $pasien->telepon,
                    'kodepoli' => $jns->kode_bpjs ? explode(',', $jns->kode_bpjs)[0] : '',
                    "namapoli" => $jns->nama,
                    "pasienbaru" => 0,
                    "norm" => $no_rm,
                    "tanggalperiksa" => $this->input->post('tglRencanaKontrol'),
                    "kodedokter" => (int)($dok->bpjs_id ?? '0'),
                    "namadokter" => $dok->nama,
                    "jampraktek" => '08:00-16:00',
                    "jeniskunjungan" => '3',
                    "nomorreferensi" => $noSuratKontrol ?? '',
                    "nomorantrean" => $ant->kode_antrian,
                    "angkaantrean" => (int) preg_replace("/[^0-9]/", "", $ant->kode_antrian),
                    "estimasidilayani" => $t,
                    "sisakuotajkn" => $jns->kuota_jkn - count($total_antrian_jkn),
                    "kuotajkn" => (int) $jns->kuota_jkn,
                    "sisakuotanonjkn" => $jns->kuota_non_jkn - count($total_antrian_non_jkn),
                    "kuotanonjkn" => (int) $jns->kuota_non_jkn,
                    "keterangan" => "Peserta harap 60 menit lebih awal guna pencatatan administrasi.",
                ];
                $res = $this->post("{$this->base_url_antrean}antrean/add", json_encode($body), true);
                $bpjs_jknmobile_log_id = $this->MainModel->insert_id('bpjs_jknmobile_log', [
                    'url' => 'antrean/add',
                    'header' => 'pend-lama',
                    'request' => json_encode($body),
                    'response' => json_encode($res),
                    'ws_bpjs_request' => '',
                    'ws_bpjs_response' => '',
                    'kode_booking' => $kode_booking,
                ]);

                return json_encode([
                    'pendaftaran_pasien' => $insert,
                    'antrian' => $id_antrian,
                    'bpjs_jknmobile_log' => $bpjs_jknmobile_log_id,
                    'no' => $noSuratKontrol
                ]);
            }

            return json_encode([
                'error' => true,
                'msg' => "$no_rm not found",
                'no' => $noSuratKontrol
            ]);
        }
        catch (Throwable $t) {
            return json_encode([
                'error' => true,
                'msg' => $t->getMessage(),
                'no' => $noSuratKontrol
            ]);
        }
    }

    public function update_kontrol()
    {
        $body = [
            'request' => [
                'noSuratKontrol' => $this->input->post('noSuratKontrol'),
                'noSEP' => $this->input->post('noSEP'),
                'kodeDokter' => $this->input->post('kode_dokter'),
                'poliKontrol' => $this->input->post('poli_kontrol'),
                'tglRencanaKontrol' => $this->input->post('tglRencanaKontrol'),
                'user' => $this->session->userdata('logged_in')->nama,
            ]
        ];

        $res = $this->put("{$this->base_url_vclaim}RencanaKontrol/Update", json_encode($body));

        if ($res->success) {
            $data_id = $this->input->post('data_id');
            $d = $this->db->query("SELECT * FROM bpjs_rencana_kontrol WHERE id = '$data_id'")->row();

            $this->MainModel->update('bpjs_rencana_kontrol', [
                'id_pasien' => $this->input->post('id_pasien') ?? 0,
                'no_sep' => $this->input->post('noSEP'),
                'request' => json_encode($body),
                'response' => json_encode($res->data),
                'success' => 1
            ], $d->id);
            $this->session->set_flashdata('success', 'Berhasil mengedit Rencana Kontrol!');
            redirect('bpjs/rencana_kontrol/daftar', 'refresh');
        }
        else {
            $this->session->set_flashdata('warning', $res->server_output->metaData->message ?? 'Gagal mengedit Rencana Kontrol!');
            redirect('bpjs/rencana_kontrol/daftar', 'refresh');
        }
    }

    public function detail_kontrol($id)
    {
        $data['data'] = $this->db->query("SELECT * FROM bpjs_rencana_kontrol WHERE id = $id")->row();
        $data['response'] = json_decode($data['data']->response);
        $data['request'] = json_decode($data['data']->request);
        $data['request'] = $data['request']->request;

        $sep = $this->search_sep($data['data']->no_sep);
        $data['sep'] = $sep['datanya'];

        $poli = urlencode($data['request']->poliKontrol);
        $res = $this->ajax_search("{$this->base_url_vclaim}referensi/poli/$poli", 'poli');
        $poli = json_decode($res);
        $data['poli'] = $poli->datanya[0];
        $data['jenis_kontrol'] = 2;
        $data['nomor'] = $data['data']->no_sep;

        $this->template->view('bpjs/rencana_kontrol/detail_kontrol', $data);
    }

    public function create_ranap()
    {
        $data['tgl_rencana'] = $this->input->post('tgl_rancana');
        $data['peserta'] = json_decode($this->input->post('peserta'));

        $p1 = '1';
        $p2 = $data['peserta']->noKartu;
        $p3 = $data['tgl_rencana'];
        $res = $this->ajax_search("{$this->base_url_vclaim}RencanaKontrol/ListSpesialistik/JnsKontrol/$p1/nomor/$p2/TglRencanaKontrol/$p3", 'list');
        $poli = json_decode($res);
        $data['poli'] = $poli->datanya;
        $data['p3'] = $p3;

        $this->template->view('bpjs/rencana_kontrol/create_ranap', $data);
    }

    public function insert_ranap()
    {
        $body = [
            'request' => [
                'noKartu' => $this->input->post('noKartu'),
                'kodeDokter' => $this->input->post('kode_dokter'),
                'poliKontrol' => $this->input->post('poli_kontrol'),
                'tglRencanaKontrol' => $this->input->post('tglRencanaKontrol'),
                'user' => $this->session->userdata('logged_in')->nama,
            ]
        ];

        $res = $this->post("{$this->base_url_vclaim}RencanaKontrol/InsertSPRI", json_encode($body));

        if ($res->success) {
            $this->MainModel->insert('bpjs_rencana_kontrol', [
                'id_pasien' => $this->input->post('id_pasien') ?? 0,
                'no_bpjs' => $this->input->post('noKartu'),
                'request' => json_encode($body),
                'response' => json_encode($res->data),
                'success' => 1,
                'type' => 'spri'
            ]);
            $this->session->set_flashdata('success', 'Berhasil menambahkan Rencana Inap!');
            redirect('bpjs/rencana_kontrol/index', 'refresh');
        }
        else {
            $this->MainModel->insert('bpjs_rencana_kontrol', [
                'id_pasien' => $this->input->post('id_pasien') ?? 0,
                'no_bpjs' => $this->input->post('noKartu'),
                'request' => json_encode($body),
                'response' => json_encode($res->server_output),
                'success' => 0,
                'type' => 'spri'
            ]);
            $this->session->set_flashdata('warning', $res->server_output->metaData->message ?? 'Gagal menambahkan Rencana Inap!');
            redirect('bpjs/rencana_kontrol/index', 'refresh');
        }
    }

    public function update_inap()
    {
        $body = [
            'request' => [
                'noSPRI' => $this->input->post('noSPRI'),
                'noKartu' => $this->input->post('noKartu'),
                'kodeDokter' => $this->input->post('kode_dokter'),
                'poliKontrol' => $this->input->post('poli_kontrol'),
                'tglRencanaKontrol' => $this->input->post('tglRencanaKontrol'),
                'user' => $this->session->userdata('logged_in')->nama,
            ]
        ];

        $res = $this->put("{$this->base_url_vclaim}RencanaKontrol/UpdateSPRI", json_encode($body));

        if ($res->success) {
            $data_id = $this->input->post('data_id');
            $d = $this->db->query("SELECT * FROM bpjs_rencana_kontrol WHERE id = '$data_id'")->row();

            $this->MainModel->update('bpjs_rencana_kontrol', [
                'id_pasien' => $this->input->post('id_pasien') ?? 0,
                'no_sep' => $this->input->post('noSEP'),
                'request' => json_encode($body),
                'response' => json_encode($res->data),
                'success' => 1
            ], $d->id);
            $this->session->set_flashdata('success', 'Berhasil mengedit Rencana Kontrol!');
            redirect('bpjs/rencana_kontrol/daftar', 'refresh');
        }
        else {
            $this->session->set_flashdata('warning', $res->server_output->metaData->message ?? 'Gagal mengedit Rencana Kontrol!');
            redirect('bpjs/rencana_kontrol/daftar', 'refresh');
        }
    }

    public function detail_inap($id)
    {
        $data['data'] = $this->db->query("SELECT * FROM bpjs_rencana_kontrol WHERE id = $id")->row();
        $data['response'] = json_decode($data['data']->response);
        $data['request'] = json_decode($data['data']->request);
        $data['request'] = $data['request']->request;

        $peserta = $this->search_peserta_by_no_bpjs($data['response']->noKartu, date('Y-m-d'));
        $data['peserta'] = $peserta['datanya']->peserta;

        $p1 = '1';
        $p2 = $data['response']->noKartu;
        $p3 = $data['response']->tglRencanaKontrol;
        $res = $this->ajax_search("{$this->base_url_vclaim}RencanaKontrol/ListSpesialistik/JnsKontrol/$p1/nomor/$p2/TglRencanaKontrol/$p3", 'list');
        $poli = json_decode($res);
        $data['poli'] = $poli->datanya;

        $poli = urlencode($data['request']->poliKontrol);
        $res = $this->ajax_search("{$this->base_url_vclaim}referensi/poli/$poli", 'poli');
        $poli = json_decode($res);
        $data['poli_s'] = $poli->datanya[0];
        $data['jenis_kontrol'] = 2;
        $data['nomor'] = $data['data']->no_sep;

        $this->template->view('bpjs/rencana_kontrol/detail_inap', $data);
    }

    public function delete_kontrol($id)
    {
        $data = $this->db->query("SELECT * FROM bpjs_rencana_kontrol WHERE id = $id")->row();
        $response = json_decode($data->response);
        $res = $this->destroy_kontrol($response->noSuratKontrol);
        $this->db->delete('bpjs_rencana_kontrol', array('id' => $id));

        $this->session->set_flashdata('success', 'Berhasil menghapus Rencana Kontrol');
        redirect('bpjs/rencana_kontrol/daftar', 'refresh');
    }

    public function print_rencana_kontrol($id)
    {
        $data['data'] = $this->db->query("SELECT * FROM bpjs_rencana_kontrol WHERE id = $id")->row();
        $data['response'] = json_decode($data['data']->response);
        $data['request'] = json_decode($data['data']->request);
        $data['request'] = $data['request']->request;

        $data['klinik'] = $this->AdministrasiModel->getKlinik()->row();
        $this->load->view('bpjs/rencana_kontrol/print', $data);
    }
}