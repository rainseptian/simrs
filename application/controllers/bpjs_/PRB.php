<?php

class PRB extends MY_BpjsController
{
    public function index()
    {
//        die(json_encode([
//            'OBG' => json_decode($this->ajax_search_dpjp_param(2, $tgl ?? date('Y-m-d'), 'OBG')),
//            'BEDAH' => json_decode($this->ajax_search_dpjp_param(2, $tgl ?? date('Y-m-d'), 'BEDAH')),
//            'INT' => json_decode($this->ajax_search_dpjp_param(2, $tgl ?? date('Y-m-d'), 'INT')),
//            'SAR' => json_decode($this->ajax_search_dpjp_param(2, $tgl ?? date('Y-m-d'), 'SAR')),
//            'ANA' => json_decode($this->ajax_search_dpjp_param(2, $tgl ?? date('Y-m-d'), 'ANA')),
//        ]));
        $this->template->view('bpjs/prb/index');
    }

    public function daftar()
    {
        $data['list'] = $this->db->query("
            SELECT prb.* FROM bpjs_prb prb
            ORDER BY prb.id DESC
        ")->result();
        $data['dpjp'] = $this->get_dpjp();
        $this->template->view('bpjs/prb/list', $data);
    }

    public function create()
    {
        $data['sep'] = json_decode($this->input->post('sep'));
        $poli = urlencode($data['sep']->poli);
        $res = $this->ajax_search("{$this->base_url_vclaim}referensi/poli/$poli", 'poli');
        $poli = json_decode($res);
        $data['poli'] = $poli->datanya[0];
        $data['diag_prb'] = json_decode($this->ajax_search_diagnosa_prb())->datanya;
        $data['dpjp'] = $this->get_dpjp();

        $this->template->view('bpjs/prb/create', $data);
    }

    public function insert()
    {
        if ($this->input->method() == 'post') {
            $body = [
                'request' => [
                    't_prb' => [
                        'noSep' => $this->input->post('noSEP'),
                        'noKartu' => $this->input->post('noKartu'),
                        'alamat' => $this->input->post('alamat'),
                        'email' => $this->input->post('email'),
                        'programPRB' => $this->input->post('programPRB'),
                        'kodeDPJP' => $this->input->post('dpjp'),
                        'keterangan' => $this->input->post('keterangan'),
                        'saran' => $this->input->post('saran'),
                        'user' => "{$this->session->userdata('logged_in')->id}",
                        'obat' => json_decode($this->input->post('obat'))
                    ]
                ]
            ];

            $res = $this->post("{$this->base_url_vclaim}PRB/insert", json_encode($body));

            if ($res->success) {
                $this->MainModel->insert('bpjs_prb', [
                    'id_pasien' => $this->input->post('id_pasien') ?? '0',
                    'no_sep' => $this->input->post('noSEP'),
                    'no_bpjs' => $this->input->post('noKartu'),
                    'raw_request' => '',
                    'request' => json_encode($body),
                    'response' => json_encode($res->data),
                    'success' => 1
                ]);
                $this->session->set_flashdata('success', 'Berhasil menambahkan PRB!');
                redirect('bpjs/prb/index', 'refresh');
            }
            else {
                $this->MainModel->insert('bpjs_prb', [
                    'id_pasien' => $this->input->post('id_pasien') ?? '0',
                    'no_sep' => $this->input->post('noSEP'),
                    'no_bpjs' => $this->input->post('noKartu'),
                    'raw_request' => '',
                    'request' => json_encode($body),
                    'response' => json_encode($res->server_output),
                    'success' => 0
                ]);
                $this->session->set_flashdata('warning', 'Gagal menambahkan PRB!');
                redirect('bpjs/prb/index', 'refresh');
            }
        }
        else {
            $data['dpjp'] = $this->get_dpjp();
            $data['user_id'] = $this->session->userdata('logged_in')->id;
            $data['diagnosa_prb'] = json_decode(parent::ajax_search_diagnosa_prb())->datanya;
            $this->template->view('bpjs/prb/create', $data);
        }
    }

    public function ajax_nomor_sep()
    {
        $keyword = $this->input->post('keyword');
        $pasien = $this->db->query("
            SELECT p.no_rm, p.no_bpjs as no_jaminan, p.id, p.nama, p.alamat, sep.no_sep
            FROM bpjs_sep sep
            JOIN pasien p ON p.id = sep.id_pasien
            WHERE (sep.no_sep LIKE '%$keyword%' OR p.nama LIKE '%$keyword%' OR p.no_bpjs LIKE '%$keyword%')
            AND sep.no_sep IS NOT NULL
            AND sep.no_sep != ''
            AND p.is_active = 1
            AND p.no_bpjs != ''
            AND p.no_bpjs IS NOT NULL 
            LIMIT 10
        ")->result();

        if (count($pasien) > 0) {
            $json['status'] = 1;
            $json['datanya'] = "<ul class='daftar-autocomplete'>";
            foreach ($pasien as $b) {
                $json['datanya'] .= " 
						<li>
						    <span id='pasien_id' style='display: none;'>" . $b->id . "</span>
						    <span id='alamat' style='display: none;'>" . $b->alamat . "</span>
						    <span id='no_bpjs' style='display: none;'>" . $b->no_jaminan . "</span>
							<b>NO RM</b> :
							<span id='no_rm'>" . $b->no_rm . "</span> <br />
							<b>Nama</b> :
							<span id='nama'>" . $b->nama . "</span> <br />
							<b>No SEP</b> :
							<span id='no_sep'>" . $b->no_sep . "</span> <br />
						</li>
					";
            }
            $json['datanya'] .= "</ul>";
        }
        else {
            $json['status'] = 0;
            $json['count'] = count($pasien);
        }

        echo json_encode($json);
    }

    public function ajax_search_obat_list()
    {
        echo parent::ajax_search_obat_list();
    }
}