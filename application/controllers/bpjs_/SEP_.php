<?php

class SEP_ extends MY_BpjsController
{
    public function __construct() {
        parent::__construct();

        $this->load->Model('AdministrasiModel');
        $this->load->helper(array('file', 'php_with_mpdf_helper'));
        $this->load->helper(array('file', 'mpdf'));
    }

    public function list_igd()
    {
        $data['list'] = $this->db->query("
            SELECT sep.*, p.nama, p.no_rm FROM bpjs_sep sep
            JOIN pasien p ON p.id = sep.id_pasien AND p.is_active = 1 
            WHERE type = 'igd'
            ORDER BY sep.id DESC
        ")->result();
        $this->template->view('bpjs/sep/igd/list', $data);
    }

    public function create_igd()
    {
        if ($this->input->method() == 'post') {
            $body = [
                'request' => [
                    't_sep' => [
                        'noKartu' => $this->input->post('nomor_kartu'),
                        'tglSep' => $this->input->post('tgl_sep'),
                        'ppkPelayanan' => $this->input->post('ppk_pelayanan'),
                        'jnsPelayanan' => $this->input->post('jns_pelayanan'),
                        'klsRawat' => [
                            'klsRawatHak' => '1',
                            'klsRawatNaik' => '',
                            'pembiayaan' => '',
                            'penanggungJawab' => '',
                        ],
                        'noMR' => $this->input->post('no_mr'),
                        'rujukan' => [
                            'asalRujukan' => $this->input->post('asal_rujukan'),
                            'tglRujukan' => $this->input->post('tgl_rujukan'),
                            'noRujukan' => $this->input->post('no_rujukan'),
                            'ppkRujukan' => $this->input->post('ppk_rujukan'),
                        ],
                        'catatan' => $this->input->post('catatan'),
                        'diagAwal' => $this->input->post('diag_awal'),
                        'poli' => [
                            'tujuan' => $this->input->post('poli_tujuan'),
                            'eksekutif' => $this->input->post('poli_eksekutif'),
                        ],
                        'cob' => [
                            'cob' => '0',
                        ],
                        'katarak' => [
                            'katarak' => '0',
                        ],
                        'jaminan' => [
                            'lakaLantas' => $this->input->post('laka_lantas'),
                            'penjamin' => [
                                'tglKejadian' => $this->input->post('tgl_kejadian'),
                                'keterangan' => $this->input->post('keterangan'),
                                'suplesi' => [
                                    'suplesi' => $this->input->post('suplesi'),
                                    'noSepSuplesi' => $this->input->post('no_sep_suplesi'),
                                    'lokasiLaka' => [
                                        'kdPropinsi' => $this->input->post('kd_propinsi'),
                                        'kdKabupaten' => $this->input->post('kd_kabupaten') ?? '',
                                        'kdKecamatan' => $this->input->post('kd_kecamatan') ?? '',
                                    ]
                                ]
                            ]
                        ],
                        'tujuanKunj' => '0',
                        'flagProcedure' => '',
                        'kdPenunjang' => '',
                        'assesmentPel' => '',
                        'skdp' => [
                            'noSurat' => '',
                            'kodeDPJP' => '',
                        ],
                        'dpjpLayan' => $this->input->post('dpjp_layan'),
                        'noTelp' => $this->input->post('no_telp'),
                        'user' => $this->input->post('user'),
                    ]
                ]
            ];

            $res = $this->post("{$this->base_url_vclaim}SEP/2.0/insert", json_encode($body));

            if ($res->success) {
                $this->MainModel->insert('bpjs_sep', [
                    'id_pasien' => $this->input->post('id_pasien'),
                    'no_bpjs' => $this->input->post('nomor_kartu'),
                    'raw_request' => '',
                    'request' => json_encode($body),
                    'response' => json_encode($res->data),
                    'success' => 1,
                    'type' => 'igd'
                ]);
                $this->session->set_flashdata('success', 'Berhasil menambahkan SEP!');
                redirect('bpjs/sep/igd/list', 'refresh');
            }
            else {
                $this->MainModel->insert('bpjs_sep', [
                    'id_pasien' => $this->input->post('id_pasien'),
                    'no_bpjs' => $this->input->post('nomor_kartu'),
                    'raw_request' => '',
                    'request' => json_encode($body),
                    'response' => json_encode($res->server_output),
                    'success' => 0,
                    'type' => 'igd'
                ]);
                $this->session->set_flashdata('warning', 'Gagal menambahkan SEP!');
                redirect('bpjs/sep/igd/list', 'refresh');
            }
        }
        else {
            $data['dpjp'] = [
                [
                    'kode' => '6517',
                    'nama' => 'dr. Zakky Sukmajaya, Sp.OG'
                ],
                [
                    'kode' => '265867',
                    'nama' => 'DR. AIRLANGGA WITRA NANDA ABDILLAH'
                ],
                [
                    'kode' => '6519',
                    'nama' => 'dr. Fera Diastyarini, Sp. A'
                ],
            ];
            $data['provinsi'] = json_decode(parent::ajax_search_provinsi())->datanya;
            $data['nama'] = $this->session->userdata('logged_in')->nama;

            $this->template->view('bpjs/sep/igd/create_v2', $data);
        }
    }

    public function list_kunjungan_pertama()
    {
        $data['list'] = $this->db->query("
            SELECT sep.*, p.nama, p.no_rm FROM bpjs_sep sep
            JOIN pasien p ON p.id = sep.id_pasien AND p.is_active = 1 
            WHERE type = 'kunjungan pertama'
            ORDER BY sep.id DESC
        ")->result();
        $this->template->view('bpjs/sep/kunjungan_pertama/list', $data);
    }

    public function create_kunjungan_pertama()
    {
        if ($this->input->method() == 'post') {
            $body = [
                'request' => [
                    't_sep' => [
                        'noKartu' => $this->input->post('nomor_kartu'),
                        'tglSep' => $this->input->post('tgl_sep'),
                        'ppkPelayanan' => $this->input->post('ppk_pelayanan'),
                        'jnsPelayanan' => $this->input->post('jns_pelayanan'),
                        'klsRawat' => [
                            'klsRawatHak' => '1',
                            'klsRawatNaik' => '',
                            'pembiayaan' => '',
                            'penanggungJawab' => '',
                        ],
                        'noMR' => $this->input->post('no_mr'),
                        'rujukan' => [
                            'asalRujukan' => $this->input->post('asal_rujukan'),
                            'tglRujukan' => $this->input->post('tgl_rujukan'),
                            'noRujukan' => $this->input->post('no_rujukan'),
                            'ppkRujukan' => $this->input->post('ppk_rujukan'),
                        ],
                        'catatan' => $this->input->post('catatan'),
                        'diagAwal' => $this->input->post('diag_awal'),
                        'poli' => [
                            'tujuan' => $this->input->post('poli_tujuan'),
                            'eksekutif' => $this->input->post('poli_eksekutif'),
                        ],
                        'cob' => [
                            'cob' => '0',
                        ],
                        'katarak' => [
                            'katarak' => '0',
                        ],
                        'jaminan' => [
                            'lakaLantas' => $this->input->post('laka_lantas'),
                            'penjamin' => [
                                'tglKejadian' => $this->input->post('tgl_kejadian'),
                                'keterangan' => $this->input->post('keterangan'),
                                'suplesi' => [
                                    'suplesi' => $this->input->post('suplesi'),
                                    'noSepSuplesi' => $this->input->post('no_sep_suplesi'),
                                    'lokasiLaka' => [
                                        'kdPropinsi' => $this->input->post('kd_propinsi'),
                                        'kdKabupaten' => $this->input->post('kd_kabupaten') ?? '',
                                        'kdKecamatan' => $this->input->post('kd_kecamatan') ?? '',
                                    ]
                                ]
                            ]
                        ],
                        'tujuanKunj' => '0',
                        'flagProcedure' => '',
                        'kdPenunjang' => '',
                        'assesmentPel' => '',
                        'skdp' => [
                            'noSurat' => '',
                            'kodeDPJP' => '',
                        ],
                        'dpjpLayan' => $this->input->post('dpjp_layan'),
                        'noTelp' => $this->input->post('no_telp'),
                        'user' => $this->input->post('user'),
                    ]
                ]
            ];

            $res = $this->post("{$this->base_url_vclaim}SEP/2.0/insert", json_encode($body));

            if ($res->success) {
                $this->MainModel->insert('bpjs_sep', [
                    'id_pasien' => $this->input->post('id_pasien'),
                    'no_bpjs' => $this->input->post('nomor_kartu'),
                    'raw_request' => '',
                    'request' => json_encode($body),
                    'response' => json_encode($res->data),
                    'success' => 1,
                    'type' => 'kunjungan pertama'
                ]);
                $this->session->set_flashdata('success', 'Berhasil menambahkan SEP!');
                redirect('bpjs/sep/kunjungan_pertama/list', 'refresh');
            }
            else {
                $this->MainModel->insert('bpjs_sep', [
                    'id_pasien' => $this->input->post('id_pasien'),
                    'no_bpjs' => $this->input->post('nomor_kartu'),
                    'raw_request' => '',
                    'request' => json_encode($body),
                    'response' => json_encode($res->server_output),
                    'success' => 0,
                    'type' => 'kunjungan pertama'
                ]);
                $this->session->set_flashdata('warning', 'Gagal menambahkan SEP!');
                redirect('bpjs/sep/kunjungan_pertama/list', 'refresh');
            }
        }
        else {
            $data['dpjp'] = [
                [
                    'kode' => '6517',
                    'nama' => 'dr. Zakky Sukmajaya, Sp.OG'
                ],
                [
                    'kode' => '265867',
                    'nama' => 'DR. AIRLANGGA WITRA NANDA ABDILLAH'
                ],
                [
                    'kode' => '6519',
                    'nama' => 'dr. Fera Diastyarini, Sp. A'
                ],
            ];
            $data['provinsi'] = json_decode(parent::ajax_search_provinsi())->datanya;
            $data['nama'] = $this->session->userdata('logged_in')->nama;

            $this->template->view('bpjs/sep/kunjungan_pertama/create_v2', $data);
        }
    }

    public function list_kunjungan_kedua()
    {
        $data['list'] = $this->db->query("
            SELECT sep.*, p.nama, p.no_rm FROM bpjs_sep sep
            JOIN pasien p ON p.id = sep.id_pasien AND p.is_active = 1 
            WHERE type = 'kunjungan kedua'
            ORDER BY sep.id DESC
        ")->result();
        $this->template->view('bpjs/sep/kunjungan_kedua/list', $data);
    }

    public function create_kunjungan_kedua()
    {

    }

    public function list_rawat_inap()
    {
        $data['list'] = $this->db->query("
            SELECT sep.*, p.nama, p.no_rm FROM bpjs_sep sep
            JOIN pasien p ON p.id = sep.id_pasien AND p.is_active = 1 
            WHERE type = 'rawat inap'
            ORDER BY sep.id DESC
        ")->result();
        $this->template->view('bpjs/sep/rawat_inap/list', $data);
    }

    public function create_rawat_inap()
    {

    }

    public function print_sep($id)
    {
        $this->db->query("UPDATE bpjs_sep SET print_ke = print_ke + 1 WHERE id = $id");
        $data['data'] = $this->db->query("
            SELECT 
                   sep.*, p.nama, p.no_rm, p.tanggal_lahir, IF(p.jk = 'L', 'Laki-Laki', 'Perempuan') as jenis_kelamin,
                   p.telepon
            FROM bpjs_sep sep
            JOIN pasien p ON p.id = sep.id_pasien AND p.is_active = 1 
            WHERE sep.id = $id
        ")->row();
        $data['response'] = json_decode($data['data']->response);
        $data['response'] = $data['response']->sep;
        $data['request'] = json_decode($data['data']->request);
        $data['request'] = $data['request']->request->t_sep;

        $dpjp = '';
        foreach ($this->get_dpjp() as $v) {
            if ($v['kode'] == $data['request']->dpjpLayan) {
                $dpjp = $v['nama'];
                break;
            }
        }
        $data['dpjp'] = $dpjp;

        $data['klinik'] = $this->AdministrasiModel->getKlinik()->row();
        $this->load->view('bpjs/sep/print', $data);
    }

}