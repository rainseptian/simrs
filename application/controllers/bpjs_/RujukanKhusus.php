<?php

class RujukanKhusus extends MY_BpjsController
{
    public function index()
    {
        $this->template->view('bpjs/rujukan_khusus/index');
    }

    public function daftar()
    {
        $data['list'] = $this->db->query("
            SELECT rujukan.*, p.nama, p.no_rm FROM bpjs_rujukan rujukan
            LEFT JOIN pasien p ON p.id = rujukan.id_pasien AND p.is_active = 1 
            WHERE is_khusus = 1
            ORDER BY rujukan.id DESC
        ")->result();
        $data['dpjp'] = $this->get_dpjp();
        $this->template->view('bpjs/rujukan_khusus/list', $data);
    }

    public function create()
    {
        $data['sep'] = json_decode($this->input->post('sep'));
        $poli = urlencode($data['sep']->poli);
        $res = $this->ajax_search("{$this->base_url_vclaim}referensi/poli/$poli", 'poli');
        $poli = json_decode($res);
        $data['poli'] = $poli->datanya[0];

        $this->template->view('bpjs/rujukan_khusus/create', $data);
    }
}