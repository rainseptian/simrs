<?php

class Rujukan extends MY_BpjsController
{
    public function __construct()
    {
        parent::__construct();

        $this->load->Model('AdministrasiModel');
        $this->load->helper(array('file', 'php_with_mpdf_helper'));
        $this->load->helper(array('file', 'mpdf'));
    }

    public function index()
    {
        $this->template->view('bpjs/rujukan/index');
    }

    public function daftar()
    {
        $data['list'] = $this->db->query("
            SELECT rujukan.*, p.nama, p.no_rm FROM bpjs_rujukan rujukan
            LEFT JOIN pasien p ON p.id = rujukan.id_pasien AND p.is_active = 1 
            WHERE is_khusus = 0
            ORDER BY rujukan.id DESC
        ")->result();
        $data['dpjp'] = $this->get_dpjp();
        $this->template->view('bpjs/rujukan/list', $data);
    }

    public function create()
    {
        $data['sep'] = json_decode($this->input->post('sep'));
        $poli = urlencode($data['sep']->poli);
        $res = $this->ajax_search("{$this->base_url_vclaim}referensi/poli/$poli", 'poli');
        $poli = json_decode($res);
        $data['poli'] = $poli->datanya[0];

        $this->template->view('bpjs/rujukan/create', $data);
    }

    public function insert()
    {
        if ($this->input->method() == 'post') {
            $body = [
                'request' => [
                    't_rujukan' => [
                        'noSep' => $this->input->post('noSEP'),
                        'tglRujukan' => $this->input->post('tglRujukan'),
                        'tglRencanaKunjungan' => $this->input->post('tglRencanaKunjungan'),
                        'ppkDirujuk' => $this->input->post('ppkDirujuk'),
                        'jnsPelayanan' => $this->input->post('jnsPelayanan'),
                        'catatan' => $this->input->post('catatan'),
                        'diagRujukan' => $this->input->post('diagRujukan'),
                        'tipeRujukan' => $this->input->post('tipeRujukan'),
                        'poliRujukan' => $this->input->post('tipeRujukan') == '2' || $this->input->post('jnsPelayanan') == '1' ? '' : $this->input->post('spesialis'),
                        'user' => $this->session->userdata('logged_in')->nama,
                    ]
                ]
            ];

            $res = $this->post("{$this->base_url_vclaim}Rujukan/2.0/insert", json_encode($body));

            if ($res->success) {
                $this->MainModel->insert('bpjs_rujukan', [
                    'id_pasien' => $this->input->post('id_pasien') ?? 0,
                    'no_sep' => $this->input->post('noSEP'),
                    'request' => json_encode($body),
                    'response' => json_encode($res->data),
                    'success' => 1
                ]);
                $this->session->set_flashdata('success', 'Berhasil menambahkan rujukan!');
                redirect('bpjs/rujukan/index', 'refresh');
            }
            else {
                $this->MainModel->insert('bpjs_rujukan', [
                    'id_pasien' => $this->input->post('id_pasien') ?? 0,
                    'no_sep' => $this->input->post('noSEP'),
                    'request' => json_encode($body),
                    'response' => json_encode($res->server_output),
                    'success' => 0
                ]);
                $this->session->set_flashdata('warning', $res->server_output->metaData->message ?? 'Gagal menambahkan rujukan!');
                redirect('bpjs/rujukan/index', 'refresh');
            }
        }
        else {
            $data['dpjp'] = $this->get_dpjp();
            $data['user_id'] = $this->session->userdata('logged_in')->id;
            $data['nama'] = $this->session->userdata('logged_in')->nama;

            $this->template->view('bpjs/rujukan/create', $data);
        }
    }

    public function update()
    {
        if ($this->input->method() == 'post') {
            $body = [
                'request' => [
                    't_rujukan' => [
                        'noRujukan' => $this->input->post('noRujukan'),
                        'tglRujukan' => $this->input->post('tglRujukan'),
                        'tglRencanaKunjungan' => $this->input->post('tglRencanaKunjungan'),
                        'ppkDirujuk' => $this->input->post('ppkDirujuk'),
                        'jnsPelayanan' => $this->input->post('jnsPelayanan'),
                        'catatan' => $this->input->post('catatan'),
                        'diagRujukan' => $this->input->post('diagRujukan'),
                        'tipeRujukan' => $this->input->post('tipeRujukan'),
                        'poliRujukan' => $this->input->post('poliRujukan'),
//                        'poliRujukan' => $this->input->post('tipeRujukan') == '2' || $this->input->post('jnsPelayanan') == '1' ? '' : $this->input->post('poliRujukan'),
                        'user' => $this->session->userdata('logged_in')->nama,
                    ]
                ]
            ];

            $res = $this->put("{$this->base_url_vclaim}Rujukan/2.0/Update", json_encode($body));

            if ($res->success) {
                $id = $this->input->post('id');
                $d = $this->db->query("SELECT * FROM bpjs_rujukan WHERE id = '$id'")->row();
                $b_req = json_decode($d->request);

                $b_req->request->t_rujukan->noRujukan = $this->input->post('noRujukan');
                $b_req->request->t_rujukan->ppkDirujuk = $this->input->post('ppkDirujuk');
                $b_req->request->t_rujukan->tipe = $this->input->post('tipe');
                $b_req->request->t_rujukan->jnsPelayanan = $this->input->post('jnsPelayanan');
                $b_req->request->t_rujukan->catatan = $this->input->post('catatan');
                $b_req->request->t_rujukan->diagRujukan = $this->input->post('diagRujukan');
                $b_req->request->t_rujukan->tipeRujukan = $this->input->post('tipeRujukan');
                $b_req->request->t_rujukan->poliRujukan = $this->input->post('poliRujukan');

                $this->MainModel->update('bpjs_rujukan', [
                    'id_pasien' => $this->input->post('id_pasien') ?? 0,
                    'no_sep' => $this->input->post('noSEP'),
                    'request' => json_encode($b_req),
//                    'response' => json_encode($res->data),
                    'success' => 1
                ], $d->id);
                $this->session->set_flashdata('success', 'Berhasil mengedit rujukan!');
                redirect('bpjs/rujukan/daftar', 'refresh');
            }
            else {
                $resp = $res->server_output;
                $this->session->set_flashdata('warning', 'Gagal mengedit SEP! : ' . ($resp->metaData->message ?? ''));
                redirect('bpjs/rujukan/daftar', 'refresh');
            }
        }
        else {
            $data['dpjp'] = $this->get_dpjp();
            $data['user_id'] = $this->session->userdata('logged_in')->id;
            $data['nama'] = $this->session->userdata('logged_in')->nama;

            $this->template->view('bpjs/rujukan/create', $data);
        }
    }

    public function detail($id)
    {
        $rujukan = $this->db->query("SELECT * FROM bpjs_rujukan WHERE id = $id")->row();
        $data['rdb'] = $rujukan;
        $data['rujukan'] = json_decode($rujukan->request)->request->t_rujukan;
        $data['response'] = json_decode($rujukan->response)->rujukan;
        $data['sep'] = $this->search_sep($rujukan->no_sep)['datanya'];
        $poli = urlencode($data['sep']->poli);
        $res = $this->ajax_search("{$this->base_url_vclaim}referensi/poli/$poli", 'poli');
        $poli = json_decode($res);
        $data['poli'] = $poli->datanya[0];

        $this->template->view('bpjs/rujukan/detail', $data);

    }

    public function print_rujukan($id)
    {
        $data['data'] = $this->db->query("SELECT sep.* FROM bpjs_rujukan sep WHERE sep.id = $id")->row();
        $data['response'] = json_decode($data['data']->response);
        $data['response'] = $data['response']->rujukan;
        $data['request'] = json_decode($data['data']->request);
        $data['request'] = $data['request']->request->t_rujukan;
        $data['klinik'] = $this->AdministrasiModel->getKlinik()->row();
        $this->load->view('bpjs/rujukan/print', $data);
    }

    public function delete_rujukan($id = 0, $noRujukan = 0)
    {
        $req = [
            'request' => [
                't_rujukan' => [
                    'noRujukan' => $noRujukan,
                    'user' => $this->session->userdata('logged_in')->nama
                ]
            ]
        ];
        $this->delete("{$this->base_url_vclaim}Rujukan/delete", json_encode($req));

        $this->db->delete('bpjs_rujukan', array('id' => $id));

        $this->session->set_flashdata('success', 'Berhasil menghapus rujukan');
        redirect('bpjs/rujukan/daftar', 'refresh');
    }

}