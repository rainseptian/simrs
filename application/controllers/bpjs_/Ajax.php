<?php

class Ajax extends MY_BpjsController
{
    public function search_ruang_rawat()
    {
        $res = $this->get("{$this->base_url_vclaim}referensi/ruangrawat");
        if ($res->success) {
            $json['status'] = 1;
            $json['datanya'] = $res->data->list;
        }
        else if ($res->server_output->metaData->message == 'Data Tidak Ada') {
            $json['status'] = 1;
            $json['datanya'] = [];
        }
        else {
            $json['status'] = 0;
            $json['server_output'] = $res->server_output;
        }

        echo json_encode($json);
    }

    public function search_kelas_rawat()
    {
        $res = $this->get("{$this->base_url_vclaim}referensi/kelasrawat");
        if ($res->success) {
            $json['status'] = 1;
            $json['datanya'] = $res->data->list;
        }
        else if ($res->server_output->metaData->message == 'Data Tidak Ada') {
            $json['status'] = 1;
            $json['datanya'] = [];
        }
        else {
            $json['status'] = 0;
            $json['server_output'] = $res->server_output;
        }

        echo json_encode($json);
    }

    public function search_spesialistik()
    {
        $res = $this->get("{$this->base_url_vclaim}referensi/spesialistik");
        if ($res->success) {
            $json['status'] = 1;
            $json['datanya'] = $res->data->list;
        }
        else if ($res->server_output->metaData->message == 'Data Tidak Ada') {
            $json['status'] = 1;
            $json['datanya'] = [];
        }
        else {
            $json['status'] = 0;
            $json['server_output'] = $res->server_output;
        }

        echo json_encode($json);
    }

    public function search_cara_keluar()
    {
        $res = $this->get("{$this->base_url_vclaim}referensi/spesialistik");
        if ($res->success) {
            $json['status'] = 1;
            $json['datanya'] = $res->data->list;
        }
        else if ($res->server_output->metaData->message == 'Data Tidak Ada') {
            $json['status'] = 1;
            $json['datanya'] = [];
        }
        else {
            $json['status'] = 0;
            $json['server_output'] = $res->server_output;
        }

        echo json_encode($json);
    }

    public function raw()
    {
        $_POST = json_decode($this->input->raw_input_stream, true);
        echo parent::raw();
    }
}