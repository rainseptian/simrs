<?php

class LPK extends MY_BpjsController
{
    public function index()
    {
        $data['list'] = $this->db->query("
            SELECT lpk.*, p.nama, p.no_rm FROM bpjs_lpk lpk
            JOIN pasien p ON p.id = lpk.id_pasien AND p.is_active = 1 
            ORDER BY lpk.id DESC
        ")->result();
        $data['dpjp'] = $this->get_dpjp();
        $this->template->view('bpjs/lpk/list', $data);
    }

    public function create()
    {
        if ($this->input->method() == 'post') {
            $diagnosa = [];
            for ($i = 1; $i <= (int) $this->input->post('diagnosa_count'); $i++) {
                $d = $this->input->post('diagnosa_'.$i);
                if ($d) {
                    $diagnosa[] = [
                        'kode' => $d,
                        'level' => $this->input->post('level_'.$i),
                    ];
                }
            }

            $body = [
                'request' => [
                    't_lpk' => [
                        'noSep' => $this->input->post('nomor_sep'),
                        'tglMasuk' => $this->input->post('tgl_masuk'),
                        'tglKeluar' => $this->input->post('tgl_keluar'),
                        'jaminan' => $this->input->post('jaminan'),
                        'poli' => [
                            'poli' => $this->input->post('poli'),
                        ],
                        'perawatan' => [
                            'ruangRawat' => $this->input->post('ruang_rawat'),
                            'kelasRawat' => $this->input->post('kelas_rawat'),
                            'spesialistik' => $this->input->post('spesialistik'),
                            'caraKeluar' => $this->input->post('cara_keluar'),
                            'kondisiPulang' => $this->input->post('kondisi_pulang'),
                        ],
                        'diagnosa' => $diagnosa,
                        'procedure' => is_array($this->input->post('procedure')) ?
                            array_map(function ($v) { return ['kode' => $v]; }, $this->input->post('procedure')) :
                            [['kode' => $this->input->post('procedure')]],
                        'rencanaTL' => [
                            'tindakLanjut' => $this->input->post('tindakLanjut'),
                            'dirujukKe' => [
                                'kodePPK' => $this->input->post('ppk_rujukan'),
                            ],
                            'kontrolKembali' => [
                                'tglKontrol' => $this->input->post('tgl_kontrol'),
                                'poli' => $this->input->post('poli_kontrol') ?? '',
                            ],
                        ],
                        'DPJP' => $this->input->post('dpjp_layan'),
                        'user' => $this->input->post('user'),
                    ]
                ]
            ];

            $res = $this->post("{$this->base_url_vclaim}LPK/insert", json_encode($body));

            if ($res->success) {
                $this->MainModel->insert('bpjs_lpk', [
                    'id_pasien' => $this->input->post('id_pasien'),
                    'no_sep' => $this->input->post('nomor_sep'),
                    'raw_request' => '',
                    'request' => json_encode($body),
                    'response' => json_encode($res->data),
                    'success' => 1
                ]);
                $this->session->set_flashdata('success', 'Berhasil menambahkan LPK!');
                redirect('bpjs/lpk/index', 'refresh');
            }
            else {
                $this->MainModel->insert('bpjs_lpk', [
                    'id_pasien' => $this->input->post('id_pasien'),
                    'no_sep' => $this->input->post('nomor_sep'),
                    'raw_request' => '',
                    'request' => json_encode($body),
                    'response' => json_encode($res->server_output),
                    'success' => 0
                ]);
                $this->session->set_flashdata('warning', 'Gagal menambahkan LPK!');
                redirect('bpjs/lpk/index', 'refresh');
            }
        }
        else {
            $data['dpjp'] = $this->get_dpjp();
            $data['nama'] = $this->session->userdata('logged_in')->nama;

            $data['ruang_rawat'] = json_decode(parent::ajax_search_ruang_rawat())->datanya;
            $data['kelas_rawat'] = json_decode(parent::ajax_search_kelas_rawat())->datanya;
            $data['spesialistik'] = json_decode(parent::ajax_search_spesialistik())->datanya;
            $data['cara_keluar'] = json_decode(parent::ajax_search_cara_keluar())->datanya;
            $data['kondisi_pulang'] = json_decode(parent::ajax_search_kondisi_pulang())->datanya;
            $this->template->view('bpjs/lpk/create', $data);
        }
    }

    public function ajax_nomor_sep()
    {
        $keyword = $this->input->post('keyword');
        $pasien = $this->db->query("
            SELECT p.no_rm, p.no_bpjs as no_jaminan, p.id, p.nama, p.telepon, sep.no_sep
            FROM bpjs_sep sep
            JOIN pasien p ON p.id = sep.id_pasien
            WHERE (sep.no_sep LIKE '%$keyword%' OR p.nama LIKE '%$keyword%')
            AND sep.no_sep IS NOT NULL
            AND sep.no_sep != ''
            AND p.is_active = 1
            AND p.no_bpjs != ''
            AND p.no_bpjs IS NOT NULL 
            LIMIT 10
        ")->result();

        if (count($pasien) > 0) {
            $json['status'] = 1;
            $json['datanya'] = "<ul class='daftar-autocomplete'>";
            foreach ($pasien as $b) {
                $json['datanya'] .= " 
						<li>
						    <span id='pasien_id' style='display: none;'>" . $b->id . "</span>
						    <span id='telepon' style='display: none;'>" . $b->telepon . "</span>
							<b>NO RM</b> :
							<span id='no_rm'>" . $b->no_rm . "</span> <br />
							<b>Nama</b> :
							<span id='nama'>" . $b->nama . "</span> <br />
							<b>No SEP</b> :
							<span id='no_sep'>" . $b->no_sep . "</span> <br />
						</li>
					";
            }
            $json['datanya'] .= "</ul>";
        }
        else {
            $json['status'] = 0;
            $json['count'] = count($pasien);
        }

        echo json_encode($json);
    }
}