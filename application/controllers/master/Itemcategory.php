<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Itemcategory extends Admin_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->load->library('Customlib');
        $this->load->library('template');
    }

    function index()
    {
        $this->session->set_userdata('top_menu', 'setup');
        $this->session->set_userdata('sub_menu', 'inventory/index');
        $data['title'] = 'Item Categorey List';
        $category_result = $this->itemcategory_model->get();
        $data['categorylist'] = $category_result;

        $this->template->view('master/inventory/item_category_list', $data);
    }

    function add()
    {
        $this->form_validation->set_rules('itemcategory', $this->lang->line('item') . " " . $this->lang->line('category'), 'trim|required');
        if ($this->form_validation->run() == FALSE) {

            $msg = array(
                'name' => form_error('itemcategory'),
            );

            $array = array('status' => 'fail', 'error' => $msg, 'message' => '');
        } else {

            $data = array(
                'item_category' => $this->input->post('itemcategory'),
                'description' => $this->input->post('description'),
            );
            $this->itemcategory_model->add($data);
            $array = array('status' => 'success', 'error' => '', 'message' => $this->lang->line('success_message'));
        }

        echo json_encode($array);
    }

    function edit()
    {
        $id = $this->input->post('cat_id');
        $this->form_validation->set_rules('itemcategory', $this->lang->line('item') . " " . $this->lang->line('category'), 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $msg = array(
                'name' => form_error('itemcategory'),
            );

            $array = array('status' => 'fail', 'error' => $msg, 'message' => '');
        } else {

            $data = array(
                'id' => $id,
                'item_category' => $this->input->post('itemcategory'),
                'description' => $this->input->post('description'),
            );
            $this->itemcategory_model->add($data);
            $array = array('status' => 'success', 'error' => '', 'message' => $this->lang->line('update_message'));
        }

        echo json_encode($array);
    }

    function get_data($id)
    {
        $category = $this->itemcategory_model->get($id);
        echo json_encode($category);
    }

    function delete($id)
    {
        $this->itemcategory_model->remove($id);
        redirect('master/itemcategory');
    }
}
