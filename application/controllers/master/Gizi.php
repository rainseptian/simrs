<?php

if (!defined('BASEPATH'))exit('No direct script access allowed');

class Gizi extends Admin_Controller
{
    function __construct() {
        parent::__construct();

        $this->load->model('GiziModel');
        $this->load->model('MainModel');

        $this->load->library('form_validation');
        $this->load->library('template');
        $this->load->library('Customlib');
    }

    public function index()
    {
        $data['list'] = $this->GiziModel->getMakanan();
        $data['tipe'] = $this->GiziModel->getTipeMakanan();
        $this->template->view('master/rawat_inap/makanan', $data);
    }

    public function getMakanan($id)
    {
        $data['makanan'] = $this->GiziModel->getMakananById($id);
        echo json_encode($data);
    }

    public function addMakanan()
    {
        $m = [
            'tipe_makanan_id' => $this->input->post('tipe_makanan_id'),
            'nama' => $this->input->post('nama'),
            'harga' => $this->input->post('harga')
        ];

        $this->MainModel->insert('makanan', $m);
        $this->session->set_flashdata('success', 'Berhasil menambah makanan');
        redirect('master/Gizi');
    }

    public function editMakanan()
    {
        $m = [
            'tipe_makanan_id' => $this->input->post('tipe_makanan_id'),
            'nama' => $this->input->post('nama'),
            'harga' => $this->input->post('harga')
        ];

        $this->MainModel->update('makanan', $m, $this->input->post('id'));
        $this->session->set_flashdata('success', 'Berhasil mengedit makanan');
        redirect('master/Gizi');
    }

    public function deleteMakanan($id)
    {
        $this->MainModel->update('makanan', ['is_active' => 0], $id);
        $this->session->set_flashdata('success', 'Berhasil menghapus makanan');
        redirect('master/Gizi');
    }

    public function tipe()
    {
        $data['list'] = $this->GiziModel->getTipeMakanan();
        $this->template->view('master/rawat_inap/tipe_makanan', $data);
    }

    public function getTipeMakanan($id)
    {
        $data['tipe'] = $this->GiziModel->getTipeMakananById($id);
        echo json_encode($data);
    }

    public function addTipe()
    {
        $m = ['nama' => $this->input->post('nama')];

        $this->MainModel->insert('tipe_makanan', $m);
        $this->session->set_flashdata('success', 'Berhasil menambah tipe makanan');
        redirect('master/Gizi/tipe');
    }

    public function editTipe()
    {
        $m = ['nama' => $this->input->post('nama')];

        $this->MainModel->update('tipe_makanan', $m, $this->input->post('id'));
        $this->session->set_flashdata('success', 'Berhasil mengedit tipe makanan');
        redirect('master/Gizi/tipe');
    }

    public function deleteTipe($id)
    {
        $this->MainModel->update('tipe_makanan', ['is_active' => 0], $id);
        $this->session->set_flashdata('success', 'Berhasil menghapus tipe makanan');
        redirect('master/Gizi/tipe');
    }
}
