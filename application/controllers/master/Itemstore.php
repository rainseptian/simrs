<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Itemstore extends Admin_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->load->library('Customlib');
        $this->load->library('template');
    }

    function index()
    {
        $this->session->set_userdata('top_menu', 'setup');
        $this->session->set_userdata('sub_menu', 'inventory/index');

        $itemstore_result = $this->itemstore_model->get();
        $data['itemstorelist'] = $itemstore_result;

        $this->template->view('master/inventory/item_store_list', $data);
    }

    function add()
    {
        $this->form_validation->set_rules('name', $this->lang->line('item') . " " . $this->lang->line('store') . " " . $this->lang->line('name'), 'trim|required');

        if ($this->form_validation->run() == FALSE) {
            $msg = array(
                'name' => form_error('name'),
            );

            $array = array('status' => 'fail', 'error' => $msg, 'message' => '');
        } else {
            $data = array(
                'item_store' => $this->input->post('name'),
                'code' => $this->input->post('code'),
                'description' => $this->input->post('description'),
            );
            $this->itemstore_model->add($data);
            $array = array('status' => 'success', 'error' => '', 'message' => $this->lang->line('success_message'));
        }

        echo json_encode($array);
    }

    function edit()
    {
        $id = $this->input->post('id');
        $this->form_validation->set_rules('name', $this->lang->line('item') . " " . $this->lang->line('store') . " " . $this->lang->line('name'), 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $msg = array(
                'name' => form_error('name'),
            );
            $array = array('status' => 'fail', 'error' => $msg, 'message' => '');
        } else {
            $data = array(
                'id' => $id,
                'item_store' => $this->input->post('name'),
                'code' => $this->input->post('code'),
                'description' => $this->input->post('description'),
            );

            $this->itemstore_model->add($data);
            $array = array('status' => 'success', 'error' => '', 'message' => $this->lang->line('update_message'));
        }

        echo json_encode($array);
    }

    function get_data($id)
    {
        $itemstore_result = $this->itemstore_model->get($id);
        echo json_encode($itemstore_result);
    }

    function delete($id)
    {
        $this->itemstore_model->remove($id);
        redirect('master/itemstore');
    }
}
