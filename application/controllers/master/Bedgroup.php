<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

use LZCompressor\LZString;

class Bedgroup extends Admin_Controller {

    protected $salt = 'smart-clinic-and-hospital';
    protected $cons_id = 30389;
    protected $secret_key = '2cJ24C23FC';
    protected $user_key = '83b9cf13a3ec7ea406a0f33c7c9892b9'; // vclaim
    protected $timestamp = "";

    protected $base_url_dev = 'https://dvlp.bpjs-kesehatan.go.id:8888/aplicaresws/';
    protected $base_url = 'https://new-api.bpjs-kesehatan.go.id/aplicaresws/';
    protected $kode_ppk = '0207S001';

    private $dvlp = false;
    private $applicares = [
        [
            "kodekelas" => "NON",
            "namakelas" => "-"
        ],
        [
            "kodekelas" => "VVP",
            "namakelas" => "VVIP"
        ],
        [
            "kodekelas" => "VIP",
            "namakelas" => "VIP"
        ],
        [
            "kodekelas" => "UTM",
            "namakelas" => "UTAMA"
        ],
        [
            "kodekelas" => "KL1",
            "namakelas" => "KELAS I"
        ],
        [
            "kodekelas" => "KL2",
            "namakelas" => "KELAS II"
        ],
        [
            "kodekelas" => "KL3",
            "namakelas" => "KELAS III"
        ],
        [
            "kodekelas" => "ICU",
            "namakelas" => "ICU"
        ],
        [
            "kodekelas" => "ICC",
            "namakelas" => "ICCU"
        ],
        [
            "kodekelas" => "NIC",
            "namakelas" => "NICU"
        ],
        [
            "kodekelas" => "PIC",
            "namakelas" => "PICU"
        ],
        [
            "kodekelas" => "IGD",
            "namakelas" => "IGD"
        ],
        [
            "kodekelas" => "UGD",
            "namakelas" => "UGD"
        ],
        [
            "kodekelas" => "SAL",
            "namakelas" => "RUANG BERSALIN"
        ],
        [
            "kodekelas" => "HCU",
            "namakelas" => "HCU"
        ],
        [
            "kodekelas" => "ISO",
            "namakelas" => "RUANG ISOLASI"
        ]
    ];

    function __construct() {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->library('template');
        $this->load->library('Customlib');
        $this->load->model('MainModel');
    }

    public function add_bed_group() {
        $this->form_validation->set_rules(
                'name', $this->lang->line('name'), array('required',
            array('check_exists', array($this->bedgroup_model, 'valid_bed_group'))
                )
        );
        $this->form_validation->set_rules('floor', $this->lang->line('floor'), 'trim|required');

        if ($this->form_validation->run() == FALSE) {
            $msg = array(
                'name' => form_error('name'),
                'floor' => form_error('floor'),
            );
            $this->session->set_flashdata('warning', 'Gagal menambah bed group: ' . $msg);
            redirect('master/bedgroup', 'refresh');
        }
        else {
            $bed = array(
                'name' => $this->input->post('name'),
                'description' => $this->input->post('description'),
                'floor' => $this->input->post('floor'),
                'price' => $this->input->post('harga'),
            );

            $this->bedgroup_model->add_bed_group($bed);

            $this->session->set_flashdata('success', 'Berhasil menambah bed group!');
            redirect('master/bedgroup', 'refresh');
        }
    }

    function index() {
        $this->session->set_userdata('top_menu', 'setup');
        $this->session->set_userdata('sub_sidebar_menu', 'admin/setup/bedgroup');
        $this->session->set_userdata('sub_menu', 'bed');
        $data['bedgroup_list'] = $this->bedgroup_model->get_bedgroup();
        $data['floor'] = $this->floor_model->floor_list();
        $data['app'] = $this->applicares;

        $res = $this->get("{$this->base_url}rest/bed/read/$this->kode_ppk/1/100");
        if ($res->success) {
            $data['ketersediaan'] = $res->data->response->list;
        }
        else {
            $data['ketersediaan'] = [];
        }

        $this->template->view('master/rawat_inap/bed_group', $data);
    }

    function delete_bedgroup($id) {
        $v = $this->db->query("SELECT * FROM bed_group WHERE id = $id")->row();
        if ($v->is_bridging) {
            $req = json_decode($v->request);
            $body = [
                'kodekelas' => $req->kodekelas,
                'koderuang' => $req->koderuang,
            ];
            $res = $this->post("{$this->base_url}rest/bed/delete/{$this->kode_ppk}", json_encode($body));

            if ($res->success) {
                $this->bedgroup_model->delete_bedgroup($id);
                $this->session->set_flashdata('success', 'Berhasil menghapus dan bridging!');
                redirect('master/bedgroup', 'refresh');
            }
            else {
                $this->session->set_flashdata('warning', "Gagal bridging: {$res->server_output->metadata->message}");
                redirect('master/bedgroup', 'refresh');
            }
        }
        else {
            $this->bedgroup_model->delete_bedgroup($id);
            $this->session->set_flashdata('success', 'Berhasil menghapus!');
            redirect('master/bedgroup', 'refresh');
        }
    }

    function delete_bridging($kodekelas, $koderuang) {
        $q = null;
        $v = $this->db->query("SELECT * FROM bed_group")->result();

        foreach ($v as $b) {
            $req = json_decode($b->request);
            if ($req->kodekelas == urldecode($kodekelas) && $req->koderuang == urldecode($koderuang)) {
                $body = [
                    'kodekelas' => $req->kodekelas,
                    'koderuang' => $req->koderuang,
                ];
                $res = $this->post("{$this->base_url}rest/bed/delete/{$this->kode_ppk}", json_encode($body));

                if ($res->success) {
                    $this->MainModel->update('bed_group', [
                        'is_bridging' => 0,
                        'request' => '',
                        'response' => '',
                    ], $b->id);
                    $this->session->set_flashdata('success', 'Berhasil menghapus dan bridging!');
                    $q = true;
                }
                else {
                    $this->session->set_flashdata('warning', "Gagal bridging: {$res->server_output->metadata->message}");
                    $q = false;
                }

                break;
            }
        }

        if ($q === null) {
            $body = [
                'kodekelas' => $kodekelas,
                'koderuang' => $koderuang,
            ];
            $this->post("{$this->base_url}rest/bed/delete/{$this->kode_ppk}", json_encode($body));
            $this->session->set_flashdata('success', "Berhasil menghapus dan bridging!");
        }
        redirect('master/bedgroup', 'refresh');
    }

    function update_bedgroup() {
        $this->form_validation->set_rules('name', $this->lang->line('name'), 'trim|required');
        $this->form_validation->set_rules('floor', $this->lang->line('floor'), 'trim|required');

        if ($this->form_validation->run() == FALSE) {

            $msg = array(
                'name' => form_error('name'),
                'floor' => form_error('floor'),
            );

            $array = array('status' => 'fail', 'error' => $msg, 'message' => '');
        } else {

            $bed = array(
                'id' => $this->input->post('id'),
                'name' => $this->input->post('name'),
                'floor' => $this->input->post('floor'),
                'description' => $this->input->post('description'),
                'price' => $this->input->post('harga'),
            );

            $this->bedgroup_model->add_bed_group($bed);
            $msg = "Bed Group Updated Successfully";
            $array = array('status' => 'success', 'error' => '', 'message' => $this->lang->line('update_message'));
        }

        echo json_encode($array);
    }

    public function getbedgroupdata($id)
    {
        $data = $this->bedgroup_model->bedgroup_list($id);
        echo json_encode($data);
    }

    public function bpjs_update_tempat_tidur()
    {
//         error_reporting(E_ALL);
//
//         // Display errors in output
//         ini_set('display_errors', 1);

        $body = [
            'kodekelas' => $this->input->post('kodekelas'),
            'koderuang' => $this->input->post('koderuang'),
            'namaruang' => $this->input->post('namaruang'),
            'kapasitas' => $this->input->post('kapasitas'),
            'tersedia' => $this->input->post('tersedia'),
            'tersediapria' => $this->input->post('tersediapria'),
            'tersediawanita' => $this->input->post('tersediawanita'),
            'tersediapriawanita' => $this->input->post('tersediapriawanita'),
        ];

        $act = $this->input->post('is_bridging') ? 'update' : 'create';
        $res = $this->post("{$this->base_url}rest/bed/$act/{$this->kode_ppk}", json_encode($body));

        if ($res->success) {
            $this->MainModel->update('bed_group', [
                'request' => json_encode($body),
                'response' => json_encode($res),
                'is_bridging' => 1
            ], $this->input->post('id'));

            $this->session->set_flashdata('success', 'Berhasil bridging!');
            redirect('master/bedgroup', 'refresh');
        }
        else {
            $this->MainModel->update('bed_group', [
                'request' => json_encode($body),
                'response' => json_encode($res),
            ], $this->input->post('id'));

            $this->session->set_flashdata('warning', "Gagal bridging: {$res->server_output->metadata->message}");
            redirect('master/bedgroup', 'refresh');
        }
    }

    protected function post($url, $body)
    {
        $header = $this->signature();

        if ($this->dvlp) {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, 'https://app.ucare.id/api/bpjs');
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, [
                'bpjs_url' => $url,
                'bpjs_method' => 'POST',
                'bpjs_headers' => json_encode($this->signature()),
                'bpjs_body' => $body
            ]);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            $server_output = curl_exec ($ch);
            $error_exec = curl_errno($ch) ? curl_error($ch) : null;
            curl_close($ch);

            $server_output = json_decode($server_output);
        }
        else {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $body);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            $server_output = curl_exec ($ch);
            $error_exec = curl_errno($ch) ? curl_error($ch) : null;
            curl_close($ch);

            if ($error_exec) {
                return (object) [
                    'success' => false,
                    'server_output' => $error_exec
                ];
            }

            $server_output = json_decode($server_output);
        }

        if ($server_output->metaData->code == 200 || $server_output->metadata->code == 200 || $server_output->metadata->code == 1) {
            $response = $server_output->response;
            $decrypted = $this->stringDecrypt($response);
            $decompressed = $this->decompress($decrypted);
            if (!$decompressed) {
                return (object) [
                    'success' => true,
                    'data' => $server_output
                ];
            }
            return (object) [
                'success' => true,
                'data' => json_decode($decompressed)
            ];
        }
        else {
            return (object) [
                'success' => false,
                'server_output' => $server_output
            ];
        }
    }

    protected function get($url)
    {
        $header = $this->signature();

        if ($this->dvlp) {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, 'https://app.ucare.id/api/bpjs');
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, [
                'bpjs_url' => $url,
                'bpjs_method' => 'GET',
                'bpjs_headers' => json_encode($this->signature()),
            ]);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            $server_output = curl_exec ($ch);
            $error_exec = curl_errno($ch) ? curl_error($ch) : null;
            curl_close($ch);

            $server_output = json_decode($server_output);
        }
        else {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            $server_output = curl_exec ($ch);
            $error_exec = curl_errno($ch) ? curl_error($ch) : null;
            curl_close($ch);

            if ($error_exec) {
                return (object) [
                    'success' => false,
                    'server_output' => $error_exec
                ];
            }

            $server_output = json_decode($server_output);
        }

        if ($server_output->metaData->code == 200 || $server_output->metadata->code == 200 || $server_output->metadata->code == 1) {
            $response = $server_output->response;
            $decrypted = $this->stringDecrypt($response);
            $decompressed = $this->decompress($decrypted);
            if (!$decompressed) {
                return (object) [
                    'success' => true,
                    'data' => $server_output
                ];
            }
            return (object) [
                'success' => true,
                'data' => json_decode($decompressed)
            ];
        }
        else {
            return (object) [
                'success' => false,
                'server_output' => $server_output
            ];
        }
    }

    protected function signature()
    {
        $cons_id = $this->cons_id;
        $secret_key = $this->secret_key;
        $user_key = $this->user_key;

        date_default_timezone_set('UTC');
        $this->timestamp = strval(time()-strtotime('1970-01-01 00:00:00'));
        $tStamp = $this->timestamp;
        $signature = hash_hmac('sha256', $cons_id."&".$tStamp, $secret_key, true);
        $encodedSignature = base64_encode($signature);
        date_default_timezone_set('Asia/Jakarta');

        return [
            "X-cons-id: {$cons_id}",
            "X-timestamp: {$tStamp}",
            "X-signature: {$encodedSignature}",
            "user_key: {$user_key}",
            'Accept: application/json',
            'Content-Type: application/json',
        ];
    }

    function stringDecrypt($string)
    {
        $tStamp = $this->timestamp;
        $key = "{$this->cons_id}{$this->secret_key}{$tStamp}";

        $encrypt_method = 'AES-256-CBC';
        $key_hash = hex2bin(hash('sha256', $key));
        $iv = substr(hex2bin(hash('sha256', $key)), 0, 16);

        return openssl_decrypt(base64_decode($string), $encrypt_method, $key_hash, OPENSSL_RAW_DATA, $iv);
    }

    function decompress($string)
    {
        return LZString::decompressFromEncodedURIComponent($string);
    }

    public function test()
    {
        $body = [
            "kodekelas" => "KL3",
            "koderuang" => "ss",
            "namaruang" => "tes",
            "kapasitas" => "0",
            "tersedia" => "0",
            "tersediapria" => "0",
            "tersediawanita" => "0",
            "tersediapriawanita" => "0",
        ];
//            'bpjs_url' => 'https://dvlp.bpjs-kesehatan.go.id:8888/aplicaresws/rest/ref/kelas',

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://app.ucare.id/api/bpjs');
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, [
            'bpjs_url' => 'https://dvlp.bpjs-kesehatan.go.id:8888/aplicaresws/rest/bed/update/0207S001',
            'bpjs_method' => 'POST',
            'bpjs_headers' => json_encode($this->signature()),
            'bpjs_body' => json_encode($body)
        ]);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $server_output = curl_exec ($ch);

        $server_output = json_decode($server_output);
        $error_exec = curl_errno($ch) ? curl_error($ch) : null;
        curl_close($ch);

        if ($server_output->err) {
            $r = (object) [
                'success' => false,
                'server_output' => $server_output
            ];
        }
        else {
            $r = (object) [
                'success' => true,
                'data' => $server_output
            ];
        }
        die(json_encode($r));
    }

}
