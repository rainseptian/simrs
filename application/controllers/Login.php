<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
        $this->load->model('LoginModel');
	}

	public function index()
	{
		session_destroy();
		$this->load->view('login/form_login');
	}

    public function login() {
        $username = $this->input->post('username');
        $password = md5($this->input->post('password'));
        $checkusername = $this->LoginModel->check_username($this->input->post('username', TRUE))->row();
        $checkpassword = $this->LoginModel->check_password(md5($this->input->post('password', TRUE)))->row();

        $data_login = $this->LoginModel->ambilProfile($username,$password)->row();

        if($data_login) {
            if ($data_login->nama_grup == 'superadmin') {
                 $this->session->set_userdata('logged_in', $data_login);
                 redirect('Dashboard','refresh');
            }
            else if ($data_login->nama_grup == 'dokter') {
                $this->session->set_userdata('logged_in', $data_login);
                $non_rajal = $this->db->query("SELECT * FROM user_dokter_ruangan WHERE dokter_id = {$data_login->id}")->result();
                $this->session->set_userdata('non_rajal', $non_rajal);
                if ($data_login->id_jenis_pendaftaran == 59) {
                    redirect('Radio/search','refresh');
                }
                else {
                    redirect('pemeriksaan/listpemeriksaanPasien','refresh');
                }
            }
            else if ($data_login->nama_grup == 'perawat') {
                if ($data_login->jenis_perawat == 'igd') {
                    $this->session->set_userdata('logged_in', $data_login);
                    redirect('Igd/pendaftaran','refresh');
                }
                else if ($data_login->jenis_perawat == 'rawat_inap') {
                    $this->session->set_userdata('logged_in', $data_login);
                    redirect('RawatInap/showList','refresh');
                }
                else if ($data_login->jenis_perawat == 'ruang_bersalin') {
                    $this->session->set_userdata('logged_in', $data_login);
                    redirect('RuangBersalin/showList','refresh');
                }
                else if ($data_login->jenis_perawat == 'ruang_operasi') {
                    $this->session->set_userdata('logged_in', $data_login);
                    redirect('RuangOperasi/showList','refresh');
                }
                else {
                    $this->session->set_userdata('logged_in', $data_login);
                    redirect('PemeriksaanAwal','refresh');
                }
            }
            else if ($data_login->nama_grup == 'apoteker') {
                $this->session->set_userdata('logged_in', $data_login);
                redirect('apotek/resep_n','refresh');
            }
            else if ($data_login->nama_grup == 'front_office') {
                $this->session->set_userdata('logged_in', $data_login);
                redirect('pemeriksaan/listpemeriksaanPasien','refresh');
            }
            else if ($data_login->nama_grup == 'administrasi_keuangan') {
                $this->session->set_userdata('logged_in', $data_login);
                redirect('Insentif/listInsentifDokter','refresh');
            }
            else if ($data_login->nama_grup == 'admin') {
                $this->session->set_userdata('logged_in', $data_login);
                redirect('Dashboard','refresh');
            }
            else if ($data_login->nama_grup == 'kasir') {
                 $this->session->set_userdata('logged_in', $data_login);
                 redirect('Administrasi/listPasienSelesaiPeriksa','refresh');
            }
            else if ($data_login->nama_grup == 'laborat') {
                $this->session->set_userdata('logged_in', $data_login);
                redirect('pemeriksaan/listpemeriksaanPasien','refresh');
            }
            else if ($data_login->nama_grup == 'ekg') {
                $this->session->set_userdata('logged_in', $data_login);
                redirect('pemeriksaan/listpemeriksaanPasien','refresh');
            }
            else if ($data_login->nama_grup == 'spirometri') {
                $this->session->set_userdata('logged_in', $data_login);
                redirect('pemeriksaan/listpemeriksaanPasien','refresh');
            }
            else if ($data_login->nama_grup == 'admin_obat') {
                $this->session->set_userdata('logged_in', $data_login);
                redirect('apotek/stokObat','refresh');
            }
            else {
                $this->session->set_userdata('logged_in', $data_login);
                redirect('Dashboard','refresh');
            }
        }
        else if ($checkpassword && $checkusername && !$data_login){
            $this->session->set_flashdata('warning', 'Password dan Email tidak cocok!');
            redirect('Login','refresh');
        }
        else if (!$checkusername && $checkpassword){
            $this->session->set_flashdata('warning', 'Maaf username tidak benar!');
            redirect('Login','refresh');
        }
        else if ($checkusername && !$checkpassword) {
            $this->session->set_flashdata('warning', 'Maaf password tidak benar!');
            redirect('Login','refresh');
        }
        else {
        	$this->session->set_flashdata('warning', 'Maaf username dan password tidak benar!');
            redirect('Login','refresh');
        }
    }

    public function logout()
    {
       /* $id_user = $this->session->userdata('logged_in')['result']->client_id;
        $ip = $this->input->ip_address();*/
        //$this->LoginModel->Logout($id_user,$ip);
        redirect('Login/index','refresh');
    }
}
