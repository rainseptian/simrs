<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class LaporanHarianShift extends MY_Controller {

	public function __construct()
	{
		parent::__construct();

        $this->load->library('template');
        $this->load->Model('LaporanHarianShiftModel');
	}

	public function laporan_harian()
    {
        $from = $this->input->get('from') ? $this->input->get('from') : date('Y-m-01');
        $to = $this->input->get('to') ? $this->input->get('to') : date('Y-m-d');

        $data['list'] = $this->LaporanHarianShiftModel->byPeriode($from, $to);

        $this->template->view('laporan_harian_shift/list_laporan_harian_shift', $data);
    }

    public function input_laporan_harian()
    {
        $this->load->Model('PerawatModel');
        $this->load->Model('DokterModel');

        $data['perawats'] = $this->PerawatModel->listPerawat()->result();
        $data['dokters'] = $this->DokterModel->listDokter()->result();
        $data['edit'] = false;

        $this->template->view('laporan_harian_shift/input_laporan_harian_shift', $data);
    }

    public function edit_laporan_harian($id)
    {
        $this->load->Model('PerawatModel');
        $this->load->Model('DokterModel');

        $row = $this->LaporanHarianShiftModel->getById($id);

        $data['perawats'] = $this->PerawatModel->listPerawat()->result();
        $data['dokters'] = $this->DokterModel->listDokter()->result();
        $data['detail'] = $row;
        $data['edit'] = true;

        $this->template->view('laporan_harian_shift/input_laporan_harian_shift', $data);
    }

    public function detail($id)
    {
        $row = $this->LaporanHarianShiftModel->getById($id);
        $data['detail'] = $row;

        $this->template->view('laporan_harian_shift/detail_laporan_harian_shift', $data);
    }

    public function download($id)
    {
        $row = $this->LaporanHarianShiftModel->getById($id);
        $data['detail'] = $row;

        $date = date('dmY_His');
        $f_name = "laporan_harian_shift_" . $date . ".pdf";

        $this->load->helper('mpdf');
        $mpdf=new mPDF('c');
        $html = $this->load->view('laporan_harian_shift/print_laporan_harian_shift', $data, true);

        $mpdf->WriteHTML($html, 2);
        $mpdf->Output($f_name, 'D');
        exit;
    }

    public function cetak($id)
    {
        $row = $this->LaporanHarianShiftModel->getById($id);
        $data['detail'] = $row;

        $this->load->view('laporan_harian_shift/print_laporan_harian_shift', $data);
    }

    public function tambah()
    {
        $data = [
            "shift" => $this->input->post('shift'),
            "koordinator_id" => $this->input->post('koordinator_id'),
            "anggota_ids" => json_encode($this->input->post('anggota_ids')),
            "magang" => $this->input->post('magang'),
            "dokter_jaga_id" => $this->input->post('dokter_jaga_id'),
            "pasien_ranap" => $this->input->post('pasien_ranap'),
            "pasien_poli_umum" => $this->input->post('pasien_poli_umum'),
            "pasien_poli_igd" => $this->input->post('pasien_poli_igd'),
            "pasien_poli_obgyn" => $this->input->post('pasien_poli_obgyn'),
            "pasien_poli_bedah" => $this->input->post('pasien_poli_bedah'),
            "pasien_poli_anak" => $this->input->post('pasien_poli_anak'),
            "pasien_poli_penyakit_dalam" => $this->input->post('pasien_poli_penyakit_dalam'),
            "pasien_poli_bersalin" => $this->input->post('pasien_poli_bersalin'),
            "pasien_poli_operasi" => $this->input->post('pasien_poli_operasi'),
            "pasien_poli_hcu" => $this->input->post('pasien_poli_hcu'),
            "pasien_poli_perinatologi" => $this->input->post('pasien_poli_perinatologi'),
            "pasien_poli_radiologi" => $this->input->post('pasien_poli_radiologi'),
            "aps_antibody" => $this->input->post('aps_antibody'),
            "aps_antigen" => $this->input->post('aps_antigen'),
            "mrs_antibody" => $this->input->post('mrs_antibody'),
            "mrs_antigen" => $this->input->post('mrs_antigen'),
            "krs" => $this->input->post('krs'),
            "pasien_rujuk" => $this->input->post('pasien_rujuk'),
            "ambulance" => $this->input->post('ambulance'),
            "obat_emergency" => $this->input->post('obat_emergency'),
            "rm_lengkap" => $this->input->post('rm_lengkap'),
            "kejadian_penting" => $this->input->post('kejadian_penting'),
            "stok_antigen" => $this->input->post('stok_antigen'),
            "stok_antibody" => $this->input->post('stok_antibody'),
        ];
        
        $res = $this->LaporanHarianShiftModel->insert($data);

        if ($res)
        {
            $this->session->set_flashdata('success', 'Input laporan harian shift berhasil!');
        }
        else
        {
            $this->session->set_flashdata('error', 'Input laporan harian shift gagal!');
        }

        redirect('LaporanHarianShift/laporan_harian');
    }

    public function edit()
    {
        $data = [
            "shift" => $this->input->post('shift'),
            "koordinator_id" => $this->input->post('koordinator_id'),
            "anggota_ids" => json_encode($this->input->post('anggota_ids')),
            "magang" => $this->input->post('magang'),
            "dokter_jaga_id" => $this->input->post('dokter_jaga_id'),
            "pasien_ranap" => $this->input->post('pasien_ranap'),
            "pasien_poli_umum" => $this->input->post('pasien_poli_umum'),
            "pasien_poli_igd" => $this->input->post('pasien_poli_igd'),
            "pasien_poli_obgyn" => $this->input->post('pasien_poli_obgyn'),
            "pasien_poli_bedah" => $this->input->post('pasien_poli_bedah'),
            "pasien_poli_anak" => $this->input->post('pasien_poli_anak'),
            "pasien_poli_penyakit_dalam" => $this->input->post('pasien_poli_penyakit_dalam'),
            "pasien_poli_bersalin" => $this->input->post('pasien_poli_bersalin'),
            "pasien_poli_operasi" => $this->input->post('pasien_poli_operasi'),
            "pasien_poli_hcu" => $this->input->post('pasien_poli_hcu'),
            "pasien_poli_perinatologi" => $this->input->post('pasien_poli_perinatologi'),
            "pasien_poli_radiologi" => $this->input->post('pasien_poli_radiologi'),
            "aps_antibody" => $this->input->post('aps_antibody'),
            "aps_antigen" => $this->input->post('aps_antigen'),
            "mrs_antibody" => $this->input->post('mrs_antibody'),
            "mrs_antigen" => $this->input->post('mrs_antigen'),
            "krs" => $this->input->post('krs'),
            "pasien_rujuk" => $this->input->post('pasien_rujuk'),
            "ambulance" => $this->input->post('ambulance'),
            "obat_emergency" => $this->input->post('obat_emergency'),
            "rm_lengkap" => $this->input->post('rm_lengkap'),
            "kejadian_penting" => $this->input->post('kejadian_penting'),
            "stok_antigen" => $this->input->post('stok_antigen'),
            "stok_antibody" => $this->input->post('stok_antibody'),
        ];

        $res = $this->LaporanHarianShiftModel->update($data, $this->input->post('id'));

        if ($res)
        {
            $this->session->set_flashdata('success', 'Edit laporan harian shift berhasil!');
        }
        else
        {
            $this->session->set_flashdata('error', 'Edit laporan harian shift gagal!');
        }

        redirect('LaporanHarianShift/laporan_harian');
    }
}
