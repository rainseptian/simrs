<?php

class Bpjs extends MY_BpjsController
{
    public function __construct() {
        parent::__construct();

        $this->load->Model('AdministrasiModel');
        $this->load->helper(array('file', 'php_with_mpdf_helper'));
        $this->load->helper(array('file', 'mpdf'));
    }

    public function poli()
    {
        echo $this->ajax_search_poli();
    }

    public function dpjp()
    {
        echo $this->ajax_search_dpjp();
    }

    public function ajax_nomor_kartu()
    {
        echo parent::ajax_nomor_kartu();
    }

    public function ajax_search_faskes()
    {
        echo parent::ajax_search_faskes();
    }

    public function ajax_search_diagnosa()
    {
        echo parent::ajax_search_diagnosa();
    }

    public function ajax_search_obat()
    {
        echo parent::ajax_search_obat();
    }

    public function ajax_search_poli()
    {
        echo parent::ajax_search_poli();
    }

    public function ajax_search_provinsi()
    {
        echo parent::ajax_search_provinsi();
    }

    public function ajax_search_kabupaten()
    {
        echo parent::ajax_search_kabupaten();
    }

    public function ajax_search_kecamatan()
    {
        echo parent::ajax_search_kecamatan();
    }

    public function ajax_search_dpjp()
    {
        echo parent::ajax_search_dpjp();
    }

    public function ajax_search_procedure()
    {
        echo parent::ajax_search_procedure();
    }

    public function ajax_search_peserta_by_nik()
    {
        echo parent::ajax_search_peserta_by_nik();
    }

    public function ajax_search_peserta_by_no_bpjs()
    {
        echo parent::ajax_search_peserta_by_no_bpjs();
    }

    public function ajax_search_rujukan_by_no_bpjs()
    {
        echo parent::ajax_search_rujukan_by_no_bpjs();
    }

    public function ajax_search_rujukan()
    {
        echo parent::ajax_search_rujukan();
    }

    public function ajax_search_spesialistik_rujukan()
    {
        echo parent::ajax_search_spesialistik_rujukan();
    }

    public function ajax_search_sep()
    {
        echo parent::ajax_search_sep();
    }

    public function ajax_search_surat_kontrol()
    {
        echo parent::ajax_search_surat_kontrol();
    }

    public function ajax_search_jadwal_dokter()
    {
        echo parent::ajax_search_jadwal_dokter();
    }
}