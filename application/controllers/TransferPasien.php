<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class TransferPasien extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->load->library('template');
        $this->load->Model('PendaftaranModel');
        $this->load->Model('TransferPasienModel');
        $this->load->Model('RawatInapModel');
        $this->load->Model('RuangBersalinModel');
        $this->load->Model('RuangOperasiModel');
        $this->load->Model('MainModel');
    }

    public function showList($jenis = 1)
    {
        if ($jenis == 1) {
            $data['list'] = $this->TransferPasienModel->getTujuanRanap();
        }
        else {
            $data['list'] = $this->TransferPasienModel->getBaruByJenis($jenis);
        }
        $data['poli'] = $this->TransferPasienModel->getPoli();
        $this->template->view('transfer/list',$data);
    }

    public function listInap($jenis = 1)
    {
        $data['jenis'] = $jenis;
        $data['dari'] = $jenis == 3 ? 'Bangsal' : $this->TransferPasienModel->getDariRuangByJenisTransferId($jenis)->nama;
        $data['ke'] = $jenis == 3 ? 'Bangsal Lain' : $this->TransferPasienModel->getKeRuangByJenisTransferId($jenis)->nama;

        $jenis_transfer = $this->TransferPasienModel->getJenisTransferById($jenis);

        if ($jenis_transfer->dari_jenis_ruangan_id == 2) { // bersalin
            $data['list'] = $this->RuangBersalinModel->getBaru();
            foreach ($data['list'] as &$d) {
                $d['dari'] = $data['dari'];
            }
        }
        else if ($jenis_transfer->dari_jenis_ruangan_id == 3) {
            $data['list'] = $this->RuangOperasiModel->getBaru();
            foreach ($data['list'] as &$d) {
                $d['dari'] = $data['dari'];
            }
        }
        else {
            $data['list'] = $this->RawatInapModel->getBaru();
            foreach ($data['list'] as &$d) {
                $tf = $this->RawatInapModel->getLastTransferByRawatInapId($d['id']);
                if ($tf->jenis == 3) {
                    $bed = $this->bed_model->bed_listsearch($d['dari']);
                    $d['dari'] = $bed['name'].' - '.$bed['bedgroup'];
                }
                else if ($tf->jenis == 1) {
                    $d['dari'] = $d['nama_poli'];
                }
            }
        }

        $this->template->view('transfer/listInap', $data);
    }

    public function add($jenis)
    {
        $data['jenis'] = $jenis;
        $data['dokter'] = $this->PendaftaranModel->getDokter();

        $jenis_transfer = $this->TransferPasienModel->getJenisTransferById($jenis);
        $data['dari_jenis_ruangan_id'] = $jenis_transfer->dari_jenis_ruangan_id;
        $data['dari'] = $this->TransferPasienModel->getJenisRuanganById($jenis_transfer->dari_jenis_ruangan_id)->nama;

        if ($jenis_transfer->ke_jenis_ruangan_id == 4) { // ke peri
            $data['tujuan'] = $this->bed_model->perinatologi_bed_listsearch();
        }
        else if ($jenis_transfer->ke_jenis_ruangan_id == 5) { // ke hcu
            $data['tujuan'] = $this->bed_model->hcu_bed_listsearch();
        }
        else if ($jenis == 3) {
            $data['tujuan'] = $this->bed_model->bed_listsearch();
        }
        else {
            $data['tujuan'] = $this->TransferPasienModel->getJenisRuanganById($jenis_transfer->ke_jenis_ruangan_id)->nama;
        }

        if ($jenis_transfer->id == 1) {
            $this->template->view('transfer/add',$data);
        }
        else if ($jenis_transfer->dari_jenis_ruangan_id == 1 || $jenis_transfer->dari_jenis_ruangan_id == 2 || $jenis_transfer->dari_jenis_ruangan_id == 3) { // dari ranap, vk, ok
            $ri_id = $this->input->post('rawat_inap_id');
            $data['rawat_inap'] = $this->RawatInapModel->getById($ri_id);

            $this->template->view('transfer/add',$data);
        }
        else if ($jenis_transfer->dari_jenis_ruangan_id == 6) { // dari poli
            $data['current_place_type'] = strtolower($this->TransferPasienModel->getJenisRuanganById($jenis_transfer->ke_jenis_ruangan_id)->nama);
            $this->template->view('transfer/add_without_bangsal',$data);
        }
        else { // bukan dari poli bukan dari ranap
            $data['current_place_type'] = strtolower($this->TransferPasienModel->getJenisRuanganById($jenis_transfer->ke_jenis_ruangan_id)->nama);
            $this->template->view('transfer/add',$data);
        }
    }

    public function edit($transfer_id)
    {
        $data['transfer'] = $this->TransferPasienModel->getTransferDetailById($transfer_id);
        $data['redirect'] = $this->input->post('redirect');

        $jenis_transfer = $this->TransferPasienModel->getJenisTransferById($data['transfer']->jenis);

        if ($jenis_transfer->dari_jenis_ruangan_id == 6) {
            $data['dari'] = $data['transfer']->nama_poli;
        }
        else {
            $data['dari'] = $this->TransferPasienModel->getJenisRuanganById($jenis_transfer->dari_jenis_ruangan_id)->nama;
        }

        $data['tujuan'] = $this->TransferPasienModel->getJenisRuanganById($jenis_transfer->ke_jenis_ruangan_id)->nama;

        $this->template->view('transfer/edit',$data);
    }

    public function insert()
    {
        $jenis = $this->input->post('jenis');

        $jenis_transfer = $this->TransferPasienModel->getJenisTransferById($jenis);

        if ($jenis == 1) {
            $dari = $this->input->post('id_poli_dari');
            $transfer = [
                'jenis' => $jenis,
                'tanggal' => $this->input->post('tanggal'),
                'pukul' => $this->input->post('pukul'),
                'pasien_id' => $this->input->post('id_pasien'),
                'dokter_id' => $this->input->post('dokter'),
                'dpjp_id' => $this->input->post('dpjp'),
                'penanggung_jawab' => $this->input->post('penanggung_jawab'),
                'jenis_ruangan_id' => 1,
                'dari' => $dari,
                'tujuan' => $this->input->post('tujuan'),
                'current_place_type' => 'bangsal',
                'current_place_id' => $this->input->post('tujuan'),
                'catatan' => $this->input->post('note'),
                'status' => 'baru',
                'meta' => serialize($this->input->post('meta')),
            ];
            $this->MainModel->insert('transfer', $transfer);

            redirect('TransferPasien/showList/'.$jenis);
        }
        else if ($jenis == 3) {
            $rawat_inap_id = $this->input->post('rawat_inap_id');
            $ri = $this->RawatInapModel->getById($rawat_inap_id);
            $dari = $this->input->post('id_bed_dari');

            $transfer = [
                'rawat_inap_id' => $rawat_inap_id,
                'prev_transfer_id' => $ri->transfer_id,
                'jenis' => $jenis,
                'pasien_id' => $ri->pasien_id,
                'dokter_id' => $ri->dokter_id,
                'dpjp_id' => $ri->dpjp_id,
                'penanggung_jawab' => $ri->penanggung_jawab,
                'jenis_ruangan_id' => 1,
                'dari' => $dari,
                'tujuan' => $this->input->post('tujuan'),
                'current_place_type' => 'bangsal',
                'current_place_id' => $this->input->post('tujuan'),
                'tipe_pasien' => $ri->tipe_pasien,
                'catatan' => $this->input->post('note'),
                'status' => 'ditransfer',
                'transfered_at' => date('Y-m-d H:i:s'),
                'meta' => serialize($this->input->post('meta')),
            ];
            $new_tf_id = $this->MainModel->insert_id('transfer', $transfer);
            $this->MainModel->update('transfer', ['status' => 'selesai'], $ri->transfer_id);

            // ini adalah untuk mengeset new state untuk current rawat inap
            $rawat_inap = [
                'transfer_id' => $new_tf_id,
                'pasien_id' => $this->input->post('id_pasien'),
                'dokter_id' => $this->input->post('dokter'),
                'dpjp_id' => $this->input->post('dpjp'),
                'penanggung_jawab' => $this->input->post('penanggung_jawab'),
                'dari' => $dari,
                'tujuan' => $this->input->post('tujuan'),
                'current_place_type' => 'bangsal',
                'current_place_id' => $this->input->post('tujuan'),
                'tipe_pasien' => $this->input->post('meta')['status_pasien'],
                'catatan' => $this->input->post('note'),
                'meta' => serialize($this->input->post('meta')),
            ];
            $this->MainModel->update('rawat_inap', $rawat_inap, $rawat_inap_id);

            $this->MainModel->update('bed', ['is_active' => 'yes'], $dari);
            $this->MainModel->update('bed', ['is_active' => 'no'], $this->input->post('tujuan'));

            redirect('RawatInap/showList/'.$jenis);
        }
        else if ($jenis_transfer->dari_jenis_ruangan_id == 1 || $jenis_transfer->dari_jenis_ruangan_id == 2 || $jenis_transfer->dari_jenis_ruangan_id == 3) {
            $ke_ruangan = $this->TransferPasienModel->getJenisRuanganById($jenis_transfer->ke_jenis_ruangan_id)->nama;
            $rawat_inap_id = $this->input->post('rawat_inap_id');
            $ri = $this->RawatInapModel->getById($rawat_inap_id);

            $transfer = [
                'rawat_inap_id' => $rawat_inap_id,
                'prev_transfer_id' => $ri->transfer_id,
                'jenis' => $jenis,
                'tanggal' => $this->input->post('tanggal'),
                'pukul' => $this->input->post('pukul'),
                'pasien_id' => $ri->pasien_id,
                'dokter_id' => $ri->dokter_id,
                'dpjp_id' => $ri->dpjp_id,
                'penanggung_jawab' => $ri->penanggung_jawab,
                'jenis_ruangan_id' => $jenis_transfer->ke_jenis_ruangan_id,
                'dari' => $ri->tujuan,
                'tujuan' => $ke_ruangan,
                'current_place_type' => strtolower($ke_ruangan),
                'current_place_id' => 0,
                'tipe_pasien' => $ri->tipe_pasien,
                'catatan' => $this->input->post('note'),
                'status' => 'ditransfer',
                'meta' => serialize($this->input->post('meta')),
                'transfered_at' => date('Y-m-d H:i:s'),
            ];
            $id = $this->MainModel->insert_id('transfer', $transfer);

            $trans = $this->TransferPasienModel->getTransferDetailById($id);

            $rawat_inap = [
                'transfer_id' => $id,
                'pasien_id' => $trans->pasien_id,
                'dokter_id' => $trans->dokter_id,
                'dpjp_id' => $trans->dpjp_id,
                'penanggung_jawab' => $trans->penanggung_jawab,
                'dari' => $trans->dari,
                'tujuan' => $trans->tujuan,
                'current_place_type' => $trans->current_place_type,
                'current_place_id' => $trans->current_place_id,
                'tipe_pasien' => unserialize($trans->meta)['status_pasien'],
                'catatan' => $trans->catatan,
                'meta' => $trans->meta,
            ];
            $this->MainModel->update($tabel = 'rawat_inap', $rawat_inap, $rawat_inap_id);

            $this->session->set_flashdata('success', "Berhasil transfer ke $ke_ruangan!");
            $r = $jenis_transfer->ke_jenis_ruangan_id == 1 ? 'RawatInap' : str_replace(' ', '', $ke_ruangan);
            redirect($r.'/showList');
        }

//        else if ($jenis == 2) {
//            $rawat_inap_id = $this->input->post('rawat_inap_id');
//            $ri = $this->RawatInapModel->getById($rawat_inap_id);
//
//            $transfer = [
//                'rawat_inap_id' => $rawat_inap_id,
//                'prev_transfer_id' => $ri->transfer_id,
//                'jenis' => $jenis,
//                'tanggal' => $this->input->post('tanggal'),
//                'pukul' => $this->input->post('pukul'),
//                'pasien_id' => $ri->pasien_id,
//                'dokter_id' => $ri->dokter_id,
//                'penanggung_jawab' => $ri->penanggung_jawab,
//                'jenis_ruangan_id' => 3,
//                'dari' => $ri->tujuan,
//                'tujuan' => 'Ruang Operasi',
//                'current_place_type' => 'ruang operasi',
//                'current_place_id' => 0,
//                'tipe_pasien' => $ri->tipe_pasien,
//                'catatan' => $this->input->post('note'),
//                'status' => 'ditransfer',
//                'meta' => serialize($this->input->post('meta')),
//                'transfered_at' => date('Y-m-d H:i:s'),
//            ];
//            $id = $this->MainModel->insert_id('transfer', $transfer);
//
//            $trans = $this->TransferPasienModel->getTransferDetailById($id);
//
//            $rawat_inap = [
//                'transfer_id' => $id,
//                'pasien_id' => $trans->pasien_id,
//                'dokter_id' => $trans->dokter_id,
//                'penanggung_jawab' => $trans->penanggung_jawab,
//                'dari' => $trans->dari,
//                'tujuan' => $trans->tujuan,
//                'current_place_type' => $trans->current_place_type,
//                'current_place_id' => $trans->current_place_id,
//                'tipe_pasien' => unserialize($trans->meta)['status_pasien'],
//                'catatan' => $trans->catatan,
//                'meta' => $trans->meta,
//            ];
//            $this->MainModel->update($tabel = 'rawat_inap', $rawat_inap, $rawat_inap_id);
//
//            $this->session->set_flashdata('success', 'Berhasil transfer ke ruang operasi!');
//            redirect('RuangOperasi/showList');
//
////            redirect('TransferPasien/showList/'.$jenis);
//        }
//        else if ($jenis == 4) {
//            $rawat_inap_id = $this->input->post('rawat_inap_id');
//            $ri = $this->RawatInapModel->getById($rawat_inap_id);
//
//            $transfer = [
//                'rawat_inap_id' => $rawat_inap_id,
//                'prev_transfer_id' => $ri->transfer_id,
//                'jenis' => $jenis,
//                'tanggal' => $this->input->post('tanggal'),
//                'pukul' => $this->input->post('pukul'),
//                'pasien_id' => $ri->pasien_id,
//                'dokter_id' => $ri->dokter_id,
//                'penanggung_jawab' => $ri->penanggung_jawab,
//                'jenis_ruangan_id' => 2,
//                'dari' => $ri->tujuan,
//                'tujuan' => 'Ruang Bersalin',
//                'current_place_type' => 'ruang bersalin',
//                'current_place_id' => 0,
//                'tipe_pasien' => $ri->tipe_pasien,
//                'catatan' => $this->input->post('note'),
//                'status' => 'ditransfer',
//                'meta' => serialize($this->input->post('meta')),
//                'transfered_at' => date('Y-m-d H:i:s'),
//            ];
//            $id = $this->MainModel->insert_id('transfer', $transfer);
//
//            $trans = $this->TransferPasienModel->getTransferDetailById($id);
//
//            $rawat_inap = [
//                'transfer_id' => $id,
//                'pasien_id' => $trans->pasien_id,
//                'dokter_id' => $trans->dokter_id,
//                'penanggung_jawab' => $trans->penanggung_jawab,
//                'dari' => $trans->dari,
//                'tujuan' => $trans->tujuan,
//                'current_place_type' => $trans->current_place_type,
//                'current_place_id' => $trans->current_place_id,
//                'tipe_pasien' => unserialize($trans->meta)['status_pasien'],
//                'catatan' => $trans->catatan,
//                'meta' => $trans->meta,
//            ];
//            $this->MainModel->update($tabel = 'rawat_inap', $rawat_inap, $rawat_inap_id);
//
//            $this->session->set_flashdata('success', 'Berhasil transfer ke ruang bersalin!');
//            redirect('RuangBersalin/showList');
//
////            redirect('TransferPasien/showList/'.$jenis);
//        }
    }

    public function insertWithoutBangsal()
    {
        $jenis = $this->input->post('jenis');
        $jenis_transfer = $this->TransferPasienModel->getJenisTransferById($jenis);
        $jenis_ruangan_id = $jenis_transfer->ke_jenis_ruangan_id;
        $ke_ruangan = $this->TransferPasienModel->getJenisRuanganById($jenis_transfer->ke_jenis_ruangan_id)->nama;

        $transfer = [
            'jenis' => $jenis,
            'tanggal' => $this->input->post('tanggal'),
            'pukul' => $this->input->post('pukul'),
            'pasien_id' => $this->input->post('id_pasien'),
            'dokter_id' => $this->input->post('dokter'),
            'dpjp_id' => $this->input->post('dpjp'),
            'penanggung_jawab' => $this->input->post('penanggung_jawab'),
            'jenis_ruangan_id' => $jenis_ruangan_id,
            'dari' => $this->input->post('id_poli_dari'),
            'tujuan' => $this->input->post('tujuan'),
            'current_place_type' => $this->input->post('current_place_type'),
            'current_place_id' => $this->input->post('tujuan'),
            'catatan' => $this->input->post('note'),
            'meta' => serialize($this->input->post('meta')),
            'status' => 'ditransfer',
            'transfered_at' => date('Y-m-d H:i:s'),
            'via_bangsal' => 0
        ];
        $transfer_id = $this->MainModel->insert_id('transfer', $transfer);

        $trans = $this->TransferPasienModel->getTransferDetailById($transfer_id);
        $rawat_inap = [
            'transfer_id' => $transfer_id,
            'pasien_id' => $trans->pasien_id,
            'dokter_id' => $trans->dokter_id,
            'dpjp_id' => $trans->dpjp_id,
            'penanggung_jawab' => $trans->penanggung_jawab,
            'from_poli_id' => $trans->dari,
            'dari' => $trans->dari,
            'tujuan' => $trans->tujuan,
            'current_place_type' => $this->input->post('current_place_type'),
            'current_place_id' => $trans->tujuan,
            'tipe_pasien' => unserialize($trans->meta)['status_pasien'],
            'no_jaminan' => unserialize($trans->meta)['no_jaminan'],
            'pendaftaran_id' => unserialize($trans->meta)['pendaftaran_id'],
            'catatan' => $trans->catatan,
            'meta' => $trans->meta,
        ];
        $ri_id = $this->MainModel->insert_id('rawat_inap', $rawat_inap);

        $this->MainModel->update('transfer', ['rawat_inap_id' => $ri_id], $transfer_id);

        $this->session->set_flashdata('success', "Berhasil transfer ke $ke_ruangan!");
        $r = $jenis_transfer->ke_jenis_ruangan_id == 1 ? 'RawatInap' : str_replace(' ', '', $ke_ruangan);
        redirect($r.'/showList');
    }

    public function update()
    {
        $transfer_id = $this->input->post('transfer_id');
        $transfer = [
            'catatan' => $this->input->post('note'),
            'meta' => serialize($this->input->post('meta')),
            'updated_at' => date('Y-m-d H:i:s'),
        ];
        $this->MainModel->update($tabel = 'transfer', $transfer, $transfer_id);

        $this->session->set_flashdata('success', 'Berhasil mengedit data transfer!');
        redirect($this->input->post('redirect'));
    }

    public function daftar($id)
    {
        $data['transfer'] = $this->TransferPasienModel->getTransferDetailById($id);
        $data['poli'] = $this->TransferPasienModel->getPoli();
        $data['tujuan'] = $this->bed_model->bed_active_list_search();

        if ($data['transfer']->jenis == 2 || $data['transfer']->jenis == 4) {
            $prev_tf = $this->TransferPasienModel->getTransferDetailById($data['transfer']->prev_transfer_id);
            $data['rawat_inap'] = $this->RawatInapModel->getByTransferId($prev_tf->id);
        }
        if ($data['transfer']->jenis == 5 || $data['transfer']->jenis == 6) {
            $data['rawat_inap'] = $this->RawatInapModel->getByTransferId($id);
        }

        $this->template->view('transfer/daftar',$data);
    }

    // 1 => poli ke bangsal
    // 2 => bangsal ke ruang operasi
    // 3 => bangsal ke bangsal
    // 4 => bangsal ke ruang bersalin
    // 5 => ruang operasi ke bangsal
    // 6 => ruang bersalin ke bangsal
    // 7 => poli ke ruang operasi
    // 8 => poli ke ruang bersalin

    // jenis 2, 4 tidak manggil fungsi ini, soalnya langsung tertransfer ke ruang vk / ok tanpa klik daftar
    public function submitDaftar($jenis = 1)
    {
        $id = $this->input->post('id');
        $transfer = [
            'tujuan' => $this->input->post('tujuan'),
            'catatan' => $this->input->post('note'),
            'current_place_id' => $this->input->post('tujuan'),
            'status' => 'ditransfer',
            'transfered_at' => date('Y-m-d H:i:s'),
        ];
        $this->MainModel->update($tabel = 'transfer', $transfer, $id);

        $trans = $this->TransferPasienModel->getTransferDetailById($id);

        if ($jenis == 1) {
            $rawat_inap = [
                'transfer_id' => $id,
                'pasien_id' => $trans->pasien_id,
                'dokter_id' => $trans->dokter_id,
                'dpjp_id' => $trans->dpjp_id,
                'penanggung_jawab' => $trans->penanggung_jawab,
                'from_poli_id' => $trans->dari,
                'dari' => $trans->dari,
                'tujuan' => $trans->tujuan,
                'current_place_type' => 'bangsal',
                'current_place_id' => $trans->tujuan,
                'tipe_pasien' => unserialize($trans->meta)['status_pasien'],
                'no_jaminan' => unserialize($trans->meta)['no_jaminan'],
                'pendaftaran_id' => unserialize($trans->meta)['pendaftaran_id'],
                'catatan' => $trans->catatan,
                'meta' => $trans->meta,
            ];
            $ri_id = $this->MainModel->insert_id('rawat_inap', $rawat_inap);
            $this->MainModel->update('transfer', ['rawat_inap_id' => $ri_id], $id);
            $this->MainModel->update('bed', ['is_active' => 'no'], $trans->tujuan);

            $this->session->set_flashdata('success', 'Berhasil daftar rawat inap!');
            redirect('RawatInap/showList');
        }
        else if ($jenis == 2) {
            $rawat_inap = [
                'transfer_id' => $id,
                'pasien_id' => $trans->pasien_id,
                'dokter_id' => $trans->dokter_id,
                'dpjp_id' => $trans->dpjp_id,
                'penanggung_jawab' => $trans->penanggung_jawab,
                'dari' => $trans->dari,
                'tujuan' => $trans->tujuan,
                'current_place_type' => $trans->current_place_type,
                'current_place_id' => $trans->current_place_id,
                'tipe_pasien' => unserialize($trans->meta)['status_pasien'],
                'no_jaminan' => unserialize($trans->meta)['no_jaminan'],
                'pendaftaran_id' => unserialize($trans->meta)['pendaftaran_id'],
                'catatan' => $trans->catatan,
                'meta' => $trans->meta,
            ];
            $prev_tf = $this->TransferPasienModel->getTransferDetailById($trans->prev_transfer_id);
            $rawat_inap_id = $this->RawatInapModel->getByTransferId($prev_tf->id)->id;
            $this->MainModel->update($tabel = 'rawat_inap', $rawat_inap, $rawat_inap_id);

            $this->session->set_flashdata('success', 'Berhasil transfer ke ruang operasi!');
            redirect('RuangOperasi/showList');
        }
        else if ($jenis == 4) {
            $rawat_inap = [
                'transfer_id' => $id,
                'pasien_id' => $trans->pasien_id,
                'dokter_id' => $trans->dokter_id,
                'dpjp_id' => $trans->dpjp_id,
                'penanggung_jawab' => $trans->penanggung_jawab,
                'dari' => $trans->dari,
                'tujuan' => $trans->tujuan,
                'current_place_type' => $trans->current_place_type,
                'current_place_id' => $trans->current_place_id,
                'tipe_pasien' => unserialize($trans->meta)['status_pasien'],
                'no_jaminan' => unserialize($trans->meta)['no_jaminan'],
                'pendaftaran_id' => unserialize($trans->meta)['pendaftaran_id'],
                'catatan' => $trans->catatan,
                'meta' => $trans->meta,
            ];
            $prev_tf = $this->TransferPasienModel->getTransferDetailById($trans->prev_transfer_id);
            $rawat_inap_id = $this->RawatInapModel->getByTransferId($prev_tf->id)->id;
            $this->MainModel->update($tabel = 'rawat_inap', $rawat_inap, $rawat_inap_id);

            $this->session->set_flashdata('success', 'Berhasil transfer ke ruang bersalin!');
            redirect('RuangBersalin/showList');
        }
        else if ($jenis == 5 || $jenis == 6) {
            $this->MainModel->update('rawat_inap', [
                'transfer_id' => $id,
                'dari' => $this->input->post('dari'),
                'tujuan' => $this->input->post('tujuan'),
                'status' => 'Masuk',
                'current_place_type' => 'bangsal',
                'current_place_id' => $this->input->post('tujuan'),
                'via_bangsal' => 1
            ], $trans->rawat_inap_id);
            $this->MainModel->update('bed', ['is_active' => 'no'], $this->input->post('tujuan'));

            $this->session->set_flashdata('success', 'Berhasil transfer ke rawat inap!');
            redirect('RawatInap/showList');
        }
    }

    public function hapus($id)
    {
        if ($this->TransferPasienModel->hapusPendaftaran($id)) {
            $this->session->set_flashdata('success', 'Data berhasil dihapus!');
        }
        else {
            $this->session->set_flashdata('warning', 'Data gagal dihapus!');
        }
        redirect('TransferPasien/showList');
    }

    public function getPasienInfo($pasien_id)
    {
        $r['poli'] = $this->TransferPasienModel->getLastPoliByPasienId($pasien_id);
        $r['q'] = $this->db->last_query();
        $r['pasien'] = $this->TransferPasienModel->getPasienById($pasien_id);
        $r['pemeriksaan'] = $this->TransferPasienModel->getLastPemeriksaanByPasienId($pasien_id);

        echo json_encode($r);
    }
}
