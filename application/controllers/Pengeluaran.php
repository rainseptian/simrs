<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pengeluaran extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->load->library('template');
        $this->load->Model('MainModel');
        $this->load->Model('PengeluaranModel');
    }

    public function list_item()
    {
        $data['list'] = $this->PengeluaranModel->get_list_items();
        $this->template->view('master/pengeluaran/list',$data);
    }

    public function create_item()
    {
        if ($this->input->post('submit') == 1) {
            $insert = $this->MainModel->insert('pengeluaran_item', [
                'kode' => $this->input->post('kode'),
                'nama' => $this->input->post('nama'),
            ]);

            if ($insert) {
                $this->session->set_flashdata('success', 'Tambah item pengeluaran berhasil!');
                redirect('Pengeluaran/list_item', 'refresh');
            }
            else {
                $this->session->set_flashdata('warning', 'Tambah item pengeluaran gagal!');
                redirect('Pengeluaran/list_item', 'refresh');
            }
        }
        else {
            $this->template->view('master/pengeluaran/create');
        }
    }

    public function edit_item($id)
    {
        if ($this->input->post('submit') == 1) {
            $update = $this->MainModel->update('pengeluaran_item', [
                'kode' => $this->input->post('kode'),
                'nama' => $this->input->post('nama'),
            ], $id);

            if ($update) {
                $this->session->set_flashdata('success', 'Edit item pengeluaran berhasil!');
                redirect('Pengeluaran/list_item', 'refresh');
            }
            else {
                $this->session->set_flashdata('warning', 'Edit item pengeluaran gagal!');
                redirect('Pengeluaran/list_item', 'refresh');
            }
        }
        else {
            $data['data'] = $this->PengeluaranModel->find_item($id);
            $this->template->view('master/pengeluaran/edit', $data);
        }
    }

    public function delete_item($id)
    {
        $this->MainModel->delete($table='pengeluaran_item', ['is_active' => '0'], $id);
        $this->session->set_flashdata('success', 'Data item pengeluaran berhasil dihapus!');
        redirect('Pengeluaran/list_item');
    }
}