<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class RawatInap extends Admin_Controller
{
    // 1 adalah bangsal
    private $jenis_ruangan_id = 1;

    public function __construct()
    {
        parent::__construct();

        $this->marital_status = $this->config->item('marital_status');
        $this->payment_mode = $this->config->item('payment_mode');
        $this->blood_group = $this->config->item('bloodgroup');
        $this->load->model('RawatInapModel');
        $this->load->model('PendaftaranModel');
        $this->load->model('MainModel');
        $this->load->model('ObatModel');
        $this->load->model('AdministrasiModel');
        $this->load->model('PerawatModel');
        $this->load->model('PemeriksaanModel');
        $this->load->model('TarifTindakanModel');
        $this->load->model('PenyakitModel');
        $this->load->model('LaporanModel');
        $this->load->model('UserModel');
        $this->load->model('JenisLayananLaboratoirumModel');
        $this->load->model('DetailObatPemeriksaanModel');
        $this->load->model('DetailObatRacikanPemeriksaanModel');

        $this->load->library('Customlib');
        $this->load->library('template');
    }

    public function showList()
    {
        $data['list'] = $this->RawatInapModel->getBaru();
        foreach ($data['list'] as &$d) {
            $tf = $this->RawatInapModel->getLastTransferByRawatInapId($d['id']);
//            if ($d['id'] == 4125)
//            die(json_encode($tf));
            if ($tf->jenis == 3) {
                $bed = $this->bed_model->bed_listsearch($d['dari']);
                $d['dari'] = $bed['name'].' - '.$bed['bedgroup'];
            }
            else if ($tf->jenis == 1) {
                $d['dari'] = $d['nama_poli'];
            }
        }

        $data['is_superadmin'] = $this->session->userdata('logged_in')->id == 1;
        $this->template->view('rawat_inap/list', $data);
    }

    public function hapusPasien($id)
    {
        $this->MainModel->hardDelete('rawat_inap', $id);
        $this->MainModel->hardDeleteWhere('transfer', ['rawat_inap_id', $id]);
        redirect('RawatInap/showList');
    }

    public function listBolehPulang()
    {
        $data['list'] = $this->RawatInapModel->getBolehPulang();
        $this->template->view('rawat_inap/list_boleh_pulang', $data);
    }

    public function listBilling()
    {
        $data['list'] = $this->RawatInapModel->getForBilling();
        $this->template->view('rawat_inap/list_billing', $data);
    }

    public function listRekapitulasi()
    {
        $data['list'] = $this->RawatInapModel->getSelesai();
        $this->template->view('rawat_inap/list_rekapitulasi', $data);
    }

    public function dataTransfer($jenis)
    {
        $data['jenis'] = $jenis;
        if ($jenis == 'masuk') {
            $data['list'] = $this->RawatInapModel->getDataTransferMasuk();
        }
        else {
            $data['list'] = $this->RawatInapModel->getDataTransferKeluar();
        }

        $this->template->view('rawat_inap/list_data_transfer', $data);
    }

    private function pengkajian_awal($rawat_inap)
    {
        $last_pendaftaran = $this->db->query("
            SELECT pp.*, p.id as pemeriksaan_id FROM pendaftaran_pasien pp
            JOIN pemeriksaan p on pp.id = p.pendaftaran_id
            WHERE pp.pasien = $rawat_inap->pasien_id
            AND pp.jenis_pendaftaran_id = $rawat_inap->from_poli_id
            ORDER BY pp.waktu_pendaftaran desc 
        ")->row();
        $pemeriksaan_id = $last_pendaftaran->pemeriksaan_id;
//        if (!$pemeriksaan_id) {
//            return [
//                'view' => '',
//                'pemeriksaan_id' => '',
//            ];
//        }

        $data['pendaftaran'] = $this->PemeriksaanModel->getPendaftaranByIdPemeriksaan($pemeriksaan_id)->row_array();
        $data['pendaftaran']['pasien'] = $rawat_inap->pasien_id;
        $data['pemeriksaan'] = $this->PemeriksaanModel->getPemeriksaanById($pemeriksaan_id)->row_array();
        $data['images'] = $this->db->query("SELECT * FROM pemeriksaan_image WHERE pemeriksaan_id = $pemeriksaan_id ORDER BY id")->result();
        $data['form'] = unserialize($data['pemeriksaan']['form']);
        $data['tht_result'] = unserialize($data['pemeriksaan']['meta']);
        $data['hd'] = unserialize($data['pemeriksaan']['hd']);

        $data['jaminan'] = $this->config->item('pendaftaran');
        $data['obat_periksa'] = $this->DetailObatPemeriksaanModel->getDetailObatByPemeriksaanId($pemeriksaan_id)->result();
        $data['obat_racikan_periksa'] = $this->DetailObatRacikanPemeriksaanModel->getDetailObatRacikanByPemeriksaanId($pemeriksaan_id)->result();
        foreach ($data['obat_racikan_periksa'] as $k => $v) {
            $data['obat_racikan_periksa'][$k]->obat = $this->AdministrasiModel->getListObatByDetailObatRacikanPemeriksaanId($v->id)->result();
        }
        $racikan_luar = $this->DetailObatRacikanPemeriksaanModel->getDetailObatRacikanLuarByPemeriksaanId($pemeriksaan_id)->result();
        foreach ($racikan_luar as $k => $v) {
            $racikan_luar[$k]->obat = $this->AdministrasiModel->getListObatByDetailObatRacikanLuarPemeriksaanId($v->id)->result();
        }
        $data['obat_racikan_periksa'] = array_merge($data['obat_racikan_periksa'], $racikan_luar);

        $category = 'umum';
        foreach ($this->config->item('poli') as $k => $v) {
            if (in_array($data['pendaftaran']['kode_daftar'], $v['kode'])) {
                $category = $k;
                break;
            }
        }

        $data['listPerawat'] = $this->PerawatModel->listPerawat();
        $data['dokter'] = $this->PendaftaranModel->getDokter();
        $data['tindakan'] = $this->LaporanModel->getTindakanByIdPemeriksaan($pemeriksaan_id)->result();
        $data['tindakan_konsul'] = $this->LaporanModel->getTindakanKonsulByIdPemeriksaan($pemeriksaan_id)->result();
        $data['all_penyakit'] = $this->PemeriksaanModel->getPenyakitByCategory($category)->result();
        $data['penyakit'] = $this->LaporanModel->getPenyakitPemeriksaanByIdPemeriksaan($pemeriksaan_id)->result();

        if ($data['pendaftaran']['jenis_pendaftaran_id'] == 58)
            return [
                'view' => $this->load->view('laporan/rekam_medis/detail_riwayat_poli_igd', $data, true),
                'pemeriksaan_id' => $pemeriksaan_id,
                'penyakit' =>  $data['penyakit'],
                'pemeriksaan' =>  $data['pemeriksaan'],
                'form' =>  $data['form']
            ];
        else
            return [
                'view' => $this->load->view('laporan/rekam_medis/detail_riwayat_poli', $data, true),
                'pemeriksaan_id' => $pemeriksaan_id,
                'penyakit' =>  $data['penyakit'],
                'pemeriksaan' =>  $data['pemeriksaan'],
                'form' =>  $data['form']
            ];
    }

    private function getDetailBilling(&$data, $sudah_bayar = false)
    {
        $ri_id = $data['rawat_inap']->id;
        $transfer_id = $data['rawat_inap']->transfer_id;

        $data['form'] = json_decode($data['rawat_inap']->form, true);
        $data['bayar'] = $sudah_bayar ? $this->RawatInapModel->getBayarByRawatInapId($ri_id) : '';
        $data['pemeriksaan'] = $this->RawatInapModel->getPemeriksaanByRawatInapId($ri_id);
        foreach ($data['pemeriksaan'] as &$d) {
            $d->penyakit = $this->RawatInapModel->getPenyakitByDiagnosisId($d->ri_diagnosis_id);
            $d->obat = $this->RawatInapModel->getObatByResepId($d->ri_resep_id);
            $d->obat_racik = $this->RawatInapModel->getObatRacikByResepId($d->ri_resep_id);
            foreach ($d->obat_racik as &$dd) {
                $dd->detail = $this->RawatInapModel->getObatRacikDetailByResepObatRacikId($dd->id);
                $dd->total = 0;
                foreach ($dd->detail as $ddd) {
                    $dd->total += $ddd->jumlah * $ddd->harga_jual;
                }
            }
            $d->tindakan = $this->RawatInapModel->getTindakanByBiayaId($d->ri_biaya_id);
        }
        $data['diagnosis'] = $this->RawatInapModel->getDiagnosisByRawatInapId($ri_id);
        foreach ($data['diagnosis'] as &$d) {
            $d->penyakit = $this->RawatInapModel->getPenyakitByDiagnosisId($d->id);
        }

        $data['resep'] = $this->RawatInapModel->getResepByRawatInapId($ri_id);
        foreach ($data['resep'] as &$d) {
            $d->obat = $this->RawatInapModel->getObatByResepId($d->id);
            $d->obat_racik = $this->RawatInapModel->getObatRacikByResepId($d->id);
            foreach ($d->obat_racik as &$dd) {
                $dd->detail = $this->RawatInapModel->getObatRacikDetailByResepObatRacikId($dd->id);
                $dd->total = 0;
                foreach ($dd->detail as $ddd) {
                    $dd->total += $ddd->jumlah * $ddd->harga_jual;
                }
            }
        }

        $data['bahan_habis_pakai'] = $this->RawatInapModel->getBhpByRawatInapId($ri_id);
        foreach ($data['bahan_habis_pakai'] as &$d) {
            $d->bahan = $this->RawatInapModel->getBahanByBhpId($d->id);
        }

        $data['biaya'] = $this->RawatInapModel->getBiayaByRawatInapId($ri_id);
        foreach ($data['biaya'] as &$d) {
            if ($d->jenis_biaya == 'Makanan') {
                $d->makanan = $this->RawatInapModel->getMakananById($d->makanan_id);
            }
            else if ($d->jenis_biaya == 'Tindakan') {
                $d->tindakan = $this->RawatInapModel->getTindakanByBiayaId($d->id);
            }
        }
        $data['makanan_count_list'] = $this->RawatInapModel->getMakananCountByRawatInapId($ri_id);

        // TODO ini hanya berlaku jika blm ada transfer ke selain bangsal; di bayar juga ya; dan di print
        $tfs = $this->RawatInapModel->getAllBangsalTransfersByRawatInapId($ri_id);
        $bed = [];
        foreach ($tfs as $k => $v) {
            $end = $sudah_bayar ? $v->tgl_boleh_pulang : 'now';
            $bed[] = [
                'start' => $v->transfered_at,
                'end' => $k == count($tfs) - 1 ? $end : $tfs[$k + 1]->transfered_at,
                'bed' => $this->bed_model->bed_listsearch($v->tujuan)
            ];
        }
        $data['beds'] = $bed;
//        $tf = $this->RawatInapModel->getLastTransferByRawatInapId($ri_id);
//        $bed[] = [
//            'start' => $tf->created_at,
//            'end' => 'now',
//            'bed' => $this->bed_model->bed_listsearch($data['rawat_inap']->tujuan),
//        ];
//
//        while ($tf->prev_transfer_id != null) {
//            $end = $tf->created_at;
//            $tujuan = $tf->tujuan;
//            $cur = $tf->current_place_type;
//
//            $tf = $this->RawatInapModel->getTransferById($tf->prev_transfer_id);
//
//            if ($cur == 'bangsal') {
//                $bed[] = [
//                    'start' => $tf->created_at,
//                    'end' => $end,
//                    'bed' => $this->bed_model->bed_listsearch($tujuan),
//                ];
//            }
//        }
//        $data['beds'] = array_reverse($bed);

        $data['all_tindakan'] = $this->LaporanModel->getTindakanById($data['pasien']->id);
        if ($data['rawat_inap']->pendaftaran_id) {
            $data['lab'] = $this->PendaftaranModel->getPemeriksaanByPendaftaranIdAndJenisPendaftaranId($data['rawat_inap']->pendaftaran_id, 19, 'desc');
            foreach ($data['lab'] as &$datum) {
                $datum->layanan = $this->JenisLayananLaboratoirumModel->byIdPemeriksaan($datum->id);
            }
            $data['radio'] = $this->PendaftaranModel->getPemeriksaanByPendaftaranIdAndJenisPendaftaranId($data['rawat_inap']->pendaftaran_id, 59, 'desc');
        }
        else {
            $data['lab'] = [];
            $data['radio'] = [];
        }
    }

    public function detail($id, $tab = 'pemeriksaan')
    {
        $data['rawat_inap'] = $this->RawatInapModel->getById($id);
        $data['pasien'] = $this->RawatInapModel->getPasienById($data['rawat_inap']->pasien_id);
        $data['dokter'] = $this->UserModel->getDokter();
        $data['perawat'] = $this->UserModel->getPerawat();
        $data['apoteker'] = $this->UserModel->getApoteker();
        $data['gizi'] = $this->UserModel->getGizi();
        $data['penyakit'] = $this->PenyakitModel->getListRawatInap(['igd'])->result();
        $data['penyakit_icd9'] = $this->PenyakitModel->getListICD9()->result();
        $data['tindakan'] = $this->TarifTindakanModel->getListRawatInap()->result();
        $data['listPerawat'] = $this->PerawatModel->listPerawat();
        $data['tipe_makanan'] = $this->RawatInapModel->getAllTipeMakanan();
        $data['obat1'] = $this->db->get_where('obat', ['is_active' => 1, 'stok_obat >' => 0])->result();
        $data['bahan'] = $this->db->get_where('bahan_habis_pakai', ['is_active' => 1, 'jumlah >' => 0])->result();
        $data['tab'] = $tab;

        $pengkajian_awal = $this->pengkajian_awal($data['rawat_inap']);
        $data['pengkajian_awal'] = $pengkajian_awal['view'];
        $data['pemeriksaan_id'] = $pengkajian_awal['pemeriksaan_id'];
        $data['pengkajian_awal_pemeriksaan'] = $pengkajian_awal['pemeriksaan'];
        $data['form_pengkajian_awal'] = $pengkajian_awal['form'];
        $data['penyakit_pengkajian_awal'] = array_map(function($penyakit){
            return $penyakit->kode . ' - ' . $penyakit->nama;
        }, $pengkajian_awal['penyakit']);
        // echo json_encode($data['pemeriksaan']);
        
        $this->getDetailBilling($data);
        $this->template->view('rawat_inap/detail', $data);
    }

    public function test_detail($id, $tab = 'pemeriksaan')
    {
        $data['rawat_inap'] = $this->RawatInapModel->getById($id);
        $data['pasien'] = $this->RawatInapModel->getPasienById($data['rawat_inap']->pasien_id);
        $data['dokter'] = $this->PendaftaranModel->getDokter();
        $data['penyakit'] = $this->PenyakitModel->getListRawatInap()->result();
        $data['tindakan'] = $this->TarifTindakanModel->getListRawatInap()->result();
        $data['listPerawat'] = $this->PerawatModel->listPerawat();
        $data['tipe_makanan'] = $this->RawatInapModel->getAllTipeMakanan();
        $data['obat1'] = $this->db->get_where('obat', ['is_active' => 1, 'stok_obat >' => 0])->result();
        $data['bahan'] = $this->db->get_where('bahan_habis_pakai', ['is_active' => 1, 'jumlah >' => 0])->result();
        $data['tab'] = $tab;

        $pengkajian_awal = $this->pengkajian_awal($data['rawat_inap']);
        $data['pengkajian_awal'] = $pengkajian_awal['view'];
        $data['pemeriksaan_id'] = $pengkajian_awal['pemeriksaan_id'];

        $this->getDetailBilling($data);
        $this->template->view('rawat_inap/test_detail', $data);
    } 

    public function detailRekamMedis($id)
    {
        $data['rawat_inap'] = $this->RawatInapModel->getById($id);
        $data['pasien'] = $this->RawatInapModel->getPasienById($data['rawat_inap']->pasien_id);
        $data['dokter'] = $this->PendaftaranModel->getDokter();
        $data['penyakit'] = $this->PenyakitModel->getListRawatInap()->result();
        $data['tindakan'] = $this->TarifTindakanModel->getListRawatInap()->result();
        $data['listPerawat'] = $this->PerawatModel->listPerawat();
        $data['tipe_makanan'] = $this->RawatInapModel->getAllTipeMakanan();
        $data['obat1'] = $this->db->get_where('obat', ['is_active' => 1, 'stok_obat >' => 0])->result();
        $data['bahan'] = $this->db->get_where('bahan_habis_pakai', ['is_active' => 1, 'jumlah >' => 0])->result();

        $this->getDetailBilling($data);
        $this->template->view('rawat_inap/detail_rekam_medis', $data);
    }

    public function bayar($id, $sudah_bayar = false)
    {
        $data['sudah_bayar'] = $sudah_bayar;
        $data['rawat_inap'] = $this->RawatInapModel->getById($id);
        $data['pasien'] = $this->RawatInapModel->getPasienById($data['rawat_inap']->pasien_id);
        $data['dokter'] = $this->PendaftaranModel->getDokter();
        $data['penyakit'] = $this->RawatInapModel->getAllPenyakit();
        $data['tindakan'] = $this->RawatInapModel->getAllTindakan();
        $data['tipe_makanan'] = $this->RawatInapModel->getAllTipeMakanan();
        $data['obat1'] = $this->db->get_where('obat', ['is_active' => 1, 'stok_obat >' => 0])->result();
        $data['bahan'] = $this->db->get_where('bahan_habis_pakai', ['is_active' => 1, 'jumlah >' => 0])->result();

        $this->getDetailBilling($data, $sudah_bayar);
        $this->template->view('rawat_inap/bayar', $data);
    }

    public function submitBayar()
    {
        $ri_id = $this->input->post('ri_id');
        $ri = $this->RawatInapModel->getById($ri_id);
        $transfer_id = $ri->transfer_id;
        $bayar = array(
            'rawat_inap_id' => $ri_id,
            'transfer_id' => $transfer_id,
            'jenis_ruangan_id' => 1,
            'jasa_racik' => $this->input->post('jasa_racik'),
            'diskon' => $this->input->post('diskon'),
            'total' => $this->input->post('total_input'),
            'bayar' => $this->input->post('bayar'),
            'kembalian' => $this->input->post('kembalian_input'),
        );
        $id_bayar = $this->MainModel->insert_id('ri_bayar', $bayar);

        //tindakan & makanan
        $data['biaya'] = $this->RawatInapModel->getBiayaByRawatInapId($ri_id);
        foreach ($data['biaya'] as &$d) {
            if ($d->jenis_biaya == 'Makanan') {
                $makanan = $this->RawatInapModel->getMakananById($d->makanan_id);
                $byr = array(
                    'ri_bayar_id' => $id_bayar,
                    'item' => $makanan->nama,
                    'jenis_item' => 'makanan',
                    'jumlah' => 1,
                    'harga' => $makanan->harga,
                    'subtotal' => $makanan->harga,
                );
                $this->MainModel->insert('ri_bayar_detail', $byr);
            }
            else if ($d->jenis_biaya == 'Tindakan') {
                $tindakan = $this->RawatInapModel->getTindakanByBiayaId($d->id);
                foreach ($tindakan as $t) {
                    $byr = array(
                        'ri_bayar_id' => $id_bayar,
                        'item' => $t->nama,
                        'jenis_item' => 'tindakan',
                        'jumlah' => 1,
                        'harga' => $t->tarif_pasien,
                        'subtotal' => $t->tarif_pasien,
                    );
                    $this->MainModel->insert('ri_bayar_detail', $byr);
                }
            }
        }

        //obat & obat racik
        $data['resep'] = $this->RawatInapModel->getResepByRawatInapId($ri_id);
        foreach ($data['resep'] as &$d) {
            $obat = $this->RawatInapModel->getObatByResepId($d->id);
            foreach ($obat as $o) {
                $byr = array(
                    'ri_bayar_id' => $id_bayar,
                    'item' => $o->nama,
                    'jenis_item' => 'obat',
                    'jumlah' => $o->jumlah,
                    'harga' => $o->harga_jual,
                    'subtotal' => $o->harga_jual * $o->jumlah,
                );
                $this->MainModel->insert('ri_bayar_detail', $byr);
            }

            $d->obat_racik = $this->RawatInapModel->getObatRacikByResepId($d->id);
            foreach ($d->obat_racik as &$dd) {
                $dd->detail = $this->RawatInapModel->getObatRacikDetailByResepObatRacikId($dd->id);
                $dd->total = 0;
                foreach ($dd->detail as $ddd) {
                    $dd->total += $ddd->jumlah * $ddd->harga_jual;
                }

                $byr = array(
                    'ri_bayar_id' => $id_bayar,
                    'item' => $dd->nama_racikan,
                    'jenis_item' => 'obat racikan',
                    'jumlah' => 1,
                    'harga' => '',
                    'subtotal' => $dd->total
                );
                $this->MainModel->insert('ri_bayar_detail', $byr);
            }
        }

        // bhp
        $data['bahan_habis_pakai'] = $this->RawatInapModel->getBhpByRawatInapId($ri_id);
        foreach ($data['bahan_habis_pakai'] as &$d) {
            $d->bahan = $this->RawatInapModel->getBahanByBhpId($d->id);
            foreach ($d->bahan as $t) {
                $byr = array(
                    'ri_bayar_id' => $id_bayar,
                    'item' => $t->nama,
                    'jenis_item' => 'bahan habis pakai',
                    'jumlah' => $t->jumlah,
                    'harga' => $t->harga_jual,
                    'subtotal' => $t->harga_jual * $t->jumlah,
                );
                $this->MainModel->insert('ri_bayar_detail', $byr);
            }
        }

        // jasa racik
        if ($this->input->post('jasa_racik')) {
            $jasa_racik = array(
                'ri_bayar_id' => $id_bayar,
                'item' => 'Jasa Racik',
                'jenis_item' => 'jasa racik',
                'jumlah' => 0,
                'harga' => $this->input->post('jasa_racik'),
                'subtotal' => $this->input->post('jasa_racik'),
            );
            $this->MainModel->insert('ri_bayar_detail', $jasa_racik);
        }

        // bed
        $bed = array(
            'ri_bayar_id' => $id_bayar,
            'item' => 'Bed',
            'jenis_item' => 'bed',
            'jumlah' => 0,
            'harga' => $this->input->post('biaya_bed'),
            'subtotal' => $this->input->post('biaya_bed'),
        );
        $this->MainModel->insert('ri_bayar_detail', $bed);

        $this->MainModel->update('rawat_inap', ['status' => 'Selesai', 'tgl_selesai' => date('Y-m-d H:i:s')], $ri_id);
        $this->MainModel->update('transfer', ['status' => 'selesai', 'selesai_at' => date('Y-m-d H:i:s')], $transfer_id);
        $this->MainModel->update('bed', ['is_active' => 'yes'], $ri->tujuan);

        redirect('RawatInap/bayar/'.$ri_id.'/'.'true');
    }

    public function tambah($what)
    {
        $transfer_id = $this->RawatInapModel->getById($this->input->post('rawat_inap_id'))->transfer_id;
        if ($what == 'pemeriksaan') {
            $getUserById = function ($id) {
                if (!$id) {
                    return null;
                }

                $d = $this->db->query("SELECT * FROM user WHERE id = $id")->row();
                return [
                    'id' => $d->id,
                    'name' => $d->nama,
                ];
            };

            $pemeriksaan = [
                'rawat_inap_id' => $this->input->post('rawat_inap_id'),
                'creator_type' => $this->input->post('creator_type'),
                'transfer_id' => $transfer_id,
                'jenis_ruangan_id' => $this->jenis_ruangan_id,
                'pasien_id' => $this->input->post('pasien_id'),
                'dokter_id' => $this->input->post('dokter_id'),
                'shift' => $this->input->post('shift'),
                'nakes' => json_encode([
                    'p' => array_map(function ($v) use ($getUserById) { return $getUserById($v); }, $this->input->post('perawat') ?: []),
                    'pt' => array_map(function ($v) use ($getUserById) { return $getUserById($v); }, $this->input->post('perawat_tamu') ?: []),
                    'd' => $getUserById($this->input->post('dokter_id')),
                    'a' => $getUserById($this->input->post('apoteker_id')),
                    'g' => $getUserById($this->input->post('gizi_id')),
                ]),
                'subjektif' => $this->input->post('subjektif'),
                'subjectives' => json_encode([
                    'p' => $this->input->post('subjektif'),
                    'd' => $this->input->post('subjektif_d'),
                    'a' => $this->input->post('subjektif_a'),
                    'g' => $this->input->post('subjektif_g'),
                ]),
                'objektif' => serialize($this->input->post('objektif')),
                'objectives' => json_encode([
                    'p' => $this->input->post('objektif_p'),
                    'd' => $this->input->post('objektif_d'),
                    'a' => $this->input->post('objektif_a'),
                    'g' => $this->input->post('objektif_g'),
                ]),
                'asesmen' => $this->input->post('asesmen'),
                'assessments' => json_encode([
                    'p' => $this->input->post('asesmen'),
                    'd' => $this->input->post('asesmen_d'),
                    'a' => $this->input->post('asesmen_a'),
                    'g' => $this->input->post('asesmen_g'),
                ]),
                'plan' => $this->input->post('plan'),
                'plans' => json_encode([
                    'p' => $this->input->post('plan'),
                    'd' => $this->input->post('plan_d'),
                    'a' => $this->input->post('plan_a'),
                    'g' => $this->input->post('plan_g'),
                ]),
            ];
            $id_pemeriksaan = $this->MainModel->insert_id('ri_pemeriksaan', $pemeriksaan);

            if ($this->input->post('penyakit') && count($this->input->post('penyakit'))) {
                $d = [
                    'ri_pemeriksaan_id' => $id_pemeriksaan,
                    'rawat_inap_id' => $this->input->post('rawat_inap_id'),
                    'transfer_id' => $transfer_id,
                    'jenis_ruangan_id' => $this->jenis_ruangan_id,
                    'pasien_id' => $this->input->post('pasien_id'),
                    'diagnosis' => $this->input->post('diagnosis')
                ];
                $id = $this->MainModel->insert_id('ri_diagnosis', $d);

                foreach ($this->input->post('penyakit') as $p) {
                    $penyakit = [
                        'ri_diagnosis_id' => $id,
                        'penyakit_id' => $p
                    ];
                    $this->MainModel->insert('ri_diagnosis_penyakit', $penyakit);
                }
            }

            // tindakan
            $tindakan_id = $this->input->post('tindakan') ?: [];
            $tindakan_anestesi_id = $this->input->post('tindakan_anestesi') ?: [];
            $tindakan_tamu_id = $this->input->post('tindakan_tamu') ?: [];

            $dokter_anestesi = $this->input->post('dokter_anestesi');
            $dokter_tamu = $this->input->post('dokter_tamu') ?: [];
            $perawat = $this->input->post('perawat') ?: [];
            $perawat_tamu = $this->input->post('perawat_tamu') ?: [];

            $ids = array_merge($tindakan_id, $tindakan_anestesi_id, $tindakan_tamu_id);
            $harga = count($ids) ? $this->RawatInapModel->getTindakanHargaByIds($ids) : [];

            $total_harga = 0;
            foreach ($harga as $h) {
                $total_harga += $h->tarif_pasien;
            }

            $biaya = [
                'ri_pemeriksaan_id' => $id_pemeriksaan,
                'rawat_inap_id' => $this->input->post('rawat_inap_id'),
                'transfer_id' => $transfer_id,
                'jenis_ruangan_id' => $this->jenis_ruangan_id,
                'jenis_biaya' => 'Tindakan',
                'biaya' => $total_harga,
            ];
            $biaya_id = $this->MainModel->insert_id('ri_biaya', $biaya);

            foreach ($tindakan_id as $id) {
                $tarif_perawat = $this->PemeriksaanModel->getTarifTindakanById($id)->tarif_perawat;
                $tarif_per_perawat = $tarif_perawat / count($perawat);
                $this->MainModel->insert('ri_biaya_tindakan', [
                    'ri_biaya_id' => $biaya_id,
                    'tindakan_id' => $id,
                    'jumlah_perawat' => count($perawat),
                    'perawat' => implode(',', $perawat) ?? '',
                    'tarif_per_perawat' => is_nan($tarif_per_perawat) || is_infinite($tarif_per_perawat) ? 0 : $tarif_per_perawat,
                    'tipe_tindakan' => 'asli',
                    'perawat_tamu' => implode(',', $perawat_tamu) ?? ''
                ]);
            }

            foreach ($tindakan_anestesi_id as $id) {
                $this->MainModel->insert('ri_biaya_tindakan', [
                    'ri_biaya_id' => $biaya_id,
                    'tindakan_id' => $id,
                    'dokter_anestesi' => $dokter_anestesi,
                    'tipe_tindakan' => 'anestesi',
                    'perawat_tamu' => implode(',', $perawat_tamu) ?? ''
                ]);
            }

            foreach ($tindakan_tamu_id as $id) {
                $tarif_dokter = $this->PemeriksaanModel->getTarifTindakanById($id)->tarif_dokter;
                $tarif_per_dokter_tamu = $tarif_dokter / count($dokter_tamu);
                $this->MainModel->insert('ri_biaya_tindakan', [
                    'ri_biaya_id' => $biaya_id,
                    'tindakan_id' => $id,
                    'jumlah_dokter_tamu' => count($dokter_tamu),
                    'dokter_tamu' => implode(',', $dokter_tamu) ?? '',
                    'tarif_per_dokter_tamu' => is_nan($tarif_per_dokter_tamu) || is_infinite($tarif_per_dokter_tamu) ? 0 : $tarif_per_dokter_tamu,
                    'tipe_tindakan' => 'tamu',
                    'perawat_tamu' => implode(',', $perawat_tamu) ?? ''
                ]);
            }
            // end tindakan

            // konsul
            $session = $this->session->userdata('logged_in');
            $input_perawat = $this->input->post('perawat');
            for ($i = 0; $i <= $this->input->post('max_konsul_pemeriksaan_test'); $i++) {
                $dokter_id = $this->input->post('dokter_' . $i);
                if ($dokter_id) {
                    foreach ($this->input->post('tindakan_' . $i) as $key => $value) {
                        $tarif_perawat = $this->PemeriksaanModel->getTarifTindakanById($value)->tarif_perawat;
                        $tindakan = array(
                            'ri_pemeriksaan_id' => $id_pemeriksaan,
                            'tarif_tindakan_id' => $value,
                            'dokter_id' => $dokter_id,
                            'jumlah_perawat' => count($input_perawat),
                            'perawat' => implode(',', $input_perawat),
                            'tarif_per_perawat' => count($input_perawat) > 0 ? $tarif_perawat / count($input_perawat): 0,
                            'creator' => $session->id
                        );
                        $this->MainModel->insert_id('ri_detail_tindakan_konsul', $tindakan);
                    }
                }
            }   

            if (isset($_POST['resep']) && $_POST['resep']) {
                $id = $this->input->post('rawat_inap_id') . '_' . $id_pemeriksaan;
                redirect('apotek/input_resep_ranap/ranap/'.$id, 'refresh');
            }
        }
        else if ($what == 'diagnosis') {
            $d = [
                'rawat_inap_id' => $this->input->post('rawat_inap_id'),
                'transfer_id' => $transfer_id,
                'jenis_ruangan_id' => $this->jenis_ruangan_id,
                'pasien_id' => $this->input->post('pasien_id'),
                'diagnosis' => $this->input->post('diagnosis')
            ];
            $id = $this->MainModel->insert_id('ri_diagnosis', $d);

            foreach ($this->input->post('penyakit') as $p) {
                $penyakit = [
                    'ri_diagnosis_id' => $id,
                    'penyakit_id' => $p
                ];
                $this->MainModel->insert('ri_diagnosis_penyakit', $penyakit);
            }
        }
        else if ($what == 'resep') {
            $ri_resep_id = $this->MainModel->insert_id('ri_resep', [
                'rawat_inap_id' => $this->input->post('rawat_inap_id'),
                'transfer_id' => $transfer_id,
                'jenis_ruangan_id' => $this->jenis_ruangan_id,
            ]);
            $jumlah_satuan = $this->input->post('jumlah_satuan');
            $signa_obat = $this->input->post('signa_obat');
            $input_obat = $this->input->post('nama_obat');
            $i = 0;
            foreach ($input_obat as $key => $value) {
                if ($value != "") {
                    $obat = array(
                        'ri_resep_id' => $ri_resep_id,
                        'obat_id' => $value,
                        'jumlah' => $jumlah_satuan[$i],
                        'signa' => $signa_obat[$i],
                    );
                    $this->MainModel->insert_id('ri_resep_obat', $obat);
                    $obat = $this->ObatModel->getObatById($value)->row();
                    $stok = array('stok_obat' => ($obat->stok_obat) - $jumlah_satuan[$i]);
                    $this->MainModel->update('obat', $stok, $value);
                }
                $i++;
            }

            for ($n = 1; $n <= 9; $n++) {
                $signa = $this->input->post('signa' . $n);
                $catatan = $this->input->post('catatan' . $n);
                $racikan = array(
                    'ri_resep_id' => $ri_resep_id,
                    'nama_racikan' => 'racikan ' . $n,
                    'signa' => $signa ?? '-',
                    'catatan' => $catatan ?? '-',
                );
                $jumlah_satuan = $this->input->post('jumlah_satuan_racikan' . $n);

                if ($signa != "") {
                    $j = 0;
                    $ri_resep_obat_racik_id = $this->MainModel->insert_id('ri_resep_obat_racik', $racikan);
                    $input_obat = $this->input->post('nama_obat_racikan' . $n);

                    foreach ($input_obat as $key => $value) {
                        if ($value != '') {
                            $obat_racikan = array(
                                'ri_resep_obat_racik_id' => $ri_resep_obat_racik_id,
                                'obat_id' => $value,
                                'jumlah' => $jumlah_satuan[$j],
                            );

                            $this->MainModel->insert('ri_resep_obat_racik_detail', $obat_racikan);
                            $obat = $this->ObatModel->getObatById($value)->row();
                            $stok = array('stok_obat' => ($obat->stok_obat) - $jumlah_satuan[$j]);
                            $this->MainModel->update('obat', $stok, $value);
                        }
                        $j++;
                    }
                }
            }
        }
        else if ($what == 'bahan_habis_pakai') {
            $ri_bhp_id = $this->MainModel->insert_id('ri_bahan_habis_pakai', [
                'rawat_inap_id' => $this->input->post('rawat_inap_id'),
                'transfer_id' => $transfer_id,
                'jenis_ruangan_id' => $this->jenis_ruangan_id,
            ]);
            $x = 0;
            $jumlah_bahan = $_POST['qty'];
            foreach ($_POST['id'] as $value) {
                $this->db->insert('ri_bahan_habis_pakai_detail', [
                    'ri_bahan_habis_pakai_id' => $ri_bhp_id,
                    'bahan_id' => $value,
                    'jumlah' => $jumlah_bahan[$x],
                ]);
                $bahan = $this->ObatModel->getBahanById($value)->row();
                $stok = array('jumlah' => ($bahan->jumlah) - $jumlah_bahan[$x]);
                $this->MainModel->update('bahan_habis_pakai', $stok, $value);
                $x++;
            }
        }
        else if ($what == 'makanan') {
            $biaya = [
                'rawat_inap_id' => $this->input->post('rawat_inap_id'),
                'transfer_id' => $transfer_id,
                'jenis_ruangan_id' => $this->jenis_ruangan_id,
                'jenis_biaya' => 'Makanan',
                'makanan_id' => $this->input->post('makanan'),
                'biaya' => $this->input->post('harga_makanan'),
            ];
            $this->MainModel->insert('ri_biaya', $biaya);
            $what = 'assesment_gizi';
        }
        else if ($what == 'biaya') {
            $jenis_biaya = $this->input->post('jenis_biaya');
            if ($jenis_biaya == '#biaya_1') {
                $biaya = [
                    'rawat_inap_id' => $this->input->post('rawat_inap_id'),
                    'transfer_id' => $transfer_id,
                    'jenis_ruangan_id' => $this->jenis_ruangan_id,
                    'jenis_biaya' => 'Biaya Kamar',
                    'biaya' => $this->input->post('harga_bed')
                ];
                $this->MainModel->insert('ri_biaya', $biaya);
            }
            else if ($jenis_biaya == '#biaya_2') {
                $biaya = [
                    'rawat_inap_id' => $this->input->post('rawat_inap_id'),
                    'transfer_id' => $transfer_id,
                    'jenis_ruangan_id' => $this->jenis_ruangan_id,
                    'jenis_biaya' => 'Makanan',
                    'makanan_id' => $this->input->post('makanan'),
                    'biaya' => $this->input->post('harga_makanan'),
                ];
                $this->MainModel->insert('ri_biaya', $biaya);
            }
            else if ($jenis_biaya == '#biaya_3') {
                $tindakan_id = $this->input->post('tindakan') ?: [];
                $tindakan_anestesi_id = $this->input->post('tindakan_anestesi') ?: [];
                $tindakan_tamu_id = $this->input->post('tindakan_tamu') ?: [];

                $dokter_anestesi = $this->input->post('dokter_anestesi');
                $dokter_tamu = $this->input->post('dokter_tamu') ?: [];
                $perawat = $this->input->post('perawat') ?: [];
                $perawat_tamu = $this->input->post('perawat_tamu') ?: [];

                $harga = $this->RawatInapModel->getTindakanHargaByIds(array_merge($tindakan_id, $tindakan_anestesi_id, $tindakan_tamu_id));

                $total_harga = 0;
                foreach ($harga as $h) {
                    $total_harga += $h->tarif_pasien;
                }

                $biaya = [
                    'rawat_inap_id' => $this->input->post('rawat_inap_id'),
                    'transfer_id' => $transfer_id,
                    'jenis_ruangan_id' => $this->jenis_ruangan_id,
                    'jenis_biaya' => 'Tindakan',
                    'biaya' => $total_harga,
                ];
                $biaya_id = $this->MainModel->insert_id('ri_biaya', $biaya);

                foreach ($tindakan_id as $id) {
                    $tarif_perawat = $this->PemeriksaanModel->getTarifTindakanById($id)->tarif_perawat;
                    $tarif_per_perawat = $tarif_perawat / count($perawat);
                    $this->MainModel->insert('ri_biaya_tindakan', [
                        'ri_biaya_id' => $biaya_id,
                        'tindakan_id' => $id,
                        'jumlah_perawat' => count($perawat),
                        'perawat' => implode(',', $perawat) ?? '',
                        'tarif_per_perawat' => is_nan($tarif_per_perawat) || is_infinite($tarif_per_perawat) ? 0 : $tarif_per_perawat,
                        'tipe_tindakan' => 'asli',
                        'perawat_tamu' => implode(',', $perawat_tamu) ?? ''
                    ]);
                }

                foreach ($tindakan_anestesi_id as $id) {
                    $this->MainModel->insert('ri_biaya_tindakan', [
                        'ri_biaya_id' => $biaya_id,
                        'tindakan_id' => $id,
                        'dokter_anestesi' => $dokter_anestesi,
                        'tipe_tindakan' => 'anestesi',
                        'perawat_tamu' => implode(',', $perawat_tamu) ?? ''
                    ]);
                }

                foreach ($tindakan_tamu_id as $id) {
                    $tarif_dokter = $this->PemeriksaanModel->getTarifTindakanById($id)->tarif_dokter;
                    $tarif_per_dokter_tamu = $tarif_dokter / count($dokter_tamu);
                    $this->MainModel->insert('ri_biaya_tindakan', [
                        'ri_biaya_id' => $biaya_id,
                        'tindakan_id' => $id,
                        'jumlah_dokter_tamu' => count($dokter_tamu),
                        'dokter_tamu' => implode(',', $dokter_tamu) ?? '',
                        'tarif_per_dokter_tamu' => is_nan($tarif_per_dokter_tamu) || is_infinite($tarif_per_dokter_tamu) ? 0 : $tarif_per_dokter_tamu,
                        'tipe_tindakan' => 'tamu',
                        'perawat_tamu' => implode(',', $perawat_tamu) ?? ''
                    ]);
                }
            }
        }

        redirect('RawatInap/detail/'.$this->input->post('rawat_inap_id').'/'.$what);
    }

    public function hapus($what, $rawat_inap_id, $id)
    {
        if ($what == 'pemeriksaan') {
            $this->db->where('id', $id)->delete('ri_pemeriksaan');
            $this->db->where('ri_pemeriksaan_id', $id)->delete('ri_resep');
            $this->db->where('ri_pemeriksaan_id', $id)->delete('ri_diagnosis');
        }
        else if ($what == 'diagnosis') {
            $this->db->where('id', $id)->delete('ri_diagnosis');
            $this->db->where('ri_diagnosis_id', $id)->delete('ri_diagnosis_penyakit');
        }
        else if ($what == 'resep') {
            //
        }
        else if ($what == 'bahan_habis_pakai') {
            $ri_bahan_details = $this->db->where('ri_bahan_habis_pakai_id', $id)
                ->get('ri_bahan_habis_pakai_detail')
                ->result();

            foreach ($ri_bahan_details as $d) {
                $bahan = $this->ObatModel->getBahanById($d->bahan_id)->row();
                $stok = ['jumlah' => ($bahan->jumlah) + $d->jumlah];
                $this->MainModel->update('bahan_habis_pakai', $stok, $d->bahan_id);
            }

            $this->db->where('id', $id)->delete('ri_bahan_habis_pakai');
            $this->db->where('ri_bahan_habis_pakai_id', $id)->delete('ri_bahan_habis_pakai_detail');
        }
        else if ($what == 'biaya') {
            if ($this->db->where('id', $id)->get('ri_biaya')->row()->jenis_biaya == 'Tindakan') {
                $this->db->where('ri_biaya_id', $id)->delete('ri_biaya_tindakan');
            }
            $this->db->where('id', $id)->delete('ri_biaya');
        }
        else if ($what == 'makanan') {
            $what = 'assesment_gizi';
            $this->db->where('id', $id)->delete('ri_biaya');
        }

        redirect('RawatInap/detail/'.$rawat_inap_id.'/'.$what);
    }

    public function getMakananByTipeId($id)
    {
        echo json_encode($this->RawatInapModel->getMakananByTipeId($id));
    }

    public function bolehPulang($rawat_inap_id)
    {
        $ri = [
            'status' => 'Boleh Pulang',
            'total_biaya' => $this->input->post('total_biaya'),
            'tgl_boleh_pulang' => date('Y-m-d H:i:s')
        ];
        $this->MainModel->update('rawat_inap', $ri, $rawat_inap_id);
        $transfer_id = $this->RawatInapModel->getFirstBangsalTransferByRawatInapId($rawat_inap_id)->id;
        $this->MainModel->update('transfer', ['status' => 'diperiksa', 'diperiksa_at' => date('Y-m-d H:i:s')], $transfer_id);
        $this->session->set_flashdata('success', 'Pasien boleh pulang berhasil');
        redirect('RawatInap/listBolehPulang');
    }

    public function printPembayaran($bayar_id)
    {
        $id = $this->RawatInapModel->getBayarById($bayar_id)->rawat_inap_id;
        $data['rawat_inap'] = $this->RawatInapModel->getById($id);

        $transfer_id = $data['rawat_inap']->transfer_id;
        $data['klinik'] = $this->AdministrasiModel->getKlinik()->row();
        $data['bayar'] = $this->RawatInapModel->getBayarById($bayar_id);
        $data['sudah_bayar'] = true;
        $data['pasien'] = $this->RawatInapModel->getPasienById($data['rawat_inap']->pasien_id);
        $data['dokter'] = $this->PendaftaranModel->getDokter();
        $data['penyakit'] = $this->RawatInapModel->getAllPenyakit();
        $data['tindakan'] = $this->RawatInapModel->getAllTindakan();
        $data['tipe_makanan'] = $this->RawatInapModel->getAllTipeMakanan();
        $data['obat1'] = $this->db->get_where('obat', ['is_active' => 1, 'stok_obat >' => 0])->result();
        $data['bahan'] = $this->db->get_where('bahan_habis_pakai', ['is_active' => 1, 'jumlah >' => 0])->result();

        $this->getDetailBilling($data, true);
        $this->load->view('rawat_inap/nota_bayar', $data);
    }

    public function save_form($jenis, $tab)
    {
        $rawat_inap_id = $this->input->post('rawat_inap_id');
        $ri = $this->db->query("SELECT * FROM rawat_inap WHERE id = $rawat_inap_id")->row();
        $form = json_decode($ri->form ?? '{}', true);

        if ($jenis == 'edukasi' || $jenis == 'observasi' || $jenis == 'ppt' || $jenis == 'surveilans' || $jenis == 'alergi' || $jenis == 'resume_medis') {
            if($jenis == 'resume_medis' && gettype(json_decode(json_encode($form[$jenis]))) == 'object') {
                $oldJenis = $form[$jenis];
                $form[$jenis] = [];
                $form[$jenis][] = $oldJenis;
                $form[$jenis][] = $this->input->post('form')[$jenis];
            } else {
                $form[$jenis] []= $this->input->post('form')[$jenis];
            }
        }
        else {
            $form[$jenis] = $this->input->post('form')[$jenis];
        }
        $this->MainModel->update('rawat_inap', ['form' => json_encode($form)], $rawat_inap_id);

        redirect('RawatInap/detail/'.$this->input->post('rawat_inap_id').'/'.$tab);
    }

    public function resume_medis_list($rawatInapId) {
        $data['ri'] = $this->db->query("SELECT * FROM rawat_inap WHERE id = $rawatInapId")->row();        
        $data['form'] = json_decode($data['ri']->form, true);
        $resumeMedis = isset($data['form']['resume_medis']) ? $data['form']['resume_medis'] : null;

        $resumeMedises = [];
        if(!is_null($resumeMedis)) {
            if(gettype(json_decode(json_encode($resumeMedis))) == 'object') {
                $resumeMedises[] = $resumeMedis;
            } else {
                $resumeMedises = $resumeMedis;
            }
        }

        $rawatInap = $data['ri'];

        $this->template->view('rawat_inap/resume_medis_list', compact('resumeMedises', 'rawatInap'));
    }    
    
    public function pemeriksaan_list($rawatInapId) {
        $rawat_inap = $this->RawatInapModel->getById($rawatInapId);
        $pemeriksaan = $this->RawatInapModel->getPemeriksaanByRawatInapId($rawatInapId);
        foreach ($pemeriksaan as &$d) {
            $d->penyakit = $this->RawatInapModel->getPenyakitByDiagnosisId($d->ri_diagnosis_id);
            $d->obat = $this->RawatInapModel->getObatByResepId($d->ri_resep_id);
            $d->obat_racik = $this->RawatInapModel->getObatRacikByResepId($d->ri_resep_id);
            foreach ($d->obat_racik as &$dd) {
                $dd->detail = $this->RawatInapModel->getObatRacikDetailByResepObatRacikId($dd->id);
                $dd->total = 0;
                foreach ($dd->detail as $ddd) {
                    $dd->total += $ddd->jumlah * $ddd->harga_jual;
                }
            }
            $d->tindakan = $this->RawatInapModel->getTindakanByBiayaId($d->ri_biaya_id);
        }

        $this->template->view('rawat_inap/pemeriksaan_list', compact('pemeriksaan', 'rawat_inap'));
    } 

    public function pemeriksaan($rawatInapId, $pemeriksaanId) {
        $data['rawat_inap'] = $this->RawatInapModel->getById($rawatInapId);
        $data['pasien'] = $this->RawatInapModel->getPasienById($data['rawat_inap']->pasien_id);
        $data['dokter'] = $this->UserModel->getDokter();
        $data['perawat'] = $this->UserModel->getPerawat();
        $data['apoteker'] = $this->UserModel->getApoteker();
        $data['gizi'] = $this->UserModel->getGizi();
        $data['penyakit'] = $this->PenyakitModel->getListRawatInap(['igd'])->result();
        $data['penyakit_icd9'] = $this->PenyakitModel->getListICD9()->result();
        $data['tindakan'] = $this->TarifTindakanModel->getListRawatInap()->result();
        $data['listPerawat'] = $this->PerawatModel->listPerawat();
        $data['tipe_makanan'] = $this->RawatInapModel->getAllTipeMakanan();
        $data['obat1'] = $this->db->get_where('obat', ['is_active' => 1, 'stok_obat >' => 0])->result();
        $data['bahan'] = $this->db->get_where('bahan_habis_pakai', ['is_active' => 1, 'jumlah >' => 0])->result();
        $data['pemeriksaan'] = $this->RawatInapModel->getPemeriksaanById($pemeriksaanId);
        $data['ri_diagnosis_penyakit'] = $this->RawatInapModel->getDiagnosisByPemeriksaanId($pemeriksaanId);
        $data['ri_biaya'] = null;
        $data['ri_tindakan'] = [];        
        $data['ri_biaya_tindakan'] = [];
        $data['dokter_anestesi_id'] = null;
        $data['ri_detail_tindakan_konsul'] = [];
        if(count($this->RawatInapModel->getBiayaByPemeriksaanId($pemeriksaanId)) > 0){
            $data['ri_biaya'] = $this->RawatInapModel->getBiayaByPemeriksaanId($pemeriksaanId)[0];
            $data['ri_tindakan'] = $this->RawatInapModel->getTindakanByBiayaId($data['ri_biaya']->id, 'asli');
            $data['ri_tindakan_anestesi'] = $this->RawatInapModel->getTindakanByBiayaId($data['ri_biaya']->id, 'anestesi');
            if($data['ri_tindakan_anestesi']) {
                // echo json_encode($data['ri_tindakan_anestesi']);
                // die();
                $data['dokter_anestesi_id'] = $data['ri_tindakan_anestesi'][0]->dokter_anestesi;
            }            
            $data['ri_tindakan_dokter_tamu'] = $this->RawatInapModel->getTindakanByBiayaId($data['ri_biaya']->id, 'tamu');

            if($data['ri_tindakan_dokter_tamu']) {
                $data['dokter_tamu_ids'] = $data['ri_tindakan_dokter_tamu'][0]['dokter_tamu'];
            }            
        }
        $konsul = $this->RawatInapModel->getRawatInapPemeriksaanKonsul($pemeriksaanId);
        if(count($konsul) > 0) {
            $data['ri_detail_tindakan_konsul'] = $konsul; 
        }
        $this->template->view('rawat_inap/pemeriksaan', $data);
    }   

    public function resume_medis($rawatInapId, $index = 0) {
        $data['rawat_inap'] = $this->RawatInapModel->getById($rawatInapId);
        $data['pasien'] = $this->RawatInapModel->getPasienById($data['rawat_inap']->pasien_id);
        $data['dokter'] = $this->PendaftaranModel->getDokter();
        $data['penyakit'] = $this->PenyakitModel->getListRawatInap()->result();
        $data['tindakan'] = $this->TarifTindakanModel->getListRawatInap()->result();
        $data['listPerawat'] = $this->PerawatModel->listPerawat();
        $data['tipe_makanan'] = $this->RawatInapModel->getAllTipeMakanan();
        $data['obat1'] = $this->db->get_where('obat', ['is_active' => 1, 'stok_obat >' => 0])->result();
        $data['bahan'] = $this->db->get_where('bahan_habis_pakai', ['is_active' => 1, 'jumlah >' => 0])->result();

        $pengkajian_awal = $this->pengkajian_awal($data['rawat_inap']);
        $data['pengkajian_awal'] = $pengkajian_awal['view'];
        $data['pemeriksaan_id'] = $pengkajian_awal['pemeriksaan_id'];
        $data['form'] = json_decode($data['rawat_inap']->form, true);

        $this->getDetailBilling($data);
        $resumeMedis = null;
        if(isset($data['form']['resume_medis'])) {
            $resume = $data['form']['resume_medis'];
            if(gettype(json_decode(json_encode($resume))) == 'object') {
                $resumeMedis = $resume;
            }

            if(gettype(json_decode(json_encode($resume))) == 'array') {
                $resumeMedis = $resume[$index];
            }
            
            $data['form']['resume_medis'] = $resumeMedis;
        }

        $data['resume_medis_index'] = $index;

        // echo json_encode($data['form']['resume_medis']);
        $this->template->view('rawat_inap/resume_medis', $data);
    }

    public function save_form_resume_medis() 
    {        
        $rawatInapId = $this->input->post('rawat_inap_id');
        $resumeMedisIndex = $this->input->post('resume_medis_index');
        $tglKeluar = $this->input->post('tgl_keluar');
        $rawatInap = $this->RawatInapModel->getById($rawatInapId);
        
        $form = json_decode($rawatInap->form, true);
        $newForm = [];

        if(isset($form['resume_medis'])) {
            $resumeMedises = $form['resume_medis'];
            if(gettype(json_decode(json_encode($resumeMedises))) == 'array') {
                $i = 0;
                foreach($resumeMedises as $resumeMedis) {
                    if($i == $resumeMedisIndex) {
                        $updatedResumeMedis = $resumeMedis;
                        $updatedResumeMedis['tgl_keluar'] = $tglKeluar;
                        $newForm[] = $updatedResumeMedis;
                    } else {
                        $newForm[] = $resumeMedis;
                    }
                    $i++;
                }
                $form['resume_medis'] = $newForm; 
            }
            if(gettype(json_decode(json_encode($resumeMedises))) == 'object') {
                $resumeMedises['tgl_keluar'] = $tglKeluar;
                $newForm[] = $resumeMedises;
                
                $form['resume_medis'] = $newForm; 
            }
            $this->MainModel->update('rawat_inap', ['form' => json_encode($form)], $rawatInapId);
        }
        
        redirect('RawatInap/resume_medis/' . $rawatInapId . '/' . $resumeMedisIndex);
    }

    public function print_form($jenis, $rawat_inap_id, $index = 0)
    {
        $data['rawat_inap_id'] = $rawat_inap_id;
        $data['ri'] = $this->db->query("SELECT * FROM rawat_inap WHERE id = $rawat_inap_id")->row();
        $data['rawat_inap'] = $this->RawatInapModel->getById($rawat_inap_id);
        $data['form'] = json_decode($data['ri']->form, true);
        $data['pasien'] = $this->RawatInapModel->getPasienById($data['ri']->pasien_id);
        $data['klinik'] = $this->AdministrasiModel->getKlinik()->row();

        $data['pemeriksaan'] = $this->RawatInapModel->getPemeriksaanByRawatInapId($rawat_inap_id);
        foreach ($data['pemeriksaan'] as &$d) {
            $d->penyakit = $this->RawatInapModel->getPenyakitByDiagnosisId($d->ri_diagnosis_id);
            $d->obat = $this->RawatInapModel->getObatByResepId($d->ri_resep_id);
            $d->obat_racik = $this->RawatInapModel->getObatRacikByResepId($d->ri_resep_id);
            foreach ($d->obat_racik as &$dd) {
                $dd->detail = $this->RawatInapModel->getObatRacikDetailByResepObatRacikId($dd->id);
                $dd->total = 0;
                foreach ($dd->detail as $ddd) {
                    $dd->total += $ddd->jumlah * $ddd->harga_jual;
                }
            }
            $d->tindakan = $this->RawatInapModel->getTindakanByBiayaId($d->ri_biaya_id);
        }

        $resumeMedis = null;
        if($jenis == 'resume_medis') {
            if(isset($data['form']['resume_medis'])) {
                $resume = $data['form']['resume_medis'];
                if(gettype(json_decode(json_encode($resume))) == 'object') {
                    $resumeMedis = $resume;
                }

                if(gettype(json_decode(json_encode($resume))) == 'array') {
                    $resumeMedis = $resume[$index];
                }
                
                $data['form']['resume_medis'] = $resumeMedis;
            }
        }

        $this->load->view('rawat_inap/print/'.$jenis, $data);
    }
}
