<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PemeriksaanAwal extends MY_BpjsController {

    public function __construct() {
        parent::__construct();

        $this->load->library('template');
        $this->load->Model('PemeriksaanModel');
        $this->load->Model('MainModel');
        $this->load->Model('PasienModel');
        $this->load->Model('PerawatModel');
    }

    public function index() {
	    $u = $this->session->get_userdata('logged_in');
	    $u = $u['logged_in'];
	    $is_dokter = $u->nama_grup == 'dokter';
	    $is_perawat = $u->nama_grup == 'perawat';

	    if ($is_dokter || $is_perawat) {
		    $data['listPendaftaran'] = $this->PemeriksaanModel->getListPendaftaran_antriByIdJenisPendafataran($u->id_jenis_pendaftaran);
	    }
	    else {
		    $data['listPendaftaran'] = $this->PemeriksaanModel->getListPendaftaran_antri();
	    }
	    
	    $data['jenis_pendaftaran'] = $this->PemeriksaanModel->getJenisPendafataranAntrian();
	    foreach($data['jenis_pendaftaran'] as &$jp) {
	        $jp->list = $this->PemeriksaanModel->getListPendaftaran_antriByIdJenisPendafataran($jp->jenis_pendaftaran_id)->result();
	    }

        $data['jaminan'] =$this->config->item('pendaftaran');

        $this->template->view('pemeriksaan_awal/list',$data);
    }

    public function periksa($id) {
        $data['pendaftaran'] = $this->PemeriksaanModel->getPendaftaranById($id)->row_array();
        $data['jaminan'] =$this->config->item('pendaftaran');
        $data['listPerawat'] = $this->PerawatModel->listPerawat();

        $this->template->view('pemeriksaan_awal/periksa', $data);
    }

    public function simpanPeriksa($id) {
        $input_perawat = $this->input->post('perawat');
        $periksa = array(
            'pendaftaran_id'    => $this->input->post('pendaftaran_id'),
            'dokter_id'         => $this->input->post('dokter_id') ?? null,
            'pasien_id'         => $id,
            'perawat_id'        => $this->session->userdata('logged_in')->id,
            'detail_perawat_id' => implode(',', $input_perawat) == null ? '' : implode(',', $input_perawat),
            'no_rm'             => $this->input->post('no_rm'),
            'nama_pasien'       => $this->input->post('nama_pasien'),
            'keluhan_utama'     => $this->input->post('keluhan_utama'),
            'diagnosa_perawat'  => $this->input->post('diagnosa_perawat'),
            'amammesia'         => $this->input->post('amammesia'),
            'asuhan_keperawatan'=> $this->input->post('asuhan_keperawatan'),
            'bmi'               => $this->input->post('bmi'),
            'td'                => $this->input->post('td'),
            'r'                 => $this->input->post('r'),
            'bb'                => $this->input->post('bb'),
            'n'                 => $this->input->post('n'),
            's'                 => $this->input->post('s'),
            'tb'                => $this->input->post('tb'),
            'spo2'                => $this->input->post('spo2'),
            'is_bpjs'                => $this->input->post('is_bpjs'),
            'jaminan'                => $this->input->post('jaminan'),
            'status'            => 'sudah_periksa_awal',
            'sudah_periksa_perawat' => 1,
            'form' => serialize($this->input->post('form')),
            'creator'           => $this->session->userdata('logged_in')->id
        );

        if ($this->input->post('jenis_kia')) {
            $periksa['meta'] = serialize(['jenis' => $this->input->post('jenis_kia')]);
        }

        $this->MainModel->update('pendaftaran_pasien', array('status' =>'diperiksa'), $this->input->post('pendaftaran_id'));
        $pem_id = $this->MainModel->insert_id('pemeriksaan',$periksa);

        $pp_id = $this->input->post('pendaftaran_id');
        $a_id = $this->db->query("SELECT * FROM antrian WHERE pendaftaran_id = $pp_id")->row()->id;
        $this->MainModel->update('antrian', ['pemeriksaan_id' => $pem_id, 'is_called' => 1], $a_id);

        $this->session->set_flashdata('success', 'Pemeriksaan awal pasien berhasil!');
        redirect('PemeriksaanAwal/');
    }

    public function hapus($pendaftaran_id)
    {
        $this->MainModel->delete('pendaftaran_pasien', ['is_active' => 0], $pendaftaran_id);
        $this->db->where('pendaftaran_id', $pendaftaran_id)->update('pemeriksaan', ['is_active' => 0]);

        $this->session->set_flashdata('success', 'Berhasil menghapus pendaftaran');
        redirect('PemeriksaanAwal', 'refresh');
    }
}
