<?php

/**
 *
 */
class BahanHabisPakai extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->load->library('template');
        $this->load->Model('MainModel');
        $this->load->Model('GudangBahanModel');
    }

    public function stok() {
        if (isset($_POST['do_save']) && $_POST['do_save'] == 'abdush') {
            $session = $this->session->userdata('logged_in');

            if ($_POST['id'] == 0) {
                $bahan = array(
                    'nama' => $_POST['nama'],
                    'satuan' => $_POST['satuan'],
                    'creator' => $session->id
                );
                $data = $this->MainModel->insert($tabel = 'bahan_habis_pakai', $bahan);
            } else {
                $bahan = array(
                    'nama' => $_POST['nama'],
                    'satuan' => $_POST['satuan']
                );
                $data = $this->MainModel->update($tabel = 'bahan_habis_pakai', $bahan, $_POST['id']);
            }

            if ($data) {
                $this->session->set_flashdata('success', 'Tambah bahan habis pakai berhasil!');
            } else {
                $this->session->set_flashdata('warning', 'Tambah bahan habis pakai gagal!');
            }

            redirect('BahanHabisPakai/stok', 'refresh');
        } else {
            $data['bahan'] = $this->db->get_where('bahan_habis_pakai', ['is_active' => 1])->result();
            $this->template->view('master/habis_pakai/stok', $data);
        }
    }

    public function delete($id = false) {
        if ($id) {
            $data = $this->MainModel->delete($tabel = 'bahan_habis_pakai', ['is_active' => 0], $id);

            if ($data) {
                $this->session->set_flashdata('success', 'Hapus bahan habis pakai berhasil!');
            } else {
                $this->session->set_flashdata('warning', 'Hapus bahan habis pakai gagal!');
            }

            redirect('BahanHabisPakai/stok', 'refresh');
        } else {
            show_404();
        }
    }

    public function pembelian() {
        $data['pemebelian'] = $this->db->get_where('pembelian_bahan', ['is_active' => 1])->result();
        $this->template->view('master/habis_pakai/pembelian', $data);
    }

    public function settingPersenPembelian() {
        if ($this->input->post('submit') == 1) {
            $id = $this->input->post('id');
            $session = $this->session->userdata('logged_in');
            $persen = array(
                'prosentase' => $this->input->post('prosentase')

            );

            $update = $this->MainModel->update($tabel = 'prosentase_harga', $persen, $id);

            if ($update) {
                $this->session->set_flashdata('success', 'Edit persen harga obat berhasil!');
            } else {
                $this->session->set_flashdata('warning', 'Edit persen harga obat gagal!');
            }
            redirect('BahanHabisPakai/pembelian', 'refresh');
        } else {
            $data['persen'] = $this->db->get_where('prosentase_harga', ['id' => 2])->row();
            $this->template->view('master/habis_pakai/persen', $data);
        }
    }

    public function tambahPembelian() {
        if ($this->input->post('submit') == 1) {
            $session = $this->session->userdata('logged_in');
            $pembelian = array(
                'no_faktur' => $this->input->post('no_faktur'),
                'tgl_faktur' => $this->input->post('tgl_faktur'),
                'tgl_jatuh_tempo' => $this->input->post('tgl_jatuh_tempo'),
                'nama_distributor' => $this->input->post('nama_distributor'),
                'total' => $this->input->post('total'),
                'profit' => $this->db->get_where('prosentase_harga', ['id' => 2])->row()->prosentase,
                'pajak_ppn' => 10,
                'creator' => $session->id
            );

            $error = 0;

            $id_pembelian = $this->MainModel->insert_id($tabel = 'pembelian_bahan', $pembelian);
            if (!$id_pembelian) $error++;

            $data_obat = array();

            foreach ($this->input->post('obat') as $key => $value) {
                $riwayat = array(
                    'id_bahan' => $value['id_obat'],
                    'id_pembelian' => $id_pembelian,
                    'harga_beli' => $value['harga_beli'],
                    'margin' => str_replace(',', '.', $value['margin'] ?? ''),
                    'harga_jual' => $value['harga_jual'],
                    'jumlah' => $value['jumlah'],
                    'satuan' => $value['satuan']
                );

                $insert_riwata = $this->MainModel->insert_id($tabel = 'riwayat_pembelian_bahan', $riwayat);

                $data_obat[$insert_riwata] = $value;

                $id_item = $this->GudangBahanModel->updateStock($value['id_obat'], $value['jumlah']);

                $this->GudangBahanModel->updateMutasi(
                    $id_item,
                    $value['jumlah'],
                    'Gudang',
                    'Pembelian ID: ' . $id_pembelian
                );

                if (!$insert_riwata) $error++;
            }

            if ($error == 0) {
                $update_pembelian = $this->MainModel->update($tabel = 'pembelian_bahan', ['list_bahan' => serialize($data_obat)], $id_pembelian);
                if (!$update_pembelian) $error++;
            }

            if ($error == 0) {
                $this->session->set_flashdata('success', 'Tambah pembelian bahan habis pakai berhasil!');
                redirect('BahanHabisPakai/pembelian', 'refresh');
            } else {
                $this->session->set_flashdata('warning', 'Tambah pembelian bahan habis pakai gagal!');
                redirect('BahanHabisPakai/tambahPembelian', 'refresh');
            }
        } else {
            $data['persen'] = $this->db->get_where('prosentase_harga', ['id' => 2])->row();
            $data['bahan'] = $this->db->get_where('bahan_habis_pakai', ['is_active' => 1])->result();
            $data['is_edit'] = false;
            $this->template->view('master/habis_pakai/tambah_pembelian', $data);
        }
    }

    public function editPembelian($id_pembelian) {
        if ($this->input->post('submit') == 1) {
            $sesi = $this->session->userdata('logged_in');
            $id = $id_pembelian;
            $p = $this->db->get_where('pembelian_bahan', ['id' => $id_pembelian])->row();

            $old_obat = unserialize($p->list_bahan);
            $data_obat = array();
            $error = 0;

            foreach ($old_obat as $key => $value) {
                if (isset($_POST['obat']['row-' . $key])) {
                    unset($_POST['obat']['row-' . $key]);
                    $data_obat[$key] = $value;
                } else {
                    $id_item = $this->GudangBahanModel->updateStock($value['id_obat'], (0 - $value['jumlah']));

                    $this->GudangBahanModel->updateMutasi(
                        $id_item,
                        (0 - $value['jumlah']),
                        'Penyesuaian Data',
                        'Update pembelian ID: ' . $p->id
                    );

                    $this->db->delete('riwayat_pembelian_bahan', array('id' => $key));
                }
            }

            foreach ($_POST['obat'] as $key => $value) {
                $riwayat = array(
                    'id_bahan' => $value['id_obat'],
                    'id_pembelian' => $p->id,
                    'harga_beli' => $value['harga_beli'],
                    'harga_jual' => $value['harga_jual'],
                    'jumlah' => $value['jumlah'],
                    'satuan' => $value['satuan']
                );

                $insert_riwata = $this->MainModel->insert_id($tabel = 'riwayat_pembelian_bahan', $riwayat);
                $data_obat[$insert_riwata] = $value;

                $id_item = $this->GudangBahanModel->updateStock($value['id_obat'], $value['jumlah']);

                $this->GudangBahanModel->updateMutasi(
                    $id_item,
                    $value['jumlah'],
                    'Gudang',
                    'Pembelian ID: ' . $p->id
                );

                if (!$insert_riwata) $error++;
            }

            if ($error == 0) {
                $pembelian = array(
                    'no_faktur' => $this->input->post('no_faktur'),
                    'tgl_faktur' => $this->input->post('tgl_faktur'),
                    'tgl_jatuh_tempo' => $this->input->post('tgl_jatuh_tempo'),
                    'nama_distributor' => $this->input->post('nama_distributor'),
                    'total' => $this->input->post('total'),
                    'list_bahan' => serialize($data_obat)
                );
                $update_pembelian = $this->MainModel->update($tabel = 'pembelian_bahan', $pembelian, $p->id);
                if (!$update_pembelian) $error++;
            }

            if ($error == 0) {
                $this->session->set_flashdata('success', 'Data bahan habis pakai berhasil update!');
            } else {
                $this->session->set_flashdata('warning', 'Data bahan habis pakai gagal update!');
            }

            redirect('BahanHabisPakai/pembelian');

        } else {
            $p = $this->db->get_where('pembelian_bahan', ['id' => $id_pembelian])->row();
            $data['pembelian'] = $p;
            $data['persen'] = $this->db->get_where('prosentase_harga', ['id' => 2])->row();
            $data['bahan'] = $this->db->get_where('bahan_habis_pakai', ['is_active' => 1])->result();
            $data['is_edit'] = true;
            $this->template->view('master/habis_pakai/tambah_pembelian', $data);
        }
    }

    public function gudang() {
        $data['bahan'] = $this->GudangBahanModel->getObat();

        $this->template->view('master/habis_pakai/gudang', $data);
    }

    public function mutasi() {
        $data['mutasi'] = $this->GudangBahanModel->getMutasi();
//        echo json_encode($data['mutasi']);die();

        $this->template->view('master/habis_pakai/mutasi', $data);
    }

    public function tambahMutasi() {
        if ($this->input->post('submit') == 1) {

            foreach ($_POST['item'] as $key => $value) {
                $stock = 0 - $value['jumlah'];
                $this->GudangBahanModel->updateMutasi(
                    $key,
                    $stock,
                    $_POST['tujuan'],
                    $_POST['note'],
                    $_POST['tanggal'] . ' ' . $_POST['pukul']
                );

                $this->GudangBahanModel->updateStockByID($key, $stock);
            }

            $this->session->set_flashdata('success', 'Tambah mutasi bahan habis pakai berhasil!');
            redirect('BahanHabisPakai/mutasi', 'refresh');

        } else {
            $data['bahan'] = $this->db->get_where('bahan_habis_pakai', ['is_active' => 1])->result();
            $this->template->view('master/habis_pakai/tambah_mutasi', $data);
        }
    }

    public function getDataBahanDiGudang() {
        header('Content-Type: application/json');
        $data = $this->GudangBahanModel->getObatOnGudang($_POST['id']);
        echo json_encode($data);
    }
}

?>
