<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Laporan extends MY_Controller {

    public function __construct()
    {
        parent::__construct();

        $this->load->library('template');
        $this->load->Model('LaporanModel');
        $this->load->Model('PemeriksaanModel');
        $this->load->Model('AdministrasiModel');
        $this->load->Model('MainModel');
        $this->load->Model('PasienModel');
        $this->load->Model('ObatRacikanModel');
        $this->load->Model('DetailObatPemeriksaanModel');
        $this->load->Model('DetailObatRacikanPemeriksaanModel');
        $this->load->Model('RawatInapModel');
        $this->load->Model('RuangBersalinModel');
        $this->load->Model('RuangOperasiModel');
        $this->load->Model('UserModel');
        $this->load->Model('PendaftaranModel');
        $this->load->Model('PerawatModel');
        $this->load->Model('ObatModel');
        $this->load->Model('JenisLayananLaboratoirumModel');

//		$this->load->helper(array('file','php_with_mpdf_helper'));
//		$this->load->helper(array('file','mpdf'));
    }

    public function index()
    {
        redirect('Laporan/jumlahKunjungan');
    }

    public function jumlahKunjungan()
    {
        $start_date = ($this->input->get('from'))?$this->input->get('from'):date('Y-m-d');
        $end_date = ($this->input->get('to'))?$this->input->get('to'):date('Y-m-d');
        $jenis_pendaftaran = $this->input->get('jenis_pendaftaran');

        $data['listjenispendaftaran'] = $this->LaporanModel->getJenisPendaftaranAll();
        $data['jumlah_kunjungan'] = $this->LaporanModel->getKunjunganPasien($start_date, $end_date, $jenis_pendaftaran);
        $data['ListPendaftaran'] = $this->LaporanModel->getSimpleListPendaftaran($start_date,$end_date,$jenis_pendaftaran)->result();
        $data['all_penyakit'] = $this->LaporanModel->getAllPenyakitPemeriksaan()->result();
        $data['jaminan'] = $this->config->item('pendaftaran');

        foreach ($data['ListPendaftaran'] as &$d) {
            if ($d->type == 'Rajal') {
                $d->penyakit = $this->LaporanModel->getSimplePenyakitPemeriksaanByIdPemeriksaan($d->pemeriksaan_id)->result();
            }
            else if ($d->type == 'Ranap') {
                $d->penyakit = $this->LaporanModel->getSimplePenyakitPemeriksaanRiByIdPemeriksaan($d->pemeriksaan_id)->result();
            }
            else {
                $d->penyakit = $this->LaporanModel->getSimplePenyakitPemeriksaanRiByIdPemeriksaan($d->pemeriksaan_id, false)->result();
            }

            if ($d->poli == 'Laboratorium') {
                $d->layanan_lab = $this->JenisLayananLaboratoirumModel->byIdPemeriksaan($d->pemeriksaan_id);
            }
            else {
                $d->layanan_lab = [];
            }
        }

        $data['from'] = $start_date;
        $data['to'] = $end_date;
        $data['jenis_pendaftaran'] = $jenis_pendaftaran;
        $this->template->view('laporan/pasien/jumlah_kunjungan',$data);
    }

    public function jumlahPasien()
    {
        $start_date = ($this->input->get('from'))?$this->input->get('from'):date('Y-m-d');
        $end_date = ($this->input->get('to'))?$this->input->get('to'):date('Y-m-d');
        $jenis_pendaftaran = $this->input->get('jenis_pendaftaran');

        $data['listjenispendaftaran'] = $this->LaporanModel->getJenisPendaftaranAll();
        $data['jumlah_pasien'] = $this->LaporanModel->getJumlahPasien($start_date, $end_date, $jenis_pendaftaran);
        $data['list_jumlah_pasien'] = $this->LaporanModel->listJumlahPasien($start_date,$end_date,$jenis_pendaftaran)->result();
        $data['jaminan'] = $this->config->item('pendaftaran');

        foreach ($data['list_jumlah_pasien'] as &$d) {
            if ($d->type == 'Rajal') {
                $d->penyakit = $this->LaporanModel->getSimplePenyakitPemeriksaanByIdPemeriksaan($d->pemeriksaan_id)->result();
            }
            else if ($d->type == 'Ranap') {
                $d->penyakit = $this->LaporanModel->getSimplePenyakitPemeriksaanRiByIdPemeriksaan($d->pemeriksaan_id)->result();
            }
            else {
                $d->penyakit = $this->LaporanModel->getSimplePenyakitPemeriksaanRiByIdPemeriksaan($d->pemeriksaan_id, false)->result();
            }
        }

        $data['from'] = $start_date;
        $data['to'] = $end_date;
        $data['jenis_pendaftaran'] = $jenis_pendaftaran;
        $this->template->view('laporan/pasien/jumlah_pasien',$data);
    }

    public function jumlahPasienBaru()
    {
        $start_date = ($this->input->get('from'))?$this->input->get('from'):date('Y-m-d');
        $end_date = ($this->input->get('to'))?$this->input->get('to'):date('Y-m-d');
        $jenis_pendaftaran = $this->input->get('jenis_pendaftaran');

        $data['listjenispendaftaran'] = $this->LaporanModel->getJenisPendaftaranAll();
        $data['jumlah_pasien_baru'] = $this->LaporanModel->jumlahPasienBaru($start_date, $end_date, $jenis_pendaftaran);
        $data['list_jumlah_pasien_baru'] = $this->LaporanModel->listJumlahPasienBaru($start_date,$end_date,$jenis_pendaftaran)->result();
        $data['jaminan'] = $this->config->item('pendaftaran');

        foreach ($data['list_jumlah_pasien_baru'] as &$d) {
            if ($d->type == 'Rajal') {
                $d->penyakit = $this->LaporanModel->getSimplePenyakitPemeriksaanByIdPemeriksaan($d->pemeriksaan_id)->result();
            }
            else if ($d->type == 'Ranap') {
                $d->penyakit = $this->LaporanModel->getSimplePenyakitPemeriksaanRiByIdPemeriksaan($d->pemeriksaan_id)->result();
            }
            else {
                $d->penyakit = $this->LaporanModel->getSimplePenyakitPemeriksaanRiByIdPemeriksaan($d->pemeriksaan_id, false)->result();
            }
        }

        $data['from'] = $start_date;
        $data['to'] = $end_date;
        $data['jenis_pendaftaran'] = $jenis_pendaftaran;
        $this->template->view('laporan/pasien/jumlah_pasien_baru',$data);
    }

    public function jumlahPasien20()
    {
        $start_date = ($this->input->get('from'))?$this->input->get('from'):date('Y-m-d');
        $end_date = ($this->input->get('to'))?$this->input->get('to'):date('Y-m-d');
        $jenis_pendaftaran = $this->input->get('jenis_pendaftaran');
        $data['listjenispendaftaran'] = $this->LaporanModel->getJenisPendaftaranAll();
        $data['jumlah_pasien20'] = $this->LaporanModel->jumlahPasien20($start_date,$end_date,$jenis_pendaftaran);
        $data['list_jumlah_pasien20'] = $this->LaporanModel->listJumlahPasien20($start_date,$end_date,$jenis_pendaftaran)->result();
        $data['jaminan'] = $this->config->item('pendaftaran');

        $data['from'] = $start_date;
        $data['to'] = $end_date;
        $data['jenis_pendaftaran'] = $jenis_pendaftaran;
        $this->template->view('laporan/pasien/jumlah_pasien20',$data);
    }

    public function rata2Kunjungan()
    {
        $start_date                   = ($this->input->get('from'))?$this->input->get('from'):date('Y-m-d');
        $end_date                     = ($this->input->get('to'))?$this->input->get('to'):date('Y-m-d');
        $jenis_pendaftaran            = $this->input->get('jenis_pendaftaran');

        $data['listjenispendaftaran'] = $this->LaporanModel->getJenisPendaftaranAll();
        $data['rata_kunjungan']       = $this->LaporanModel->getRataKunjungan($start_date,$end_date,$jenis_pendaftaran)->result();
        $data['list_rata_kunjungan']  = $this->LaporanModel->listRataKunjungan($start_date,$end_date,$jenis_pendaftaran)->result();

        $namas = array_map(function ($v) { return $v->nama;  }, $data['rata_kunjungan']);
        $namas = array_unique($namas);
        $res = [];

        foreach ($namas as $n) {
            $tgls = [];
            $jumlah = 0;
            $id = '';
            foreach ($data['rata_kunjungan'] as $d) {
                if ($d->nama == $n) {
                    $b = str_pad($d->bulan, 2, '0', STR_PAD_LEFT);
                    $t = str_pad($d->tanggal, 2, '0', STR_PAD_LEFT);
                    $tgls[] = "{$d->tahun}-{$b}-{$t}";
                    $jumlah += $d->jumlah;
                    $id = $d->id;
                }
            }
            $rata = $jumlah / sizeof($tgls);
            $res[] = array(
                'id' => $id,
                'nama' => $n,
                'rata_rata' => round($rata, 2),
            );
        }

        $data['from']                 = $start_date;
        $data['to']                   = $end_date;
        $data['jenis_pendaftaran']    = $jenis_pendaftaran;
        $data['res']                  = $res;

        $this->template->view('laporan/pasien/rata2kunjungan',$data);
    }

    public function rata2Pasien()
    {
        $start_date                   = ($this->input->get('from'))?$this->input->get('from'):date('Y-m-d');
        $end_date                     = ($this->input->get('to'))?$this->input->get('to'):date('Y-m-d');
        $jenis_pendaftaran            = $this->input->get('jenis_pendaftaran');

        $data['listjenispendaftaran'] = $this->LaporanModel->getJenisPendaftaranAll();
        $data['rata_kunjungan']       = $this->LaporanModel->getRataPasien($start_date,$end_date,$jenis_pendaftaran)->result();

        $namas = array_map(function ($v) { return $v->nama;  }, $data['rata_kunjungan']);
        $namas = array_unique($namas);
        $res = [];

        foreach ($namas as $n) {
            $tgls = [];
            $jumlah = 0;
            $id = '';
            foreach ($data['rata_kunjungan'] as $d) {
                if ($d->nama == $n) {
                    $b = str_pad($d->bulan, 2, '0', STR_PAD_LEFT);
                    $t = str_pad($d->tanggal, 2, '0', STR_PAD_LEFT);
                    $tgls[] = "{$d->tahun}-{$b}-{$t}";
                    $jumlah += $d->jumlah;
                    $id = $d->id;
                }
            }
            $rata = $jumlah / sizeof($tgls);
            $res[] = array(
                'id' => $id,
                'nama' => $n,
                'rata_rata' => round($rata, 2),
            );
        }

        $data['from']                 = $start_date;
        $data['to']                   = $end_date;
        $data['jenis_pendaftaran']    = $jenis_pendaftaran;
        $data['res']                  = $res;
        $this->template->view('laporan/pasien/rata2pasien',$data);
    }

    public function performaDokter() {
        $start_date                   = ($this->input->get('from'))?$this->input->get('from'):date('Y-m-d');
        $end_date                     = ($this->input->get('to'))?$this->input->get('to'):date('Y-m-d');
        $jenis_pendaftaran            = $this->input->get('jenis_pendaftaran');

        $data['listjenispendaftaran'] = $this->LaporanModel->getJenisPendaftaran();
        $data['dokter'] = $this->LaporanModel->listDokter();
        $data['jenis'] = $this->LaporanModel->getJenisPendaftaran();

        if ($start_date != '' && $end_date != '') {
            $data['performa_dokter'] = $this->LaporanModel->getPerformaDokter($start_date, $end_date, $jenis_pendaftaran);
        }
        else {
            $data['performa_dokter'] = $this->LaporanModel->getPerformaDokter2($jenis_pendaftaran);
        }

        $data['from'] = $start_date;
        $data['to'] = $end_date;
        $data['jenis_pendaftaran'] = $jenis_pendaftaran;
        $this->template->view('laporan/dokter/performa_dokter', $data);
    }

    public function performaPerawat() {
        $start_date                   = ($this->input->get('from'))?$this->input->get('from'):date('Y-m-d');
        $end_date                     = ($this->input->get('to'))?$this->input->get('to'):date('Y-m-d');
        $jenis_pendaftaran            = $this->input->get('jenis_pendaftaran');

        $data['listjenispendaftaran'] = $this->LaporanModel->getJenisPendaftaran();
        $data['jenis'] = $this->LaporanModel->getJenisPendaftaran();

        if ($start_date != '' && $end_date != '') {
            $data['performa_perawat'] = $this->LaporanModel->getPerformaPerawat($start_date, $end_date, $jenis_pendaftaran);
        }
        else {
            $data['performa_perawat'] = $this->LaporanModel->getPerformaPerawat2($jenis_pendaftaran);
        }

        $data['from'] = $start_date;
        $data['to'] = $end_date;
        $data['jenis_pendaftaran'] = $jenis_pendaftaran;
        $this->template->view('laporan/perawat/performa_perawat', $data);
    }

    public function Obat() {
        $start_date         = ($this->input->get('from'))?$this->input->get('from'):date('Y-m-d');
        $end_date           = ($this->input->get('to'))?$this->input->get('to'):date('Y-m-d');
        $jenis_pendaftaran  = $this->input->get('jenis_pendaftaran');

        $obat = $this->LaporanModel->getObatPemeriksaan($start_date, $end_date, $jenis_pendaftaran)->result();
        $obat_racikan = $this->LaporanModel->getObatRacikanPemeriksaan($start_date, $end_date, $jenis_pendaftaran)->result();
        $res = $obat;
        foreach ($obat_racikan as &$o) {
            $ids = array_map(function ($v) { return $v->obat_id; }, $obat);
            if (!in_array($o->obat_id, $ids)) {
                $res[] = $o;
            }
            else {
                foreach ($res as &$r) {
                    if ($r->obat_id == $o->obat_id) {
                        $r->jumlah += $o->jumlah;
                        break;
                    }
                }
            }
        }
        usort($res, function($a, $b) {
            return $b->jumlah - $a->jumlah;
        });

        $data['listjenispendaftaran'] = $this->LaporanModel->getJenisPendaftaran();
        $data['obat'] = array_slice($res, 0, 10);
        $data['from'] = $start_date;
        $data['to'] = $end_date;
        $data['jenis_pendaftaran'] = $jenis_pendaftaran;
        $this->template->view('laporan/obat/obat',$data);
    }

    public function obat_resep_luar() {
        $start_date         = ($this->input->get('from'))?$this->input->get('from'):date('Y-m-d');
        $end_date           = ($this->input->get('to'))?$this->input->get('to'):date('Y-m-d');

        $obat = $this->LaporanModel->getObatResepLuar($start_date, $end_date)->result();
        $obat_racikan = $this->LaporanModel->getObatRacikResepLuar($start_date, $end_date)->result();
        $res = $obat;
        foreach ($obat_racikan as &$o) {
            $ids = array_map(function ($v) { return $v->obat_id; }, $obat);
            if (!in_array($o->obat_id, $ids)) {
                $res[] = $o;
            }
            else {
                foreach ($res as &$r) {
                    if ($r->obat_id == $o->obat_id) {
                        $r->jumlah += $o->jumlah;
                        break;
                    }
                }
            }
        }
        usort($res, function($a, $b) {
            return $b->jumlah - $a->jumlah;
        });

        $data['listjenispendaftaran'] = $this->LaporanModel->getJenisPendaftaran();
        $data['obat'] = array_slice($res, 0, 10);
        $data['from'] = $start_date;
        $data['to'] = $end_date;
        $this->template->view('laporan/obat/resep_luar',$data);
    }

    public function obat_obat_bebas() {
        $start_date         = ($this->input->get('from'))?$this->input->get('from'):date('Y-m-d');
        $end_date           = ($this->input->get('to'))?$this->input->get('to'):date('Y-m-d');

        $obat = $this->LaporanModel->getObatObatBebas($start_date, $end_date)->result();
        $obat_racikan = $this->LaporanModel->getObatRacikObatBebas($start_date, $end_date)->result();
        $res = $obat;
        foreach ($obat_racikan as &$o) {
            $ids = array_map(function ($v) { return $v->obat_id; }, $obat);
            if (!in_array($o->obat_id, $ids)) {
                $res[] = $o;
            }
            else {
                foreach ($res as &$r) {
                    if ($r->obat_id == $o->obat_id) {
                        $r->jumlah += $o->jumlah;
                        break;
                    }
                }
            }
        }
        usort($res, function($a, $b) {
            return $b->jumlah - $a->jumlah;
        });

        $data['listjenispendaftaran'] = $this->LaporanModel->getJenisPendaftaran();
        $data['obat'] = array_slice($res, 0, 10);
        $data['from'] = $start_date;
        $data['to'] = $end_date;
        $this->template->view('laporan/obat/obat_bebas',$data);
    }

    public function obat_obat_internal() {
        $start_date         = ($this->input->get('from'))?$this->input->get('from'):date('Y-m-d');
        $end_date           = ($this->input->get('to'))?$this->input->get('to'):date('Y-m-d');

        $obat = $this->LaporanModel->getObatObatInternal($start_date, $end_date)->result();
        $obat_racikan = $this->LaporanModel->getObatRacikObatInternal($start_date, $end_date)->result();
        $res = $obat;
        foreach ($obat_racikan as &$o) {
            $ids = array_map(function ($v) { return $v->obat_id; }, $obat);
            if (!in_array($o->obat_id, $ids)) {
                $res[] = $o;
            }
            else {
                foreach ($res as &$r) {
                    if ($r->obat_id == $o->obat_id) {
                        $r->jumlah += $o->jumlah;
                        break;
                    }
                }
            }
        }
        usort($res, function($a, $b) {
            return $b->jumlah - $a->jumlah;
        });

        $data['listjenispendaftaran'] = $this->LaporanModel->getJenisPendaftaran();
        $data['obat'] = array_slice($res, 0, 10);
        $data['from'] = $start_date;
        $data['to'] = $end_date;
        $this->template->view('laporan/obat/obat_internal',$data);
    }

    public function Penyakit() {
        $start_date         = ($this->input->get('from'))?$this->input->get('from'):date('Y-m-d');
        $end_date           = ($this->input->get('to'))?$this->input->get('to'):date('Y-m-d');
        $jenis_pendaftaran  = $this->input->get('jenis_pendaftaran');

        $data['listjenispendaftaran'] = $this->LaporanModel->getJenisPendaftaranAll();
        $data['penyakit'] = $this->LaporanModel->getPenyakitPemeriksaan($start_date, $end_date, $jenis_pendaftaran);

        $data['from'] = $start_date;
        $data['to'] = $end_date;
        $data['jenis_pendaftaran'] = $jenis_pendaftaran;
        $this->template->view('laporan/penyakit/penyakit',$data);
    }

    public function RekamMedis($pasien_id = false) {
        $start_date = $this->input->get('from');
        $end_date = $this->input->get('to');

        $data['listjenispendaftaran'] = $this->LaporanModel->getJenisPendaftaran();
        $data['pasien'] = $this->LaporanModel->getPasienPemeriksaan2();

        $data['from'] = $start_date;
        $data['to'] = $end_date;

        if ($pasien_id) {
            $data['pemeriksaan'] = $this->LaporanModel->getPemeriksaanBayarById($pasien_id)->result();
            $data['tindakan'] = $this->LaporanModel->getTindakanById($pasien_id);
            $data['obat'] = $this->LaporanModel->getObatPemeriksaanById($pasien_id);
            $data['penyakit'] = $this->LaporanModel->getPenyakitPemeriksaanById($pasien_id);
            $data['racikan']  = $this->LaporanModel->getRacikanPemeriksaan($pasien_id);
            $data['pasien_terpilih']  = $this->PasienModel->getPasienById($pasien_id)->row();

            foreach ($data['pemeriksaan'] as $v) {
                $v->racikans = $this->ObatRacikanModel->getRacikanByIdPemeriksaan($v->id)->result();
                foreach ($v->racikans as &$vv) {
                    $vv->racikan = $this->ObatRacikanModel->getObatRacikanByIdDetailObatRacikan($vv->id)->result();
                }

                $racikan_luar = $this->ObatRacikanModel->getRacikanLuarByIdPemeriksaan($v->id)->result();
                foreach ($racikan_luar as &$vv) {
                    $vv->racikan = $this->ObatRacikanModel->getObatRacikanLuarByIdDetailObatRacikan($vv->id)->result();
                }
                $v->racikans = array_merge($v->racikans, $racikan_luar);

                if (strpos($v->nama_jenis_pendaftaran, 'aborat') !== false) {
                    $v->tindakan = $this->LaporanModel->getTindakanLabByPemeriksaanId($v->id)->result();
                }
                else {
                    $v->tindakan = $this->LaporanModel->getTindakanByIdPemeriksaan($v->id)->result();
                }

                $v->images = $this->db->query("SELECT * FROM pemeriksaan_image WHERE pemeriksaan_id = $v->id ORDER BY id")->result();
            }

            $data['list_rawat_inap'] = $this->RawatInapModel->getSelesaiByPasienId($pasien_id);
            foreach ($data['list_rawat_inap'] as &$r) {
                $r['diagnosis'] = $this->RawatInapModel->getDiagnosisByRawatInapId($r['id']);
                foreach ($r['diagnosis'] as &$d) {
                    $d->penyakit = $this->RawatInapModel->getPenyakitByDiagnosisId($d->id);
                }
            }

            $data['list_ruang_bersalin'] = $this->RuangBersalinModel->getSelesaiByPasienId($pasien_id);
            foreach ($data['list_ruang_bersalin'] as &$r) {
                $r['diagnosis'] = $this->RuangBersalinModel->getDiagnosisByTransferId($r['transfer_id'], 2);
                foreach ($r['diagnosis'] as &$d) {
                    $d->penyakit = $this->RuangBersalinModel->getPenyakitByDiagnosisId($d->id);
                }
            }

            $data['list_ruang_operasi'] = $this->RuangOperasiModel->getSelesaiByPasienId($pasien_id);
            foreach ($data['list_ruang_operasi'] as &$r) {
                $r['diagnosis'] = $this->RuangOperasiModel->getDiagnosisByTransferId($r['transfer_id'], 3);
                foreach ($r['diagnosis'] as &$d) {
                    $d->penyakit = $this->RuangOperasiModel->getPenyakitByDiagnosisId($d->id);
                }
            }

            $data['tindakan'] = $this->LaporanModel->getTindakanById($data['pasien_terpilih']->id);

            $this->template->view('laporan/rekam_medis/detail_rekam_medis',$data);
        }
        else {
            $this->template->view('laporan/rekam_medis/rekam_medis',$data);
        }
    }

    public function VisitRate()
    {
        $jenis = $this->input->get('jenis');
        $from = $this->input->get('from');
        $to = $this->input->get('to');
        $bulan = $this->input->get('bulan');
        $tahun = $this->input->get('tahun');

        $start_date = $end_date = date('Y-m-d');
        if ($jenis == 2) {
            $start_date = $from;
            $end_date = $to;
        }
        if ($jenis == 3) {
            $start_date = date("$tahun-$bulan-01");
            $end_date = date("$tahun-$bulan-t");
        }

        $data['jenis'] = $jenis;
        $data['from'] = $from;
        $data['to'] = $to;
        $data['bulan'] = $bulan;
        $data['tahun'] = $tahun;
        $data['list'] = $this->LaporanModel->get_visit_rate($start_date, $end_date);
        $data['jaminan'] = $this->config->item('pendaftaran');

        $this->template->view('laporan/visit_rate', $data);
    }

    public function detail_kode_booking($kode_booking)
    {
        $ant = $this->db->query("SELECT * FROM antrian WHERE kode_booking = '$kode_booking'")->row();
        $pdf = $this->db->query("SELECT * FROM pendaftaran_pasien WHERE id = $ant->pendaftaran_id")->row();
        $pem = $this->db->query("SELECT * FROM pemeriksaan WHERE id = $ant->pemeriksaan_id")->row();
        $log = $this->db->query("SELECT * FROM bpjs_jknmobile_log WHERE kode_booking = '$kode_booking'")->result();

        echo json_encode([
            'antrian' => $ant,
            'pendaftaran' => $pdf,
            'pemeriksaan' => $pem,
            'log' => $log,
        ]);
    }

    public function DetailRiwayatPoli($pemeriksaan_id)
    {
        $data['pendaftaran'] = $this->PemeriksaanModel->getPendaftaranByIdPemeriksaan($pemeriksaan_id)->row_array();
        $data['pemeriksaan'] = $this->PemeriksaanModel->getPemeriksaanById($pemeriksaan_id)->row_array();
        $data['images'] = $this->db->query("SELECT * FROM pemeriksaan_image WHERE pemeriksaan_id = $pemeriksaan_id ORDER BY id")->result();
        $data['form'] = unserialize($data['pemeriksaan']['form']);
        $data['tht_result'] = unserialize($data['pemeriksaan']['meta']);
        $data['hd'] = unserialize($data['pemeriksaan']['hd']);

        $data['jaminan'] = $this->config->item('pendaftaran');
        $data['obat_periksa'] = $this->DetailObatPemeriksaanModel->getDetailObatByPemeriksaanId($pemeriksaan_id)->result();
        $data['obat_racikan_periksa'] = $this->DetailObatRacikanPemeriksaanModel->getDetailObatRacikanByPemeriksaanId($pemeriksaan_id)->result();
        foreach ($data['obat_racikan_periksa'] as $k => $v) {
            $data['obat_racikan_periksa'][$k]->obat = $this->AdministrasiModel->getListObatByDetailObatRacikanPemeriksaanId($v->id)->result();
        }
        $racikan_luar = $this->DetailObatRacikanPemeriksaanModel->getDetailObatRacikanLuarByPemeriksaanId($pemeriksaan_id)->result();
        foreach ($racikan_luar as $k => $v) {
            $racikan_luar[$k]->obat = $this->AdministrasiModel->getListObatByDetailObatRacikanLuarPemeriksaanId($v->id)->result();
        }
        $data['obat_racikan_periksa'] = array_merge($data['obat_racikan_periksa'], $racikan_luar);

        $category = 'umum';
        foreach ($this->config->item('poli') as $k => $v) {
            if (in_array($data['pendaftaran']['kode_daftar'], $v['kode'])) {
                $category = $k;
                break;
            }
        }

        $data['listPerawat'] = $this->PerawatModel->listPerawat();
        $data['dokter'] = $this->PendaftaranModel->getDokter();
        $data['tindakan'] = $this->LaporanModel->getTindakanByIdPemeriksaan($pemeriksaan_id)->result();
        $data['tindakan_konsul'] = $this->LaporanModel->getTindakanKonsulByIdPemeriksaan($pemeriksaan_id)->result();
        $data['all_penyakit'] = $this->PemeriksaanModel->getPenyakitByCategory($category)->result();
        $data['penyakit'] = $this->LaporanModel->getPenyakitPemeriksaanByIdPemeriksaan($pemeriksaan_id)->result();

        if ($data['pendaftaran']['jenis_pendaftaran_id'] == 58)
            $this->template->view('laporan/rekam_medis/detail_riwayat_poli_igd', $data);
        else
            $this->template->view('laporan/rekam_medis/detail_riwayat_poli', $data);
    }

    function EditRekamMedis($id, $to = 0)
    {
        $data['to'] = $to;
        $data['pemeriksaan'] = $this->LaporanModel->getPemeriksaanSudahPeriksaByIdPemeriksaan($id)->row_array();
        $data['pasien'] = $this->db->query("SELECT * FROM pasien WHERE id = {$data['pemeriksaan']['pasien_id']}")->row();
        $data['images'] = $this->db->query("SELECT * FROM pemeriksaan_image WHERE pemeriksaan_id = $id ORDER BY id")->result();
        $data['form'] = unserialize($data['pemeriksaan']['form']);
        $data['tindakan_konsul'] = $this->LaporanModel->getTindakanKonsulByIdPemeriksaan($id)->result();
        $data['s_tindakan'] = $this->LaporanModel->getTindakanByIdPemeriksaan($id)->result();
        $data['s_obat'] = $this->LaporanModel->getObatPemeriksaanByIdPemeriksaan($id)->result();
        $data['s_penyakit'] = $this->LaporanModel->getPenyakitPemeriksaanByIdPemeriksaan($id)->result();
        $data['s_racikan']  = $this->LaporanModel->getRacikanPemeriksaan($id)->result();
        $data['pasien_terpilih']  = $this->PasienModel->getPasienById($data['pemeriksaan']['pasien_id'])->row();
        $data['listPerawat'] = $this->PerawatModel->listPerawat();
        $data['dokter'] = $this->PendaftaranModel->getDokter();
        $data['klinik'] = $this->AdministrasiModel->getKlinik()->row();

        //----------- Pilihan All
        $data['obat'] = $this->PemeriksaanModel->getObat()->result();

        $data['pendaftaran'] = $this->PemeriksaanModel->getPendaftaranByIdPemeriksaan($id)->row_array();
        $category = 'umum';
        foreach ($this->config->item('poli') as $k => $v) {
            if (in_array($data['pendaftaran']['kode_daftar'], $v['kode'])) {
                $category = $k;
                break;
            }
        }

        $data['tindakan'] = $this->PemeriksaanModel->getTindakanByCategory($category);
        $data['penyakit'] = $this->PemeriksaanModel->getPenyakitByCategory($category);
        //----------- END Pilihan All

        $data['obat1'] = $this->db->get_where('obat', ['is_active' => 1, 'stok_obat >' => 0])->result();
        $data['bahan'] = $this->db->get_where('bahan_habis_pakai', ['is_active' => 1, 'jumlah >' => 0])->result();

        $pemeriksaan_id = $data['pemeriksaan']['id'];
        $data['jaminan'] = $this->config->item('pendaftaran');
        $data['obat_periksa'] = $this->DetailObatPemeriksaanModel->getDetailObatByPemeriksaanId($pemeriksaan_id)->result();

        $data['obat_racikan_periksa'] = $this->DetailObatRacikanPemeriksaanModel->getDetailObatRacikanByPemeriksaanId($pemeriksaan_id)->result();
        foreach ($data['obat_racikan_periksa'] as $k => $v) {
            $data['obat_racikan_periksa'][$k]->obat = $this->AdministrasiModel->getListObatByDetailObatRacikanPemeriksaanId($v->id)->result();
        }
        $racikan_luar = $this->DetailObatRacikanPemeriksaanModel->getDetailObatRacikanLuarByPemeriksaanId($pemeriksaan_id)->result();
        foreach ($racikan_luar as $k => $v) {
            $racikan_luar[$k]->obat = $this->AdministrasiModel->getListObatByDetailObatRacikanLuarPemeriksaanId($v->id)->result();
        }
        $data['obat_racikan_periksa'] = array_merge($data['obat_racikan_periksa'], $racikan_luar);

        if ($data['pendaftaran']['jenis_pendaftaran_id'] == 58)
            $this->template->view('laporan/rekam_medis/edit_rekam_medis_igd', $data);
        else
            $this->template->view('laporan/rekam_medis/edit_rekam_medis',$data);
    }

    function SimpanEditRekamMedis($pemeriksaan_id) {
        if (isset($_POST['meta'])) {
            $meta = serialize($this->input->post('meta'));
        }
        else {
            $meta = '';
        }

        $hd = isset($_POST['hd']) ? serialize($this->input->post('hd')) : '';

        $hp_lab = $this->input->post('hasil_penunjang_laboratorium');
        $hp_ekg = $this->input->post('hasil_penunjang_ekg');
        $hp_spirometri = $this->input->post('hasil_penunjang_spirometri');

        if ($hp_lab) {
            $hp['laboratorium'] = $hp_lab;
        }
        if ($hp_ekg) {
            $hp['ekg'] = $hp_ekg;
        }
        if ($hp_spirometri) {
            $hp['spirometri'] = $hp_spirometri;
        }

        $session = $this->session->userdata('logged_in');
        $periksa = array(
            'diagnosa_perawat' => $this->input->post('diagnosa_perawat'),
            'keluhan_utama' => $this->input->post('keluhan_utama'),
            'catatan_odontogram' => $this->input->post('catatan_odontogram'),
            'meta' => $meta,
            'hd' => $hd,
            'amammesia' => $this->input->post('amammesia'),
            'diagnosis' => $this->input->post('diagnosis'),
            'pemeriksaan_fisik' => nl2br($this->input->post('pemeriksaan_fisik')),
            'hasil_penunjang' => json_encode($hp),
            'diagnosis_banding' => $this->input->post('diagnosis_banding'),
            'deskripsi_tindakan' => $this->input->post('deskripsi_tindakan'),
            'saran_pemeriksaan' => $this->input->post('saran_pemeriksaan'),
            'form' => serialize($this->input->post('form')),
        );
        if ($this->input->post('kode_daftar') == 'PL' || $this->input->post('kode_daftar') == 'BPJS-PL') {
            $periksa['hasil_lab'] = $this->getHasilLab();
        }

        if (isset($_POST['asuhan_keperawatan'])) {
            $periksa['asuhan_keperawatan'] = $this->input->post('asuhan_keperawatan');
        }

        $this->MainModel->update('pemeriksaan', $periksa, $pemeriksaan_id);

        if ($this->input->post('surat')) {
            $this->MainModel->update('pendaftaran_pasien', [
                'surat' => $this->input->post('surat'),
                'surat_form' => json_encode($this->input->post('surat_form')),
            ], $this->input->post('pendaftaran_id'));
        }

        $input_tindakan = $this->input->post('tindakan');
        $penyakit = $this->input->post('diagnosis_jenis_penyakit');

        $this->db->delete('detail_tindakan_pemeriksaan', array('pemeriksaan_id' => $pemeriksaan_id));
        $this->db->delete('detail_penyakit_pemeriksaan', array('pemeriksaan_id' => $pemeriksaan_id));

        // --------------- TINDAKAN ---------------- //

        $tin = array();
        foreach ($input_tindakan as $key => $value) {
            $tindakan = array(
                'pemeriksaan_id' => $pemeriksaan_id,
                'tarif_tindakan_id' => $value,
                'creator' => $session->id
            );
            $tin[] = $tindakan;

            $this->MainModel->insert_id('detail_tindakan_pemeriksaan', $tindakan);
        }

        // --------------- PENYAKIT ---------------- //

        $pen = array();
        foreach ($penyakit as $key => $value) {
            $penyakit = array(
                'pemeriksaan_id' => $pemeriksaan_id,
                'penyakit_id' => $value,
                'creator' => $session->id
            );
            $pen[] = $penyakit;

            $this->MainModel->insert_id('detail_penyakit_pemeriksaan',$penyakit);
        }

        if ($this->input->post('to') == 'perawat')
            redirect('PemeriksaanAwal');
        else if ($this->input->post('to') == 'dokter')
            redirect('pemeriksaan/listPasienSelesaiPeriksa');
        else if ($this->input->post('to') == 'dokter_igd')
            redirect('Igd/selesai');
        else
            redirect('Laporan/RekamMedis/'.$this->input->post('pasien_id'));
    }

    public function edit_resep_rekam_medis($id)
    {
        if ($this->input->post()) {

            $session = $this->session->userdata('logged_in');

            // --------------- OBAT ---------------- //

            $jumlah_satuan = $this->input->post('jumlah_satuan');
            $signa_obat = $this->input->post('signa_obat');
            $input_obat = $this->input->post('nama_obat');

            foreach ($input_obat as $key => $value) {
                $obat = array(
                    'pemeriksaan_id' => $id,
                    'obat_id'        => $value,
                    'jumlah_satuan'  => $jumlah_satuan[$key],
                    'signa_obat'     => $signa_obat[$key],
                    'creator'        => $session->id
                );

                $this->MainModel->insert_id('detail_obat_pemeriksaan', $obat);
            }

            // --------------- OBAT RACIKAN ---------------- //

            for ($n=1; $n <= 5; $n++) {

                $signa = $this->input->post('signa'.$n);
                $catatan = $this->input->post('catatan'.$n);
                $racikan = array(
                    'pemeriksaan_id' => $id,
                    'nama_racikan'   => 'racikan '.$n,
                    'signa'          => $signa,
                    'catatan'        => $catatan,
                    'creator'        => $session->id
                );

                $j = 0;
                $id_obat_racikan = $this->input->post('nama_obat_racikan'.$n);
                $jumlah_satuan = $this->input->post('jumlah_satuan_racikan'.$n);
                if (!$id_obat_racikan) {
                    continue;
                }

                $cek = $this->db->query("
                    SELECT * FROM detail_obat_racikan_pemeriksaan 
                    WHERE pemeriksaan_id = $id AND nama_racikan = 'racikan $n' 
                ")->row();

                if ($cek) {
                    $this->MainModel->update('detail_obat_racikan_pemeriksaan', $racikan, $cek->id);
                    $detail_obat_racikan_pemeriksaan_id = $cek->id;
                }
                else {
                    $detail_obat_racikan_pemeriksaan_id = $this->MainModel->insert_id('detail_obat_racikan_pemeriksaan',$racikan);
                }

                foreach ($id_obat_racikan as $key => $value) {
                    $obat_racikan = array(
                        'detail_obat_racikan_pemeriksaan_id'    => $detail_obat_racikan_pemeriksaan_id,
                        'obat_id'                               => $value,
                        'jumlah_satuan'                         => $jumlah_satuan[$j],
                        'creator'                               => $session->id
                    );
                    $this->MainModel->insert_id('obat_racikan', $obat_racikan);

                    $j++;
                }
            }

            $pemeriksaan = $this->AdministrasiModel->getPemeriksaanById($id)->row();
            $this->session->set_flashdata('success', 'Edit resep berhasil!');
            redirect("Laporan/RekamMedis/$pemeriksaan->pasien_id");
        }
        else {
            $data['pemeriksaan'] = $this->AdministrasiModel->getPemeriksaanById($id)->row();
            $data['pendaftaran'] = $this->PemeriksaanModel->getPendaftaranByIdPemeriksaan($id)->row_array();
            $data['obat_all'] = $this->ObatModel->listObat()->result();
            $data['obats'] = $this->db->query("
                SELECT dop.*, o.nama, o.harga_jual
                FROM detail_obat_pemeriksaan dop
                JOIN obat o ON o.id = dop.obat_id and o.is_active = 1
                JOIN pemeriksaan pem ON pem.id = dop.pemeriksaan_id and pem.is_active = 1
                WHERE pem.id = $id
                AND dop.is_active = 1
                    UNION ALL
                SELECT dop.id, pol.pemeriksaan_id, dop.obat_id, dop.jumlah_satuan, dop.signa_obat, dop.is_active, dop.created_at, dop.updated_at, dop.creator, o.nama, o.harga_jual
                FROM detail_penjualan_obat_luar dop
                JOIN obat o ON o.id = dop.obat_id and o.is_active = 1
                JOIN penjualan_obat_luar pol on dop.penjualan_obat_luar_id = pol.id
                JOIN pemeriksaan p on pol.pemeriksaan_id = p.id
                WHERE p.id = $id
                AND dop.is_active = 1
            ")->result();
            $racikan = $this->ObatRacikanModel->getRacikanByIdPemeriksaan($data['pemeriksaan']->id)->result();
            foreach ($racikan as &$vv) {
                $vv->racikan = $this->ObatRacikanModel->getObatRacikanByIdDetailObatRacikan($vv->id)->result();
            }
            $racikan_luar = $this->ObatRacikanModel->getRacikanLuarByIdPemeriksaan($data['pemeriksaan']->id)->result();
            foreach ($racikan_luar as &$vv) {
                $vv->racikan = $this->ObatRacikanModel->getObatRacikanLuarByIdDetailObatRacikan($vv->id)->result();
            }
            $data['racikans'] = array_merge($racikan, $racikan_luar);

            $this->template->view('laporan/rekam_medis/edit_resep',$data);
        }
    }

    public function downloadPemeriksaan($pemeriksaan_id)
    {
        $data['pendaftaran'] = $this->PemeriksaanModel->getPendaftaranByIdPemeriksaan($pemeriksaan_id)->row_array();
        $data['pemeriksaan'] = $this->PemeriksaanModel->getPemeriksaanById($pemeriksaan_id)->row_array();
        $data['tht_result'] = unserialize($data['pemeriksaan']['meta']);
        $data['jaminan'] = $this->config->item('pendaftaran');
        $data['obat_periksa'] = $this->DetailObatPemeriksaanModel->getDetailObatByPemeriksaanId($pemeriksaan_id)->result();
        $data['obat_racikan_periksa'] = $this->DetailObatRacikanPemeriksaanModel->getDetailObatRacikanByPemeriksaanId($pemeriksaan_id)->result();
        foreach ($data['obat_racikan_periksa'] as $k => $v) {
            $data['obat_racikan_periksa'][$k]->obat = $this->AdministrasiModel->getListObatByDetailObatRacikanPemeriksaanId($v->id)->result();
        }

        $category = 'umum';
        foreach ($this->config->item('poli') as $k => $v) {
            if (in_array($data['pendaftaran']['kode_daftar'], $v['kode'])) {
                $category = $k;
                break;
            }
        }

        $data['tindakan'] = $this->LaporanModel->getTindakanByIdPemeriksaan($pemeriksaan_id)->result();
        $data['all_penyakit'] = $this->PemeriksaanModel->getPenyakitByCategory($category)->result();
        $data['penyakit'] = $this->LaporanModel->getPenyakitPemeriksaanByIdPemeriksaan($pemeriksaan_id)->result();

        $data['head']          = $this->load->view('template/head', $data, TRUE);
        $data['header']        = $this->load->view('template/header', $data, TRUE);
        $data['sidebar']       = $this->load->view('template/sidebar', $data, TRUE);
        $data['right_sidebar'] = $this->load->view('template/right_sidebar', $data, TRUE);
        $data['footer']        = $this->load->view('template/footer', $data, TRUE);
        $data['footer_js']     = $this->load->view('template/footer_js', $data, TRUE);

        $date = date('dmY_His');
        $f_name = "rekam_medis_" . $date . ".pdf";

        $this->load->helper('mpdf');
        $mpdf=new mPDF('c');

        $stylesheet = '
        table.bor, th.bor, td.bor {
            font-size: 12px;
            border: 1px solid #e5e5e5;
        }
        th, td {
            padding: 0px 5px;
        }
        td.cat {
            min-width: 100px;
            max-width: 300px;
        }
        td.no-data {
            padding: 5px;
            text-align: center;
        }
        .select2-container {
           width: 100% !important;
        }
        input {
           width: 100% !important;
        }
        .opt { 
          display:inline-block; 
          vertical-align:top; 
          overflow:hidden; 
          border:solid grey 1px;
        }
        .opt select { 
          padding:10px; 
          margin:-5px -20px -5px -5px; 
        }
        /*!
 * Bootstrap v4.0.0 (https://getbootstrap.com)
 * Copyright 2011-2018 The Bootstrap Authors
 * Copyright 2011-2018 Twitter, Inc.
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
/*# sourceMappingURL=bootstrap.min.css.map */
        
        ';
        $html = $this->load->view('laporan/rekam_medis/riwayat_poli_download', $param, true);
//die(json_encode($stylesheet));
        $mpdf->WriteHTML($stylesheet, 1);
        $mpdf->WriteHTML($html, 2);
        $mpdf->Output($f_name, 'D');
        exit;
    }

    public function printPemeriksaan($pemeriksaan_id)
    {
        $data['klinik'] = $this->UserModel->getKlinik()->row();
        $data['pendaftaran'] = $this->PemeriksaanModel->getPendaftaranByIdPemeriksaan($pemeriksaan_id)->row_array();
        $data['pemeriksaan'] = $this->PemeriksaanModel->getPemeriksaanById($pemeriksaan_id)->row_array();
        $data['pemeriksaan_image'] = $this->db->query("SELECT * FROM pemeriksaan_image WHERE pemeriksaan_id = $pemeriksaan_id")->result();
        $data['pasien'] = $this->db->query("SELECT * FROM pasien WHERE id = {$data['pemeriksaan']['pasien_id']}")->row_array();
        $data['form'] = unserialize($data['pemeriksaan']['form']);
        $data['telaah'] = unserialize($data['pemeriksaan']['telaah']);
        $data['tht_result'] = unserialize($data['pemeriksaan']['meta']);
        $data['jaminan'] = $this->config->item('pendaftaran');
        $data['obat_periksa'] = $this->DetailObatPemeriksaanModel->getDetailObatByPemeriksaanId($pemeriksaan_id)->result();
        $data['obat_racikan_periksa'] = $this->DetailObatRacikanPemeriksaanModel->getDetailObatRacikanByPemeriksaanId($pemeriksaan_id)->result();
        foreach ($data['obat_racikan_periksa'] as $k => $v) {
            $data['obat_racikan_periksa'][$k]->obat = $this->AdministrasiModel->getListObatByDetailObatRacikanPemeriksaanId($v->id)->result();
        }

        $category = 'umum';
        foreach ($this->config->item('poli') as $k => $v) {
            if (in_array($data['pendaftaran']['kode_daftar'], $v['kode'])) {
                $category = $k;
                break;
            }
        }
        $data['title'] = $category;

        $t = $this->LaporanModel->getTindakanByIdPemeriksaan($pemeriksaan_id)->result();
        foreach ($t as $v) {
            if ($v->tarif_tindakan_id != 523) {
                $data['tindakan'] []= $v;
            }
        }
        $data['all_penyakit'] = $this->PemeriksaanModel->getPenyakitByCategory($category)->result();
        $data['penyakit'] = $this->LaporanModel->getPenyakitPemeriksaanByIdPemeriksaan($pemeriksaan_id)->result();

        $data['soap'] = json_decode($data['pemeriksaan']['soap_apoteker']);
        $data['telaah'] = json_decode($data['pemeriksaan']['telaah'], true);
        $data['idx'] = [
            'resep_lengkap',
            'pasien_sesuai',
            'nama_obat',
            'bentuk_sediaan',
            'dosis',
            'jumlah_obat',
            'aturan_pakai',
            'tepat_indikasi',
            'tepat_dosis',
            'tepat_waktu_penggunaan',
            'duplikasi_pengobatan',
            'alergi',
            'kontraindikasi',
            'telaah_obat',
            'nama_obat_dengan_resep',
            'jumlah_dosis_dengan_resep',
            'rute_dengan_resep',
            'waktu_frekuensi_pemberian',
        ];

        if ($category == 'umum' || $category == 'penyakit-dalam' || $category == 'bedah' || $category == 'ekg')
            $this->load->view('laporan/rekam_medis/print/umum', $data);
        else if ($category == 'igd')
            $this->load->view('laporan/rekam_medis/print/igd', $data);
        else if ($category == 'gigi')
            $this->load->view('laporan/rekam_medis/print/gigi', $data);
        else if ($category == 'mata')
            $this->load->view('laporan/rekam_medis/print/mata', $data);
        else if ($category == 'kulit')
            $this->load->view('laporan/rekam_medis/print/kulit', $data);
        else if ($category == 'tht')
            $this->load->view('laporan/rekam_medis/print/tht', $data);
        else if ($category == 'anak')
            $this->load->view('laporan/rekam_medis/print/anak', $data);
        else if ($category == 'obgyn')
            $this->load->view('laporan/rekam_medis/print/obgyn', $data);
        else if ($category == 'jiwa')
            $this->load->view('laporan/rekam_medis/print/jiwa', $data);
        else if ($category == 'syaraf')
            $this->load->view('laporan/rekam_medis/print/syaraf', $data);
        else
            $this->load->view('laporan/rekam_medis/print/umum', $data);
    }

    private function getHasilLab() {
        $forms = [
            'hemoglobin','led','leukosit','hitung','eosinophyl','basophyl','stab','segment','lymposit','monosit','sel_lainnya','eosinofil','eritrosit','trombocyt','reticulocyt',
            'hematacrit','mcv','mch','mchc','waktu_pendarahan','waktu_pembekuan','waktu_prothombin','waktu_rekalsifikasi','ptt','thrombotes_owren','fibrinogen','retraksi_bekuan',
            'retraksi_osmotik','malaria','plasmodium_falcifarum','plasmodium_vivax','prc_pembendungan','darah_lengkap','rdw_cv','rdw_sd','mpv','pdw','pct','limfosit','analisa_hb',
            'analisa_hb','hba2','hbf','ferritin','tibc','pt','aptt','inr','ureum_darah','ureum_urin','creatin_darah','creatin_urine','creatin_clearence','urea_clearence','alkali_reserve',
            'fosfat_anorganik','uric_acid','serum_iron','tibc','bilirudin_total','bilirudin_direk','bilirudin_indirek','protein_total','albumin','sgot','sgpt','gamma_gt','askali_fosfatase',
            'chollinesterase','ck','ldh','ck_m8','alpha_hbdh','globulin','gula_darah_puasa','reduksi','gula_darah_2jam','reduksi_2','gula_darah_sewaktu','gtt_puasa','gtt_1/2jam','gtt_1jam',
            'gtt_11/2jam','gtt_2jam','hb_A-1c','ii','cholesterol_total','hdl_cholesterol','ldl_cholesterol','triglycerida','lipid_total','cholesterol_ldl_direk','natrium','kalium','chlorida',
            'calsium','magnesium','pengecatan_gram','bta','mikroskopik_gonore','trikomonas','jamur','kultur_sensitivitas','batang_gram-','batang_gram+','coccus_gram-','coccus_gram+','trichomonas',
            'mikroskopik_candida','widal','salmonela_typhi_O','salmonela_typhi_h','salmonela_paratyphi_a_h','salmonela_paratyphi_ao','salmonela_paratyphi_bo','salmonela_paratyphi_co',
            'salmonela_paratyphi_bh','salmonela_paratyphi_ch','hbsag','hiv','tpha','hbsag','dengue_ig_g','dengue_ig_m','anti_hbsag','antihbc_total','hbc','anti_tb_ig_m','anti_tb_ig_g','hcv',
            'anti_hev_ig_m','anti_hev_ig_g','hbeag','anti_hbe','vdrl','asto','titer_reumatoid_factor','anti_hav_igm','anti_hcv','toxoplasma_ig_a','toxoplasma_ig_g','toxoplasma_ig_g','toxoplasma_ig_m',
            'rubella_ig_g','rubella_ig_m','anti_cmv_ig_g','anti_cmv_ig_m','anti_hsv2_ig_g','anti_hsv2_ig_m','tb_ict','tes_mantoux','dengue_ns1','anti_hbsag','chinkungunya_igm','salmonella_igg',
            'salmonella_igm','serum_iron','ca_125','leptospora_igm','tpha','hbsag','igm_anti_salmonella_typhi','anti_hbs_titer','urin_rutin','fisis_warna','kejernihan','bau','kimia_ph','berat_jenis',
            'protein','glukosa','urobillinogen','billirudin','keton','lekosit_esterase','nitrit','blood','sedimen_epitel','lekosit','erytrosit','silinder_granula','silinder_lekosit','kristal',
            'bakteri','trikomonas','candida','silinder_eritrosit','silinder_hyalin','warna','bau','konsistensi','mikroskopis','telur_cacing','amuba','sisa_pencernaan','protein','lemak','karbohidrat',
            'bensidin_test','metode','abstinensia','dikeluarkan_jam','diterima_di_lab_jam','diperiksa_jam','i_makroskopis','warna','liquefaksi','konsistensi','bau','volume','ph','ii_mikroskopis',
            'konsentrasi','motilitas','a_linier_cepat','b_linier_lambat','c_tidak_progressif','d_tidak_motil','viabilitas_(%hidup)','morfologi_(%normal)','morfologi_abnormal','sel_bulat','sel_leukosit',
            'aglutinasi','fruktosa','t3','t4','tsh','ft4','egfr','tshs','cea','afp','psa','cea','administrasi','lancet','spuit_3cc','spuit_5cc','vacutainer','wing_needle','spuit_1cc','spuit_3cc','amphetamine',
            'spuit_3cc','bzo_(benzodizepiner)','thc_(marijuana)','met_(methamphetamine)','tes_kehamilan','rhesus','golongan_darah'
        ];

        $res = [];
        foreach ($forms as $form) {
            if ($this->input->post($form)) {
                $res[$form] = $this->input->post($form);
            }
        }

        return json_encode($res);
    }
}
