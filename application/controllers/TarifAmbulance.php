<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class TarifAmbulance extends Admin_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->load->library('Customlib');
        $this->load->library('template');
        $this->load->model('TarifAmbulanceModel');
        $this->load->model('MainModel');
    }

    public function index()
    {
        $data['list'] = $this->TarifAmbulanceModel->getAll();
        $this->template->view('master/tarif_ambulance/list_tarif_ambulance', $data);
    }

    public function add()
    {
        $this->template->view('master/tarif_ambulance/form_tarif_ambulance');
    }

    public function edit($id)
    {
        $tarif = $this->TarifAmbulanceModel->getById($id);
        $this->template->view('master/tarif_ambulance/form_tarif_ambulance', compact('tarif'));
    }

    public function insert()
    {
        $i = $this->MainModel->insert('tarif_ambulance', [
            'tujuan' => $this->input->post('tujuan'),
            'tarif' => $this->input->post('tarif'),
            'tarif_bpjs' => $this->input->post('tarif_bpjs'),
        ]);

        if ($i) {
            $this->session->set_flashdata('success', 'Tambah Tarif ambulance berhasil!');
            redirect('/TarifAmbulance','refresh');
        }
        else {
            $this->session->set_flashdata('warning', 'Tambah Tarif ambulance gagal!');
            redirect('/TarifAmbulance/add','refresh');
        }
    }

    public function update()
    {
        $i = $this->MainModel->update('tarif_ambulance', [
            'tujuan' => $this->input->post('tujuan'),
            'tarif' => $this->input->post('tarif'),
            'tarif_bpjs' => $this->input->post('tarif_bpjs'),
        ], $this->input->post('id'));

        if ($i) {
            $this->session->set_flashdata('success', 'Edit Tarif ambulance berhasil!');
            redirect('/TarifAmbulance','refresh');
        }
        else {
            $this->session->set_flashdata('warning', 'Edit Tarif ambulance gagal!');
            redirect('/TarifAmbulance/edit/'.$this->input->post('id'),'refresh');
        }
    }

    public function delete($id)
    {
        $this->MainModel->update('tarif_ambulance', ['is_active' => 0], $id);
        $this->session->set_flashdata('success', 'Hapus Tarif ambulance berhasil!');
        redirect('/TarifAmbulance','refresh');
    }
}
