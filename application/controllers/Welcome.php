<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use LZCompressor\LZString;

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

    protected $salt = 'smart-clinic-and-hospital';
    protected $cons_id = 28730;
    protected $secret_key = '0lS92D5882';
    protected $user_key = 'fe5fbae1ba7948d99170921bf17f6907';
    protected $user_key_vclaim = 'd43504683bb350c66b90a4974d5ee8d0';
    protected $timestamp = "";

    public function index()
	{
        $cURLConnection = curl_init();

        curl_setopt($cURLConnection, CURLOPT_URL, 'https://dvlp.bpjs-kesehatan.go.id:8888/aplicaresws/rest/ref/kelas');
        curl_setopt($cURLConnection, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($cURLConnection, CURLOPT_HTTPHEADER, $this->signature());

        $phoneList = curl_exec($cURLConnection);
        $err = curl_error($cURLConnection);
        curl_close($cURLConnection);

        die("res: $phoneList; err: $err");


//        $bu = 'https://dvlp.bpjs-kesehatan.go.id:8888/aplicaresws/';
//        $r = $this->get("{$bu}rest/ref/kelas");
//        die(json_encode($r));

		$this->load->view('welcome_message');
	}

    public function sgn() {
        die(json_encode($this->signature()));
    }

    protected function signature()
    {
        $cons_id = $this->cons_id;
        $secret_key = $this->secret_key;
        $user_key = $this->user_key_vclaim;

        date_default_timezone_set('UTC');
        $this->timestamp = strval(time()-strtotime('1970-01-01 00:00:00'));
        $tStamp = $this->timestamp;
        $signature = hash_hmac('sha256', $cons_id."&".$tStamp, $secret_key, true);
        $encodedSignature = base64_encode($signature);
        date_default_timezone_set('Asia/Jakarta');

        return [
            "X-cons-id: {$cons_id}",
            "X-timestamp: {$tStamp}",
            "X-signature: {$encodedSignature}",
            "user_key: {$user_key}",
        ];
    }

    function stringDecrypt($string)
    {
        $tStamp = $this->timestamp;
        $key = "{$this->cons_id}{$this->secret_key}{$tStamp}";

        $encrypt_method = 'AES-256-CBC';
        $key_hash = hex2bin(hash('sha256', $key));
        $iv = substr(hex2bin(hash('sha256', $key)), 0, 16);

        return openssl_decrypt(base64_decode($string), $encrypt_method, $key_hash, OPENSSL_RAW_DATA, $iv);
    }

    function decompress($string)
    {
        return LZString::decompressFromEncodedURIComponent($string);
    }

    protected function get($url)
    {
//        $header = $this->signature();

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $server_output = curl_exec ($ch);
        $err = curl_error($ch);
        curl_close ($ch);
        die(json_encode([
            $server_output,
            $err,
            $url
        ]));

        $server_output = json_decode($server_output);
        return $server_output;
//        if ($server_output->metaData->code == 200) {
//            $response = $server_output->response;
//            $decrypted = $this->stringDecrypt($response);
//            $decompressed = $this->decompress($decrypted);
//            return (object) [
//                'success' => true,
//                'data' => json_decode($decompressed)
//            ];
//        }
//        else {
//            return (object) [
//                'success' => false,
//                'server_output' => $server_output
//            ];
//        }
    }

}
