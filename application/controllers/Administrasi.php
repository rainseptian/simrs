<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Administrasi extends MY_Controller {

    private $obj = [
        "id" => "", "pendaftaran_id" => "", "dokter_id" => "", "pasien_id" => "", "perawat_id" => "",
        "apoteker_id" => "", "waktu_pemeriksaan" => "", "no_rm" => "", "nama_pasien" => "",
        "td" => "-", "n" => "-", "r" => "-", "s" => "-", "bb" => "", "tb" => "", "bmi" => "",
        "keluhan_utama" => "", "amammesia" => "", "diagnosis" => "", "pemeriksaan_fisik" => "",
        "hasil_penunjang" => "", "diagnosis_jenis_penyakit" => "", "diagnosis_banding" => "",
        "tindakan" => "", "deskripsi_tindakan" => "", "saran_pemeriksaan" => "", "diagnosa_perawat" => "",
        "asuhan_keperawatan" => "", "penanggungjawab" => "", "catatan_odontogram" => "",
        "meta" => "", "status" => "", "is_bpjs" => "", "jaminan" => "", "hasil_lab" => "",
        "is_active" => "", "created_at" => "", "updated_at" => "", "creator" => "", "jk" => "",
        "usia" => "", "nama_dokter" => ""
    ];

    public function __construct() {
        parent::__construct();

        $this->load->library('template');
        $this->load->Model('AdministrasiModel');
        $this->load->Model('PemeriksaanModel');
        $this->load->Model('MainModel');
        $this->load->Model('KlinikModel');
        $this->load->Model('ObatLuarModel');

        $this->load->Model('DetailObatPemeriksaanModel');
        $this->load->Model('DetailObatRacikanPemeriksaanModel');
        $this->load->Model('DetailBahanPemeriksaanModel');
        $this->load->Model('ObatModel');
        $this->load->Model('BahanHabisPakaiModel');
        $this->load->Model('PasienModel');
        $this->load->Model('RawatInapModel');
        $this->load->Model('RuangBersalinModel');
        $this->load->Model('RuangOperasiModel');
        $this->load->Model('PerinatologiModel');
        $this->load->Model('TarifTindakanModel');

        $this->load->helper(array('file', 'php_with_mpdf_helper'));
        $this->load->helper(array('file', 'mpdf'));
    }

    public function index() {
        redirect('Administrasi/listPasienSelesaiPeriksa');
    }

    public function listPasienSelesaiPeriksa() {
        redirect('Administrasi/kasir_all');
//        $data['listPemeriksaan'] = $this->PemeriksaanModel->getListPemeriksaanForKasir()->result();
//        $data['tindakan'] = $this->AdministrasiModel->getTindakandetail();
//        $data['obat'] = $this->AdministrasiModel->getObatPemeriksaandetail();
//        $data['racikan'] = $this->AdministrasiModel->getRacikanPemeriksaan();
//        $data['penyakit'] = $this->AdministrasiModel->getPenyakitPemeriksaandetail();
//        $data['jaminan'] = $this->config->item('pendaftaran');
//        $data['obat_luar'] = $this->ObatLuarModel->getDetailPenjualanObatLuar();
//        $data['racikan_luar'] = $this->ObatLuarModel->getDetailPenjualanObatRacikanLuar();
//
//        foreach ($this->ObatLuarModel->getPenjualanSudahObat() as $v) {
//            $o = $this->obj;
//
//            if ($v->tipe == 'resep_luar') $nama = $v->nama_pasien;
//            else if ($v->tipe == 'obat_internal') $nama = $v->nama_karyawan;
//            else $nama = '';
//
//            $o['obat_luar'] = true;
//            $o['no_rm'] = 'Penj. Obat';
//            $o['jaminan'] = $v->tipe;
//            $o['status'] = $v->progress;
//            $o['nama_pasien'] = $nama;
//            $o['nama_dokter'] = $v->nama_dokter ? $v->nama_dokter : '';
//            $o['id'] = $v->id;
//
//            $data['listPemeriksaan'][] = (object) $o;
//        }
//
//        $this->template->view('administrasi/list_pasien_selesai_v', $data);
    }

    public function kasir_all() {
        $data['listPemeriksaan'] = $this->PemeriksaanModel->getListPemeriksaanForKasirAll()->result();
        $data['jaminan'] = $this->config->item('pendaftaran');
        $data['jaminan']['-'] = [
            'class' => 'label-default',
            'label' => '&nbsp;&nbsp;-&nbsp;&nbsp;'
        ];

        foreach ($this->ObatLuarModel->getPenjualanSudahObat() as $v) {
            $o = $this->obj;

            if ($v->tipe == 'resep_luar') $nama = $v->nama_pasien;
            else if ($v->tipe == 'obat_internal') $nama = $v->nama_karyawan;
            else $nama = '';

            $o['obat_luar'] = true;
            $o['no_rm'] = 'Penj. Obat';
            $o['jaminan'] = $v->tipe;
            $o['status'] = $v->progress;
            $o['nama_pasien'] = $nama;
            $o['nama_dokter'] = $v->nama_dokter ? $v->nama_dokter : '';
            $o['id'] = $v->id;

            $data['listPemeriksaan'][] = (object) $o;
        }

        $this->template->view('administrasi/kasir_all', $data);
    }

    public function get_tindakan($id, $jenis) {
        if ($jenis == 'Poli') {
            $pemeriksaan = $this->AdministrasiModel->getPemeriksaanById($id)->row();
            if ($pemeriksaan->jenis_pendaftaran_id == '19') {
                $tindakan = $this->TarifTindakanModel->getListLab()->result();
            }
            else {
                $tindakan = $this->TarifTindakanModel->getListAll()->result();
            }
        }
        else {
            if ($jenis == 'Ranap')
                $tindakan = $this->TarifTindakanModel->getListRawatInap()->result();
            else if ($jenis == 'VK')
                $tindakan = $this->TarifTindakanModel->getListRuangBersalin()->result();
            else if ($jenis == 'OK')
                $tindakan = $this->TarifTindakanModel->getListRuangOperasi()->result();
            else if ($jenis == 'HCU')
                $tindakan = $this->TarifTindakanModel->getListHCU()->result();
            else if ($jenis == 'Perinatologi')
                $tindakan = $this->TarifTindakanModel->getListPerinatologi()->result();
        }

        echo json_encode($tindakan);
    }

    public function add_tindakan($id, $jenis) {
        $session = $this->session->userdata('logged_in');
        if ($jenis == 'Poli') {
            $pemeriksaan = $this->AdministrasiModel->getPemeriksaanById($id)->row();
            if ($pemeriksaan->jenis_pendaftaran_id == '19') {
                foreach ($this->input->post('tindakan') as $v) {
                    $this->MainModel->insert('detail_tindakan_pemeriksaan_lab', [
                        'pemeriksaan_id' => $id,
                        'jenis_layanan_id' => $v,
                        'creator' => $session->id
                    ]);
                }
            }
            else {
                foreach ($this->input->post('tindakan') as $v) {
                    $input_perawat = explode(',', $pemeriksaan->detail_perawat_id);
                    $tarif_perawat = $this->PemeriksaanModel->getTarifTindakanById($v)->tarif_perawat;
                    $this->MainModel->insert('detail_tindakan_pemeriksaan', [
                        'pemeriksaan_id' => $id,
                        'tarif_tindakan_id' => $v,
                        'jumlah_perawat' => count($input_perawat),
                        'perawat' => implode(',', $input_perawat),
                        'tarif_per_perawat' => $tarif_perawat / count($input_perawat),
                        'creator' => $session->id
                    ]);
                }
            }
        }
        else {
            $tindakan_id = $this->input->post('tindakan');
            $perawat = [];

            if ($jenis == 'Ranap') {
                $transfer_id = $this->RawatInapModel->getById($id)->transfer_id;
                $harga = $this->RawatInapModel->getTindakanHargaByIds($tindakan_id);

                $total_harga = 0;
                foreach ($harga as $h) {
                    $total_harga += $h->tarif_pasien;
                }

                $biaya = [
                    'rawat_inap_id' => $id,
                    'transfer_id' => $transfer_id,
                    'jenis_ruangan_id' => 1,
                    'jenis_biaya' => 'Tindakan',
                    'biaya' => $total_harga,
                ];
                $biaya_id = $this->MainModel->insert_id('ri_biaya', $biaya);

                foreach ($tindakan_id as $id) {
                    $tarif_perawat = $this->PemeriksaanModel->getTarifTindakanById($id)->tarif_perawat;
                    $tarif_per_perawat = $tarif_perawat / count($perawat);
                    $this->MainModel->insert('ri_biaya_tindakan', [
                        'ri_biaya_id' => $biaya_id,
                        'tindakan_id' => $id,
                        'jumlah_perawat' => count($perawat),
                        'perawat' => implode(',', $perawat),
                        'tarif_per_perawat' => is_nan($tarif_per_perawat) || is_infinite($tarif_per_perawat) ? 0 : $tarif_per_perawat,
                        'tipe_tindakan' => 'asli',
                        'perawat_tamu' => ''
                    ]);
                }
            }
            else {
                $transfer = $this->db->query("SELECT * FROM transfer WHERE id = $id")->row();
                $jenis_ruangan_id = $this->RawatInapModel->getJenisRuanganByNamaOrAlias($jenis)->id;
                $harga = $this->RuangBersalinModel->getTindakanHargaByIds($tindakan_id);

                $total_harga = 0;
                foreach ($harga as $h) {
                    $total_harga += $h->tarif_pasien;
                }

                $biaya = [
                    'rawat_inap_id' => $transfer->rawat_inap_id,
                    'transfer_id' => $id,
                    'jenis_ruangan_id' => $jenis_ruangan_id,
                    'jenis_biaya' => 'Tindakan',
                    'biaya' => $total_harga,
                ];
                $biaya_id = $this->MainModel->insert_id('ri_biaya', $biaya);

                foreach ($tindakan_id as $id) {
                    $tarif_perawat = $this->PemeriksaanModel->getTarifTindakanById($id)->tarif_perawat;
                    $tarif_per_perawat = $tarif_perawat / count($perawat);
                    $this->MainModel->insert('ri_biaya_tindakan', [
                        'ri_biaya_id' => $biaya_id,
                        'tindakan_id' => $id,
                        'jumlah_perawat' => count($perawat),
                        'perawat' => implode(',', $perawat),
                        'tarif_per_perawat' => is_infinite($tarif_per_perawat) || is_nan($tarif_per_perawat) ? 0 : $tarif_per_perawat,
                        'tipe_tindakan' => 'asli',
                        'perawat_tamu' => ''
                    ]);
                }
            }
        }

        redirect("Administrasi/kasir_all");
    }

    public function listPasienSelesaiPeriksa_sakit() {
        $data['listPemeriksaan'] = $this->PemeriksaanModel->getListPemeriksaan_sakit();
        $this->template->view('administrasi/list_pasien_selesai_sakit_v', $data);
    }

    public function listPasienSelesai_sehat() {
        $data['listPemeriksaan'] = $this->PemeriksaanModel->getListPemeriksaan_sehat();
        $this->template->view('administrasi/list_pasien_sehat_v', $data);
    }

    public function listPasienSelesaiPeriksa_rujuk() {
        $data['listPemeriksaan'] = $this->PemeriksaanModel->getListPemeriksaan_rujuk();
        $this->template->view('administrasi/list_pasien_rujuk_v', $data);
    }

    public function listPasienSelesaiPeriksa_ranap() {
        $data['listPemeriksaan'] = $this->PemeriksaanModel->getListPemeriksaan_ranap();
        $this->template->view('administrasi/list_pasien_ranap_v', $data);
    }

    public function pemberianObat($id) {
        $data['pemeriksaan'] = $this->AdministrasiModel->getPemeriksaanById($id);
        $this->template->view('administrasi/pemberian_obat_v', $data);
    }

    public function nota($id) {
        $data['pemeriksaan'] = $this->AdministrasiModel->getPemeriksaanById($id)->row();
        $data['obat'] = $this->AdministrasiModel->getObatPemeriksaanById($id);
        $data['bahan'] = $this->AdministrasiModel->getBahanHabisPakaiPemeriksaanById($id);

        if ($data['pemeriksaan']->jenis_pendaftaran_id == '19') {
            $data['tindakan'] = $this->AdministrasiModel->getTindakanLabByIdPemeriksaan($id);
        }
        else {
            $data['tindakan'] = $this->AdministrasiModel->getTindakanById($id);

            if ($data['pemeriksaan']->jenis_pendaftaran_id == '58') {
                $data['konsul'] = $this->AdministrasiModel->getTindakanKonsulIGDByIdPemeriksaan($id);
            }
        }

        $racikans = $this->AdministrasiModel->getListRacikanByPemeriksaanId($id)->result();
        foreach ($racikans as $k => $v) {
            $racikans[$k]->obat = $this->AdministrasiModel->getListObatByDetailObatRacikanPemeriksaanId($v->id)->result();
            foreach ($racikans[$k]->obat as $kk => $vv) {
                $racikans[$k]->total += $vv->jumlah_satuan * $vv->harga_jual;
            }
        }
        $data['racikan'] = $racikans;

        $this->template->view('administrasi/nota_v', $data);
    }

    private function calculateTotal($detail)
    {
        $total_tindakan = 0;
        $total_obat = 0;
        $total_obatracik = 0;
        $total_bahan = 0;
        $total_makanan = 0;
        $total_biaya_bed = 0;

        if (!function_exists('getHowManyDays')) {
            function getHowManyDays($start, $end, $add)
            {
                $d1 = date_create(date('Y-m-d', strtotime($start)));
                $d2 = date_create(date('Y-m-d', strtotime($end)));
                $waktu = (int)date_diff($d1, $d2)->format("%a");
                if ($add || $waktu == 0) {
                    $waktu++;
                }
                return $waktu;
            }
        }

        foreach ($detail['biaya'] as $biaya) {
            if ($biaya->jenis_biaya == 'Tindakan') {
                foreach ($biaya->tindakan as $tindakan) {
                    $total_tindakan += $tindakan->tarif_pasien;
                }
            }
        }
        foreach ($detail['resep'] as $resep) {
            foreach ($resep->obat as $obat) {
                $total_obat += $obat->harga_jual * $obat->jumlah;
            }
            foreach ($resep->obat_racik as $or) {
                $total_obatracik += $or->total;
            }
        }
        foreach ($detail['bahan_habis_pakai'] as $bhp) {
            foreach ($bhp->bahan as $bahan) {
                $total_bahan += $bahan->harga_jual * $bahan->jumlah;
            }
        }
        foreach ($detail['makanan_count_list'] as $m) {
            $total_makanan += $m->total;
        }

        if ($detail['beds']) {
            foreach ($detail['beds'] as $k => $v) {
                $count = getHowManyDays(
                    $v['start'],
                    $v['end'] == 'now' ? date("Y-m-d H:i") : $v['end'],
                    $k == 0
                );
                $jml = $v['bed']['group_price'] * $count;
                $total_biaya_bed += $jml;
            }
        }

        return [
            'total_tindakan' => $total_tindakan,
            'total_obat' => $total_obat,
            'total_obatracik' => $total_obatracik,
            'total_bahan' => $total_bahan,
            'total_makanan' => $total_makanan,
            'total_biaya_bed' => $total_biaya_bed,
        ];
    }

    public function nota_all($pasien_id, $jenis, $id, $bayar_all_id =  false) {
        $data['jenis'] = $jenis;
        $data['id'] = $id;
        $jenises = explode('%5E%5E%5E', $jenis);
        $ids = explode('%5E%5E%5E', $id);

        $total_tindakan = 0;
        $total_obat = 0;
        $total_obatracik = 0;
        $total_bahan = 0;
        $total_makanan = 0;
        $total_biaya_bed = 0;

        $detailPoli = function ($id, $title) use ($bayar_all_id) {
            $data['pemeriksaan'] = $this->AdministrasiModel->getPemeriksaanById($id)->row();
            $data['obat'] = $this->AdministrasiModel->getObatPemeriksaanById($id);
            $data['bahan'] = $this->AdministrasiModel->getBahanHabisPakaiPemeriksaanById($id);

            if ($data['pemeriksaan']->jenis_pendaftaran_id == '19') {
                $data['tindakan'] = $this->AdministrasiModel->getTindakanLabByIdPemeriksaan($id);
            }
            else {
                $data['tindakan'] = $this->AdministrasiModel->getTindakanById($id);

                if ($data['pemeriksaan']->jenis_pendaftaran_id == '58') {
                    $data['konsul'] = $this->AdministrasiModel->getTindakanKonsulIGDByIdPemeriksaan($id);
                }
            }

            $racikans = $this->AdministrasiModel->getListRacikanByPemeriksaanId($id)->result();
            foreach ($racikans as $k => $v) {
                $racikans[$k]->obat = $this->AdministrasiModel->getListObatByDetailObatRacikanPemeriksaanId($v->id)->result();
                foreach ($racikans[$k]->obat as $kk => $vv) {
                    $racikans[$k]->total += $vv->jumlah_satuan * $vv->harga_jual;
                }
            }
            $data['racikan'] = $racikans;
            $data['title'] = str_replace('%20', ' ', $title);
            $data['pembayaran_result'] = $bayar_all_id ? $this->db->query("SELECT * FROM bayar WHERE bayar_all_id = {$bayar_all_id} AND pemeriksaan_id = {$id}")->row() : '';
            $data['jasa_racik'] = $data['pemeriksaan']->jasa_racik;

            return $data;
        };

        $detailRanap = function ($id, $bayar_all_id = false) use (&$total_tindakan, &$total_obat, &$total_obatracik, &$total_bahan, &$total_makanan, &$total_biaya_bed) {
            $data['jasa_racik_name'] = 'jasa_racik_ri_'.$id;
            $data['rawat_inap'] = $this->RawatInapModel->getById($id);
            $ri_id = $data['rawat_inap']->id;

            $data['bayar'] = $bayar_all_id ? $this->RawatInapModel->getBayarByRawatInapId($ri_id) : '';
            $data['pemeriksaan'] = $this->RawatInapModel->getPemeriksaanByRawatInapId($ri_id);
            $data['diagnosis'] = $this->RawatInapModel->getDiagnosisByRawatInapId($ri_id);
            foreach ($data['diagnosis'] as &$d) {
                $d->penyakit = $this->RawatInapModel->getPenyakitByDiagnosisId($d->id);
            }

            $data['jasa_racik'] = 0;
            $data['resep'] = $this->RawatInapModel->getResepByRawatInapId($ri_id);
            foreach ($data['resep'] as &$d) {
                $data['jasa_racik'] += $d->jasa_racik;
                $d->obat = $this->RawatInapModel->getObatByResepId($d->id);
                $d->obat_racik = $this->RawatInapModel->getObatRacikByResepId($d->id);
                foreach ($d->obat_racik as &$dd) {
                    $dd->detail = $this->RawatInapModel->getObatRacikDetailByResepObatRacikId($dd->id);
                    $dd->total = 0;
                    foreach ($dd->detail as $ddd) {
                        $dd->total += $ddd->jumlah * $ddd->harga_jual;
                    }
                }
            }

            $data['bahan_habis_pakai'] = $this->RawatInapModel->getBhpByRawatInapId($ri_id);
            foreach ($data['bahan_habis_pakai'] as &$d) {
                $d->bahan = $this->RawatInapModel->getBahanByBhpId($d->id);
            }

            $data['biaya'] = $this->RawatInapModel->getBiayaByRawatInapId($ri_id);
            foreach ($data['biaya'] as &$d) {
                if ($d->jenis_biaya == 'Makanan') {
                    $d->makanan = $this->RawatInapModel->getMakananById($d->makanan_id);
                }
                else if ($d->jenis_biaya == 'Tindakan') {
                    $d->tindakan = $this->RawatInapModel->getTindakanByBiayaId($d->id);
                }
            }
            $data['makanan_count_list'] = $this->RawatInapModel->getMakananCountByRawatInapId($ri_id);

            $tfs = $this->RawatInapModel->getAllBangsalTransfersByRawatInapId($ri_id);
            $bed = [];
            foreach ($tfs as $k => $v) {
                $bed[] = [
                    'start' => $v->transfered_at,
                    'end' => $k == count($tfs) - 1 ? $v->tgl_boleh_pulang : $tfs[$k + 1]->transfered_at,
                    'bed' => $this->bed_model->bed_listsearch($v->tujuan)
                ];
            }
            $data['beds'] = $bed;
            $data['title'] = 'Rawat Inap';
            $data['suffix'] = 'Ranap';
            $data['sudah_bayar'] = $bayar_all_id ? true : false;

            $calc = $this->calculateTotal($data);
            $total_tindakan += $calc['total_tindakan'];
            $total_obat += $calc['total_obat'];
            $total_obatracik += $calc['total_obatracik'];
            $total_bahan += $calc['total_bahan'];
            $total_makanan += $calc['total_makanan'];
            $total_biaya_bed += $calc['total_biaya_bed'];

            return $data;
        };

        $detailRanapLike = function ($transfer_id, $jenis_ruangan_id, $title, $suffix, $bayar_all_id = false) use (&$total_tindakan, &$total_obat, &$total_obatracik, &$total_bahan, &$total_makanan, &$total_biaya_bed) {
            $data['jasa_racik_name'] = 'jasa_racik_transfer_'.$transfer_id;
            $data['bayar'] = $bayar_all_id ? $this->RuangBersalinModel->getBayarByTransferId($transfer_id, $jenis_ruangan_id) : '';
            $data['pemeriksaan'] = $this->RuangBersalinModel->getPemeriksaanByTransferId($transfer_id, $jenis_ruangan_id);
            $data['diagnosis'] = $this->RuangBersalinModel->getDiagnosisByTransferId($transfer_id, $jenis_ruangan_id);
            foreach ($data['diagnosis'] as &$d) {
                $d->penyakit = $this->RuangBersalinModel->getPenyakitByDiagnosisId($d->id);
            }

            $data['jasa_racik'] = 0;
            $data['resep'] = $this->RuangBersalinModel->getResepByTransferId($transfer_id, $jenis_ruangan_id);
            foreach ($data['resep'] as &$d) {
                $data['jasa_racik'] += $d->jasa_racik;
                $d->obat = $this->RuangBersalinModel->getObatByResepId($d->id);
                $d->obat_racik = $this->RuangBersalinModel->getObatRacikByResepId($d->id);
                foreach ($d->obat_racik as &$dd) {
                    $dd->detail = $this->RuangBersalinModel->getObatRacikDetailByResepObatRacikId($dd->id);
                    $dd->total = 0;
                    foreach ($dd->detail as $ddd) {
                        $dd->total += $ddd->jumlah * $ddd->harga_jual;
                    }
                }
            }

            $data['bahan_habis_pakai'] = $this->RuangBersalinModel->getBhpByTransferId($transfer_id, $jenis_ruangan_id);
            foreach ($data['bahan_habis_pakai'] as &$d) {
                $d->bahan = $this->RuangBersalinModel->getBahanByBhpId($d->id);
            }

            $data['biaya'] = $this->RuangBersalinModel->getBiayaByTransferId($transfer_id, $jenis_ruangan_id);
            foreach ($data['biaya'] as &$d) {
                if ($d->jenis_biaya == 'Makanan') {
                    $d->makanan = $this->RuangBersalinModel->getMakananById($d->makanan_id);
                }
                else if ($d->jenis_biaya == 'Tindakan') {
                    $d->tindakan = $this->RuangBersalinModel->getTindakanByBiayaId($d->id);
                }
            }
            $data['makanan_count_list'] = $this->RuangBersalinModel->getMakananCountByTransferId($transfer_id, $jenis_ruangan_id);

            if ($jenis_ruangan_id == 4 || $jenis_ruangan_id == 5) {
                $tfs = [$this->PerinatologiModel->getTransferById($transfer_id)];
                $bed = [];
                foreach ($tfs as $k => $v) {
                    $bed[] = [
                        'start' => $v->transfered_at,
                        'end' => $k == count($tfs) - 1 ? $v->diperiksa_at : $tfs[$k + 1]->transfered_at,
                        'bed' => $this->bed_model->bed_listsearch($v->tujuan)
                    ];
                }
                $data['beds'] = $bed;
            }

            $data['title'] = $title;
            $data['suffix'] = $suffix;
            $data['sudah_bayar'] = $bayar_all_id ? true : false;

            $calc = $this->calculateTotal($data);
            $total_tindakan += $calc['total_tindakan'];
            $total_obat += $calc['total_obat'];
            $total_obatracik += $calc['total_obatracik'];
            $total_bahan += $calc['total_bahan'];
            $total_makanan += $calc['total_makanan'];
            $total_biaya_bed += $calc['total_biaya_bed'];

            return $data;
        };

        foreach ($jenises as $k => $j) {
            $id = $ids[$k];

            if (substr($j, 0, 4 ) === "Poli") {
                $detail = $detailPoli($id, $j);
                $data['jenis_pendaftaran'] = $this->AdministrasiModel->getPemeriksaanById($id)->row()->jaminan;

                if (isset($detail['konsul'])) {
                    foreach ($detail['konsul']->result() as $tindakan) {
                        $total_tindakan += $tindakan->tarif_pasien;
                    }
                }
                foreach ($detail['tindakan']->result() as $tindakan) {
                    $total_tindakan += $tindakan->tarif_pasien;
                }
                foreach ($detail['obat']->result() as $obat) {
                    $total_obat += $obat->harga_jual * $obat->jumlah_satuan;
                }
                foreach ($detail['racikan'] as $r) {
                    foreach ($r->obat as $obat) {
                        $total_obatracik += $obat->harga_jual * $obat->jumlah_satuan;
                    }
                }
                foreach ($detail['bahan']->result() as $bahan) {
                    $total_bahan += $bahan->harga_jual * $bahan->jumlah;
                }
            }
            else if ($j == 'Ambulance') {
                $detail = [
                    'title' => 'Ambulance',
                    'ambulance' => $this->db->query("
                        SELECT * FROM panggilan_ambulance pa
                        LEFT JOIN vehicles v ON pa.vehicle_id = v.id
                        LEFT JOIN tarif_ambulance ta on pa.tarif_ambulance_id = ta.id
                        WHERE pa.id = $id
                    ")->row()
                ];
                $jp = $detail['ambulance']->jaminan;
                $data['jenis_pendaftaran'] = $jp ?: $data['jenis_pendaftaran'];
                $detail['tarif'] = $data['jenis_pendaftaran'] == 'bpjs' ? $detail['ambulance']->tarif_bpjs : $detail['ambulance']->tarif;
            }
            else if ($j == 'Ranap') {
                $detail = $detailRanap($id, $bayar_all_id);
                $jp = $this->RawatInapModel->getById($id)->tipe_pasien;
                $data['jenis_pendaftaran'] = $jp ?: $data['jenis_pendaftaran'];
            }
            else {
                $jenis_ruangan_id = $this->RawatInapModel->getJenisRuanganByNamaOrAlias($j)->id;
                $title = $j == 'VK' ? 'Ruang Bersalin' : ($j == 'OK' ? 'Ruang Operasi' : $j);
                $detail = $detailRanapLike($id, $jenis_ruangan_id, $title, $j, $bayar_all_id);
                $jp = $this->RuangOperasiModel->getTransferById($id)->tipe_pasien;
                $data['jenis_pendaftaran'] = $jp ?: $data['jenis_pendaftaran'];
            }

            if (substr($j, 0, 4 ) === "Poli") {
                $data['Poli'][str_replace('%20', ' ', $j)][] = [
                    'id' => $ids[$k],
                    'detail' => $detail
                ];
            }
            else if ($j == 'Ambulance') {
                $data['Ambulance'][$j][] = [
                    'id' => $ids[$k],
                    'detail' => $detail
                ];
            }
            else {
                $data['Ruang'][$j][] = [
                    'id' => $ids[$k],
                    'detail' => $detail
                ];
            }
        }

        $data['pasien'] = $this->PasienModel->getPasienById($pasien_id)->row();
        $data['total_tindakan'] = $total_tindakan;
        $data['total_obat'] = $total_obat;
        $data['total_obatracik'] = $total_obatracik;
        $data['total_bahan'] = $total_bahan;
        $data['total_makanan'] = $total_makanan;
        $data['total_biaya_bed'] = $total_biaya_bed;
        $data['sudah_bayar'] = $bayar_all_id ? true : false;
        $data['bayar'] = $bayar_all_id ? $this->MainModel->select('bayar_all', ['id', $bayar_all_id])->row() : '';

        if ($bayar_all_id) {
            $this->session->set_flashdata('success', 'Pembayaran berhasil');
        }

        $this->template->view('administrasi/nota_all', $data);
    }

    public function submit_nota_all()
    {
        $jenises = explode('%5E%5E%5E', $this->input->post('jenis'));
        $ids = explode('%5E%5E%5E', $this->input->post('id'));

        $bayar_all_id = $this->MainModel->insert_id('bayar_all', [
            'jasa_racik' => $this->input->post('total_jasa_racik'),
            'diskon' => $this->input->post('diskon'),
            'total' => $this->input->post('total_input'),
            'bayar' => $this->input->post('bayar'),
            'kembalian' => $this->input->post('kembalian_input'),
        ]);

        foreach ($jenises as $k => $jenis) {
            $id = $ids[$k];

            if (substr($jenis, 0, 4 ) === "Poli") {
                $session = $this->session->userdata('logged_in');
                $id_bayar = $this->MainModel->insert_id('bayar', [
                    'pemeriksaan_id' => $id,
                    'jasa_racik' => $this->input->post('jasa_racik_' . $id),
                    'bayar_all_id' => $bayar_all_id,
                    'creator' => $session->id
                ]);

                $this->insert_detail_bayar($id_bayar, $session, $id);
                $this->updateStock($id);

                $pemeriksaan = $this->MainModel->select('pemeriksaan', ['id', $id])->row();
                $pendaftaran = $this->MainModel->select('pendaftaran_pasien', ['id', $pemeriksaan->pendaftaran_id])->row();
                $jp_id = $pendaftaran->jenis_pendaftaran_id;

                $st = ($jp_id == 19 || $jp_id == 59) ? 'selesai' : 'sudah_bayar';
                $this->MainModel->update('pemeriksaan', ['status' => $st], $id);
            }
            else if ($jenis == 'Ambulance') {
                $this->MainModel->insert_id('bayar_ambulance', [
                    'bayar_all_id' => $bayar_all_id,
                    'panggilan_id' => $id,
                    'tarif' => $this->input->post('biaya_ambulance'),
                    'diskon' => 0,
                    'total' => $this->input->post('biaya_ambulance'),
                    'bayar' => $this->input->post('biaya_ambulance'),
                    'kembalian' => 0,
                ]);
                $this->MainModel->update('panggilan_ambulance', ['status' => 'selesai'], $id);
            }
            else {
                if ($jenis == 'Ranap') {
                    $ri_id = $id;
                    $ri = $this->RawatInapModel->getById($ri_id);
                    $transfer_id = $ri->transfer_id;
                }
                else {
                    $transfer_id = $id;
                    $ri_id = $this->RuangBersalinModel->getTransferById($transfer_id)->rawat_inap_id;
                }

                $jenis_ruangan_id = $this->RuangBersalinModel->getTransferById($transfer_id)->jenis_ruangan_id;
                $bayar = array(
                    'rawat_inap_id' => $ri_id,
                    'transfer_id' => $transfer_id,
                    'jenis_ruangan_id' => $jenis_ruangan_id,
                    'jasa_racik' => $jenis == 'Ranap' ? $this->input->post('jasa_racik_ri_'.$ri_id) : $this->input->post('jasa_racik_transfer_'.$transfer_id),
                    'bayar_all_id' => $bayar_all_id,
                );
                $id_bayar = $this->MainModel->insert_id('ri_bayar', $bayar);

                $this->submit_bayar_ranap_like($jenis, $jenis_ruangan_id, $id_bayar, $ri_id, $transfer_id);
            }
        }

        $jenis = str_replace('%5E%5E%5E', '^^^', $this->input->post('jenis'));
        $id = str_replace('%5E%5E%5E', '^^^', $this->input->post('id'));
        $pasien_id = $this->input->post('pasien_id');

        redirect("Administrasi/nota_all/$pasien_id/$jenis/$id/$bayar_all_id");
    }

    private function insert_detail_bayar($id_bayar, $session, $pemeriksaan_id = -1)
    {
        // tindakan
        $pemeriksaan = $this->AdministrasiModel->getPemeriksaanById($pemeriksaan_id)->row();
        $tindakan = $this->AdministrasiModel->getTindakanById($pemeriksaan_id)->result();
        if ($pemeriksaan->jenis_pendaftaran_id == '58') {
            $tindakan = array_merge($tindakan, $this->AdministrasiModel->getTindakanKonsulIGDByIdPemeriksaan($pemeriksaan_id)->result());
        }
        if ($pemeriksaan->jenis_pendaftaran_id == '19') {
            $tindakan = $this->AdministrasiModel->getTindakanLabByIdPemeriksaan($pemeriksaan_id)->result();
        }

        foreach ($tindakan as $t) {
            $tindakan = array(
                'bayar_id' => $id_bayar,
                'item_id' => $t->tarif_tindakan_id ?? ($t->jenis_layanan_id ?? null),
                'item' => $t->nama,
                'jenis_item' => 'tindakan',
                'jumlah' => 1,
                'harga' => $t->tarif_pasien,
                'subtotal' => $t->tarif_pasien,
                'creator' => $session->id,
            );
            $this->MainModel->insert('detail_bayar', $tindakan);
        }

        // racikan
        $racikans = $this->AdministrasiModel->getListRacikanByPemeriksaanId($pemeriksaan_id)->result();
        foreach ($racikans as $k => $v) {
            $racikans[$k]->total = 0;
            $racikans[$k]->obat = $this->AdministrasiModel->getListObatByDetailObatRacikanPemeriksaanId($v->id)->result();
            foreach ($racikans[$k]->obat as $kk => $vv) {
                $racikans[$k]->total += $vv->jumlah_satuan * $vv->harga_jual;
            }
        }

        foreach ($racikans as $racikan) {
            $racikan = array(
                'bayar_id' => $id_bayar,
                'item' => $racikan->nama_racikan,
                'jenis_item' => 'obat racikan',
                'jumlah' => 1,
                'harga' => '',
                'subtotal' => $racikan->total,
                'creator' => $session->id,
            );
            $this->MainModel->insert('detail_bayar', $racikan);
        }

        // obat
        $obats = $this->AdministrasiModel->getObatPemeriksaanById($pemeriksaan_id)->result();
        foreach ($obats as $obat) {
            $obat = array(
                'bayar_id' => $id_bayar,
                'item_id' => $obat->obat_id ?? null,
                'item' => $obat->nama,
                'jenis_item' => 'obat',
                'jumlah' => $obat->jumlah_satuan,
                'harga' => $obat->harga_jual,
                'subtotal' => $obat->harga_jual * $obat->jumlah_satuan,
                'creator' => $session->id,
            );
            $this->MainModel->insert('detail_bayar', $obat);
        }

        // bahan
        $bahans = $this->AdministrasiModel->getBahanHabisPakaiPemeriksaanById($pemeriksaan_id)->result();
        foreach ($bahans as $bahan) {
            $bahan = array(
                'bayar_id' => $id_bayar,
                'item_id' => $bahan->bahan_id,
                'item' => $bahan->nama,
                'jenis_item' => 'bahan habis pakai',
                'jumlah' => $bahan->jumlah,
                'harga' => $bahan->harga_jual,
                'subtotal' => $bahan->harga_jual * $bahan->jumlah,
                'creator' => $session->id,
            );
            $this->MainModel->insert('detail_bayar', $bahan);
        }

        // je er
        $jr = $this->input->post('jasa_racik_' . $pemeriksaan_id);
        if ($jr) {
            $jasa_racik = array(
                'bayar_id' => $id_bayar,
                'item' => 'Jasa Racik',
                'jenis_item' => 'jasa racik',
                'jumlah' => 0,
                'harga' => $jr,
                'subtotal' => $jr,
                'creator' => $session->id,
            );
            $this->MainModel->insert('detail_bayar', $jasa_racik);
        }
    }

    private function submit_bayar_ranap_like($jenis, $jenis_ruangan_id, $id_bayar, $ri_id, $transfer_id)
    {
        $is_ranap = $jenis == 'Ranap';

        //tindakan & makanan
        $data['biaya'] = $is_ranap ?
            $this->RawatInapModel->getBiayaByRawatInapId($ri_id) :
            $this->RuangBersalinModel->getBiayaByTransferId($transfer_id, $jenis_ruangan_id);
        foreach ($data['biaya'] as &$d) {
            if ($d->jenis_biaya == 'Makanan') {
                if ($d->makanan_id) {
                    $makanan = $this->RawatInapModel->getMakananById($d->makanan_id);
                    $byr = array(
                        'ri_bayar_id' => $id_bayar,
                        'item_id' => $d->makanan_id,
                        'item' => $makanan->nama,
                        'jenis_item' => 'makanan',
                        'jumlah' => 1,
                        'harga' => $makanan->harga,
                        'subtotal' => $makanan->harga,
                    );
                    $this->MainModel->insert('ri_bayar_detail', $byr);
                }
            }
            else if ($d->jenis_biaya == 'Tindakan') {
                $tindakan = $this->RawatInapModel->getTindakanByBiayaId($d->id);
                foreach ($tindakan as $t) {
                    $byr = array(
                        'ri_bayar_id' => $id_bayar,
                        'item_id' => $t->tindakan_id,
                        'item' => $t->nama,
                        'jenis_item' => 'tindakan',
                        'jumlah' => 1,
                        'harga' => $t->tarif_pasien,
                        'subtotal' => $t->tarif_pasien,
                    );
                    $this->MainModel->insert('ri_bayar_detail', $byr);
                }
            }
        }

        //obat & obat racik
        $data['resep'] = $is_ranap ?
            $this->RawatInapModel->getResepByRawatInapId($ri_id) :
            $this->RuangBersalinModel->getResepByTransferId($transfer_id, $jenis_ruangan_id);
        foreach ($data['resep'] as &$d) {
            $obat = $this->RawatInapModel->getObatByResepId($d->id);
            foreach ($obat as $o) {
                $byr = array(
                    'ri_bayar_id' => $id_bayar,
                    'item_id' => $o->obat_id,
                    'item' => $o->nama,
                    'jenis_item' => 'obat',
                    'jumlah' => $o->jumlah,
                    'harga' => $o->harga_jual,
                    'subtotal' => $o->harga_jual * $o->jumlah,
                );
                $this->MainModel->insert('ri_bayar_detail', $byr);
            }

            $d->obat_racik = $this->RawatInapModel->getObatRacikByResepId($d->id);
            foreach ($d->obat_racik as &$dd) {
                $dd->detail = $this->RawatInapModel->getObatRacikDetailByResepObatRacikId($dd->id);
                $dd->total = 0;
                foreach ($dd->detail as $ddd) {
                    $dd->total += $ddd->jumlah * $ddd->harga_jual;
                }

                $byr = array(
                    'ri_bayar_id' => $id_bayar,
                    'item' => $dd->nama_racikan,
                    'jenis_item' => 'obat racikan',
                    'jumlah' => 1,
                    'harga' => '',
                    'subtotal' => $dd->total
                );
                $this->MainModel->insert('ri_bayar_detail', $byr);
            }
        }

        // bhp
        $data['bahan_habis_pakai'] = $is_ranap ?
            $this->RawatInapModel->getBhpByRawatInapId($ri_id) :
            $this->RuangBersalinModel->getBhpByTransferId($transfer_id, $jenis_ruangan_id);
        foreach ($data['bahan_habis_pakai'] as &$d) {
            $d->bahan = $this->RawatInapModel->getBahanByBhpId($d->id);
            foreach ($d->bahan as $t) {
                $byr = array(
                    'ri_bayar_id' => $id_bayar,
                    'item_id' => $t->bph_id,
                    'item' => $t->nama,
                    'jenis_item' => 'bahan habis pakai',
                    'jumlah' => $t->jumlah,
                    'harga' => $t->harga_jual,
                    'subtotal' => $t->harga_jual * $t->jumlah,
                );
                $this->MainModel->insert('ri_bayar_detail', $byr);
            }
        }

        // jasa racik
        $jr_name = $is_ranap ? 'jasa_racik_ri_' . $ri_id : 'jasa_racik_transfer_' . $transfer_id;
        if ($this->input->post($jr_name)) {
            $jasa_racik = array(
                'ri_bayar_id' => $id_bayar,
                'item' => 'Jasa Racik',
                'jenis_item' => 'jasa racik',
                'jumlah' => 0,
                'harga' => $this->input->post($jr_name),
                'subtotal' => $this->input->post($jr_name),
            );
            $this->MainModel->insert('ri_bayar_detail', $jasa_racik);
        }

        if ($is_ranap) {
            // bed
            $bed = array(
                'ri_bayar_id' => $id_bayar,
                'item' => 'Bed',
                'jenis_item' => 'bed',
                'jumlah' => 0,
                'harga' => $this->input->post('biaya_bed_'.$jenis),
                'subtotal' => $this->input->post('biaya_bed_'.$jenis),
            );
            $this->MainModel->insert('ri_bayar_detail', $bed);

            $this->MainModel->update('rawat_inap', ['status' => 'Selesai', 'tgl_selesai' => date('Y-m-d H:i:s')], $ri_id);
            $this->MainModel->update('bed', ['is_active' => 'yes'], $this->RawatInapModel->getById($ri_id)->tujuan);
        }

        $this->MainModel->update('transfer', ['status' => 'selesai', 'selesai_at' => date('Y-m-d H:i:s')], $transfer_id);
    }

    public function print_nota_all($pasien_id, $jenis, $id, $bayar_all_id =  false)
    {
        $data['jenis'] = $jenis;
        $data['id'] = $id;
        $jenises = explode('%5E%5E%5E', $jenis);
        $ids = explode('%5E%5E%5E', $id);

        $total_tindakan = 0;
        $total_obat = 0;
        $total_obatracik = 0;
        $total_bahan = 0;
        $total_makanan = 0;
        $total_biaya_bed = 0;

        $detailPoli = function ($id, $title) use ($bayar_all_id) {
            $data['pemeriksaan'] = $this->AdministrasiModel->getPemeriksaanById($id)->row();
            $data['obat'] = $this->AdministrasiModel->getObatPemeriksaanById($id);
            $data['bahan'] = $this->AdministrasiModel->getBahanHabisPakaiPemeriksaanById($id);

            if ($data['pemeriksaan']->jenis_pendaftaran_id == '19') {
                $data['tindakan'] = $this->AdministrasiModel->getTindakanLabByIdPemeriksaan($id);
            }
            else {
                $data['tindakan'] = $this->AdministrasiModel->getTindakanById($id);

                if ($data['pemeriksaan']->jenis_pendaftaran_id == '58') {
                    $data['konsul'] = $this->AdministrasiModel->getTindakanKonsulIGDByIdPemeriksaan($id);
                }
            }

            $racikans = $this->AdministrasiModel->getListRacikanByPemeriksaanId($id)->result();
            foreach ($racikans as $k => $v) {
                $racikans[$k]->obat = $this->AdministrasiModel->getListObatByDetailObatRacikanPemeriksaanId($v->id)->result();
                foreach ($racikans[$k]->obat as $kk => $vv) {
                    $racikans[$k]->total += $vv->jumlah_satuan * $vv->harga_jual;
                }
            }
            $data['racikan'] = $racikans;
            $data['title'] = str_replace('%20', ' ', $title);
            $data['pembayaran_result'] = $this->MainModel->select('bayar', ['bayar_all_id', $bayar_all_id])->row();
            $data['jasa_racik'] = $data['pemeriksaan']->jasa_racik;

            return $data;
        };

        $detailRanap = function ($id, $bayar_all_id = false) use (&$total_tindakan, &$total_obat, &$total_obatracik, &$total_bahan, &$total_makanan, &$total_biaya_bed) {
            $data['rawat_inap'] = $this->RawatInapModel->getById($id);
            $ri_id = $data['rawat_inap']->id;

            $data['bayar'] = $bayar_all_id ? $this->RawatInapModel->getBayarByRawatInapId($ri_id) : '';
            $data['pemeriksaan'] = $this->RawatInapModel->getPemeriksaanByRawatInapId($ri_id);
            $data['diagnosis'] = $this->RawatInapModel->getDiagnosisByRawatInapId($ri_id);
            foreach ($data['diagnosis'] as &$d) {
                $d->penyakit = $this->RawatInapModel->getPenyakitByDiagnosisId($d->id);
            }

            $data['resep'] = $this->RawatInapModel->getResepByRawatInapId($ri_id);
            foreach ($data['resep'] as &$d) {
                $d->obat = $this->RawatInapModel->getObatByResepId($d->id);
                $d->obat_racik = $this->RawatInapModel->getObatRacikByResepId($d->id);
                foreach ($d->obat_racik as &$dd) {
                    $dd->detail = $this->RawatInapModel->getObatRacikDetailByResepObatRacikId($dd->id);
                    $dd->total = 0;
                    foreach ($dd->detail as $ddd) {
                        $dd->total += $ddd->jumlah * $ddd->harga_jual;
                    }
                }
            }

            $data['bahan_habis_pakai'] = $this->RawatInapModel->getBhpByRawatInapId($ri_id);
            foreach ($data['bahan_habis_pakai'] as &$d) {
                $d->bahan = $this->RawatInapModel->getBahanByBhpId($d->id);
            }

            $data['biaya'] = $this->RawatInapModel->getBiayaByRawatInapId($ri_id);
            foreach ($data['biaya'] as &$d) {
                if ($d->jenis_biaya == 'Makanan') {
                    $d->makanan = $this->RawatInapModel->getMakananById($d->makanan_id);
                }
                else if ($d->jenis_biaya == 'Tindakan') {
                    $d->tindakan = $this->RawatInapModel->getTindakanByBiayaId($d->id);
                }
            }
            $data['makanan_count_list'] = $this->RawatInapModel->getMakananCountByRawatInapId($ri_id);

            $tfs = $this->RawatInapModel->getAllBangsalTransfersByRawatInapId($ri_id);
            $bed = [];
            foreach ($tfs as $k => $v) {
                $bed[] = [
                    'start' => $v->transfered_at,
                    'end' => $k == count($tfs) - 1 ? $v->tgl_boleh_pulang : $tfs[$k + 1]->transfered_at,
                    'bed' => $this->bed_model->bed_listsearch($v->tujuan)
                ];
            }
            $data['beds'] = $bed;
            $data['title'] = 'Rawat Inap';
            $data['suffix'] = 'Ranap';
            $data['sudah_bayar'] = $bayar_all_id ? true : false;

            $calc = $this->calculateTotal($data);
            $total_tindakan += $calc['total_tindakan'];
            $total_obat += $calc['total_obat'];
            $total_obatracik += $calc['total_obatracik'];
            $total_bahan += $calc['total_bahan'];
            $total_makanan += $calc['total_makanan'];
            $total_biaya_bed += $calc['total_biaya_bed'];

            return $data;
        };

        $detailRanapLike = function ($transfer_id, $jenis_ruangan_id, $title, $suffix, $bayar_all_id = false) use (&$total_tindakan, &$total_obat, &$total_obatracik, &$total_bahan, &$total_makanan, &$total_biaya_bed) {
            $data['bayar'] = $bayar_all_id ? $this->RuangBersalinModel->getBayarByTransferId($transfer_id, $jenis_ruangan_id) : '';
            $data['pemeriksaan'] = $this->RuangBersalinModel->getPemeriksaanByTransferId($transfer_id, $jenis_ruangan_id);
            $data['diagnosis'] = $this->RuangBersalinModel->getDiagnosisByTransferId($transfer_id, $jenis_ruangan_id);
            foreach ($data['diagnosis'] as &$d) {
                $d->penyakit = $this->RuangBersalinModel->getPenyakitByDiagnosisId($d->id);
            }

            $data['resep'] = $this->RuangBersalinModel->getResepByTransferId($transfer_id, $jenis_ruangan_id);
            foreach ($data['resep'] as &$d) {
                $d->obat = $this->RuangBersalinModel->getObatByResepId($d->id);
                $d->obat_racik = $this->RuangBersalinModel->getObatRacikByResepId($d->id);
                foreach ($d->obat_racik as &$dd) {
                    $dd->detail = $this->RuangBersalinModel->getObatRacikDetailByResepObatRacikId($dd->id);
                    $dd->total = 0;
                    foreach ($dd->detail as $ddd) {
                        $dd->total += $ddd->jumlah * $ddd->harga_jual;
                    }
                }
            }

            $data['bahan_habis_pakai'] = $this->RuangBersalinModel->getBhpByTransferId($transfer_id, $jenis_ruangan_id);
            foreach ($data['bahan_habis_pakai'] as &$d) {
                $d->bahan = $this->RuangBersalinModel->getBahanByBhpId($d->id);
            }

            $data['biaya'] = $this->RuangBersalinModel->getBiayaByTransferId($transfer_id, $jenis_ruangan_id);
            foreach ($data['biaya'] as &$d) {
                if ($d->jenis_biaya == 'Makanan') {
                    $d->makanan = $this->RuangBersalinModel->getMakananById($d->makanan_id);
                }
                else if ($d->jenis_biaya == 'Tindakan') {
                    $d->tindakan = $this->RuangBersalinModel->getTindakanByBiayaId($d->id);
                }
            }
            $data['makanan_count_list'] = $this->RuangBersalinModel->getMakananCountByTransferId($transfer_id, $jenis_ruangan_id);

            if ($jenis_ruangan_id == 4 ||  $jenis_ruangan_id == 5) {
                $tfs = [$this->PerinatologiModel->getTransferById($transfer_id)];
                $bed = [];
                foreach ($tfs as $k => $v) {
                    $bed[] = [
                        'start' => $v->transfered_at,
                        'end' => $k == count($tfs) - 1 ? $v->diperiksa_at : $tfs[$k + 1]->transfered_at,
                        'bed' => $this->bed_model->bed_listsearch($v->tujuan)
                    ];
                }
                $data['beds'] = $bed;
            }

            $data['title'] = $title;
            $data['suffix'] = $suffix;
            $data['sudah_bayar'] = $bayar_all_id ? true : false;

            $calc = $this->calculateTotal($data);
            $total_tindakan += $calc['total_tindakan'];
            $total_obat += $calc['total_obat'];
            $total_obatracik += $calc['total_obatracik'];
            $total_bahan += $calc['total_bahan'];
            $total_makanan += $calc['total_makanan'];
            $total_biaya_bed += $calc['total_biaya_bed'];

            return $data;
        };

        foreach ($jenises as $k => $j) {
            $id = $ids[$k];

            if (substr($j, 0, 4 ) === "Poli") {
                $detail = $detailPoli($id, $j);
                $data['jenis_pendaftaran'] = $this->AdministrasiModel->getPemeriksaanById($id)->row()->jaminan;

                if (isset($detail['konsul'])) {
                    foreach ($detail['konsul']->result() as $tindakan) {
                        $total_tindakan += $tindakan->tarif_pasien;
                    }
                }
                foreach ($detail['tindakan']->result() as $tindakan) {
                    $total_tindakan += $tindakan->tarif_pasien;
                }
                foreach ($detail['obat']->result() as $obat) {
                    $total_obat += $obat->harga_jual * $obat->jumlah_satuan;
                }
                foreach ($detail['racikan'] as $r) {
                    foreach ($r->obat as $obat) {
                        $total_obatracik += $obat->harga_jual * $obat->jumlah_satuan;
                    }
                }
                foreach ($detail['bahan']->result() as $bahan) {
                    $total_bahan += $bahan->harga_jual * $bahan->jumlah;
                }
            }
            else if ($j == 'Ambulance') {
                $detail = [
                    'title' => 'Ambulance',
                    'ambulance' => $this->db->query("
                        SELECT * FROM panggilan_ambulance pa
                        LEFT JOIN vehicles v ON pa.vehicle_id = v.id
                        LEFT JOIN tarif_ambulance ta on pa.tarif_ambulance_id = ta.id
                        WHERE pa.id = $id
                    ")->row()
                ];
                $jp = $detail['ambulance']->jaminan;
                $data['jenis_pendaftaran'] = $jp ?: $data['jenis_pendaftaran'];
                $detail['tarif'] = $data['jenis_pendaftaran'] == 'bpjs' ? $detail['ambulance']->tarif_bpjs : $detail['ambulance']->tarif;
            }
            else if ($j == 'Ranap') {
                $detail = $detailRanap($id, $bayar_all_id);
                $jp = $this->RawatInapModel->getById($id)->tipe_pasien;
                $data['jenis_pendaftaran'] = $jp ?: $data['jenis_pendaftaran'];
            }
            else {
                $jenis_ruangan_id = $this->RawatInapModel->getJenisRuanganByNamaOrAlias($j)->id;
                $title = $j == 'VK' ? 'Ruang Bersalin' : ($j == 'OK' ? 'Ruang Operasi' : $j);
                $detail = $detailRanapLike($id, $jenis_ruangan_id, $title, $j, $bayar_all_id);
                $jp = $this->RuangOperasiModel->getTransferById($id)->tipe_pasien;
                $data['jenis_pendaftaran'] = $jp ?: $data['jenis_pendaftaran'];
            }

            if (substr($j, 0, 4 ) === "Poli") {
                $data['Poli'][str_replace('%20', ' ', $j)][] = [
                    'id' => $ids[$k],
                    'detail' => $detail
                ];
            }
            else if ($j == 'Ambulance') {
                $data['Ambulance'][$j][] = [
                    'id' => $ids[$k],
                    'detail' => $detail
                ];
            }
            else {
                $data['Ruang'][$j][] = [
                    'id' => $ids[$k],
                    'detail' => $detail
                ];
            }
        }

        $data['pasien'] = $this->PasienModel->getPasienById($pasien_id)->row();
        $data['total_tindakan'] = $total_tindakan;
        $data['total_obat'] = $total_obat;
        $data['total_obatracik'] = $total_obatracik;
        $data['total_bahan'] = $total_bahan;
        $data['total_makanan'] = $total_makanan;
        $data['total_biaya_bed'] = $total_biaya_bed;
        $data['sudah_bayar'] = $bayar_all_id ? true : false;
        $data['bayar'] = $bayar_all_id ? $this->MainModel->select('bayar_all', ['id', $bayar_all_id])->row() : '';

        $this->load->view('administrasi/nota_all_print', $data);
    }

    public function nota_obat_luar($id)
    {
        $data['penjualan'] = $this->ObatLuarModel->getPenjualanById($id);
        $data['bahan'] = $this->ObatLuarModel->getDetailPenjualanObatLuarBahanByIdPenjualan($id);
        $data['obat'] = $this->ObatLuarModel->getDetailPenjualanObatLuarByIdPenjualan($id);
        $data['racikan'] = $this->ObatLuarModel->getDetailPenjualanObatRacikanLuarByIdPenjualan($id);

        foreach ($data['racikan'] as $k => $v) {
            $data['racikan'][$k]->obat = $this->ObatLuarModel->getListObatByDetailPenjualanObatRacikanLuarId($v->id);
            foreach ($data['racikan'][$k]->obat as $kk => $vv) {
                $data['racikan'][$k]->total += $vv->jumlah_satuan * $vv->harga_jual;
            }
        }

        $this->template->view('administrasi/nota_obat_luar_v', $data);
    }

    public function edit_obat_luar($id)
    {
        $data['penjualan'] = $this->ObatLuarModel->getPenjualanById($id);
        $data['obat_all'] = $this->ObatModel->listObat()->result();
        $data['obats'] = $this->ObatLuarModel->getDetailPenjualanObatLuarByIdPenjualan($id);
        $data['racikans'] = $this->ObatLuarModel->getDetailPenjualanObatRacikanLuarByIdPenjualan($id);
        $data['goto_apotek'] = $this->uri->segment(4) ? 1 : 0;

        foreach ($data['racikans'] as $k => $v) {
            $data['racikans'][$k]->obat = $this->ObatLuarModel->getListObatByDetailPenjualanObatRacikanLuarId($v->id);
            foreach ($data['racikans'][$k]->obat as $kk => $vv) {
                $data['racikans'][$k]->total += $vv->jumlah_satuan * $vv->harga_jual;
            }
        }

        $this->template->view('administrasi/edit_obat_luar_v', $data);
    }

    public function simpan_edit_obat_luar($penjualan_id)
    {
        $session = $this->session->userdata('logged_in');

//        $obat_pemeriksaan = $this
//            ->ObatLuarModel
//            ->getDetailPenjualanObatLuarByIdPenjualan($penjualan_id);
//        $obatnya_obat_racikan = $this
//            ->ObatLuarModel
//            ->getAllObatOfObatRacikanByPemeriksaanId($penjualan_id)
//            ->result();

        $this->db->delete('detail_penjualan_obat_luar', array('penjualan_obat_luar_id' => $penjualan_id));
        $this->db->delete('detail_penjualan_obat_racikan_luar', array('penjualan_obat_luar_id' => $penjualan_id));

        // --------------- OBAT ---------------- //

        $jumlah_satuan = $this->input->post('jumlah_satuan');
        $signa_obat = $this->input->post('signa_obat');
        $input_obat = $this->input->post('nama_obat');
        $i = 0;

        $ob = array();
        foreach ($input_obat as $key => $value) {
            $obat = array(
                'penjualan_obat_luar_id' => $penjualan_id,
                'obat_id'        => $value,
                'jumlah_satuan'  => $jumlah_satuan[$i],
                'signa_obat'     => $signa_obat[$i],
                'creator'        => $session->id
            );

            if ($obat['obat_id'] != "") {
                $ob[] = $obat;
            }

            $this->MainModel->insert_id('detail_penjualan_obat_luar', $obat);
            $i++;
        }

        // --------------- OBAT RACIKAN ---------------- //

        $ra = array();
        for ($n=1; $n <= 5; $n++) {
            $signa = $this->input->post('signa'.$n);
            $catatan = $this->input->post('catatan'.$n);
            $racikan = array(
                'penjualan_obat_luar_id' => $penjualan_id,
                'nama_racikan'   => 'racikan '.$n,
                'signa'          => $signa ?? '-',
                'catatan'        => $catatan,
                'creator'        => $session->id
            );

            if ($signa != "") {
                $ra[] = $racikan;
                $j = 0;
                $detail_penjualan_obat_racikan_luar_id = $this->MainModel->insert_id('detail_penjualan_obat_racikan_luar', $racikan);
                $id_obat_racikan = $this->input->post('nama_obat_racikan' . $n);
                $jumlah_satuan = $this->input->post('jumlah_satuan_racikan' . $n);

                foreach ($id_obat_racikan as $key => $value) {
                    $obat_racikan = array(
                        'detail_penjualan_obat_racikan_luar_id' => $detail_penjualan_obat_racikan_luar_id,
                        'obat_id' => $value,
                        'jumlah_satuan' => $jumlah_satuan[$j],
                        'creator' => $session->id
                    );

                    $this->MainModel->insert_id('obat_racikan_luar', $obat_racikan);
                    $j++;
                }
            }
        }

        if ($this->input->post('goto_apotek')) {
            redirect('Apotek/resep_nota_obat_luar/'.$penjualan_id);
        }
        else {
            redirect('Administrasi/listPasienSelesaiPeriksa');
        }
    }

    public function nota_submit() {
        $session = $this->session->userdata('logged_in');
        $bayar = array(
            'pemeriksaan_id' => $this->input->post('pemeriksaan_id'),
            'jasa_racik' => $this->input->post('jasa_racik'),
            'diskon' => $this->input->post('diskon'),
            'total' => $this->input->post('total'),
            'bayar' => $this->input->post('bayar'),
            'kembalian' => $this->input->post('kembalian'),
            'creator' => $session->id
        );

        $id_bayar = $this->MainModel->insert_id('bayar', $bayar);

        if (!$id_bayar) {
            echo json_encode('err');die();
        }

        $tarif_pasien = $this->input->post('tarif_pasien');
        $nama_tindakan = $this->input->post('nama_tindakan');
        $i = 0;
        foreach ($nama_tindakan as $key => $value) {
            $tindakan = array(
                'bayar_id' => $id_bayar,
                'item' => $nama_tindakan[$i],
                'jenis_item' => 'tindakan',
                'jumlah' => 1,
                'harga' => $tarif_pasien[$i],
                'subtotal' => $tarif_pasien[$i],
                'creator' => $session->id,
            );
            $this->MainModel->insert('detail_bayar', $tindakan);
            $i++;
        }

        $nama_racikan = $this->input->post('nama_racikan');
        $total_racikan = $this->input->post('total_racikan');
        $k = 0;
        foreach ($nama_racikan as $key => $value) {
            $racikan = array(
                'bayar_id' => $id_bayar,
                'item' => $value,
                'jenis_item' => 'obat racikan',
                'jumlah' => 1,
                'harga' => '',
                'subtotal' => $total_racikan[$k],
                'creator' => $session->id,
            );
            $this->MainModel->insert('detail_bayar', $racikan);
            $k++;
        }

        $nama_obat = $this->input->post('nama_obat');
        $jumlah_satuan = $this->input->post('jumlah_satuan');
        $harga_jual = $this->input->post('harga_jual');
        $subtotal = $this->input->post('subtotal_obat');
        $j = 0;
        foreach ($nama_obat as $key => $value) {
            $obat = array(
                'bayar_id' => $id_bayar,
                'item' => $nama_obat[$j],
                'jenis_item' => 'obat',
                'jumlah' => $jumlah_satuan[$j],
                'harga' => $harga_jual[$j],
                'subtotal' => $subtotal[$j],
                'creator' => $session->id,

            );
            $this->MainModel->insert('detail_bayar', $obat);
            $j++;
        }

        $nama_bahan = $this->input->post('nama_bahan');
        $jumlah_satuan_bahan = $this->input->post('jumlah_satuan_bahan');
        $harga_jual_bahan = $this->input->post('harga_jual_bahan');
        $subtotal_bahan = $this->input->post('subtotal_bahan');
        $b = 0;

        foreach ($nama_bahan as $key => $value) {
            $bahan = array(
                'bayar_id' => $id_bayar,
                'item' => $nama_bahan[$b],
                'jenis_item' => 'bahan habis pakai',
                'jumlah' => $jumlah_satuan_bahan[$b],
                'harga' => $harga_jual_bahan[$b],
                'subtotal' => $subtotal_bahan[$b],
                'creator' => $session->id,
            );
            $this->MainModel->insert('detail_bayar', $bahan);
            $b++;
        }

        if ($this->input->post('jasa_racik')) {
            $jasa_racik = array(
                'bayar_id' => $id_bayar,
                'item' => 'Jasa Racik',
                'jenis_item' => 'jasa racik',
                'jumlah' => 0,
                'harga' => $this->input->post('jasa_racik'),
                'subtotal' => $this->input->post('jasa_racik'),
                'creator' => $session->id,

            );
            $this->MainModel->insert('detail_bayar', $jasa_racik);
        }

        $id = $this->input->post('pemeriksaan_id');
        $this->MainModel->update('pemeriksaan', ['status' => 'sudah_bayar'], $id);

        $data['pemeriksaan'] = $this->AdministrasiModel->getPemeriksaanById($id)->row();
        $data['tindakan'] = $this->AdministrasiModel->getTindakanById($id);
        $data['obat'] = $this->AdministrasiModel->getObatPemeriksaanById($id);
        $data['bahan'] = $this->AdministrasiModel->getBahanHabisPakaiPemeriksaanById($id);
        $data['id_bayar'] = $id_bayar;
        $data['pembayaran'] = $this->AdministrasiModel->getBayarById($id_bayar);

        $racikans = $this->AdministrasiModel->getListRacikanByPemeriksaanId($id)->result();
        foreach ($racikans as $k => $v) {
            $racikans[$k]->obat = $this->AdministrasiModel->getListObatByDetailObatRacikanPemeriksaanId($v->id)->result();
            foreach ($racikans[$k]->obat as $kk => $vv) {
                $racikans[$k]->total += $vv->jumlah_satuan * $vv->harga_jual;
            }
        }

        $pemeriksaan_id = $this->input->post('pemeriksaan_id');
        $this->updateStock($pemeriksaan_id);

        $data['racikan'] = $racikans;
        $bayar['jumlah'] = $this->input->post('jumlah');
        $data['pembayaran_result'] = $bayar;

        $this->session->set_flashdata('success', 'Submit pembayaran berhasil');
        $this->template->view('administrasi/nota_v', $data);
    }

    private function updateStock($pemeriksaan_id)
    {
        // ############################## MASALAH PENGUPDATEAN STOK OBAT DAN BAHAN HABIS PAKAI ##############################

        $obat_pemeriksaan = $this
            ->DetailObatPemeriksaanModel
            ->getDetailObatByPemeriksaanId($pemeriksaan_id)
            ->result();

        foreach ($obat_pemeriksaan as $o) {
            $stok_lawas = $this->ObatModel->getObatById($o->obat_id)->row()->stok_obat;
            $stok_baru = $stok_lawas - $o->jumlah_satuan;
            $this->MainModel->update('obat', ['stok_obat' => $stok_baru], $o->obat_id);
        }

        $obatnya_obat_racikan = $this
            ->DetailObatRacikanPemeriksaanModel
            ->getAllObatOfObatRacikanByPemeriksaanId($pemeriksaan_id)
            ->result();

        foreach ($obatnya_obat_racikan as $o) {
            $stok_lawas = $this->ObatModel->getObatById($o->obat_id)->row()->stok_obat;
            $stok_baru = $stok_lawas - $o->jumlah_satuan;
            $this->MainModel->update('obat', ['stok_obat' => $stok_baru], $o->obat_id);
        }

        $bahan_terpakai = $this
            ->DetailBahanPemeriksaanModel
            ->getDetailBahanByPemeriksaanId($pemeriksaan_id)
            ->result();

        foreach ($bahan_terpakai as $b) {
            $stok_lawas = $this->BahanHabisPakaiModel->getBahanById($b->bahan_id)->row()->jumlah;
            $stok_baru = $stok_lawas - $b->jumlah;
            $this->MainModel->update('bahan_habis_pakai', ['jumlah' => $stok_baru], $b->bahan_id);
        }

        // ############################## END MASALAH PENGUPDATEAN STOK OBAT DAN BAHAN HABIS PAKAI ##############################
    }

    public function nota_obat_luar_submit() {
        $session = $this->session->userdata('logged_in');
        $bayar = [
            'penjualan_obat_luar_id' => $this->input->post('penjualan_id'),
            'jasa_racik' => $this->input->post('jasa_racik'),
            'diskon' => $this->input->post('diskon'),
            'total' => $this->input->post('total'),
            'bayar' => $this->input->post('bayar'),
            'kembalian' => $this->input->post('kembalian'),
            'creator' => $session->id
        ];
        $id_bayar = $this->MainModel->insert_id('bayar_obat_luar', $bayar);

        $nama_racikan = $this->input->post('nama_racikan');
        $total_racikan = $this->input->post('total_racikan');
        $k = 0;
        foreach ($nama_racikan as $key => $value) {
            $this->MainModel->insert('detail_bayar_obat_luar', [
                'bayar_obat_luar_id' => $id_bayar,
                'item' => $value,
                'jenis_item' => 'obat racikan',
                'jumlah' => 1,
                'harga' => '',
                'subtotal' => $total_racikan[$k],
                'creator' => $session->id
            ]);
            $k++;
        }

        $nama_obat = $this->input->post('nama_obat');
        $jumlah_satuan = $this->input->post('jumlah_satuan');
        $harga_jual = $this->input->post('harga_jual');
        $subtotal = $this->input->post('subtotal_obat');
        $j = 0;
        foreach ($nama_obat as $key => $value) {
            $this->MainModel->insert('detail_bayar_obat_luar', [
                'bayar_obat_luar_id' => $id_bayar,
                'item' => $nama_obat[$j],
                'jenis_item' => 'obat',
                'jumlah' => $jumlah_satuan[$j],
                'harga' => $harga_jual[$j],
                'subtotal' => $subtotal[$j],
                'creator' => $session->id
            ]);
            $j++;
        }

        $nama_bahan = $this->input->post('nama_bahan');
        $jumlah_satuan_bahan = $this->input->post('jumlah_satuan_bahan');
        $harga_jual_bahan = $this->input->post('harga_jual_bahan');
        $subtotal_bahan = $this->input->post('subtotal_bahan');
        $j = 0;
        foreach ($nama_bahan as $key => $value) {
            $this->MainModel->insert('detail_bayar_obat_luar', [
                'bayar_obat_luar_id' => $id_bayar,
                'item' => $nama_bahan[$j],
                'jenis_item' => 'bahan',
                'jumlah' => $jumlah_satuan_bahan[$j],
                'harga' => $harga_jual_bahan[$j],
                'subtotal' => $subtotal_bahan[$j],
                'creator' => $session->id
            ]);
            $j++;
        }

        if ($this->input->post('jasa_racik')) {
            $jasa_racik = array(
                'bayar_obat_luar_id' => $id_bayar,
                'item' => 'Jasa Racik',
                'jenis_item' => 'jasa racik',
                'jumlah' => 0,
                'harga' => $this->input->post('jasa_racik'),
                'subtotal' => $this->input->post('jasa_racik'),
                'creator' => $session->id,

            );
            $this->MainModel->insert('detail_bayar_obat_luar', $jasa_racik);
        }

        // done inserting and updating. redirecting...

        $id = $this->input->post('penjualan_id'); // id = penjualan_obat_luar_id
        $this->MainModel->update('penjualan_obat_luar', ['progress' => 'sudah_bayar'], $id);

        // ############################## MASALAH PENGUPDATEAN STOK OBAT OF OBAT LUAR ##############################

        $obats = $this
            ->ObatLuarModel
            ->getDetailPenjualanObatLuarByIdPenjualan($id);

        foreach ($obats as $o) {
            $stok_lawas = $this->ObatModel->getObatById($o->obat_id)->row()->stok_obat;
            $stok_baru = $stok_lawas - $o->jumlah_satuan;
            $this->MainModel->update('obat', ['stok_obat' => $stok_baru], $o->obat_id);
        }

        $obat_racikans = $this
            ->ObatLuarModel
            ->getAllObatOfObatRacikanByPemeriksaanId($id)
            ->result();

        foreach ($obat_racikans as $o) {
            $stok_lawas = $this->ObatModel->getObatById($o->obat_id)->row()->stok_obat;
            $stok_baru = $stok_lawas - $o->jumlah_satuan;
            $this->MainModel->update('obat', ['stok_obat' => $stok_baru], $o->obat_id);
        }

        $bahans = $this
            ->ObatLuarModel
            ->getDetailPenjualanObatLuarBahanByIdPenjualan($id);

        foreach ($bahans as $o) {
            $stok_lawas = $this->ObatModel->getBahanById($o->bahan_id)->row()->jumlah;
            $stok_baru = $stok_lawas - $o->jumlah_satuan;
            $this->MainModel->update('bahan_habis_pakai', ['jumlah' => $stok_baru], $o->bahan_id);
        }

        // ############################## END MASALAH PENGUPDATEAN STOK OBAT OF OBAT LUAR ##############################

        $data['id_bayar'] = $id_bayar;
        $data['pembayaran'] = $this->AdministrasiModel->getBayarById($id_bayar); // INI

        $data['penjualan'] = $this->ObatLuarModel->getPenjualanById($id);
        $data['bahan'] = $this->ObatLuarModel->getDetailPenjualanObatLuarBahanByIdPenjualan($id);
        $data['obat'] = $this->ObatLuarModel->getDetailPenjualanObatLuarByIdPenjualan($id);
        $data['racikan'] = $this->ObatLuarModel->getDetailPenjualanObatRacikanLuarByIdPenjualan($id);

        foreach ($data['racikan'] as $k => $detail_penjualan_obat_racikan_luar) {
            $data['racikan'][$k]->obat = $this->ObatLuarModel->getListObatByDetailPenjualanObatRacikanLuarId($detail_penjualan_obat_racikan_luar->id);
            foreach ($data['racikan'][$k]->obat as $kk => $vv) {
                $data['racikan'][$k]->total += $vv->jumlah_satuan * $vv->harga_jual;
            }
        }

        $bayar['jumlah'] = $this->input->post('jumlah');
        $data['pembayaran_result'] = $bayar;

        $this->session->set_flashdata('success', 'Submit pembayaran berhasil');
        $this->template->view('administrasi/nota_obat_luar_v', $data);
    }

    public function nota_print($id_bayar) {
        $data['pemeriksaan'] = $this->AdministrasiModel->getBayarById($id_bayar)->row();
        $data['tindakan'] = $this->AdministrasiModel->getTindakanBayarById($id_bayar);
        $data['obat'] = $this->AdministrasiModel->getObatBayarById($id_bayar);
        $data['klinik'] = $this->AdministrasiModel->getKlinik()->row();
        $data['bahan'] = $this->AdministrasiModel->getBahanBayarById($id_bayar);
        $data['jasa_racik'] = $this->AdministrasiModel->getJasaRacikBayarById($id_bayar)->row()->harga;

        $racikans = $this->AdministrasiModel->getListRacikanByPemeriksaanId($data['pemeriksaan']->id)->result();
        foreach ($racikans as $k => $v) {
            $racikans[$k]->obat = $this->AdministrasiModel->getListObatByDetailObatRacikanPemeriksaanId($v->id)->result();
            foreach ($racikans[$k]->obat as $kk => $vv) {
                $racikans[$k]->total += $vv->jumlah_satuan * $vv->harga_jual;
            }
        }
        $data['racikan'] = $racikans;
        $data['klinik'] = $this->KlinikModel->getKlinik();
        $data['id_bayar'] = $id_bayar;

        $this->load->view('administrasi/nota_print', $data);
    }

    public function nota_obat_luar_print($id_bayar) {
        $data['penjualan'] = $this->ObatLuarModel->getPenjualanByIdBayar($id_bayar);
        $data['klinik'] = $this->KlinikModel->getKlinik();
        $data['bahan'] = $this->ObatLuarModel->getBahanBayarByIdBayar($id_bayar);
        $data['obat'] = $this->ObatLuarModel->getObatBayarByIdBayar($id_bayar);
        $data['racikan'] = $this->ObatLuarModel->getListRacikanByIdPenjualanId($data['penjualan']->id);

        foreach ($data['racikan'] as $k => $detail_penjualan_obat_racikan_luar) {
            $data['racikan'][$k]->obat = $this->ObatLuarModel->getListObatByDetailPenjualanObatRacikanLuarId($detail_penjualan_obat_racikan_luar->id);
            foreach ($data['racikan'][$k]->obat as $kk => $vv) {
                $data['racikan'][$k]->total += $vv->jumlah_satuan * $vv->harga_jual;
            }
        }
        $data['id_bayar'] = $id_bayar;

        $this->load->view('administrasi/nota_obat_luar_print', $data);
    }

    public function surat_sehat($id) {
        $data['pemeriksaan'] = $this->AdministrasiModel->getPemeriksaanById($id)->row();
        $data['pasien'] = $this->db->query("SELECT * FROM pasien WHERE id = {$data['pemeriksaan']->pasien_id}")->row();
        $data['tindakan'] = $this->AdministrasiModel->getTindakanById($id);
        $data['obat'] = $this->AdministrasiModel->getObatPemeriksaanById($id);
        $data['klinik'] = $this->AdministrasiModel->getKlinik()->row();
        $data['surat_form'] = json_decode($data['pemeriksaan']->surat_form)->sehat;
        $this->template->view('administrasi/surat_sehat_v', $data);
    }

    public function surat_sehat_print($id) {
        $data['pemeriksaan'] = $this->AdministrasiModel->getPemeriksaanById($id)->row();
        $data['pasien'] = $this->db->query("SELECT * FROM pasien WHERE id = {$data['pemeriksaan']->pasien_id}")->row();
        $data['tindakan'] = $this->AdministrasiModel->getTindakanById($id);
        $data['obat'] = $this->AdministrasiModel->getObatPemeriksaanById($id);
        $data['klinik'] = $this->AdministrasiModel->getKlinik()->row();
        $data['untuk'] = $this->input->post('untuk');
        $data['vm_kanan'] = $this->input->post('vm_kanan');
        $data['vm_kiri'] = $this->input->post('vm_kiri');
        $data['telinga_kanan'] = $this->input->post('telinga_kanan');
        $data['telinga_kiri'] = $this->input->post('telinga_kiri');
        $data['tes_buta_warna'] = $this->input->post('tes_buta_warna');
        $data['goldar'] = $this->input->post('goldar');

        $this->load->view('administrasi/surat_sehat_print', $data);
    }

    public function surat_ranap($id) {
        $data['pemeriksaan'] = $this->AdministrasiModel->getPemeriksaanById($id)->row();
        $data['pasien'] = $this->db->query("SELECT * FROM pasien WHERE id = {$data['pemeriksaan']->pasien_id}")->row();
        $data['tindakan'] = $this->AdministrasiModel->getTindakanById($id);
        $data['obat'] = $this->AdministrasiModel->getObatPemeriksaanById($id);
        $data['klinik'] = $this->AdministrasiModel->getKlinik()->row();
        $data['surat_form'] = json_decode($data['pemeriksaan']->surat_form)->ranap;
        $this->template->view('administrasi/surat_ranap_v', $data);
    }

    public function surat_ranap_print($id) {
        $data['pemeriksaan'] = $this->AdministrasiModel->getPemeriksaanById($id)->row();
        $data['pasien'] = $this->db->query("SELECT * FROM pasien WHERE id = {$data['pemeriksaan']->pasien_id}")->row();
        $data['tindakan'] = $this->AdministrasiModel->getTindakanById($id);
        $data['obat'] = $this->AdministrasiModel->getObatPemeriksaanById($id);
        $data['klinik'] = $this->AdministrasiModel->getKlinik()->row();
        $data['diagnosa'] = nl2br($this->input->post('diagnosa'));
        $data['rencana'] = nl2br($this->input->post('rencana'));

        $this->load->view('administrasi/surat_ranap_print', $data);
    }

    public function surat_rujuk($id) {
        $data['pemeriksaan'] = $this->AdministrasiModel->getPemeriksaanById($id)->row();
        $data['pasien'] = $this->db->query("SELECT * FROM pasien WHERE id = {$data['pemeriksaan']->pasien_id}")->row();
        $data['tindakan'] = $this->AdministrasiModel->getTindakanById($id);
        $data['obat'] = $this->AdministrasiModel->getObatPemeriksaanById($id);
        $data['klinik'] = $this->AdministrasiModel->getKlinik()->row();
        $data['surat_form'] = json_decode($data['pemeriksaan']->surat_form)->rujuk;
        $this->template->view('administrasi/surat_rujuk_v', $data);
    }

    public function surat_rujuk_print($id) {
        $data['pemeriksaan'] = $this->AdministrasiModel->getPemeriksaanById($id)->row();
        $data['pasien'] = $this->db->query("SELECT * FROM pasien WHERE id = {$data['pemeriksaan']->pasien_id}")->row();
        $data['tindakan'] = $this->AdministrasiModel->getTindakanById($id);
        $data['obat'] = $this->AdministrasiModel->getObatPemeriksaanById($id);
        $data['klinik'] = $this->AdministrasiModel->getKlinik()->row();
        $data['yth'] = $this->input->post('yth');
        $data['dokter'] = $this->input->post('dokter');
        $data['di'] = $this->input->post('di');
        $data['keluhan'] = $this->input->post('keluhan');
        $data['diagnosa'] = $this->input->post('diagnosa');
        $data['terapi'] = $this->input->post('terapi');

        $this->load->view('administrasi/surat_rujuk_print', $data);
    }

    public function surat_sakit($id) {
        $data['pemeriksaan'] = $this->AdministrasiModel->getPemeriksaanById($id)->row();
        $data['surat_form'] = json_decode($data['pemeriksaan']->surat_form)->sakit;
        $this->template->view('administrasi/surat_sakit_v', $data);
    }

    public function surat_sakit_print($id) {
        $data['pemeriksaan'] = $this->AdministrasiModel->getPemeriksaanById($id)->row();
        $data['tindakan'] = $this->AdministrasiModel->getTindakanById($id);
        $data['obat'] = $this->AdministrasiModel->getObatPemeriksaanById($id);
        $data['klinik'] = $this->AdministrasiModel->getKlinik()->row();
        $data['jumlah_hari'] = $this->input->post('jumlah_hari');
        $data['start_date'] = $this->input->post('start_date');
        $data['end_date'] = $this->input->post('end_date');

        $this->load->view('administrasi/surat_sakit_print', $data);
    }

//    public function rekapitulasi() {
//
//        if ($this->input->get('tgl')) {
//            $data['listPemeriksaan'] = $this->PemeriksaanModel->getListPemeriksaanSelesai($this->input->get('tgl'));
//            $data['tgl'] = $this->input->get('tgl');
//        }
//        else {
//            $data['listPemeriksaan'] = $this->PemeriksaanModel->getListPemeriksaanSelesai('harian');
//        }
//
//        $data['tindakan'] = $this->AdministrasiModel->getTindakandetail();
//        $data['obat'] = $this->AdministrasiModel->getObatPemeriksaandetail();
//        $data['racikan'] = $this->AdministrasiModel->getRacikanPemeriksaan();
//        $data['penyakit'] = $this->AdministrasiModel->getPenyakitPemeriksaandetail();
//        $data['jaminan'] = $this->config->item('pendaftaran');
//
//
//        $this->template->view('administrasi/rekapitulasi', $data);
//    }

    public function rekapitulasi()
    {
        if ($this->input->get('tgl')) {
            $data['listPemeriksaan'] = $this->PemeriksaanModel->getListPemeriksaanSelesai($this->input->get('tgl'))->result();
            $data['tgl'] = $this->input->get('tgl');
        }
        else {
            $data['listPemeriksaan'] = $this->PemeriksaanModel->getListPemeriksaanSelesai('harian')->result();
        }

        set_time_limit(600);
        ini_set('memory_limit', '-1');
        $data['tindakan'] = $this->AdministrasiModel->getTindakanDetail();
        $data['obat'] = $this->AdministrasiModel->getObatPemeriksaanDetail();
        $data['racikan'] = $this->AdministrasiModel->getRacikanPemeriksaan();
        $data['penyakit'] = $this->AdministrasiModel->getPenyakitPemeriksaanDetail();
        $data['jaminan'] = $this->config->item('pendaftaran');

        foreach ($data['listPemeriksaan'] as &$d) {
            $d->tarif_tindakan = $this->AdministrasiModel->getTarifTindakanByPemeriksaanId($d->id);
            $d->harga_obat = $this->AdministrasiModel->getTarifObatByPemeriksaanId($d->id);
        }

        $this->template->view('administrasi/rekapitulasi', $data);
    }

    public function rekap_resep_transfer($current_place_type, $tgl)
    {
        if ($tgl) {
            $data['penjualan'] = $this->RawatInapModel->getPenjualanResepLuarSelesai($tgl);
            $data['tgl'] = $tgl;
        }
        else {
            $data['penjualan'] = $this->RawatInapModel->getPenjualanResepLuarSelesai('harian');
        }

//        $data['obat'] = $this->ObatLuarModel->getDetailPenjualanObatLuar();
//        $data['racikan'] = $this->ObatLuarModel->getRekapRacikan();
//
//        $this->template->view('master/apotek/rekap_resep_luar', $data);

        $data['current_place_type'] = $current_place_type;
        $what = $current_place_type == 'OK' ? 'ruang operasi' : ($current_place_type == 'VK' ? 'ruang bersalin' : $current_place_type);
        $data['list'] = $this->RawatInapModel->getSelesaiOfTransfer(strtolower($what));
        $this->template->view('master/apotek/rekap_resep_transfer', $data);
    }
}

