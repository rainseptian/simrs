<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PembelianObatModel extends CI_Model {

    public function getPembelianObat($periode = '', $limit = -1) {
        if (is_array($periode)) {
            $this->db->where('created_at >=', $periode['start']);
            $this->db->where('created_at <=', $periode['end'].' 23:59:59');
        }
        if ($limit != -1) {
            $this->db->limit($limit);
        }
        return $this->db
            ->where('is_active', 1)
            ->order_by('id', 'desc')
            ->get('pembelian_obat');
    }

    public function getPembelianObatyId($id) {
        return $this->db
            ->where('id', $id)
            ->where('is_active', 1)
            ->get('pembelian_obat');
    }
}
