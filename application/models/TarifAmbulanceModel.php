<?php

defined('BASEPATH') OR exit('No direct script access allowed');


class TarifAmbulanceModel extends CI_Model
{
    public function getAll()
    {
        return $this->db->where('is_active', 1)->get('tarif_ambulance')->result();
    }

    public function getById($id)
    {
        return $this->db->where('is_active', 1)->where('id', $id)->get('tarif_ambulance')->row();
    }
}
