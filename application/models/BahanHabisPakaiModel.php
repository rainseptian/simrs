<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 6/25/19
 * Time: 9:40 AM
 */

class BahanHabisPakaiModel extends CI_Model {

    public function getBahanById($id) {
        return $this->db
            ->where('is_active', 1)
            ->where('id', $id)
            ->get('bahan_habis_pakai');
    }

}
