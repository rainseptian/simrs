<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class TransferPasienModel extends CI_Model
{
    public function getBaru()
    {
        return $this->db->select('t.*, p.nama as nama_pasien, p.no_rm, u.nama as nama_dokter')
            ->from('transfer t')
            ->join('pasien p', 'p.id = t.pasien_id')
            ->join('user u', 'u.id = t.dokter_id')
            ->where('t.status', 'baru')
            ->where('t.is_active', '1')
            ->get()
            ->result();
    }

    public function getBaruByJenis($jenis)
    {
        return $this->db->select('t.*, p.nama as nama_pasien, p.no_rm, u.nama as nama_dokter')
            ->from('transfer t')
            ->join('pasien p', 'p.id = t.pasien_id')
            ->join('user u', 'u.id = t.dokter_id')
            ->where('t.status', 'baru')
            ->where('t.jenis', $jenis)
            ->where('t.is_active', '1')
            ->get()
            ->result();
    }

    public function getTujuanRanap()
    {
        return $this->db->select('t.*, p.nama as nama_pasien, p.no_rm, u.nama as nama_dokter')
            ->from('transfer t')
            ->join('pasien p', 'p.id = t.pasien_id')
            ->join('user u', 'u.id = t.dokter_id')
            ->where('t.status', 'baru')
            ->where_in('t.jenis', [1, 5, 6])
            ->where('t.is_active', '1')
            ->get()
            ->result();
    }

    public function getTransferDetailById($id)
    {
        return $this->db->select('t.*, p.nama as nama_pasien, p.no_rm, u.nama as nama_dokter, jp.nama as nama_poli, t.tujuan')
            ->from('transfer t')
            ->join('pasien p', 'p.id = t.pasien_id')
            ->join('user u', 'u.id = t.dokter_id')
            ->join('jenis_pendaftaran jp', 'jp.id = t.dari', 'left')
            ->where('t.is_active', '1')
            ->where('t.id', $id)
            ->get()
            ->row();
    }

    function hapusPendaftaran($transfer_id)
    {
        return $this->db->query("DELETE FROM transfer WHERE id = '$transfer_id'");
    }

    public function getPoli()
    {
        return $this->db->where('is_active', '1')->get('jenis_pendaftaran')->result();
    }

    public function getLastPoliByPasienId($pasien_id)
    {
        return $this->db
            ->select('jp.*' )
            ->from('pendaftaran_pasien pp')
            ->join('jenis_pendaftaran jp', 'jp.id = pp.jenis_pendaftaran_id and jp.is_active = 1')
            ->where('pp.pasien', $pasien_id)
            ->where('pp.pendaftaran_id IS NULL', null, false)
            ->order_by('pp.id', 'desc')
            ->get()
            ->row();
    }

    public function getLastPemeriksaanByPasienId($pasien_id)
    {
        return $this->db
            ->select('pemeriksaan.*, p.penanggungjawab')
            ->from('pemeriksaan')
            ->join('pendaftaran_pasien p', 'p.id = pemeriksaan.pendaftaran_id')
            ->where('pasien_id', $pasien_id)
            ->order_by('pemeriksaan.id', 'desc')
            ->limit(1)
            ->get()
            ->row();
    }

    public function getPasienById($id)
    {
        return $this->db->select('*')
            ->from('pasien')
            ->where('id', $id)
            ->get()
            ->row();
    }

    public function getKeRuangByJenisTransferId($id)
    {
        return $this->db->select('jr.nama')
            ->from('jenis_transfer jt')
            ->join('jenis_ruangan jr', 'jr.id = jt.ke_jenis_ruangan_id')
            ->where('jt.id', $id)
            ->get()
            ->row();
    }

    public function getDariRuangByJenisTransferId($id)
    {
        return $this->db->select('jr.nama')
            ->from('jenis_transfer jt')
            ->join('jenis_ruangan jr', 'jr.id = jt.dari_jenis_ruangan_id')
            ->where('jt.id', $id)
            ->get()
            ->row();
    }

    public function getJenisTransferById($id)
    {
        return $this->db->select('jt.*, dari.nama as nama_dari, ke.nama as nama_ke')
            ->from('jenis_transfer jt')
            ->join('jenis_ruangan dari', 'dari.id = jt.dari_jenis_ruangan_id')
            ->join('jenis_ruangan ke', 'ke.id = jt.ke_jenis_ruangan_id')
            ->where('jt.id', $id)
            ->get()
            ->row();
    }

    public function getJenisRuanganById($id)
    {
        return $this->db
            ->where('id', $id)
            ->get('jenis_ruangan')
            ->row();
    }

    public function getJenisRuanganByNama($nama)
    {
        return $this->db
            ->where('nama', $nama)
            ->get('jenis_ruangan')
            ->row();
    }

    function delete($id) {
        return $this->db->query("UPDATE transfer SET is_active = 0 WHERE id = '$id'");
    }
}
