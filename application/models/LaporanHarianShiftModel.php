<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class LaporanHarianShiftModel extends CI_Model
{
    private $table = "laporan_harian_shift";

    public function all()
    {
        $rows = $this->db
            ->select("laporan_harian_shift.*, koordinator.nama as nama_koordinator, dokter_jaga.nama as nama_dokter_jaga")
            ->from($this->table)
            ->join("user koordinator", "koordinator.id = laporan_harian_shift.koordinator_id")
            ->join("user dokter_jaga", "dokter_jaga.id = laporan_harian_shift.dokter_jaga_id")
            ->order_by('tanggal_laporan', 'desc')
            ->get()
            ->result();

        foreach ($rows as $row)
        {
            $this->refine($row);
        }

        return $rows;
    }

    public function byPeriode($from, $to)
    {
        $rows = $this->db
            ->select("laporan_harian_shift.*, koordinator.nama as nama_koordinator, dokter_jaga.nama as nama_dokter_jaga")
            ->from($this->table)
            ->join("user koordinator", "koordinator.id = laporan_harian_shift.koordinator_id")
            ->join("user dokter_jaga", "dokter_jaga.id = laporan_harian_shift.dokter_jaga_id")
            ->where("DATE(tanggal_laporan) BETWEEN '$from' AND '$to'")
            ->order_by('tanggal_laporan', 'desc')
            ->get()
            ->result();

        foreach ($rows as $row)
        {
            $this->refine($row);
        }

        return $rows;
    }

    public function getById($id)
    {
        $row = $this->db
            ->select("laporan_harian_shift.*, koordinator.nama as nama_koordinator, dokter_jaga.nama as nama_dokter_jaga")
            ->from($this->table)
            ->join("user koordinator", "koordinator.id = laporan_harian_shift.koordinator_id")
            ->join("user dokter_jaga", "dokter_jaga.id = laporan_harian_shift.dokter_jaga_id")
            ->where("laporan_harian_shift.id", $id)
            ->get()
            ->row();

        if ($row)
        {
            $this->refine($row);
            return $row;
        }
        else
        {
            return null;
        }
    }

    public function insert($data)
    {
        return $this->db->insert($this->table, $data);
    }

    public function update($data, $id)
    {
        $this->db->where('id', $id);
        return $this->db->update($this->table, $data);
    }

    private function refine($row)
    {
        $row->anggota_ids = json_decode($row->anggota_ids);

        $anggotas = $this->db
            ->select("nama")
            ->from("user")
            ->where_in("id", $row->anggota_ids)
            ->get()
            ->result();

        $row->nama_anggotas = array_map(function ($ar)
        {
            return $ar->nama;
        }, $anggotas);
    }
}