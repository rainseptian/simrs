<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class GiziModel extends CI_Model
{
    public function getMakanan()
    {
        return $this->db
            ->select('m.*, tm.nama as tipe')
            ->join('tipe_makanan tm', 'tm.id = m.tipe_makanan_id')
            ->where('m.is_active', '1')
            ->get('makanan m')
            ->result();
    }

    public function getMakananById($id)
    {
        return $this->db
            ->select('m.*, tm.nama as tipe')
            ->join('tipe_makanan tm', 'tm.id = m.tipe_makanan_id')
            ->where('m.is_active', '1')
            ->where('m.id', $id)
            ->get('makanan m')
            ->row();
    }

    public function getTipeMakanan()
    {
        return $this->db
            ->where('tm.is_active', '1')
            ->get('tipe_makanan tm')
            ->result();
    }

    public function getTipeMakananById($id)
    {
        return $this->db
            ->where('tm.is_active', '1')
            ->where('tm.id', $id)
            ->get('tipe_makanan tm')
            ->row();
    }



    public function listGizi() {

        $this->db->select('u.*,ug.id user_grup_id, g.nama_grup');
        $this->db->from('user u');
        $this->db->join('user_grup ug', 'ug.user_id = u.id');
        $this->db->join('grup g', 'ug.grup_id = g.id');
        $this->db->where('u.is_active', 1);
        $this->db->where('g.nama_grup', 'gizi');

        return $this->db->get();
    }

    public function getUserById($id) {

        $this->db->select('u.*,ug.id as user_grup_id, ug.grup_id, g.nama_grup');
        $this->db->from('user u');
        $this->db->join('user_grup ug', 'ug.user_id = u.id');
        $this->db->join('grup g', 'ug.grup_id = g.id');
        $this->db->where('u.is_active', 1);
        $this->db->where('u.id', $id);

        return $this->db->get();
    }

    public function getGrup() {
        $this->db->where('is_active', 1);
        return $this->db->get('grup');
    }
}
