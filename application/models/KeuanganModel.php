<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class KeuanganModel extends CI_Model {

    public function getPemeriksaanSelesai($from, $to, $nama, $tipe)
    {
        $this->db->select('pem.*, p.nama nama_pasien, p.alamat, p.jk, p.usia, u.nama nama_dokter, b.id as id_bayar, jp.nama as nama_poli');
        $this->db->from('pemeriksaan pem');
        $this->db->join('pendaftaran_pasien pp', 'pp.id = pem.pendaftaran_id');
        $this->db->join('jenis_pendaftaran jp', 'jp.id = pp.jenis_pendaftaran_id');
        $this->db->join('pasien p', 'p.id = pem.pasien_id and p.is_active = 1');
        $this->db->join('user u', 'u.id = pem.dokter_id and u.is_active = 1');
        $this->db->join('bayar b', 'b.pemeriksaan_id = pem.id');
        $this->db->where('pem.is_active', '1');
        $this->db->where('pem.sudah_obat', '1');

        if ($from) {
            $this->db->where('pem.created_at >=', $from . ' 00:00:00');
        }
        if ($to) {
            $this->db->where('pem.created_at <=', $to . ' 23:59:59');
        }
        if ($nama) {
            $this->db->like('p.nama', $nama);
        }
        if ($tipe) {
            $this->db->where('pem.jaminan', $tipe);
        }

        $this->db->where('(pem.status', "'sudah_obat'", FALSE);
        $this->db->or_where('pem.status', "'sudah_bayar'", FALSE);
        $this->db->or_where('pem.status', "'selesai')", FALSE);
        $this->db->order_by('pem.waktu_pemeriksaan', 'asc');
        return $this->db->get();
    }

    public function getBayarAll($from, $to)
    {
        if ($from) {
            $this->db->where('created_at >=', $from . ' 00:00:00');
        }
        if ($to) {
            $this->db->where('created_at <=', $to . ' 23:59:59');
        }
        return $this->db->get('bayar_all');
    }

    public function getBayarDetailByBayarAllId($id, $nama)
    {
        $w = $nama ? " AND p.nama LIKE '%$nama%'" : "";

        return $this->db->query("
            SELECT 
                a.*, 
                GROUP_CONCAT(a.id SEPARATOR '^^^') as id, 
                GROUP_CONCAT(a.jenis SEPARATOR '^^^') as jenis, 
                GROUP_CONCAT(a.nama_dokter SEPARATOR '^^^') as nama_dokter,
                GROUP_CONCAT(IFNULL(a.jaminan, '-') SEPARATOR '^^^') as jaminan
            FROM (
                SELECT 
                    CONCAT('Poli - ', jp.nama) as jenis, pem.updated_at tgl, pem.pasien_id, pem.id,
                    pem.status, pem.jaminan, pem.no_rm, p.nama nama_pasien, u.nama nama_dokter
                FROM pemeriksaan pem
                JOIN pendaftaran_pasien pp ON pem.pendaftaran_id = pp.id
                JOIN jenis_pendaftaran jp ON jp.id = pp.jenis_pendaftaran_id
                JOIN pasien p ON p.id = pem.pasien_id and p.is_active = 1
                JOIN user u ON u.id = pem.dokter_id and u.is_active = 1
                JOIN bayar b ON b.pemeriksaan_id = pem.id
                WHERE pem.is_active = 1
                AND pem.sudah_obat
                AND (pem.status = 'selesai' OR pem.status = 'sudah_bayar')
                AND b.bayar_all_id = $id
                $w
            UNION ALL
                SELECT 
                    'Ranap' as jenis, r.tgl_boleh_pulang tgl, r.pasien_id, r.id,
                    r.status, r.tipe_pasien as jaminan, p.no_rm, p.nama nama_pasien, u.nama nama_dokter
                FROM rawat_inap r
                JOIN pasien p ON p.id = r.pasien_id and p.is_active = 1
                JOIN user u ON u.id = r.dokter_id and u.is_active = 1
                JOIN ri_bayar b ON b.rawat_inap_id = r.id and b.transfer_id = r.transfer_id
                WHERE r.is_active = 1
                AND r.status = 'Selesai'
                AND b.bayar_all_id = $id
                AND b.jenis_ruangan_id = 1
                $w
            UNION ALL
                SELECT 
                    'VK' as jenis, t.diperiksa_at tgl, t.pasien_id, t.id,
                    t.status, t.tipe_pasien as jaminan, p.no_rm, p.nama nama_pasien, u.nama nama_dokter
                FROM transfer t
                JOIN pasien p ON p.id = t.pasien_id and p.is_active = 1
                JOIN user u ON u.id = t.dokter_id and u.is_active = 1
                JOIN ri_bayar b ON b.rawat_inap_id = t.rawat_inap_id and b.transfer_id = t.id
                WHERE t.is_active = 1
                AND t.current_place_type = 'ruang bersalin'
                AND t.status = 'selesai'
                AND b.bayar_all_id = $id
                AND b.jenis_ruangan_id = 2
                $w
            UNION ALL
                SELECT 
                    'OK' as jenis, t.diperiksa_at tgl, t.pasien_id, t.id,
                    t.status, t.tipe_pasien as jaminan, p.no_rm, p.nama nama_pasien, u.nama nama_dokter
                FROM transfer t
                JOIN pasien p ON p.id = t.pasien_id and p.is_active = 1
                JOIN user u ON u.id = t.dokter_id and u.is_active = 1
                JOIN ri_bayar b ON b.rawat_inap_id = t.rawat_inap_id and b.transfer_id = t.id
                WHERE t.is_active = 1
                AND t.current_place_type = 'ruang operasi'
                AND t.status = 'selesai'
                AND b.bayar_all_id = $id
                AND b.jenis_ruangan_id = 3
                $w
            UNION ALL
                SELECT 
                    'HCU' as jenis, t.diperiksa_at tgl, t.pasien_id, t.id,
                    t.status, t.tipe_pasien as jaminan, p.no_rm, p.nama nama_pasien, u.nama nama_dokter
                FROM transfer t
                JOIN pasien p ON p.id = t.pasien_id and p.is_active = 1
                JOIN user u ON u.id = t.dokter_id and u.is_active = 1
                JOIN ri_bayar b ON b.rawat_inap_id = t.rawat_inap_id and b.transfer_id = t.id
                WHERE t.is_active = 1
                AND t.current_place_type = 'hcu'
                AND t.status = 'selesai'
                AND b.bayar_all_id = $id
                AND b.jenis_ruangan_id = 5
                $w
            UNION ALL
                SELECT 
                    'Perinatologi' as jenis, t.diperiksa_at tgl, t.pasien_id, t.id,
                    t.status, t.tipe_pasien as jaminan, p.no_rm, p.nama nama_pasien, u.nama nama_dokter
                FROM transfer t
                JOIN pasien p ON p.id = t.pasien_id and p.is_active = 1
                JOIN user u ON u.id = t.dokter_id and u.is_active = 1
                JOIN ri_bayar b ON b.rawat_inap_id = t.rawat_inap_id and b.transfer_id = t.id
                WHERE t.is_active = 1
                AND t.current_place_type = 'perinatologi'
                AND t.status = 'selesai'
                AND b.bayar_all_id = $id
                AND b.jenis_ruangan_id = 4
                $w
            ) a
            GROUP BY a.pasien_id
            ORDER by a.tgl ASC 
        ");
    }

    public function getBayarByBayarAllId($id, $nama)
    {
        if ($nama) {
            $this->db->like('p.nama_pasien', $nama);
        }

        $this->db->join('pemeriksaan p', 'p.id = b.pemeriksaan_id');
        $this->db->join('pendaftaran_pasien pp', 'pp.id = p.pendaftaran_id');
        $this->db->join('jenis_pendaftaran jp', 'jp.id = pp.jenis_pendaftaran_id');
        $this->db->join('user u', 'u.id = p.dokter_id');
        $this->db->where('b.bayar_all_id', $id);
        $this->db->where('b.is_active', 1);
        $this->db->select("b.*, p.*, CONCAT('Poli - ', jp.nama) as jenis, u.nama as nama_dokter");
        return $this->db->get('bayar b');
    }

    public function getRiBayarByBayarAllId($id, $nama)
    {
        if ($nama) {
            $this->db->like('p.nama', $nama);
        }

        $this->db->join('transfer t', 't.id = b.transfer_id');
        $this->db->join('rawat_inap ri', 'ri.id = t.rawat_inap_id');
        $this->db->join('jenis_ruangan jr', 'jr.id = t.jenis');
        $this->db->join('pasien p', 'p.id = t.pasien_id');
        $this->db->join('user u', 'u.id = ri.dokter_id');
        $this->db->where('b.bayar_all_id', $id);
        $this->db->where('b.is_active', 1);
        $this->db->select('b.*, t.*, p.nama as nama_pasien, p.no_rm, jr.nama as jenis, ri.tipe_pasien as jaminan, u.nama as nama_dokter');
        return $this->db->get('ri_bayar b');
    }

    public function getListLabatindakan()
    {
        $this->db->where('is_active', '1');
        $this->db->where('jenis_item', 'tindakan');

        return $this->db->get('detail_bayar');
    }

    public function getListPemasukanDetail_OLD($item)
    {
        $this->db->where('is_active', '1');
        $this->db->where('jenis_item', $item);

        return $this->db->get('detail_bayar');
    }

    public function getListPemasukanDetail($item, $jenis_pendaftaran_id, $table_id, $periode = 'harian')
    {
        if ($table_id == 1) {
            $time = '';
            if ($periode == 'bulanan') {
                $time = ' AND MONTH(db.created_at) = MONTH(CURDATE()) AND YEAR(db.created_at) = YEAR(CURDATE())';
            }
            else if (is_array($periode)) {
                $time = " AND db.created_at >= '{$periode['start']}' AND db.created_at <= '{$periode['end']}'";
            }

            $q = "
                SELECT db.*, p.nama_pasien, p.no_rm, p.waktu_pemeriksaan, db.created_at as waktu_bayar, pp.jaminan as tipe_pasien
                FROM detail_bayar db
                JOIN bayar b ON db.bayar_id = b.id
                JOIN pemeriksaan p ON b.pemeriksaan_id = p.id
                join pasien pas on pas.id = p.pasien_id and pas.is_active = 1
                JOIN pendaftaran_pasien pp ON p.pendaftaran_id = pp.id
                WHERE pp.jenis_pendaftaran_id = '$jenis_pendaftaran_id'
                AND db.jenis_item = '$item'
                $time
            ";
        }
        else {
            $time = '';
            if ($periode == 'bulanan') {
                $time = ' AND MONTH(rbd.created_at) = MONTH(CURDATE()) AND YEAR(rbd.created_at) = YEAR(CURDATE())';
            }
            else if (is_array($periode)) {
                $time = " AND rbd.created_at >= '{$periode['start']}' AND rbd.created_at <= '{$periode['end']}'";
            }

            $q = "
                SELECT rbd.*, p.nama as nama_pasien, p.no_rm, t.created_at as waktu_pemeriksaan, rbd.created_at as waktu_bayar, ri.tipe_pasien
                FROM ri_bayar_detail rbd
                JOIN ri_bayar rb ON rbd.ri_bayar_id = rb.id
                join transfer t on t.id = rb.transfer_id
                JOIN rawat_inap ri ON ri.id = rb.rawat_inap_id
                JOIN pasien p ON p.id = ri.pasien_id and p.is_active = 1
                WHERE rb.jenis_ruangan_id = '$jenis_pendaftaran_id'
                AND rbd.jenis_item = '$item'
                AND rbd.is_active = '1'
                $time
            ";
        }

        return $this->db->query($q);
    }

    public function getListPemasukanJasaRacikDetail($item, $jenis_pendaftaran_id, $table_id, $periode = 'harian')
    {
        if ($table_id == 1 || $table_id == 2) {
            $time = '';
            if ($periode == 'bulanan') {
                $time = ' AND MONTH(db.created_at) = MONTH(CURDATE()) AND YEAR(db.created_at) = YEAR(CURDATE())';
            }
            else if (is_array($periode)) {
                $time = " AND db.created_at >= '{$periode['start']}' AND db.created_at <= '{$periode['end']}'";
            }

            $q = "
                SELECT db.item, db.jumlah, db.harga, db.subtotal, 
                p.nama_pasien, p.no_rm, p.waktu_pemeriksaan, db.created_at as waktu_bayar, pp.jaminan as tipe_pasien
                FROM detail_bayar db
                JOIN bayar b ON db.bayar_id = b.id
                JOIN pemeriksaan p ON b.pemeriksaan_id = p.id
                join pasien pas on pas.id = p.pasien_id and pas.is_active = 1
                JOIN pendaftaran_pasien pp ON p.pendaftaran_id = pp.id
                WHERE pp.jenis_pendaftaran_id = '$jenis_pendaftaran_id'
                AND db.jenis_item = '$item'
                $time
            ";
//            UNION ALL
//                SELECT 'jasa racik' as item, 0 as jumlah, p.jasa_racik as harga, p.jasa_racik as subtotal,
//                p.nama_pasien, p.no_rm, p.waktu_pemeriksaan, db.created_at as waktu_bayar, pp.jaminan as tipe_pasien
//                FROM pemeriksaan p
//                join pasien pas on pas.id = p.pasien_id and pas.is_active = 1
//                JOIN pendaftaran_pasien pp ON p.pendaftaran_id = pp.id
//                JOIN jenis_pendaftaran jp ON pp.jenis_pendaftaran_id = jp.id
//                JOIN bayar db ON db.pemeriksaan_id = p.id
//                where jp.status = 1 AND jp.is_active = 1
//            and p.status = 'selesai' and p.jasa_racik > 0
//            and pp.jenis_pendaftaran_id = '$jenis_pendaftaran_id'
//                $time
        }
        else if ($table_id == 3 || $table_id == 4) {
            $time = '';
            if ($periode == 'bulanan') {
                $time = ' AND MONTH(rbd.created_at) = MONTH(CURDATE()) AND YEAR(rbd.created_at) = YEAR(CURDATE())';
            }
            else if (is_array($periode)) {
                $time = " AND rbd.created_at >= '{$periode['start']}' AND rbd.created_at <= '{$periode['end']}'";
            }

            $q = "
                SELECT rbd.item, rbd.jumlah, rbd.harga, rbd.subtotal, 
                p.nama as nama_pasien, p.no_rm, t.created_at as waktu_pemeriksaan, rbd.created_at as waktu_bayar, ri.tipe_pasien
                FROM ri_bayar_detail rbd
                JOIN ri_bayar rb ON rbd.ri_bayar_id = rb.id
                join transfer t on t.id = rb.transfer_id
                JOIN rawat_inap ri ON ri.id = rb.rawat_inap_id
                JOIN pasien p ON p.id = ri.pasien_id and p.is_active = 1
                WHERE rb.jenis_ruangan_id = '$jenis_pendaftaran_id'
                AND rbd.jenis_item = '$item'
                AND rbd.is_active = '1'
                $time
                UNION ALL 
                SELECT 'jasa racik' as item, 0 as jumlah, rr.jasa_racik as harga, rr.jasa_racik as subtotal,
                pas.nama as nama_pasien, pas.no_rm, t.created_at as waktu_pemeriksaan, rbd.created_at as waktu_bayar, ri.tipe_pasien
                FROM rawat_inap ri
                JOIN ri_resep rr ON ri.id = rr.rawat_inap_id
                join transfer t on t.id = rr.transfer_id
                join pasien pas on pas.id = ri.pasien_id and pas.is_active = 1
                JOIN jenis_ruangan jr ON jr.id = rr.jenis_ruangan_id
                JOIN ri_bayar rbd ON rbd.rawat_inap_id = ri.id
                WHERE ri.status = 'Selesai'
                and rr.jenis_ruangan_id = '$jenis_pendaftaran_id'
                AND rr.jasa_racik > 0
                $time
            ";
        }

        return $this->db->query($q);
    }

    public function getListPemasukanObatLuarDetail($tipe = 'all', $periode = 'harian')
    {
        $q = "
            SELECT db.* 
            FROM detail_bayar_obat_luar db
            JOIN bayar_obat_luar b ON db.bayar_obat_luar_id = b.id
            JOIN penjualan_obat_luar pol ON b.penjualan_obat_luar_id = pol.id
        ";

        if ($tipe == 'all') {
            $q .= "WHERE 1 ";
        }
        else {
            $q .= "WHERE pol.tipe = '$tipe' ";
        }

        if ($periode == 'harian') {
            // $q .= ' AND DATE(db.created_at) = CURDATE()';
        }
        else if (is_array($periode)) {
            // $q .= ' AND MONTH(db.created_at) = MONTH(CURDATE()) AND YEAR(db.created_at) = YEAR(CURDATE())';
            $q .= " AND db.created_at >= '{$periode['start']}' AND db.created_at <= '{$periode['end']}'";
        }
        else {
            $q .= ' AND MONTH(db.created_at) = MONTH(CURDATE()) AND YEAR(db.created_at) = YEAR(CURDATE())';
        }

        return $this->db->query($q);
    }

    public function getTotallabaTindakan() {
        return $this->db->query("SELECT jenis_item, sum(subtotal) as total from detail_bayar where is_active = '1' group by jenis_item");
    }

    public function getLabaByLayanan($periode = 'harian')
    {
        if ($periode == 'harian') {
            $time = '';
        }
        else if (is_array($periode)) {
            $time = " AND db.created_at >= '{$periode['start']} 00:00:00' AND db.created_at <= '{$periode['end']} 23:59:59'";
        }
        else {
            $time = ' AND MONTH(db.created_at) = MONTH(CURDATE()) AND YEAR(db.created_at) = YEAR(CURDATE())';
        }

        $qq = "
            SELECT tbl.jenis_item, SUM(tbl.total) as total, tbl.jaminan, tbl.created_at FROM (
                    SELECT db.jenis_item, SUM(db.subtotal) as total, pp.jaminan, db.created_at
                    FROM detail_bayar db
                    JOIN bayar b ON db.bayar_id = b.id
                    JOIN pemeriksaan p ON b.pemeriksaan_id = p.id
                    JOIN pendaftaran_pasien pp ON p.pendaftaran_id = pp.id
                    JOIN pasien pas ON pas.id = p.pasien_id AND pas.is_active = '1'
                    WHERE db.is_active = '1'
                    AND pp.jaminan <> ''
                    AND pp.jaminan <> 'Umum'
                    AND p.status = 'selesai'
                    $time
                    GROUP BY pp.jaminan
                UNION ALL
                    SELECT db.jenis_item, SUM(db.subtotal) as total, ri.tipe_pasien as jaminan, db.created_at
                    FROM ri_bayar_detail db
                    JOIN ri_bayar b ON db.ri_bayar_id = b.id
                    JOIN transfer t ON t.id = b.transfer_id
                    JOIN jenis_transfer jt ON jt.id = t.jenis
                    JOIN jenis_ruangan jr ON jr.id = jt.ke_jenis_ruangan_id
                    JOIN rawat_inap ri ON b.rawat_inap_id = ri.id
                    JOIN pasien p ON p.id = ri.pasien_id AND p.is_active = '1'
                    WHERE db.is_active = '1'
                    AND ri.is_active = '1'
                    AND ri.tipe_pasien <> ''
                    AND ri.tipe_pasien <> 'Umum'
                    AND ri.status = 'Selesai'
                    $time
                    GROUP BY ri.tipe_pasien
            ) tbl
            GROUP BY tbl.jaminan
        ";

        return $this->db->query($qq);
    }

    public function getListPiutangByJaminan($periode, $jaminan = 'umum')
    {
        if ($periode == 'harian') {
            $time = '';
        }
        else if (is_array($periode)) {
            $time = " AND db.created_at >= '{$periode['start']} 00:00:00' AND db.created_at <= '{$periode['end']} 23:59:59'";
        }
        else {
            $time = ' AND MONTH(db.created_at) = MONTH(CURDATE()) AND YEAR(db.created_at) = YEAR(CURDATE())';
        }

        $q = "
            SELECT * FROM (
                    SELECT 1 as table_id, db.jenis_item, db.item, db.jumlah, db.harga, db.subtotal, 
                    p.waktu_pemeriksaan, pas.nama nama_pasien, pas.no_rm, u.nama nama_dokter, jp.nama ruangan 
                    FROM detail_bayar db JOIN bayar b ON db.bayar_id = b.id 
                    JOIN pemeriksaan p ON b.pemeriksaan_id = p.id 
                    JOIN pendaftaran_pasien pp ON p.pendaftaran_id = pp.id 
                    JOIN jenis_pendaftaran jp ON jp.id = pp.jenis_pendaftaran_id
                    JOIN pasien pas ON pas.id = p.pasien_id AND pas.is_active = '1'
                    join user u on u.id = p.dokter_id
                    WHERE db.is_active = '1' AND pp.jaminan = '$jaminan'   
                    AND p.status = 'selesai'
                    $time
                UNION ALL         
                    SELECT 2 as table_id, db.jenis_item, db.item, db.jumlah, db.harga, db.subtotal, 
                    ri.created_at as waktu_pemeriksaan, pas.nama nama_pasien, pas.no_rm, '-' nama_dokter, jr.nama ruangan
                    FROM ri_bayar_detail db
                    JOIN ri_bayar b ON db.ri_bayar_id = b.id
                    JOIN transfer t ON t.id = b.transfer_id
                    JOIN jenis_transfer jt ON jt.id = t.jenis
                    JOIN jenis_ruangan jr ON jr.id = jt.ke_jenis_ruangan_id
                    JOIN rawat_inap ri ON b.rawat_inap_id = ri.id
                    JOIN pasien pas ON pas.id = ri.pasien_id AND pas.is_active = '1'
                    WHERE db.is_active = '1' AND ri.tipe_pasien = '$jaminan'   
                    AND t.status = 'selesai'
                    $time
            ) tbl
            order by tbl.waktu_pemeriksaan
        ";

        return $this->db->query($q);
    }

    public function getLabaByPendaftaran($jenis_pemasukan, $periode = 'harian')
    {
        $time = '';
        if ($periode == 'bulanan') {
            $time = ' AND MONTH(db.created_at) = MONTH(CURDATE()) AND YEAR(db.created_at) = YEAR(CURDATE())';
        }
        else if (is_array($periode)) {
            $time = " AND db.created_at >= '{$periode['start']}' AND db.created_at <= '{$periode['end']}'";
        }

        $time2 = '';
        if ($periode == 'bulanan') {
            $time2 = ' AND MONTH(rbd.created_at) = MONTH(CURDATE()) AND YEAR(rbd.created_at) = YEAR(CURDATE())';
        }
        else if (is_array($periode)) {
            $time2 = " AND rbd.created_at >= '{$periode['start']}' AND rbd.created_at <= '{$periode['end']}'";
        }

        $q = "
            SELECT tbl.* FROM (
                SELECT 1 as table_id, db.jenis_item, SUM(db.subtotal) as total, jp.id as jenis_pendaftaran_id, jp.nama as jenis_pendaftaran
                FROM detail_bayar db
                JOIN bayar b ON db.bayar_id = b.id
                JOIN pemeriksaan p ON b.pemeriksaan_id = p.id
                join pasien pas on pas.id = p.pasien_id and pas.is_active = 1
                JOIN pendaftaran_pasien pp ON p.pendaftaran_id = pp.id
                JOIN jenis_pendaftaran jp ON pp.jenis_pendaftaran_id = jp.id
                WHERE db.is_active = '1' AND db.jenis_item = '$jenis_pemasukan' 
                $time
                GROUP BY jp.id
            UNION ALL 
                SELECT 2 as table_id, rbd.jenis_item, SUM(rbd.subtotal) as total, jr.id as jenis_pendaftaran_id, jr.nama as jenis_pendaftaran
                FROM ri_bayar_detail rbd
                JOIN ri_bayar rb ON rb.id = rbd.ri_bayar_id
                join rawat_inap ri ON ri.id = rb.rawat_inap_id
                join pasien pas on pas.id = ri.pasien_id and pas.is_active = 1
                JOIN jenis_ruangan jr ON jr.id = rb.jenis_ruangan_id
                WHERE rbd.is_active = '1' AND rbd.jenis_item = '$jenis_pemasukan'
                $time2
                GROUP BY jr.id
            ) tbl
        ";

        return $this->db->query($q);
    }

    public function getLabaJasaRacikByPendaftaran($periode = 'harian')
    {
        $time_db = '';
        if ($periode == 'bulanan') {
            $time_db = ' AND MONTH(db.created_at) = MONTH(CURDATE()) AND YEAR(db.created_at) = YEAR(CURDATE())';
        }
        else if (is_array($periode)) {
            $time_db = " AND db.created_at >= '{$periode['start']}' AND db.created_at <= '{$periode['end']}'";
        }

        $time2 = '';
        if ($periode == 'bulanan') {
            $time2 = ' AND MONTH(rbd.created_at) = MONTH(CURDATE()) AND YEAR(rbd.created_at) = YEAR(CURDATE())';
        }
        else if (is_array($periode)) {
            $time2 = " AND rbd.created_at >= '{$periode['start']}' AND rbd.created_at <= '{$periode['end']}'";
        }

        $q = "
            SELECT *, db.jp_id as jenis_pendaftaran_id, sum(db.sub_total) as total FROM (
                SELECT 1 as table_id, db.jenis_item, SUM(db.subtotal) as sub_total, jp.id as jp_id, jp.nama as jenis_pendaftaran
                FROM detail_bayar db
                JOIN bayar b ON db.bayar_id = b.id
                JOIN pemeriksaan p ON b.pemeriksaan_id = p.id
                join pasien pas on pas.id = p.pasien_id and pas.is_active = 1
                JOIN pendaftaran_pasien pp ON p.pendaftaran_id = pp.id
                JOIN jenis_pendaftaran jp ON pp.jenis_pendaftaran_id = jp.id
                WHERE db.is_active = '1' AND db.jenis_item = 'jasa racik'
                AND jp.status = 1 AND jp.is_active = 1
                $time_db
                GROUP BY jp.id
            UNION ALL 
                SELECT 3 as table_id, rbd.jenis_item, SUM(rbd.subtotal) as sub_total, jr.id as jp_id, jr.nama as jenis_pendaftaran
                FROM ri_bayar_detail rbd
                JOIN ri_bayar rb ON rb.id = rbd.ri_bayar_id
                join rawat_inap ri ON ri.id = rb.rawat_inap_id
                join pasien pas on pas.id = ri.pasien_id and pas.is_active = 1
                JOIN jenis_ruangan jr ON jr.id = rb.jenis_ruangan_id
                WHERE rbd.is_active = '1' AND rbd.jenis_item = 'jasa racik'
                $time2
                GROUP BY jr.id
            UNION ALL 
                SELECT 4 as table_id, 'jasa racik' as jenis_item, SUM(rr.jasa_racik) as sub_total, jr.id as jp_id, jr.nama as jenis_pendaftaran
                FROM rawat_inap ri
                JOIN ri_resep rr ON ri.id = rr.rawat_inap_id
                join pasien pas on pas.id = ri.pasien_id and pas.is_active = 1
                JOIN jenis_ruangan jr ON jr.id = rr.jenis_ruangan_id
                JOIN ri_bayar rbd ON rbd.rawat_inap_id = ri.id
                WHERE ri.status = 'Selesai'
                $time2
                GROUP BY jr.id
                HAVING SUM(rr.jasa_racik) > 0
            ) db
            group by db.jp_id
            order by total desc;
        ";
//        UNION ALL
//                SELECT 2 as table_id, 'jasa racik' as jenis_item, SUM(p.jasa_racik) as sub_total, jp.id as jp_id, jp.nama as jenis_pendaftaran
//                FROM pemeriksaan p
//                join pasien pas on pas.id = p.pasien_id and pas.is_active = 1
//                JOIN pendaftaran_pasien pp ON p.pendaftaran_id = pp.id
//                JOIN jenis_pendaftaran jp ON pp.jenis_pendaftaran_id = jp.id
//                JOIN bayar db ON db.pemeriksaan_id = p.id
//                where jp.status = 1 AND jp.is_active = 1
//    and p.status = 'selesai'
//                $time_db
//                GROUP BY jp.id
//                having sum(p.jasa_racik) > 0

        return $this->db->query($q);
    }

    public function getLabaObatLuarByPendaftaran($periode = 'harian') {

        $q = "
            SELECT db.jenis_item, SUM(db.subtotal) as total, pol.tipe
            FROM detail_bayar_obat_luar db
            JOIN bayar_obat_luar b ON db.bayar_obat_luar_id = b.id
            JOIN penjualan_obat_luar pol ON b.penjualan_obat_luar_id = pol.id
            WHERE db.is_active = '1'  
        ";

        if ($periode == 'harian') {
            // $q .= ' AND DATE(db.created_at) = CURDATE()';
        }
        else if (is_array($periode)) {
            // $q .= ' AND MONTH(db.created_at) = MONTH(CURDATE()) AND YEAR(db.created_at) = YEAR(CURDATE())';
            $q .= " AND db.created_at >= '{$periode['start']}' AND db.created_at <= '{$periode['end']}'";
        }
        else {
            $q .= ' AND MONTH(db.created_at) = MONTH(CURDATE()) AND YEAR(db.created_at) = YEAR(CURDATE())';
        }
//         echo json_encode($q);die();

        $q .= ' GROUP BY pol.tipe';

        return $this->db->query($q);
    }

    public function getListLabaobat()
    {
        $this->db->where('is_active', '1' );
        $this->db->where('jenis_item', 'obat' );

        return $this->db->get('detail_bayar');
    }

    public function getListPengeluarantindakan()
    {
        $this->db->select('db.*,' );
        $this->db->from('detail_bayar db');
        $this->db->join('bayar b', 'b.id = db.bayar_id');
        $this->db->join('pemeriksaan pem', 'pem.id = b.pemeriksaan_id');
        $this->db->where('is_active', '1' );
        $this->db->where('jenis_item', 'tindakan' );
        return $this->db->get('detail_bayar db');
    }

    public function getListPengeluaranobat()
    {
        $this->db->where('is_active', '1' );
        $this->db->where('jenis_item', 'obat' );
        return $this->db->get('detail_bayar');
    }

    public function getTindakanPemeriksaan()
    {
                $this->db->select('dtp.*, td.nama , td.tarif_perawat, td.tarif_dokter, td.tarif_lain,  td.klinik,pem.waktu_pemeriksaan' );
                $this->db->join('tarif_tindakan td', 'td.id = dtp.tarif_tindakan_id and td.is_active = 1');
                $this->db->join('pemeriksaan pem', 'pem.id = dtp.pemeriksaan_id and pem.is_active = 1');
                $this->db->where('dtp.is_active', '1' );
                $this->db->order_by('dtp.pemeriksaan_id', 'ASC' );
        return  $this->db->get('detail_tindakan_pemeriksaan dtp');
    }

     public function getTotalTindakan()
    {
                $this->db->select('sum(td.tarif_perawat) tarif_perawat, sum(td.tarif_dokter) tarif_dokter, sum(td.tarif_lain) tarif_lain,  sum(td.klinik) klinik' );
                $this->db->join('tarif_tindakan td', 'td.id = dtp.tarif_tindakan_id and td.is_active = 1');
                $this->db->join('pemeriksaan pem', 'pem.id = dtp.pemeriksaan_id and pem.is_active = 1');
                $this->db->where('dtp.is_active', '1' );
                $this->db->group_by('dtp.is_active');
                $this->db->order_by('dtp.pemeriksaan_id', 'ASC' );
        return  $this->db->get('detail_tindakan_pemeriksaan dtp');
    }

    public function getObatPemeriksaan()
    {
                $this->db->select('dop.*, o.nama , o.harga_jual, o.harga_beli, pem.waktu_pemeriksaan' );
                $this->db->join('obat o', 'o.id = dop.obat_id and o.is_active = 1');
                $this->db->join('pemeriksaan pem', 'pem.id = dop.pemeriksaan_id and pem.is_active = 1');
                $this->db->where('dop.is_active', '1' );
                $this->db->order_by('dop.pemeriksaan_id', 'ASC' );
        return  $this->db->get('detail_obat_pemeriksaan dop');
    }

    public function getTotalObatPemeriksaan()
    {
                $this->db->select('sum(dop.jumlah_satuan * o.harga_beli ) as subtotal' );
                $this->db->join('obat o', 'o.id = dop.obat_id and o.is_active = 1');
                $this->db->join('pemeriksaan pem', 'pem.id = dop.pemeriksaan_id and pem.is_active = 1');
                $this->db->where('dop.is_active', '1' );
                $this->db->group_by('dop.is_active');
        return  $this->db->get('detail_obat_pemeriksaan dop');
    }

    public function getListPemasukanTindakan($start_date, $end_date, $jenis_pendaftaran_id, $tindakan_ids, $jaminan, $dokter)
    {
        $res = [];

        if (!$jenis_pendaftaran_id || is_numeric($jenis_pendaftaran_id)) {
            $t = join(',', $tindakan_ids);
            $tindakan_filter = count($tindakan_ids) ? "AND db.item_id IN($t)" : '';

            $jenis_pendaftaran_filter = $jenis_pendaftaran_id ? "AND pp.jenis_pendaftaran_id = '$jenis_pendaftaran_id'" : '';
            $jaminan_filter = $jaminan ? "AND pp.jaminan = '$jaminan'" : '';
            $dokter_filter = $dokter ? "AND p.dokter_id = $dokter" : '';

            $r = $this->db
                ->query("
                    SELECT db.*, b.created_at, pas.nama as nama_pasien, u.nama as nama_dokter, jp.nama as nama_poli, pp.jaminan
                    FROM detail_bayar db
                    JOIN bayar b ON db.bayar_id = b.id
                    JOIN pemeriksaan p ON b.pemeriksaan_id = p.id
                    JOIN pendaftaran_pasien pp ON p.pendaftaran_id = pp.id
                    JOIN pasien pas ON pas.id = pp.pasien and pas.is_active = 1
                    JOIN user u ON u.id = p.dokter_id and u.is_active = 1
                    JOIN jenis_pendaftaran jp ON jp.id = pp.jenis_pendaftaran_id
                    WHERE db.jenis_item = 'tindakan'
                    AND b.created_at >= '$start_date 00:00:00'
                    AND b.created_at <= '$end_date 23:59:59'
                    AND p.status = 'selesai'
                    $jenis_pendaftaran_filter
                    $tindakan_filter
                    $jaminan_filter
                    $dokter_filter
                    ORDER BY b.created_at
                ")
                ->result();

            $res = array_merge($res, $r);
        }
        if (!$jenis_pendaftaran_id || !is_numeric($jenis_pendaftaran_id)) {
            $t = join(',', $tindakan_ids);
            $tindakan_filter = count($tindakan_ids) ? "AND db.item_id IN($t)" : '';

            $jenis_pendaftaran_filter = $jenis_pendaftaran_id ? "AND t.jenis_ruangan_id = '".str_replace('ri-', '', $jenis_pendaftaran_id)."'" : '';
            $jaminan_filter = $jaminan ? "AND ri.tipe_pasien = '$jaminan'" : '';
            $dokter_filter = $dokter ? "AND ri.dokter_id = $dokter" : '';

            $r = $this->db
                ->query("
                    SELECT db.*, b.created_at, pas.nama as nama_pasien, u.nama as nama_dokter, jr.nama as nama_poli, ri.tipe_pasien as jaminan
                    FROM ri_bayar_detail db
                    JOIN ri_bayar b ON db.ri_bayar_id = b.id
                    JOIN rawat_inap ri ON ri.id = b.rawat_inap_id
                    JOIN pasien pas ON pas.id = ri.pasien_id and pas.is_active = 1
                    JOIN user u ON u.id = ri.dokter_id and u.is_active = 1
                    JOIN transfer t ON t.id = b.transfer_id
                    JOIN jenis_ruangan jr ON jr.id = t.jenis_ruangan_id
                    WHERE db.jenis_item = 'tindakan'
                    AND b.created_at >= '$start_date 00:00:00'
                    AND b.created_at <= '$end_date 23:59:59'
                    AND t.status = 'selesai'
                    $jenis_pendaftaran_filter
                    $tindakan_filter
                    $jaminan_filter
                    $dokter_filter
                    ORDER BY b.created_at
                ")
                ->result();

            $res = array_merge($res, $r);
        }

        if (!$jenis_pendaftaran_id) {
            usort($res, function ($a, $b) {
                return strtotime($a->created_at) <=> strtotime($b->created_at);
            });
        }

        return $res;
    }

    public function getListPemasukanKonsumsi($start_date, $end_date)
    {
        return $this->db
            ->query("
                    SELECT db.*, b.created_at, pas.nama as nama_pasien, u.nama as nama_dokter, jr.nama as nama_poli, ri.tipe_pasien as jaminan
                    FROM ri_bayar_detail db 
                    JOIN ri_bayar b ON db.ri_bayar_id = b.id
                    JOIN rawat_inap ri ON ri.id = b.rawat_inap_id
                    JOIN pasien pas ON pas.id = ri.pasien_id and pas.is_active = 1
                    JOIN user u ON u.id = ri.dokter_id and u.is_active = 1
                    JOIN transfer t ON t.id = b.transfer_id
                    JOIN jenis_ruangan jr ON jr.id = t.jenis_ruangan_id
                    WHERE db.jenis_item = 'makanan'
                    AND b.created_at >= '$start_date 00:00:00'
                    AND b.created_at <= '$end_date 23:59:59'
                    AND t.status = 'selesai'
                    ORDER BY b.created_at
                ")
            ->result();
    }

    public function getFrekuensiTindakan($start_date, $end_date, $jenis_pendaftaran_id, $jaminan)
    {
        $jenis_pendaftaran_filter = $jenis_pendaftaran_id ? "AND pp.jenis_pendaftaran_id = '$jenis_pendaftaran_id'" : '';
        $jaminan_filter = $jaminan ? "AND pp.jaminan = '$jaminan'" : '';

        $q = "
            SELECT db.item_id as id, db.item as nama, count(db.item_id) as frekuensi,
                IF(TIME(db.created_at) >= '08:00:00' AND TIME(db.created_at) < '18:00:00', 'Shift I', 'Shift II') as shift
            FROM detail_bayar db
            JOIN bayar b ON db.bayar_id = b.id
            JOIN pemeriksaan p ON b.pemeriksaan_id = p.id
            JOIN pendaftaran_pasien pp ON p.pendaftaran_id = pp.id
            JOIN jenis_pendaftaran jp ON jp.id = pp.jenis_pendaftaran_id
            WHERE db.jenis_item = 'tindakan'
            AND db.created_at >= '$start_date 00:00:00'
            AND db.created_at <= '$end_date 23:59:59'
            AND p.status = 'selesai'
            $jenis_pendaftaran_filter
            $jaminan_filter
            GROUP BY db.item_id HAVING count(db.item_id) > 0 
            ORDER BY frekuensi DESC 
        ";

        return $this->db->query($q);
    }


}
