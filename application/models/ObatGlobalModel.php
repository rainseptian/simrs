<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ObatGlobalModel extends CI_Model
{
    public function get_obat_luar($periode = '')
    {
        if ($periode == 'harian') {
            $this->db->where('date_format(db.created_at,"%Y-%m-%d")', 'CURDATE()', FALSE);
        }
        else if (is_array($periode)) {
            $this->db->where('db.created_at >=', $periode['start']);
            $this->db->where('db.created_at <=', $periode['end'].' 23:59:59');
        }
        else {
            $this->db->where('MONTH(db.created_at)', 'MONTH(CURDATE())', FALSE);
            $this->db->where('YEAR(db.created_at)', 'YEAR(CURDATE())', FALSE);
        }

        return $this->db
            ->where('db.is_active', '1')
            ->join('penjualan_obat_luar pol', 'pol.id = db.penjualan_obat_luar_id')
            ->join('bayar_obat_luar bol', 'bol.penjualan_obat_luar_id = pol.id')
            ->join('obat o', 'o.id = db.obat_id')
            ->select('db.obat_id, o.nama as nama_obat, db.jumlah_satuan as jumlah, o.harga_jual as harga, pol.tipe as poli')
            ->get('detail_penjualan_obat_luar db')
            ->result();
    }

    public function get_obat_racikan_luar($periode = '')
    {
        if ($periode == 'harian') {
            $this->db->where('date_format(db.created_at,"%Y-%m-%d")', 'CURDATE()', FALSE);
        }
        else if (is_array($periode)) {
            $this->db->where('db.created_at >=', $periode['start']);
            $this->db->where('db.created_at <=', $periode['end'].' 23:59:59');
        }
        else {
            $this->db->where('MONTH(db.created_at)', 'MONTH(CURDATE())', FALSE);
            $this->db->where('YEAR(db.created_at)', 'YEAR(CURDATE())', FALSE);
        }

        return $this->db
            ->where('db.is_active', '1')
            ->join('detail_penjualan_obat_racikan_luar dporl', 'dporl.id = db.detail_penjualan_obat_racikan_luar_id')
            ->join('penjualan_obat_luar pol', 'pol.id = dporl.penjualan_obat_luar_id')
            ->join('bayar_obat_luar bol', 'bol.penjualan_obat_luar_id = pol.id')
            ->join('obat o', 'o.id = db.obat_id')
            ->select('db.obat_id, o.nama as nama_obat, db.jumlah_satuan as jumlah, o.harga_jual as harga, pol.tipe as poli')
            ->get('obat_racikan_luar db')
            ->result();
    }

    public function get_obat_pemeriksaan($periode = '', $poli = '')
    {
        if ($periode == 'harian') {
            $this->db->where('date_format(db.created_at,"%Y-%m-%d")', 'CURDATE()', FALSE);
        }
        else if (is_array($periode)) {
            $this->db->where('db.created_at >=', $periode['start']);
            $this->db->where('db.created_at <=', $periode['end'].' 23:59:59');
        }
        else {
            $this->db->where('MONTH(db.created_at)', 'MONTH(CURDATE())', FALSE);
            $this->db->where('YEAR(db.created_at)', 'YEAR(CURDATE())', FALSE);
        }

        if ($poli) {
            $this->db->where('pp.jenis_pendaftaran_id', $poli);
        }

        return $this->db
            ->where('db.jenis_item', 'obat')
            ->where('db.is_active', '1')
            ->join('obat o', 'o.id = db.item_id OR db.item = o.nama')
            ->join('bayar b', 'b.id = db.bayar_id')
            ->join('pemeriksaan pem', 'pem.id = b.pemeriksaan_id')
            ->join('pendaftaran_pasien pp', 'pp.id = pem.pendaftaran_id')
            ->join('jenis_pendaftaran jp', 'jp.id = pp.jenis_pendaftaran_id')
            ->select('db.item_id as obat_id, o.nama as nama_obat, db.jumlah, db.harga, jp.nama as poli')
            ->group_by('pem.id')
            ->get('detail_bayar db')
            ->result();
    }

    public function get_obat_racikan_pemeriksaan($periode = '', $poli = '')
    {
        if ($periode == 'harian') {
            $this->db->where('date_format(db.created_at,"%Y-%m-%d")', 'CURDATE()', FALSE);
        }
        else if (is_array($periode)) {
            $this->db->where('db.created_at >=', $periode['start']);
            $this->db->where('db.created_at <=', $periode['end'].' 23:59:59');
        }
        else {
            $this->db->where('MONTH(db.created_at)', 'MONTH(CURDATE())', FALSE);
            $this->db->where('YEAR(db.created_at)', 'YEAR(CURDATE())', FALSE);
        }

        if ($poli) {
            $this->db->where('pp.jenis_pendaftaran_id', $poli);
        }

        return $this->db
            ->where('db.is_active', '1')
            ->join('detail_obat_racikan_pemeriksaan dorp', 'dorp.id = db.detail_obat_racikan_pemeriksaan_id')
            ->join('pemeriksaan pem', 'pem.id = dorp.pemeriksaan_id')
            ->join('pendaftaran_pasien pp', 'pp.id = pem.pendaftaran_id')
            ->join('jenis_pendaftaran jp', 'jp.id = pp.jenis_pendaftaran_id')
            ->join('bayar b', 'b.pemeriksaan_id = pem.id')
            ->join('obat o', 'o.id = db.obat_id')
            ->select('db.obat_id, o.nama as nama_obat, db.jumlah_satuan as jumlah, o.harga_jual as harga, jp.nama as poli')
            ->group_by('pem.id')
            ->get('obat_racikan db')
            ->result();
    }

    public function get_obat_pemeriksaan_ranap_like($periode = '')
    {
        if ($periode == 'harian') {
            $this->db->where('date_format(rro.created_at,"%Y-%m-%d")', 'CURDATE()', FALSE);
        }
        else if (is_array($periode)) {
            $this->db->where('rro.created_at >=', $periode['start'].' 00:00:00');
            $this->db->where('rro.created_at <=', $periode['end'].' 23:59:59');
        }
        else {
            $this->db->where('MONTH(rro.created_at)', 'MONTH(CURDATE())', FALSE);
            $this->db->where('YEAR(rro.created_at)', 'YEAR(CURDATE())', FALSE);
        }

        return $this->db
            ->join('obat o', 'o.id = rro.obat_id')
            ->select('rro.obat_id, o.nama as nama_obat, rro.jumlah, o.harga_jual as harga')
            ->get('ri_resep_obat rro')
            ->result();
    }

    public function get_obat_racikan_pemeriksaan_ranap_like($periode = '')
    {
        if ($periode == 'harian') {
            $this->db->where('date_format(rrord.created_at,"%Y-%m-%d")', 'CURDATE()', FALSE);
        }
        else if (is_array($periode)) {
            $this->db->where('rrord.created_at >=', $periode['start'].' 00:00:00');
            $this->db->where('rrord.created_at <=', $periode['end'].' 23:59:59');
        }
        else {
            $this->db->where('MONTH(rrord.created_at)', 'MONTH(CURDATE())', FALSE);
            $this->db->where('YEAR(rrord.created_at)', 'YEAR(CURDATE())', FALSE);
        }

        return $this->db
            ->join('obat o', 'o.id = rrord.obat_id')
            ->select('rrord.obat_id, o.nama as nama_obat, rrord.jumlah, o.harga_jual as harga')
            ->get('ri_resep_obat_racik_detail rrord')
            ->result();
    }
}
