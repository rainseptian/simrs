<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class KasirModel extends CI_Model {

    public function listKasir() {

        $this->db->select('u.*,ug.id user_grup_id, g.nama_grup');
        $this->db->from('user u');
        $this->db->join('user_grup ug', 'ug.user_id = u.id');
        $this->db->join('grup g', 'ug.grup_id = g.id');
        $this->db->where('u.is_active', 1);
        $this->db->where('g.nama_grup', 'kasir');

        return $this->db->get();
    }

    public function getUserById($id) {

        $this->db->select('u.*,ug.id as user_grup_id, ug.grup_id, g.nama_grup');
        $this->db->from('user u');
        $this->db->join('user_grup ug', 'ug.user_id = u.id');
        $this->db->join('grup g', 'ug.grup_id = g.id');
        $this->db->where('u.is_active', 1);
        $this->db->where('u.id', $id);

        return $this->db->get();
    }

    public function getGrup() {
        $this->db->where('is_active', 1);
        return $this->db->get('grup');
    }

    public function getListPemeriksaanForRekapKasirAllComplete($by, $periode = 'harian', $shift = -1, $mode = 'date')
    {
        // 1: 07:00 - 14:59 || 2: 15:00 - 22:59 || 3: 23:00 - 06:59

        $filter = [];
        if ($by == 'waktu') {
            if ($periode == 'harian') {
                $filter [] = "ba.created_at >= '" . date('Y-m-d') . " 00:00:00'";
                $filter [] = "ba.created_at <= '" . date('Y-m-d') . " 23:59:59'";
            }
            else if (is_array($periode)) {
                $filter [] = "ba.created_at >= '" . $periode['start'] . " 00:00:00'";
                $filter [] = "ba.created_at <= '" . $periode['end'] . " 23:59:59'";
            }
        }
        else {
            $filter []= "rj.pasien_id = $by";
        }

        $where = implode(' AND ', $filter);

        $ba = $this->db->query("
            SELECT ba.*,
                   CONCAT(
                           '[',
                           GROUP_CONCAT(
                                   JSON_OBJECT(
                                           'no_rm', rj.no_rm,
                                           'nama_pasien', rj.nama_pasien,
                                           'nama_dokter', rj.nama_dokter,
                                           'nama_poli', rj.nama_poli,
                                           'jaminan', rj.jaminan,
            
                                           'jasa_medis', rj.jasa_medis,
                                           'detail_jasa_medis', rj.detail_jasa_medis,
                                           'obat', rj.obat,
                                           'detail_obat', rj.detail_obat,
                                           'obat_racik', rj.obat_racik,
                                           'detail_obat_racik', rj.detail_obat_racik,
                                           'bhp', rj.bhp,
                                           'detail_bhp', rj.detail_bhp,
            
                                           'bayar_id', rj.bayar_id,
                                           'pemeriksaan_id', rj.pemeriksaan_id,
                                           'rawat_inap_id', rj.rawat_inap_id,
                                           'transfer_id', rj.transfer_id,
                                           'jenis_ruangan_id', rj.jenis_ruangan_id
                                       )
                               ),
                           ']'
                       ) AS bayar
            FROM bayar_all ba
            
            LEFT JOIN (
                (SELECT b.id as bayar_id, b.bayar_all_id, pem.created_at as waktu,
                        pem.id as pemeriksaan_id, '-1' as rawat_inap_id, '-1' as transfer_id, '-1' as jenis_ruangan_id,
                        p.no_rm, p.nama as nama_pasien, p.id as pasien_id, u.nama as nama_dokter,
                        jp.nama as nama_poli,
                        pp.jaminan,
                        IFNULL(t1.subtotal, 0) as jasa_medis, IFNULL(t1.detail, '[]') as detail_jasa_medis,
                        IFNULL(t2.subtotal, 0) as obat, IFNULL(t2.detail, '[]') as detail_obat,
                        IFNULL(t3.subtotal, 0) as obat_racik, IFNULL(t3.detail, '[]') as detail_obat_racik,
                        IFNULL(t4.subtotal, 0) as bhp, IFNULL(t4.detail, '[]') as detail_bhp
                 FROM bayar b
                          JOIN pemeriksaan pem ON pem.id = b.pemeriksaan_id and pem.is_active = 1
                          JOIN user u ON u.id = pem.dokter_id and u.is_active = 1
                          JOIN pasien p ON p.id = pem.pasien_id and p.is_active = 1
                          JOIN pendaftaran_pasien pp ON pp.id = pem.pendaftaran_id and pp.is_active = 1
                          JOIN jenis_pendaftaran jp ON jp.id = pp.jenis_pendaftaran_id and jp.status = 1
                          LEFT JOIN (
                     SELECT SUM(subtotal) as subtotal, bayar_id,
                            CONCAT(
                                    '[',
                                    GROUP_CONCAT(
                                            JSON_OBJECT(
                                                    'item', detail_bayar.item,
                                                    'item_id', detail_bayar.item_id,
                                                    'harga', detail_bayar.harga,
                                                    'jumlah', detail_bayar.jumlah,
                                                    'subtotal', detail_bayar.subtotal
                                                )
                                        ),
                                    ']'
                                ) AS detail
                     FROM detail_bayar
                     WHERE jenis_item = 'tindakan' GROUP BY bayar_id
                 ) as t1 ON t1.bayar_id = b.id
                          LEFT JOIN (
                     SELECT SUM(subtotal) as subtotal, bayar_id,
                            CONCAT(
                                    '[',
                                    GROUP_CONCAT(
                                            JSON_OBJECT(
                                                    'item', detail_bayar.item,
                                                    'item_id', detail_bayar.item_id,
                                                    'harga', detail_bayar.harga,
                                                    'jumlah', detail_bayar.jumlah,
                                                    'subtotal', detail_bayar.subtotal
                                                )
                                        ),
                                    ']'
                                ) AS detail
                     FROM detail_bayar
                     WHERE jenis_item = 'obat' GROUP BY bayar_id
                 ) as t2 ON t2.bayar_id = b.id
                          LEFT JOIN (
                     SELECT SUM(subtotal) as subtotal, bayar_id,
                            '[]' AS detail
                     FROM detail_bayar
                     WHERE jenis_item = 'obat racikan' GROUP BY bayar_id
                 ) as t3 ON t3.bayar_id = b.id
                          LEFT JOIN (
                     SELECT SUM(subtotal) as subtotal, bayar_id,
                            CONCAT(
                                    '[',
                                    GROUP_CONCAT(
                                            JSON_OBJECT(
                                                    'item', detail_bayar.item,
                                                    'item_id', detail_bayar.item_id,
                                                    'harga', detail_bayar.harga,
                                                    'jumlah', detail_bayar.jumlah,
                                                    'subtotal', detail_bayar.subtotal
                                                )
                                        ),
                                    ']'
                                ) AS detail
                     FROM detail_bayar
                     WHERE jenis_item = 'bahan habis pakai' GROUP BY bayar_id
                 ) as t4 ON t4.bayar_id = b.id)
                UNION ALL
                (SELECT b.id as bayar_id, b.bayar_all_id, r.created_at as waktu,
                        '-1' as pemeriksaan_id, r.id as rawat_inap_id, '-1' as transfer_id, b.jenis_ruangan_id,
                        p.no_rm, p.nama as nama_pasien, p.id as pasien_id, u.nama as nama_dokter,
                        'Ranap' as nama_poli,
                        IFNULL(r.tipe_pasien, '-') as jaminan,
                        IFNULL(t1.subtotal, 0) as jasa_medis, IFNULL(t1.detail, '[]') as detail_jasa_medis,
                        IFNULL(t2.subtotal, 0) as obat, IFNULL(t2.detail, '[]') as detail_obat,
                        IFNULL(t3.subtotal, 0) as obat_racik, IFNULL(t3.detail, '[]') as detail_obat_racik,
                        IFNULL(t4.subtotal, 0) as bhp, IFNULL(t4.detail, '[]') as detail_bhp
                 FROM ri_bayar b
                          JOIN rawat_inap r ON r.id = b.rawat_inap_id
                          JOIN pasien p ON p.id = r.pasien_id and p.is_active = 1
                          JOIN user u ON u.id = r.dokter_id and u.is_active = 1
                          LEFT JOIN (
                     SELECT SUM(subtotal) as subtotal, ri_bayar_id,
                            CONCAT(
                                    '[',
                                    GROUP_CONCAT(
                                            JSON_OBJECT(
                                                    'item', ri_bayar_detail.item,
                                                    'item_id', ri_bayar_detail.item_id,
                                                    'harga', ri_bayar_detail.harga,
                                                    'jumlah', ri_bayar_detail.jumlah,
                                                    'subtotal', ri_bayar_detail.subtotal
                                                )
                                        ),
                                    ']'
                                ) AS detail
                     FROM ri_bayar_detail
                     WHERE (jenis_item = 'tindakan' OR jenis_item = 'makanan' OR jenis_item = 'bed') GROUP BY ri_bayar_id
                 ) as t1 ON t1.ri_bayar_id = b.id
                          LEFT JOIN (
                     SELECT SUM(subtotal) as subtotal, ri_bayar_id,
                            CONCAT(
                                    '[',
                                    GROUP_CONCAT(
                                            JSON_OBJECT(
                                                    'item', ri_bayar_detail.item,
                                                    'item_id', ri_bayar_detail.item_id,
                                                    'harga', ri_bayar_detail.harga,
                                                    'jumlah', ri_bayar_detail.jumlah,
                                                    'subtotal', ri_bayar_detail.subtotal
                                                )
                                        ),
                                    ']'
                                ) AS detail
                     FROM ri_bayar_detail
                     WHERE jenis_item = 'obat' GROUP BY ri_bayar_id
                 ) as t2 ON t2.ri_bayar_id = b.id
                          LEFT JOIN (
                     SELECT SUM(subtotal) as subtotal, ri_bayar_id,
                            '[]' AS detail
                     FROM ri_bayar_detail
                     WHERE jenis_item = 'obat racikan' GROUP BY ri_bayar_id
                 ) as t3 ON t3.ri_bayar_id = b.id
                          LEFT JOIN (
                     SELECT SUM(subtotal) as subtotal, ri_bayar_id,
                            CONCAT(
                                    '[',
                                    GROUP_CONCAT(
                                            JSON_OBJECT(
                                                    'item', ri_bayar_detail.item,
                                                    'item_id', ri_bayar_detail.item_id,
                                                    'harga', ri_bayar_detail.harga,
                                                    'jumlah', ri_bayar_detail.jumlah,
                                                    'subtotal', ri_bayar_detail.subtotal
                                                )
                                        ),
                                    ']'
                                ) AS detail
                     FROM ri_bayar_detail
                     WHERE jenis_item = 'bahan habis pakai' GROUP BY ri_bayar_id
                 ) as t4 ON t4.ri_bayar_id = b.id
                 WHERE b.jenis_ruangan_id = 1)
                UNION ALL 
                (SELECT b.id as bayar_id, b.bayar_all_id, t.created_at as waktu,
                        '-1' as pemeriksaan_id, t.rawat_inap_id, t.id as transfer_id, b.jenis_ruangan_id,
                        p.no_rm, p.nama as nama_pasien, p.id as pasien_id, u.nama as nama_dokter,
                        IF(t.current_place_type = 'ruang bersalin', 'VK', IF(t.current_place_type = 'ruang operasi', 'OK', IF(t.current_place_type = 'hcu', 'HCU', 'Perinatologi'))) as nama_poli,
                        IFNULL(ri.tipe_pasien, '-') as jaminan,
                        IFNULL(t1.subtotal, 0) as jasa_medis, IFNULL(t1.detail, '[]') as detail_jasa_medis,
                        IFNULL(t2.subtotal, 0) as obat, IFNULL(t2.detail, '[]') as detail_obat,
                        IFNULL(t3.subtotal, 0) as obat_racik, IFNULL(t3.detail, '[]') as detail_obat_racik,
                        IFNULL(t4.subtotal, 0) as bhp, IFNULL(t4.detail, '[]') as detail_bhp
                 FROM ri_bayar b
                          JOIN transfer t ON t.id = b.transfer_id
                          JOIN rawat_inap ri ON ri.id = t.rawat_inap_id
                          JOIN pasien p ON p.id = t.pasien_id and p.is_active = 1
                          JOIN user u ON u.id = t.dokter_id and u.is_active = 1
                          LEFT JOIN (
                     SELECT SUM(subtotal) as subtotal, ri_bayar_id,
                            CONCAT(
                                    '[',
                                    GROUP_CONCAT(
                                            JSON_OBJECT(
                                                    'item', ri_bayar_detail.item,
                                                    'item_id', ri_bayar_detail.item_id,
                                                    'harga', ri_bayar_detail.harga,
                                                    'jumlah', ri_bayar_detail.jumlah,
                                                    'subtotal', ri_bayar_detail.subtotal
                                                )
                                        ),
                                    ']'
                                ) AS detail
                     FROM ri_bayar_detail
                     WHERE (jenis_item = 'tindakan' OR jenis_item = 'makanan' OR jenis_item = 'bed') GROUP BY ri_bayar_id
                 ) as t1 ON t1.ri_bayar_id = b.id
                          LEFT JOIN (
                     SELECT SUM(subtotal) as subtotal, ri_bayar_id,
                            CONCAT(
                                    '[',
                                    GROUP_CONCAT(
                                            JSON_OBJECT(
                                                    'item', ri_bayar_detail.item,
                                                    'item_id', ri_bayar_detail.item_id,
                                                    'harga', ri_bayar_detail.harga,
                                                    'jumlah', ri_bayar_detail.jumlah,
                                                    'subtotal', ri_bayar_detail.subtotal
                                                )
                                        ),
                                    ']'
                                ) AS detail
                     FROM ri_bayar_detail
                     WHERE jenis_item = 'obat' GROUP BY ri_bayar_id
                 ) as t2 ON t2.ri_bayar_id = b.id
                          LEFT JOIN (
                     SELECT SUM(subtotal) as subtotal, ri_bayar_id,
                            '[]' AS detail
                     FROM ri_bayar_detail
                     WHERE jenis_item = 'obat racikan' GROUP BY ri_bayar_id
                 ) as t3 ON t3.ri_bayar_id = b.id
                          LEFT JOIN (
                     SELECT SUM(subtotal) as subtotal, ri_bayar_id,
                            CONCAT(
                                    '[',
                                    GROUP_CONCAT(
                                            JSON_OBJECT(
                                                    'item', ri_bayar_detail.item,
                                                    'item_id', ri_bayar_detail.item_id,
                                                    'harga', ri_bayar_detail.harga,
                                                    'jumlah', ri_bayar_detail.jumlah,
                                                    'subtotal', ri_bayar_detail.subtotal
                                                )
                                        ),
                                    ']'
                                ) AS detail
                     FROM ri_bayar_detail
                     WHERE jenis_item = 'bahan habis pakai' GROUP BY ri_bayar_id
                 ) as t4 ON t4.ri_bayar_id = b.id
                 WHERE b.jenis_ruangan_id != 1)
                ORDER BY waktu
            ) as rj on rj.bayar_all_id = ba.id and rj.nama_pasien is not null
            
            WHERE $where
            GROUP BY ba.id
        ")->result();

        return $ba;
    }

    public function getListPemeriksaanForRekapKasirAll($by, $periode = 'harian', $shift = -1, $mode = 'date')
    {
        // 1: 07:00 - 14:59 || 2: 15:00 - 22:59 || 3: 23:00 - 06:59

        $filter = [];
        $filter_obat_luar = [];

        if ($by == 'waktu') {
            if ($periode == 'harian') {
                $filter []= "ba.created_at >= '" . date('Y-m-d') . " 00:00:00'";
                $filter []= "ba.created_at <= '" . date('Y-m-d') . " 23:59:59'";
                $filter_obat_luar []= "ba.created_at >= '" . date('Y-m-d') . " 00:00:00'";
                $filter_obat_luar []= "ba.created_at <= '" . date('Y-m-d') . " 23:59:59'";
            }
            else if (is_array($periode)) {
                $filter []= "ba.created_at >= '" . $periode['start'] . " 00:00:00'";
                $filter []= "ba.created_at <= '" . $periode['end'] . " 23:59:59'";
                $filter_obat_luar []= "ba.created_at >= '" . $periode['start'] . " 00:00:00'";
                $filter_obat_luar []= "ba.created_at <= '" . $periode['end'] . " 23:59:59'";
            }
        }
        else {
            $filter []= "rj.pasien_id = $by";
            $filter_obat_luar []= "pasien_id = $by";
        }

        $where = implode(' AND ', $filter);
        $where_obat_luar = implode(' AND ', $filter_obat_luar);

        $ba = $this->db->query("
            SELECT * FROM (
              SELECT ba.id, ba.jasa_racik, ba.diskon, ba.total, ba.kembalian, ba.created_at, rj.pasien_id, 
                   GROUP_CONCAT(rj.jenis SEPARATOR '^^^') as jenis, GROUP_CONCAT(rj.print_id SEPARATOR '^^^') as print_id,
                   CONCAT(
                       '[',
                           GROUP_CONCAT(
                               JSON_OBJECT(
                                   'no_rm', rj.no_rm,
                                   'nama_pasien', rj.nama_pasien,
                                   'nama_dokter', rj.nama_dokter,
                                   'nama_poli', rj.nama_poli,
                                   'jaminan', rj.jaminan,
                                   'jasa_medis', rj.jasa_medis,
                                   'obat', rj.obat,
                                   'obat_racik', rj.obat_racik,
                                   'bhp', rj.bhp,
                                   'bayar_id', rj.bayar_id,
                                   'pemeriksaan_id', rj.pemeriksaan_id,
                                   'rawat_inap_id', rj.rawat_inap_id,
                                   'transfer_id', rj.transfer_id,
                                   'jenis_ruangan_id', rj.jenis_ruangan_id
                               )
                           ),
                       ']'
                   ) AS bayar
            FROM bayar_all ba

            LEFT JOIN (
                (SELECT b.id as bayar_id, b.bayar_all_id, pem.created_at as waktu,
                       pem.id as pemeriksaan_id, '-1' as rawat_inap_id, '-1' as transfer_id, '-1' as jenis_ruangan_id,
                       p.no_rm, p.nama as nama_pasien, p.id as pasien_id, u.nama as nama_dokter,
                       jp.nama as nama_poli,
                       pp.jaminan,
                       IFNULL(t1.subtotal, 0) as jasa_medis, IFNULL(t2.subtotal, 0) as obat, IFNULL(t3.subtotal, 0) as obat_racik, IFNULL(t4.subtotal, 0) as bhp,
                       CONCAT('Poli - ', jp.nama) as jenis, pem.id as print_id
                FROM bayar b
                         JOIN pemeriksaan pem ON pem.id = b.pemeriksaan_id and pem.is_active = 1
                         JOIN user u ON u.id = pem.dokter_id and u.is_active = 1
                         JOIN pasien p ON p.id = pem.pasien_id and p.is_active = 1
                         JOIN pendaftaran_pasien pp ON pp.id = pem.pendaftaran_id and pp.is_active = 1
                         JOIN jenis_pendaftaran jp ON jp.id = pp.jenis_pendaftaran_id and jp.status = 1
                         LEFT JOIN (
                    SELECT SUM(subtotal) as subtotal, bayar_id FROM detail_bayar WHERE jenis_item = 'tindakan' GROUP BY bayar_id
                ) as t1 ON t1.bayar_id = b.id
                         LEFT JOIN (
                    SELECT SUM(subtotal) as subtotal, bayar_id FROM detail_bayar WHERE jenis_item = 'obat' GROUP BY bayar_id
                ) as t2 ON t2.bayar_id = b.id
                         LEFT JOIN (
                    SELECT SUM(subtotal) as subtotal, bayar_id FROM detail_bayar WHERE jenis_item = 'obat racikan' GROUP BY bayar_id
                ) as t3 ON t3.bayar_id = b.id
                         LEFT JOIN (
                    SELECT SUM(subtotal) as subtotal, bayar_id FROM detail_bayar WHERE jenis_item = 'bahan habis pakai' GROUP BY bayar_id
                ) as t4 ON t4.bayar_id = b.id)
            UNION ALL
                (SELECT b.id as bayar_id, b.bayar_all_id, r.created_at as waktu,
                       '-1' as pemeriksaan_id, r.id as rawat_inap_id, '-1' as transfer_id, b.jenis_ruangan_id,
                       p.no_rm, p.nama as nama_pasien, p.id as pasien_id, u.nama as nama_dokter,
                       'Ranap' as nama_poli,
                       IFNULL(r.tipe_pasien, '-') as jaminan,
                       IFNULL(t1.subtotal, 0) as jasa_medis, IFNULL(t2.subtotal, 0) as obat, IFNULL(t3.subtotal, 0) as obat_racik, IFNULL(t4.subtotal, 0) as bhp, 
                       'Ranap' as jenis, r.id as print_id
                FROM ri_bayar b
                         JOIN rawat_inap r ON r.id = b.rawat_inap_id
                         JOIN pasien p ON p.id = r.pasien_id and p.is_active = 1
                         JOIN user u ON u.id = r.dokter_id and u.is_active = 1
                         LEFT JOIN (
                    SELECT SUM(subtotal) as subtotal, ri_bayar_id FROM ri_bayar_detail WHERE (jenis_item = 'tindakan' OR jenis_item = 'makanan' OR jenis_item = 'bed') GROUP BY ri_bayar_id
                ) as t1 ON t1.ri_bayar_id = b.id
                         LEFT JOIN (
                    SELECT SUM(subtotal) as subtotal, ri_bayar_id FROM ri_bayar_detail WHERE jenis_item = 'obat' GROUP BY ri_bayar_id
                ) as t2 ON t2.ri_bayar_id = b.id
                         LEFT JOIN (
                    SELECT SUM(subtotal) as subtotal, ri_bayar_id FROM ri_bayar_detail WHERE jenis_item = 'obat racikan' GROUP BY ri_bayar_id
                ) as t3 ON t3.ri_bayar_id = b.id
                         LEFT JOIN (
                    SELECT SUM(subtotal) as subtotal, ri_bayar_id FROM ri_bayar_detail WHERE jenis_item = 'bahan habis pakai' GROUP BY ri_bayar_id
                ) as t4 ON t4.ri_bayar_id = b.id
                WHERE b.jenis_ruangan_id = 1)
            UNION ALL
                (SELECT b.id as bayar_id, b.bayar_all_id, t.created_at as waktu,
                       '-1' as pemeriksaan_id, t.rawat_inap_id, t.id as transfer_id, b.jenis_ruangan_id,
                       p.no_rm, p.nama as nama_pasien, p.id as pasien_id, u.nama as nama_dokter,
                       IF(t.current_place_type = 'ruang bersalin', 'VK', IF(t.current_place_type = 'ruang operasi', 'OK', IF(t.current_place_type = 'hcu', 'HCU', 'Perinatologi'))) as nama_poli,
                       IFNULL(ri.tipe_pasien, '-') as jaminan,
                       IFNULL(t1.subtotal, 0) as jasa_medis, IFNULL(t2.subtotal, 0) as obat, IFNULL(t3.subtotal, 0) as obat_racik, IFNULL(t4.subtotal, 0) as bhp,
                       (CASE
                            WHEN t.current_place_type = 'ruang bersalin' THEN 'VK'
                            WHEN t.current_place_type = 'ruang operasi' THEN 'OK'
                            WHEN t.current_place_type = 'hcu' THEN 'HCU'
                            WHEN t.current_place_type = 'perinatologi' THEN 'Perinatologi'
                       END) as jenis, t.id as print_id
                FROM ri_bayar b
                         JOIN transfer t ON t.id = b.transfer_id
                         JOIN rawat_inap ri ON t.rawat_inap_id = ri.id
                         JOIN pasien p ON p.id = t.pasien_id and p.is_active = 1
                         JOIN user u ON u.id = t.dokter_id and u.is_active = 1
                         LEFT JOIN (
                    SELECT SUM(subtotal) as subtotal, ri_bayar_id FROM ri_bayar_detail WHERE (jenis_item = 'tindakan' OR jenis_item = 'makanan' OR jenis_item = 'bed') GROUP BY ri_bayar_id
                ) as t1 ON t1.ri_bayar_id = b.id
                         LEFT JOIN (
                    SELECT SUM(subtotal) as subtotal, ri_bayar_id FROM ri_bayar_detail WHERE jenis_item = 'obat' GROUP BY ri_bayar_id
                ) as t2 ON t2.ri_bayar_id = b.id
                         LEFT JOIN (
                    SELECT SUM(subtotal) as subtotal, ri_bayar_id FROM ri_bayar_detail WHERE jenis_item = 'obat racikan' GROUP BY ri_bayar_id
                ) as t3 ON t3.ri_bayar_id = b.id
                         LEFT JOIN (
                    SELECT SUM(subtotal) as subtotal, ri_bayar_id FROM ri_bayar_detail WHERE jenis_item = 'bahan habis pakai' GROUP BY ri_bayar_id
                ) as t4 ON t4.ri_bayar_id = b.id
                WHERE b.jenis_ruangan_id != 1)
            UNION ALL
                (SELECT ba.id as bayar_id, ba.bayar_all_id, pa.created_at as waktu,
                       pa.id as pemeriksaan_id, '-1' as rawat_inap_id, '-1' as transfer_id, '-1' as jenis_ruangan_id,
                       p.no_rm, p.nama as nama_pasien, p.id as pasien_id, '' as nama_dokter,
                       CONVERT(concat('Ambulance - Rp ', FORMAT(if(pa.jaminan = 'bpjs', ta.tarif_bpjs, ta.tarif),2,'de_DE')) USING utf8) as nama_poli,
                       pa.jaminan,
                       0 as jasa_medis, 0 as obat, 0 as obat_racik, 0 as bhp,
                       'Ambulance' as jenis, pa.id as print_id
                FROM bayar_ambulance ba
                LEFT JOIN panggilan_ambulance pa on pa.id = ba.panggilan_id
                LEFT JOIN vehicles v on pa.vehicle_id = v.id
                LEFT JOIN tarif_ambulance ta on pa.tarif_ambulance_id = ta.id
                JOIN pasien p on pa.pasien_id = p.id
                WHERE ba.bayar_all_id IS NOT NULL)
            ORDER BY waktu
            ) as rj on rj.bayar_all_id = ba.id and rj.nama_pasien is not null

            WHERE $where
            GROUP BY ba.id

            UNION ALL
                (SELECT ba.id, ba.jasa_racik, ba.diskon, ba.total, ba.kembalian, ba.created_at, p.id as pasien_id, 
                        'resep_luar' as jenis, pol.id as print_id,
                        concat('[', json_object(
                           'no_rm', coalesce(p.no_rm, '-'),
                           'nama_pasien', pol.nama_pasien,
                           'nama_dokter', '',
                           'nama_poli', replace(pol.tipe, '_', ' '),
                           'jaminan', '-',
                           'jasa_medis', 0,
                           'obat', IFNULL(t1.subtotal, 0),
                           'obat_racik', IFNULL(t2.subtotal, 0),
                           'bhp', 0,
                           'bayar_id', ba.id,
                           'pemeriksaan_id', pol.id,
                           'rawat_inap_id', '-1',
                           'transfer_id', '-1',
                           'jenis_ruangan_id', 'obat_luar'
                        ), ']') as bayar
                FROM bayar_obat_luar ba
                JOIN penjualan_obat_luar pol on ba.penjualan_obat_luar_id = pol.id
                LEFT JOIN pemeriksaan pem on pem.id = pol.pemeriksaan_id
                LEFT JOIN pasien p on p.id = pem.pasien_id
                LEFT JOIN (
                    SELECT SUM(subtotal) as subtotal, bayar_obat_luar_id FROM detail_bayar_obat_luar WHERE jenis_item = 'obat' GROUP BY bayar_obat_luar_id
                ) as t1 ON t1.bayar_obat_luar_id = ba.id
                LEFT JOIN (
                    SELECT SUM(subtotal) as subtotal, bayar_obat_luar_id FROM detail_bayar_obat_luar WHERE jenis_item = 'obat racikan' GROUP BY bayar_obat_luar_id
                ) as t2 ON t2.bayar_obat_luar_id = ba.id
                WHERE $where_obat_luar)
            ) as t
            ORDER BY created_at
        ")->result();

        return $ba;
    }
}