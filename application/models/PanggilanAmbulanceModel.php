<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PanggilanAmbulanceModel extends CI_Model
{
    public function getBaru()
    {
        return $this->db
            ->select('pa.*, CONCAT(v.vehicle_model, " - ", v.vehicle_no) as ambulance, ta.tujuan, ta.tarif, p.no_rm')
            ->where('pa.is_active', 1)
            ->where('pa.status', 'baru')
            ->join('vehicles v', 'v.id = pa.vehicle_id')
            ->join('tarif_ambulance ta', 'ta.id = pa.tarif_ambulance_id')
            ->join('pasien p', 'p.id = pa.pasien_id')
            ->get('panggilan_ambulance pa')
            ->result();
    }

    public function getSelesai($periode, $jaminan = '')
    {
        if ($jaminan) {
            $this->db->where('jaminan', $jaminan);
        }

        return $this->db
            ->select('pa.*, CONCAT(v.vehicle_model, " - ", v.vehicle_no) as ambulance, ta.tujuan, ta.tarif')
            ->where('pa.is_active', 1)
            ->where('pa.status', 'selesai')
            ->where('pa.created_at >= ', $periode['start'] . " 00:00:00")
            ->where('pa.created_at <= ', $periode['end'] . " 23:59:59")
            ->join('vehicles v', 'v.id = pa.vehicle_id')
            ->join('tarif_ambulance ta', 'ta.id = pa.tarif_ambulance_id')
            ->get('panggilan_ambulance pa')
            ->result();
    }

    public function getBayarByBayarId($id)
    {
        return $this->db
            ->select('ba.*, pa.*, CONCAT(v.vehicle_model, " - ", v.vehicle_no) as ambulance, ta.tujuan, ta.tarif')
            ->from('bayar_ambulance ba')
            ->join('panggilan_ambulance pa', 'ba.panggilan_id = pa.id')
            ->join('vehicles v', 'v.id = pa.vehicle_id')
            ->join('tarif_ambulance ta', 'ta.id = pa.tarif_ambulance_id')
            ->where('ba.id', $id)
            ->get()
            ->row();
    }
}
