<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PengeluaranModel extends CI_Model
{
    public function get_list_items()
    {
        return $this->db->order_by('id', 'desc')->where('is_active', '1')->get('pengeluaran_item');
    }

    public function find_item($id)
    {
        return $this->db->where('id', $id)->where('is_active', '1')->get('pengeluaran_item')->row();
    }

    public function get_pengeluaran_rutin_harian_by_id($id)
    {
        return $this->db->where('pengeluaran_rutin_harian.is_active', '1')
            ->select('pengeluaran_rutin_harian.*, pengeluaran_item.nama as nama_item')
            ->join('pengeluaran_item', 'pengeluaran_item.id = pengeluaran_rutin_harian.pengeluaran_item_id')
            ->where('pengeluaran_rutin_harian.id', $id)
            ->get('pengeluaran_rutin_harian')
            ->row();
    }

    public function get_pengeluaran_rutin_harian()
    {
        $m = date('m');
        $y = date('Y');
        return $this->db->where('pengeluaran_rutin_harian.is_active', '1')
            ->select('pengeluaran_rutin_harian.*, pengeluaran_item.nama as nama_item')
            ->join('pengeluaran_item', 'pengeluaran_item.id = pengeluaran_rutin_harian.pengeluaran_item_id')
            ->where("MONTH(pengeluaran_rutin_harian.d_date) = {$m}")
            ->where("YEAR(pengeluaran_rutin_harian.d_date) = {$y}")
            ->get('pengeluaran_rutin_harian')
            ->result();
    }

    public function get_pengeluaran_rutin_harian_by_periode($start_date, $end_date)
    {
        return $this->db->where('pengeluaran_rutin_harian.is_active', '1')
            ->select('pengeluaran_rutin_harian.*, pengeluaran_item.nama as nama_item')
            ->join('pengeluaran_item', 'pengeluaran_item.id = pengeluaran_rutin_harian.pengeluaran_item_id')
            ->where('pengeluaran_rutin_harian.d_date >=', "$start_date 00:00:00")
            ->where('pengeluaran_rutin_harian.d_date <=', "$end_date 23:59:59")
            ->get('pengeluaran_rutin_harian')
            ->result();
    }

}