<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class RuangOperasiModel extends CI_Model
{
    public function getBaru()
    {
        return $this->db
            ->select('
                r.id, r.tipe_pasien, t.id as transfer_id, t.transfered_at, p.nama as nama_pasien, p.no_rm, p.alamat, 
                u.nama as nama_dokter, bed.name as bed_name, bed_type.name as bed_type_name,floor.name as floor_name, 
                bed_group.name as bedgroup, jp.nama as nama_poli, t.via_bangsal, t.jenis, t.operasi_at
            ')
            ->from('transfer t')
            ->join('rawat_inap r', 'r.id = t.rawat_inap_id')
            ->join('pasien p', 'p.id = r.pasien_id')
            ->join('user u', 'u.id = r.dokter_id')
            ->join('jenis_pendaftaran jp', 'jp.id = r.from_poli_id', 'left')
            ->join('bed', 'bed.id = t.dari', 'left')
            ->join('bed_type', 'bed.bed_type_id = bed_type.id', 'left')
            ->join('bed_group', 'bed.bed_group_id = bed_group.id', 'left')
            ->join('floor', 'floor.id = bed_group.floor', 'left')
            ->where('r.is_active', '1')
            ->where('t.is_active', '1')
            ->where('t.status', 'ditransfer')
            ->where('t.current_place_type', 'ruang operasi')
            ->order_by('t.id', 'desc')
            ->get()
            ->result_array();
    }

    public function getForBilling()
    {
        return $this->db
            ->select('
                r.*, t.id as transfer_id, t.transfered_at, t.diperiksa_at, t.total_biaya, p.nama as nama_pasien, p.no_rm, p.alamat, u.nama as nama_dokter, jp.nama as nama_poli,
                bed.name as bed_name, bed_type.name as bed_type_name,floor.name as floor_name, bed_group.name as bedgroup, t.via_bangsal
            ')
            ->from('transfer t')
            ->join('rawat_inap r', 'r.id = t.rawat_inap_id')
            ->join('pasien p', 'p.id = r.pasien_id and p.is_active = 1')
            ->join('user u', 'u.id = r.dokter_id')
            ->join('jenis_pendaftaran jp', 'jp.id = r.dari', 'left')
            ->join('bed', 'bed.id = r.tujuan', 'left')
            ->join('bed_type', 'bed.bed_type_id = bed_type.id', 'left')
            ->join('bed_group', 'bed.bed_group_id = bed_group.id', 'left')
            ->join('floor', 'floor.id = bed_group.floor', 'left')
            ->where('r.is_active', '1')
            ->where('t.is_active', '1')
            ->where('t.current_place_type', 'ruang operasi')
            ->where('t.status', 'diperiksa')
            ->order_by('t.id', 'desc')
            ->get()
            ->result_array();
    }

    public function getSelesai()
    {
        return $this->db
            ->select('
                r.*, t.transfered_at, t.diperiksa_at, t.selesai_at, t.total_biaya, p.nama as nama_pasien, p.no_rm, p.alamat, u.nama as nama_dokter, jp.nama as nama_poli,
                bed.name as bed_name, bed_type.name as bed_type_name,floor.name as floor_name, bed_group.name as bedgroup, t.via_bangsal, b.id as bayar_id
            ')
            ->from('transfer t')
            ->join('ri_bayar b', 't.id = b.transfer_id and b.jenis_ruangan_id = 3')
            ->join('rawat_inap r', 'r.id = t.rawat_inap_id')
            ->join('pasien p', 'p.id = r.pasien_id')
            ->join('user u', 'u.id = r.dokter_id')
            ->join('jenis_pendaftaran jp', 'jp.id = r.dari', 'left')
            ->join('bed', 'bed.id = r.tujuan', 'left')
            ->join('bed_type', 'bed.bed_type_id = bed_type.id', 'left')
            ->join('bed_group', 'bed.bed_group_id = bed_group.id', 'left')
            ->join('floor', 'floor.id = bed_group.floor', 'left')
            ->where('r.is_active', '1')
            ->where('t.is_active', '1')
            ->where('t.status', 'selesai')
            ->where('t.current_place_type', 'ruang operasi')
            ->order_by('t.id', 'desc')
            ->get()
            ->result_array();
    }

    public function getSelesaiByPasienId($id)
    {
        return $this->db
            ->select('
                r.*, t.id as transfer_id, t.transfered_at, t.diperiksa_at, t.selesai_at, t.total_biaya, p.nama as nama_pasien, p.no_rm, p.alamat, u.nama as nama_dokter, jp.nama as nama_poli,
                bed.name as bed_name, bed_type.name as bed_type_name,floor.name as floor_name, bed_group.name as bedgroup, t.via_bangsal
            ')
            ->from('transfer t')
            ->join('rawat_inap r', 'r.id = t.rawat_inap_id')
            ->join('pasien p', 'p.id = r.pasien_id')
            ->join('user u', 'u.id = r.dokter_id')
            ->join('jenis_pendaftaran jp', 'jp.id = r.dari', 'left')
            ->join('bed', 'bed.id = r.tujuan', 'left')
            ->join('bed_type', 'bed.bed_type_id = bed_type.id', 'left')
            ->join('bed_group', 'bed.bed_group_id = bed_group.id', 'left')
            ->join('floor', 'floor.id = bed_group.floor', 'left')
            ->where('r.is_active', '1')
            ->where('r.pasien_id', $id)
            ->where('t.status', 'selesai')
            ->where('t.current_place_type', 'ruang operasi')
            ->order_by('t.id', 'desc')
            ->get()
            ->result_array();
    }

    public function getById($id)
    {
        return $this->db
            ->select('
                r.*, p.nama as nama_pasien, p.no_rm, p.alamat, u.id as dokter_id, u.nama as nama_dokter, jp.nama as nama_poli,
                bed.name as bed_name, bed_type.name as bed_type_name,floor.name as floor_name, bed_group.name as bedgroup, 
                bed_group.price as bedgroup_price, bed.id as bed_id, t.catatan, t.via_bangsal, t.jenis
            ')
            ->from('rawat_inap r')
            ->join('transfer t', 't.id = r.transfer_id')
            ->join('pasien p', 'p.id = r.pasien_id')
            ->join('user u', 'u.id = r.dokter_id')
            ->join('jenis_pendaftaran jp', 'jp.id = r.dari', 'left')
            ->join('bed', 'bed.id = r.tujuan', 'left')
            ->join('bed_type', 'bed.bed_type_id = bed_type.id', 'left')
            ->join('bed_group', 'bed.bed_group_id = bed_group.id', 'left')
            ->join('floor', 'floor.id = bed_group.floor', 'left')
            ->where('r.is_active', '1')
            ->where('r.id', $id)
            ->get()
            ->row();
    }

    public function getByTransferId($id)
    {
        return $this->db
            ->select('
                r.*, p.nama as nama_pasien, p.no_rm, p.alamat, u.id as dokter_id, u.nama as nama_dokter, jp.nama as nama_poli,
                bed.name as bed_name, bed_type.name as bed_type_name,floor.name as floor_name, bed_group.name as bedgroup, 
                bed_group.price as bedgroup_price, bed.id as bed_id, t.catatan, t.via_bangsal
            ')
            ->from('rawat_inap r')
            ->join('transfer t', 't.id = r.transfer_id')
            ->join('pasien p', 'p.id = r.pasien_id')
            ->join('user u', 'u.id = r.dokter_id')
            ->join('jenis_pendaftaran jp', 'jp.id = r.dari', 'left')
            ->join('bed', 'bed.id = r.tujuan')
            ->join('bed_type', 'bed.bed_type_id = bed_type.id')
            ->join('bed_group', 'bed.bed_group_id = bed_group.id')
            ->join('floor', 'floor.id = bed_group.floor')
            ->where('r.is_active', '1')
            ->where('r.transfer_id', $id)
            ->get()
            ->row();
    }

    public function getDataTransferMasuk()
    {
        return $this->db
            ->select('
                t.id, t.tanggal, t.pukul, p.nama as nama_pasien, p.no_rm, p.alamat, u.nama as nama_dokter, 
                t.updated_at, bed.name as bed_name, bed_group.name as bedgroup, t.via_bangsal, jp.nama as nama_poli
            ')
            ->from('transfer t')
            ->join('pasien p', 'p.id = t.pasien_id')
            ->join('user u', 'u.id = t.dokter_id')
            ->join('jenis_pendaftaran jp', 'jp.id = t.dari', 'left')
            ->join('bed', 'bed.id = t.dari', 'left')
            ->join('bed_type', 'bed.bed_type_id = bed_type.id', 'left')
            ->join('bed_group', 'bed.bed_group_id = bed_group.id', 'left')
            ->join('floor', 'floor.id = bed_group.floor', 'left')
            ->join('jenis_transfer jt', 'jt.id = t.jenis')
            ->where('jt.ke_jenis_ruangan_id', '3')
            ->order_by('t.id', 'desc')
            ->get()
            ->result_array();
    }

    public function getDataTransferKeluar()
    {
        return $this->db
            ->select('
                t.id, t.tanggal, t.pukul, p.nama as nama_pasien, p.no_rm, p.alamat, u.nama as nama_dokter, 
                t.updated_at, bed.name as bed_name, bed_group.name as bedgroup, t.via_bangsal, jp.nama as nama_poli
            ')
            ->from('transfer t')
            ->join('pasien p', 'p.id = t.pasien_id')
            ->join('user u', 'u.id = t.dokter_id')
            ->join('bed', 'bed.id = t.tujuan', 'left')
            ->join('jenis_pendaftaran jp', 'jp.id = t.dari', 'left')
            ->join('bed_type', 'bed.bed_type_id = bed_type.id', 'left')
            ->join('bed_group', 'bed.bed_group_id = bed_group.id', 'left')
            ->join('floor', 'floor.id = bed_group.floor', 'left')
            ->join('jenis_transfer jt', 'jt.id = t.jenis')
            ->where('jt.dari_jenis_ruangan_id', '3')
            ->order_by('t.id', 'desc')
            ->get()
            ->result_array();
    }

    public function getPasienById($id)
    {
        return $this->db->select('*')->from('pasien')->where('id', $id)->get()->row();
    }

    public function getPemeriksaanByRawatInapId($id)
    {
        return $this->db->select('p.*, u.nama as nama_dokter')
            ->from('ri_pemeriksaan p')
            ->join('user u', 'u.id = p.dokter_id')
            ->where('p.rawat_inap_id', $id)
            ->get()
            ->result();
    }

    public function getPemeriksaanByTransferId($transfer_id, $jenis_ruangan_id)
    {
        return $this->db->select('p.*, u.nama as nama_dokter')
            ->from('ri_pemeriksaan p')
            ->join('user u', 'u.id = p.dokter_id')
            ->where('p.transfer_id', $transfer_id)
            ->where('p.jenis_ruangan_id', $jenis_ruangan_id)
            ->get()
            ->result();
    }

    public function getDiagnosisByRawatInapId($id)
    {
        return $this->db
            ->where('r.rawat_inap_id', $id)
            ->get('ri_diagnosis r')
            ->result();
    }

    public function getDiagnosisByTransferId($transfer_id, $jenis_ruangan_id)
    {
        return $this->db
            ->where('r.transfer_id', $transfer_id)
            ->where('r.jenis_ruangan_id', $jenis_ruangan_id)
            ->get('ri_diagnosis r')
            ->result();
    }

    public function getPenyakitByDiagnosisId($id)
    {
        return $this->db
            ->where('dp.ri_diagnosis_id', $id)
            ->join('penyakit p', 'p.id = dp.penyakit_id')
            ->get('ri_diagnosis_penyakit dp')
            ->result();
    }

    public function getResepByRawatInapId($id)
    {
        return $this->db
            ->where('rawat_inap_id', $id)
            ->get('ri_resep')
            ->result();
    }

    public function getResepByTransferId($transfer_id, $jenis_ruangan_id)
    {
        return $this->db
            ->where('transfer_id', $transfer_id)
            ->where('jenis_ruangan_id', $jenis_ruangan_id)
            ->get('ri_resep')
            ->result();
    }

    public function getBhpByRawatInapId($id)
    {
        return $this->db
            ->where('rawat_inap_id', $id)
            ->get('ri_bahan_habis_pakai')
            ->result();
    }

    public function getBhpByTransferId($transfer_id, $jenis_ruangan_id)
    {
        return $this->db
            ->where('transfer_id', $transfer_id)
            ->where('jenis_ruangan_id', $jenis_ruangan_id)
            ->get('ri_bahan_habis_pakai')
            ->result();
    }

    public function getBiayaByRawatInapId($id)
    {
        return $this->db
            ->where('rawat_inap_id', $id)
            ->get('ri_biaya')
            ->result();
    }

    public function getBiayaByTransferId($transfer_id, $jenis_ruangan_id)
    {
        return $this->db
            ->where('transfer_id', $transfer_id)
            ->where('jenis_ruangan_id', $jenis_ruangan_id)
            ->get('ri_biaya')
            ->result();
    }

    public function getObatByResepId($id)
    {
        return $this->db
            ->select('o.nama, o.harga_jual, ro.jumlah, ro.signa')
            ->join('obat o', 'o.id = ro.obat_id')
            ->where('ro.ri_resep_id', $id)
            ->get('ri_resep_obat ro')
            ->result();
    }

    public function getBahanByBhpId($id)
    {
        return $this->db
            ->select('bhp.nama, bhpd.jumlah, bhp.harga_jual')
            ->join('bahan_habis_pakai bhp', 'bhp.id = bhpd.bahan_id')
            ->where('bhpd.ri_bahan_habis_pakai_id', $id)
            ->get('ri_bahan_habis_pakai_detail bhpd')
            ->result();
    }

    public function getObatRacikByResepId($id)
    {
        return $this->db
            ->select('ror.id, ror.nama_racikan, ror.signa, ror.catatan')
            ->where('ror.ri_resep_id', $id)
            ->get('ri_resep_obat_racik ror')
            ->result();
    }

    public function getObatRacikDetailByResepObatRacikId($id)
    {
        return $this->db
            ->select('o.nama, o.harga_jual, rord.jumlah')
            ->join('obat o', 'o.id = rord.obat_id')
            ->where('rord.ri_resep_obat_racik_id', $id)
            ->get('ri_resep_obat_racik_detail rord')
            ->result();
    }

    public function getAllPenyakit()
    {
        return $this->db->where('is_active', '1')->get('penyakit')->result();
    }

    public function getAllTindakan()
    {
        return $this->db->where('is_active', '1')->get('tarif_tindakan')->result();
    }

    public function getAllTipeMakanan()
    {
        return $this->db->where('is_active', '1')->get('tipe_makanan')->result();
    }

    public function getMakananById($id)
    {
        return $this->db->where('is_active', '1')->where('id', $id)->get('makanan')->row();
    }

    public function getMakananByTipeId($id)
    {
        return $this->db->where('is_active', '1')
            ->where('tipe_makanan_id', $id)
            ->get('makanan')
            ->result();
    }

    public function getMakananCountByRawatInapId($id)
    {
        return $this->db
            ->where('b.jenis_biaya', 'Makanan')
            ->where('b.rawat_inap_id', $id)
            ->group_by('m.id')
            ->join('makanan m', 'm.id = b.makanan_id')
            ->select('m.nama, m.harga, count(m.id) as jumlah, (count(m.id) * m.harga) as total')
            ->get('ri_biaya b')
            ->result();
    }

    public function getMakananCountByTransferId($transfer_id, $jenis_ruangan_id)
    {
        return $this->db
            ->where('b.jenis_biaya', 'Makanan')
            ->where('b.transfer_id', $transfer_id)
            ->where('b.jenis_ruangan_id', $jenis_ruangan_id)
            ->group_by('m.id')
            ->join('makanan m', 'm.id = b.makanan_id')
            ->select('m.nama, m.harga, count(m.id) as jumlah, (count(m.id) * m.harga) as total')
            ->get('ri_biaya b')
            ->result();
    }

    public function getTindakanHargaByIds($id)
    {
        return $this->db->where('is_active', '1')
            ->where_in('id', $id)
            ->select('tarif_pasien')
            ->get('tarif_tindakan')
            ->result();
    }

    public function getTindakanByBiayaId($id)
    {
        return $this->db
            ->where('bt.ri_biaya_id', $id)
            ->select('tt.nama, tt.tarif_pasien')
            ->join('tarif_tindakan tt', 'tt.id = bt.tindakan_id')
            ->get('ri_biaya_tindakan bt')
            ->result();
    }

    public function getBayarByRawatInapId($id)
    {
        return $this->db
            ->where('b.rawat_inap_id', $id)
            ->select('b.*')
            ->get('ri_bayar b')
            ->row();
    }

    public function getBayarByTransferId($transfer_id, $jenis_ruangan_id)
    {
        return $this->db
            ->where('b.transfer_id', $transfer_id)
            ->where('b.jenis_ruangan_id', $jenis_ruangan_id)
            ->select('b.*')
            ->get('ri_bayar b')
            ->row();
    }

    public function getBayarById($id)
    {
        return $this->db
            ->where('b.id', $id)
            ->select('b.*')
            ->get('ri_bayar b')
            ->row();
    }

    public function getTransferById($id)
    {
        return $this->db
            ->where('id', $id)
            ->get('transfer')
            ->row();
    }

    public function getLastOperasiTransferByRawatInapId($id)
    {
        return $this->db
            ->limit(1)
            ->order_by('t.id', 'desc')
            ->where('t.rawat_inap_id', $id)
            ->where('t.current_place_type', 'ruang operasi')
            ->select('t.*')
            ->get('transfer t')
            ->row();
    }
}
