<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class RawatInapModel extends CI_Model
{
    public function getBaru()
    {
//        return $this->db
//            ->select('
//                r.*, p.nama as nama_pasien, p.no_rm, p.alamat, u.nama as nama_dokter, jp.nama as nama_poli,
//                bed.name as bed_name, bed_type.name as bed_type_name,floor.name as floor_name, bed_group.name as bedgroup
//            ')
//            ->from('rawat_inap r')
//            ->join('pasien p', 'p.id = r.pasien_id')
//            ->join('user u', 'u.id = r.dokter_id')
//            ->join('jenis_pendaftaran jp', 'jp.id = r.from_poli_id', 'left')
//            ->join('bed', 'bed.id = r.tujuan', 'left')
//            ->join('bed_type', 'bed.bed_type_id = bed_type.id', 'left')
//            ->join('bed_group', 'bed.bed_group_id = bed_group.id', 'left')
//            ->join('floor', 'floor.id = bed_group.floor')
//            ->where('r.is_active', '1')
//            ->where('r.status', 'Masuk')
//            ->get()
//            ->result_array();
        return $this->db
            ->select('
                r.*, p.nama as nama_pasien, p.no_rm, p.alamat, u.nama as nama_dokter, jp.nama as nama_poli,
                bed.name as bed_name, bed_type.name as bed_type_name,floor.name as floor_name, bed_group.name as bedgroup
            ')
            ->from('rawat_inap r')
            ->join('pasien p', 'p.id = r.pasien_id')
            ->join('user u', 'u.id = r.dokter_id')
            ->join('jenis_pendaftaran jp', 'jp.id = r.from_poli_id', 'left')
            ->join('bed', 'bed.id = r.tujuan', 'left')
            ->join('bed_type', 'bed.bed_type_id = bed_type.id', 'left')
            ->join('bed_group', 'bed.bed_group_id = bed_group.id', 'left')
            ->join('floor', 'floor.id = bed_group.floor', 'left')
            ->where('r.tujuan !=', 'Ruang Operasi')
            ->where('r.tujuan !=', 'Ruang Bersalin')
            ->where('r.via_bangsal', '1')
            ->where('r.is_active', '1')
            ->where('r.status', 'Masuk')
            ->get()
            ->result_array();
    }

    public function getNotRanapBaru($current_place_type)
    {
        return $this->db
            ->select('
                r.id, r.tipe_pasien, t.id as transfer_id, t.transfered_at, p.nama as nama_pasien, p.no_rm, p.alamat, 
                u.nama as nama_dokter, bed.name as bed_name, bed_type.name as bed_type_name,floor.name as floor_name, 
                bed_group.name as bedgroup, jp.nama as nama_poli, t.via_bangsal
            ')
            ->from('transfer t')
            ->join('rawat_inap r', 'r.id = t.rawat_inap_id')
            ->join('user u', 'u.id = r.dokter_id')
            ->join('jenis_pendaftaran jp', 'jp.id = r.from_poli_id', 'left')
            ->join('bed', 'bed.id = r.dari', 'left')
            ->join('bed_type', 'bed.bed_type_id = bed_type.id', 'left')
            ->join('bed_group', 'bed.bed_group_id = bed_group.id', 'left')
            ->join('floor', 'floor.id = bed_group.floor', 'left')
            ->where('r.is_active', '1')
            ->where('t.is_active', '1')
            ->where('t.status', 'ditransfer')
            ->where('t.current_place_type', $current_place_type)
            ->order_by('t.id', 'desc')
            ->get()
            ->result_array();
    }

    public function getRawatInapPemeriksaanKonsul($pemeriksaanId) 
    {
        $this->db->select('dtp.*, td.id as id_tarif_rindakan, td.nama , td.tarif_pasien, td.id, u.id as dokter_id, u.nama as nama_dokter');
        $this->db->join('tarif_tindakan td', 'td.id = dtp.tarif_tindakan_id and td.is_active = 1');
        $this->db->join('ri_pemeriksaan pem', 'pem.id = dtp.ri_pemeriksaan_id and dtp.is_active = 1');
        $this->db->join('user u', 'u.id = dtp.dokter_id and u.is_active = 1');
        $this->db->where('pem.id', $pemeriksaanId );
        $this->db->where('dtp.is_active', '1' );
        return  $this->db->get('ri_detail_tindakan_konsul dtp')->result();
    }

    public function getBolehPulang()
    {
        return $this->db
            ->select('
                r.*, p.nama as nama_pasien, p.no_rm, p.alamat, u.nama as nama_dokter, jp.nama as nama_poli,
                bed.name as bed_name, bed_type.name as bed_type_name,floor.name as floor_name, bed_group.name as bedgroup
            ')
            ->from('rawat_inap r')
            ->join('pasien p', 'p.id = r.pasien_id')
            ->join('user u', 'u.id = r.dokter_id')
            ->join('jenis_pendaftaran jp', 'jp.id = r.from_poli_id', 'left')
            ->join('transfer t', 't.rawat_inap_id = r.id AND t.current_place_type = \'bangsal\'')
            ->join('bed', 'bed.id = t.tujuan', 'left') // <------ asale r.tujuan
            ->join('bed_type', 'bed.bed_type_id = bed_type.id', 'left')
            ->join('bed_group', 'bed.bed_group_id = bed_group.id', 'left')
            ->join('floor', 'floor.id = bed_group.floor', 'left')
            ->where('r.is_active', '1')
            ->where('r.status', 'Boleh Pulang')
            ->get()
            ->result_array();
    }

    public function getForBilling()
    {
        return $this->db
            ->select('
                r.*, p.nama as nama_pasien, p.no_rm, p.alamat, u.nama as nama_dokter, jp.nama as nama_poli,
                bed.name as bed_name, bed_type.name as bed_type_name,floor.name as floor_name, bed_group.name as bedgroup
            ')
            ->from('rawat_inap r')
            ->join('pasien p', 'p.id = r.pasien_id and p.is_active = 1')
            ->join('user u', 'u.id = r.dokter_id')
            ->join('jenis_pendaftaran jp', 'jp.id = r.from_poli_id', 'left')
            ->join('transfer t', 't.rawat_inap_id = r.id AND t.current_place_type = \'bangsal\'')
            ->join('bed', 'bed.id = t.tujuan', 'left') // <------ asale r.tujuan
            ->join('bed_type', 'bed.bed_type_id = bed_type.id', 'left')
            ->join('bed_group', 'bed.bed_group_id = bed_group.id', 'left')
            ->join('floor', 'floor.id = bed_group.floor', 'left')
            ->where('r.is_active', '1')
            ->where("(r.status = 'Boleh Pulang'", null, false)
            ->or_where("r.status = 'Sudah Bayar')", null, false)
            ->get()
            ->result_array();
    }

    public function getSelesai($page = 1 , $size = 10, $search = null)
    {
        $this->db
            ->select('
                r.*, r.created_at as tgl, p.nama as nama_pasien, p.no_rm, p.alamat, u.nama as nama_dokter, jp.nama as nama_poli,
                bed.name as bed_name, bed_type.name as bed_type_name,floor.name as floor_name, bed_group.name as bedgroup, b.id as bayar_id
            ')
            ->from('rawat_inap r')
            ->join('ri_bayar b', 'r.id = b.rawat_inap_id and b.jenis_ruangan_id = 1')
            ->join('pasien p', 'p.id = r.pasien_id')
            ->join('user u', 'u.id = r.dokter_id')
            ->join('jenis_pendaftaran jp', 'jp.id = r.from_poli_id', 'left')
            ->join('bed', 'bed.id = r.tujuan')
            ->join('bed_type', 'bed.bed_type_id = bed_type.id')
            ->join('bed_group', 'bed.bed_group_id = bed_group.id')
            ->join('floor', 'floor.id = bed_group.floor')
            ->where('r.is_active', '1')
            ->where('r.status', 'Selesai')
            ->limit($size, ($page - 1) *  $size);

            if($search) {
                $this->db
                    ->group_start()
                    ->or_like('p.nama', $search)
                    ->or_like('p.no_rm', $search)
                    ->or_like('u.nama', $search)
                    ->group_end();
            }

            $this->db->order_by('r.id', 'asc');

        return $this->db->get()
            ->result_array();
    }

    public function getSelesaiDatatable($search = null)
    {
        $this->db
            ->select('
                r.*, r.created_at as tgl, p.nama as nama_pasien, p.no_rm, p.alamat, u.nama as nama_dokter, jp.nama as nama_poli,
                bed.name as bed_name, bed_type.name as bed_type_name,floor.name as floor_name, bed_group.name as bedgroup, b.id as bayar_id
            ')
            ->from('rawat_inap r')
            ->join('ri_bayar b', 'r.id = b.rawat_inap_id and b.jenis_ruangan_id = 1')
            ->join('pasien p', 'p.id = r.pasien_id')
            ->join('user u', 'u.id = r.dokter_id')
            ->join('jenis_pendaftaran jp', 'jp.id = r.from_poli_id', 'left')
            ->join('bed', 'bed.id = r.tujuan')
            ->join('bed_type', 'bed.bed_type_id = bed_type.id')
            ->join('bed_group', 'bed.bed_group_id = bed_group.id')
            ->join('floor', 'floor.id = bed_group.floor')
            ->where('r.is_active', '1')
            ->where('r.status', 'Selesai');

            if($search) {
                $this->db
                    ->group_start()
                    ->or_like('p.nama', $search)
                    ->or_like('p.no_rm', $search)
                    ->or_like('u.nama', $search)
                    ->group_end();
            }

            $this->db->order_by('r.id', 'asc');
            return $this->db;
    }

    public function getListPemeriksaanSelesai($periode = '') {
        $this->db->select('pem.*, p.nama nama_pasien, p.alamat, p.jk, p.usia, u.nama nama_dokter, jp.nama AS nama_jenis_pendaftaran, b.total AS total_bayar');
        $this->db->from('pemeriksaan pem');
        $this->db->join('pasien p', 'p.id = pem.pasien_id and p.is_active = 1');
        $this->db->join('user u', 'u.id = pem.dokter_id and u.is_active = 1');
        $this->db->join('pendaftaran_pasien ps', 'pem.pendaftaran_id = ps.id');
        $this->db->join('jenis_pendaftaran jp', 'jp.id = ps.jenis_pendaftaran_id');
        $this->db->join('bayar b', 'b.pemeriksaan_id = pem.id');
        $this->db->where('pem.is_active', '1');
        $this->db->where('pem.status', "'selesai'", FALSE);
        $this->db->order_by('pem.waktu_pemeriksaan', 'asc');

        if ($periode == 'harian') {
            $this->db->where('DATE(pem.created_at) = CURDATE()', null, FALSE);
        } else if ($periode == 'bulanan') {
            $this->db->where('MONTH(pem.created_at) = MONTH(CURDATE())', null, FALSE);
        } else {
            $this->db->where("DATE(pem.created_at) = '$periode'", null, FALSE);
        }

        return $this->db->get();
    }

    public function getSelesaiByPasienId($id)
    {
        return $this->db
            ->select('
                r.*, p.nama as nama_pasien, p.no_rm, p.alamat, u.nama as nama_dokter, jp.nama as nama_poli,
                bed.name as bed_name, bed_type.name as bed_type_name,floor.name as floor_name, bed_group.name as bedgroup
            ')
            ->from('rawat_inap r')
            ->join('pasien p', 'p.id = r.pasien_id')
            ->join('user u', 'u.id = r.dokter_id')
            ->join('jenis_pendaftaran jp', 'jp.id = r.from_poli_id', 'left')
            ->join('bed', 'bed.id = r.tujuan')
            ->join('bed_type', 'bed.bed_type_id = bed_type.id')
            ->join('bed_group', 'bed.bed_group_id = bed_group.id')
            ->join('floor', 'floor.id = bed_group.floor')
            ->where('r.is_active', '1')
            ->where('r.status', 'Selesai')
            ->where('r.pasien_id', $id)
            ->get()
            ->result_array();
    }

    public function getById($id)
    {
        return $this->db
            ->select('
                r.*, p.nama as nama_pasien, p.jk, p.no_rm, p.alamat, u.id as dokter_id, u.nama as nama_dokter, dpjp.id as dpjp_id, dpjp.nama as nama_dpjp, 
                jp.nama as nama_poli, bed.name as bed_name, bed_type.name as bed_type_name,floor.name as floor_name, bed_group.name as bedgroup, 
                bed_group.price as bedgroup_price, bed.id as bed_id, t.catatan
            ')
            ->from('rawat_inap r')
            ->join('transfer t', 't.id = r.transfer_id', 'left')
            ->join('pasien p', 'p.id = r.pasien_id', 'left')
            ->join('user u', 'u.id = r.dokter_id', 'left')
            ->join('user dpjp', 'dpjp.id = r.dpjp_id', 'left')
            ->join('jenis_pendaftaran jp', 'jp.id = r.from_poli_id', 'left')
            ->join('bed', 'bed.id = r.tujuan', 'left')
            ->join('bed_type', 'bed.bed_type_id = bed_type.id', 'left')
            ->join('bed_group', 'bed.bed_group_id = bed_group.id', 'left')
            ->join('floor', 'floor.id = bed_group.floor', 'left')
            ->where('r.is_active', '1')
            ->where('r.id', $id)
            ->get()
            ->row();
    }

    public function getByTransferId($id)
    {
        return $this->db
            ->select('
                r.*, p.nama as nama_pasien, p.no_rm, p.alamat, u.id as dokter_id, u.nama as nama_dokter, jp.nama as nama_poli,
                bed.name as bed_name, bed_type.name as bed_type_name,floor.name as floor_name, bed_group.name as bedgroup, 
                bed_group.price as bedgroup_price, bed.id as bed_id, t.catatan
            ')
            ->from('rawat_inap r')
            ->join('transfer t', 't.id = r.transfer_id')
            ->join('pasien p', 'p.id = r.pasien_id')
            ->join('user u', 'u.id = r.dokter_id')
            ->join('jenis_pendaftaran jp', 'jp.id = r.from_poli_id', 'left')
            ->join('bed', 'bed.id = r.tujuan')
            ->join('bed_type', 'bed.bed_type_id = bed_type.id')
            ->join('bed_group', 'bed.bed_group_id = bed_group.id')
            ->join('floor', 'floor.id = bed_group.floor')
            ->where('r.is_active', '1')
            ->where('r.transfer_id', $id)
            ->get()
            ->row();
    }

    public function getDataTransferMasuk()
    {
        return $this->db
            ->select('
                t.id, t.tanggal, t.pukul, p.nama as nama_pasien, p.no_rm, p.alamat, u.nama as nama_dokter, 
                jp.nama as nama_poli, t.tujuan, t.updated_at
            ')
            ->from('transfer t')
            ->join('pasien p', 'p.id = t.pasien_id')
            ->join('user u', 'u.id = t.dokter_id')
            ->join('jenis_pendaftaran jp', 'jp.id = t.dari', 'left')
            ->join('jenis_transfer jt', 'jt.id = t.jenis')
            ->where('jt.ke_jenis_ruangan_id', '1')
            ->order_by('t.id', 'desc')
            ->limit(300)
            ->get()
            ->result();
    }

    public function getDataTransferKeluar()
    {
        return $this->db
            ->select('
                t.id, t.tanggal, t.pukul, p.nama as nama_pasien, p.no_rm, p.alamat, u.nama as nama_dokter, 
                jp.nama as nama_poli, t.tujuan, t.updated_at
            ')
            ->from('transfer t')
            ->join('pasien p', 'p.id = t.pasien_id')
            ->join('user u', 'u.id = t.dokter_id')
            ->join('jenis_pendaftaran jp', 'jp.id = t.dari', 'left')
            ->join('jenis_transfer jt', 'jt.id = t.jenis')
            ->where('jt.dari_jenis_ruangan_id', '1')
            ->order_by('t.id', 'desc')
            ->limit(300)
            ->get()
            ->result();
    }

    public function getPasienById($id)
    {
        return $this->db->select('*')->from('pasien')->where('id', $id)->get()->row();
    }

    public function getPemeriksaanByRawatInapId($id)
    {
        return $this->db->select("p.*, coalesce(u.nama, '') as nama_dokter, d.diagnosis, d.id as ri_diagnosis_id, r.id as ri_resep_id, b.id as ri_biaya_id")
            ->from('ri_pemeriksaan p')
            ->join('user u', 'u.id = p.dokter_id', 'left')
            ->join('ri_diagnosis d', 'p.id = d.ri_pemeriksaan_id', 'left')
            ->join('ri_resep r', 'p.id = r.ri_pemeriksaan_id', 'left')
            ->join('ri_biaya b', 'p.id = b.ri_pemeriksaan_id', 'left')
            ->where('p.rawat_inap_id', $id)
            ->where('p.jenis_ruangan_id', 1)
            ->order_by('created_at', 'desc')
            ->get()
            ->result();
    }

    public function getPemeriksaanById($id)
    {
        return $this->db->select("p.*, coalesce(u.nama, '') as nama_dokter, d.diagnosis, d.id as ri_diagnosis_id, r.id as ri_resep_id, b.id as ri_biaya_id")
            ->from('ri_pemeriksaan p')
            ->join('user u', 'u.id = p.dokter_id', 'left')
            ->join('ri_diagnosis d', 'p.id = d.ri_pemeriksaan_id', 'left')
            ->join('ri_resep r', 'p.id = r.ri_pemeriksaan_id', 'left')
            ->join('ri_biaya b', 'p.id = b.ri_pemeriksaan_id', 'left')
            ->where('p.id', $id)
            ->where('p.jenis_ruangan_id', 1)
            ->order_by('created_at', 'desc')
            ->get()
            ->row();
    }

//    public function getPemeriksaanByTransferId($transfer_id, $jenis_ruangan_id)
//    {
//        return $this->db->select('p.*, u.nama as nama_dokter')
//            ->from('ri_pemeriksaan p')
//            ->join('user u', 'u.id = p.dokter_id')
//            ->where('p.transfer_id', $transfer_id)
//            ->where('p.jenis_ruangan_id', $jenis_ruangan_id)
//            ->get()
//            ->result();
//    }

    public function getDiagnosisByRawatInapId($id)
    {
        return $this->db
            ->where('r.rawat_inap_id', $id)
            ->where('r.jenis_ruangan_id', 1)
            ->get('ri_diagnosis r')
            ->result();
    }

//    public function getDiagnosisByTransferId($transfer_id, $jenis_ruangan_id)
//    {
//        return $this->db
//            ->where('r.transfer_id', $transfer_id)
//            ->where('r.jenis_ruangan_id', $jenis_ruangan_id)
//            ->get('ri_diagnosis r')
//            ->result();
//    }

    public function getDiagnosisByPemeriksaanId($id)
    {
        return $this->db
            ->where('dp.ri_diagnosis_id', $id)
            ->join('penyakit p', 'p.id = dp.penyakit_id')
            ->get('ri_diagnosis_penyakit dp')
            ->result();
    }

    public function getPenyakitByDiagnosisId($id)
    {
        return $this->db
            ->where('dp.ri_diagnosis_id', $id)
            ->join('penyakit p', 'p.id = dp.penyakit_id')
            ->get('ri_diagnosis_penyakit dp')
            ->result();
    }

    public function getResepByRawatInapId($id)
    {
        return $this->db
            ->where('rawat_inap_id', $id)
            ->where('jenis_ruangan_id', 1)
            ->get('ri_resep')
            ->result();
    }

//    public function getResepByTransferId($transfer_id, $jenis_ruangan_id)
//    {
//        return $this->db
//            ->where('transfer_id', $transfer_id)
//            ->where('jenis_ruangan_id', $jenis_ruangan_id)
//            ->get('ri_resep')
//            ->result();
//    }

    public function getBhpByRawatInapId($id)
    {
        return $this->db
            ->where('rawat_inap_id', $id)
            ->where('jenis_ruangan_id', 1)
            ->get('ri_bahan_habis_pakai')
            ->result();
    }

//    public function getBhpByTransferId($transfer_id, $jenis_ruangan_id)
//    {
//        return $this->db
//            ->where('transfer_id', $transfer_id)
//            ->where('jenis_ruangan_id', $jenis_ruangan_id)
//            ->get('ri_bahan_habis_pakai')
//            ->result();
//    }

    public function getBiayaByRawatInapId($id)
    {
        return $this->db
            ->where('rawat_inap_id', $id)
            ->where('jenis_ruangan_id', 1)
            ->get('ri_biaya')
            ->result();
    }

    public function getBiayaByPemeriksaanId($id)
    {
        return $this->db
            ->where('ri_pemeriksaan_id', $id)
            ->where('jenis_ruangan_id', 1)
            ->get('ri_biaya')
            ->result();
    }

//    public function getBiayaByTransferId($transfer_id, $jenis_ruangan_id)
//    {
//        return $this->db
//            ->where('transfer_id', $transfer_id)
//            ->where('jenis_ruangan_id', $jenis_ruangan_id)
//            ->get('ri_biaya')
//            ->result();
//    }

    public function getObatByResepId($id)
    {
        return $this->db
            ->select('ro.id, o.id as obat_id, o.nama, o.harga_jual, ro.jumlah, ro.signa')
            ->join('obat o', 'o.id = ro.obat_id')
            ->where('ro.ri_resep_id', $id)
            ->get('ri_resep_obat ro')
            ->result();
    }

    public function getBahanByBhpId($id)
    {
        return $this->db
            ->select('bhp.nama, bhp.id as bph_id, bhpd.jumlah, bhp.harga_jual')
            ->join('bahan_habis_pakai bhp', 'bhp.id = bhpd.bahan_id')
            ->where('bhpd.ri_bahan_habis_pakai_id', $id)
            ->get('ri_bahan_habis_pakai_detail bhpd')
            ->result();
    }

    public function getObatRacikByResepId($id)
    {
        return $this->db
            ->select('ror.id, ror.nama_racikan, ror.signa, ror.catatan')
            ->where('ror.ri_resep_id', $id)
            ->get('ri_resep_obat_racik ror')
            ->result();
    }

    public function getObatRacikDetailByResepObatRacikId($id)
    {
        return $this->db
            ->select('o.nama, o.harga_jual, rord.jumlah')
            ->join('obat o', 'o.id = rord.obat_id')
            ->where('rord.ri_resep_obat_racik_id', $id)
            ->get('ri_resep_obat_racik_detail rord')
            ->result();
    }

    public function getAllPenyakit()
    {
        return $this->db->where('is_active', '1')->get('penyakit')->result();
    }

    public function getAllTindakan()
    {
        return $this->db->where('is_active', '1')->get('tarif_tindakan')->result();
    }

    public function getAllTipeMakanan()
    {
        return $this->db->where('is_active', '1')->get('tipe_makanan')->result();
    }

    public function getMakananById($id)
    {
        return $this->db->where('is_active', '1')->where('id', $id)->get('makanan')->row();
    }

    public function getMakananByTipeId($id)
    {
        return $this->db->where('is_active', '1')
            ->where('tipe_makanan_id', $id)
            ->get('makanan')
            ->result();
    }

    public function getMakananCountByRawatInapId($id)
    {
        return $this->db
            ->where('b.jenis_biaya', 'Makanan')
            ->where('b.rawat_inap_id', $id)
            ->where('b.jenis_ruangan_id', 1)
            ->group_by('m.id')
            ->join('makanan m', 'm.id = b.makanan_id')
            ->select('m.nama, m.harga, count(m.id) as jumlah, (count(m.id) * m.harga) as total')
            ->get('ri_biaya b')
            ->result();
    }

//    public function getMakananCountByTransferId($transfer_id, $jenis_ruangan_id)
//    {
//        return $this->db
//            ->where('b.jenis_biaya', 'Makanan')
//            ->where('b.transfer_id', $transfer_id)
//            ->where('b.jenis_ruangan_id', $jenis_ruangan_id)
//            ->group_by('m.id')
//            ->join('makanan m', 'm.id = b.makanan_id')
//            ->select('m.nama, m.harga, count(m.id) as jumlah, (count(m.id) * m.harga) as total')
//            ->get('ri_biaya b')
//            ->result();
//    }

    public function getTindakanHargaByIds($id)
    {
        return $this->db->where('is_active', '1')
            ->where_in('id', $id)
            ->select('tarif_pasien')
            ->get('tarif_tindakan')
            ->result();
    }

    public function getTindakanByBiayaId($id, $tipeTindakan = null)
    {
       $this->db
            ->where('bt.ri_biaya_id', $id)
            ->select('tt.nama, tt.tarif_pasien, tt.id as tindakan_id, bt.dokter_anestesi')
            ->join('tarif_tindakan tt', 'tt.id = bt.tindakan_id');

        if(!is_null($tipeTindakan)) {
            $this->db
                ->where('bt.tipe_tindakan', $tipeTindakan);
        }
        
        return $this->db->get('ri_biaya_tindakan bt')
            ->result();
    }

    public function getBayarByRawatInapId($id)
    {
        return $this->db
            ->where('b.rawat_inap_id', $id)
            ->where('b.jenis_ruangan_id', 1)
            ->select('b.*')
            ->get('ri_bayar b')
            ->row();
    }

//    public function getBayarByTransferId($transfer_id, $jenis_ruangan_id)
//    {
//        return $this->db
//            ->where('b.transfer_id', $transfer_id)
//            ->where('b.jenis_ruangan_id', $jenis_ruangan_id)
//            ->select('b.*')
//            ->get('ri_bayar b')
//            ->row();
//    }

    public function getBayarById($id)
    {
        return $this->db
            ->where('b.id', $id)
            ->select('b.*')
            ->get('ri_bayar b')
            ->row();
    }

    public function getTransferById($id)
    {
        return $this->db
            ->where('id', $id)
            ->get('transfer')
            ->row();
    }

    public function getFirstBangsalTransferByRawatInapId($id)
    {
        return $this->db
            ->limit(1)
            ->order_by('t.id', 'asc')
            ->where('t.current_place_type', 'bangsal')
            ->where('ri.id', $id)
            ->join('transfer t', 't.id = ri.transfer_id')
            ->select('t.*')
            ->get('rawat_inap ri')
            ->row();
    }

    public function getLastTransferByRawatInapId($id)
    {
        return $this->db
            ->limit(1)
            ->order_by('t.id', 'desc')
            ->where('ri.id', $id)
            ->join('transfer t', 't.id = ri.transfer_id')
            ->select('t.*')
            ->get('rawat_inap ri')
            ->row();
    }

    public function getLastOperasiTransferByRawatInapId($id)
    {
        return $this->db
            ->limit(1)
            ->order_by('t.id', 'desc')
            ->where('ri.id', $id)
            ->where('t.current_place_type', 'ruang operasi')
            ->join('transfer t', 't.id = ri.transfer_id')
            ->select('t.*')
            ->get('rawat_inap ri')
            ->row();
    }

    public function getLastBersalinTransferByRawatInapId($id)
    {
        return $this->db
            ->limit(1)
            ->order_by('t.id', 'desc')
            ->where('ri.id', $id)
            ->where('t.current_place_type', 'ruang bersalin')
            ->join('transfer t', 't.id = ri.transfer_id')
            ->select('t.*')
            ->get('rawat_inap ri')
            ->row();
    }

    public function getAllBangsalTransfersByRawatInapId($id)
    {
        return $this->db
            ->order_by('t.id')
            ->where('t.rawat_inap_id', $id)
            ->where('t.current_place_type', 'bangsal')
            ->join('rawat_inap ri', 'ri.id = t.rawat_inap_id')
            ->select('t.*, ri.tgl_boleh_pulang')
            ->get('transfer t')
            ->result();
    }

    public function getBaruOfTransfer($current_place_type) // dipanggil di list input resep
    {
        return $this->db
            ->select('
                r.id, r.tipe_pasien, t.id as transfer_id, t.transfered_at, p.nama as nama_pasien, p.no_rm, p.alamat, 
                u.nama as nama_dokter, bed.name as bed_name, bed_type.name as bed_type_name,floor.name as floor_name, 
                bed_group.name as bedgroup, jp.nama as nama_poli, t.via_bangsal
            ')
            ->from('transfer t')
            ->join('rawat_inap r', 'r.id = t.rawat_inap_id')
            ->join('pasien p', 'p.id = r.pasien_id')
            ->join('user u', 'u.id = r.dokter_id')
            ->join('jenis_pendaftaran jp', 'jp.id = r.from_poli_id', 'left')
            ->join('bed', 'bed.id = t.dari', 'left')
            ->join('bed_type', 'bed.bed_type_id = bed_type.id', 'left')
            ->join('bed_group', 'bed.bed_group_id = bed_group.id', 'left')
            ->join('floor', 'floor.id = bed_group.floor', 'left')
            ->where('r.is_active', '1')
            ->where('t.is_active', '1')
            ->where('t.status', 'ditransfer')
            ->where('t.current_place_type', $current_place_type)
            ->order_by('t.id', 'desc')
            ->get()
            ->result_array();
    }

    public function getSelesaiOfTransfer($current_place_type) // dipanggil di rekap input resep
    {
        return $this->db
            ->select('
                r.id, r.tipe_pasien, t.id as transfer_id, t.transfered_at as tgl, p.nama as nama_pasien, p.no_rm, p.alamat, 
                u.nama as nama_dokter, bed.name as bed_name, bed_type.name as bed_type_name,floor.name as floor_name, 
                bed_group.name as bedgroup, jp.nama as nama_poli, t.via_bangsal
            ')
            ->from('transfer t')
            ->join('rawat_inap r', 'r.id = t.rawat_inap_id')
            ->join('pasien p', 'p.id = r.pasien_id')
            ->join('user u', 'u.id = r.dokter_id')
            ->join('jenis_pendaftaran jp', 'jp.id = r.from_poli_id', 'left')
            ->join('bed', 'bed.id = t.dari', 'left')
            ->join('bed_type', 'bed.bed_type_id = bed_type.id', 'left')
            ->join('bed_group', 'bed.bed_group_id = bed_group.id', 'left')
            ->join('floor', 'floor.id = bed_group.floor', 'left')
            ->where('r.is_active', '1')
            ->where('t.is_active', '1')
            ->where('t.status', 'selesai')
            ->where('t.current_place_type', $current_place_type)
            ->order_by('t.id', 'desc')
            ->get()
            ->result_array();
    }

    public function getSelesaiOfTransferByRawatInapId($rawatInapId) // dipanggil di rekap input resep
    {
        return $this->db
            ->select('
                r.id, r.tipe_pasien, t.id as transfer_id, t.transfered_at as tgl, p.nama as nama_pasien, p.no_rm, p.alamat, 
                u.nama as nama_dokter, bed.name as bed_name, bed_type.name as bed_type_name,floor.name as floor_name, 
                bed_group.name as bedgroup, jp.nama as nama_poli, t.via_bangsal, p.usia, p.telepon
            ')
            ->from('transfer t')
            ->join('rawat_inap r', 'r.id = t.rawat_inap_id')
            ->join('pasien p', 'p.id = r.pasien_id')
            ->join('user u', 'u.id = r.dokter_id')
            ->join('jenis_pendaftaran jp', 'jp.id = r.from_poli_id', 'left')
            ->join('bed', 'bed.id = t.dari', 'left')
            ->join('bed_type', 'bed.bed_type_id = bed_type.id', 'left')
            ->join('bed_group', 'bed.bed_group_id = bed_group.id', 'left')
            ->join('floor', 'floor.id = bed_group.floor', 'left')
            ->where('r.is_active', '1')
            ->where('t.is_active', '1')
            ->where('t.status', 'selesai')
            ->where('r.id', $rawatInapId)
            ->order_by('t.id', 'desc')
            ->get()
            ->row();
    }

    public function getObatRanap($rawatInapId, $transferId, $jenisRuanganId) {
        if($transferId) {
            return $this->db->query("
                    SELECT o.nama, SUM(rro.jumlah) as jumlah, o.harga_jual as harga FROM ri_resep_obat rro
                    JOIN obat o on o.id = rro.obat_id
                    JOIN ri_resep rr ON rr.id = rro.ri_resep_id
                    WHERE ri_resep_id IN (
                        SELECT id FROM ri_resep
                        WHERE rawat_inap_id = {$rawatInapId}
                        AND transfer_id = {$transferId}
                        AND jenis_ruangan_id = {$jenisRuanganId}
                    )
                    GROUP BY o.id
            ")->result();
        } else {
            return $this->db->query("
                SELECT o.nama, SUM(rro.jumlah) as jumlah, o.harga_jual as harga 
                FROM ri_resep_obat rro
                JOIN obat o on o.id = rro.obat_id
                JOIN ri_resep rr ON rr.id = rro.ri_resep_id
                WHERE ri_resep_id IN (
                    SELECT id FROM ri_resep
                    WHERE rawat_inap_id = {$rawatInapId}
                    AND jenis_ruangan_id = {$jenisRuanganId}
                )
                GROUP BY o.id
            ")->result();
        }
    }

    public function getObatRacikRanap($rawatInapId) {
        $racikans = $this->db->query("
            SELECT rror.id, rror.nama_racikan, rror.signa, rror.catatan FROM ri_resep_obat_racik rror
            JOIN ri_resep rr ON rr.id = rror.ri_resep_id
            WHERE ri_resep_id IN (
                SELECT id FROM ri_resep
                WHERE rawat_inap_id = {$rawatInapId}
            )
        ")->result();

        foreach ($racikans as &$or) {
            $or->obat = $this->db->query("
                SELECT o.nama, o.harga_jual as harga, rrord.jumlah FROM ri_resep_obat_racik_detail rrord
                JOIN obat o ON o.id = rrord.obat_id
                WHERE ri_resep_obat_racik_id = {$or->id} 
            ")->result();
        }

        return $racikans;
    }
    

    public function getSelesaiOfTransferDatatable($search = null)
    {
        $this->db
            ->select('
                r.id, r.tipe_pasien, t.id as transfer_id, t.transfered_at as tgl, p.nama as nama_pasien, p.no_rm, p.alamat, 
                u.nama as nama_dokter, bed.name as bed_name, bed_type.name as bed_type_name,floor.name as floor_name, 
                bed_group.name as bedgroup, jp.nama as nama_poli, t.via_bangsal
            ')
            ->from('transfer t')
            ->join('rawat_inap r', 'r.id = t.rawat_inap_id')
            ->join('pasien p', 'p.id = r.pasien_id')
            ->join('user u', 'u.id = r.dokter_id')
            ->join('jenis_pendaftaran jp', 'jp.id = r.from_poli_id', 'left')
            ->join('bed', 'bed.id = t.dari', 'left')
            ->join('bed_type', 'bed.bed_type_id = bed_type.id', 'left')
            ->join('bed_group', 'bed.bed_group_id = bed_group.id', 'left')
            ->join('floor', 'floor.id = bed_group.floor', 'left')
            ->where('r.is_active', '1')
            ->where('t.is_active', '1')
            ->where('t.status', 'selesai');
        if($search) {
            $this->db
            ->group_start()
                ->or_like('p.nama', $search)
                ->or_like('p.no_rm', $search)
                ->or_like('u.nama', $search)
            ->group_end();
        }
        
        $this->db->order_by('t.id', 'desc');

        return $this->db;
    }


    public function getTransaksiBulanIni() {
        $this->db->select('SUM(b.total) AS total_bayar');
        $this->db->from('transfer t');
        $this->db->join('pasien p ', 'p.id = t.pasien_id and p.is_active = 1');
        $this->db->join('ri_bayar b', 'b.transfer_id = t.id');
        $this->db->where('t.is_active', '1');
        $this->db->where('t.status', "'selesai'", FALSE);
        $this->db->where('MONTH(b.created_at) = MONTH(CURDATE()) AND YEAR(b.created_at) = YEAR(CURDATE())', null, false);
        return $this->db->get();
    }

    public function getJenisRuanganByNamaOrAlias($nama)
    {
        return $this->db
            ->where('nama', $nama)
            ->or_where('alias', $nama)
            ->get('jenis_ruangan')
            ->row();
    }

}
