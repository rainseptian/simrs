<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 6/25/19
 * Time: 9:40 AM
 */

class DetailObatPemeriksaanModel extends CI_Model {

    public function getDetailObatByPemeriksaanId($pemeriksaan_id) {
        return $this->db->query("
            SELECT dop.*, o.nama, o.harga_jual
            FROM detail_obat_pemeriksaan dop
            JOIN obat o ON o.id = dop.obat_id and o.is_active = 1
            JOIN pemeriksaan pem ON pem.id = dop.pemeriksaan_id and pem.is_active = 1
            WHERE pem.id = $pemeriksaan_id
            AND dop.is_active = 1
                UNION ALL
            SELECT dop.id, pol.pemeriksaan_id, dop.obat_id, dop.jumlah_satuan, dop.signa_obat, dop.is_active, dop.created_at, dop.updated_at, dop.creator, o.nama, o.harga_jual
            FROM detail_penjualan_obat_luar dop
            JOIN obat o ON o.id = dop.obat_id and o.is_active = 1
            JOIN penjualan_obat_luar pol on dop.penjualan_obat_luar_id = pol.id
            JOIN pemeriksaan p on pol.pemeriksaan_id = p.id
            WHERE p.id = $pemeriksaan_id
            AND dop.is_active = 1
        ");
    }

}