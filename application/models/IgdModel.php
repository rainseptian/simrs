<?php


class IgdModel extends CI_Model
{
    private $jenis_pendaftaran_id = 58; // igd

    public function getListPasienAntri()
    {
        return $this->db->select('pp.*, p.nama nama_pasien, p.alamat, p.jk, p.usia, u.nama nama_dokter, jp.nama jenis_pendaftaran')
            ->from('pendaftaran_pasien pp')
            ->join('pasien p', 'p.id = pp.pasien and p.is_active = 1')
            ->join('user u', 'u.id = pp.dokter and u.is_active = 1')
            ->join('jenis_pendaftaran jp', 'jp.id = pp.jenis_pendaftaran_id and jp.is_active = 1')
            ->where('pp.is_active', '1') 
            ->where('pp.status', 'antri')
            ->where('pp.jenis_pendaftaran_id', $this->jenis_pendaftaran_id)
            ->order_by('pp.waktu_pendaftaran', 'asc')
            ->get();
    }

    public function getJenisPendaftaranIgd()
    {
        return $this->db->where('is_active', '1')
            ->where('status', '1')
            ->where('id', $this->jenis_pendaftaran_id)
            ->get('jenis_pendaftaran')
            ->row();
    }

    public function getListPemeriksaanSudahPeriksaAwal()
    {
        return $this->db
            ->select('pem.*, p.nama nama_pasien, p.alamat, p.jk, p.usia, u.nama nama_dokter, jp.nama as jenis_pendaftaran, a.kode_antrian, a.is_mobile_jkn, a.is_check_in')
            ->from('pemeriksaan pem')
            ->join('pendaftaran_pasien pp', 'pp.id = pem.pendaftaran_id')
            ->join('jenis_pendaftaran jp', 'jp.id = pp.jenis_pendaftaran_id')
            ->join('pasien p', 'p.id = pem.pasien_id and p.is_active = 1')
            ->join('user u', 'u.id = pem.dokter_id and u.is_active = 1')
            ->join('antrian a', 'a.pendaftaran_id = pp.id')
            ->where('pem.is_active', '1')
//            ->where('pem.status', 'sudah_periksa_awal')
            ->where("(pem.status = 'sudah_periksa_awal'", null, false)
            ->or_where("pem.status = 'belum')", null, false)
            ->where('pp.jenis_pendaftaran_id', $this->jenis_pendaftaran_id)
            ->order_by('pem.waktu_pemeriksaan', 'asc')
            ->get();
    }

    public function getListPemeriksaanSudahPeriksa()
    {
        return $this->db->select('pem.*, p.nama nama_pasien, p.alamat, p.jk, p.usia, u.nama nama_dokter')
            ->from('pemeriksaan pem')
            ->join('pendaftaran_pasien pp', 'pp.id = pem.pendaftaran_id')
            ->join('pasien p', 'p.id = pem.pasien_id and p.is_active = 1')
            ->join('user u', 'u.id = pem.dokter_id and u.is_active = 1')
            ->where('pem.is_active', '1')
            ->where('pem.status', 'sudah_periksa')
            ->where('pp.jenis_pendaftaran_id', $this->jenis_pendaftaran_id)
            ->order_by('pem.waktu_pemeriksaan', 'desc')
            ->get();
    }

}
