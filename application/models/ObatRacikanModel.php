<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ObatRacikanModel extends CI_Model {

    public function getRacikanByIdPemeriksaan($id) {
        return $this->db
            ->where('is_active', 1)
            ->where('pemeriksaan_id', $id)
            ->get('detail_obat_racikan_pemeriksaan');
    }

    public function getObatRacikanByIdDetailObatRacikan($id) {
        return $this->db
            ->where('obr.is_active', 1)
            ->where('detail_obat_racikan_pemeriksaan_id', $id)
            ->join('obat o', 'obr.obat_id = o.id')
            ->get('obat_racikan obr');
    }

    public function getRacikanLuarByIdPemeriksaan($id) {
        return $this->db
            ->where('is_active', 1)
            ->where('pemeriksaan_id', $id)
            ->select('d.*, p.pemeriksaan_id')
            ->join('penjualan_obat_luar p', 'p.id = d.penjualan_obat_luar_id')
            ->get('detail_penjualan_obat_racikan_luar d');
    }

    public function getObatRacikanLuarByIdDetailObatRacikan($id) {
        return $this->db
            ->where('obr.is_active', 1)
            ->where('detail_penjualan_obat_racikan_luar_id', $id)
            ->join('obat o', 'obr.obat_id = o.id')
            ->get('obat_racikan_luar obr');
    }
}