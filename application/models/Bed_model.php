<?php

Class Bed_model extends CI_Model {

    public function bedcategorie($table_name) {
        $this->db->select('id, name');
        $this->db->from($table_name);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function valid_bed($str) {
        $name = $this->input->post('name');
        if ($this->check_floor_exists($name)) {
            $this->form_validation->set_message('check_exists', 'Bed already exists');
            return FALSE;
        } else {
            return TRUE;
        }
    }

    public function check_floor_exists($name) {
        $bedid = $this->input->post("bedid");
        if ($bedid != 0) {
            $data = array('name' => $name, 'id !=' => $bedid);
            $query = $this->db->where($data)->get('bed');

            if ($query->num_rows() > 0) {
                return TRUE;
            } else {
                return FALSE;
            }
        } else {
            $this->db->where('name', $name);
            $query = $this->db->get('bed');
            if ($query->num_rows() > 0) {
                return TRUE;
            } else {
                return FALSE;
            }
        }
    }

    public function bedNoType() {
        $query = $this->db->select('bed.*,bed_type.id as btid,bed_type.name as bed_type')
                ->join('bed_type', 'bed.bed_type_id = bed_type.id')
                ->where('bed.is_active', 'yes')
                ->get('bed');
        return $query->result_array();
    }

    public function bed_list($id = null) {
        $data = array();
        $this->db->select('
            bed.*, bed_type.name as bed_type_name,floor.name as floor_name, 
            bed_group.name as bedgroup,bed_group.id as bedgroupid,pasien.id as pid,
            pasien.is_active as patient_status, pasien.no_rm as patient_unique_id,
            pasien.nama as patient_name,pasien.jk as gender, pasien.alamat as guardian_name,
            pasien.telepon as mobileno, rawat_inap.created_at as tgl_masuk, rawat_inap.tujuan as bed, 
            rawat_inap.status as ipd_discharged, rawat_inap.id as ri_id
        ')->from('bed');
        $this->db->join('bed_type', 'bed.bed_type_id = bed_type.id','left');
        $this->db->join('bed_group', 'bed.bed_group_id = bed_group.id','left');
        $this->db->join('floor', 'floor.id = bed_group.floor','left');
        $this->db->join('rawat_inap', "bed.id = rawat_inap.tujuan and rawat_inap.status != 'Selesai'", 'left');
//        $this->db->join('rawat_inap', "bed.id = rawat_inap.tujuan and rawat_inap.status != 'Selesai'", 'left');
        $this->db->join('pasien', 'pasien.id = rawat_inap.pasien_id', 'left');
        $this->db->order_by('bed.id', 'asc');

        if ($id) {
            $this->db->where('bed.id', $id);
        } else {
            $this->db->order_by('bed.id', 'desc');
        }
        $query = $this->db->get();
        if ($id != null) {
            $result = $query->row_array();
        } else {
            $result = $query->result_array();
        }

        if (!empty($result)) {
            foreach ($result as $key => $value) {
//                if ($value["pid"]) {
//                    if (($value['patient_status'] == '1') && ($value['ipd_discharged'] == 'Masuk' || $value['ipd_discharged'] == 'Boleh Pulang')) {
//                        $data[] = $value;
//                    } elseif (($value['is_active'] == 'yes')) {
//                        $val =  $value['bed'];
//                        $data[$val] = $value;
//                    }
//                } else {
                    $data[] = $value ;
//                }
            }
        }
        return $data;
    }

   public function bed_active() {
//        $query = $this->db->query("
//            SELECT bed.id, bed.name, bed.is_active,patients.id as pid,patients.discharged,
//            patients.is_active as patient_status,patients.patient_unique_id,
//            patients.patient_name,patients.gender,patients.guardian_name,
//            patients.mobileno,ipd_details.bed as bid,ipd_details.discharged as ipd_discharged
//            FROM bed
//            RIGHT JOIN ipd_details ON ipd_details.bed = bed.id
//            RIGHT JOIN patients ON patients.id = ipd_details.patient_id
//            where bed.is_active = 'yes' group by bed.id
//        ");
        $query = $this->db->query("
            SELECT b.id, b.name, b.is_active, p.id as pid, 
            p.is_active as patient_status, 
            p.nama as patient_name,p.jk as gender, 
            p.telepon as mobileno, ri.tujuan as bid, ri.status as ipd_discharged 
            FROM bed b
            RIGHT JOIN rawat_inap ri ON ri.tujuan = b.id 
            RIGHT JOIN pasien p ON p.id = ri.pasien_id 
            where b.is_active = 'yes' group by b.id
        ");
        $result = $query->result_array();
        return $result;
    }

    public function bed_listsearch($id = null) {
        $data = array();
        $this->db->select('bed.*, bed_type.name as bed_type_name,floor.name as floor_name, bed_group.name as bedgroup,
        bed_group.id as bedgroupid, bed_group.price as group_price')->from('bed');
        $this->db->join('bed_type', 'bed.bed_type_id = bed_type.id');
        $this->db->join('bed_group', 'bed.bed_group_id = bed_group.id');
        $this->db->join('floor', 'floor.id = bed_group.floor');
        $this->db->order_by('bed.id', 'asc');
        if ($id != null) {
            $this->db->where('bed.id', $id);
        } else {
            $this->db->order_by('bed.id', 'desc');
        }
        $query = $this->db->get();
        if ($id != null) {
            $result = $query->row_array();
        } else {
            $result = $query->result_array();
        }
        return $result;
    }

    public function hcu_bed_listsearch($id = null) {
        $data = array();
        $this->db->select('bed.*, bed_type.name as bed_type_name,floor.name as floor_name, bed_group.name as bedgroup,
        bed_group.id as bedgroupid, bed_group.price as group_price')->from('bed');
        $this->db->join('bed_type', 'bed.bed_type_id = bed_type.id');
        $this->db->join('bed_group', 'bed.bed_group_id = bed_group.id');
        $this->db->join('floor', 'floor.id = bed_group.floor');
        $this->db->order_by('bed.id', 'asc');
        $this->db->where('bed_group.id', '4');
        if ($id != null) {
            $this->db->where('bed.id', $id);
        } else {
            $this->db->order_by('bed.id', 'desc');
        }
        $query = $this->db->get();
        if ($id != null) {
            $result = $query->row_array();
        } else {
            $result = $query->result_array();
        }
        return $result;
    }

    public function perinatologi_bed_listsearch($id = null) {
        $data = array();
        $this->db->select('bed.*, bed_type.name as bed_type_name,floor.name as floor_name, bed_group.name as bedgroup,
        bed_group.id as bedgroupid, bed_group.price as group_price')->from('bed');
        $this->db->join('bed_type', 'bed.bed_type_id = bed_type.id');
        $this->db->join('bed_group', 'bed.bed_group_id = bed_group.id');
        $this->db->join('floor', 'floor.id = bed_group.floor');
        $this->db->order_by('bed.id', 'asc');
        $this->db->where('bed_group.id', '9');
        if ($id != null) {
            $this->db->where('bed.id', $id);
        } else {
            $this->db->order_by('bed.id', 'desc');
        }
        $query = $this->db->get();
        if ($id != null) {
            $result = $query->row_array();
        } else {
            $result = $query->result_array();
        }
        return $result;
    }

    public function bed_active_list_search() {
        $this->db->select('bed.*, bed_type.name as bed_type_name,floor.name as floor_name, bed_group.name as bedgroup,
        bed_group.id as bedgroupid, bed_group.price as group_price')->from('bed');
        $this->db->join('bed_type', 'bed.bed_type_id = bed_type.id');
        $this->db->join('bed_group', 'bed.bed_group_id = bed_group.id');
        $this->db->join('floor', 'floor.id = bed_group.floor');
        $this->db->order_by('bed.id', 'asc');
        $this->db->where('bed.is_active', 'yes');
        return $this->db->order_by('bed.id', 'desc')->get()->result_array();
    }

    public function getBedDetails($id) {
        $data = array();
        $this->db->select('bed.*, bed_type.name as bed_type_name,floor.name as floor_name, bed_group.name as bedgroup,bed_group.id as bedgroupid')->from('bed');
        $this->db->join('bed_type', 'bed.bed_type_id = bed_type.id');
        $this->db->join('bed_group', 'bed.bed_group_id = bed_group.id');
        $this->db->join('floor', 'floor.id = bed_group.floor');
        $this->db->where("bed.id", $id);
        $query = $this->db->get();
        return $query->row_array();
    }

    public function savebed($data) {
        if (isset($data["id"])) {
            $this->db->where("id", $data["id"])->update("bed", $data);
        } else {
            $this->db->insert("bed", $data);
        }
    }

    public function getbedbybedgroup($bed_group, $active = '', $bed_id = 0) {
        $this->db->select('bed.*, bed_type.name as bed_type_name,bed_group.name as bedgroup ')->from('bed');
        $this->db->join('bed_type', 'bed.bed_type_id = bed_type.id');
        $this->db->join('bed_group', 'bed.bed_group_id = bed_group.id');
        if (!empty($active)) {
            $this->db->where('bed.is_active', $active);
        }
        $this->db->where('bed.bed_group_id', $bed_group);
        if (!empty($bed_id)) {
            $this->db->or_where('bed.id', $bed_id);
        }
        $query = $this->db->get();
        return $query->result_array();
    }

    public function delete($id) {
        $this->db->where("id", $id)->delete("bed");
    }

    public function getBedStatus() {
        $this->db->select('bed.*, bed_type.name as bed_type_name,bed_group.name as bedgroup,floor.name as floor_name')->from('bed');
        $this->db->join('bed_type', 'bed.bed_type_id = bed_type.id');
        $this->db->join('bed_group', 'bed.bed_group_id = bed_group.id');
        $this->db->join('floor', 'floor.id = bed_group.floor');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function checkbed($bedid) {
        $query = $this->db->select('bed.is_active')->where("id", $bedid)->get("bed");
        $result = $query->row_array();
        if ($result['is_active'] == 'yes') {
            return true;
        } else {
            return false;
        }
    }

}
