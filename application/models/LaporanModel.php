<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class LaporanModel extends CI_Model {


    public function getListPendaftaran_antri()
    {
        $this->db->select('pp.*, p.nama nama_pasien, p.jk, p.usia, u.nama nama_dokter, jp.nama jenis_pendaftaran' );
        $this->db->from('pendaftaran_pasien pp');
        $this->db->join('pasien p', 'p.id = pp.pasien and p.is_active = 1');
        $this->db->join('user u', 'u.id = pp.dokter and u.is_active = 1');
        $this->db->join('jenis_pendaftaran jp', 'jp.id = pp.jenis_pendaftaran_id and jp.is_active = 1');
        $this->db->where('pp.is_active', '1' );
        $this->db->where('pp.status', 'antri' );
        return  $this->db->get();
    }

    public function getPendaftaranById($id)
    {
        $this->db->select('pp.*, p.nama nama_pasien, p.jk, p.usia, u.nama nama_dokter, jp.nama jenis_pendaftaran' );
        $this->db->from('pendaftaran_pasien pp');
        $this->db->join('pasien p', 'p.id = pp.pasien and p.is_active = 1');
        $this->db->join('user u', 'u.id = pp.dokter and u.is_active = 1');
        $this->db->join('jenis_pendaftaran jp', 'jp.id = pp.jenis_pendaftaran_id and jp.is_active = 1');
        $this->db->where('pp.is_active', '1' );
        $this->db->where('pp.id', $id );

        return $this->db->get();
    }

    public function getJenisPendaftaran()
    {
        $this->db->where('is_active', '1' );
        $this->db->where('status', '1' );
        return  $this->db->get('jenis_pendaftaran');
    }

    public function getJenisPendaftaranAll()
    {
        return $this->db->query(" 
            SELECT id, nama FROM jenis_pendaftaran WHERE is_active = 1 AND status = 1
            UNION ALL
            SELECT id, nama FROM jenis_ruangan WHERE id != 6
        ");
    }

    public function getListPendaftaran($start_date,$end_date,$jenis_pendaftaran)
    {
        $this->db->select('pp.*, pem.id as pemeriksaan_id, p.id pasien_id, p.nama nama_pasien, p.no_rm, p.jk, p.usia, p.alamat, u.nama nama_dokter, jp.id as jenis_pendaftaran_id, jp.nama jenis_pendaftaran, jp.nama as poli' )
            ->from('pendaftaran_pasien pp')
            ->join('pasien p', 'p.id = pp.pasien and p.is_active = 1')
            ->join('user u', 'u.id = pp.dokter and u.is_active = 1')
            ->join('jenis_pendaftaran jp', 'jp.id = pp.jenis_pendaftaran_id and jp.is_active = 1')
            ->join('pemeriksaan pem', 'pp.id = pem.pendaftaran_id')
            ->where('pp.is_active', '1' )
            ->where('pp.waktu_pendaftaran >=', $start_date . ' 00:00:00')
            ->where('pp.waktu_pendaftaran <=', $end_date . ' 23:59:59');

        if ($jenis_pendaftaran) $this->db->where('pp.jenis_pendaftaran_id', $jenis_pendaftaran );
        return $this->db->get();
    }

    private function filter_jenis_pendaftaran($jenis_pendaftaran)
    {
        $jp_poli = $jp_ranap = $jp_trf = '';

        if ($jenis_pendaftaran) {

            $jp_poli = $jp_ranap = $jp_trf = 'AND 0';

            if ($jenis_pendaftaran > 10) {
                $jp_poli = "AND pp.jenis_pendaftaran_id = $jenis_pendaftaran";
            }
            else if ($jenis_pendaftaran == 1) {
                $jp_ranap = "AND 1";
            }
            else {
                if ($jenis_pendaftaran == 2) {
                    $jp_trf = "AND t.current_place_type = 'ruang bersalin'";
                }
                else if ($jenis_pendaftaran == 3) {
                    $jp_trf = "AND t.current_place_type = 'ruang operasi'";
                }
                else if ($jenis_pendaftaran == 4) {
                    $jp_trf = "AND t.current_place_type = 'perinatologi'";
                }
                else if ($jenis_pendaftaran == 5) {
                    $jp_trf = "AND t.current_place_type = 'hcu'";
                }
            }
        }

        return compact('jp_poli', 'jp_ranap', 'jp_trf');
    }

    public function getSimpleListPendaftaran($start_date, $end_date, $jenis_pendaftaran = null)
    {
        $filter = $this->filter_jenis_pendaftaran($jenis_pendaftaran);
        $jp_poli = $filter['jp_poli'];
        $jp_ranap = $filter['jp_ranap'];
        $jp_trf = $filter['jp_trf'];

        return $this->db->query("
            SELECT pp.waktu_pendaftaran, pem.id as pemeriksaan_id, p.nama as nama_pasien, p.jk, p.no_rm, p.usia, p.alamat, u.nama nama_dokter, jp.nama as poli, pp.jaminan, pem.meta, pem.diagnosis, 'Rajal' as type
            FROM pendaftaran_pasien pp
            JOIN pasien p ON p.id = pp.pasien and p.is_active = 1
            JOIN user u ON u.id = pp.dokter and u.is_active = 1
            JOIN jenis_pendaftaran jp ON jp.id = pp.jenis_pendaftaran_id and jp.is_active = 1
            JOIN pemeriksaan pem ON pp.id = pem.pendaftaran_id
            WHERE pp.is_active = 1
            AND pp.waktu_pendaftaran >= '$start_date 00:00:00'
            AND pp.waktu_pendaftaran <= '$end_date 23:59:59'
            $jp_poli

            UNION ALL

            SELECT r.created_at as waktu_pendaftaran, r.id as pemeriksaan_id, p.nama as nama_pasien, p.jk, p.no_rm, p.usia, p.alamat, u.nama as nama_dokter, 'Ranap' as poli, r.tipe_pasien as jaminan, '' as meta, '' as diagnosis, 'Ranap' as type
            FROM rawat_inap r
            JOIN pasien p ON p.id = r.pasien_id and p.is_active = 1
            JOIN user u ON u.id = r.dokter_id and u.is_active = 1
            WHERE tujuan REGEXP '^[0-9]+$'
            AND via_bangsal = 1 AND r.is_active = 1
            AND r.created_at >= '$start_date 00:00:00'
            AND r.created_at <= '$end_date 23:59:59'
            $jp_ranap

            UNION ALL

            SELECT t.transfered_at as waktu_pendaftaran, t.id as pemeriksaan_id, p.nama as nama_pasien, p.jk, p.no_rm, p.usia, p.alamat, u.nama as nama_dokter,
                   (CASE 
                        WHEN t.current_place_type = 'ruang bersalin' THEN 'Ruang Bersalin'
                        WHEN t.current_place_type = 'ruang operasi' THEN 'Ruang Operasi'
                        WHEN t.current_place_type = 'perinatologi' THEN 'Perinatologi'
                        WHEN t.current_place_type = 'hcu' THEN 'HCU'
                    END) as poli, r.tipe_pasien as jaminan, '' as meta, '' as diagnosis, 'Ranap-Like' as type
            FROM transfer t
            JOIN rawat_inap r ON r.id = t.rawat_inap_id AND r.is_active = 1
            JOIN pasien p ON p.id = r.pasien_id and p.is_active = 1
            JOIN user u ON u.id = r.dokter_id and u.is_active = 1
            WHERE (t.current_place_type = 'ruang bersalin' OR t.current_place_type = 'ruang operasi' OR t.current_place_type = 'perinatologi' OR t.current_place_type = 'hcu')
            AND t.is_active = 1
            AND t.transfered_at >= '$start_date 00:00:00'
            AND t.transfered_at <= '$end_date 23:59:59'
            $jp_trf

            ORDER BY waktu_pendaftaran
        ");
    }

    public function getKunjunganPasien($start_date, $end_date, $jenis_pendaftaran = null)
    {
        $filter = $this->filter_jenis_pendaftaran($jenis_pendaftaran);
        $jp_poli = $filter['jp_poli'];
        $jp_ranap = $filter['jp_ranap'];
        $jp_trf = $filter['jp_trf'];

        return $this->db->query("
            SELECT jp.id, jp.nama, count(pp.jenis_pendaftaran_id) as jumlah
            FROM pendaftaran_pasien pp
            JOIN pasien p ON p.id = pp.pasien and p.is_active = 1
            JOIN user u ON u.id = pp.dokter and u.is_active = 1
            JOIN jenis_pendaftaran jp ON jp.id = pp.jenis_pendaftaran_id and jp.is_active = 1
            WHERE pp.is_active = 1 
            AND pp.waktu_pendaftaran >= '$start_date 00:00:00'
            AND pp.waktu_pendaftaran <= '$end_date 23:59:59'
            $jp_poli
            GROUP BY pp.jenis_pendaftaran_id
            
            UNION ALL

            SELECT 1 as id, 'Ranap' as nama, count(r.id) as jumlah
            FROM rawat_inap r
            JOIN pasien p ON p.id = r.pasien_id and p.is_active = 1
            JOIN user u ON u.id = r.dokter_id and u.is_active = 1
            WHERE tujuan REGEXP '^[0-9]+$'
            AND via_bangsal = 1 AND r.is_active = 1
            AND r.created_at >= '$start_date 00:00:00'
            AND r.created_at <= '$end_date 23:59:59'
            $jp_ranap

            UNION ALL

            SELECT (CASE 
                        WHEN t.current_place_type = 'ruang bersalin' THEN 2 
                        WHEN t.current_place_type = 'ruang operasi' THEN 3 
                        WHEN t.current_place_type = 'perinatologi' THEN 4 
                        WHEN t.current_place_type = 'hcu' THEN 5
                    END) as id, 
                   (CASE 
                        WHEN t.current_place_type = 'ruang bersalin' THEN 'Ruang Bersalin'
                        WHEN t.current_place_type = 'ruang operasi' THEN 'Ruang Operasi'
                        WHEN t.current_place_type = 'perinatologi' THEN 'Perinatologi'
                        WHEN t.current_place_type = 'hcu' THEN 'HCU'
                    END) as nama, 
                   count(t.id) as jumlah
            FROM transfer t
            JOIN rawat_inap r ON r.id = t.rawat_inap_id AND r.is_active = 1
            JOIN pasien p ON p.id = r.pasien_id and p.is_active = 1
            JOIN user u ON u.id = r.dokter_id and u.is_active = 1
            WHERE (t.current_place_type = 'ruang bersalin' OR t.current_place_type = 'ruang operasi' OR t.current_place_type = 'perinatologi' OR t.current_place_type = 'hcu')
            AND t.is_active = 1
            AND t.transfered_at >= '$start_date 00:00:00'
            AND t.transfered_at <= '$end_date 23:59:59'
            $jp_trf
            GROUP BY t.current_place_type

            ORDER BY jumlah DESC 
        ");
    }

    public function getJumlahPasien($start_date, $end_date, $jenis_pendaftaran)
    {
        $filter = $this->filter_jenis_pendaftaran($jenis_pendaftaran);
        $jp_poli = $filter['jp_poli'];
        $jp_ranap = $filter['jp_ranap'];
        $jp_trf = $filter['jp_trf'];

        return $this->db->query("
            SELECT count(distinct pp.pasien) as jumlah, jp.nama, jp.id as id_jp 
            from jenis_pendaftaran jp 
            join pendaftaran_pasien pp ON jp.id = pp.jenis_pendaftaran_id AND pp.is_active = 1
            join pasien p on pp.pasien = p.id AND p.is_active = 1
            join user u on u.id = pp.dokter and u.is_active = 1
            WHERE jp.is_active = 1 AND jp.status = 1 
            AND pp.waktu_pendaftaran >= '$start_date 00:00:00' AND pp.waktu_pendaftaran <= '$end_date 23:59:59' AND pp.is_active = 1
            $jp_poli
            group by jp.nama

            UNION ALL

            SELECT count(distinct r.pasien_id) as jumlah, 'Ranap' as nama, 1 as id_jp
            FROM rawat_inap r
            JOIN pasien p ON p.id = r.pasien_id and p.is_active = 1
            JOIN user u ON u.id = r.dokter_id and u.is_active = 1
            WHERE tujuan REGEXP '^[0-9]+$'
            AND via_bangsal = 1 AND r.is_active = 1
            AND r.created_at >= '$start_date 00:00:00'
            AND r.created_at <= '$end_date 23:59:59'
            $jp_ranap

            UNION ALL

            SELECT count(distinct t.pasien_id) as jumlah, 
                   (CASE 
                        WHEN t.current_place_type = 'ruang bersalin' THEN 'Ruang Bersalin'
                        WHEN t.current_place_type = 'ruang operasi' THEN 'Ruang Operasi'
                        WHEN t.current_place_type = 'perinatologi' THEN 'Perinatologi'
                        WHEN t.current_place_type = 'hcu' THEN 'HCU'
                    END) as nama , 
                   (CASE 
                        WHEN t.current_place_type = 'ruang bersalin' THEN 2 
                        WHEN t.current_place_type = 'ruang operasi' THEN 3 
                        WHEN t.current_place_type = 'perinatologi' THEN 4 
                        WHEN t.current_place_type = 'hcu' THEN 5
                    END) as id_jp
            FROM transfer t
            JOIN rawat_inap r ON r.id = t.rawat_inap_id AND r.is_active = 1
            JOIN pasien p ON p.id = r.pasien_id and p.is_active = 1
            JOIN user u ON u.id = r.dokter_id and u.is_active = 1
            WHERE (t.current_place_type = 'ruang bersalin' OR t.current_place_type = 'ruang operasi' OR t.current_place_type = 'perinatologi' OR t.current_place_type = 'hcu')
            AND t.is_active = 1
            AND t.transfered_at >= '$start_date 00:00:00'
            AND t.transfered_at <= '$end_date 23:59:59'
            $jp_trf
            GROUP BY t.current_place_type

            ORDER BY jumlah DESC 
        ");
    }

    public function listJumlahPasien($start_date, $end_date, $jenis_pendaftaran)
    {
        $filter = $this->filter_jenis_pendaftaran($jenis_pendaftaran);
        $jp_poli = $filter['jp_poli'];
        $jp_ranap = $filter['jp_ranap'];
        $jp_trf = $filter['jp_trf'];

        return $this->db->query("
            SELECT pp.waktu_pendaftaran, pem.id as pemeriksaan_id, p.nama, p.jk, p.no_rm, p.usia, p.alamat, u.nama as nama_dokter, jp.nama as poli, pp.jaminan, pem.meta, pem.diagnosis, 'Rajal' as type
            from pasien p 
            join pendaftaran_pasien pp ON pp.pasien = p.id  AND pp.is_active = '1'
            join jenis_pendaftaran jp ON jp.id = pp.jenis_pendaftaran_id AND jp.is_active = '1' AND jp.status = 1
            join pemeriksaan pem ON pp.id = pem.pendaftaran_id
            join user u ON u.id = pp.dokter AND u.is_active = '1'
            WHERE p.is_active = 1 
            AND pp.waktu_pendaftaran >= '$start_date 00:00:00' 
            AND pp.waktu_pendaftaran <= '$end_date 23:59:59' 
            $jp_poli

            UNION ALL

            SELECT r.created_at as waktu_pendaftaran, r.id as pemeriksaan_id, p.nama, p.jk, p.no_rm, p.usia, p.alamat, u.nama as nama_dokter, 'Ranap' as poli, r.tipe_pasien as jaminan, '' as meta, '' as diagnosis, 'Ranap' as type
            FROM pasien p
            JOIN rawat_inap r ON p.id = r.pasien_id and r.is_active = 1
            JOIN user u ON u.id = r.dokter_id and u.is_active = 1
            WHERE tujuan REGEXP '^[0-9]+$'
            AND via_bangsal = 1 AND p.is_active = 1
            AND r.created_at >= '$start_date 00:00:00'
            AND r.created_at <= '$end_date 23:59:59'
            $jp_ranap

            UNION ALL

            SELECT t.transfered_at as waktu_pendaftaran, t.id as pemeriksaan_id, p.nama, p.jk, p.no_rm, p.usia, p.alamat, u.nama as nama_dokter,
                   (CASE 
                        WHEN t.current_place_type = 'ruang bersalin' THEN 'Ruang Bersalin'
                        WHEN t.current_place_type = 'ruang operasi' THEN 'Ruang Operasi'
                        WHEN t.current_place_type = 'perinatologi' THEN 'Perinatologi'
                        WHEN t.current_place_type = 'hcu' THEN 'HCU'
                    END) as poli, r.tipe_pasien as jaminan, '' as meta, '' as diagnosis, 'Ranap-Like' as type
            FROM pasien p
            JOIN transfer t ON p.id = t.pasien_id and t.is_active = 1
            JOIN rawat_inap r ON r.id = t.rawat_inap_id AND r.is_active = 1
            JOIN user u ON u.id = r.dokter_id and u.is_active = 1
            WHERE (t.current_place_type = 'ruang bersalin' OR t.current_place_type = 'ruang operasi' OR t.current_place_type = 'perinatologi' OR t.current_place_type = 'hcu')
            AND p.is_active = 1
            AND t.transfered_at >= '$start_date 00:00:00'
            AND t.transfered_at <= '$end_date 23:59:59'
            $jp_trf

            ORDER BY waktu_pendaftaran
        ");
    }

    public function jumlahPasienBaru($start_date, $end_date, $jenis_pendaftaran)
    {
        $filter = $this->filter_jenis_pendaftaran($jenis_pendaftaran);
        $jp_poli = $filter['jp_poli'];
        $jp_ranap = $filter['jp_ranap'];
        $jp_trf = $filter['jp_trf'];

        return $this->db->query("
            SELECT count(distinct pp.pasien) as jumlah, jp.nama, jp.id as id_jp 
            from pendaftaran_pasien pp 
            join jenis_pendaftaran jp ON jp.id = pp.jenis_pendaftaran_id 
            join pasien p on pp.pasien = p.id 
            WHERE jp.is_active = 1 AND jp.status = 1 AND pp.is_active = 1 
            AND pp.waktu_pendaftaran >= '$start_date 00:00:00' 
            AND pp.waktu_pendaftaran <= '$end_date 23:59:59'
            AND date(p.created_at) = date(pp.waktu_pendaftaran)
            $jp_poli
            group by jp.nama

            UNION ALL

            SELECT count(distinct r.pasien_id) as jumlah, 'Ranap' as nama, 1 as id_jp
            FROM rawat_inap r
            JOIN pasien p ON p.id = r.pasien_id and p.is_active = 1
            JOIN user u ON u.id = r.dokter_id and u.is_active = 1
            WHERE tujuan REGEXP '^[0-9]+$'
            AND via_bangsal = 1 AND r.is_active = 1
            AND r.created_at >= '$start_date 00:00:00'
            AND r.created_at <= '$end_date 23:59:59'
            AND date(p.created_at) = date(r.created_at)
            $jp_ranap

            UNION ALL

            SELECT count(distinct t.pasien_id) as jumlah, 
                   (CASE 
                        WHEN t.current_place_type = 'ruang bersalin' THEN 'Ruang Bersalin'
                        WHEN t.current_place_type = 'ruang operasi' THEN 'Ruang Operasi'
                        WHEN t.current_place_type = 'perinatologi' THEN 'Perinatologi'
                        WHEN t.current_place_type = 'hcu' THEN 'HCU'
                    END) as nama , 
                   (CASE 
                        WHEN t.current_place_type = 'ruang bersalin' THEN 2 
                        WHEN t.current_place_type = 'ruang operasi' THEN 3 
                        WHEN t.current_place_type = 'perinatologi' THEN 4 
                        WHEN t.current_place_type = 'hcu' THEN 5
                    END) as id_jp
            FROM transfer t
            JOIN rawat_inap r ON r.id = t.rawat_inap_id AND r.is_active = 1
            JOIN pasien p ON p.id = r.pasien_id and p.is_active = 1
            JOIN user u ON u.id = r.dokter_id and u.is_active = 1
            WHERE (t.current_place_type = 'ruang bersalin' OR t.current_place_type = 'ruang operasi' OR t.current_place_type = 'perinatologi' OR t.current_place_type = 'hcu')
            AND t.is_active = 1
            AND t.transfered_at >= '$start_date 00:00:00'
            AND t.transfered_at <= '$end_date 23:59:59'
            AND date(p.created_at) = date(t.created_at)
            $jp_trf
            GROUP BY t.current_place_type

            ORDER BY jumlah DESC 
        ");
    }

    public function listJumlahPasienBaru($start_date,$end_date,$jenis_pendaftaran)
    {
        $filter = $this->filter_jenis_pendaftaran($jenis_pendaftaran);
        $jp_poli = $filter['jp_poli'];
        $jp_ranap = $filter['jp_ranap'];
        $jp_trf = $filter['jp_trf'];

        return  $this->db->query("
            SELECT pp.waktu_pendaftaran, pem.id as pemeriksaan_id, p.nama, p.alamat, p.usia, p.jk, p.no_rm, u.nama as nama_dokter, jp.nama as poli, pp.jaminan, 'Rajal' as type
            from pasien p 
            join pendaftaran_pasien pp ON pp.pasien = p.id  AND pp.is_active = '1'
            join jenis_pendaftaran jp ON jp.id = pp.jenis_pendaftaran_id AND jp.is_active = '1' AND jp.status = 1
            join pemeriksaan pem ON pp.id = pem.pendaftaran_id
            join user u ON u.id = pp.dokter AND u.is_active = '1'
            WHERE pp.is_active = 1 
            AND pp.waktu_pendaftaran >= '$start_date 00:00:00' 
            AND pp.waktu_pendaftaran <= '$end_date 23:59:59'
            AND date(p.created_at) = date(pp.waktu_pendaftaran)
            $jp_poli
            
            UNION ALL

            SELECT r.created_at as waktu_pendaftaran, r.id as pemeriksaan_id, p.nama, p.alamat, p.usia, p.jk, p.no_rm, u.nama as nama_dokter, 'Ranap' as poli, r.tipe_pasien as jaminan, 'Ranap' as type
            FROM pasien p
            JOIN rawat_inap r ON p.id = r.pasien_id and r.is_active = 1
            JOIN user u ON u.id = r.dokter_id and u.is_active = 1
            WHERE tujuan REGEXP '^[0-9]+$'
            AND via_bangsal = 1 AND p.is_active = 1
            AND r.created_at >= '$start_date 00:00:00'
            AND r.created_at <= '$end_date 23:59:59'
            AND date(p.created_at) = date(r.created_at)
            $jp_ranap

            UNION ALL

            SELECT t.transfered_at as waktu_pendaftaran, t.id as pemeriksaan_id, p.nama, p.alamat, p.usia, p.jk, p.no_rm, u.nama as nama_dokter,
                   (CASE 
                        WHEN t.current_place_type = 'ruang bersalin' THEN 'Ruang Bersalin'
                        WHEN t.current_place_type = 'ruang operasi' THEN 'Ruang Operasi'
                        WHEN t.current_place_type = 'perinatologi' THEN 'Perinatologi'
                        WHEN t.current_place_type = 'hcu' THEN 'HCU'
                    END) as poli, r.tipe_pasien as jaminan, 'Ranap-Like' as type
            FROM pasien p
            JOIN transfer t ON p.id = t.pasien_id and t.is_active = 1
            JOIN rawat_inap r ON r.id = t.rawat_inap_id AND r.is_active = 1
            JOIN user u ON u.id = r.dokter_id and u.is_active = 1
            WHERE (t.current_place_type = 'ruang bersalin' OR t.current_place_type = 'ruang operasi' OR t.current_place_type = 'perinatologi' OR t.current_place_type = 'hcu')
            AND p.is_active = 1
            AND t.transfered_at >= '$start_date 00:00:00'
            AND t.transfered_at <= '$end_date 23:59:59'
            AND date(p.created_at) = date(t.created_at)
            $jp_trf

            ORDER BY waktu_pendaftaran
        ");
    }

    public function jumlahPasien20($start_date,$end_date,$jenis_pendaftaran)
    {
        $filter = $this->filter_jenis_pendaftaran($jenis_pendaftaran);
        $jp_poli = $filter['jp_poli'];
        $jp_ranap = $filter['jp_ranap'];
        $jp_trf = $filter['jp_trf'];

        return $this->db->query("
            SELECT p.*, count(pp.pasien) as jumlah, jp.nama as nama_jenis_pendaftaran 
            from pendaftaran_pasien pp 
            join pasien p ON pp.pasien = p.id  AND p.is_active = '1'
            join jenis_pendaftaran jp ON jp.id = pp.jenis_pendaftaran_id AND jp.is_active = '1' AND jp.status = 1
            WHERE pp.waktu_pendaftaran >='".$start_date." 00:00:00' AND pp.waktu_pendaftaran <='".$end_date." 23:59:59'
            AND pp.is_active = 1 
            group by pp.pasien order by count(pp.pasien) DESC
        ");
    }

    public function listJumlahPasien20($start_date,$end_date,$jenis_pendaftaran)
    {
        $filter = $this->filter_jenis_pendaftaran($jenis_pendaftaran);
        $jp_poli = $filter['jp_poli'];
        $jp_ranap = $filter['jp_ranap'];
        $jp_trf = $filter['jp_trf'];

        return $this->db->query("
            SELECT COUNT(pasien) as total, nama, no_rm, usia, alamat, jk, jaminan, poli
            FROM (
                SELECT pp.pasien, p.nama, p.no_rm, p.usia, p.alamat, p.jk, pp.jaminan, jp.nama as poli FROM pendaftaran_pasien pp 
                JOIN pasien p ON p.id = pp.pasien AND p.is_active = '1'
                join jenis_pendaftaran jp ON jp.id = pp.jenis_pendaftaran_id AND jp.is_active = '1' AND jp.status = 1
                WHERE pp.waktu_pendaftaran >='".$start_date." 00:00:00' AND pp.waktu_pendaftaran <='".$end_date." 23:59:59'
                $jp_poli
                AND pp.is_active = 1
                
                UNION ALL
    
                SELECT r.pasien_id as pasien, p.nama, p.no_rm, p.usia, p.alamat, p.jk, r.tipe_pasien as jaminan, 'Ranap' as poli
                FROM rawat_inap r
                JOIN pasien p ON p.id = r.pasien_id and p.is_active = 1
                JOIN user u ON u.id = r.dokter_id and u.is_active = 1
                WHERE tujuan REGEXP '^[0-9]+$'
                AND via_bangsal = 1 AND r.is_active = 1
                AND r.created_at >= '$start_date 00:00:00'
                AND r.created_at <= '$end_date 23:59:59'
                $jp_ranap
    
                UNION ALL
    
                SELECT t.pasien_id as pasien, p.nama, p.no_rm, p.usia, p.alamat, p.jk, r.tipe_pasien as jaminan,
                       (CASE 
                            WHEN t.current_place_type = 'ruang bersalin' THEN 'Ruang Bersalin'
                            WHEN t.current_place_type = 'ruang operasi' THEN 'Ruang Operasi'
                            WHEN t.current_place_type = 'perinatologi' THEN 'Perinatologi'
                            WHEN t.current_place_type = 'hcu' THEN 'HCU'
                        END) as poli
                FROM transfer t
                JOIN rawat_inap r ON r.id = t.rawat_inap_id AND r.is_active = 1
                JOIN pasien p ON p.id = r.pasien_id and p.is_active = 1
                JOIN user u ON u.id = r.dokter_id and u.is_active = 1
                WHERE (t.current_place_type = 'ruang bersalin' OR t.current_place_type = 'ruang operasi' OR t.current_place_type = 'perinatologi' OR t.current_place_type = 'hcu')
                AND t.is_active = 1
                AND t.transfered_at >= '$start_date 00:00:00'
                AND t.transfered_at <= '$end_date 23:59:59'
                $jp_trf

            ) as t
            GROUP BY pasien
            ORDER BY total DESC
            LIMIT 20
        ");
    }

    public function listRataKunjungan($start_date,$end_date)
    {
        return $this->db->query("
            SELECT year(pp.waktu_pendaftaran) tahun ,month(pp.waktu_pendaftaran) bulan ,day(pp.waktu_pendaftaran) tanggal, p.*,pp.waktu_pendaftaran
            from pasien p 
            join pendaftaran_pasien pp ON pp.pasien = p.id  AND pp.is_active = 1
            join jenis_pendaftaran jp ON jp.id = pp.jenis_pendaftaran_id AND jp.is_active = 1 AND jp.status = 1
            WHERE pp.waktu_pendaftaran >='".$start_date." 00:00:00' AND pp.waktu_pendaftaran <='".$end_date." 23:59:59' AND pp.is_active = 1
       ");
    }

    public function getRataKunjungan($start_date,$end_date,$jenis_pendaftaran)
    {
        $filter = $this->filter_jenis_pendaftaran($jenis_pendaftaran);
        $jp_poli = $filter['jp_poli'];
        $jp_ranap = $filter['jp_ranap'];
        $jp_trf = $filter['jp_trf'];

        return  $this->db->query("
            SELECT year(pp.waktu_pendaftaran) tahun ,month(pp.waktu_pendaftaran) bulan ,day(pp.waktu_pendaftaran) tanggal, count(jp.nama) as jumlah, jp.nama, jp.id
            from pendaftaran_pasien pp
            join pasien p ON pp.pasien = p.id  AND pp.is_active = 1
            join jenis_pendaftaran jp ON jp.id = pp.jenis_pendaftaran_id AND jp.is_active = 1 AND jp.status = 1
            WHERE pp.waktu_pendaftaran >='".$start_date." 00:00:00' 
            AND pp.waktu_pendaftaran <='".$end_date." 23:59:59' 
            AND pp.is_active = 1
            $jp_poli
            group by year(pp.waktu_pendaftaran),month(pp.waktu_pendaftaran),day(pp.waktu_pendaftaran), jp.id
            
            UNION ALL

            SELECT year(r.created_at) tahun ,month(r.created_at) bulan ,day(r.created_at) tanggal, count(r.id) as jumlah, 'Ranap' as nama, 1 as id
            FROM rawat_inap r
            JOIN pasien p ON p.id = r.pasien_id and p.is_active = 1
            JOIN user u ON u.id = r.dokter_id and u.is_active = 1
            WHERE tujuan REGEXP '^[0-9]+$'
            AND via_bangsal = 1 AND r.is_active = 1
            AND r.created_at >= '$start_date 00:00:00'
            AND r.created_at <= '$end_date 23:59:59'
            $jp_ranap
            group by year(r.created_at),month(r.created_at),day(r.created_at)

            UNION ALL

            SELECT year(t.created_at) tahun ,month(t.created_at) bulan ,day(t.created_at) as tanggal, count(t.id) as jumlah, 
                   (CASE 
                        WHEN t.current_place_type = 'ruang bersalin' THEN 'Ruang Bersalin'
                        WHEN t.current_place_type = 'ruang operasi' THEN 'Ruang Operasi'
                        WHEN t.current_place_type = 'perinatologi' THEN 'Perinatologi'
                        WHEN t.current_place_type = 'hcu' THEN 'HCU'
                    END) as nama,
                   (CASE 
                        WHEN t.current_place_type = 'ruang bersalin' THEN 2
                        WHEN t.current_place_type = 'ruang operasi' THEN 3
                        WHEN t.current_place_type = 'perinatologi' THEN 4
                        WHEN t.current_place_type = 'hcu' THEN 5
                    END) as id
            FROM transfer t
            JOIN rawat_inap r ON r.id = t.rawat_inap_id AND r.is_active = 1
            JOIN pasien p ON p.id = r.pasien_id and p.is_active = 1
            JOIN user u ON u.id = r.dokter_id and u.is_active = 1
            WHERE (t.current_place_type = 'ruang bersalin' OR t.current_place_type = 'ruang operasi' OR t.current_place_type = 'perinatologi' OR t.current_place_type = 'hcu')
            AND t.is_active = 1
            AND t.transfered_at >= '$start_date 00:00:00'
            AND t.transfered_at <= '$end_date 23:59:59'
            $jp_trf
            group by year(t.created_at),month(t.created_at),day(t.created_at)
        ");
    }

    public function getRataPasien($start_date,$end_date,$jenis_pendaftaran)
    {
        return  $this->db->query("
            SELECT year(pp.waktu_pendaftaran) tahun ,month(pp.waktu_pendaftaran) bulan ,day(pp.waktu_pendaftaran) tanggal, count(jp.nama) as jumlah, jp.nama, jp.id
            from pendaftaran_pasien pp
            join pasien p ON pp.pasien = p.id  AND pp.is_active = 1
            join jenis_pendaftaran jp ON jp.id = pp.jenis_pendaftaran_id AND jp.is_active = 1 AND jp.status = 1
            WHERE pp.waktu_pendaftaran >='".$start_date." 00:00:00' AND pp.waktu_pendaftaran <='".$end_date." 23:59:59' AND pp.is_active = 1
            group by pp.pasien, year(pp.waktu_pendaftaran), month(pp.waktu_pendaftaran), day(pp.waktu_pendaftaran)
        ");
    }

    public function getPerformaDokter($start_date, $end_date, $jenis_pendaftaran)
    {
        $w = $jenis_pendaftaran ? " AND pp.jenis_pendaftaran_id = '$jenis_pendaftaran' " : '';
        return  $this->db->query("
            select u.nama nama_dokter, jp.nama, count(*) jumlah 
            FROM pemeriksaan p 
            join user u ON u.id = p.dokter_id  AND u.is_active = 1
            join pendaftaran_pasien pp ON p.pendaftaran_id = pp.id  AND pp.is_active = 1
            join jenis_pendaftaran jp ON jp.id = pp.jenis_pendaftaran_id AND jp.is_active = 1 AND jp.status = 1
            WHERE p.waktu_pemeriksaan >='$start_date 00:00:00' AND p.waktu_pemeriksaan <='$end_date 23:59:59' AND pp.is_active = 1 $w
            GROUP BY u.id
       ");
    }

    public function getPerformaDokter2($jenis_pendaftaran)
    {
        $w = $jenis_pendaftaran ? " WHERE pp.jenis_pendaftaran_id = '$jenis_pendaftaran' " : '';
        return  $this->db->query("
            select u.nama nama_dokter, jp.nama, count(*) jumlah 
            FROM pemeriksaan p 
            join user u ON u.id = p.dokter_id  AND u.is_active = 1
            join pendaftaran_pasien pp ON p.pendaftaran_id = pp.id  AND pp.is_active = 1
            join jenis_pendaftaran jp ON jp.id = pp.jenis_pendaftaran_id AND jp.is_active = 1 AND jp.status = 1 AND pp.is_active = 1
            $w
            GROUP BY u.id
       ");
    }

    public function getPerformaPerawat($start_date, $end_date, $jenis_pendaftaran)
    {
        $w = $jenis_pendaftaran ? " AND pp.jenis_pendaftaran_id = '$jenis_pendaftaran' " : '';
        return  $this->db->query("
            select u.nama nama_perawat, jp.nama, count(*) jumlah 
            FROM pemeriksaan p 
            join user u ON u.id = p.perawat_id  AND u.is_active = 1
            join pendaftaran_pasien pp ON p.pendaftaran_id = pp.id  AND pp.is_active = 1
            join jenis_pendaftaran jp ON jp.id = pp.jenis_pendaftaran_id AND jp.is_active = 1 AND jp.status = 1
            WHERE p.waktu_pemeriksaan >='$start_date 00:00:00' AND p.waktu_pemeriksaan <='$end_date 23:59:59' AND pp.is_active = 1 $w
            GROUP BY u.id
       ");
    }

    public function getPerformaPerawat2($jenis_pendaftaran)
    {
        $w = $jenis_pendaftaran ? " WHERE pp.jenis_pendaftaran_id = '$jenis_pendaftaran' " : '';
        return  $this->db->query("
            select u.nama nama_perawat, jp.nama, count(*) jumlah 
            FROM pemeriksaan p 
            join user u ON u.id = p.perawat_id  AND u.is_active = 1
            join pendaftaran_pasien pp ON p.pendaftaran_id = pp.id  AND pp.is_active = 1
            join jenis_pendaftaran jp ON jp.id = pp.jenis_pendaftaran_id AND jp.is_active = 1 AND jp.status = 1 AND pp.is_active = 1
            $w
            GROUP BY u.id
       ");
    }

    public function listDokter()
    {

        $this->db->select('u.*,ug.id user_grup_id, g.nama_grup');
        $this->db->from('user u');
        $this->db->join('user_grup ug', 'ug.user_id = u.id');
        $this->db->join('grup g', 'ug.grup_id = g.id');
        $this->db->where('u.is_active', 1);
        $this->db->where('g.nama_grup', 'dokter');

        return $this->db->get();
    }

    public function getObatPemeriksaan($start_date, $end_date, $jenis_pendaftaran)
    {
        $jp = $jenis_pendaftaran ? " AND pp.jenis_pendaftaran_id = '$jenis_pendaftaran'" : "";
        return $this->db->query("
            select obat_id, nama, sum(jumlah_satuan) jumlah from (
                select dop.*, o.nama, o.harga_jual from detail_obat_pemeriksaan dop 
                join obat o ON o.id = dop.obat_id and o.is_active = 1 
                join pemeriksaan p ON p.id = dop.pemeriksaan_id and p.is_active = 1
                join pendaftaran_pasien pp ON pp.id = p.pendaftaran_id and p.is_active = 1
                WHERE p.waktu_pemeriksaan >='".$start_date." 00:00:00' AND p.waktu_pemeriksaan <='".$end_date." 23:59:59' $jp
                and dop.is_active = 1
            ) as A group by obat_id
        ");
    }

    public function getObatRacikanPemeriksaan($start_date, $end_date, $jenis_pendaftaran)
    {
        $jp = $jenis_pendaftaran ? " AND pp.jenis_pendaftaran_id = '$jenis_pendaftaran'" : "";
        return $this->db->query("
            select obat_id, nama, sum(jumlah_satuan) jumlah from (
                select dorp.*, o.id obat_id, o.nama, o.harga_jual, ora.jumlah_satuan from detail_obat_racikan_pemeriksaan dorp 
                join obat_racikan ora ON ora.detail_obat_racikan_pemeriksaan_id = dorp.id and ora.is_active = 1
                join obat o ON o.id = ora.obat_id
                join pemeriksaan p ON p.id = dorp.pemeriksaan_id and p.is_active = 1
                join pendaftaran_pasien pp ON pp.id = p.pendaftaran_id and p.is_active = 1
                WHERE p.waktu_pemeriksaan >='".$start_date." 00:00:00' AND p.waktu_pemeriksaan <='".$end_date." 23:59:59' $jp
                and dorp.is_active = 1
            ) as A group by obat_id
        ");
    }

    public function getObatResepLuar($start_date, $end_date)
    {
        return $this->db->query("
            select obat_id, nama, sum(jumlah_satuan) jumlah from (
                select dop.*, o.nama, o.harga_jual from detail_penjualan_obat_luar dop 
                join obat o ON o.id = dop.obat_id and o.is_active = 1 
                join penjualan_obat_luar pol ON pol.id = dop.penjualan_obat_luar_id
                WHERE pol.created_at >='".$start_date." 00:00:00' AND pol.created_at <='".$end_date." 23:59:59' 
                and dop.is_active = 1 
                and pol.tipe = 'resep_luar'
            ) as A group by obat_id
        ");
    }

    public function getObatRacikResepLuar($start_date, $end_date)
    {
        return $this->db->query("
            select obat_id, nama, sum(jumlah_satuan) jumlah from (
                select dorp.*, ora.obat_id, ora.jumlah_satuan, o.nama, o.harga_jual from detail_penjualan_obat_racikan_luar dorp 
                join obat_racikan_luar ora ON ora.detail_penjualan_obat_racikan_luar_id = dorp.id and ora.is_active = 1
                join obat o ON o.id = ora.obat_id
                join penjualan_obat_luar pol ON pol.id = dorp.penjualan_obat_luar_id
                WHERE pol.created_at >='".$start_date." 00:00:00' AND pol.created_at <='".$end_date." 23:59:59' 
                and dorp.is_active = 1
                and pol.tipe = 'resep_luar'
            ) as A group by obat_id
        ");
    }

    public function getObatObatBebas($start_date, $end_date)
    {
        return $this->db->query("
            select obat_id, nama, sum(jumlah_satuan) jumlah from (
                select dop.*, o.nama, o.harga_jual from detail_penjualan_obat_luar dop 
                join obat o ON o.id = dop.obat_id and o.is_active = 1 
                join penjualan_obat_luar pol ON pol.id = dop.penjualan_obat_luar_id
                WHERE pol.created_at >='".$start_date." 00:00:00' AND pol.created_at <='".$end_date." 23:59:59' 
                and dop.is_active = 1 
                and pol.tipe = 'obat_bebas'
            ) as A group by obat_id
        ");
    }

    public function getObatRacikObatBebas($start_date, $end_date)
    {
        return $this->db->query("
            select obat_id, nama, sum(jumlah_satuan) jumlah from (
                select dorp.*, ora.obat_id, ora.jumlah_satuan, o.nama, o.harga_jual from detail_penjualan_obat_racikan_luar dorp 
                join obat_racikan_luar ora ON ora.detail_penjualan_obat_racikan_luar_id = dorp.id and ora.is_active = 1
                join obat o ON o.id = ora.obat_id
                join penjualan_obat_luar pol ON pol.id = dorp.penjualan_obat_luar_id
                WHERE pol.created_at >='".$start_date." 00:00:00' AND pol.created_at <='".$end_date." 23:59:59' 
                and dorp.is_active = 1
                and pol.tipe = 'obat_bebas'
            ) as A group by obat_id
        ");
    }

    public function getObatObatInternal($start_date, $end_date)
    {
        return $this->db->query("
            select obat_id, nama, sum(jumlah_satuan) jumlah from (
                select dop.*, o.nama, o.harga_jual from detail_penjualan_obat_luar dop 
                join obat o ON o.id = dop.obat_id and o.is_active = 1 
                join penjualan_obat_luar pol ON pol.id = dop.penjualan_obat_luar_id
                WHERE pol.created_at >='".$start_date." 00:00:00' AND pol.created_at <='".$end_date." 23:59:59' 
                and dop.is_active = 1 
                and pol.tipe = 'obat_internal'
            ) as A group by obat_id
        ");
    }

    public function getObatRacikObatInternal($start_date, $end_date)
    {
        return $this->db->query("
            select obat_id, nama, sum(jumlah_satuan) jumlah from (
                select dorp.*, ora.obat_id, ora.jumlah_satuan, o.nama, o.harga_jual from detail_penjualan_obat_racikan_luar dorp 
                join obat_racikan_luar ora ON ora.detail_penjualan_obat_racikan_luar_id = dorp.id and ora.is_active = 1
                join obat o ON o.id = ora.obat_id
                join penjualan_obat_luar pol ON pol.id = dorp.penjualan_obat_luar_id
                WHERE pol.created_at >='".$start_date." 00:00:00' AND pol.created_at <='".$end_date." 23:59:59' 
                and dorp.is_active = 1
                and pol.tipe = 'obat_internal'
            ) as A group by obat_id
        ");
    }

    public function getObatPemeriksaan2()
    {
        /*    $this->db->select('dop.*, o.nama , o.harga_jual' );
            $this->db->join('obat o', 'o.id = dop.obat_id and o.is_active = 1');
            $this->db->where('dop.is_active', '1' );
    return  $this->db->get('detail_obat_pemeriksaan dop');*/
        return $this->db->query("select nama, sum(jumlah_satuan) jumlah from (select dop.*, o.nama, o.harga_jual from detail_obat_pemeriksaan dop 
                                    join obat o ON o.id = dop.obat_id and o.is_active = 1 
                                    and dop.is_active =1) as A group by obat_id");
    }

    public function getPenyakitPemeriksaan($start_date, $end_date, $jenis_pendaftaran)
    {
        $filter = $this->filter_jenis_pendaftaran($jenis_pendaftaran);
        $jp_poli = $filter['jp_poli'];
        $jp_ranap = $filter['jp_ranap'];
        $jp_trf = $filter['jp_trf'];

        return $this->db->query("
            select nama, count(penyakit_id) jumlah from (
                select dpp.penyakit_id, pen.nama from detail_penyakit_pemeriksaan dpp 
                join penyakit pen ON pen.id = dpp.penyakit_id and pen.is_active = 1 
                join pemeriksaan p ON p.id = dpp.pemeriksaan_id and p.is_active = 1
                join pendaftaran_pasien pp ON pp.id = p.pendaftaran_id and p.is_active = 1
                WHERE p.waktu_pemeriksaan >='".$start_date." 00:00:00' AND p.waktu_pemeriksaan <='".$end_date." 23:59:59' 
                $jp_poli
                and dpp.is_active = 1
                
                UNION ALL
    
                SELECT dp.penyakit_id, pen.nama
                FROM rawat_inap r
                JOIN pasien p ON p.id = r.pasien_id and p.is_active = 1
                JOIN user u ON u.id = r.dokter_id and u.is_active = 1
                JOIN ri_diagnosis d ON d.rawat_inap_id = r.id
                JOIN ri_diagnosis_penyakit dp ON dp.ri_diagnosis_id = d.id  
                JOIN penyakit pen ON pen.id = dp.penyakit_id and pen.is_active = 1 
                WHERE tujuan REGEXP '^[0-9]+$'
                AND via_bangsal = 1 AND r.is_active = 1
                AND r.created_at >= '$start_date 00:00:00'
                AND r.created_at <= '$end_date 23:59:59'
                $jp_ranap
    
                UNION ALL
    
                SELECT dp.penyakit_id, pen.nama
                FROM transfer t
                JOIN rawat_inap r ON r.id = t.rawat_inap_id AND r.is_active = 1
                JOIN pasien p ON p.id = r.pasien_id and p.is_active = 1
                JOIN user u ON u.id = r.dokter_id and u.is_active = 1
                JOIN ri_diagnosis d ON d.transfer_id = t.id
                JOIN ri_diagnosis_penyakit dp ON dp.ri_diagnosis_id = d.id  
                JOIN penyakit pen ON pen.id = dp.penyakit_id and pen.is_active = 1 
                WHERE (t.current_place_type = 'ruang bersalin' OR t.current_place_type = 'ruang operasi' OR t.current_place_type = 'perinatologi' OR t.current_place_type = 'hcu')
                AND t.is_active = 1
                AND t.transfered_at >= '$start_date 00:00:00'
                AND t.transfered_at <= '$end_date 23:59:59'
                $jp_trf

            ) as A 
            group by penyakit_id
            ORDER BY jumlah DESC
        ");
    }

    public function getPenyakitPemeriksaan2()
    {
        /*    $this->db->select('dpp.*, pen.nama ' );
            $this->db->join('penyakit o', 'pen.id = dpp.penyakit_id and pen.is_active = 1');
            $this->db->where('dpp.is_active', '1' );
    return  $this->db->get('detail_penyakit_pemeriksaan dpp');*/
        return $this->db->query("select nama, count(penyakit_id) jumlah from (select dpp.*, pen.nama from detail_penyakit_pemeriksaan dpp 
                                    join penyakit pen ON pen.id = dpp.penyakit_id and pen.is_active = 1 
                                    and dpp.is_active =1) as A group by penyakit_id");
    }

    public function getPasienPemeriksaan($start_date,$end_date){
        /*return  $this->db->query("SELECT * from pasien p
                                   join pemeriksaan pem ON p.id = pem.pasien_id and pem.is_active = 1
                                    WHERE pem.waktu_pemeriksaan >='".$start_date."' AND pem.waktu_pemeriksaan <='".$end_date."'
                              ");*/
    }

    public function getPasienPemeriksaan2(){
        return  $this->db->query("SELECT * from pasien where is_active = 1 ORDER BY id DESC LIMIT 300");

    }

    public function getPemeriksaanBayarById($id)
    {
        return $this->db
            ->select('pem.*, p.nama as nama_pasien, p.jk, p.usia,p.alamat, p.telepon, p.pekerjaan, u.nama as nama_dokter, jp.id as jenis_pendaftaran_id, jp.nama as nama_jenis_pendaftaran, jp.kode as kode_pendaftaran' )
            ->from('pemeriksaan pem')
            ->join('pasien p', 'p.id = pem.pasien_id and p.is_active = 1')
            ->join('user u', 'u.id = pem.dokter_id and u.is_active = 1')
            ->join('pendaftaran_pasien pp', 'pp.id = pem.pendaftaran_id')
            ->join('jenis_pendaftaran jp', 'jp.id = pp.jenis_pendaftaran_id')
            ->where('pem.is_active', '1' )
            ->where('pem.pasien_id', $id )
            ->where("(pem.status = 'bayar' OR pem.status = 'selesai' OR pem.status = 'sudah_periksa')")
            ->limit(500)
            ->get();
    }

    public function getPemeriksaanBayarByIdPemeriksaan($id)
    {
        return $this->db
            ->select('pem.*, p.nama as nama_pasien, p.jk, p.usia,p.alamat, p.telepon, p.pekerjaan, u.nama nama_dokter' )
            ->from('pemeriksaan pem')
            ->join('pasien p', 'p.id = pem.pasien_id and p.is_active = 1')
            ->join('user u', 'u.id = pem.dokter_id and u.is_active = 1')
            ->join('pendaftaran_pasien pp', 'pp.id = pem.pendaftaran_id')
            ->where('pem.is_active', '1' )
            ->where('pem.id', $id )
            ->where("(pem.status = 'bayar' OR pem.status = 'selesai')")
            ->limit(500)
            ->get();
    }

    public function getPemeriksaanSudahPeriksaByIdPemeriksaan($id)
    {
        return $this->db
            ->select('pem.*, p.nama as nama_pasien, p.jk, p.usia, p.alamat, p.telepon, p.pekerjaan, u.nama nama_dokter' )
            ->from('pemeriksaan pem')
            ->join('pasien p', 'p.id = pem.pasien_id and p.is_active = 1')
            ->join('user u', 'u.id = pem.dokter_id and u.is_active = 1')
            ->join('pendaftaran_pasien pp', 'pp.id = pem.pendaftaran_id')
            ->where('pem.is_active', '1' )
            ->where('pem.id', $id )
            ->where("(pem.status = 'bayar' OR pem.status = 'selesai' OR pem.status = 'sudah_periksa')")
            ->limit(500)
            ->get();
    }

    public function getTindakanById($id)
    {
        $this->db->select('dtp.*, td.nama , td.tarif_pasien' );
        $this->db->join('tarif_tindakan td', 'td.id = dtp.tarif_tindakan_id and td.is_active = 1');
        $this->db->join('pemeriksaan pem', 'pem.id = dtp.pemeriksaan_id and dtp.is_active = 1');
        $this->db->where('pem.pasien_id', $id );
        $this->db->where('dtp.is_active', '1' );
        return  $this->db->get('detail_tindakan_pemeriksaan dtp');
    }

    public function getTindakanLabByPemeriksaanId($id)
    {
        $this->db->select('dtp.*, td.nama , td.tarif_pasien' );
        $this->db->join('jenis_layanan_laboratorium td', 'td.id = dtp.jenis_layanan_id and td.is_active = 1');
        $this->db->join('pemeriksaan pem', 'pem.id = dtp.pemeriksaan_id and dtp.is_active = 1');
        $this->db->where('pem.id', $id );
        $this->db->where('dtp.is_active', '1' );
        return  $this->db->get('detail_tindakan_pemeriksaan_lab dtp');
    }

    public function getObatPemeriksaanById($id)
    {
        return $this->db->query("
            SELECT dop.*, o.nama, o.harga_jual
            FROM detail_obat_pemeriksaan dop
            JOIN obat o ON o.id = dop.obat_id and o.is_active = 1
            JOIN pemeriksaan pem ON pem.id = dop.pemeriksaan_id and pem.is_active = 1
            WHERE pem.pasien_id = $id
            AND dop.is_active = 1
                UNION ALL
            SELECT dop.id, pol.pemeriksaan_id, dop.obat_id, dop.jumlah_satuan, dop.signa_obat, dop.is_active, dop.created_at, dop.updated_at, dop.creator, o.nama, o.harga_jual
            FROM detail_penjualan_obat_luar dop
            JOIN obat o ON o.id = dop.obat_id and o.is_active = 1
            JOIN penjualan_obat_luar pol on dop.penjualan_obat_luar_id = pol.id
            JOIN pemeriksaan p on pol.pemeriksaan_id = p.id
            WHERE p.pasien_id = $id
            AND dop.is_active = 1
        ");
    }

    public function getPenyakitPemeriksaanById($id)
    {
        $this->db->select('dpp.*, pen.nama , pen.kode' );
        $this->db->join('penyakit pen', 'pen.id = dpp.penyakit_id and pen.is_active = 1');
        $this->db->join('pemeriksaan pem', 'pem.id = dpp.pemeriksaan_id and pem.is_active = 1');
        $this->db->where('pem.pasien_id', $id );
        $this->db->where('dpp.is_active', '1' );
        return  $this->db->get('detail_penyakit_pemeriksaan dpp');
    }

    public function getAllPenyakitPemeriksaan()
    {
        $this->db->select('dpp.pemeriksaan_id, pen.nama , pen.kode');
        $this->db->join('penyakit pen', 'pen.id = dpp.penyakit_id and pen.is_active = 1');
        $this->db->join('pemeriksaan pem', 'pem.id = dpp.pemeriksaan_id and pem.is_active = 1');
        $this->db->where('dpp.is_active', '1' );
        return  $this->db->get('detail_penyakit_pemeriksaan dpp');
    }

    public function getRacikanPemeriksaanById($id)
    {
        $sql = "SELECT id,nama_racikan, signa, sum(subtotal) as total from
                        (select dorp.*, ora.obat_id, ora.jumlah_satuan, o.nama, o.harga_jual, (ora.jumlah_satuan*o.harga_jual) as subtotal from detail_obat_racikan_pemeriksaan dorp 
                        join obat_racikan ora ON dorp.id = ora.detail_obat_racikan_pemeriksaan_id
                        join obat o on o.id = ora.obat_id where dorp.pemeriksaan_id = $id ) as AA group by id,nama_racikan,signa
                                    ";
        return $this->db->query($sql);
    }

    public function getRacikanPemeriksaan()
    {
        $sql = "SELECT id, pemeriksaan_id, nama_racikan, signa, sum(subtotal) as total from
(select dorp.*, ora.obat_id, ora.jumlah_satuan, o.nama, o.harga_jual, (ora.jumlah_satuan*o.harga_jual) as subtotal from detail_obat_racikan_pemeriksaan dorp 
join obat_racikan ora ON dorp.id = ora.detail_obat_racikan_pemeriksaan_id
join obat o on o.id = ora.obat_id 
join pemeriksaan pem ON pem.id = dorp.pemeriksaan_id  ) as AA group by id,pemeriksaan_id,nama_racikan,signa
            ";
        return $this->db->query($sql);
    }

    // ---------------- BY ID PEMERIKSAAN ---------------

    public function getTindakanByIdPemeriksaan($id) {
        $this->db->select('dtp.*, td.id as id_tarif_rindakan, td.nama , td.tarif_pasien, td.id' );
        $this->db->join('tarif_tindakan td', 'td.id = dtp.tarif_tindakan_id and td.is_active = 1');
        $this->db->join('pemeriksaan pem', 'pem.id = dtp.pemeriksaan_id and dtp.is_active = 1');
        $this->db->where('pem.id', $id );
        $this->db->where('dtp.is_active', '1' );
        return  $this->db->get('detail_tindakan_pemeriksaan dtp');
    }

    public function getTindakanKonsulByIdPemeriksaan($id) {
        $this->db->select('dtp.*, td.id as id_tarif_rindakan, td.nama , td.tarif_pasien, td.id, u.id as dokter_id, u.nama as nama_dokter');
        $this->db->join('tarif_tindakan td', 'td.id = dtp.tarif_tindakan_id and td.is_active = 1');
        $this->db->join('pemeriksaan pem', 'pem.id = dtp.pemeriksaan_id and dtp.is_active = 1');
        $this->db->join('user u', 'u.id = dtp.dokter_id and u.is_active = 1');
        $this->db->where('pem.id', $id );
        $this->db->where('dtp.is_active', '1' );
        return  $this->db->get('detail_tindakan_konsul dtp');
    }

    public function getObatPemeriksaanByIdPemeriksaan($id) {
//        $this->db->select('dop.*, o.nama , o.harga_jual, o.id' );
//        $this->db->join('obat o', 'o.id = dop.obat_id and o.is_active = 1');
//        $this->db->join('pemeriksaan pem', 'pem.id = dop.pemeriksaan_id and pem.is_active = 1');
//        $this->db->where('pem.id', $id );
//        $this->db->where('dop.is_active', '1' );
//        return $this->db->get('detail_obat_pemeriksaan dop');

        return $this->db->query("
            SELECT dop.*, o.nama, o.harga_jual, o.id
            FROM detail_obat_pemeriksaan dop
            JOIN obat o ON o.id = dop.obat_id and o.is_active = 1
            JOIN pemeriksaan pem ON pem.id = dop.pemeriksaan_id and pem.is_active = 1
            WHERE pem.id = $id
            AND dop.is_active = 1
                UNION ALL
            SELECT dop.id, pol.pemeriksaan_id, dop.obat_id, dop.jumlah_satuan, dop.signa_obat, dop.is_active, dop.created_at, dop.updated_at, dop.creator, o.nama, o.harga_jual, o.id
            FROM detail_penjualan_obat_luar dop
            JOIN obat o ON o.id = dop.obat_id and o.is_active = 1
            JOIN penjualan_obat_luar pol on dop.penjualan_obat_luar_id = pol.id
            JOIN pemeriksaan p on pol.pemeriksaan_id = p.id
            WHERE p.id = $id
            AND dop.is_active = 1
        ");
    }

    public function getPenyakitPemeriksaanByIdPemeriksaan($id) {
        $this->db->select('dpp.*, pen.id as id_penyakit, pen.nama , pen.kode, pen.id' );
        $this->db->join('penyakit pen', 'pen.id = dpp.penyakit_id and pen.is_active = 1');
        $this->db->join('pemeriksaan pem', 'pem.id = dpp.pemeriksaan_id and pem.is_active = 1');
        $this->db->where('pem.id', $id );
        $this->db->where('dpp.is_active', '1' );
        return  $this->db->get('detail_penyakit_pemeriksaan dpp');
    }

    public function getSimplePenyakitPemeriksaanByIdPemeriksaan($id) {
        $this->db->select('dpp.pemeriksaan_id, pen.nama , pen.kode' );
        $this->db->join('penyakit pen', 'pen.id = dpp.penyakit_id and pen.is_active = 1');
        $this->db->join('pemeriksaan pem', 'pem.id = dpp.pemeriksaan_id and pem.is_active = 1');
        $this->db->where('pem.id', $id );
        $this->db->where('dpp.is_active', '1' );
        return  $this->db->get('detail_penyakit_pemeriksaan dpp');
    }

    public function getSimplePenyakitPemeriksaanRiByIdPemeriksaan($id, $ranap = true) {
        if ($ranap) {
            $where = "WHERE d.rawat_inap_id = $id";
        }
        else {
            $where = "WHERE d.transfer_id = $id";
        }

        return $this->db->query("
            SELECT d.rawat_inap_id as pemeriksaan_id, p.nama, p.kode  
            FROM ri_diagnosis d
            JOIN ri_diagnosis_penyakit dp on d.id = dp.ri_diagnosis_id
            JOIN penyakit p on dp.penyakit_id = p.id
            $where
        ");
    }

    public function get_visit_rate($start, $end)
    {
        return $this->db->query("
            SELECT
                p.nama, jp.nama as poli, pem.waktu_pemeriksaan, pp.tipe_daftar, pp.penanggungjawab, a.kode_booking, pp.jaminan, pp.id as pendaftaran_id,
                (
                    SELECT max(success) FROM bpjs_jknmobile_log l WHERE l.kode_booking = a.kode_booking and url = 'antrean/add' group by a.kode_booking
                ) as add_antrian,
                (
                    SELECT response FROM bpjs_jknmobile_log l WHERE l.kode_booking = a.kode_booking and url = 'antrean/add' group by a.kode_booking
                ) as add_antrian_response,
                (
                    SELECT request FROM bpjs_jknmobile_log l WHERE l.kode_booking = a.kode_booking and url = 'antrean/add' group by a.kode_booking
                ) as add_antrian_request,
                (
                    SELECT waktu
                    FROM bpjs_jknmobile_log l
                    WHERE l.kode_booking = a.kode_booking
                      and l.task_id = 1
                      and url = 'antrean/updatewaktu'
                      and success = 1
                ) as task_1,
                (
                    SELECT waktu
                    FROM bpjs_jknmobile_log l
                    WHERE l.kode_booking = a.kode_booking
                      and l.task_id = 2
                      and url = 'antrean/updatewaktu'
                      and success = 1
                ) as task_2,
                (
                    SELECT waktu
                    FROM bpjs_jknmobile_log l
                    WHERE l.kode_booking = a.kode_booking
                      and l.task_id = 3
                      and url = 'antrean/updatewaktu'
                      and success = 1
                ) as task_3,
                (
                    SELECT waktu
                    FROM bpjs_jknmobile_log l
                    WHERE l.kode_booking = a.kode_booking
                      and l.task_id = 4
                      and url = 'antrean/updatewaktu'
                      and success = 1
                ) as task_4,
                (
                    SELECT waktu
                    FROM bpjs_jknmobile_log l
                    WHERE l.kode_booking = a.kode_booking
                      and l.task_id = 5
                      and url = 'antrean/updatewaktu'
                      and success = 1
                ) as task_5,
                (
                    SELECT waktu
                    FROM bpjs_jknmobile_log l
                    WHERE l.kode_booking = a.kode_booking
                      and l.task_id = 6
                      and url = 'antrean/updatewaktu'
                      and success = 1
                ) as task_6,
                (
                    SELECT waktu
                    FROM bpjs_jknmobile_log l
                    WHERE l.kode_booking = a.kode_booking
                      and l.task_id = 7
                      and url = 'antrean/updatewaktu'
                      and success = 1
                ) as task_7
            FROM pemeriksaan pem
            JOIN pasien p ON p.id = pem.pasien_id
            JOIN pendaftaran_pasien pp ON pp.id = pem.pendaftaran_id
            JOIN jenis_pendaftaran jp ON jp.id = pp.jenis_pendaftaran_id
            JOIN antrian a ON a.pemeriksaan_id = pem.id
            WHERE pem.waktu_pemeriksaan >= '$start 00:00:00' 
              and pem.waktu_pemeriksaan <= '$end 23:59:59'
              and jp.kode_bpjs is not null and pp.is_active = 1
            order by waktu_pemeriksaan desc
        ")->result();

    }
}
