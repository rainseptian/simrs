<?php
defined('BASEPATH') or exit('No direct script access allowed');

class MainModel extends CI_Model
{
    public function select($tabel, $where)
    {
        return $this->db->where($where[0], $where[1])->get($tabel);
    }

    public function insert($tabel, $data)
    {
        $final_data = $data;
        if ($tabel == 'bpjs_jknmobile_log' && $data['url'] == 'antrean/updatewaktu') {
            $req = json_decode($data['request'], true);
            $res = json_decode($data['response'], true);

            $final_data['kode_booking'] = $req['kodebooking'];
            $final_data['task_id'] = $req['taskid'];
            $final_data['waktu'] = $req['waktu'];
            $final_data['success'] = isset($res['success']) ? ($res['success'] ? 1 : 0) : 0;
        }
        if ($tabel == 'bpjs_jknmobile_log' && $data['url'] == 'antrean/add') {
            $req = json_decode($data['request'], true);
            $res = json_decode($data['response'], true);

            $final_data['kode_booking'] = $req['kodebooking'];
            $final_data['success'] = isset($res['success']) ? ($res['success'] ? 1 : 0) : 0;
        }

        return $this->db->insert($tabel, $final_data);
    }

    public function insert_id($tabel, $data)
    {
        $final_data = $data;
        if ($tabel == 'bpjs_jknmobile_log' && $data['url'] == 'antrean/updatewaktu') {
            $req = json_decode($data['request'], true);
            $res = json_decode($data['response'], true);

            $final_data['kode_booking'] = $req['kodebooking'];
            $final_data['task_id'] = $req['taskid'];
            $final_data['waktu'] = $req['waktu'];
            $final_data['success'] = isset($res['success']) ? ($res['success'] ? 1 : 0) : 0;
        }
        if ($tabel == 'bpjs_jknmobile_log' && $data['url'] == 'antrean/add') {
            $req = json_decode($data['request'], true);
            $res = json_decode($data['response'], true);

            $final_data['kode_booking'] = $req['kodebooking'];
            $final_data['success'] = isset($res['success']) ? ($res['success'] ? 1 : 0) : 0;
        }

        $this->db->insert($tabel, $final_data);
        return $this->db->insert_id();
    }

    public function update($tabel, $data, $id)
    {
        $this->db->where('id', $id);
        return $this->db->update($tabel, $data);
    }

    public function delete($tabel, $data, $id)
    {
        $this->db->where('id', $id);
        return $this->db->update($tabel, $data);
    }

    public function hardDelete($tabel, $id)
    {
        $this->db->where('id', $id);
        return $this->db->delete($tabel);
    }

    public function hardDeleteWhere($tabel, $where)
    {
        return $this->db->where($where[0], $where[1])->delete($tabel);
    }
}