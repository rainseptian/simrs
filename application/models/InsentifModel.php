<?php
defined('BASEPATH') or exit('No direct script access allowed');

class InsentifModel extends CI_Model
{
    public function listInsentifDokter($jenis, $dari, $sampai)
    {
        if ($jenis) {
            if ($jenis == "6") {
                return $this->db->query("
                    select
                        u.id as dokter_id, u.nama, (
                            ifnull((
                                select sum(tt.tarif_dokter)
                                from detail_tindakan_pemeriksaan dtp
                                join pemeriksaan p on p.id = dtp.pemeriksaan_id
                                join tarif_tindakan tt on tt.id = dtp.tarif_tindakan_id
                                join pasien on pasien.id = p.pasien_id and pasien.is_active = 1
                                where dtp.is_active = 1
                                and p.dokter_id = u.id
                                and (DATE(p.waktu_pemeriksaan) between '$dari' and '$sampai')
                                group by p.dokter_id
                            ), 0) +
                            ifnull((
                                select sum(tt.tarif_dokter)
                                from detail_tindakan_konsul dtk
                                join pemeriksaan p on p.id = dtk.pemeriksaan_id
                                join tarif_tindakan tt on tt.id = dtk.tarif_tindakan_id
                                join pasien on pasien.id = p.pasien_id and pasien.is_active = 1
                                where dtk.is_active = 1
                                and dtk.dokter_id = u.id
                                and (DATE(p.waktu_pemeriksaan) between '$dari' and '$sampai')
                                group by dtk.dokter_id
                            ), 0) 
                        ) as total_insentif
                    from user as u
                    join user_grup ug on ug.user_id = u.id
                    join grup g on ug.grup_id = g.id
                    where g.nama_grup = 'dokter'
                    order by total_insentif desc
                ");
            }
            else {
                return $this->db->query("
                SELECT u.id dokter_id, u.nama, IFNULL(insentif.total_insentif, 0)  + IFNULL(tamu.tarif_tamu, 0) total_insentif
                FROM user u
                join user_grup ug on ug.user_id = u.id
                join grup g on ug.grup_id = g.id
                left join (
                    select u.id, sum(tt.tarif_dokter) total_insentif
                        FROM `ri_biaya_tindakan` rbt
                        JOIN ri_biaya rb on rbt.ri_biaya_id = rb.id
                        JOIN rawat_inap ri on rb.rawat_inap_id = ri.id
                        join pasien on pasien.id = ri.pasien_id and pasien.is_active = 1
                        JOIN tarif_tindakan tt on rbt.tindakan_id = tt.id
                        join transfer t on t.id = rb.transfer_id
                        join user u on u.id = ri.dokter_id  
                        WHERE rbt.tipe_tindakan = 'asli'
                        and t.jenis_ruangan_id = $jenis
                        and (DATE(rbt.created_at) between '$dari' and ' $sampai')
                        GROUP BY ri.dokter_id
                    UNION ALL
                    select u.id, sum(tt.tarif_dokter) total_insentif
                        FROM `ri_biaya_tindakan` rbt
                        JOIN tarif_tindakan tt on rbt.tindakan_id = tt.id
                        JOIN ri_biaya rb on rbt.ri_biaya_id = rb.id
                        JOIN rawat_inap ri on rb.rawat_inap_id = ri.id
                        join pasien on pasien.id = ri.pasien_id and pasien.is_active = 1
                        join transfer t on t.id = rb.transfer_id
                        join user u on u.id = rbt.dokter_anestesi  
                        WHERE rbt.tipe_tindakan = 'anestesi'
                        and t.jenis_ruangan_id = $jenis
                        and (DATE(rbt.created_at) between '$dari' and ' $sampai')
                        GROUP BY rbt.dokter_anestesi
                    UNION ALL
                    select u.id, sum(tt.tarif_dokter)
                        from ri_detail_tindakan_konsul dtk
                        join ri_pemeriksaan p on p.id = dtk.ri_pemeriksaan_id
                        join tarif_tindakan tt on tt.id = dtk.tarif_tindakan_id
                        join pasien on pasien.id = p.pasien_id and pasien.is_active = 1
                        join user u on u.id = dtk.dokter_id 
                        where dtk.dokter_id = u.id
                        and (DATE(p.created_at) between '$dari' and ' $sampai')
                        group by dtk.dokter_id 
                ) insentif on insentif.id = u.id
                LEFT join (
                    select rbt.dokter_tamu, IFNULL(sum(tarif_per_dokter_tamu), 0) tarif_tamu
                    from ri_biaya_tindakan rbt
                    JOIN ri_biaya rb on rbt.ri_biaya_id = rb.id
                    JOIN rawat_inap ri on rb.rawat_inap_id = ri.id
                    join pasien on pasien.id = ri.pasien_id and pasien.is_active = 1
                    join transfer t on t.id = rb.transfer_id
                    where rbt.tipe_tindakan = 'tamu'
                    and t.jenis_ruangan_id = $jenis
                    and (DATE(rbt.created_at) between '$dari' and ' $sampai')
                ) tamu on find_in_set(u.id, tamu.dokter_tamu) > 0
                 where g.nama_grup = 'dokter'
                order by 3 desc
                ");
            }
        }
        else {
            return $this->db->query("
            SELECT u.id, u.nama, IFNULL(SUM(insentif.total_insentif), 0) total_insentif
                FROM user u
                join user_grup ug on ug.user_id = u.id
                join grup g on ug.grup_id = g.id
                left join (
                    select u.id, u.nama, sum(tt.tarif_dokter) total_insentif
                    from detail_tindakan_pemeriksaan dtp
                    join pemeriksaan p on p.id = dtp.pemeriksaan_id
                    join pasien on pasien.id = p.pasien_id and pasien.is_active = 1
                    join tarif_tindakan tt on tt.id = dtp.tarif_tindakan_id
                    join user u on u.id = p.dokter_id 
                    WHERE dtp.is_active = 1
                        and p.dokter_id = u.id
                        and (DATE(p.waktu_pemeriksaan) between '$dari' and '$sampai')
                    group by p.dokter_id
                    UNION ALL 
                    select u.id, u.nama, sum(tt.tarif_dokter)
                    from detail_tindakan_konsul dtk
                    join pemeriksaan p on p.id = dtk.pemeriksaan_id
                    join pasien on pasien.id = p.pasien_id and pasien.is_active = 1
                    join tarif_tindakan tt on tt.id = dtk.tarif_tindakan_id
                    join user u on u.id = dtk.dokter_id 
                    where dtk.is_active = 1
                        and dtk.dokter_id = u.id
                        and (DATE(p.waktu_pemeriksaan) between '$dari' and '$sampai')
                    group by dtk.dokter_id
                    UNION ALL 
                    select u.id, u.nama, sum(tt.tarif_dokter)
                        FROM `ri_biaya_tindakan` rbt
                        JOIN ri_biaya rb on rbt.ri_biaya_id = rb.id
                        JOIN rawat_inap ri on rb.rawat_inap_id = ri.id
                        JOIN tarif_tindakan tt on rbt.tindakan_id = tt.id
                        join pasien on pasien.id = ri.pasien_id and pasien.is_active = 1
                        join user u on u.id = ri.dokter_id  
                        WHERE rbt.tipe_tindakan = 'asli'
                        and (DATE(rbt.created_at) between '$dari' and '$sampai')
                        GROUP BY ri.dokter_id
                    UNION ALL
                    select u.id, u.nama,  sum(tt.tarif_dokter)
                        FROM `ri_biaya_tindakan` rbt
                        JOIN tarif_tindakan tt on rbt.tindakan_id = tt.id
                        JOIN ri_biaya rb on rb.id = rbt.ri_biaya_id
                        JOIN rawat_inap ri on ri.id = rb.rawat_inap_id
                        join pasien on pasien.id = ri.pasien_id and pasien.is_active = 1
                        join user u on u.id = rbt.dokter_anestesi  
                        WHERE rbt.dokter_anestesi = u.id
                        AND rbt.tipe_tindakan = 'anestesi'
                        and (DATE(rbt.created_at) between '$dari' and '$sampai')
                        GROUP BY rbt.dokter_anestesi
                    UNION ALL 
                    select  u.id, u.nama, sum(tt.tarif_dokter)
                        from ri_detail_tindakan_konsul dtk
                        join ri_pemeriksaan p on p.id = dtk.ri_pemeriksaan_id
                        join tarif_tindakan tt on tt.id = dtk.tarif_tindakan_id
                        join pasien on pasien.id = p.pasien_id and pasien.is_active = 1
                        join user u on u.id = dtk.dokter_id  
                        where dtk.is_active = 1
                        and (DATE(p.created_at) between '$dari' and '$sampai')
                        group by dtk.dokter_id
                    UNION ALL 
                    select u.id, u.nama, IFNULL( sum(tarif_per_dokter_tamu), 0) tarif_tamu
                        from ri_biaya_tindakan rbt
                        JOIN ri_biaya rb on rb.id = rbt.ri_biaya_id
                        JOIN rawat_inap ri on ri.id = rb.rawat_inap_id
                        join pasien on pasien.id = ri.pasien_id and pasien.is_active = 1
                        join user u on u.id = rbt.dokter_tamu  
                        WHERE rbt.tipe_tindakan = 'tamu'
                        and (DATE(rbt.created_at) between '$dari' and '$sampai')
                    group by u.id, u.nama
                ) insentif on insentif.id = u.id
                where g.nama_grup = 'dokter'
                group by u.id, u.nama
                order by 3 desc;
            ");
        }
    }

    public function DetailInsentifDokter($id, $jenis, $dari, $sampai)
    {
        if ($jenis) {
            if ($jenis == 6) {
                return $this->db->query("
                    SELECT ab.* FROM (
                        select 1 as table_id, p.waktu_pemeriksaan, tt.nama nama_tindakan, tt.tarif_dokter, u.nama nama_dokter, pas.nama nama_pasien, p.no_rm, p.jaminan tipe_pasien, jp.nama jenis_ruangan
                        from detail_tindakan_pemeriksaan dtp
                        join pemeriksaan p on p.id = dtp.pemeriksaan_id
                        join pendaftaran_pasien pp on pp.id = p.pendaftaran_id
                        join jenis_pendaftaran jp on jp.id = pp.jenis_pendaftaran_id
                        join pasien pas on pas.id = p.pasien_id and pas.is_active = 1
                        join user u on u.id = p.dokter_id
                        join tarif_tindakan tt on tt.id = dtp.tarif_tindakan_id
                        where dtp.is_active = 1
                        and p.dokter_id = {$id}
                        and (DATE(p.waktu_pemeriksaan) between '$dari' and '$sampai')
                    UNION ALL
                        select 2 as table_id, p.waktu_pemeriksaan, tt.nama nama_tindakan, tt.tarif_dokter, u.nama nama_dokter, pas.nama nama_pasien, p.no_rm, p.jaminan tipe_pasien, jp.nama jenis_ruangan
                        from detail_tindakan_konsul dtk
                        join pemeriksaan p on p.id = dtk.pemeriksaan_id
                        join pendaftaran_pasien pp on pp.id = p.pendaftaran_id
                        join jenis_pendaftaran jp on jp.id = pp.jenis_pendaftaran_id
                        join pasien pas on pas.id = p.pasien_id and pas.is_active = 1
                        join user u on u.id = dtk.dokter_id
                        join tarif_tindakan tt on tt.id = dtk.tarif_tindakan_id
                        where dtk.is_active = 1
                        and dtk.dokter_id = {$id}
                        and (DATE(dtk.created_at) between '$dari' and '$sampai')
                    ) ab
                    ORDER BY ab.waktu_pemeriksaan
                ");
            }
            else {
                return $this->db->query("
                    SELECT ab.* FROM (
                        select 3 as table_id, rbt.created_at as waktu_pemeriksaan, tt.nama nama_tindakan, tt.tarif_dokter, u.nama nama_dokter, pas.nama nama_pasien, pas.no_rm, ri.tipe_pasien, jr.nama jenis_ruangan
                        FROM `ri_biaya_tindakan` rbt
                        JOIN ri_biaya rb on rbt.ri_biaya_id = rb.id
                        JOIN jenis_ruangan jr on jr.id = rb.jenis_ruangan_id 
                        JOIN rawat_inap ri on rb.rawat_inap_id = ri.id
                        JOIN tarif_tindakan tt on rbt.tindakan_id = tt.id
                        join pasien pas on pas.id = ri.pasien_id and pas.is_active = 1
                        JOIN user u on ri.dokter_id = u.id
                        join transfer t on t.id = rb.transfer_id
                        WHERE ri.dokter_id = {$id}
                        AND rbt.tipe_tindakan = 'asli'
                        and (DATE(rbt.created_at) between '$dari' and '$sampai')
                        and t.jenis_ruangan_id = $jenis
                    UNION ALL
                        select 4 as table_id, rbt.created_at as waktu_pemeriksaan, tt.nama nama_tindakan, tt.tarif_dokter, u.nama nama_dokter, pas.nama nama_pasien, pas.no_rm, ri.tipe_pasien, jr.nama jenis_ruangan
                        FROM `ri_biaya_tindakan` rbt
                        JOIN tarif_tindakan tt on rbt.tindakan_id = tt.id
                        JOIN ri_biaya rb on rbt.ri_biaya_id = rb.id
                        JOIN jenis_ruangan jr on jr.id = rb.jenis_ruangan_id 
                        JOIN rawat_inap ri on rb.rawat_inap_id = ri.id
                        join pasien pas on pas.id = ri.pasien_id and pas.is_active = 1
                        JOIN user u on rbt.dokter_anestesi = u.id
                        join transfer t on t.id = rb.transfer_id
                        WHERE rbt.dokter_anestesi = {$id}
                        AND rbt.tipe_tindakan = 'anestesi'
                        and (DATE(rbt.created_at) between '$dari' and '$sampai')
                        and t.jenis_ruangan_id = $jenis
                    UNION ALL
                        SELECT 5 as table_id, rbt.created_at as waktu_pemeriksaan, tt.nama nama_tindakan, rbt.tarif_per_dokter_tamu as tarif_dokter, (select nama from user where id = {$id}) as nama_dokter, pas.nama nama_pasien, pas.no_rm, ri.tipe_pasien, jr.nama jenis_ruangan
                        from ri_biaya_tindakan rbt
                        join tarif_tindakan tt on tt.id = rbt.tindakan_id
                        JOIN ri_biaya rb on rbt.ri_biaya_id = rb.id
                        JOIN jenis_ruangan jr on jr.id = rb.jenis_ruangan_id  
                        JOIN rawat_inap ri on rb.rawat_inap_id = ri.id
                        join transfer t on t.id = rb.transfer_id
                        join pasien pas on pas.id = ri.pasien_id and pas.is_active = 1
                        WHERE find_in_set({$id}, rbt.dokter_tamu)
                        AND rbt.tipe_tindakan = 'tamu'
                        and (DATE(rbt.created_at) between '$dari' and '$sampai')
                        and t.jenis_ruangan_id = $jenis
                    ) ab
                    ORDER BY ab.waktu_pemeriksaan
                ");
            }
        }
        else {
            return $this->db->query("
                SELECT ab.* FROM (
                    select 1 as table_id, p.waktu_pemeriksaan, tt.nama nama_tindakan, tt.tarif_dokter, u.nama nama_dokter, pas.nama nama_pasien, p.no_rm, p.jaminan tipe_pasien, jp.nama jenis_ruangan
                    from detail_tindakan_pemeriksaan dtp
                    join pemeriksaan p on p.id = dtp.pemeriksaan_id
                    join pendaftaran_pasien pp on pp.id = p.pendaftaran_id
                    join jenis_pendaftaran jp on jp.id = pp.jenis_pendaftaran_id
                    join pasien pas on pas.id = p.pasien_id and pas.is_active = 1
                    join user u on u.id = p.dokter_id
                    join tarif_tindakan tt on tt.id = dtp.tarif_tindakan_id
                    where dtp.is_active = 1
                    and p.dokter_id = {$id}
                    and (DATE(p.waktu_pemeriksaan) between '$dari' and '$sampai')
                UNION ALL
                    select 2 as table_id, p.waktu_pemeriksaan, tt.nama nama_tindakan, tt.tarif_dokter, u.nama nama_dokter, pas.nama nama_pasien, p.no_rm, p.jaminan tipe_pasien, jp.nama jenis_ruangan
                    from detail_tindakan_konsul dtk
                    join pemeriksaan p on p.id = dtk.pemeriksaan_id
                    join pendaftaran_pasien pp on pp.id = p.pendaftaran_id
                    join jenis_pendaftaran jp on jp.id = pp.jenis_pendaftaran_id
                    join pasien pas on pas.id = p.pasien_id and pas.is_active = 1
                    join user u on u.id = dtk.dokter_id
                    join tarif_tindakan tt on tt.id = dtk.tarif_tindakan_id
                    where dtk.is_active = 1
                    and dtk.dokter_id = {$id}
                    and (DATE(dtk.created_at) between '$dari' and '$sampai')
                UNION ALL
                    select 3 as table_id, rbt.created_at as waktu_pemeriksaan, tt.nama nama_tindakan, tt.tarif_dokter, u.nama nama_dokter, pas.nama nama_pasien, pas.no_rm, ri.tipe_pasien, jr.nama jenis_ruangan
                    FROM `ri_biaya_tindakan` rbt
                    JOIN ri_biaya rb on rbt.ri_biaya_id = rb.id
                    JOIN jenis_ruangan jr on jr.id = rb.jenis_ruangan_id 
                    JOIN rawat_inap ri on rb.rawat_inap_id = ri.id
                    JOIN tarif_tindakan tt on rbt.tindakan_id = tt.id
                    join pasien pas on pas.id = ri.pasien_id and pas.is_active = 1
                    JOIN user u on ri.dokter_id = u.id
                    join transfer t on t.id = rb.transfer_id
                    WHERE ri.dokter_id = {$id}
                    AND rbt.tipe_tindakan = 'asli'
                    and (DATE(rbt.created_at) between '$dari' and '$sampai')
                UNION ALL
                    select 4 as table_id, rbt.created_at as waktu_pemeriksaan, tt.nama nama_tindakan, tt.tarif_dokter, u.nama nama_dokter, pas.nama nama_pasien, pas.no_rm, ri.tipe_pasien, jr.nama jenis_ruangan
                    FROM `ri_biaya_tindakan` rbt
                    JOIN tarif_tindakan tt on rbt.tindakan_id = tt.id
                    JOIN ri_biaya rb on rbt.ri_biaya_id = rb.id
                    JOIN jenis_ruangan jr on jr.id = rb.jenis_ruangan_id 
                    JOIN rawat_inap ri on rb.rawat_inap_id = ri.id
                    join pasien pas on pas.id = ri.pasien_id and pas.is_active = 1
                    JOIN user u on rbt.dokter_anestesi = u.id
                    join transfer t on t.id = rb.transfer_id
                    WHERE rbt.dokter_anestesi = {$id}
                    AND rbt.tipe_tindakan = 'anestesi'
                    and (DATE(rbt.created_at) between '$dari' and '$sampai')
                UNION ALL
                    SELECT 5 as table_id, rbt.created_at as waktu_pemeriksaan, tt.nama nama_tindakan, rbt.tarif_per_dokter_tamu as tarif_dokter, (select nama from user where id = {$id}) as nama_dokter, pas.nama nama_pasien, pas.no_rm, ri.tipe_pasien, jr.nama jenis_ruangan
                    from ri_biaya_tindakan rbt
                    join tarif_tindakan tt on tt.id = rbt.tindakan_id
                    JOIN ri_biaya rb on rbt.ri_biaya_id = rb.id
                    JOIN jenis_ruangan jr on jr.id = rb.jenis_ruangan_id  
                    JOIN rawat_inap ri on rb.rawat_inap_id = ri.id
                    join pasien pas on pas.id = ri.pasien_id and pas.is_active = 1
                    join transfer t on t.id = rb.transfer_id
                    WHERE find_in_set({$id}, rbt.dokter_tamu)
                    AND rbt.tipe_tindakan = 'tamu'
                    and (DATE(rbt.created_at) between '$dari' and '$sampai')
                ) ab
                ORDER BY ab.waktu_pemeriksaan
            ");
        }
    }

    public function shiftDokter()
    {
        return $this->db->query("
      select b.*, u.nama from
        (select dokter_id, count(*) hari,(select insentif from insentif_shift where shift ='dokter') as shift, (count(*)*(select insentif from insentif_shift where shift ='dokter')) as jumlah from
            (SELECT YEAR(waktu_pemeriksaan) Y,MONTH(waktu_pemeriksaan) M,DAY(waktu_pemeriksaan) D, dokter_id from
                pemeriksaan  group by YEAR(waktu_pemeriksaan),MONTH(waktu_pemeriksaan),DAY(waktu_pemeriksaan), dokter_id) as A  group by dokter_id) as b JOIN user u ON b.dokter_id = u.id");

    }

    public function listInsentifPerawat($jenis, $dari, $sampai)
    {
        if ($jenis) {
            if ($jenis == 6) {
                return $this->db->query("
                    select 
                        u.id as perawat_id, u.nama, (
                            ifnull((
                                select sum(tarif_per_perawat) 
                                from detail_tindakan_pemeriksaan 
                                join pemeriksaan p on p.id = detail_tindakan_pemeriksaan.pemeriksaan_id
                                join pendaftaran_pasien pp on pp.id = p.pendaftaran_id
                                join pasien pas on pas.id = p.pasien_id and pas.is_active = 1
                                where find_in_set(u.id, perawat)
                                and (DATE(detail_tindakan_pemeriksaan.created_at) between '$dari' and '$sampai')
                            ), 0) +
                            ifnull((
                                select sum(tarif_per_perawat) 
                                from detail_tindakan_konsul 
                                join pemeriksaan p on p.id = detail_tindakan_konsul.pemeriksaan_id
                                join pendaftaran_pasien pp on pp.id = p.pendaftaran_id
                                join pasien pas on pas.id = p.pasien_id and pas.is_active = 1
                                where find_in_set(u.id, perawat)
                                and (DATE(detail_tindakan_konsul.created_at) between '$dari' and '$sampai')
                            ), 0)
                        ) as total_insentif
                    from user as u 
                    join user_grup ug on ug.user_id = u.id
                    join grup g on ug.grup_id = g.id
                    where g.nama_grup = 'perawat'
                    order by total_insentif desc
                ");
            }
            else {
                return $this->db->query("
                    select 
                        u.id as perawat_id, u.nama, (
                            ifnull((
                                select sum(tarif_per_perawat) 
                                from ri_biaya_tindakan 
                                JOIN ri_biaya rb on ri_biaya_tindakan.ri_biaya_id = rb.id
                                join transfer t on t.id = rb.transfer_id
                                JOIN rawat_inap ri on rb.rawat_inap_id = ri.id
                                join pasien pas on pas.id = ri.pasien_id and pas.is_active = 1
                                where find_in_set(u.id, perawat)
                                and (DATE(ri_biaya_tindakan.created_at) between '$dari' and '$sampai')
                                and t.jenis_ruangan_id = $jenis
                            ), 0)
                        ) as total_insentif
                    from user as u 
                    join user_grup ug on ug.user_id = u.id
                    join grup g on ug.grup_id = g.id
                    where g.nama_grup = 'perawat'
                    order by total_insentif desc
                ");
            }
        }
        else {
            return $this->db->query("
                select 
                    u.id as perawat_id, u.nama, (
                        ifnull((
                            select sum(tarif_per_perawat) 
                            from detail_tindakan_pemeriksaan 
                            join pemeriksaan p on p.id = detail_tindakan_pemeriksaan.pemeriksaan_id
                            join pendaftaran_pasien pp on pp.id = p.pendaftaran_id
                            join pasien pas on pas.id = p.pasien_id and pas.is_active = 1
                            where find_in_set(u.id, perawat)
                            and (DATE(detail_tindakan_pemeriksaan.created_at) between '$dari' and '$sampai')
                        ), 0) +
                        ifnull((
                            select sum(tarif_per_perawat) 
                            from detail_tindakan_konsul 
                            join pemeriksaan p on p.id = detail_tindakan_konsul.pemeriksaan_id
                            join pendaftaran_pasien pp on pp.id = p.pendaftaran_id
                            join pasien pas on pas.id = p.pasien_id and pas.is_active = 1
                            where find_in_set(u.id, perawat)
                            and (DATE(detail_tindakan_konsul.created_at) between '$dari' and '$sampai')
                        ), 0) +
                        ifnull((
                            select sum(tarif_per_perawat) 
                            from ri_biaya_tindakan 
                            JOIN ri_biaya rb on ri_biaya_tindakan.ri_biaya_id = rb.id
                            JOIN rawat_inap ri on rb.rawat_inap_id = ri.id
                            join pasien pas on pas.id = ri.pasien_id and pas.is_active = 1
                            where find_in_set(u.id, perawat)
                            and (DATE(ri_biaya_tindakan.created_at) between '$dari' and '$sampai')
                        ), 0)
                    ) as total_insentif
                from user as u 
                join user_grup ug on ug.user_id = u.id
                join grup g on ug.grup_id = g.id
                where g.nama_grup = 'perawat'
                order by total_insentif desc
            ");
        }
    }

    public function DetailInsentifPerawat($id, $jenis, $dari, $sampai)
    {
        if ($jenis) {
            if ($jenis == 6) {
                return $this->db->query("
                    SELECT ab.* FROM (
                        SELECT 1 as table_id, dtp.jumlah_perawat, dtp.tarif_per_perawat, p.waktu_pemeriksaan, tt.tarif_perawat, tt.nama as nama_tindakan, (select nama from user where id = {$id}) as nama_perawat, pas.nama nama_pasien, p.no_rm, p.jaminan tipe_pasien, jp.nama jenis_ruangan
                        from detail_tindakan_pemeriksaan dtp
                        join pemeriksaan p on p.id = dtp.pemeriksaan_id
                        join pendaftaran_pasien pp on pp.id = p.pendaftaran_id
                        join jenis_pendaftaran jp on jp.id = pp.jenis_pendaftaran_id
                        join pasien pas on pas.id = p.pasien_id and pas.is_active = 1
                        join tarif_tindakan tt on tt.id = dtp.tarif_tindakan_id
                        where dtp.is_active = 1
                        and find_in_set({$id}, dtp.perawat)
                        and (DATE(dtp.created_at) between '$dari' and '$sampai')
                    UNION ALL
                        SELECT 2 as table_id, dtk.jumlah_perawat, dtk.tarif_per_perawat, p.waktu_pemeriksaan, tt.tarif_perawat, tt.nama as nama_tindakan, (select nama from user where id = {$id}) as nama_perawat, pas.nama nama_pasien, p.no_rm, p.jaminan tipe_pasien, jp.nama jenis_ruangan
                        from detail_tindakan_konsul dtk
                        join pemeriksaan p on p.id = dtk.pemeriksaan_id
                        join pendaftaran_pasien pp on pp.id = p.pendaftaran_id
                        join jenis_pendaftaran jp on jp.id = pp.jenis_pendaftaran_id
                        join pasien pas on pas.id = p.pasien_id and pas.is_active = 1
                        join tarif_tindakan tt on tt.id = dtk.tarif_tindakan_id
                        where dtk.is_active = 1
                        and find_in_set({$id}, dtk.perawat)
                        and (DATE(dtk.created_at) between '$dari' and '$sampai')
                    ) ab
                    ORDER BY ab.waktu_pemeriksaan
                ");
            }
            else {
                return $this->db->query("
                    SELECT ab.* FROM (
                        SELECT 3 as table_id, rbt.jumlah_perawat, rbt.tarif_per_perawat, rbt.created_at as waktu_pemeriksaan, tt.tarif_perawat, tt.nama as nama_tindakan, (select nama from user where id = {$id}) as nama_perawat, pas.nama nama_pasien, pas.no_rm, ri.tipe_pasien, jr.nama jenis_ruangan
                        from ri_biaya_tindakan rbt
                        JOIN ri_biaya rb on rbt.ri_biaya_id = rb.id
                        JOIN jenis_ruangan jr on jr.id = rb.jenis_ruangan_id  
                        JOIN rawat_inap ri on rb.rawat_inap_id = ri.id
                        join pasien pas on pas.id = ri.pasien_id and pas.is_active = 1
                        join tarif_tindakan tt on tt.id = rbt.tindakan_id
                        join transfer t on t.id = rb.transfer_id
                        and find_in_set({$id}, rbt.perawat)
                        and (DATE(rbt.created_at) between '$dari' and '$sampai')
                        and t.jenis_ruangan_id = $jenis
                    ) ab
                    ORDER BY ab.waktu_pemeriksaan
                ");
            }
        }
        else {
            return $this->db->query("
                SELECT ab.* FROM (
                    SELECT 1 as table_id, dtp.jumlah_perawat, dtp.tarif_per_perawat, p.waktu_pemeriksaan, tt.tarif_perawat, tt.nama as nama_tindakan, (select nama from user where id = {$id}) as nama_perawat, pas.nama nama_pasien, p.no_rm, p.jaminan tipe_pasien, jp.nama jenis_ruangan
                    from detail_tindakan_pemeriksaan dtp
                    join pemeriksaan p on p.id = dtp.pemeriksaan_id
                    join pendaftaran_pasien pp on pp.id = p.pendaftaran_id
                    join jenis_pendaftaran jp on jp.id = pp.jenis_pendaftaran_id
                    join pasien pas on pas.id = p.pasien_id and pas.is_active = 1
                    join tarif_tindakan tt on tt.id = dtp.tarif_tindakan_id
                    where dtp.is_active = 1
                    and find_in_set({$id}, dtp.perawat)
                    and (DATE(dtp.created_at) between '$dari' and '$sampai')
                UNION ALL
                    SELECT 2 as table_id, dtk.jumlah_perawat, dtk.tarif_per_perawat, p.waktu_pemeriksaan, tt.tarif_perawat, tt.nama as nama_tindakan, (select nama from user where id = {$id}) as nama_perawat, pas.nama nama_pasien, p.no_rm, p.jaminan tipe_pasien, jp.nama jenis_ruangan
                    from detail_tindakan_konsul dtk
                    join pemeriksaan p on p.id = dtk.pemeriksaan_id
                    join pendaftaran_pasien pp on pp.id = p.pendaftaran_id
                    join jenis_pendaftaran jp on jp.id = pp.jenis_pendaftaran_id
                    join pasien pas on pas.id = p.pasien_id and pas.is_active = 1
                    join tarif_tindakan tt on tt.id = dtk.tarif_tindakan_id
                    where dtk.is_active = 1
                    and find_in_set({$id}, dtk.perawat)
                    and (DATE(dtk.created_at) between '$dari' and '$sampai')
                UNION ALL
                    SELECT 3 as table_id, rbt.jumlah_perawat, rbt.tarif_per_perawat, rbt.created_at as waktu_pemeriksaan, tt.tarif_perawat, tt.nama as nama_tindakan, (select nama from user where id = {$id}) as nama_perawat, pas.nama nama_pasien, pas.no_rm, ri.tipe_pasien, jr.nama jenis_ruangan
                    from ri_biaya_tindakan rbt
                    JOIN ri_biaya rb on rbt.ri_biaya_id = rb.id
                    JOIN jenis_ruangan jr on jr.id = rb.jenis_ruangan_id  
                    JOIN rawat_inap ri on rb.rawat_inap_id = ri.id
                    join pasien pas on pas.id = ri.pasien_id and pas.is_active = 1
                    join tarif_tindakan tt on tt.id = rbt.tindakan_id
                    and find_in_set({$id}, rbt.perawat)
                    and (DATE(rbt.created_at) between '$dari' and '$sampai')
                ) ab
                ORDER BY ab.waktu_pemeriksaan
            ");
        }
    }

    public function shiftPerawat()
    {
        return $this->db->query("select b.*, u.nama from
        (select perawat_id, count(*) hari, (select insentif from insentif_shift where shift ='perawat') as shift, (count(*)*(select insentif from insentif_shift where shift ='perawat')) as jumlah from
            (SELECT YEAR(waktu_pemeriksaan) Y,MONTH(waktu_pemeriksaan) M,DAY(waktu_pemeriksaan) D, perawat_id from
                pemeriksaan  group by YEAR(waktu_pemeriksaan),MONTH(waktu_pemeriksaan),DAY(waktu_pemeriksaan), perawat_id) as A  group by perawat_id) as b JOIN user u ON b.perawat_id = u.id");

    }

    public function shiftApoteker()
    {
        return $this->db->query("select b.*, u.nama from
        (select apoteker_id, count(*) hari, (select insentif from insentif_shift where shift ='apoteker') as shift, (count(*)*(select insentif from insentif_shift where shift ='apoteker')) as jumlah from
            (SELECT YEAR(waktu_pemeriksaan) Y,MONTH(waktu_pemeriksaan) M,DAY(waktu_pemeriksaan) D, apoteker_id from
                pemeriksaan  group by YEAR(waktu_pemeriksaan),MONTH(waktu_pemeriksaan),DAY(waktu_pemeriksaan), apoteker_id) as A  group by apoteker_id) as b JOIN user u ON b.apoteker_id = u.id");

    }

    public function listInsentifShiftDokter()
    {
        $this->db->select('p.dokter_id, u.nama');
        $this->db->join('pemeriksaan p', 'p.id = dtp.pemeriksaan_id');
        $this->db->join('user u', 'u.id = p.dokter_id');
        $this->db->where('dtp.is_active', 1);
        $this->db->group_by('p.dokter_id');
        return $this->db->get('detail_tindakan_pemeriksaan dtp');
    }

    public function DetailInsentifApoteker($id)
    {
        return $this->db->select('pemeriksaan.waktu_pemeriksaan, pemeriksaan.no_rm, user.id, user.nama')
            ->from('pemeriksaan')
            ->join('user', 'user.id = pemeriksaan.apoteker_id', 'left')
            ->where(['pemeriksaan.is_active' => 1, 'user.id' => $id])
            ->get()->result();
    }

    public function getInsentifResep()
    {
        return $this->db->select('insentif')
            ->from('insentif_shift')
            ->where(['shift' => 'resep'])
            ->get()->row()->insentif;
    }

    public function listInsentifApoteker()
    {
        return $this->db->select('
          pemeriksaan.apoteker_id AS the_apotek,
          (SELECT COUNT(*) FROM pemeriksaan WHERE apoteker_id= the_apotek) AS jml_resep,
          user.nama
          ')
            ->from('pemeriksaan')
            ->join('user', 'user.id = pemeriksaan.apoteker_id', 'left')
            ->where([
                'pemeriksaan.is_active' => 1,
                ' pemeriksaan.apoteker_id >' => 0
            ])
            ->group_by('pemeriksaan.apoteker_id')
            ->get()->result();

    }

    public function getObatById($id)
    {
        $this->db->where('is_active', 1);
        $this->db->where('id', $id);
        return $this->db->get('obat');
    }

    public function getSettingpersen()
    {
        $this->db->where('is_active', 1);
        return $this->db->get('prosentase_harga');
    }

    public function getUserById($id)
    {

        $this->db->select('u.*,ug.id as user_grup_id, ug.grup_id, g.nama_grup');
        $this->db->from('user u');
        $this->db->join('user_grup ug', 'ug.user_id = u.id');
        $this->db->join('grup g', 'ug.grup_id = g.id');
        $this->db->where('u.is_active', 1);
        $this->db->where('u.id', $id);

        return $this->db->get();
    }

    public function getGrup()
    {
        $this->db->where('is_active', 1);
        return $this->db->get('grup');
    }

    public function listShift()
    {

        return $this->db->get('insentif_shift');
    }

    public function listShiftById($id)
    {
        $this->db->where('id', $id);
        return $this->db->get('insentif_shift');
    }

    public function listInsentifObatDokter($jenis, $dari, $sampai, $resep_luar)
    {
        if ($resep_luar)
        {
            return $this->db->query("
                select
                    u.id as dokter_id, u.nama, (ifnull((
                        select sum((harga_jual - harga_beli) * detail_penjualan_obat_luar.jumlah_satuan)
                        from detail_penjualan_obat_luar
                        join obat on obat.id = detail_penjualan_obat_luar.obat_id
                        join penjualan_obat_luar on penjualan_obat_luar.id = detail_penjualan_obat_luar.penjualan_obat_luar_id
                        where penjualan_obat_luar.dokter_id = u.id
                        and penjualan_obat_luar.tipe = 'resep_luar'
                        and penjualan_obat_luar.progress = 'selesai'
                        and (DATE(penjualan_obat_luar.created_at) between '$dari' and '$sampai')
                    ), 0) + ifnull((
                        select sum((harga_jual - harga_beli) * obat_racikan_luar.jumlah_satuan)
                        from obat_racikan_luar
                        join obat on obat.id = obat_racikan_luar.obat_id
                        join detail_penjualan_obat_racikan_luar ON detail_penjualan_obat_racikan_luar.id = obat_racikan_luar.detail_penjualan_obat_racikan_luar_id
                        join penjualan_obat_luar on penjualan_obat_luar.id = detail_penjualan_obat_racikan_luar.penjualan_obat_luar_id
                        where penjualan_obat_luar.dokter_id = u.id
                        and penjualan_obat_luar.tipe = 'resep_luar'
                        and penjualan_obat_luar.progress = 'selesai'
                        and (DATE(penjualan_obat_luar.created_at) between '$dari' and '$sampai')
                    ), 0)) * 0.1 as total_insentif 
                from user as u
                join user_grup ug on ug.user_id = u.id
                join grup g on ug.grup_id = g.id
                where g.nama_grup = 'dokter'
                order by total_insentif desc
            ");
        }
        // Kalo jenisnya dipilih
        else if ($jenis)
        {
            // Rawat jalan
            if ($jenis == "6")
                return $this->db->query("
                select
                    u.id as dokter_id, u.nama, (ifnull((
                        select sum((harga_jual - harga_beli) * detail_obat_pemeriksaan.jumlah_satuan)
                        from detail_obat_pemeriksaan
                        join obat on obat.id = detail_obat_pemeriksaan.obat_id
                        join pemeriksaan on pemeriksaan.id = detail_obat_pemeriksaan.pemeriksaan_id
                        where pemeriksaan.dokter_id = u.id
                        and (DATE(waktu_pemeriksaan) between '$dari' and '$sampai')
                    ), 0) + ifnull((
                        select sum((harga_jual - harga_beli) * obat_racikan.jumlah_satuan)
                        from obat_racikan
                        join obat on obat.id = obat_racikan.obat_id
                        join detail_obat_racikan_pemeriksaan ON detail_obat_racikan_pemeriksaan.id = obat_racikan.detail_obat_racikan_pemeriksaan_id
                        join pemeriksaan on pemeriksaan.id = detail_obat_racikan_pemeriksaan.pemeriksaan_id
                        where pemeriksaan.dokter_id = u.id
                        and (DATE(waktu_pemeriksaan) between '$dari' and '$sampai')
                    ), 0)) * 0.1 as total_insentif 
                from user as u
                join user_grup ug on ug.user_id = u.id
                join grup g on ug.grup_id = g.id
                where g.nama_grup = 'dokter'
                order by total_insentif desc
            ");
            // Selain rawat jalan
            else
                return $this->db->query("
                select
                    u.id as dokter_id, u.nama, (ifnull((
                        select sum((harga_jual - harga_beli) * ri_resep_obat.jumlah)
                        from ri_resep_obat
                        join obat on obat.id = ri_resep_obat.obat_id
                        join ri_resep on ri_resep.id = ri_resep_obat.ri_resep_id
                        join transfer on transfer.id = ri_resep.transfer_id
                        where transfer.dokter_id = u.id
                        and transfer.jenis_ruangan_id = $jenis
                        and (DATE(ri_resep.created_at) between '$dari' and '$sampai')
                    ), 0) + ifnull((
                        select sum((harga_jual - harga_beli) * ri_resep_obat_racik_detail.jumlah)
                        from ri_resep_obat_racik_detail
                        join obat on obat.id = ri_resep_obat_racik_detail.obat_id
                        join ri_resep_obat_racik ON ri_resep_obat_racik.id = ri_resep_obat_racik_detail.ri_resep_obat_racik_id 
                        join ri_resep on ri_resep.id = ri_resep_obat_racik.ri_resep_id
                        join transfer on transfer.id = ri_resep.transfer_id
                        where transfer.dokter_id = u.id
                        and transfer.jenis_ruangan_id = $jenis
                        and (DATE(ri_resep.created_at) between '$dari' and '$sampai')
                    ), 0)) * 0.1 as total_insentif 
                from user as u
                join user_grup ug on ug.user_id = u.id
                join grup g on ug.grup_id = g.id
                where g.nama_grup = 'dokter'
                order by total_insentif desc
            ");
        }
        // Kalau jenisnya gak dipilih (tampil semua)
        else
        {
            return $this->db->query("
                select
                    u.id as dokter_id, u.nama, (ifnull((
                        select sum((harga_jual - harga_beli) * detail_obat_pemeriksaan.jumlah_satuan)
                        from detail_obat_pemeriksaan
                        join obat on obat.id = detail_obat_pemeriksaan.obat_id
                        join pemeriksaan on pemeriksaan.id = detail_obat_pemeriksaan.pemeriksaan_id
                        where pemeriksaan.dokter_id = u.id
                        and (DATE(waktu_pemeriksaan) between '$dari' and '$sampai')
                    ), 0) + ifnull((
                        select sum((harga_jual - harga_beli) * obat_racikan.jumlah_satuan)
                        from obat_racikan
                        join obat on obat.id = obat_racikan.obat_id
                        join detail_obat_racikan_pemeriksaan ON detail_obat_racikan_pemeriksaan.id = obat_racikan.detail_obat_racikan_pemeriksaan_id
                        join pemeriksaan on pemeriksaan.id = detail_obat_racikan_pemeriksaan.pemeriksaan_id
                        where pemeriksaan.dokter_id = u.id
                        and (DATE(waktu_pemeriksaan) between '$dari' and '$sampai')
                    ), 0) + ifnull((
                        select sum((harga_jual - harga_beli) * ri_resep_obat.jumlah)
                        from ri_resep_obat
                        join obat on obat.id = ri_resep_obat.obat_id
                        join ri_resep on ri_resep.id = ri_resep_obat.ri_resep_id
                        join transfer on transfer.id = ri_resep.transfer_id
                        where transfer.dokter_id = u.id
                        and (DATE(ri_resep.created_at) between '$dari' and '$sampai')
                    ), 0) + ifnull((
                        select sum((harga_jual - harga_beli) * ri_resep_obat_racik_detail.jumlah)
                        from ri_resep_obat_racik_detail
                        join obat on obat.id = ri_resep_obat_racik_detail.obat_id
                        join ri_resep_obat_racik ON ri_resep_obat_racik.id = ri_resep_obat_racik_detail.ri_resep_obat_racik_id 
                        join ri_resep on ri_resep.id = ri_resep_obat_racik.ri_resep_id
                        join transfer on transfer.id = ri_resep.transfer_id
                        where transfer.dokter_id = u.id
                        and (DATE(ri_resep.created_at) between '$dari' and '$sampai')
                    ), 0)) * 0.1 as total_insentif 
                from user as u
                join user_grup ug on ug.user_id = u.id
                join grup g on ug.grup_id = g.id
                where g.nama_grup = 'dokter'
                order by total_insentif desc
            ");
        }
    }

    public function detailInsentifObatDokter($id, $jenis, $dari, $sampai, $resep_luar)
    {
        if ($resep_luar)
        {
            return $this->db->query("
                select user.id as dokter_id, user.nama as nama_dokter, nama_pasien,
                       penjualan_obat_luar.created_at as waktu_pemeriksaan, obat.nama as nama_obat, detail_penjualan_obat_luar.jumlah_satuan as jumlah,
                        obat.harga_jual, obat.harga_beli, obat.harga_jual - obat.harga_beli as laba,
                       (obat.harga_jual - obat.harga_beli) * detail_penjualan_obat_luar.jumlah_satuan as akumulasi,
                       (obat.harga_jual - obat.harga_beli) * detail_penjualan_obat_luar.jumlah_satuan * 0.1 as insentif,
                       '' as jaminan, '' as no_rm
                from detail_penjualan_obat_luar
                join obat on obat.id = detail_penjualan_obat_luar.obat_id
                join penjualan_obat_luar on penjualan_obat_luar.id = detail_penjualan_obat_luar.penjualan_obat_luar_id
                join user on user.id = penjualan_obat_luar.dokter_id
                where user.id = $id
                and penjualan_obat_luar.tipe = 'resep_luar'
                and penjualan_obat_luar.progress = 'selesai'
                and (DATE(penjualan_obat_luar.created_at) between '$dari' and '$sampai')

                union

                select user.id as dokter_id, user.nama as nama_dokter, nama_pasien,
                       penjualan_obat_luar.created_at as waktu_pemeriksaan, obat.nama as nama_obat, obat_racikan_luar.jumlah_satuan as jumlah,
                        obat.harga_jual, obat.harga_beli, obat.harga_jual - obat.harga_beli as laba,
                       (obat.harga_jual - obat.harga_beli) * obat_racikan_luar.jumlah_satuan as akumulasi,
                       (obat.harga_jual - obat.harga_beli) * obat_racikan_luar.jumlah_satuan * 0.1 as insentif,
                       '' as jaminan, '' as no_rm
                from obat_racikan_luar
                join obat on obat.id = obat_racikan_luar.obat_id
                join detail_penjualan_obat_racikan_luar on detail_penjualan_obat_racikan_luar.id = obat_racikan_luar.detail_penjualan_obat_racikan_luar_id
                join penjualan_obat_luar on penjualan_obat_luar.id = detail_penjualan_obat_racikan_luar.penjualan_obat_luar_id
                join user on user.id = penjualan_obat_luar.dokter_id
                where user.id = $id
                and penjualan_obat_luar.tipe = 'resep_luar'
                and penjualan_obat_luar.progress = 'selesai'
                and (DATE(penjualan_obat_luar.created_at) between '$dari' and '$sampai')
                
                order by waktu_pemeriksaan desc
            ");
        }
        // Kalo jenisnya dipilih
        else if ($jenis)
        {
            // Rawat jalan
            if ($jenis == "6")
                return $this->db->query("
                    select user.id as dokter_id, user.nama as nama_dokter, pasien.nama as nama_pasien,
                           waktu_pemeriksaan, obat.nama as nama_obat, detail_obat_pemeriksaan.jumlah_satuan as jumlah,
                            obat.harga_jual, obat.harga_beli, obat.harga_jual - obat.harga_beli as laba,
                           (obat.harga_jual - obat.harga_beli) * detail_obat_pemeriksaan.jumlah_satuan as akumulasi,
                           (obat.harga_jual - obat.harga_beli) * detail_obat_pemeriksaan.jumlah_satuan * 0.1 as insentif,
                           pemeriksaan.jaminan, pasien.no_rm
                    from detail_obat_pemeriksaan
                    join obat on obat.id = detail_obat_pemeriksaan.obat_id
                    join pemeriksaan on pemeriksaan.id = detail_obat_pemeriksaan.pemeriksaan_id
                    join pasien on pasien.id = pemeriksaan.pasien_id
                    join user on user.id = pemeriksaan.dokter_id
                    where user.id = $id
                    and (DATE(waktu_pemeriksaan) between '$dari' and '$sampai')

                    union

                    select user.id as dokter_id, user.nama as nama_dokter, pasien.nama as nama_pasien,
                           waktu_pemeriksaan, obat.nama as nama_obat, obat_racikan.jumlah_satuan as jumlah,
                            obat.harga_jual, obat.harga_beli, obat.harga_jual - obat.harga_beli as laba,
                           (obat.harga_jual - obat.harga_beli) * obat_racikan.jumlah_satuan as akumulasi,
                           (obat.harga_jual - obat.harga_beli) * obat_racikan.jumlah_satuan * 0.1 as insentif,
                           pemeriksaan.jaminan, pasien.no_rm
                    from obat_racikan
                    join obat on obat.id = obat_racikan.obat_id
                    join detail_obat_racikan_pemeriksaan on detail_obat_racikan_pemeriksaan.id = obat_racikan.detail_obat_racikan_pemeriksaan_id
                    join pemeriksaan on pemeriksaan.id = detail_obat_racikan_pemeriksaan.pemeriksaan_id
                    join pasien on pasien.id = pemeriksaan.pasien_id
                    join user on user.id = pemeriksaan.dokter_id
                    where user.id = $id
                    and (DATE(waktu_pemeriksaan) between '$dari' and '$sampai')
                    
                    order by waktu_pemeriksaan desc
                ");
            // Selain rawat jalan
            else
                return $this->db->query("
                    select user.id as dokter_id, user.nama as nama_dokter, pasien.nama as nama_pasien,
                           ri_resep.created_at as waktu_pemeriksaan, obat.nama as nama_obat, ri_resep_obat.jumlah,
                            obat.harga_jual, obat.harga_beli, obat.harga_jual - obat.harga_beli as laba,
                           (obat.harga_jual - obat.harga_beli) * ri_resep_obat.jumlah as akumulasi,
                           (obat.harga_jual - obat.harga_beli) * ri_resep_obat.jumlah * 0.1 as insentif,
                           rawat_inap.tipe_pasien as jaminan, pasien.no_rm
                    from ri_resep_obat
                    join obat on obat.id = ri_resep_obat.obat_id
                    join ri_resep on ri_resep.id = ri_resep_obat.ri_resep_id
                    join transfer on transfer.id = ri_resep.transfer_id
                    join rawat_inap on rawat_inap.id = transfer.rawat_inap_id
                    join pasien on pasien.id = transfer.pasien_id
                    join user on user.id = transfer.dokter_id
                    where user.id = $id
                    and transfer.jenis_ruangan_id = $jenis
                    and (DATE(ri_resep.created_at) between '$dari' and '$sampai')
                    
                    union
                    
                    select user.id as dokter_id, user.nama as nama_dokter, pasien.nama as nama_pasien,
                           ri_resep.created_at as waktu_pemeriksaan, obat.nama as nama_obat, ri_resep_obat_racik_detail.jumlah,
                            obat.harga_jual, obat.harga_beli, obat.harga_jual - obat.harga_beli as laba,
                           (obat.harga_jual - obat.harga_beli) * ri_resep_obat_racik_detail.jumlah as akumulasi,
                           (obat.harga_jual - obat.harga_beli) * ri_resep_obat_racik_detail.jumlah * 0.1 as insentif,
                           rawat_inap.tipe_pasien as jaminan, pasien.no_rm
                    from ri_resep_obat_racik_detail
                    join obat on obat.id = ri_resep_obat_racik_detail.obat_id
                    join ri_resep_obat_racik on ri_resep_obat_racik.id = ri_resep_obat_racik_detail.ri_resep_obat_racik_id
                    join ri_resep on ri_resep.id = ri_resep_obat_racik.ri_resep_id
                    join transfer on transfer.id = ri_resep.transfer_id
                    join rawat_inap on rawat_inap.id = transfer.rawat_inap_id
                    join pasien on pasien.id = transfer.pasien_id
                    join user on user.id = transfer.dokter_id
                    where user.id = $id
                    and transfer.jenis_ruangan_id = $jenis
                    and (DATE(ri_resep.created_at) between '$dari' and '$sampai')
                    
                    order by waktu_pemeriksaan desc
                ");
        }
        // Kalau jenisnya gak dipilih (tampil semua)
        else
        {
            return $this->db->query("
                select user.id as dokter_id, user.nama as nama_dokter, pasien.nama as nama_pasien,
                       waktu_pemeriksaan, obat.nama as nama_obat, detail_obat_pemeriksaan.jumlah_satuan as jumlah,
                        obat.harga_jual, obat.harga_beli, obat.harga_jual - obat.harga_beli as laba,
                       (obat.harga_jual - obat.harga_beli) * detail_obat_pemeriksaan.jumlah_satuan as akumulasi,
                       (obat.harga_jual - obat.harga_beli) * detail_obat_pemeriksaan.jumlah_satuan * 0.1 as insentif,
                       pemeriksaan.jaminan, pasien.no_rm
                from detail_obat_pemeriksaan
                join obat on obat.id = detail_obat_pemeriksaan.obat_id
                join pemeriksaan on pemeriksaan.id = detail_obat_pemeriksaan.pemeriksaan_id
                join pasien on pasien.id = pemeriksaan.pasien_id
                join user on user.id = pemeriksaan.dokter_id
                where user.id = $id
                and (DATE(waktu_pemeriksaan) between '$dari' and '$sampai')

                union

                select user.id as dokter_id, user.nama as nama_dokter, pasien.nama as nama_pasien,
                       waktu_pemeriksaan, obat.nama as nama_obat, obat_racikan.jumlah_satuan as jumlah,
                        obat.harga_jual, obat.harga_beli, obat.harga_jual - obat.harga_beli as laba,
                       (obat.harga_jual - obat.harga_beli) * obat_racikan.jumlah_satuan as akumulasi,
                       (obat.harga_jual - obat.harga_beli) * obat_racikan.jumlah_satuan * 0.1 as insentif,
                       pemeriksaan.jaminan, pasien.no_rm
                from obat_racikan
                join obat on obat.id = obat_racikan.obat_id
                join detail_obat_racikan_pemeriksaan on detail_obat_racikan_pemeriksaan.id = obat_racikan.detail_obat_racikan_pemeriksaan_id
                join pemeriksaan on pemeriksaan.id = detail_obat_racikan_pemeriksaan.pemeriksaan_id
                join pasien on pasien.id = pemeriksaan.pasien_id
                join user on user.id = pemeriksaan.dokter_id
                where user.id = $id
                and (DATE(waktu_pemeriksaan) between '$dari' and '$sampai')
    
                union

                select user.id as dokter_id, user.nama as nama_dokter, pasien.nama as nama_pasien,
                       ri_resep.created_at as waktu_pemeriksaan, obat.nama as nama_obat, ri_resep_obat.jumlah,
                        obat.harga_jual, obat.harga_beli, obat.harga_jual - obat.harga_beli as laba,
                       (obat.harga_jual - obat.harga_beli) * ri_resep_obat.jumlah as akumulasi,
                       (obat.harga_jual - obat.harga_beli) * ri_resep_obat.jumlah * 0.1 as insentif,
                       rawat_inap.tipe_pasien as jaminan, pasien.no_rm
                from ri_resep_obat
                join obat on obat.id = ri_resep_obat.obat_id
                join ri_resep on ri_resep.id = ri_resep_obat.ri_resep_id
                join transfer on transfer.id = ri_resep.transfer_id
                join rawat_inap on rawat_inap.id = transfer.rawat_inap_id
                join pasien on pasien.id = transfer.pasien_id
                join user on user.id = transfer.dokter_id
                where user.id = $id
                and (DATE(ri_resep.created_at) between '$dari' and '$sampai')
                
                union
                
                select user.id as dokter_id, user.nama as nama_dokter, pasien.nama as nama_pasien,
                       ri_resep.created_at as waktu_pemeriksaan, obat.nama as nama_obat, ri_resep_obat_racik_detail.jumlah,
                        obat.harga_jual, obat.harga_beli, obat.harga_jual - obat.harga_beli as laba,
                       (obat.harga_jual - obat.harga_beli) * ri_resep_obat_racik_detail.jumlah as akumulasi,
                       (obat.harga_jual - obat.harga_beli) * ri_resep_obat_racik_detail.jumlah * 0.1 as insentif,
                       rawat_inap.tipe_pasien as jaminan, pasien.no_rm
                from ri_resep_obat_racik_detail
                join obat on obat.id = ri_resep_obat_racik_detail.obat_id
                join ri_resep_obat_racik on ri_resep_obat_racik.id = ri_resep_obat_racik_detail.ri_resep_obat_racik_id
                join ri_resep on ri_resep.id = ri_resep_obat_racik.ri_resep_id
                join transfer on transfer.id = ri_resep.transfer_id
                join rawat_inap on rawat_inap.id = transfer.rawat_inap_id
                join pasien on pasien.id = transfer.pasien_id
                join user on user.id = transfer.dokter_id
                where user.id = $id
                and (DATE(ri_resep.created_at) between '$dari' and '$sampai')
                
                order by waktu_pemeriksaan desc
            ");
        }
    }

    public function detailInsentifObatApoteker($jenis, $dari, $sampai, $resep_luar)
    {
        if ($resep_luar)
        {
            return $this->db->query("
                select nama_pasien,
                       penjualan_obat_luar.created_at as waktu_pemeriksaan, obat.nama as nama_obat, detail_penjualan_obat_luar.jumlah_satuan as jumlah,
                        obat.harga_jual, obat.harga_beli, obat.harga_jual - obat.harga_beli as laba,
                       (obat.harga_jual - obat.harga_beli) * detail_penjualan_obat_luar.jumlah_satuan as akumulasi,
                       (obat.harga_jual - obat.harga_beli) * detail_penjualan_obat_luar.jumlah_satuan * 0.075 as insentif,
                       '' as jaminan, '' as no_rm
                from detail_penjualan_obat_luar
                join obat on obat.id = detail_penjualan_obat_luar.obat_id
                join penjualan_obat_luar on penjualan_obat_luar.id = detail_penjualan_obat_luar.penjualan_obat_luar_id
                where (DATE(penjualan_obat_luar.created_at) between '$dari' and '$sampai')
                and penjualan_obat_luar.tipe = 'resep_luar'
                and penjualan_obat_luar.progress = 'selesai'

                union

                select nama_pasien,
                       penjualan_obat_luar.created_at as waktu_pemeriksaan, obat.nama as nama_obat, obat_racikan_luar.jumlah_satuan as jumlah,
                        obat.harga_jual, obat.harga_beli, obat.harga_jual - obat.harga_beli as laba,
                       (obat.harga_jual - obat.harga_beli) * obat_racikan_luar.jumlah_satuan as akumulasi,
                       (obat.harga_jual - obat.harga_beli) * obat_racikan_luar.jumlah_satuan * 0.075 as insentif,
                       '' as jaminan, '' as no_rm
                from obat_racikan_luar
                join obat on obat.id = obat_racikan_luar.obat_id
                join detail_penjualan_obat_racikan_luar on detail_penjualan_obat_racikan_luar.id = obat_racikan_luar.detail_penjualan_obat_racikan_luar_id
                join penjualan_obat_luar on penjualan_obat_luar.id = detail_penjualan_obat_racikan_luar.penjualan_obat_luar_id
                where (DATE(penjualan_obat_luar.created_at) between '$dari' and '$sampai')
                and penjualan_obat_luar.tipe = 'resep_luar'
                and penjualan_obat_luar.progress = 'selesai'
                
                order by waktu_pemeriksaan desc
            ");
        }
        // Kalo jenisnya dipilih
        else if ($jenis)
        {
            // Rawat jalan
            if ($jenis == "6")
                return $this->db->query("
                    select pasien.nama as nama_pasien,
                           waktu_pemeriksaan, obat.nama as nama_obat, detail_obat_pemeriksaan.jumlah_satuan as jumlah,
                            obat.harga_jual, obat.harga_beli, obat.harga_jual - obat.harga_beli as laba,
                           (obat.harga_jual - obat.harga_beli) * detail_obat_pemeriksaan.jumlah_satuan as akumulasi,
                           (obat.harga_jual - obat.harga_beli) * detail_obat_pemeriksaan.jumlah_satuan * 0.075 as insentif,
                           pemeriksaan.jaminan, pasien.no_rm
                    from detail_obat_pemeriksaan
                    join obat on obat.id = detail_obat_pemeriksaan.obat_id
                    join pemeriksaan on pemeriksaan.id = detail_obat_pemeriksaan.pemeriksaan_id
                    join pasien on pasien.id = pemeriksaan.pasien_id
                    where (DATE(waktu_pemeriksaan) between '$dari' and '$sampai')

                    union

                    select pasien.nama as nama_pasien,
                           waktu_pemeriksaan, obat.nama as nama_obat, obat_racikan.jumlah_satuan as jumlah,
                            obat.harga_jual, obat.harga_beli, obat.harga_jual - obat.harga_beli as laba,
                           (obat.harga_jual - obat.harga_beli) * obat_racikan.jumlah_satuan as akumulasi,
                           (obat.harga_jual - obat.harga_beli) * obat_racikan.jumlah_satuan * 0.075 as insentif,
                           pemeriksaan.jaminan, pasien.no_rm
                    from obat_racikan
                    join obat on obat.id = obat_racikan.obat_id
                    join detail_obat_racikan_pemeriksaan on detail_obat_racikan_pemeriksaan.id = obat_racikan.detail_obat_racikan_pemeriksaan_id
                    join pemeriksaan on pemeriksaan.id = detail_obat_racikan_pemeriksaan.pemeriksaan_id
                    join pasien on pasien.id = pemeriksaan.pasien_id
                    where (DATE(waktu_pemeriksaan) between '$dari' and '$sampai')
                    
                    order by waktu_pemeriksaan desc
                ");
            // Selain rawat jalan
            else
                return $this->db->query("
                    select pasien.nama as nama_pasien,
                           ri_resep.created_at as waktu_pemeriksaan, obat.nama as nama_obat, ri_resep_obat.jumlah,
                            obat.harga_jual, obat.harga_beli, obat.harga_jual - obat.harga_beli as laba,
                           (obat.harga_jual - obat.harga_beli) * ri_resep_obat.jumlah as akumulasi,
                           (obat.harga_jual - obat.harga_beli) * ri_resep_obat.jumlah * 0.075 as insentif,
                           rawat_inap.tipe_pasien as jaminan, pasien.no_rm
                    from ri_resep_obat
                    join obat on obat.id = ri_resep_obat.obat_id
                    join ri_resep on ri_resep.id = ri_resep_obat.ri_resep_id
                    join transfer on transfer.id = ri_resep.transfer_id
                    join rawat_inap on rawat_inap.id = transfer.rawat_inap_id
                    join pasien on pasien.id = transfer.pasien_id
                    where transfer.jenis_ruangan_id = $jenis
                    and (DATE(ri_resep.created_at) between '$dari' and '$sampai')
                    
                    union
                    
                    select pasien.nama as nama_pasien,
                           ri_resep.created_at as waktu_pemeriksaan, obat.nama as nama_obat, ri_resep_obat_racik_detail.jumlah,
                            obat.harga_jual, obat.harga_beli, obat.harga_jual - obat.harga_beli as laba,
                           (obat.harga_jual - obat.harga_beli) * ri_resep_obat_racik_detail.jumlah as akumulasi,
                           (obat.harga_jual - obat.harga_beli) * ri_resep_obat_racik_detail.jumlah * 0.075 as insentif,
                           rawat_inap.tipe_pasien as jaminan, pasien.no_rm
                    from ri_resep_obat_racik_detail
                    join obat on obat.id = ri_resep_obat_racik_detail.obat_id
                    join ri_resep_obat_racik on ri_resep_obat_racik.id = ri_resep_obat_racik_detail.ri_resep_obat_racik_id
                    join ri_resep on ri_resep.id = ri_resep_obat_racik.ri_resep_id
                    join transfer on transfer.id = ri_resep.transfer_id
                    join rawat_inap on rawat_inap.id = transfer.rawat_inap_id
                    join pasien on pasien.id = transfer.pasien_id
                    where transfer.jenis_ruangan_id = $jenis
                    and (DATE(ri_resep.created_at) between '$dari' and '$sampai')
                    
                    order by waktu_pemeriksaan desc
                ");
        }
        // Kalau jenisnya gak dipilih (tampil semua)
        else
        {
            return $this->db->query("
                select pasien.nama as nama_pasien,
                       waktu_pemeriksaan, obat.nama as nama_obat, detail_obat_pemeriksaan.jumlah_satuan as jumlah,
                        obat.harga_jual, obat.harga_beli, obat.harga_jual - obat.harga_beli as laba,
                       (obat.harga_jual - obat.harga_beli) * detail_obat_pemeriksaan.jumlah_satuan as akumulasi,
                       (obat.harga_jual - obat.harga_beli) * detail_obat_pemeriksaan.jumlah_satuan * 0.075 as insentif,
                       pemeriksaan.jaminan, pasien.no_rm
                from detail_obat_pemeriksaan
                join obat on obat.id = detail_obat_pemeriksaan.obat_id
                join pemeriksaan on pemeriksaan.id = detail_obat_pemeriksaan.pemeriksaan_id
                join pasien on pasien.id = pemeriksaan.pasien_id
                where (DATE(waktu_pemeriksaan) between '$dari' and '$sampai')

                union

                select pasien.nama as nama_pasien,
                       waktu_pemeriksaan, obat.nama as nama_obat, obat_racikan.jumlah_satuan as jumlah,
                        obat.harga_jual, obat.harga_beli, obat.harga_jual - obat.harga_beli as laba,
                       (obat.harga_jual - obat.harga_beli) * obat_racikan.jumlah_satuan as akumulasi,
                       (obat.harga_jual - obat.harga_beli) * obat_racikan.jumlah_satuan * 0.075 as insentif,
                       pemeriksaan.jaminan, pasien.no_rm
                from obat_racikan
                join obat on obat.id = obat_racikan.obat_id
                join detail_obat_racikan_pemeriksaan on detail_obat_racikan_pemeriksaan.id = obat_racikan.detail_obat_racikan_pemeriksaan_id
                join pemeriksaan on pemeriksaan.id = detail_obat_racikan_pemeriksaan.pemeriksaan_id
                join pasien on pasien.id = pemeriksaan.pasien_id
                where (DATE(waktu_pemeriksaan) between '$dari' and '$sampai')
    
                union

                select pasien.nama as nama_pasien,
                       ri_resep.created_at as waktu_pemeriksaan, obat.nama as nama_obat, ri_resep_obat.jumlah,
                        obat.harga_jual, obat.harga_beli, obat.harga_jual - obat.harga_beli as laba,
                       (obat.harga_jual - obat.harga_beli) * ri_resep_obat.jumlah as akumulasi,
                       (obat.harga_jual - obat.harga_beli) * ri_resep_obat.jumlah * 0.075 as insentif,
                       rawat_inap.tipe_pasien as jaminan, pasien.no_rm
                from ri_resep_obat
                join obat on obat.id = ri_resep_obat.obat_id
                join ri_resep on ri_resep.id = ri_resep_obat.ri_resep_id
                join transfer on transfer.id = ri_resep.transfer_id
                join rawat_inap on rawat_inap.id = transfer.rawat_inap_id
                join pasien on pasien.id = transfer.pasien_id
                and (DATE(ri_resep.created_at) between '$dari' and '$sampai')
                
                union
                
                select pasien.nama as nama_pasien,
                       ri_resep.created_at as waktu_pemeriksaan, obat.nama as nama_obat, ri_resep_obat_racik_detail.jumlah,
                        obat.harga_jual, obat.harga_beli, obat.harga_jual - obat.harga_beli as laba,
                       (obat.harga_jual - obat.harga_beli) * ri_resep_obat_racik_detail.jumlah as akumulasi,
                       (obat.harga_jual - obat.harga_beli) * ri_resep_obat_racik_detail.jumlah * 0.075 as insentif,
                       rawat_inap.tipe_pasien as jaminan, pasien.no_rm
                from ri_resep_obat_racik_detail
                join obat on obat.id = ri_resep_obat_racik_detail.obat_id
                join ri_resep_obat_racik on ri_resep_obat_racik.id = ri_resep_obat_racik_detail.ri_resep_obat_racik_id
                join ri_resep on ri_resep.id = ri_resep_obat_racik.ri_resep_id
                join transfer on transfer.id = ri_resep.transfer_id
                join rawat_inap on rawat_inap.id = transfer.rawat_inap_id
                join pasien on pasien.id = transfer.pasien_id
                and (DATE(ri_resep.created_at) between '$dari' and '$sampai')
                
                order by waktu_pemeriksaan desc
            ");
        }
    }

    public function detailInsentifObatKlinik($jenis, $dari, $sampai, $resep_luar)
    {
        if ($resep_luar)
        {
            return $this->db->query("
                select nama_pasien,
                       penjualan_obat_luar.created_at as waktu_pemeriksaan, obat.nama as nama_obat, detail_penjualan_obat_luar.jumlah_satuan as jumlah,
                        obat.harga_jual, obat.harga_beli, obat.harga_jual - obat.harga_beli as laba,
                       (obat.harga_jual - obat.harga_beli) * detail_penjualan_obat_luar.jumlah_satuan as akumulasi,
                       (obat.harga_jual - obat.harga_beli) * detail_penjualan_obat_luar.jumlah_satuan * 0.825 as insentif,
                       '' as jaminan, '' as no_rm
                from detail_penjualan_obat_luar
                join obat on obat.id = detail_penjualan_obat_luar.obat_id
                join penjualan_obat_luar on penjualan_obat_luar.id = detail_penjualan_obat_luar.penjualan_obat_luar_id
                join user on user.id = penjualan_obat_luar.dokter_id
                where (DATE(penjualan_obat_luar.created_at) between '$dari' and '$sampai')
                and penjualan_obat_luar.tipe = 'resep_luar'
                and penjualan_obat_luar.progress = 'selesai'

                union

                select nama_pasien,
                       penjualan_obat_luar.created_at as waktu_pemeriksaan, obat.nama as nama_obat, obat_racikan_luar.jumlah_satuan as jumlah,
                        obat.harga_jual, obat.harga_beli, obat.harga_jual - obat.harga_beli as laba,
                       (obat.harga_jual - obat.harga_beli) * obat_racikan_luar.jumlah_satuan as akumulasi,
                       (obat.harga_jual - obat.harga_beli) * obat_racikan_luar.jumlah_satuan * 0.825 as insentif,
                       '' as jaminan, '' as no_rm
                from obat_racikan_luar
                join obat on obat.id = obat_racikan_luar.obat_id
                join detail_penjualan_obat_racikan_luar on detail_penjualan_obat_racikan_luar.id = obat_racikan_luar.detail_penjualan_obat_racikan_luar_id
                join penjualan_obat_luar on penjualan_obat_luar.id = detail_penjualan_obat_racikan_luar.penjualan_obat_luar_id
                join user on user.id = penjualan_obat_luar.dokter_id
                where (DATE(penjualan_obat_luar.created_at) between '$dari' and '$sampai')
                and penjualan_obat_luar.tipe = 'resep_luar'
                and penjualan_obat_luar.progress = 'selesai'
                
                order by waktu_pemeriksaan desc
            ");
        }
        // Kalo jenisnya dipilih
        else if ($jenis)
        {
            // Rawat jalan
            if ($jenis == "6")
                return $this->db->query("
                    select pasien.nama as nama_pasien,
                           waktu_pemeriksaan, obat.nama as nama_obat, detail_obat_pemeriksaan.jumlah_satuan as jumlah,
                            obat.harga_jual, obat.harga_beli, obat.harga_jual - obat.harga_beli as laba,
                           (obat.harga_jual - obat.harga_beli) * detail_obat_pemeriksaan.jumlah_satuan as akumulasi,
                           (obat.harga_jual - obat.harga_beli) * detail_obat_pemeriksaan.jumlah_satuan * 0.825 as insentif,
                           pemeriksaan.jaminan, pasien.no_rm
                    from detail_obat_pemeriksaan
                    join obat on obat.id = detail_obat_pemeriksaan.obat_id
                    join pemeriksaan on pemeriksaan.id = detail_obat_pemeriksaan.pemeriksaan_id
                    join pasien on pasien.id = pemeriksaan.pasien_id
                    where (DATE(waktu_pemeriksaan) between '$dari' and '$sampai')

                    union

                    select pasien.nama as nama_pasien,
                           waktu_pemeriksaan, obat.nama as nama_obat, obat_racikan.jumlah_satuan as jumlah,
                            obat.harga_jual, obat.harga_beli, obat.harga_jual - obat.harga_beli as laba,
                           (obat.harga_jual - obat.harga_beli) * obat_racikan.jumlah_satuan as akumulasi,
                           (obat.harga_jual - obat.harga_beli) * obat_racikan.jumlah_satuan * 0.825 as insentif,
                           pemeriksaan.jaminan, pasien.no_rm
                    from obat_racikan
                    join obat on obat.id = obat_racikan.obat_id
                    join detail_obat_racikan_pemeriksaan on detail_obat_racikan_pemeriksaan.id = obat_racikan.detail_obat_racikan_pemeriksaan_id
                    join pemeriksaan on pemeriksaan.id = detail_obat_racikan_pemeriksaan.pemeriksaan_id
                    join pasien on pasien.id = pemeriksaan.pasien_id
                    where (DATE(waktu_pemeriksaan) between '$dari' and '$sampai')
                    
                    order by waktu_pemeriksaan desc
                ");
            // Selain rawat jalan
            else
                return $this->db->query("
                    select pasien.nama as nama_pasien,
                           ri_resep.created_at as waktu_pemeriksaan, obat.nama as nama_obat, ri_resep_obat.jumlah,
                            obat.harga_jual, obat.harga_beli, obat.harga_jual - obat.harga_beli as laba,
                           (obat.harga_jual - obat.harga_beli) * ri_resep_obat.jumlah as akumulasi,
                           (obat.harga_jual - obat.harga_beli) * ri_resep_obat.jumlah * 0.825 as insentif,
                            rawat_inap.tipe_pasien as jaminan, pasien.no_rm
                    from ri_resep_obat
                    join obat on obat.id = ri_resep_obat.obat_id
                    join ri_resep on ri_resep.id = ri_resep_obat.ri_resep_id
                    join transfer on transfer.id = ri_resep.transfer_id
                    join rawat_inap on rawat_inap.id = transfer.rawat_inap_id
                    join pasien on pasien.id = transfer.pasien_id
                    where transfer.jenis_ruangan_id = $jenis
                    and (DATE(ri_resep.created_at) between '$dari' and '$sampai')
                    
                    union
                    
                    select pasien.nama as nama_pasien,
                           ri_resep.created_at as waktu_pemeriksaan, obat.nama as nama_obat, ri_resep_obat_racik_detail.jumlah,
                            obat.harga_jual, obat.harga_beli, obat.harga_jual - obat.harga_beli as laba,
                           (obat.harga_jual - obat.harga_beli) * ri_resep_obat_racik_detail.jumlah as akumulasi,
                           (obat.harga_jual - obat.harga_beli) * ri_resep_obat_racik_detail.jumlah * 0.825 as insentif,
                            rawat_inap.tipe_pasien as jaminan, pasien.no_rm
                    from ri_resep_obat_racik_detail
                    join obat on obat.id = ri_resep_obat_racik_detail.obat_id
                    join ri_resep_obat_racik on ri_resep_obat_racik.id = ri_resep_obat_racik_detail.ri_resep_obat_racik_id
                    join ri_resep on ri_resep.id = ri_resep_obat_racik.ri_resep_id
                    join transfer on transfer.id = ri_resep.transfer_id
                    join rawat_inap on rawat_inap.id = transfer.rawat_inap_id
                    join pasien on pasien.id = transfer.pasien_id
                    where transfer.jenis_ruangan_id = $jenis
                    and (DATE(ri_resep.created_at) between '$dari' and '$sampai')
                    
                    order by waktu_pemeriksaan desc
                ");
        }
        // Kalau jenisnya gak dipilih (tampil semua)
        else
        {
            return $this->db->query("
                select pasien.nama as nama_pasien,
                       waktu_pemeriksaan, obat.nama as nama_obat, detail_obat_pemeriksaan.jumlah_satuan as jumlah,
                        obat.harga_jual, obat.harga_beli, obat.harga_jual - obat.harga_beli as laba,
                       (obat.harga_jual - obat.harga_beli) * detail_obat_pemeriksaan.jumlah_satuan as akumulasi,
                       (obat.harga_jual - obat.harga_beli) * detail_obat_pemeriksaan.jumlah_satuan * 0.825 as insentif,
                       pemeriksaan.jaminan, pasien.no_rm
                from detail_obat_pemeriksaan
                join obat on obat.id = detail_obat_pemeriksaan.obat_id
                join pemeriksaan on pemeriksaan.id = detail_obat_pemeriksaan.pemeriksaan_id
                join pasien on pasien.id = pemeriksaan.pasien_id
                where (DATE(waktu_pemeriksaan) between '$dari' and '$sampai')

                union

                select pasien.nama as nama_pasien,
                       waktu_pemeriksaan, obat.nama as nama_obat, obat_racikan.jumlah_satuan as jumlah,
                        obat.harga_jual, obat.harga_beli, obat.harga_jual - obat.harga_beli as laba,
                       (obat.harga_jual - obat.harga_beli) * obat_racikan.jumlah_satuan as akumulasi,
                       (obat.harga_jual - obat.harga_beli) * obat_racikan.jumlah_satuan * 0.825 as insentif,
                       pemeriksaan.jaminan, pasien.no_rm
                from obat_racikan
                join obat on obat.id = obat_racikan.obat_id
                join detail_obat_racikan_pemeriksaan on detail_obat_racikan_pemeriksaan.id = obat_racikan.detail_obat_racikan_pemeriksaan_id
                join pemeriksaan on pemeriksaan.id = detail_obat_racikan_pemeriksaan.pemeriksaan_id
                join pasien on pasien.id = pemeriksaan.pasien_id
                where (DATE(waktu_pemeriksaan) between '$dari' and '$sampai')
    
                union

                select pasien.nama as nama_pasien,
                       ri_resep.created_at as waktu_pemeriksaan, obat.nama as nama_obat, ri_resep_obat.jumlah,
                        obat.harga_jual, obat.harga_beli, obat.harga_jual - obat.harga_beli as laba,
                       (obat.harga_jual - obat.harga_beli) * ri_resep_obat.jumlah as akumulasi,
                       (obat.harga_jual - obat.harga_beli) * ri_resep_obat.jumlah * 0.825 as insentif,
                       rawat_inap.tipe_pasien as jaminan, pasien.no_rm
                from ri_resep_obat
                join obat on obat.id = ri_resep_obat.obat_id
                join ri_resep on ri_resep.id = ri_resep_obat.ri_resep_id
                join transfer on transfer.id = ri_resep.transfer_id
                join rawat_inap on rawat_inap.id = transfer.rawat_inap_id
                join pasien on pasien.id = transfer.pasien_id
                and (DATE(ri_resep.created_at) between '$dari' and '$sampai')
                
                union
                
                select pasien.nama as nama_pasien,
                       ri_resep.created_at as waktu_pemeriksaan, obat.nama as nama_obat, ri_resep_obat_racik_detail.jumlah,
                        obat.harga_jual, obat.harga_beli, obat.harga_jual - obat.harga_beli as laba,
                       (obat.harga_jual - obat.harga_beli) * ri_resep_obat_racik_detail.jumlah as akumulasi,
                       (obat.harga_jual - obat.harga_beli) * ri_resep_obat_racik_detail.jumlah * 0.825 as insentif,
                       rawat_inap.tipe_pasien as jaminan, pasien.no_rm
                from ri_resep_obat_racik_detail
                join obat on obat.id = ri_resep_obat_racik_detail.obat_id
                join ri_resep_obat_racik on ri_resep_obat_racik.id = ri_resep_obat_racik_detail.ri_resep_obat_racik_id
                join ri_resep on ri_resep.id = ri_resep_obat_racik.ri_resep_id
                join transfer on transfer.id = ri_resep.transfer_id
                join rawat_inap on rawat_inap.id = transfer.rawat_inap_id
                join pasien on pasien.id = transfer.pasien_id
                and (DATE(ri_resep.created_at) between '$dari' and '$sampai')
                
                order by waktu_pemeriksaan desc
            ");
        }
    }

    public function detailInsentifObatBebasApoteker($dari, $sampai)
    {
        return $this->db->query("
            select penjualan_obat_luar.created_at as waktu_pemeriksaan, obat.nama as nama_obat, detail_penjualan_obat_luar.jumlah_satuan as jumlah,
                    obat.harga_jual, obat.harga_beli, obat.harga_jual - obat.harga_beli as laba,
                   (obat.harga_jual - obat.harga_beli) * detail_penjualan_obat_luar.jumlah_satuan as akumulasi,
                   (obat.harga_jual - obat.harga_beli) * detail_penjualan_obat_luar.jumlah_satuan * 0.075 as insentif
            from detail_penjualan_obat_luar
            join obat on obat.id = detail_penjualan_obat_luar.obat_id
            join penjualan_obat_luar on penjualan_obat_luar.id = detail_penjualan_obat_luar.penjualan_obat_luar_id
            where (DATE(penjualan_obat_luar.created_at) between '$dari' and '$sampai')
            and penjualan_obat_luar.tipe = 'obat_bebas'
            and penjualan_obat_luar.progress = 'selesai'

            union

            select penjualan_obat_luar.created_at as waktu_pemeriksaan, obat.nama as nama_obat, obat_racikan_luar.jumlah_satuan as jumlah,
                    obat.harga_jual, obat.harga_beli, obat.harga_jual - obat.harga_beli as laba,
                   (obat.harga_jual - obat.harga_beli) * obat_racikan_luar.jumlah_satuan as akumulasi,
                   (obat.harga_jual - obat.harga_beli) * obat_racikan_luar.jumlah_satuan * 0.075 as insentif
            from obat_racikan_luar
            join obat on obat.id = obat_racikan_luar.obat_id
            join detail_penjualan_obat_racikan_luar on detail_penjualan_obat_racikan_luar.id = obat_racikan_luar.detail_penjualan_obat_racikan_luar_id
            join penjualan_obat_luar on penjualan_obat_luar.id = detail_penjualan_obat_racikan_luar.penjualan_obat_luar_id
            where (DATE(penjualan_obat_luar.created_at) between '$dari' and '$sampai')
            and penjualan_obat_luar.tipe = 'obat_bebas'
            and penjualan_obat_luar.progress = 'selesai'
            
            order by waktu_pemeriksaan desc
        ");
    }
}