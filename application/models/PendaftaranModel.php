<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PendaftaranModel extends CI_Model {


    public function getDokter() {
        $this->db->select('u.*, ug.grup_id, g.nama_grup');
        $this->db->from('user u');
        $this->db->join('user_grup ug', 'ug.user_id = u.id', 'left');
        $this->db->join('grup g', 'g.id = ug.grup_id', 'left');
        $this->db->where('g.nama_grup', 'dokter');
        $this->db->where('u.is_active', '1');

        return $this->db->get();
    }

    public function getPasienById($id) {
        $this->db->where('id', $id);
        $this->db->where('is_active', '1');

        return $this->db->get('pasien');
    }

    public function getPemeriksaanPasienById($id) {
        $this->db->select('pem.*, p.no_rm,p.nama nama_p, u.nama nama_d, jp.nama as nama_poli');
        $this->db->join('pasien p', 'p.id = pem.pasien_id', 'left');
        $this->db->join('user u', 'u.id = pem.dokter_id', 'left');
        $this->db->join('pendaftaran_pasien pen', 'pen.id = pem.pendaftaran_id', 'left');
        $this->db->join('jenis_pendaftaran jp', 'jp.id = pen.jenis_pendaftaran_id', 'left');
        $this->db->where('pem.pasien_id', $id);
        $this->db->where('pem.is_active', '1');
        $this->db->order_by('pem.created_at');

        return $this->db->get('pemeriksaan pem');
    }

    public function getPemeriksaanByPendaftaranIdAndJenisPendaftaranId($pendaftaran_id, $jp_id, $sort = 'asc') {
        $this->db->select('pem.*, p.no_rm,p.nama nama_p, u.nama nama_d, jp.nama as nama_poli, pen.jenis_pendaftaran_id');
        $this->db->join('pasien p', 'p.id = pem.pasien_id', 'left');
        $this->db->join('user u', 'u.id = pem.dokter_id', 'left');
        $this->db->join('pendaftaran_pasien pen', 'pen.id = pem.pendaftaran_id', 'left');
        $this->db->join('jenis_pendaftaran jp', 'jp.id = pen.jenis_pendaftaran_id', 'left');
        $this->db->where('(pen.pendaftaran_id =', $pendaftaran_id, FALSE);
        $this->db->or_where("pen.id = '{$pendaftaran_id}')", NULL, FALSE);
        $this->db->where('pen.jenis_pendaftaran_id', $jp_id);
        $this->db->where('pem.is_active', '1');
        $this->db->order_by('pem.id', $sort);
        $this->db->limit(10);

        return $this->db->get('pemeriksaan pem')->result();
    }

    public function getPendaftaranByIdPasien($id) {
        return $this->db
            ->select('pem.*')
            ->where('pem.pasien', $id)
            ->get('pendaftaran_pasien pem');
    }

    public function cek_id() {
        $this->db->select_max('no_rm');
        $this->db->from('pasien');
        return $this->db->get();
    }

    public function getJenisPendaftaran() {
        $this->db->where('is_active', '1');
        $this->db->where('status', '1');
        return $this->db->get('jenis_pendaftaran');
    }

    public function getJenisPendaftaranById($id) {
        if ($id < 5) {
            return $this->db
                ->where('id', $id)
                ->get('jenis_ruangan');
        }
        $this->db->where('id', $id);
        $this->db->where('is_active', '1');
        $this->db->where('status', '1');
        return $this->db->get('jenis_pendaftaran');
    }

    public function getListPendaftaran() {
        $this->db->select('pp.*, p.nama nama_pasien, p.jk, p.usia, u.nama nama_dokter, jp.nama jenis_pendaftaran');
        $this->db->from('pendaftaran_pasien pp');
        $this->db->join('pasien p', 'p.id = pp.pasien and p.is_active = 1');
        $this->db->join('user u', 'u.id = pp.dokter and u.is_active = 1');
        $this->db->join('jenis_pendaftaran jp', 'jp.id = pp.jenis_pendaftaran_id and jp.is_active = 1');
        $this->db->where('pp.is_active', '1');
        return $this->db->get();
    }

    public function getListPasienAntri() {
        $this->db->select('pp.*, p.nama nama_pasien,p.nik, p.id as pasien_id, p.jk, p.usia, u.nama nama_dokter, jp.nama jenis_pendaftaran, a.kode_antrian, a.is_mobile_jkn, a.is_check_in');
        $this->db->select('max(log.success) as success, log.request, log.response, a.kode_booking');
        $this->db->from('pendaftaran_pasien pp');
        $this->db->join('pasien p', 'p.id = pp.pasien and p.is_active = 1');
        $this->db->join('user u', 'u.id = pp.dokter and u.is_active = 1');
        $this->db->join('jenis_pendaftaran jp', 'jp.id = pp.jenis_pendaftaran_id and jp.is_active = 1');
        $this->db->join('antrian a', 'a.pendaftaran_id = pp.id');
        $this->db->join('bpjs_jknmobile_log log', "log.kode_booking = a.kode_booking and url = 'antrean/add'", 'left');
        $this->db->where('date(a.due_date) <=', date('Y-m-d 23:59:59', strtotime("+7 day")));
        $this->db->where('pp.is_active', '1');
        $this->db->where('pp.status', 'antri');
        $this->db->order_by('pp.waktu_pendaftaran', 'asc');
        $this->db->group_by('a.kode_booking');

        return $this->db->get();
    }

    public function getListPasienKontrolAntri() {
        $this->db->select('pp.*,p.nik, p.nama nama_pasien, p.id as pasien_id, p.jk, p.usia, u.nama nama_dokter, jp.nama jenis_pendaftaran');
        $this->db->select('a.kode_antrian, a.is_mobile_jkn, a.is_check_in, max(log.success) as success, log.request, log.response, a.kode_booking');
        $this->db->from('pendaftaran_pasien pp');
        $this->db->join('pasien p', 'p.id = pp.pasien and p.is_active = 1');
        $this->db->join('user u', 'u.id = pp.dokter and u.is_active = 1');
        $this->db->join('jenis_pendaftaran jp', 'jp.id = pp.jenis_pendaftaran_id and jp.is_active = 1');
        $this->db->join('antrian a', 'a.pendaftaran_id = pp.id');
        $this->db->join('bpjs_jknmobile_log log', "log.kode_booking = a.kode_booking and url = 'antrean/add'", 'left');
        $this->db->where('date(a.due_date) <=', date('Y-m-d 23:59:59', strtotime("+7 day")));
        $this->db->where('pp.is_active', '1');
        $this->db->where('pp.status', 'antri');
        $this->db->where('pp.penanggungjawab', 'Rencana Kontrol');
        $this->db->order_by('pp.waktu_pendaftaran', 'asc');
        $this->db->group_by('a.kode_booking');

        return $this->db->get();
    }

    public function cari($id = '') {
        $this->db->where('no_rm', $id);
        $this->db->where('is_active', '1');
        return $this->db->get('pasien');
    }

    public function getDetailPasienBaruHariIni() {
        $this->db->select('jp.nama AS nama_jenis_pendaftaran, COUNT(pp.jenis_pendaftaran_id) AS jumlah_pasien, jp.id as id_poli');
        $this->db->from('pendaftaran_pasien pp');
        $this->db->join('pemeriksaan p', 'p.pendaftaran_id = pp.id');
        $this->db->join('jenis_pendaftaran jp', 'jp.id = pp.jenis_pendaftaran_id');
        $this->db->join('pasien pas', 'pas.id = pp.pasien');
        $this->db->where('p.status', 'selesai');
        $this->db->where('DATE(pp.created_at) = CURDATE()', null, false);
        $this->db->where('DATE(pas.created_at) = CURDATE()', null, false);
        // $this->db->where('DATE(pp.created_at) = SUBDATE(NOW(), 1)', null, false );
        $this->db->group_by('pp.jenis_pendaftaran_id');

        return $this->db->get();
    }

    public function getListDetailPasienBaruHariIni($jenis_pendaftaran) {
        $this->db->select('pp.*, p.id pasien_id, p.nama nama_pasien, p.no_rm, p.jk, p.usia, u.nama nama_dokter, jp.id as jenis_pendaftaran_id, jp.nama jenis_pendaftaran' );
        $this->db->from('pemeriksaan pem');
        $this->db->join('pendaftaran_pasien pp', 'pem.pendaftaran_id = pp.id');
        $this->db->join('pasien p', 'p.id = pp.pasien and p.is_active = 1');
        $this->db->join('user u', 'u.id = pp.dokter and u.is_active = 1');
        $this->db->join('jenis_pendaftaran jp', 'jp.id = pp.jenis_pendaftaran_id and jp.is_active = 1');
        $this->db->where('pem.status', 'selesai');
        $this->db->where('DATE(pp.created_at) = CURDATE()', null, false);
        if ($jenis_pendaftaran) $this->db->where('pp.jenis_pendaftaran_id', $jenis_pendaftaran );
        return  $this->db->get();
    }

    function cari_kode($keyword) {
        $sql = "
            SELECT 
                *
            FROM 
                pasien
            WHERE
                ( 
                    no_rm LIKE '%" . $this->db->escape_like_str($keyword) . "%' 
                    OR nama LIKE '%" . $this->db->escape_like_str($keyword) . "%' 
                    OR nik LIKE '%".$this->db->escape_like_str($keyword)."%' 
                )
            AND is_active = 1
            LIMIT 100
            ";
        return $this->db->query($sql);
    }

    function getAktifPasien($keyword) {
        $sql = "
            SELECT p.*, pp.jaminan, pp.no_jaminan, MAX(pp.id) as pendaftaran_id
            FROM pendaftaran_pasien pp
            JOIN pemeriksaan pem ON pem.pendaftaran_id = pp.id
            JOIN pasien p ON pem.pasien_id = p.id
            WHERE ( 
                p.no_rm LIKE '%" . $this->db->escape_like_str($keyword) . "%' 
                OR p.nama LIKE '%" . $this->db->escape_like_str($keyword) . "%' 
            )
            AND p.is_active = 1
            AND pem.status != 'selesai'
            AND pem.status != 'sudah_bayar'
            AND pp.pendaftaran_id IS null
            GROUP BY pp.pasien
            ORDER BY pp.id desc
            LIMIT 100
            ";
        return $this->db->query($sql);
    }

    function hapusPendaftaran($idPendaftaran) {
        $this->db->query("DELETE FROM antrian WHERE pendaftaran_id = $idPendaftaran");
        $this->db->query("UPDATE pemeriksaan SET is_active = 0 WHERE pendaftaran_id = '$idPendaftaran'");
        return $this->db->query("UPDATE pendaftaran_pasien SET is_active = 0 WHERE id = '$idPendaftaran'");
    }
}
