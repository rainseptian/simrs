<?php

class Datatables extends CI_Input{

    private $columns = [];
    private $builders = [];
    private $model;
    private $modelFunctionName;
    private $modelBuilder;

    public function populate(){

        $this->modelBuilder = $this->getModelBuilder();
        $this->runBuilder();
        $totalRecord = $this->getTotalRecord();
        $data = $this->paginate($totalRecord);
        $formattedDatas = [];
        
        $i = $this->getStart() + 1;
        foreach ($data as $item) {
            $formattedData = [];
            foreach($this->columns as $column) {
                $formattedData[$column['column']] = $column['fn']($item, $i);
            }
            $formattedDatas[] = $formattedData;
            $i++;
        }

        $output['draw'] = !empty($this->getDraw()) ? intval($this->getDraw()) : 0;
        $output['recordsTotal']    = intval(count($data));
        $output['recordsFiltered'] = intval($totalRecord);
        $output['data']            = $formattedDatas;

        return $output;

    }

    private function paginate($totalRecord) {

        $length = $this->getLength();        
        $db = $this->modelBuilder;
        if($totalRecord < $length) {
            $db->limit($length);
        } else {
            $db->limit($length, ($this->getPage()) *  $length);
        }
        return $db->get()->result_array();
    }

    public function dbBuilder($callback) {
        $this->builders[] = $callback;
        return $this;
    }

    private function runBuilder() {
        $db = $this->modelBuilder;
        foreach($this->builders as $builder) {
            $builder($db);
        }
    }

    public function getModelBuilder() {
        $fn = $this->modelFunctionName;
        return $this->model->$fn($this->getSearch());
    }

    private function getTotalRecord() {
        $db = clone $this->modelBuilder; 
        $db->select('count(*) as total_record');
        return $db->get()->row()->total_record;
        
    }

    public function setModel($model) {
        $this->model = $model;
        return $this;
    }

    public function setModelFunctionName($functionName) {
        $this->modelFunctionName = $functionName;
        return $this;
    }

    /**
     * @return Datatables
     */
    public function addColumn($columnName, $callback) {
        $this->columns[] = ['column' => $columnName, 'fn' => $callback];
        return $this;
    }

    public function getDraw() {
        return $this->get('draw');
    }

    public function getStart() {
        return $this->get('start');
    }

    public function getPage() {
        return $this->getStart() / $this->get('length') + 1;
    }    

    public function getLength() {
        return (int)$this->get('length');
    }

    public function getSearch() {
        return  $this->get('search')['value'];
    }
}