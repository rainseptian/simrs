<?php
$currency_symbol = 'Rp';
?>
<link rel="stylesheet"
      href="<?php echo base_url() ?>assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
<link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/autocomplete/style-gue.css">
<style type="text/css">
    @import('https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.0/css/bootstrap.min.css');
</style>

<style type="text/css">
    .table-responsive {overflow-x: inherit;}
</style>
<div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title titlefix"> Data Transfer <?=ucwords($jenis)?> Perinatologi</h3>
                    </div><!-- /.box-header -->
                    <?php $warning = $this->session->flashdata('warning');
                    if (!empty($warning)){ ?>
                        <div class="alert alert-warning alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <h4><i class="icon fa fa-warning"></i> Alert!</h4>
                            <?php echo $warning ?>
                        </div>
                    <?php } ?>
                    <?php $success = $this->session->flashdata('success');
                    if (!empty($success)){ ?>
                        <div class="alert alert-success alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <h4><i class="icon fa fa-check"></i> Success!</h4>
                            <?php echo $success ?>
                        </div>
                    <?php } ?>
                    <div class="box-body">
                        <table class="table table-striped table-bordered table-hover data-table" cellspacing="0" width="100%">
                            <thead>
                            <tr>
                                <th>No</th>
                                <th>Tgl Transfer</th>
                                <th>Tgl Edit</th>
                                <th>Nama Pasien</th>
                                <th>Alamat</th>
                                <th>Nama Dokter</th>
                                <th><?=$jenis == 'masuk' ? 'Dari' : 'Ke'?></th>
                                <th>Aksi</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $no = 1;
                            foreach ($list as $r) : ?>
                                <tr>
                                    <td><?=$no++?></td>
                                    <td><?=date('d-F-Y H:i', strtotime($r['tanggal'].' '.$r['pukul']))?></td>
                                    <td><?=$r['updated_at'] ? date('d-F-Y H:i', strtotime($r['updated_at'])) : '-'?></td>
                                    <td>
                                        <?=$r['nama_pasien']?>
                                        <br>
                                        <span style="font-size: 12px; color: #999999; font-style: italic"><?=$r['no_rm']?></span>
                                    </td>
                                    <td><?=$r['alamat']?></td>
                                    <td><?=$r['nama_dokter']?></td>
                                    <td><?=$r['dari']?></td>
                                    <td>
                                        <form method="post" action="<?=base_url('TransferPasien/edit/'.$r['id'])?>">
                                            <input type="hidden" name="redirect" value="<?='Perinatologi/dataTransfer/'.$jenis?>">
                                            <button type="submit" class="btn btn-primary">
                                                <span class="fa fa-edit"></span> Edit
                                            </button>
                                        </form>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<script type="text/javascript">
    $(function () {
        $('.data-table').DataTable()
    })
</script>
