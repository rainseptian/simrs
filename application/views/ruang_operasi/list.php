<?php
$currency_symbol = 'Rp';
?>
<link rel="stylesheet"
      href="<?php echo base_url() ?>assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
<link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/autocomplete/style-gue.css">
<style type="text/css">
    @import('https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.0/css/bootstrap.min.css');
</style>

<style type="text/css">
    .table-responsive {overflow-x: inherit;}
</style>
<div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title titlefix"> Pasien Ruang Operasi</h3>
                    </div><!-- /.box-header -->
                    <?php $warning = $this->session->flashdata('warning');
                    if (!empty($warning)){ ?>
                        <div class="alert alert-warning alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <h4><i class="icon fa fa-warning"></i> Alert!</h4>
                            <?php echo $warning ?>
                        </div>
                    <?php } ?>
                    <?php $success = $this->session->flashdata('success');
                    if (!empty($success)){ ?>
                        <div class="alert alert-success alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <h4><i class="icon fa fa-check"></i> Success!</h4>
                            <?php echo $success ?>
                        </div>
                    <?php } ?>
                    <div class="box-body">
                        <table class="table table-striped table-bordered table-hover data-table" cellspacing="0" width="100%">
                            <thead>
                            <tr>
                                <th>No</th>
                                <th>Tgl Daftar</th>
                                <th>Tgl Operasi</th>
                                <th>No RM</th>
                                <th>Nama Pasien</th>
                                <th>Alamat</th>
                                <th>Nama Dokter</th>
                                <th>Dari</th>
                                <th>Tipe Pasien</th>
                                <th>Aksi</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $no = 1;
                            foreach ($list as $r) : ?>
                                <tr>
                                    <td><?=$no++?></td>
                                    <td><?=date('d-M-Y H:i', strtotime($r['transfered_at']))?></td>
                                    <td>
                                        <?php if ($r['operasi_at']) : ?>
                                            <?=date('d-M-Y H:i', strtotime($r['operasi_at']))?>
                                            <a style="cursor: pointer"
                                               href="#"
                                               onclick="edit_tanggal(<?=$r['transfer_id']?>, '<?=$r['nama_pasien']?>', '<?=$r['operasi_at']?>')">
                                                Edit
                                            </a>
                                        <?php else: ?>
                                            <button class="btn btn-sm btn-success" onclick="atur_tanggal(<?=$r['transfer_id']?>, '<?=$r['nama_pasien']?>')">Atur Tanggal</button>
                                        <?php endif; ?>
                                    </td>
                                    <td><?=$r['no_rm']?></td>
                                    <td><?=$r['nama_pasien']?></td>
                                    <td><?=$r['alamat']?></td>
                                    <td><?=$r['nama_dokter']?></td>
                                    <td><?=$r['dari']?></td>
                                    <td><?=$r['tipe_pasien']?></td>
                                    <td>
                                        <a href="<?php echo base_url(); ?>RuangOperasi/detail/<?php echo $r['id']; ?>/<?php echo $r['transfer_id']; ?>">
                                            <button type="button" class="btn btn-primary btn-block"><i class="fa fa-arrows"></i> Periksa</button>
                                        </a>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<div class="modal fade" id="modal-tanggal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <form action="<?=base_url()?>/RuangOperasi/atur_tanggal" method="post">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 style="display: inline-block" class="modal-title" id="exampleModalLabel">Atur tanggal operasi</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="id" id="atur_tanggal_id">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-12">
                                <div class="form-group">
                                    <label>Tanggal</label>
                                    <input type="date" class="form-control" name="tgl" id="tgl_atur" value="<?=date('Y-m-d')?>">
                                </div>
                                <div class="form-group">
                                    <label>Jam</label>
                                    <input type="time" class="form-control" name="jam" id="jam_atur" value="<?=date('H:i')?>">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-success">Simpan</button>
                </div>
            </div>
        </form>
    </div>
</div>

<script type="text/javascript">
    $(function () {
        $('.data-table').DataTable()
    })

    function atur_tanggal(id, name) {
        $('#atur_tanggal_id').val(id)
        $('#exampleModalLabel').html(`Atur Tanggal Operasi: ${name}`)
        $('#modal-tanggal').modal('show')
    }

    function edit_tanggal(id, name, tgl) {
        const waktu = tgl.split(' ')
        $('#tgl_atur').val(waktu[0])
        $('#jam_atur').val(waktu[1])

        $('#atur_tanggal_id').val(id)
        $('#exampleModalLabel').html(`Edit Tanggal Operasi: ${name}`)
        $('#modal-tanggal').modal('show')
    }
</script>
