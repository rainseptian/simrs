<?php
$u = $this->session->userdata('logged_in');
?>
<link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/iCheck/all.css">
<div class="content-wrapper">
    <style type="text/css">
        .form-group {
            margin-bottom: 2px;
        }
    </style>
    <section class="content-header">
        <h1>
            Pendaftaran Pasien Lama
            <small>Preview</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url() ?>Dashbord"><i class="fa fa-dashboard"></i> Dashbord</a></li>
            <li class="active">Pendaftaran</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <form class="form-horizontal" method="post" action="<?php echo base_url() ?>Igd/daftarPasienLama">
                <div class="col-xs-12">
                    <div class="box box-danger">
                        <div class="box-header">
                            <h3 class="box-title">Data Pasien</h3>
                        </div>
                        <?php $warning = $this->session->flashdata('warning');
                        if (!empty($warning)) { ?>
                            <div class="alert alert-warning alert-dismissible">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;
                                </button>
                                <h4><i class="icon fa fa-warning"></i> Alert!</h4>
                                <?php echo $warning ?>
                            </div>
                        <?php } ?>
                        <?php $success = $this->session->flashdata('success');
                        if (!empty($success)) { ?>
                            <div class="alert alert-success alert-dismissible">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;
                                </button>
                                <h4><i class="icon fa fa-check"></i> Success!</h4>
                                <?php echo $success ?>
                            </div>
                        <?php } ?>


                        <!-- Date dd/mm/yyyy -->

                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="nama" class="col-sm-4 control-label">No RM</label>

                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" id="no_rm" name="no_rm"
                                                   value="<?php echo $pasien->no_rm; ?>"
                                                   placeholder="Masukkan no_rm pasien" readonly>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="nama" class="col-sm-4 control-label">Nama</label>

                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" id="nama"
                                                   value="<?php echo $pasien->nama; ?>" name="nama"
                                                   placeholder="Masukkan nama pasien">
                                            <input type="hidden" class="form-control" id="id" name="id"
                                                   value="<?php echo $pasien->id; ?>"
                                                   placeholder="Masukkan nama pasien">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="tanggal_lahir" class="col-sm-4 control-label">Tanggal Lahir</label>

                                        <div class="col-sm-8">
                                            <input type="date" class="form-control" id="tanggal_lahir"
                                                   name="tanggal_lahir" value="<?php echo $pasien->tanggal_lahir; ?>"
                                            >
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="tempat_lahir" class="col-sm-4 control-label">Tempat Lahir</label>

                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" id="tempat_lahir"
                                                   name="tempat_lahir" value="<?php echo $pasien->tempat_lahir; ?>"
                                                   placeholder="masukkan tempat lahir pasien">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="jenis_kelamin" class="col-sm-4 control-label">Jenis Kelamin</label>
                                        <div class="col-sm-8">
                                            <label class="radio-inline"><input type="radio" value="L"
                                                                               name="jenis_kelamin"
                                                                               id="jenis_kelamin" <?= $pasien->jk == 'L' ? 'checked' : '' ?>>Laki
                                                - laki</label>
                                            <label class="radio-inline"><input type="radio" value="P"
                                                                               name="jenis_kelamin"
                                                                               id="jenis_kelamin" <?= $pasien->jk == 'P' ? 'checked' : '' ?>>Perempuan</label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="alamat" class="col-sm-4 control-label">Alamat (Sesuai KTP)</label>

                                        <div class="col-sm-8">
                                            <textarea type="text" class="form-control" id="alamat" name="alamat"
                                                      placeholder="Masukkan alamat pasien"
                                            ><?php echo $pasien->alamat; ?></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="telepon" class="col-sm-4 control-label">No. Telepon</label>

                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" id="telepon" name="telepon"
                                                   value="<?php echo $pasien->telepon; ?>"
                                                   placeholder="Masukkan telepon pasien">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="pekerjaan" class="col-sm-4 control-label">Pekerjaan</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" id="pekerjaan" name="pekerjaan"
                                                   value="<?php echo $pasien->pekerjaan; ?>"
                                                   placeholder="masukkan pekerjaan Pasien">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="agama" class="col-sm-4 control-label">Agama</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" id="agama" name="agama"
                                                   value="<?php echo $pasien->agama; ?>"
                                                   placeholder="masukkan agama Pasien" required>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="tingkat_pendidikan" class="col-sm-4 control-label">Tingkat
                                            Pendidikan</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" id="tingkat_pendidikan"
                                                   name="tingkat_pendidikan"
                                                   value="<?php echo $pasien->tingkat_pendidikan; ?>"
                                                   placeholder="masukkan tingkat pendidikan Pasien" required>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="penanggungjawab" class="col-sm-4 control-label">Penanggung Jawab</label>

                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" id="penanggungjawab"
                                                   name="penanggungjawab"
                                                   value="<?php echo $pendaftaran->penanggungjawab; ?>"
                                                   placeholder="Masukkan penanggung jawab dari pasien">
                                        </div>
                                    </div>
<!--                                    <div class="form-group hidden">-->
<!--                                        <label for="asuhan" class="col-sm-4 control-label">Catatan-->
<!--                                            Alergi/Lainnya</label>-->
<!---->
<!--                                        <div class="col-sm-8">-->
<!--                                            <input type="hidden" class="form-control" id="asuhan" name="asuhan"-->
<!--                                                   value="--><?php //echo $pendaftaran->asuhan_keperawatan; ?><!--"-->
<!--                                                   placeholder="Masukkan catatan alergi/lainnya" required>-->
<!--                                        </div>-->
<!--                                    </div>-->
                                    <input type="hidden" class="form-control" id="biopsikososial" name="biopsikososial"
                                           placeholder="Masukkan bio psiko sosial" value="">
                                    <div class="form-group">
                                        <label for="jenis_pendaftaran" class="col-sm-4 control-label">Poli</label>

                                        <div class="col-sm-8">
                                            <input type="hidden" name="jenis_pendaftaran"
                                                   value="<?= $jenis_pendaftaran->id ?>">
                                            <input type="text" class="form-control"
                                                   readonly
                                                   value="<?= $jenis_pendaftaran->nama ?>"
                                                   placeholder="Masukkan poli">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputtext3" class="col-sm-4 control-label">Jenis Pendaftaran</label>
                                        <div class="col-sm-8">
                                            <select id="jaminan" class="abdush-select form-control" name="jaminan"
                                                    required>
                                                <option value="">--Pilih Jenis Pendaftaran--</option>
                                                <?php foreach ($jaminan as $key => $value) { ?>
                                                    <option value="<?= $key ?>"><?= $value['label'] ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group" id="nomor">
                                        <label for="asuhan" class="col-sm-4 control-label">No</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" name="no_jaminan"
                                                   value="<?php echo $pasien->no_bpjs; ?>"
                                                   placeholder="Nomor">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputtext3" class="col-sm-4 control-label">Pilih Dokter</label>

                                        <div class="col-sm-8">
                                            <select class="form-control abdush-select" name="dokter" required>
                                                <option value="" selected>-- Pilih Dokter --</option>
                                                <?php foreach ($dokter->result() as $key => $value) {
                                                    ?>
                                                    <option value="<?php echo $value->id ?>"><?php echo $value->nama; ?></option>
                                                <?php } ?>

                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php if ($u->nama_grup == 'front_office') : ?>
                            <div class="box-footer">
                                <button type="submit" name="submit" value="1"
                                        class="btn btn-primary btn-lg btn-flat pull-right">Daftar
                                </button>
                                <a href="<?php echo base_url() ?>pendaftaran/listPendaftaranPasien"
                                   class="btn btn-default btn-lg btn-flat pull-right">Batal</a>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
                <?php if ($u->nama_grup != 'front_office') : ?>
                    <div class="col-md-12">
                        <div class="box box-danger">
                            <div class="box-header">
                                <h3 class="box-title">Pemeriksaan Deteksi Vital</h3>
                            </div>
                            <div class="box-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="col-sm-3 col-md-3 col-lg-3  form-group">
                                            <label for="td" class="col-sm-3 control-label">TD</label>
                                            <div class="input-group col-sm-6 col-md-6 col-sm-6 col-lg-6">
                                                <input type="text" class="form-control requirable" name="td">
                                                <span class="input-group-addon">mmHg</span>
                                            </div>
                                        </div>
                                        <div class="col-sm-3 col-md-3 col-lg-3  form-group">
                                            <label for="inputEmail3" class="col-sm-3 control-label">R</label>
                                            <div class="input-group col-sm-6 col-md-6 col-sm-6 col-lg-6">
                                                <input type="text" class="form-control requirable" name="r">
                                                <span class="input-group-addon">K/Min</span>
                                            </div>
                                        </div>
                                        <div class="col-sm-3 col-md-3 col-lg-3  form-group">
                                            <label for="inputEmail3" class="col-sm-3 control-label">BB</label>
                                            <div class="input-group col-sm-6 col-md-6 col-sm-6 col-lg-6">
                                                <input type="text" class="form-control requirable" id="bb" name="bb"
                                                       onkeyup="set_bmi()">
                                                <span class="input-group-addon">Kg</span>
                                            </div>
                                        </div>
                                        <div class="col-sm-3 col-md-3 col-lg-3  form-group">
                                            <label for="inputEmail3" class="col-sm-3 control-label">N</label>
                                            <div class="input-group col-sm-6 col-md-6 col-sm-6 col-lg-6">
                                                <input type="text" class="form-control requirable" name="n">
                                                <span class="input-group-addon">K/Min</span>
                                            </div>
                                        </div>
                                        <div class="col-sm-3 col-md-3 col-lg-3  form-group">
                                            <label for="inputEmail3" class="col-sm-3 control-label">S</label>
                                            <div class="input-group col-sm-6 col-md-6 col-sm-6 col-lg-6">
                                                <input type="text" class="form-control requirable" id="s" name="s">
                                                <span class="input-group-addon">'0</span>
                                            </div>
                                        </div>
                                        <div class="col-sm-3 col-md-3 col-lg-3  form-group">
                                            <label for="inputEmail3" class="col-sm-3 control-label">TB</label>
                                            <div class="input-group col-sm-6 col-md-6 col-sm-6 col-lg-6">
                                                <input type="text" class="form-control requirable" id="tb" name="tb"
                                                       onkeyup="set_bmi()">
                                                <span class="input-group-addon">cm</span>
                                            </div>
                                        </div>
                                        <div class="col-sm-3 col-md-3 col-lg-3  form-group">
                                            <label for="inputEmail3" class="col-sm-3 control-label">BMI</label>
                                            <div class="input-group col-sm-6 col-md-6 col-sm-6 col-lg-6">
                                                <input type="text" class="form-control requirable" name="bmi" id="bmi">

                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="box-footer">
                                <button type="submit" name="submit" value="1"
                                        class="btn btn-primary btn-lg btn-flat pull-right">Daftar
                                </button>
                                <a href="<?php echo base_url() ?>pendaftaran/listPendaftaranPasien"
                                   class="btn btn-default btn-lg btn-flat pull-right">Batal</a>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>
            </form>
            <!-- /.box -->


            <div class="col-xs-12">
                <div class="box box-danger">
                    <div class="box-header">
                        <h3 class="box-title">Riwayat Periksa</h3>&nbsp;&nbsp;

                    </div>

                    <div class="box-body">
                        <table id="example2" class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>No</th>
                                <th>Tanggal Periksa</th>
                                <th>Diagnosis Jenis Penyakit</th>
                                <th>Obat</th>
                                <th>Obat Racik</th>
                                <th>Tindakan</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $no = 0;
                            foreach ($pemeriksaan as $row) {
                                $no++;
                                ?>
                                <tr>
                                    <td> <?php echo $no; ?></td>
                                    <td> <?php echo date('d-F-Y', strtotime($row->waktu_pemeriksaan)); ?></td>
                                    <!-- <td> <?php echo $row->no_rm; ?></td> -->
                                    <td>
                                        <table style="font-size: 10px;" class="table table-bordered">
                                            <thead>
                                            <tr>
                                                <th>Nama Penyakit</th>
                                                <th>Kode</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php
                                            if ($penyakit) {

                                                foreach ($penyakit->result() as $row1) {
                                                    if ($row->id == $row1->pemeriksaan_id) {
                                                        ?>
                                                        <tr>
                                                            <td> <?php echo $row1->nama; ?></td>
                                                            <td> <?php echo $row1->kode; ?></td>
                                                        </tr>
                                                    <?php }
                                                }
                                            } else { ?>
                                                <tr>
                                                    <td bgcolor="lightblue" colspan="3"> Data Tidak ada</td>
                                                </tr>
                                            <?php } ?>

                                            </tbody>
                                        </table>
                                    </td>
                                    <td>
                                        <table style="font-size: 10px;" class="table table-bordered">
                                            <thead>
                                            <tr>

                                                <th>Nama Obat</th>
                                                <th> signa</th>
                                                <th> jumlah</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php
                                            if ($obat) {

                                                foreach ($obat->result() as $row2) {
                                                    if ($row->id == $row2->pemeriksaan_id) {
                                                        ?>
                                                        <tr>

                                                            <td> <?php echo $row2->nama; ?></td>
                                                            <td> <?php echo $row2->signa_obat; ?></td>
                                                            <td> <?php echo $row2->jumlah_satuan; ?></td>
                                                        </tr>
                                                    <?php }
                                                    $no++;
                                                }
                                            } else {
                                                ?>
                                                <tr>
                                                    <td bgcolor="lightblue" colspan="3"> Data Tidak ada</td>
                                                </tr>
                                            <?php } ?>


                                            </tbody>

                                        </table>
                                    </td>
                                    <td>
                                        <table style="font-size: 10px;" class="table table-bordered">
                                            <thead>
                                            <tr>

                                                <th>Nama Obat</th>
                                                <th> Signa Obat</th>

                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php
                                            if ($racikan) {

                                                foreach ($racikan->result() as $row4) {
                                                    if ($row->id == $row4->pemeriksaan_id) {
                                                        ?>
                                                        <tr>

                                                            <td> <?php echo $row4->nama_racikan; ?></td>
                                                            <td> <?php echo $row4->signa; ?></td>

                                                        </tr>
                                                    <?php }
                                                    $no++;
                                                }
                                            } else {
                                                ?>
                                                <tr>
                                                    <td bgcolor="lightblue" colspan="3"> Data Tidak ada</td>
                                                </tr>
                                            <?php } ?>


                                            </tbody>

                                        </table>
                                    </td>
                                    <td>
                                        <table style="font-size: 10px;" class="table table-bordered">
                                            <thead>
                                            <tr>
                                                <th>Nama Tindakan</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php


                                            if ($tindakan) {

                                                foreach ($tindakan->result() as $row3) {
                                                    if ($row->id == $row3->pemeriksaan_id) {
                                                        ?>
                                                        <tr>

                                                            <td> <?php echo $row3->nama; ?></td>

                                                        </tr>
                                                    <?php }
                                                }
                                            } else {
                                                ?>
                                                <tr>
                                                    <td bgcolor="lightblue" colspan="3"> Data Tidak ada</td>
                                                </tr>
                                            <?php } ?>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>

                            <?php } ?>


                            </tbody>

                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>


            <!-- /.col (right) -->
        </div>
        <!-- /.row -->

    </section>
    <!-- /.content -->
</div>

<div class="modal modal-primary fade" id="modal-info">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Primary Modal</h4>
            </div>
            <div class="modal-body">
                <p>One fine body…</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-outline">Save changes</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<script src="<?php echo base_url() ?>assets/plugins/autocomplete/ajax.js"></script>
<!-- InputMask -->
<script src="<?php echo base_url() ?>assets/plugins/input-mask/jquery.inputmask.js"></script>
<script src="<?php echo base_url() ?>assets/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="<?php echo base_url() ?>assets/plugins/input-mask/jquery.inputmask.extensions.js"></script>

<script type="text/javascript">
    //Datemask dd/mm/yyyy
    $('#datemask').inputmask('dd/mm/yyyy', {'placeholder': 'dd/mm/yyyy'})
    //Datemask2 mm/dd/yyyy
    $('#datemask2').inputmask('mm/dd/yyyy', {'placeholder': 'mm/dd/yyyy'})
    //Money Euro
    $('[data-mask]').inputmask()

    function set_bmi() {
        var tb = $('#tb').val();
        var bb = $('#bb').val();
        var tbm = tb / 100;
        var bmi = bb / (tbm * tbm);

        $('#bmi').val(bmi.toFixed(2));
    }

    $(function () {
        $('#nomor').hide();
        $('#jaminan').on('change', function (e) {
            if (this.value === 'umum') {
                $('#nomor').hide();
            } else {
                $('#nomor').show();
            }
        });
        // $('#poli').on('change', function() {
        //     $('.requirable').prop('required', this.value != 19);
        // });
    });

</script>
