<?php

include BASEPATH.'../application/views/template/InputBuilder.php';

?>

<link rel="stylesheet" href="<?= base_url() ?>assets/plugins/iCheck/all.css">

<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Pemeriksaan Pasien
            <small>Preview</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?= base_url() ?>Dashbord"><i class="fa fa-dashboard"></i> Dashbord</a></li>
            <li class="active">Pemeriksaan</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-7">
                <div class="box box-danger">
                    <div class="box-header">
                    </div>
                    <div class="box-body">
                        <form class="form-horizontal" method="post" action="<?= base_url()?>Igd/submitPeriksaAwal/<?= $pendaftaran['pasien'] ?>">
                            <div class="box-body">
                                <input type="hidden" name="is_bpjs" value="<?=$pendaftaran['is_bpjs']?>">
                                <input type="hidden" name="jaminan" value="<?=$pendaftaran['jaminan']?>">
                                <div class="row" >
                                    <div class="col-md-12">
                                        <?php
                                        sm4('no_rm', 'No Rekam Medis')->val($pendaftaran['no_rm'])->readonly()->build();
                                        ?>
                                        <div class="form-group">
                                            <label class="col-sm-3"></label>
                                            <div class="col-sm-9">
                                                <span class="label <?=$jaminan[$pendaftaran['jaminan']]['class']?>"><?=$jaminan[$pendaftaran['jaminan']]['label']?></span>
                                                <?php if (!isset($jaminan[$pendaftaran['jaminan']])) { ?>
                                                    <span class="label label-warning">Umum</span>
                                                <?php } ?>

                                            </div>
                                        </div>
                                        <?php
                                        sm4('nama_pasien', 'Nama Pasien')->val($pendaftaran['nama_pasien'])->readonly()->build();
                                        sm4('usia', 'Usia')->val($pendaftaran['usia'])->readonly()->build();
                                        sm4('alamat', 'Alamat')->val($pendaftaran['alamat'])->readonly()->build();
                                        sm4('bmi', 'BMI')->val($pendaftaran['bmi'])->build();
                                        ?>
                                    </div>
                                </div>
                                <div class="box-footer">
                                    <div class="row" >
                                        <div class="col-md-12">
                                            <?php
                                            unit('td', 'TD')->val($pendaftaran['td'] ?: ($pemeriksaan['td'] ?? ''))->required()->unit('mmHg')->build();
                                            unit('r', 'R')->val($pendaftaran['r'] ?: ($pemeriksaan['r'] ?? ''))->required()->unit('K/Min')->build();
                                            unit('bb', 'BB')->val($pendaftaran['bb'] ?: ($pemeriksaan['bb'] ?? ''))->required()->unit('Kg')->onkeyup('set_bmi()')->build();
                                            unit('n', 'N')->val($pendaftaran['n'] ?: ($pemeriksaan['n'] ?? ''))->required()->unit('K/Min')->build();
                                            unit('s', 'S')->val($pendaftaran['s'] ?: ($pemeriksaan['s'] ?? ''))->required()->unit("'0")->build();
                                            unit('tb', 'TB')->val($pendaftaran['tb'] ?: ($pemeriksaan['tb'] ?? ''))->required()->unit("cm")->onkeyup('set_bmi()')->build();
                                            unit('spo2', 'Spo2')->val($pemeriksaan['spo2'] ?? '')->required()->unit("%")->build();
                                            ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="box-footer">
                                    <div class="row" >
                                        <div class="col-md-12">
                                            <?php
                                            // jika poli KIA
                                            if ($pendaftaran['jenis_pendaftaran_id'] == 57) : ?>
                                                <div class="form-group">
                                                    <label for="jenis_kia" class="col-sm-3 control-label">Jenis Pemeriksaan KIA</label>
                                                    <div class="col-sm-9">
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="jenis_kia[]" value="hamil">Pemeriksaan Ibu Hamil
                                                            </label>
                                                        </div>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="jenis_kia[]" value="kb"> Keikutsertaan KB
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php endif; ?>
                                            <!--                                form perawat-->
                                            <div>
                                                <h4><strong>PENGKAJIAN INSTALASI GAWAT DARURAT</strong></h4>
                                                <div class="form-group form-horizontal" style="display: flex; align-items: center; margin-left: 0px">
                                                    <label>Jam</label>
                                                    <input style="margin-left: 10px; width: 100px" type="time" class="form-control" name="form[igd][jam]" value="<?=$form['igd']['jam'] ?? date('H:i')?>">
                                                </div>
                                                <p style="margin-top: 10px; margin-bottom: 0">Tingkat kegawatan</p>
                                                <div class="form-check" style="display: inline; padding-right: 10px">
                                                    <input class="form-check-input" type="radio" name="form[igd][kegawatan]" value="Merah" id="kegawatan1" <?= isset($form['igd']['kegawatan']) && $form['igd']['kegawatan'] == 'Merah' ? 'checked' : '' ?>>
                                                    <label class="form-check-label" for="kegawatan1" style="font-weight: normal !important;">
                                                        Merah
                                                    </label>
                                                </div>
                                                <div class="form-check" style="display: inline; padding-right: 10px">
                                                    <input class="form-check-input" type="radio" name="form[igd][kegawatan]" value="Kuning" id="kegawatan2" <?= isset($form['igd']['kegawatan']) && $form['igd']['kegawatan'] == 'Kuning' ? 'checked' : '' ?>>
                                                    <label class="form-check-label" for="kegawatan2" style="font-weight: normal !important;">
                                                        Kuning
                                                    </label>
                                                </div>
                                                <div class="form-check" style="display: inline; padding-right: 10px">
                                                    <input class="form-check-input" type="radio" name="form[igd][kegawatan]" value="Hijau" id="kegawatan3" <?= isset($form['igd']['kegawatan']) && $form['igd']['kegawatan'] == 'Hijau' ? 'checked' : '' ?>>
                                                    <label class="form-check-label" for="kegawatan3" style="font-weight: normal !important;">
                                                        Hijau
                                                    </label>
                                                </div>
                                                <div class="form-check" style="display: inline">
                                                    <input class="form-check-input" type="radio" name="form[igd][kegawatan]" value="Hitam" id="kegawatan4" <?= isset($form['igd']['kegawatan']) && $form['igd']['kegawatan'] == 'Hitam' ? 'checked' : '' ?>>
                                                    <label class="form-check-label" for="kegawatan4" style="font-weight: normal !important;">
                                                        Hitam
                                                    </label>
                                                </div>
                                                <p style="margin-top: 10px; margin-bottom: 0">Jenis Pelayanan</p>
                                                <div class="form-check" style="display: inline; padding-right: 10px">
                                                    <input class="form-check-input" type="radio" name="form[igd][pelayanan]" value="Bedah" id="pelayanan1" <?= isset($form['igd']['pelayanan']) && $form['igd']['pelayanan'] == 'Bedah' ? 'checked' : '' ?>>
                                                    <label class="form-check-label" for="pelayanan1" style="font-weight: normal !important;">Bedah</label>
                                                </div>
                                                <div class="form-check" style="display: inline; padding-right: 10px">
                                                    <input class="form-check-input" type="radio" name="form[igd][pelayanan]" value="Non Bedah" id="pelayanan2" <?= isset($form['igd']['pelayanan']) && $form['igd']['pelayanan'] == 'Non Bedah' ? 'checked' : '' ?>>
                                                    <label class="form-check-label" for="pelayanan2" style="font-weight: normal !important;">Non Bedah</label>
                                                </div>
                                                <div class="form-check" style="display: inline; padding-right: 10px">
                                                    <input class="form-check-input" type="radio" name="form[igd][pelayanan]" value="Kebidanan" id="pelayanan3" <?= isset($form['igd']['pelayanan']) && $form['igd']['pelayanan'] == 'Kebidanan' ? 'checked' : '' ?>>
                                                    <label class="form-check-label" for="pelayanan3" style="font-weight: normal !important;">Kebidanan</label>
                                                </div>
                                                <div class="form-check" style="display: inline">
                                                    <input class="form-check-input" type="radio" name="form[igd][pelayanan]" value="Anak" id="pelayanan4" <?= isset($form['igd']['pelayanan']) && $form['igd']['pelayanan'] == 'Anak' ? 'checked' : '' ?>>
                                                    <label class="form-check-label" for="pelayanan4" style="font-weight: normal !important;">Anak</label>
                                                </div>
                                                <p style="margin-top: 10px; margin-bottom: 0">Alasan Datang</p>
                                                <div class="form-check" style="display: inline; padding-right: 10px">
                                                    <input class="form-check-input" type="radio" name="form[igd][alasan_datang]" value="Penyakit" id="alasan_datang1" <?= isset($form['igd']['alasan_datang']) && $form['igd']['alasan_datang'] == 'Penyakit' ? 'checked' : '' ?>>
                                                    <label class="form-check-label" for="alasan_datang1" style="font-weight: normal !important;">Penyakit</label>
                                                </div>
                                                <div class="form-check" style="display: inline">
                                                    <input class="form-check-input" type="radio" name="form[igd][alasan_datang]" value="Trauma / Ruda Paksa" id="alasan_datang4" <?= isset($form['igd']['alasan_datang']) && $form['igd']['alasan_datang'] == 'Trauma / Ruda Paksa' ? 'checked' : '' ?>>
                                                    <label class="form-check-label" for="alasan_datang4" style="font-weight: normal !important;">Trauma / Ruda Paksa</label>
                                                </div>
                                                <p style="margin-top: 10px; margin-bottom: 0">Cara Masuk</p>
                                                <div class="form-check" style="display: inline; padding-right: 10px">
                                                    <input class="form-check-input" type="radio" name="form[igd][cara_masuk]" value="Sendiri" id="cara_masuk1" <?= isset($form['igd']['cara_masuk']) && $form['igd']['cara_masuk'] == 'Sendiri' ? 'checked' : '' ?>>
                                                    <label class="form-check-label" for="cara_masuk1" style="font-weight: normal !important;">Sendiri</label>
                                                </div>
                                                <div class="form-check" style="display: inline">
                                                    <input class="form-check-input" type="radio" name="form[igd][cara_masuk]" value="Rujukan" id="cara_masuk4" <?= isset($form['igd']['cara_masuk']) && $form['igd']['cara_masuk'] == 'Rujukan' ? 'checked' : '' ?>>
                                                    <label class="form-check-label" for="cara_masuk4" style="font-weight: normal !important; padding-right: 10px">Rujukan</label>
                                                    <input type="text" class="form-control" style="display: inline; width: 250px" placeholder="Rujukan" name="form[igd][cara_masuk_rujukan]" value="<?= $form['igd']['cara_masuk_rujukan'] ?? '' ?>">
                                                </div>
                                                <hr>
                                            </div>
                                            <div>
                                                <h4><strong>Alergi</strong></h4>
                                                <p>Apakah pasien mempunyai indikasi alergi ?</p>
                                                <div class="form-check" style="display: inline">
                                                    <input class="form-check-input" type="radio" name="form[alergi]" value="ya" id="flexRadioDefault1" <?= isset($form['alergi']) && $form['alergi'] == 'ya' ? 'checked' : '' ?>>
                                                    <label class="form-check-label" for="flexRadioDefault1" style="font-weight: normal !important;">
                                                        Ya
                                                    </label>
                                                </div>
                                                <div class="form-check" style="display: inline; margin-left: 16px">
                                                    <input class="form-check-input" type="radio" name="form[alergi]" value="tidak" id="flexRadioDefault2" <?= isset($form['alergi']) && $form['alergi'] == 'tidak' ? 'checked' : '' ?>>
                                                    <label class="form-check-label" for="flexRadioDefault2" style="font-weight: normal !important;">
                                                        Tidak
                                                    </label>
                                                </div>
                                                <?php sm9('form[alergi_detail]', 'Jika Ya, Jelaskan')->textarea()->val($form['alergi_detail'] ?? '')->build(); ?>
                                            </div>
                                            <div style="margin-top: 24px">
                                                <h4><strong>Nyeri</strong></h4>
                                                <p style="margin-bottom: 2px !important;">Skala Numerik</p>
                                                <div style="display: flex; width: 100%;">
                                                    <div style="flex: 1; display: flex; flex-direction: column; align-items: center">
                                                        <p style="margin-bottom: 0 !important;">1</p>
                                                        <input class="form-check-input" type="radio" name="form[nyeri]" value="1" style="margin-top: 0 !important;" <?= isset($form['nyeri']) && $form['nyeri'] == '1' ? 'checked' : '' ?>>
                                                    </div>
                                                    <div style="flex: 1; display: flex; flex-direction: column; align-items: center">
                                                        <p style="margin-bottom: 0 !important;">2</p>
                                                        <input class="form-check-input" type="radio" name="form[nyeri]" value="2" style="margin-top: 0 !important;" <?= isset($form['nyeri']) && $form['nyeri'] == '2' ? 'checked' : '' ?>>
                                                    </div>
                                                    <div style="flex: 1; display: flex; flex-direction: column; align-items: center">
                                                        <p style="margin-bottom: 0 !important;">3</p>
                                                        <input class="form-check-input" type="radio" name="form[nyeri]" value="3" style="margin-top: 0 !important;" <?= isset($form['nyeri']) && $form['nyeri'] == '3' ? 'checked' : '' ?>>
                                                    </div>
                                                    <div style="flex: 1; display: flex; flex-direction: column; align-items: center">
                                                        <p style="margin-bottom: 0 !important;">4</p>
                                                        <input class="form-check-input" type="radio" name="form[nyeri]" value="4" style="margin-top: 0 !important;" <?= isset($form['nyeri']) && $form['nyeri'] == '4' ? 'checked' : '' ?>>
                                                    </div>
                                                    <div style="flex: 1; display: flex; flex-direction: column; align-items: center">
                                                        <p style="margin-bottom: 0 !important;">5</p>
                                                        <input class="form-check-input" type="radio" name="form[nyeri]" value="5" style="margin-top: 0 !important;" <?= isset($form['nyeri']) && $form['nyeri'] == '5' ? 'checked' : '' ?>>
                                                    </div>
                                                    <div style="flex: 1; display: flex; flex-direction: column; align-items: center">
                                                        <p style="margin-bottom: 0 !important;">6</p>
                                                        <input class="form-check-input" type="radio" name="form[nyeri]" value="6" style="margin-top: 0 !important;" <?= isset($form['nyeri']) && $form['nyeri'] == '6' ? 'checked' : '' ?>>
                                                    </div>
                                                    <div style="flex: 1; display: flex; flex-direction: column; align-items: center">
                                                        <p style="margin-bottom: 0 !important;">7</p>
                                                        <input class="form-check-input" type="radio" name="form[nyeri]" value="7" style="margin-top: 0 !important;" <?= isset($form['nyeri']) && $form['nyeri'] == '7' ? 'checked' : '' ?>>
                                                    </div>
                                                    <div style="flex: 1; display: flex; flex-direction: column; align-items: center">
                                                        <p style="margin-bottom: 0 !important;">8</p>
                                                        <input class="form-check-input" type="radio" name="form[nyeri]" value="8" style="margin-top: 0 !important;" <?= isset($form['nyeri']) && $form['nyeri'] == '8' ? 'checked' : '' ?>>
                                                    </div>
                                                    <div style="flex: 1; display: flex; flex-direction: column; align-items: center">
                                                        <p style="margin-bottom: 0 !important;">9</p>
                                                        <input class="form-check-input" type="radio" name="form[nyeri]" value="9" style="margin-top: 0 !important;" <?= isset($form['nyeri']) && $form['nyeri'] == '9' ? 'checked' : '' ?>>
                                                    </div>
                                                    <div style="flex: 1; display: flex; flex-direction: column; align-items: center">
                                                        <p style="margin-bottom: 0 !important;">10</p>
                                                        <input class="form-check-input" type="radio" name="form[nyeri]" value="10" style="margin-top: 0 !important;" <?= isset($form['nyeri']) && $form['nyeri'] == '10' ? 'checked' : '' ?>>
                                                    </div>
                                                </div>
                                                <p style="margin-bottom: 2px !important; margin-top: 8px !important;">Wong Baker Faces</p>
                                                <div style="display: flex; flex-direction: column">
                                                    <img src="<?php echo base_url(); ?>assets/img/wong.jpeg" alt="User Image">
                                                    <div style="display: flex; flex: 1; margin-left: 6px; margin-right: 8px;">
                                                        <input class="form-check-input" type="radio" name="form[wong]" value="0" style="margin-top: 0 !important; flex: 1" <?= isset($form['wong']) && $form['wong'] == '0' ? 'checked' : '' ?>>
                                                        <input class="form-check-input" type="radio" name="form[wong]" value="2" style="margin-top: 0 !important; flex: 1" <?= isset($form['wong']) && $form['wong'] == '2' ? 'checked' : '' ?>>
                                                        <input class="form-check-input" type="radio" name="form[wong]" value="4" style="margin-top: 0 !important; flex: 1" <?= isset($form['wong']) && $form['wong'] == '4' ? 'checked' : '' ?>>
                                                        <input class="form-check-input" type="radio" name="form[wong]" value="6" style="margin-top: 0 !important; flex: 1" <?= isset($form['wong']) && $form['wong'] == '6' ? 'checked' : '' ?>>
                                                        <input class="form-check-input" type="radio" name="form[wong]" value="8" style="margin-top: 0 !important; flex: 1" <?= isset($form['wong']) && $form['wong'] == '8' ? 'checked' : '' ?>>
                                                        <input class="form-check-input" type="radio" name="form[wong]" value="10" style="margin-top: 0 !important; flex: 1" <?= isset($form['wong']) && $form['wong'] == '10' ? 'checked' : '' ?>>
                                                    </div>
                                                </div>
                                            </div>
                                            <div style="margin-top: 24px">
                                                <h4><strong>Gizi</strong></h4>
                                                <div class="col-sm-10 form-group">
                                                    <label for="td" class="col-sm-6 control-label">Tinggi Badan</label>
                                                    <div class="input-group col-sm-4">
                                                        <input type="text" class="form-control" name="form[tb]" id="form_tb" onkeyup="setImt()" autocomplete="off" value="<?=$form['tb'] ?? ''?>">
                                                        <span class="input-group-addon">cm</span>
                                                    </div>
                                                </div>
                                                <div class="col-sm-10 form-group">
                                                    <label for="td" class="col-sm-6 control-label">Berat Badan</label>
                                                    <div class="input-group col-sm-4">
                                                        <input type="text" class="form-control" name="form[bb]" id="form_bb" onkeyup="setImt()" autocomplete="off" value="<?=$form['bb'] ?? ''?>">
                                                        <span class="input-group-addon">kg</span>
                                                    </div>
                                                </div>
                                                <div class="col-sm-10 form-group">
                                                    <label for="td" class="col-sm-6 control-label">Indeks Massa Tubuh (IMT)</label>
                                                    <div class="input-group col-sm-4">
                                                        <input type="text" class="form-control" name="form[imt]" id="form_imt" autocomplete="off" value="<?=$form['imt'] ?? ''?>">
                                                        <span class="input-group-addon">kg/m2</span>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <div style="display: flex; flex-direction: column">
                                                            <div style="display: flex">
                                                                <div class="checkbox" style="flex: 1">
                                                                    <label>
                                                                        <input type="radio" name="form[gizi]" value="underweight" id="underweight" <?=isset($form['gizi']) && $form['gizi'] == 'underweight' ? 'checked' : ''?>> Underweight ( < 18.5 )
                                                                    </label>
                                                                </div>
                                                                <div class="checkbox" style="flex: 1">
                                                                    <label>
                                                                        <input type="radio" name="form[gizi]" value="overweight" id="overweight" <?=isset($form['gizi']) && $form['gizi'] == 'overweight' ? 'checked' : ''?>> Overweight (25 – 29.99)
                                                                    </label>
                                                                </div>
                                                            </div>
                                                            <div style="display: flex">
                                                                <div class="checkbox" style="flex: 1">
                                                                    <label>
                                                                        <input type="radio" name="form[gizi]" value="normal" id="normal" <?=isset($form['gizi']) && $form['gizi'] == 'normal' ? 'checked' : ''?>> Normal ( 18.5 – 24.99)
                                                                    </label>
                                                                </div>
                                                                <div class="checkbox" style="flex: 1">
                                                                    <label>
                                                                        <input type="radio" name="form[gizi]" value="obese" id="obese" <?=isset($form['gizi']) && $form['gizi'] == 'obese' ? 'checked' : ''?>> Obese (≥ 30)
                                                                    </label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div style="margin-top: 24px">
                                                <h4><strong>SKRINING RESIKO JATUH</strong></h4>
                                                <div style="display: flex">
                                                    <div>
                                                        <p style="margin-bottom: 2px !important;">Cara berjalan pasien (salah satu/lebih)</p>
                                                    </div>
                                                </div>
                                                <p style="margin-bottom: 2px !important;">a. Tidak seimbang/sempoyongan/limbung</p>
                                                <div class="checkbox" style="display: inline">
                                                    <label>
                                                        <input type="radio" name="form[tidak_seimbang]" value="ya" <?=isset($form['tidak_seimbang']) && $form['tidak_seimbang'] == 'ya' ? 'checked' : ''?>> Ya
                                                    </label>
                                                </div>
                                                <div class="checkbox" style="display: inline">
                                                    <label>
                                                        <input type="radio" name="form[tidak_seimbang]" value="tidak" <?=isset($form['tidak_seimbang']) && $form['tidak_seimbang'] == 'tidak' ? 'checked' : ''?>> Tidak
                                                    </label>
                                                </div>
                                                <br>
                                                <p style="margin-bottom: 2px !important;">b. Jalan dengan alat bantu (kruk, tripod, kursi roda, orang lain)</p>
                                                <div class="checkbox" style="display: inline">
                                                    <label>
                                                        <input type="radio" name="form[alat_bantu]" value="ya" <?=isset($form['alat_bantu']) && $form['alat_bantu'] == 'ya' ? 'checked' : ''?>> Ya
                                                    </label>
                                                </div>
                                                <div class="checkbox" style="display: inline">
                                                    <label>
                                                        <input type="radio" name="form[alat_bantu]" value="tidak" <?=isset($form['alat_bantu']) && $form['alat_bantu'] == 'tidak' ? 'checked' : ''?>> Tidak
                                                    </label>
                                                </div>
                                                <br>
                                                <p style="margin-bottom: 2px !important;">Ada keterbatasan gerak ?</p>
                                                <div class="checkbox" style="display: inline">
                                                    <label>
                                                        <input type="radio" name="form[skrining_keterbatasan_gerak]" value="ya" <?=isset($form['skrining_keterbatasan_gerak']) && $form['skrining_keterbatasan_gerak'] == 'ya' ? 'checked' : ''?>> Ya
                                                    </label>
                                                </div>
                                                <div class="checkbox" style="display: inline">
                                                    <label>
                                                        <input type="radio" name="form[skrining_keterbatasan_gerak]" value="tidak" <?=isset($form['skrining_keterbatasan_gerak']) && $form['skrining_keterbatasan_gerak'] == 'tidak' ? 'checked' : ''?>> Tidak
                                                    </label>
                                                </div>
                                            </div>
                                            <hr>
                                            <div>
                                                <?php
                                                sm9('diagnosa_perawat', 'Diagnosa Perawat')->textarea(3)->val($pemeriksaan['diagnosa_perawat'] ?? '')->build();
                                                sm9('keluhan_utama', 'Keluhan Utama')->textarea(3)->val($pemeriksaan['keluhan_utama'] ?? '')->build();
                                                sm9('asuhan_keperawatan', 'Catatan Alergi/Lainnya')->textarea(3)->val($pemeriksaan['asuhan_keperawatan'] ?? '')->build();
                                                sm9('form[tindakan_perawat]', 'Tindakan Perawat')->textarea(3)->val($form['tindakan_perawat'] ?? '')->build();

                                                sm9('form[daftar_masalah_medis]', 'Daftar Masalah Medis')->val($form['daftar_masalah_medis'] ?? '')->hide()->build();
                                                sm9('form[dsdok]', 'Data Subyektif dan Obyektif Keperawatan')->val($form['dsdok'] ?? '')->hide()->build();
                                                sm9('form[dmk]', 'Daftar Masalah Keperawatan')->val($form['dmk'] ?? '')->hide()->build();
                                                ?>
                                            </div>
                                            <!--                                end form perawat-->
                                        </div>
                                    </div>
                                </div>

                                <input type="hidden" class="form-control" name="pendaftaran_id" id="pendaftaran_id" value="<?= $pendaftaran['id']; ?>">
                                <input type="hidden" class="form-control" name="pemeriksaan_id" id="pemeriksaan_id" value="<?= $pemeriksaan['id']; ?>">
                                <input type="hidden" class="form-control" name="dokter_id" id="dokter_id" value="<?= $pendaftaran['dokter']; ?>" >

                            </div>
                            <div class="box-footer">
                                <button type="submit" name="submit" value="1" class="btn btn-primary btn-lg btn-flat pull-right">Simpan</button>
                                <a href="<?php echo base_url() ?>Igd/pemeriksaanAwal" class="btn btn-default btn-lg btn-flat pull-right">Batal</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<script type="text/javascript">
    function set_bmi() {
        var tb = $('#tb').val();
        var bb = $('#bb').val();
        var tbm = tb / 100;
        var bmi = bb / (tbm * tbm);

        $('#bmi').val(bmi.toFixed(2));
    }


    function setImt() {
        const tb = (parseInt($('#form_tb').val() || '0') ?? 0) / 100
        const bb = parseInt($('#form_bb').val() || '0') ?? 0
        const imt = bb / (tb * tb)

        $('#form_imt').val(imt.toFixed(2))

        $('#underweight').prop('checked', imt < 18.5)
        $('#normal').prop('checked', imt >= 18.5 && imt <= 24.99)
        $('#overweight').prop('checked', imt >= 25 && imt <= 29.99)
        $('#obese').prop('checked', imt >= 30)
    }

</script>
