<link rel="stylesheet" href="<?php echo base_url() ?>assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Data List Pasien IGD
            <small></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a href="#">List Pasien</a></li>
            <li class="active">Data List Pasien IGD</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Data Pasien Sudah Diperiksa</h3>&nbsp;&nbsp;
                    </div>
                    <?php $warning = $this->session->flashdata('warning');
                    if (!empty($warning)) { ?>
                        <div class="alert alert-warning alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <h4><i class="icon fa fa-warning"></i> Alert!</h4>
                            <?php echo $warning ?>
                        </div>
                    <?php } ?>
                    <?php $success = $this->session->flashdata('success');
                    if (!empty($success)) { ?>
                        <div class="alert alert-success alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <h4><i class="icon fa fa-check"></i> Success!</h4>
                            <?php echo $success ?>
                        </div>
                    <?php } ?>
                    <div class="box-body">
                        <table id="example2" class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>No</th>
                                <th>Tanggal Periksa</th>
                                <th>NO RM</th>
                                <th>Nama Pasien</th>
                                <th>Nama Dokter</th>
                                <th>Diagnosa jenis penyakit</th>
                                <th>Status</th>
                                <th>Aksi</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $no = 1;
                            foreach ($listPemeriksaan as $row) { ?>
                                <tr>
                                    <td> <?php echo $no; ?></td>
                                    <td> <?php echo date('d-F-Y', strtotime($row->waktu_pemeriksaan)); ?></td>
                                    <td style="width: 100px;">
                                        <?php echo $row->no_rm; ?><br>
                                        <small>
                                            <span class="label <?= $jaminan[$row->jaminan]['class'] ?>"><?= $jaminan[$row->jaminan]['label'] ?></span>
                                            <?php if (!isset($jaminan[$row->jaminan])) { ?>
                                                <span class="label label-warning">Umum</span>
                                            <?php } ?>
                                        </small>
                                    </td>
                                    <td> <?php echo ucwords($row->nama_pasien); ?></td>
                                    <td> <?php echo ucwords($row->nama_dokter); ?></td>
                                    <td>
                                        <?php if ($row->diagnosis_jenis_penyakit) { ?>
                                            <?= $row->diagnosis_jenis_penyakit; ?>
                                        <?php } else { ?>
                                            <table style="padding: 0px 5px;">
                                                <tbody>
                                                <?php foreach ($row->penyakits as $penyakit) { ?>
                                                    <tr>
                                                        <td><?= $penyakit->kode . ' - ' . $penyakit->nama ?></td>
                                                    </tr>
                                                <?php } ?>
                                                </tbody>
                                            </table>
                                        <?php } ?>
                                    </td>
                                    <td>
                                        <span class="pull-right-container text-uppercase">
                                            <small class="label pull-right bg-green">
                                                <?php echo ucwords(str_replace('_', ' ', $row->status)); ?>
                                            </small>
                                        </span>
                                    </td>
                                    <td>
                                        <a data-toggle="modal" id="add" onclick="holdModal('modal-operan', <?=$row->id?>)" class="btn btn-sm btn-primary">
                                            <i class="fa fa-plus"></i> <span>Tindakan Operan</span>
                                        </a>
                                    </td>
                                </tr>
                                <?php $no++;
                            } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<div class="modal fade" id="modal-operan" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content modal-media-content">
            <form id="form_add" method="post" accept-charset="utf-8">
                <div class="modal-header modal-media-header">
                    <button type="button" class="close pt4" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Tambah Tindakan Operan</h4>
                </div>
                <div class="modal-body">

                    <div class="row">
                        <div class="form-group">
                            <label for="a" class="col-sm-3 control-label">Tarif / Tindakan Operan</label>
                            <div class="col-sm-9">
                                <select id="tindakan_operan"
                                        class="form-control select2"
                                        multiple="multiple"
                                        name="tindakan_operan[]"
                                        data-placeholder="Pilih tindakan"
                                        style="width: 100%;">
                                </select>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="form-group">
                            <label for="a" class="col-sm-3 control-label">Perawat Operan</label>
                            <div class="col-sm-9">
                                <select id="perawat_operan"
                                        class="form-control select2"
                                        multiple="multiple"
                                        name="perawat_operan[]"
                                        data-placeholder="Pilih perawat"
                                        style="width: 100%;">
                                </select>
                            </div>
                        </div>
                    </div>
                    <br>

                </div>
                <div class="modal-footer">
                    <button type="submit" data-loading-text="Menambahkan..." id="submit_new_pasien" class="btn btn-info pull-right">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script src="<?= base_url() ?>assets/bower_components/select2/dist/js/select2.full.min.js"></script>

<script>
    $(function () {
        $('#example1').DataTable()
        $('#example2').DataTable({
            rowReorder: {
                selector: 'td:nth-child(2)'
            },
            'paging': true,
            'lengthChange': true,
            'searching': true,
            'ordering': true,
            'info': true,
            'autoWidth': true
        })
        $('.select2').select2();
    })

    function number_format (number, decimals, dec_point, thousands_sep) {
        // Strip all characters but numerical ones.
        number = (number + '').replace(/[^0-9+\-Ee.]/g, '');
        var n = !isFinite(+number) ? 0 : +number,
            prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
            sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
            dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
            s = '',
            toFixedFix = function (n, prec) {
                var k = Math.pow(10, prec);
                return '' + Math.round(n * k) / k;
            };
        // Fix for IE parseFloat(0.55).toFixed(0) = 0;
        s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
        if (s[0].length > 3) {
            s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
        }
        if ((s[1] || '').length < prec) {
            s[1] = s[1] || '';
            s[1] += new Array(prec - s[1].length + 1).join('0');
        }
        return s.join(dec);
    }

    function holdModal(modalId, pemeriksaanId) {
        $('#' + modalId).modal({
            backdrop: 'static',
            keyboard: false,
            show: true
        });
        showTindakan(pemeriksaanId)
    }

    const all_pemeriksaan = <?= json_encode($listPemeriksaan) ?>;
    const all_tindakan = <?= json_encode($tindakan) ?>;
    const all_perawat = <?= json_encode($listPerawat) ?>;

    function showTindakan(pemeriksaanId) {
        const p = all_pemeriksaan.find(v => parseInt(v.id) === parseInt(pemeriksaanId))

        $('#tindakan_operan')
            .find('option')
            .remove();

        $.each(all_tindakan, function (i, item) {
            $('#tindakan_operan').append($('<option>', {
                value: item.id,
                text : `${item.nama} - Rp.${number_format(item.tarif_pasien, 2, ',', '.')}`,
                selected: !!p.tindakan_operan.find(v => parseInt(v.tarif_tindakan_id) === parseInt(item.id))
            }));
        });

        $('#perawat_operan')
            .find('option')
            .remove();

        $.each(all_perawat, function (i, item) {
            $('#perawat_operan').append($('<option>', {
                value: item.id,
                text : item.nama,
                selected: !!p.perawat_operan.find(v => parseInt(v.id) === parseInt(item.id))
            }));
        });

        $('#form_add').attr('action', '<?= base_url() ?>Igd/add_operan/' + pemeriksaanId);
    }

</script>
