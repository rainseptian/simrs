<?php

function getKodePoli($kode_daftar = '', $data_poli) {
    foreach ($data_poli as $key => $value) {
        if (in_array($kode_daftar, $value['kode'])) return $key;
    }

    //kalau gak ada ketentuan jadinya umum
    return 'umum';
}

$data_poli = $this->config->item('poli');;
$kode_poli = getKodePoli($pendaftaran['kode_daftar'], $data_poli);

class InputBuilder {

    var $name;
    var $label;
    var $value = '';
    var $sm = 9;
    var $placeholder = '';
    var $unit = '';
    var $options;
    var $selectedOptIds = [];

    var $optdisplay;
    var $onkeyup;

    var $united = false;
    var $textarea = false;
    var $select = false;
    var $required = false;
    var $readonly = false;
    var $hide = false;

    function __construct($name, $label, $sm) {
        $this->name = $name;
        $this->label = $label;
        $this->sm = $sm;
    }

    public function val($v) {
        $this->value = $v;
        return $this;
    }

    public function placeholder($v) {
        $this->placeholder = $v;
        return $this;
    }

    public function unit($v) {
        $this->unit = $v;
        return $this;
    }

    public function united() {
        $this->united = true;
        return $this;
    }

    public function textarea() {
        $this->textarea = true;
        return $this;
    }

    public function select() {
        $this->select = true;
        return $this;
    }

    public function options($v) {
        $this->options = $v;
        return $this;
    }

    public function selectedOptionIds($v) {
        $this->selectedOptIds = $v;
        return $this;
    }

    public function display($v) {
        $this->optdisplay = $v;
        return $this;
    }

    public function onkeyup($v) {
        $this->onkeyup = $v;
        return $this;
    }

    public function required($b = true) {
        $this->required = $b;
        return $this;
    }

    public function readonly() {
        $this->readonly = true;
        return $this;
    }

    public function hide() {
        $this->hide = true;
        return $this;
    }

    public function build() {
        if ($this->select) {
            $this->buildSelect($this->hide);
        } else if ($this->united) {
            $this->buildUnited($this->hide);
        } else {
            $this->buildTypeText($this->hide);
        }
    }

    public function buildTypeText($hide) { ?>
        <div class="form-group <?=$hide ? 'hidden' : ''?>">
            <label for="<?= $this->name ?>" class="col-sm-3 control-label"><?= $this->label ?></label>
            <div class="col-sm-<?= $this->sm ?>">
                <?php if ($this->textarea) : ?>
                    <textarea type="text" class="form-control" name="<?= $this->name ?>" id="<?= $this->name ?>"
                              <?= $this->required ? 'required' : '' ?> <?= $this->readonly ? 'readonly' : '' ?>><?= $this->value ?></textarea>
                <?php else : ?>
                    <input type="text" class="form-control" name="<?= $this->name ?>" id="<?= $this->name ?>"
                           value="<?= $this->value ?>" <?= $this->required ? 'required' : '' ?> <?= $this->readonly ? 'readonly' : '' ?>>
                <?php endif; ?>
            </div>
        </div>
    <?php }

    public function buildSelect($hide) {
        if ($this->name == "tindakan[]"){ ?>
            <div class="form-group <?=$hide ? 'hidden' : ''?>">
                <label for="<?= $this->name ?>" class="col-sm-3 control-label"><?= $this->label ?></label>
                <div class="col-sm-<?= $this->sm ?>">
                    <select class="form-control select2" multiple="multiple" name="<?= $this->name ?>"
                            data-placeholder="<?= $this->placeholder ?>"
                            style="width: 100%;" <?= $this->required ? 'required' : '' ?>>
                        <?php foreach ($this->options as $key => $value) {
                            $select = ($value->nama == "Administrasi" )?'selected':''; ?>
                            <option value="<?= $value->id; ?>" <?php echo $select; ?> ><?= call_user_func($this->optdisplay, $value) ?></option>
                        <?php } ?>
                    </select>
                </div>
            </div>
        <?php   } else { ?>
            <div class="form-group <?=$hide ? 'hidden' : ''?>">
                <label for="<?= $this->name ?>" class="col-sm-3 control-label"><?= $this->label ?></label>
                <div class="col-sm-<?= $this->sm ?>">
                    <select class="form-control select2" multiple="multiple" name="<?= $this->name ?>"
                            data-placeholder="<?= $this->placeholder ?>"
                            style="width: 100%;" <?= $this->required ? 'required' : '' ?>>

                        <?php foreach ($this->options as $key => $value) {
                            $select = (in_array($value->id, $this->selectedOptIds)) ? 'selected':''; ?>
                            <option value="<?= $value->id; ?>" <?php echo $select; ?>><?= call_user_func($this->optdisplay, $value) ?></option>
                        <?php } ?>
                    </select>
                </div>
            </div>
            <?php
        }
    }

    public function buildUnited($hide) { ?>
        <div class="col-sm-6 col-md-6 col-lg-6 form-group <?=$hide ? 'hidden' : ''?>">
            <label for="<?= $this->name ?>" class="col-sm-3 control-label"><?= $this->label ?></label>
            <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">
                <input
                    type="text"
                    class="form-control"
                    id="<?= $this->name ?>"
                    name="<?= $this->name ?>"
                    value="<?= $this->value ?>"
                    <?= $this->required ? 'required' : '' ?>
                    <?= $this->readonly ? 'readonly' : '' ?>
                    <?= isset($this->onkeyup) && $this->onkeyup != '' ? 'onkeyup=' . $this->onkeyup : '' ?>
                >
                <span class="input-group-addon"><?= $this->unit ?></span>
            </div>
        </div>
    <?php }
}

function unit($name, $label) {
    $i = new InputBuilder($name, $label, 9);
    $i->united();
    return $i;
}

function sel($name, $label) {
    $i = new InputBuilder($name, $label, 9);
    $i->select();
    return $i;
}

function sm4($name, $label) {
    return new InputBuilder($name, $label, 4);
}

function sm9($name, $label) {
    return new InputBuilder($name, $label, 9);
}

function odontogram() {
    $src = base_url() . '/assets/img/odontogram.png';
    echo '<center><img src="' . $src . '" width="400px;"></center>';
}

function br() {
    echo '<br>';
}

function hasil_penunjang($hasil_penunjang) { ?>
    <div class="form-group">
        <label for="hasil_penunjang_laboratorium" class="col-sm-3 control-label">
            Hasil Penunjang
        </label>
    </div>
    <div class="form-group">
        <label for="hasil_penunjang_laboratorium" class="col-sm-3 control-label">
            Laboratorium
        </label>
        <div class="col-sm-9">
            <input type="text" class="form-control" name="hasil_penunjang_laboratorium"
                   id="hasil_penunjang_laboratorium" value="<?=$hasil_penunjang->laboratorium?>">
        </div>
    </div>
    <div class="form-group">
        <label for="hasil_penunjang_laboratorium" class="col-sm-3 control-label">
            EKG
        </label>
        <div class="col-sm-9">
            <input type="text" class="form-control" name="hasil_penunjang_ekg" id="hasil_penunjang_ekg" value="<?=$hasil_penunjang->ekg?>">
        </div>
    </div>
    <div class="form-group">
        <label for="hasil_penunjang_laboratorium" class="col-sm-3 control-label">
            Spirometri
        </label>
        <div class="col-sm-9">
            <input type="text" class="form-control" name="hasil_penunjang_spirometri" id="hasil_penunjang_spirometri"
                   value="<?=$hasil_penunjang->spirometri?>">
        </div>
    </div>
<?php } ?>

<style media="screen">
    .select2-container {
        width: 100% !important;
    }
</style>
<link rel="stylesheet" href="<?= base_url() ?>assets/plugins/iCheck/all.css">
<form class="form-horizontal" method="post" action="<?= base_url() ?>Igd/periksa/<?= $pemeriksaan['id'] ?>">
    <div class="content-wrapper">
        <section class="content-header">
            <h1>
                Pemeriksaan Pasien
                <small>Preview</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="<?= base_url() ?>Dashbord"><i class="fa fa-dashboard"></i> Dashbord</a></li>
                <li class="active">Pemeriksaan</li>
            </ol>
        </section>

        <?php $warning = $this->session->flashdata('warning');
        if (!empty($warning)){ ?>
            <div class="alert alert-warning alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-warning"></i> Alert!</h4>
                <?php echo $warning ?>
            </div>
        <?php } ?>
        <?php $success = $this->session->flashdata('success');
        if (!empty($success)){ ?>
            <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-check"></i> Success!</h4>
                <?php echo $success ?>
            </div>
        <?php } ?>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-7">
                    <div class="box box-danger">
                        <div class="box-header">
                            <h3 class="box-title"> No. Rekam Medis : <?= $pendaftaran['no_rm']; ?> </h3>
                        </div>
                        <div class="box-body">
                            <div class="box-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <?php
                                        unit('td', 'TD')->val($pemeriksaan['td'])->required()->unit('mmHg')->build();
                                        unit('r', 'R')->val($pemeriksaan['r'])->required()->unit('K/Min')->build();
                                        unit('bb', 'BB')->val($pemeriksaan['bb'])->required()->unit('Kg')->onkeyup('set_bmi()')->build();
                                        unit('n', 'N')->val($pemeriksaan['n'])->required()->unit('K/Min')->build();
                                        unit('s', 'S')->val($pemeriksaan['s'])->required()->unit("'0")->build();
                                        unit('tb', 'TB')->val($pemeriksaan['tb'])->required()->unit("cm")->build();
                                        ?>
                                    </div>
                                </div>

                                <input type="hidden" class="form-control" name="pendaftaran_id" id="pendaftran_id"
                                       value="<?= $pemeriksaan['id']; ?>">
                                <input type="hidden" class="form-control" name="dokter_id" id="dokter_id"
                                       value="<?= $pemeriksaan['dokter_id']; ?>">
                                <input type="hidden" class="form-control" name="kode_daftar" id="kode_daftar"
                                       value="<?= $pendaftaran['kode_daftar']; ?>">

                                <?php
                                sm4('bmi', 'BMI')->val($pemeriksaan['bmi'])->build();
                                sm4('no_rm', 'No Rekam Medis')->val($pemeriksaan['no_rm'])->readonly()->build();
                                ?>
                                <div class="form-group">
                                    <label class="col-sm-3"></label>
                                    <div class="col-sm-9">
                                        <span class="label <?= $jaminan[$pemeriksaan['jaminan']]['class'] ?>"><?= $jaminan[$pemeriksaan['jaminan']]['label'] ?></span>
                                        <?php if (!isset($jaminan[$pemeriksaan['jaminan']])) { ?>
                                            <span class="label label-warning">Umum</span>
                                        <?php } ?>
                                        <button type="button" name="button" class="btn btn-sm btn-primary btn-rekamedis"
                                                data-pasien_id="<?= $pendaftaran['pasien'] ?>"><i
                                                class="fa fa-search"></i> Lihat Rekam Medis
                                        </button>
                                    </div>
                                </div>
                                <?php
                                sm4('nama_pasien', 'Nama Pasien')->val($pemeriksaan['nama_pasien'])->readonly()->build();
                                sm9('diagnosa_perawat', 'Diagnosa Perawat')->val($pemeriksaan['diagnosa_perawat'])->build();
                                sm9('keluhan_utama', 'Keluhan Utama')->val($pemeriksaan['keluhan_utama'])->build();
                                sm9('amammesia', 'Anamnesis')->val($pemeriksaan['amammesia'] ?? '')->build();
                                sm9('pemeriksaan_fisik', 'Pemeriksaan Fisik')->val($pemeriksaan['pemeriksaan_fisik'] ?? '')->textarea()->build();
                                hasil_penunjang($hasil_penunjang);
                                sm9('asuhan_keperawatan', 'Catatan Alergi/Lainnya')->val($pemeriksaan['asuhan_keperawatan'])->build();
                                ?>
                                <input type="hidden" name="kode_daftar" value="<?= $pendaftaran['kode_daftar'] ?>">
                                <?php
                                sel('diagnosis_jenis_penyakit[]', 'Diagnosis Jenis Penyakit')
                                    ->placeholder('Pilih penyakit untuk pasien')
                                    ->options($penyakit->result())
                                    ->display(function ($value) {
                                        return $value->kode . ' - ' . $value->nama;
                                    })
                                    ->build();

                                sm9('diagnosis', 'Diagnosis')->val($pemeriksaan['diagnosis'])->build();

                                sel('tindakan[]', 'Tarif / Tindakan')
                                    ->placeholder('Pilih tindakan untuk pasien')
                                    ->options($tindakan->result())
                                    ->display(function ($value) use (&$pendaftaran) {
                                        return $value->nama . " - Rp." . number_format($value->tarif_pasien, 2, ',', '.');
                                    })
                                    ->build();

                                sel('tindakan_operan[]', 'Tarif / Tindakan Operan')
                                    ->placeholder('Pilih tindakan untuk pasien')
                                    ->options($tindakan->result())
                                    ->display(function ($value) use (&$pendaftaran) {
                                        return $value->nama . " - Rp." . number_format($value->tarif_pasien, 2, ',', '.');
                                    })
                                    ->build();
                                ?>
                                <div class="form-group" style=" margin-top: 20px;">
                                    <label for="hasil_penunjang_laboratorium" class="col-sm-4 control-label">
                                        KONSULTASI DOKTER
                                    </label>
                                    <button type="button" class="btn btn-success btn-sm pull-right" id="btn-add-konsul" style="margin-right: 14px">
                                        <span class="fa fa-plus"></span> Tambah Konsul
                                    </button>
                                </div>
                                <div class="row" style="margin-bottom: 14px">
                                    <div class="col-md-12">
                                        <input type="hidden" name="max_konsul" id="max_konsul" value="0">
                                        <table id="tbl-konsul" class="table table-condensed table-bordered" style="margin-bottom: 8px; table-layout:fixed;">
                                            <tr>
                                                <th style="width: 50%;">Tarif Tindakan</th>
                                                <th>Dokter</th>
                                                <th style="width: 46px"></th>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                                <?php
                                sel('perawat[]', 'Perawat')
                                    ->placeholder('Pilih perawat')
                                    ->options($listPerawat->result())
                                    ->display(function ($value) {
                                        return ucwords($value->nama);
                                    })
                                    ->selectedOptionIds(explode(',', $pemeriksaan['detail_perawat_id']))
                                    ->required()
                                    ->build();

                                sel('perawat_operan[]', 'Perawat Operan')
                                    ->placeholder('Pilih perawat Operan')
                                    ->options($listPerawat->result())
                                    ->display(function ($value) {
                                        return ucwords($value->nama);
                                    })
                                    ->build();

                                sm9('deskripsi_tindakan', 'Tata Laksana')->val($pemeriksaan['deskripsi_tindakan'])->build();
                                sm9('saran_pemeriksaan', 'Rujukan')->val($pemeriksaan['saran_pemeriksaan'])->build();
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-12 col-lg-5">
                    <div class="box box-primary">
<!--                        <div class="box-header">-->
<!--                            <h3 class="box-title"></h3>-->
<!--                        </div>-->
                        <div class="box-body">
                            <div class="nav-tabs-custom">
                                <ul class="nav nav-tabs">
<!--                                    <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true">Obat-->
<!--                                            Satuan</a></li>-->
<!--                                    <li class=""><a href="#tab_2" data-toggle="tab" aria-expanded="false">Obat-->
<!--                                            Racik</a></li>-->
                                    <li class="active"><a href="#tab_3" data-toggle="tab" aria-expanded="false">Bahan
                                            Habis Pakai</a></li>
                                </ul>
                                <div class="tab-content">
<!--                                    <div class="tab-pane active" id="tab_1">-->
<!--                                        <div class="row">-->
<!--                                            <div class="col-md-12">-->
<!--                                                <div class="row">-->
<!--                                                    <div class="col-sm-8">-->
<!--                                                        <select id="obat-option">-->
<!--                                                            <option value="">-- Pilih Obat --</option>-->
<!--                                                            --><?php //foreach ($obat1 as $key => $value) { ?>
<!--                                                                <option value="--><?//= $value->id ?><!--">--><?//= $value->nama ?><!--</option>-->
<!--                                                            --><?php //} ?>
<!--                                                        </select>-->
<!--                                                    </div>-->
<!---->
<!--                                                    <div class="col-sm-4">-->
<!--                                                        <button type="button" name="button"-->
<!--                                                                class="btn btn-sm btn-block btn-primary"-->
<!--                                                                id="button-add-obat"><i class="fa fa-plus"></i>-->
<!--                                                            Tambah obat-->
<!--                                                        </button>-->
<!--                                                        <input type="hidden" id="abdush-counter2" value="0">-->
<!--                                                    </div>-->
<!---->
<!--                                                </div>-->
<!---->
<!--                                                <div class="form-area-obat" style="margin-top:15px;">-->
<!--                                                    <table class="table table-striped table-hover">-->
<!--                                                        <thead>-->
<!--                                                        <tr>-->
<!--                                                            <th>Nama</th>-->
<!--                                                            <th>Stok Obat</th>-->
<!--                                                            <th>Jml</th>-->
<!--                                                            <th>Signa</th>-->
<!---->
<!--                                                        </tr>-->
<!--                                                        </thead>-->
<!--                                                        <tbody>-->
<!---->
<!--                                                        </tbody>-->
<!--                                                    </table>-->
<!--                                                </div>-->
<!--                                            </div>-->
<!---->
<!--                                        </div>-->
<!--                                    </div>-->
<!--                                    <div class="tab-pane" id="tab_2">-->
<!--                                        <div class="row">-->
<!--                                            <div class="col-md-12">-->
<!--                                                --><?php //for ($i = 1; $i < 9; $i++) { ?>
<!--                                                    <div class="row ">-->
<!--                                                        <div class="col-md-12">-->
<!--                                                            <div style="margin-top: 20px;">-->
<!--                                                                <label for="obat">Racikan --><?//= $i; ?><!--</label>-->
<!--                                                            </div>-->
<!--                                                            <div class="row">-->
<!--                                                                <div class="col-sm-8">-->
<!--                                                                    <select name="obat_racikan--><?//= $i; ?><!--[]" id="obat-racik-option---><?//=$i?><!--">-->
<!--                                                                        <option value="">-- Pilih Obat --</option>-->
<!--                                                                        --><?php //foreach ($obat1 as $key => $value) { ?>
<!--                                                                            <option value="--><?//= $value->id ?><!--">--><?//= $value->nama ?><!--</option>-->
<!--                                                                        --><?php //} ?>
<!--                                                                    </select>-->
<!--                                                                </div>-->
<!---->
<!--                                                                <div class="col-sm-4">-->
<!--                                                                    <button type="button" name="button"-->
<!--                                                                            class="btn btn-sm btn-block btn-primary"-->
<!--                                                                            id="button-add-obat-racik---><?//=$i?><!--"><i class="fa fa-plus"></i>-->
<!--                                                                        Tambah obat-->
<!--                                                                    </button>-->
<!--                                                                    <input type="hidden" id="abdush-counter---><?//=$i?><!--" value="0">-->
<!--                                                                </div>-->
<!---->
<!--                                                            </div>-->
<!---->
<!--                                                            <div class="form-area-obat-racik---><?//=$i?><!--" style="margin-top:15px;">-->
<!--                                                                <table class="table table-striped table-hover">-->
<!--                                                                    <thead>-->
<!--                                                                    <tr>-->
<!--                                                                        <th>Nama</th>-->
<!--                                                                        <th>Stok Obat</th>-->
<!--                                                                        <th>Jml</th>-->
<!--                                                                    </tr>-->
<!--                                                                    </thead>-->
<!--                                                                    <tbody>-->
<!---->
<!--                                                                    </tbody>-->
<!--                                                                </table>-->
<!--                                                                <input type="text"-->
<!--                                                                       class="form-control"-->
<!--                                                                       id="signa-obat-racik---><?//= $i; ?><!--"-->
<!--                                                                       name="signa--><?//= $i; ?><!--"-->
<!--                                                                       placeholder="signa">-->
<!--                                                                <input type="text"-->
<!--                                                                       class="form-control"-->
<!--                                                                       name="catatan--><?//= $i; ?><!--"-->
<!--                                                                       placeholder="catatan">-->
<!--                                                            </div>-->
<!--                                                        </div>-->
<!---->
<!--                                                    </div>-->
<!--                                                --><?php //} ?>
<!--                                            </div>-->
<!--                                        </div>-->
<!--                                    </div>-->

                                    <div class="tab-pane active" id="tab_3">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="row">
                                                    <div class="col-sm-8">
                                                        <select id="bahan-option">
                                                            <option value="">-- Pilih Bahan --</option>
                                                            <?php foreach ($bahan as $key => $value) { ?>
                                                                <option value="<?= $value->id ?>"><?= $value->nama ?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>

                                                    <div class="col-sm-4">
                                                        <button type="button" name="button"
                                                                class="btn btn-sm btn-block btn-primary"
                                                                id="button-add-bahan"><i class="fa fa-plus"></i>
                                                            Tambah Bahan
                                                        </button>
                                                        <input type="hidden" id="abdush-counter2" value="0">
                                                    </div>

                                                </div>

                                                <div class="form-area" style="margin-top:15px;">
                                                    <table class="table table-striped table-hover">
                                                        <thead>
                                                        <tr>
                                                            <th>Nama Bahan</th>
                                                            <th>Stok</th>
                                                            <th>Jml</th>
                                                            <th></th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>

                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>

                                        </div>
                                    </div>


                                </div>
                            </div>
                        </div>
                        <div class="box-footer">
                            <button type="submit" name="submit" value="1"
                                    class="btn btn-primary btn-lg btn-flat pull-right">Simpan
                            </button>
                            <a href="<?= base_url() ?>Igd/pemeriksaan"
                               class="btn btn-default btn-lg btn-flat pull-right">Batal</a>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-12 col-lg-5">
                    <div class="box box-primary">
                        <div class="box-header">
                            <h3 class="box-title">Permintaan</h3>
                        </div>
                        <div class="box-footer">
                            <button type="submit" name="rujuk" value="lab" class="btn btn-warning btn-block btn-lg">Laboratorium</button>
                            <button type="submit" name="rujuk" value="radio" class="btn btn-danger btn-block btn-lg">Radiologi</button>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</form>

<div id='ResponseInput'></div>

<!-- Select2 -->
<script src="<?= base_url() ?>assets/bower_components/select2/dist/js/select2.full.min.js"></script>
<!-- InputMask -->

<script type="text/javascript">

    function delete_konsul(num) {
        $(`#row-konsul-${num}`).remove();
    }

    $(document).ready(function () {
        let konsul_num = 0;
        $('#btn-add-konsul').click(function () {
            konsul_num++;
            $('#max_konsul').val(konsul_num);
            $('#tbl-konsul tr:last').after(`
                <tr id="row-konsul-${konsul_num}">
                    <td style="width: 50%;">
                        <div class="form-group">
                            <div class="col-sm-12">
                                <select class="form-control select2" multiple="multiple" name="tindakan_${konsul_num}[]" id="tindakan_${konsul_num}"
                                        data-placeholder="Pilih tindakan"
                                        style="width: 100%;" required>
                                        <?php foreach ($tindakan->result() as $key => $value) { ?>
                                            <option value="<?= $value->id; ?>"><?= $value->nama . " - Rp." . number_format($value->tarif_pasien, 2, ',', '.') ?></option>
                                        <?php } ?>
                                </select>
                            </div>
                        </div>
                    </td>
                    <td>
                        <div class="form-group">
                            <div class="col-sm-12">
                                <select class="form-control abdush-select" name="dokter_${konsul_num}" style="width: 100%;" required>
                                    <option value="" selected>-- Pilih Dokter --</option>
                                    <?php foreach ($dokter->result() as $key => $value) { ?>
                                        <option value="<?php echo $value->id ?>"><?php echo $value->nama; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                    </td>
                    <td style="width: 46px">
                        <button type="button" class="btn btn-sm btn-danger" onclick="delete_konsul(${konsul_num})">
                            <span class="fa fa-trash"></span>
                        </button>
                    </td>
                </td>
            `);
            $(`#tindakan_${konsul_num}`).select2();
        })
    })

    $('.select2').select2();

    function set_bmi() {
        var tb = $('#tb').val();
        var bb = $('#bb').val();
        var tbm = tb / 100;
        var bmi = bb / (tbm * tbm);

        $('#bmi').val(bmi.toFixed(2));
    }

    function loadData(i) {
        var id = $('#obat' + i).val();
        var jumlah_satuan = $('#jumlah_satuan' + i).val();
        var stok = $('#stok' + i).val();
        var urls = "<?= base_url(); ?>obat/getStokObat";
        var datax = {"id": id};

        if (parseInt(stok) < parseInt(jumlah_satuan)) {
            alert('Stok obat tidak cukup. Silahkan kurangi jumlah atau ganti obat lain!');
            $('#jumlah_satuan' + i).val('');
        }
    }

    for (var i = 1; i < 11; i++) {
        for (var j = 1; j < 3; j++) {
            function loadDataRacikan(i, j) {
                var ij = (i.toString() + j.toString());


                var id = $('#obat_racikan' + ij).val();
                var jumlah_satuan = $('#jumlah_satuan' + ij).val();
                var urls = "<?= base_url(); ?>obat/getStokObat";
                var datax = {"id": id};

                $.ajax({
                    type: 'GET',
                    url: urls,
                    data: datax,

                    success: function (stok) {
                        if (parseInt(stok) < parseInt(jumlah_satuan)) {
                            alert('Stok obat tidak cukup. Silahkan kurangi jumlah atau ganti obat lain!');
                            $('#jumlah_satuan' + ij).val('');
                        }


                    }
                });
            }
        }
    }

</script>

<script type="text/javascript">
    jQuery(function ($) {
        var bahan = <?php echo json_encode($bahan); ?>;
        $('#bahan-option').select2();

        var obat = <?php echo json_encode($obat1); ?>;
        $('#obat-option').select2();

        $('#button-add-bahan').on('click', function (e) {
            var id_bahan = $('#bahan-option').val();
            if (id_bahan == '') {
                alert('Anda belum memilih bahan habis pakai !');
            }
            else {
                var theBahan = {};
                var counter = parseInt($('#abdush-counter2').val());
                $.each(bahan, function (i, v) {
                    if (v.id == id_bahan) {
                        theBahan = v;
                        return;
                    }
                });

                var html = `
                <tr>
                  <td>
                    ` + theBahan.nama + `
                    <input type="hidden" name="id[]" value="` + theBahan.id + `">
                  </td>
                  <td>` + theBahan.jumlah + ` ` + theBahan.satuan + `</td>
                  <td>
                    <input style="width:65px;" type="text" class="form-control" name="qty[]" id="bahan[` + counter + `][qty]">
                  </td>
                  <td>
                    <button class="btn btn-sm btn-danger btn-delete-row" type="button"> <i class="fa fa-trash"></i> </button>
                  </td>
                </tr>`;

                $('.form-area tbody').append(html);
                $('#abdush-counter').val(counter + 1);
                $('input[name="bahan[' + counter + '][qty]"]').focus();

                $('.btn-delete-row').unbind('click');
                $('.btn-delete-row').each(function () {
                    $(this).on('click', function () {
                        $(this).parents('tr').remove();
                    });
                });
            }

            $('#bahan-option').val('').trigger('change');
        });

        $('#button-add-obat').on('click', function (e) {
            var id_Obat = $('#obat-option').val();
            if (id_Obat == '') {
                alert('Anda belum memilih obat !');
            }
            else {
                var theObat = {};
                var counter = parseInt($('#abdush-counter2').val());
                $.each(obat, function (i, v) {
                    if (v.id == id_Obat) {
                        theObat = v;
                        return;
                    }
                });

                var html = `
                <tr>
                  <td>
                    ` + theObat.nama + `
                    <input type="hidden" name="nama_obat[]" value="` + theObat.id + `">
                  </td>
                  <td>` + theObat.stok_obat + ` item </td>
                  <input type="hidden" id="stok` + counter + `" value="` + theObat.stok_obat + `">
                  <td>
                    <input style="width:65px;" type="text" class="form-control" onchange="loadData(` + counter + `);" name="jumlah_satuan[]" id="jumlah_satuan` + counter + `">
                  </td>
                  <td>
                    <input style="width:100px;" type="text" class="form-control"  name="signa_obat[]" id="signa_obat` + counter + `">
                  </td>
                  <td>
                    <button class="btn btn-sm btn-danger btn-delete-row" type="button"> <i class="fa fa-trash"></i> </button>
                  </td>
                </tr>

                `;

                $('.form-area-obat tbody').append(html);
                $('#abdush-counter2').val(counter + 1);
                $('input[id="jumlah_satuan' + counter + '"]').focus();

                $('.btn-delete-row').unbind('click');
                $('.btn-delete-row').each(function () {
                    $(this).on('click', function () {
                        $(this).parents('tr').remove();
                    });
                });
            }

            $('#obat-option').val('').trigger('change');
        });

        let initializer = {
            init: function() {
                for (let i = 1; i < 9; i++) {
                    $(`#obat-racik-option-${i}`).select2();
                    $(`#button-add-obat-racik-${i}`).on('click', e => {
                        this.onBtnAddClick(i);
                    });
                }
            },
            checkHasObat: function(i) {
                let rowCount = $(`.form-area-obat-racik-${i} >table >tbody >tr`).length;
                if (rowCount === 0) {
                    $(`#signa-obat-racik-${i}`).prop('required',false);
                }
            },
            onBtnAddClick: function(i) {
                let that = this;
                let opt = $(`#obat-racik-option-${i}`);
                let id_Obat = opt.val();
                if (id_Obat === '') {
                    alert('Anda belum memilih obat !');
                }
                else {
                    let theObat = {};
                    let counter = parseInt($(`#abdush-counter-${i}`).val());
                    $.each(obat, function (i, v) {
                        if (v.id === id_Obat) {
                            theObat = v;
                            return;
                        }
                    });

                    $(`.form-area-obat-racik-${i} tbody`).append(this.getTableRow(i, theObat, counter));
                    $(`#signa-obat-racik-${i}`).prop('required',true);
                    $(`#abdush-counter-${i}`).val(counter + 1);
                    $('input[id="jumlah_satuan_racik' + counter + '"]').focus();

                    $('.btn-delete-row').unbind('click');
                    $('.btn-delete-row').each(function () {
                        $(this).on('click', function () {
                            $(this).parents('tr').remove();
                            that.checkHasObat(i);
                        });
                    });
                }
                opt.val('').trigger('change');
            },
            getTableRow: (i, obat, counter) => (`
                <tr>
                  <td>
                    ` + obat.nama + `
                    <input type="hidden" name="nama_obat_racikan${i}[]" value="` + obat.id + `">
                  </td>
                  <td>` + obat.stok_obat + ` item </td>
                  <input type="hidden" id="stok` + counter + `" value="` + obat.stok_obat + `">
                  <td>
                    <input style="width:65px;" type="text" class="form-control" onchange="loadData(` + counter + `);" name="jumlah_satuan_racikan${i}[]" id="jumlah_satuan_racikan${i}` + counter + `">
                  </td>
                  <td>
                    <button class="btn btn-sm btn-danger btn-delete-row" type="button"> <i class="fa fa-trash"></i> </button>
                  </td>
                </tr>
            `)
        };

        initializer.init();
    });
</script>
