<?php

function getKodePoli($kode_daftar = '', $data_poli) {
    foreach ($data_poli as $key => $value) {
        if (in_array($kode_daftar, $value['kode'])) return $key;
    }

    //kalau gak ada ketentuan jadinya umum
    return 'umum';
}

$data_poli = $this->config->item('poli');;
$kode_poli = getKodePoli($pendaftaran['kode_daftar'], $data_poli);

include BASEPATH.'../application/views/template/InputBuilder.php';

?>

<style media="screen">
    .select2-container {
        width: 100% !important;
    }
</style>
<link rel="stylesheet" href="<?= base_url() ?>assets/plugins/iCheck/all.css">
<form class="form-horizontal" method="post" action="<?= base_url() ?>Igd/periksa/<?= $pemeriksaan['id'] ?>">
    <div class="content-wrapper">
        <section class="content-header">
            <h1>
                Pemeriksaan Pasien
                <small>Preview</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="<?= base_url() ?>Dashbord"><i class="fa fa-dashboard"></i> Dashbord</a></li>
                <li class="active">Pemeriksaan</li>
            </ol>
        </section>

        <?php $warning = $this->session->flashdata('warning');
        if (!empty($warning)){ ?>
            <div class="alert alert-warning alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-warning"></i> Alert!</h4>
                <?php echo $warning ?>
            </div>
        <?php } ?>
        <?php $success = $this->session->flashdata('success');
        if (!empty($success)){ ?>
            <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-check"></i> Success!</h4>
                <?php echo $success ?>
            </div>
        <?php } ?>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-7">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="box box-danger">
                                <div class="box-header">
                                    <h3 class="box-title"> No. Rekam Medis : <?= $pendaftaran['no_rm']; ?> </h3>
                                </div>
                                <div class="box-body">
                                    <div class="box-body">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <?php
                                                unit('td', 'TD')->val($pemeriksaan['td'])->required()->unit('mmHg')->build();
                                                unit('r', 'R')->val($pemeriksaan['r'])->required()->unit('K/Min')->build();
                                                unit('bb', 'BB')->val($pemeriksaan['bb'])->required()->unit('Kg')->onkeyup('set_bmi()')->build();
                                                unit('n', 'N')->val($pemeriksaan['n'])->required()->unit('K/Min')->build();
                                                unit('s', 'S')->val($pemeriksaan['s'])->required()->unit("'0")->build();
                                                unit('tb', 'TB')->val($pemeriksaan['tb'])->required()->unit("cm")->build();
                                                unit('spo2', 'Spo2')->val($pemeriksaan['spo2'])->required()->unit("%")->build();
                                                ?>
                                            </div>
                                        </div>

                                        <input type="hidden" class="form-control" name="pendaftaran_id" id="pendaftran_id"
                                               value="<?= $pemeriksaan['pendaftaran_id']; ?>">
                                        <input type="hidden" class="form-control" name="dokter_id" id="dokter_id"
                                               value="<?= $pemeriksaan['dokter_id']; ?>">
                                        <input type="hidden" class="form-control" name="kode_daftar" id="kode_daftar"
                                               value="<?= $pendaftaran['kode_daftar']; ?>">

                                        <?php
                                        sm4('bmi', 'BMI')->val($pemeriksaan['bmi'])->build();
                                        sm4('no_rm', 'No Rekam Medis')->val($pemeriksaan['no_rm'])->readonly()->build();
                                        ?>
                                        <div class="form-group">
                                            <label class="col-sm-3"></label>
                                            <div class="col-sm-9">
                                                <span class="label <?= $jaminan[$pemeriksaan['jaminan']]['class'] ?>"><?= $jaminan[$pemeriksaan['jaminan']]['label'] ?></span>
                                                <?php if (!isset($jaminan[$pemeriksaan['jaminan']])) { ?>
                                                    <span class="label label-warning">Umum</span>
                                                <?php } ?>
                                                <button type="button" name="button" class="btn btn-sm btn-primary btn-rekamedis"
                                                        data-pasien_id="<?= $pendaftaran['pasien'] ?>"><i
                                                            class="fa fa-search"></i> Lihat Rekam Medis
                                                </button>
                                            </div>
                                        </div>
                                        <?php
                                        sm4('nama_pasien', 'Nama Pasien')->val($pemeriksaan['nama_pasien'])->readonly()->build();
                                        sm4('usia', 'Usia')->val($pendaftaran['usia'])->readonly()->build();
                                        sm4('alamat', 'Alamat')->val($pendaftaran['alamat'])->readonly()->build();
                                        ?>
                                        <!--                                form perawat-->
                                        <div>
                                            <h4><strong>PENGKAJIAN INSTALASI GAWAT DARURAT</strong></h4>
                                            <div class="form-group form-horizontal" style="display: flex; align-items: center; margin-left: 0px">
                                                <label>Jam</label>
                                                <input style="margin-left: 10px; width: 100px" type="time" class="form-control" name="form[igd][jam]" value="<?=$form['igd']['jam'] ?? date('H:i')?>">
                                            </div>
                                            <p style="margin-top: 10px; margin-bottom: 0">Tingkat kegawatan</p>
                                            <div class="form-check" style="display: inline; padding-right: 10px">
                                                <input class="form-check-input" type="radio" name="form[igd][kegawatan]" value="Merah" id="kegawatan1" <?= isset($form['igd']['kegawatan']) && $form['igd']['kegawatan'] == 'Merah' ? 'checked' : '' ?>>
                                                <label class="form-check-label" for="kegawatan1" style="font-weight: normal !important;">
                                                    Merah
                                                </label>
                                            </div>
                                            <div class="form-check" style="display: inline; padding-right: 10px">
                                                <input class="form-check-input" type="radio" name="form[igd][kegawatan]" value="Kuning" id="kegawatan2" <?= isset($form['igd']['kegawatan']) && $form['igd']['kegawatan'] == 'Kuning' ? 'checked' : '' ?>>
                                                <label class="form-check-label" for="kegawatan2" style="font-weight: normal !important;">
                                                    Kuning
                                                </label>
                                            </div>
                                            <div class="form-check" style="display: inline; padding-right: 10px">
                                                <input class="form-check-input" type="radio" name="form[igd][kegawatan]" value="Hijau" id="kegawatan3" <?= isset($form['igd']['kegawatan']) && $form['igd']['kegawatan'] == 'Hijau' ? 'checked' : '' ?>>
                                                <label class="form-check-label" for="kegawatan3" style="font-weight: normal !important;">
                                                    Hijau
                                                </label>
                                            </div>
                                            <div class="form-check" style="display: inline">
                                                <input class="form-check-input" type="radio" name="form[igd][kegawatan]" value="Hitam" id="kegawatan4" <?= isset($form['igd']['kegawatan']) && $form['igd']['kegawatan'] == 'Hitam' ? 'checked' : '' ?>>
                                                <label class="form-check-label" for="kegawatan4" style="font-weight: normal !important;">
                                                    Hitam
                                                </label>
                                            </div>
                                            <p style="margin-top: 10px; margin-bottom: 0">Jenis Pelayanan</p>
                                            <div class="form-check" style="display: inline; padding-right: 10px">
                                                <input class="form-check-input" type="radio" name="form[igd][pelayanan]" value="Bedah" id="pelayanan1" <?= isset($form['igd']['pelayanan']) && $form['igd']['pelayanan'] == 'Bedah' ? 'checked' : '' ?>>
                                                <label class="form-check-label" for="pelayanan1" style="font-weight: normal !important;">Bedah</label>
                                            </div>
                                            <div class="form-check" style="display: inline; padding-right: 10px">
                                                <input class="form-check-input" type="radio" name="form[igd][pelayanan]" value="Non Bedah" id="pelayanan2" <?= isset($form['igd']['pelayanan']) && $form['igd']['pelayanan'] == 'Non Bedah' ? 'checked' : '' ?>>
                                                <label class="form-check-label" for="pelayanan2" style="font-weight: normal !important;">Non Bedah</label>
                                            </div>
                                            <div class="form-check" style="display: inline; padding-right: 10px">
                                                <input class="form-check-input" type="radio" name="form[igd][pelayanan]" value="Kebidanan" id="pelayanan3" <?= isset($form['igd']['pelayanan']) && $form['igd']['pelayanan'] == 'Kebidanan' ? 'checked' : '' ?>>
                                                <label class="form-check-label" for="pelayanan3" style="font-weight: normal !important;">Kebidanan</label>
                                            </div>
                                            <div class="form-check" style="display: inline">
                                                <input class="form-check-input" type="radio" name="form[igd][pelayanan]" value="Anak" id="pelayanan4" <?= isset($form['igd']['pelayanan']) && $form['igd']['pelayanan'] == 'Anak' ? 'checked' : '' ?>>
                                                <label class="form-check-label" for="pelayanan4" style="font-weight: normal !important;">Anak</label>
                                            </div>
                                            <p style="margin-top: 10px; margin-bottom: 0">Alasan Datang</p>
                                            <div class="form-check" style="display: inline; padding-right: 10px">
                                                <input class="form-check-input" type="radio" name="form[igd][alasan_datang]" value="Penyakit" id="alasan_datang1" <?= isset($form['igd']['alasan_datang']) && $form['igd']['alasan_datang'] == 'Penyakit' ? 'checked' : '' ?>>
                                                <label class="form-check-label" for="alasan_datang1" style="font-weight: normal !important;">Penyakit</label>
                                            </div>
                                            <div class="form-check" style="display: inline">
                                                <input class="form-check-input" type="radio" name="form[igd][alasan_datang]" value="Trauma / Ruda Paksa" id="alasan_datang4" <?= isset($form['igd']['alasan_datang']) && $form['igd']['alasan_datang'] == 'Trauma / Ruda Paksa' ? 'checked' : '' ?>>
                                                <label class="form-check-label" for="alasan_datang4" style="font-weight: normal !important;">Trauma / Ruda Paksa</label>
                                            </div>
                                            <p style="margin-top: 10px; margin-bottom: 0">Cara Masuk</p>
                                            <div class="form-check" style="display: inline; padding-right: 10px">
                                                <input class="form-check-input" type="radio" name="form[igd][cara_masuk]" value="Sendiri" id="cara_masuk1" <?= isset($form['igd']['cara_masuk']) && $form['igd']['cara_masuk'] == 'Sendiri' ? 'checked' : '' ?>>
                                                <label class="form-check-label" for="cara_masuk1" style="font-weight: normal !important;">Sendiri</label>
                                            </div>
                                            <div class="form-check" style="display: inline">
                                                <input class="form-check-input" type="radio" name="form[igd][cara_masuk]" value="Rujukan" id="cara_masuk4" <?= isset($form['igd']['cara_masuk']) && $form['igd']['cara_masuk'] == 'Rujukan' ? 'checked' : '' ?>>
                                                <label class="form-check-label" for="cara_masuk4" style="font-weight: normal !important; padding-right: 10px">Rujukan</label>
                                                <input type="text" class="form-control" style="display: inline; width: 250px" placeholder="Rujukan" name="form[igd][cara_masuk_rujukan]" value="<?= $form['igd']['cara_masuk_rujukan'] ?? '' ?>">
                                            </div>
                                            <hr>
                                        </div>
                                        <div>
                                            <h4><strong>Alergi</strong></h4>
                                            <p>Apakah pasien mempunyai indikasi alergi ?</p>
                                            <div class="form-check" style="display: inline">
                                                <input class="form-check-input" type="radio" name="form[alergi]" value="ya" id="flexRadioDefault1" <?= isset($form['alergi']) && $form['alergi'] == 'ya' ? 'checked' : '' ?>>
                                                <label class="form-check-label" for="flexRadioDefault1" style="font-weight: normal !important;">
                                                    Ya
                                                </label>
                                            </div>
                                            <div class="form-check" style="display: inline; margin-left: 16px">
                                                <input class="form-check-input" type="radio" name="form[alergi]" value="tidak" id="flexRadioDefault2" <?= isset($form['alergi']) && $form['alergi'] == 'tidak' ? 'checked' : '' ?>>
                                                <label class="form-check-label" for="flexRadioDefault2" style="font-weight: normal !important;">
                                                    Tidak
                                                </label>
                                            </div>
                                            <?php sm9('form[alergi_detail]', 'Jika Ya, Jelaskan')->break_label()->textarea()->val($form['alergi_detail'] ?? '')->build(); ?>
                                        </div>
                                        <div style="margin-top: 24px">
                                            <h4><strong>Nyeri</strong></h4>
                                            <p style="margin-bottom: 2px !important;">Skala Numerik</p>
                                            <div style="display: flex; width: 100%;">
                                                <div style="flex: 1; display: flex; flex-direction: column; align-items: center">
                                                    <p style="margin-bottom: 0 !important;">1</p>
                                                    <input class="form-check-input" type="radio" name="form[nyeri]" value="1" style="margin-top: 0 !important;" <?= isset($form['nyeri']) && $form['nyeri'] == '1' ? 'checked' : '' ?>>
                                                </div>
                                                <div style="flex: 1; display: flex; flex-direction: column; align-items: center">
                                                    <p style="margin-bottom: 0 !important;">2</p>
                                                    <input class="form-check-input" type="radio" name="form[nyeri]" value="2" style="margin-top: 0 !important;" <?= isset($form['nyeri']) && $form['nyeri'] == '2' ? 'checked' : '' ?>>
                                                </div>
                                                <div style="flex: 1; display: flex; flex-direction: column; align-items: center">
                                                    <p style="margin-bottom: 0 !important;">3</p>
                                                    <input class="form-check-input" type="radio" name="form[nyeri]" value="3" style="margin-top: 0 !important;" <?= isset($form['nyeri']) && $form['nyeri'] == '3' ? 'checked' : '' ?>>
                                                </div>
                                                <div style="flex: 1; display: flex; flex-direction: column; align-items: center">
                                                    <p style="margin-bottom: 0 !important;">4</p>
                                                    <input class="form-check-input" type="radio" name="form[nyeri]" value="4" style="margin-top: 0 !important;" <?= isset($form['nyeri']) && $form['nyeri'] == '4' ? 'checked' : '' ?>>
                                                </div>
                                                <div style="flex: 1; display: flex; flex-direction: column; align-items: center">
                                                    <p style="margin-bottom: 0 !important;">5</p>
                                                    <input class="form-check-input" type="radio" name="form[nyeri]" value="5" style="margin-top: 0 !important;" <?= isset($form['nyeri']) && $form['nyeri'] == '5' ? 'checked' : '' ?>>
                                                </div>
                                                <div style="flex: 1; display: flex; flex-direction: column; align-items: center">
                                                    <p style="margin-bottom: 0 !important;">6</p>
                                                    <input class="form-check-input" type="radio" name="form[nyeri]" value="6" style="margin-top: 0 !important;" <?= isset($form['nyeri']) && $form['nyeri'] == '6' ? 'checked' : '' ?>>
                                                </div>
                                                <div style="flex: 1; display: flex; flex-direction: column; align-items: center">
                                                    <p style="margin-bottom: 0 !important;">7</p>
                                                    <input class="form-check-input" type="radio" name="form[nyeri]" value="7" style="margin-top: 0 !important;" <?= isset($form['nyeri']) && $form['nyeri'] == '7' ? 'checked' : '' ?>>
                                                </div>
                                                <div style="flex: 1; display: flex; flex-direction: column; align-items: center">
                                                    <p style="margin-bottom: 0 !important;">8</p>
                                                    <input class="form-check-input" type="radio" name="form[nyeri]" value="8" style="margin-top: 0 !important;" <?= isset($form['nyeri']) && $form['nyeri'] == '8' ? 'checked' : '' ?>>
                                                </div>
                                                <div style="flex: 1; display: flex; flex-direction: column; align-items: center">
                                                    <p style="margin-bottom: 0 !important;">9</p>
                                                    <input class="form-check-input" type="radio" name="form[nyeri]" value="9" style="margin-top: 0 !important;" <?= isset($form['nyeri']) && $form['nyeri'] == '9' ? 'checked' : '' ?>>
                                                </div>
                                                <div style="flex: 1; display: flex; flex-direction: column; align-items: center">
                                                    <p style="margin-bottom: 0 !important;">10</p>
                                                    <input class="form-check-input" type="radio" name="form[nyeri]" value="10" style="margin-top: 0 !important;" <?= isset($form['nyeri']) && $form['nyeri'] == '10' ? 'checked' : '' ?>>
                                                </div>
                                            </div>
                                            <p style="margin-bottom: 2px !important; margin-top: 8px !important;">Wong Baker Faces</p>
                                            <div style="display: flex; flex-direction: column">
                                                <img src="<?php echo base_url(); ?>assets/img/wong.jpeg" alt="User Image">
                                                <div style="display: flex; flex: 1; margin-left: 6px; margin-right: 8px;">
                                                    <input class="form-check-input" type="radio" name="form[wong]" value="0" style="margin-top: 0 !important; flex: 1" <?= isset($form['wong']) && $form['wong'] == '0' ? 'checked' : '' ?>>
                                                    <input class="form-check-input" type="radio" name="form[wong]" value="2" style="margin-top: 0 !important; flex: 1" <?= isset($form['wong']) && $form['wong'] == '2' ? 'checked' : '' ?>>
                                                    <input class="form-check-input" type="radio" name="form[wong]" value="4" style="margin-top: 0 !important; flex: 1" <?= isset($form['wong']) && $form['wong'] == '4' ? 'checked' : '' ?>>
                                                    <input class="form-check-input" type="radio" name="form[wong]" value="6" style="margin-top: 0 !important; flex: 1" <?= isset($form['wong']) && $form['wong'] == '6' ? 'checked' : '' ?>>
                                                    <input class="form-check-input" type="radio" name="form[wong]" value="8" style="margin-top: 0 !important; flex: 1" <?= isset($form['wong']) && $form['wong'] == '8' ? 'checked' : '' ?>>
                                                    <input class="form-check-input" type="radio" name="form[wong]" value="10" style="margin-top: 0 !important; flex: 1" <?= isset($form['wong']) && $form['wong'] == '10' ? 'checked' : '' ?>>
                                                </div>
                                            </div>
                                        </div>
                                        <div style="margin-top: 24px">
                                            <h4><strong>Gizi</strong></h4>
                                            <div class="col-sm-10 form-group">
                                                <label for="td" class="col-sm-6 control-label">Tinggi Badan</label>
                                                <div class="input-group col-sm-4">
                                                    <input type="text" class="form-control" name="form[tb]" id="form_tb" onkeyup="setImt()" autocomplete="off" value="<?=$form['tb'] ?? ''?>">
                                                    <span class="input-group-addon">cm</span>
                                                </div>
                                            </div>
                                            <div class="col-sm-10 form-group">
                                                <label for="td" class="col-sm-6 control-label">Berat Badan</label>
                                                <div class="input-group col-sm-4">
                                                    <input type="text" class="form-control" name="form[bb]" id="form_bb" onkeyup="setImt()" autocomplete="off" value="<?=$form['bb'] ?? ''?>">
                                                    <span class="input-group-addon">kg</span>
                                                </div>
                                            </div>
                                            <div class="col-sm-10 form-group">
                                                <label for="td" class="col-sm-6 control-label">Indeks Massa Tubuh (IMT)</label>
                                                <div class="input-group col-sm-4">
                                                    <input type="text" class="form-control" name="form[imt]" id="form_imt" autocomplete="off" value="<?=$form['imt'] ?? ''?>">
                                                    <span class="input-group-addon">kg/m2</span>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <div style="display: flex; flex-direction: column">
                                                        <div style="display: flex">
                                                            <div class="checkbox" style="flex: 1">
                                                                <label>
                                                                    <input type="radio" name="form[gizi]" value="underweight" id="underweight" <?=isset($form['gizi']) && $form['gizi'] == 'underweight' ? 'checked' : ''?>> Underweight ( < 18.5 )
                                                                </label>
                                                            </div>
                                                            <div class="checkbox" style="flex: 1">
                                                                <label>
                                                                    <input type="radio" name="form[gizi]" value="overweight" id="overweight" <?=isset($form['gizi']) && $form['gizi'] == 'overweight' ? 'checked' : ''?>> Overweight (25 – 29.99)
                                                                </label>
                                                            </div>
                                                        </div>
                                                        <div style="display: flex">
                                                            <div class="checkbox" style="flex: 1">
                                                                <label>
                                                                    <input type="radio" name="form[gizi]" value="normal" id="normal" <?=isset($form['gizi']) && $form['gizi'] == 'normal' ? 'checked' : ''?>> Normal ( 18.5 – 24.99)
                                                                </label>
                                                            </div>
                                                            <div class="checkbox" style="flex: 1">
                                                                <label>
                                                                    <input type="radio" name="form[gizi]" value="obese" id="obese" <?=isset($form['gizi']) && $form['gizi'] == 'obese' ? 'checked' : ''?>> Obese (≥ 30)
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div style="margin-top: 24px">
                                            <h4><strong>SKRINING RESIKO JATUH</strong></h4>
                                            <div style="display: flex">
                                                <div>
                                                    <p style="margin-bottom: 2px !important;">Cara berjalan pasien (salah satu/lebih)</p>
                                                </div>
                                            </div>
                                            <p style="margin-bottom: 2px !important;">a. Tidak seimbang/sempoyongan/limbung</p>
                                            <div class="checkbox" style="display: inline">
                                                <label>
                                                    <input type="radio" name="form[tidak_seimbang]" value="ya" <?=isset($form['tidak_seimbang']) && $form['tidak_seimbang'] == 'ya' ? 'checked' : ''?>> Ya
                                                </label>
                                            </div>
                                            <div class="checkbox" style="display: inline">
                                                <label>
                                                    <input type="radio" name="form[tidak_seimbang]" value="tidak" <?=isset($form['tidak_seimbang']) && $form['tidak_seimbang'] == 'tidak' ? 'checked' : ''?>> Tidak
                                                </label>
                                            </div>
                                            <br>
                                            <p style="margin-bottom: 2px !important;">b. Jalan dengan alat bantu (kruk, tripod, kursi roda, orang lain)</p>
                                            <div class="checkbox" style="display: inline">
                                                <label>
                                                    <input type="radio" name="form[alat_bantu]" value="ya" <?=isset($form['alat_bantu']) && $form['alat_bantu'] == 'ya' ? 'checked' : ''?>> Ya
                                                </label>
                                            </div>
                                            <div class="checkbox" style="display: inline">
                                                <label>
                                                    <input type="radio" name="form[alat_bantu]" value="tidak" <?=isset($form['alat_bantu']) && $form['alat_bantu'] == 'tidak' ? 'checked' : ''?>> Tidak
                                                </label>
                                            </div>
                                            <br>
                                            <p style="margin-bottom: 2px !important;">Ada keterbatasan gerak ?</p>
                                            <div class="checkbox" style="display: inline">
                                                <label>
                                                    <input type="radio" name="form[skrining_keterbatasan_gerak]" value="ya" <?=isset($form['skrining_keterbatasan_gerak']) && $form['skrining_keterbatasan_gerak'] == 'ya' ? 'checked' : ''?>> Ya
                                                </label>
                                            </div>
                                            <div class="checkbox" style="display: inline">
                                                <label>
                                                    <input type="radio" name="form[skrining_keterbatasan_gerak]" value="tidak" <?=isset($form['skrining_keterbatasan_gerak']) && $form['skrining_keterbatasan_gerak'] == 'tidak' ? 'checked' : ''?>> Tidak
                                                </label>
                                            </div>
                                        </div>
                                        <hr>
                                        <div>
                                            <?php
                                            sm9('diagnosa_perawat', 'Diagnosa Perawat')->textarea(3)->val($pemeriksaan['diagnosa_perawat'] ?? '')->build();
                                            sm9('keluhan_utama', 'Keluhan Utama')->textarea(3)->val($pemeriksaan['keluhan_utama'] ?? '')->build();
                                            sm9('asuhan_keperawatan', 'Catatan Alergi/Lainnya')->textarea(3)->val($pemeriksaan['asuhan_keperawatan'] ?? '')->build();
                                            sm9('form[tindakan_perawat]', 'Tindakan Perawat')->textarea(3)->val($form['tindakan_perawat'] ?? '')->build();

                                            sm9('form[daftar_masalah_medis]', 'Daftar Masalah Medis')->val($form['daftar_masalah_medis'] ?? '')->hide()->build();
                                            sm9('form[dsdok]', 'Data Subyektif dan Obyektif Keperawatan')->val($form['dsdok'] ?? '')->hide()->build();
                                            sm9('form[dmk]', 'Daftar Masalah Keperawatan')->val($form['dmk'] ?? '')->hide()->build();
                                            ?>
                                        </div>
                                        <!--                                end form perawat-->
                                        <?php
                                        //                                sm9('diagnosa_perawat', 'Diagnosa Perawat')->val($pemeriksaan['diagnosa_perawat'])->build();
                                        //                                sm9('amammesia', 'Anamnesis')->build();
                                        //                                sm9('pemeriksaan_fisik', 'Pemeriksaan Fisik')->textarea()->build();
                                        //                                hasil_penunjang();
                                        //                                sm9('asuhan_keperawatan', 'Catatan Alergi/Lainnya')->val($pemeriksaan['asuhan_keperawatan'])->build();
                                        ?>
                                        <input type="hidden" name="kode_daftar" value="<?= $pendaftaran['kode_daftar'] ?>">

                                        <div style="margin-top: 24px">
                                            <h3><strong>SURVEY PRIMER</strong></h3>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <?php
                                                    sm9('form[mulai_diperiksa_tanggal]', 'Mulai diperiksa di ruang label tanggal')
                                                        ->val($form['mulai_diperiksa_tanggal'] ?? '')
                                                        ->break_label()
                                                        ->build();
                                                    ?>
                                                </div>
                                                <div class="col-md-6">
                                                    <?php
                                                    sm9('form[mulai_diperiksa_jam]', 'Jam')
                                                        ->val($form['mulai_diperiksa_jam'] ?? '')
                                                        ->break_label()
                                                        ->build();
                                                    ?>
                                                </div>
                                            </div>
                                            <?php
                                            sm9('form[survey_primer]', 'Survey Primer')
                                                ->val($form['survey_primer'] ?? '')
                                                ->textarea()
                                                ->break_label()
                                                ->build();
                                            ?>
                                            <h4><strong>Pemeriksaan Fisik</strong></h4>
                                            <p style="margin-bottom: 2px !important;"><strong>Air Way</strong></p>
                                            <p style="margin-bottom: 2px !important;"><strong>Jalan Nafas</strong></p>
                                            <div class="checkbox" style="display: inline">
                                                <label>
                                                    <input type="radio" name="form[jalan_nafas]" value="Bebas" <?=isset($form['jalan_nafas']) && $form['jalan_nafas'] == 'Bebas' ? 'checked' : ''?>> Bebas
                                                </label>
                                            </div>
                                            <div class="checkbox" style="display: inline">
                                                <label>
                                                    <input type="radio" name="form[jalan_nafas]" value="Tersumbat sebagian" <?=isset($form['jalan_nafas']) && $form['jalan_nafas'] == 'Tersumbat sebagian' ? 'checked' : ''?>> Tersumbat sebagian
                                                </label>
                                            </div>
                                            <div class="checkbox" style="display: inline">
                                                <label>
                                                    <input type="radio" name="form[jalan_nafas]" value="Tersumbat total" <?=isset($form['jalan_nafas']) && $form['jalan_nafas'] == 'Tersumbat total' ? 'checked' : ''?>> Tersumbat total
                                                </label>
                                            </div>
                                            <br>
                                            <br>
                                            <p style="margin-bottom: 2px !important;"><strong>Breathing</strong></p>
                                            <p style="margin-bottom: 2px !important;"><strong>Pengembangan Paru</strong></p>
                                            <div class="checkbox" style="display: inline">
                                                <label>
                                                    <input type="radio" name="form[pengembangan_paru]" value="Simetris" <?=isset($form['pengembangan_paru']) && $form['pengembangan_paru'] == 'Simetris' ? 'checked' : ''?>> Simetris
                                                </label>
                                            </div>
                                            <div class="checkbox" style="display: inline">
                                                <label>
                                                    <input type="radio" name="form[pengembangan_paru]" value="Asimetris" <?=isset($form['pengembangan_paru']) && $form['pengembangan_paru'] == 'Asimetris' ? 'checked' : ''?>> Asimetris
                                                </label>
                                            </div>
                                            <br>
                                            <br>
                                            <p style="margin-bottom: 2px !important;"><strong>Pola Nafas</strong></p>
                                            <div style="width: 100%; display: flex">
                                                <div>
                                                    <div class="checkbox" style="display: inline">
                                                        <label>
                                                            <input type="radio" name="form[pola_nafas]" value="Normal/Eupnea"
                                                                <?=isset($form['pola_nafas']) && $form['pola_nafas'] == 'Normal/Eupnea' ? 'checked' : ''?>> Normal/Eupnea
                                                        </label>
                                                    </div>
                                                    <div class="checkbox" style="display: inline">
                                                        <label>
                                                            <input type="radio" name="form[pola_nafas]" value="Bradipnoe"
                                                                <?=isset($form['pola_nafas']) && $form['pola_nafas'] == 'Bradipnoe' ? 'checked' : ''?>> Bradipnoe
                                                        </label>
                                                    </div>
                                                </div>
                                                <div>
                                                    <div class="checkbox" style="display: inline">
                                                        <label>
                                                            <input type="radio" name="form[pola_nafas]" value="Tachipnoe"
                                                                <?=isset($form['pola_nafas']) && $form['pola_nafas'] == 'Tachipnoe' ? 'checked' : ''?>> Tachipnoe
                                                        </label>
                                                    </div>
                                                    <div class="checkbox" style="display: inline">
                                                        <label>
                                                            <input type="radio" name="form[pola_nafas]" value="Dispnoe"
                                                                <?=isset($form['pola_nafas']) && $form['pola_nafas'] == 'Dispnoe' ? 'checked' : ''?>> Dispnoe
                                                        </label>
                                                    </div>
                                                </div>
                                                <div>
                                                    <div class="checkbox" style="display: inline">
                                                        <label>
                                                            <input type="radio" name="form[pola_nafas]" value="Apnoe"
                                                                <?=isset($form['pola_nafas']) && $form['pola_nafas'] == 'Apnoe' ? 'checked' : ''?>> Apnoe
                                                        </label>
                                                    </div>
                                                    <div class="checkbox" style="display: inline">
                                                        <label>
                                                            <input type="radio" name="form[pola_nafas]" value="Cheynestoke"
                                                                <?=isset($form['pola_nafas']) && $form['pola_nafas'] == 'Cheynestoke' ? 'checked' : ''?>> Cheynestoke
                                                        </label>
                                                    </div>
                                                </div>
                                                <div>
                                                    <div class="checkbox" style="display: inline">
                                                        <label>
                                                            <input type="radio" name="form[pola_nafas]" value="Kussmaul"
                                                                <?=isset($form['pola_nafas']) && $form['pola_nafas'] == 'Kussmaul' ? 'checked' : ''?>> Kussmaul
                                                        </label>
                                                    </div>
                                                    <div class="checkbox" style="display: inline">
                                                        <label>
                                                            <input type="radio" name="form[pola_nafas]" value="Biot"
                                                                <?=isset($form['pola_nafas']) && $form['pola_nafas'] == 'Biot' ? 'checked' : ''?>> Biot
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <br>
                                            <p style="margin-bottom: 2px !important;"><strong>Suara Nafas</strong></p>
                                            <div class="checkbox" style="display: inline">
                                                <label>
                                                    <input type="radio" name="form[suara_nafas]" value="Vesikuler" <?=isset($form['suara_nafas']) && $form['suara_nafas'] == 'Vesikuler' ? 'checked' : ''?>> Vesikuler
                                                </label>
                                            </div>
                                            <div class="checkbox" style="display: inline">
                                                <label>
                                                    <input type="radio" name="form[suara_nafas]" value="Ronchi" <?=isset($form['suara_nafas']) && $form['suara_nafas'] == 'Ronchi' ? 'checked' : ''?>> Ronchi
                                                </label>
                                            </div>
                                            <div class="checkbox" style="display: inline">
                                                <label>
                                                    <input type="radio" name="form[suara_nafas]" value="Wheezing" <?=isset($form['suara_nafas']) && $form['suara_nafas'] == 'Wheezing' ? 'checked' : ''?>> Wheezing
                                                </label>
                                            </div>
                                            <div class="checkbox" style="display: inline">
                                                <label>
                                                    <input type="radio" name="form[suara_nafas]" value="Stridor" <?=isset($form['suara_nafas']) && $form['suara_nafas'] == 'Stridor' ? 'checked' : ''?>> Stridor
                                                </label>
                                            </div>
                                            <br>
                                            <br>
                                            <p style="margin-bottom: 2px !important;"><strong>Circulation</strong></p>
                                            <p style="margin-bottom: 2px !important;"><strong>Kulit</strong></p>
                                            <div class="checkbox" style="display: inline">
                                                <label>
                                                    <input type="radio" name="form[kulit]" value="Normal" <?=isset($form['kulit']) && $form['kulit'] == 'Normal' ? 'checked' : ''?>> Normal
                                                </label>
                                            </div>
                                            <div class="checkbox" style="display: inline">
                                                <label>
                                                    <input type="radio" name="form[kulit]" value="Pucat" <?=isset($form['kulit']) && $form['kulit'] == 'Pucat' ? 'checked' : ''?>> Pucat
                                                </label>
                                            </div>
                                            <br>
                                            <br>
                                            <p style="margin-bottom: 2px !important;"><strong>Warna Membran</strong></p>
                                            <div class="checkbox" style="display: inline">
                                                <label>
                                                    <input type="radio" name="form[warna_membran]" value="Merah" <?=isset($form['warna_membran']) && $form['warna_membran'] == 'Merah' ? 'checked' : ''?>> Merah
                                                </label>
                                            </div>
                                            <div class="checkbox" style="display: inline">
                                                <label>
                                                    <input type="radio" name="form[warna_membran]" value="Abu - Abu" <?=isset($form['warna_membran']) && $form['warna_membran'] == 'Abu - Abu' ? 'checked' : ''?>> Abu - Abu
                                                </label>
                                            </div>
                                            <br>
                                            <br>
                                            <p style="margin-bottom: 2px !important;"><strong>Akral Kulit</strong></p>
                                            <div class="checkbox" style="display: inline">
                                                <label>
                                                    <input type="radio" name="form[akral]" value="Hangat" <?=isset($form['akral']) && $form['akral'] == 'Hangat' ? 'checked' : ''?>> Hangat
                                                </label>
                                            </div>
                                            <div class="checkbox" style="display: inline">
                                                <label>
                                                    <input type="radio" name="form[akral]" value="Dingin" <?=isset($form['akral']) && $form['akral'] == 'Dingin' ? 'checked' : ''?>> Dingin
                                                </label>
                                            </div>
                                            <br>
                                            <br>
                                            <p style="margin-bottom: 2px !important;"><strong>Mukosa</strong></p>
                                            <div class="checkbox" style="display: inline">
                                                <label>
                                                    <input type="radio" name="form[mukosa]" value="Basah" <?=isset($form['mukosa']) && $form['mukosa'] == 'Basah' ? 'checked' : ''?>> Basah
                                                </label>
                                            </div>
                                            <div class="checkbox" style="display: inline">
                                                <label>
                                                    <input type="radio" name="form[mukosa]" value="Kering" <?=isset($form['mukosa']) && $form['mukosa'] == 'Kering' ? 'checked' : ''?>> Kering
                                                </label>
                                            </div>
                                            <br>
                                            <br>
                                            <p style="margin-bottom: 2px !important;"><strong>Pengisian Kapiler / CRT</strong></p>
                                            <div class="checkbox" style="display: inline">
                                                <label>
                                                    <input type="radio" name="form[kapiler_crt]" value="kurang_2" <?=isset($form['kapiler_crt']) && $form['kapiler_crt'] == 'kurang_2' ? 'checked' : ''?>> ≤ 2 detik
                                                </label>
                                            </div>
                                            <div class="checkbox" style="display: inline">
                                                <label>
                                                    <input type="radio" name="form[kapiler_crt]" value="lebih_2" <?=isset($form['kapiler_crt']) && $form['kapiler_crt'] == 'lebih_2' ? 'checked' : ''?>> ≥ 2 detik
                                                </label>
                                            </div>
                                            <br>
                                            <br>
                                            <p style="margin-bottom: 2px !important;"><strong>Nadi</strong></p>
                                            <div class="checkbox" style="display: inline">
                                                <label>
                                                    <input type="radio" name="form[nadi]" value="Kuat" <?=isset($form['nadi']) && $form['nadi'] == 'Kuat' ? 'checked' : ''?>> Kuat
                                                </label>
                                            </div>
                                            <div class="checkbox" style="display: inline">
                                                <label>
                                                    <input type="radio" name="form[nadi]" value="Lemah" <?=isset($form['nadi']) && $form['nadi'] == 'Lemah' ? 'checked' : ''?>> Lemah
                                                </label>
                                            </div>
                                            <br>
                                            <br>
                                            <p style="margin-bottom: 2px !important;"><strong>Ritmen</strong></p>
                                            <div class="checkbox" style="display: inline">
                                                <label>
                                                    <input type="radio" name="form[ritmen]" value="Teratur" <?=isset($form['ritmen']) && $form['ritmen'] == 'Teratur' ? 'checked' : ''?>> Teratur
                                                </label>
                                            </div>
                                            <div class="checkbox" style="display: inline">
                                                <label>
                                                    <input type="radio" name="form[ritmen]" value="Tidak Teratur" <?=isset($form['ritmen']) && $form['ritmen'] == 'Tidak Teratur' ? 'checked' : ''?>> Tidak Teratur
                                                </label>
                                            </div>
                                            <br>
                                            <br>
                                            <p style="margin-bottom: 2px !important;"><strong>Disability</strong></p>
                                            <?php
                                            sm9('form[disability][GCS]', 'GCS')->break_label()->textarea()->val($form['disability']['GCS'] ?? '')->build();
                                            sm9('form[disability][E]', 'E')->break_label()->textarea()->val($form['disability']['E'] ?? '')->build();
                                            sm9('form[disability][V]', 'V')->break_label()->textarea()->val($form['disability']['V'] ?? '')->build();
                                            sm9('form[disability][M]', 'M')->break_label()->textarea()->val($form['disability']['M'] ?? '')->build();
                                            ?>
                                            <p style="margin-bottom: 2px !important;"><strong>Kesadaran</strong></p>
                                            <div style="width: 100%; display: flex">
                                                <div>
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="radio" name="form[kesadaran]" value="Composmentis" <?=isset($form['kesadaran']) && $form['kesadaran'] == 'Composmentis' ? 'checked' : ''?>> Composmentis
                                                        </label>
                                                    </div>
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="radio" name="form[kesadaran]" value="Apatis" <?=isset($form['kesadaran']) && $form['kesadaran'] == 'Apatis' ? 'checked' : ''?>> Apatis
                                                        </label>
                                                    </div>
                                                </div>
                                                <div>
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="radio" name="form[kesadaran]" value="Somnolen" <?=isset($form['kesadaran']) && $form['kesadaran'] == 'Somnolen' ? 'checked' : ''?>> Somnolen
                                                        </label>
                                                    </div>
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="radio" name="form[kesadaran]" value="Stupor" <?=isset($form['kesadaran']) && $form['kesadaran'] == 'Stupor' ? 'checked' : ''?>> Stupor
                                                        </label>
                                                    </div>
                                                </div>
                                                <div>
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="radio" name="form[kesadaran]" value="Delirium" <?=isset($form['kesadaran']) && $form['kesadaran'] == 'Delirium' ? 'checked' : ''?>> Delirium
                                                        </label>
                                                    </div>
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="radio" name="form[kesadaran]" value="Coma" <?=isset($form['kesadaran']) && $form['kesadaran'] == 'Coma' ? 'checked' : ''?>> Coma
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>

                                            <h3 style="margin-top: 24px"><strong>SURVEY SEKUNDER</strong></h3>
                                            <p style="margin-bottom: 2px !important;"><strong>Anamnesis didapatkan dari</strong></p>
                                            <div class="checkbox">
                                                <label>
                                                    <input type="radio" name="form[anamnesis_dari]" value="Autoanamnesis" <?=isset($form['anamnesis_dari']) && $form['anamnesis_dari'] == 'Autoanamnesis' ? 'checked' : ''?>> Autoanamnesis
                                                </label>
                                            </div>
                                            <div class="checkbox" style="display: inline">
                                                <label>
                                                    <input type="radio" name="form[anamnesis_dari]" value="Alloanamnesis" <?=isset($form['anamnesis_dari']) && $form['anamnesis_dari'] == 'Alloanamnesis' ? 'checked' : ''?>> Alloanamnesis dengan
                                                </label>
                                            </div>
                                            <input type="text" class="form-control" style="display: inline !important; width: 100px !important;" name="form[Alloanamnesis_dari]" value="<?=$form['Alloanamnesis_dari'] ?? ''?>" autocomplete="off">
                                            hubungan dengan pasien
                                            <input type="text" class="form-control" style="display: inline !important; width: 100px !important;" name="form[Alloanamnesis_hubungan]" value="<?=$form['Alloanamnesis_hubungan'] ?? ''?>" autocomplete="off">
                                            <br>
                                            <br>
                                            <?php
                                            sm9('keluhan_utama', 'Keluhan Utama')
                                                ->textarea(3)
                                                ->val($pemeriksaan['keluhan_utama'] ?? '')
                                                ->break_label()
                                                ->build();
                                            sm9('form[riwayat_penyakit]', 'Riwayat Penyakit (lokasi, onset dan kronologis, kualitas, kuantitas, factor memperberat, factor memperingan, gejala penyerta)')
                                                ->textarea(3)
                                                ->val($form['riwayat_penyakit'] ?? '')
                                                ->break_label()
                                                ->build();
                                            sm9('form[obat_obatan]', 'Obat-Obatan yang sedang dikonsumsi dan atau dibawa pasien pada saat ini')
                                                ->textarea(3)
                                                ->val($form['obat_obatan'] ?? '')
                                                ->break_label()
                                                ->build();
                                            sm9('form[sudah_ada]', 'Pemeriksaan dan penunjang hasil yang sudah ada')
                                                ->textarea(3)
                                                ->val($form['sudah_ada'] ?? '')
                                                ->break_label()
                                                ->build();
                                            ?>
                                            <br>
                                            <p style="margin-bottom: 2px !important;"><strong>Status Generalis</strong></p>
                                            <p style="margin-bottom: 2px !important;"><strong>Kondisi Umum</strong></p>
                                            <div class="checkbox" style="display: inline">
                                                <label>
                                                    <input type="radio" name="form[kondisi_umum]" value="Baik" <?=isset($form['kondisi_umum']) && $form['kondisi_umum'] == 'Baik' ? 'checked' : ''?>> Baik
                                                </label>
                                            </div>
                                            <div class="checkbox" style="display: inline">
                                                <label>
                                                    <input type="radio" name="form[kondisi_umum]" value="Tampak sakit" <?=isset($form['kondisi_umum']) && $form['kondisi_umum'] == 'Tampak sakit' ? 'checked' : ''?>> Tampak sakit
                                                </label>
                                            </div>
                                            <div class="checkbox" style="display: inline">
                                                <label>
                                                    <input type="radio" name="form[kondisi_umum]" value="Sesak" <?=isset($form['kondisi_umum']) && $form['kondisi_umum'] == 'Sesak' ? 'checked' : ''?>> Sesak
                                                </label>
                                            </div>
                                            <div class="checkbox" style="display: inline">
                                                <label>
                                                    <input type="radio" name="form[kondisi_umum]" value="Pucat" <?=isset($form['kondisi_umum']) && $form['kondisi_umum'] == 'Pucat' ? 'checked' : ''?>> Pucat
                                                </label>
                                            </div>
                                            <div class="checkbox" style="display: inline">
                                                <label>
                                                    <input type="radio" name="form[kondisi_umum]" value="Lemah" <?=isset($form['kondisi_umum']) && $form['kondisi_umum'] == 'Lemah' ? 'checked' : ''?>> Lemah
                                                </label>
                                            </div>
                                            <div class="checkbox" style="display: inline">
                                                <label>
                                                    <input type="radio" name="form[kondisi_umum]" value="Kejang" <?=isset($form['kondisi_umum']) && $form['kondisi_umum'] == 'Kejang' ? 'checked' : ''?>> Kejang
                                                </label>
                                            </div>
                                            <div class="checkbox" style="display: flex; align-items: center">
                                                <label>
                                                    <input type="radio" name="form[kondisi_umum]" value="Lainnya" <?=isset($form['kondisi_umum']) && $form['kondisi_umum'] == 'Lainnya' ? 'checked' : ''?>> Lainnya
                                                </label>
                                                <textarea class="form-control" style="flex: 1; margin-left: 10px" name="form[kondisi_umum_lainnya]" autocomplete="off"><?=$form['kondisi_umum_lainnya'] ?? ''?></textarea>
                                            </div>
                                            <br>
                                            <p style="margin-bottom: 2px !important;"><strong>Jantung</strong></p>
                                            <img src="<?=base_url('assets/img/jantung.jpeg')?>" alt="" width="200">
                                            <?php
                                            sm9('form[jantung][inspeksi]', 'Inspeksi')
                                                ->val($form['jantung']['inspeksi'] ?? '')
                                                ->break_label()
                                                ->textarea()
                                                ->build();
                                            sm9('form[jantung][palpasi]', 'palpasi')
                                                ->val($form['jantung']['palpasi'] ?? '')
                                                ->break_label()
                                                ->textarea()
                                                ->build();
                                            sm9('form[jantung][perkusi]', 'Perkusi')
                                                ->val($form['jantung']['perkusi'] ?? '')
                                                ->break_label()
                                                ->textarea()
                                                ->build();
                                            sm9('form[jantung][auskultasi]', 'Auskultasi')
                                                ->val($form['jantung']['auskultasi'] ?? '')
                                                ->break_label()
                                                ->textarea()
                                                ->build();
                                            ?>
                                            <br>
                                            <?php
                                            sm9('form[paru][pemeriksaan_abdomen]', 'Pemeriksaan Abdomen')
                                                ->val($form['paru']['pemeriksaan_abdomen'] ?? '')
                                                ->textarea()
                                                ->break_label()
                                                ->build();
                                            ?>
                                            <p style="margin-bottom: 2px !important;"><strong>Paru</strong></p>
                                            <img src="<?=base_url('assets/img/paru.jpeg')?>" alt="" width="250">
                                            <?php
                                            sm9('form[paru][inspeksi]', 'Inspeksi')
                                                ->val($form['paru']['inspeksi'] ?? '')
                                                ->textarea()
                                                ->break_label()
                                                ->build();
                                            sm9('form[paru][palpasi]', 'Palpasi')
                                                ->val($form['paru']['palpasi'] ?? '')
                                                ->textarea()
                                                ->break_label()
                                                ->build();
                                            sm9('form[paru][perkusi]', 'Perkusi')
                                                ->val($form['paru']['perkusi'] ?? '')
                                                ->textarea()
                                                ->break_label()
                                                ->build();
                                            sm9('form[paru][auskultasi]', 'Auskultasi')
                                                ->val($form['paru']['auskultasi'] ?? '')
                                                ->textarea()
                                                ->break_label()
                                                ->build();
                                            ?>
                                            <br>
                                            <br>
                                            <p style="margin-bottom: 2px !important;"><strong>Status Lokalis</strong></p>
                                            <img src="<?=base_url('assets/img/lokalis.jpeg')?>" alt="" width="250">
                                            <?php
                                            sm9('form[lokalis][catatan]', 'Catatan')->textarea(3)->val($form['lokalis']['catatan'] ?? '')->break_label()->build();
                                            sm9('form[lokalis][dmm]', 'Daftar Masalah Medis')->textarea(3)->val($form['lokalis']['dmm'] ?? '')->break_label()->build();
                                            sm9('form[lokalis][dkdb]', 'Diagnosa Kerja / Diagnosa Banding')->textarea(3)->val($form['lokalis']['dkdb'] ?? '')->break_label()->build();
                                            sm9('form[lokalis][iad]', 'Instruksi Awal Dokter')->textarea(3)->val($form['lokalis']['iad'] ?? '')->break_label()->build();
                                            br();
                                            br();
                                            sel('diagnosis_jenis_penyakit[]', 'Diagnosa ICD-10')
                                                ->id('diagnosis_jenis_penyakit')
                                                ->onchange('on_diagnosis_change()')
                                                ->placeholder('Pilih Diagnosa ICD-10')
                                                ->options($penyakit->result())
                                                ->selectedOptions($s_penyakit)
                                                ->display(function ($value) {
                                                    return $value->kode . ' - ' . $value->nama;
                                                })
                                                ->build();
                                            sm9('diagnosis', 'Diagnosa')
                                                ->id('diagnosis')
//                                                ->onkeyup('on_diagnosis_change()')
                                                ->textarea(2)
                                                ->val($pemeriksaan['diagnosis'])
                                                ->build();

                                            sel('tindakan[]', 'Tarif / Tindakan')
                                                ->id('tindakan')
                                                ->onchange('on_tindakan_change()')
                                                ->placeholder('Pilih tindakan untuk pasien')
                                                ->options($tindakan->result())
                                                ->display(function ($value) use (&$pendaftaran) {
                                                    return $value->nama . " - Rp." . number_format($value->tarif_pasien, 2, ',', '.');
                                                })
                                                ->selectedOptionIds(array_map(function ($v) { return $v->tarif_tindakan_id; }, $tindakan_pasien->result()))
                                                ->build();

                                            //                                    sel('tindakan_operan[]', 'Tarif / Tindakan Operan')
                                            //                                        ->placeholder('Pilih tindakan untuk pasien')
                                            //                                        ->options($tindakan->result())
                                            //                                        ->display(function ($value) use (&$pendaftaran) {
                                            //                                            return $value->nama . " - Rp." . number_format($value->tarif_pasien, 2, ',', '.');
                                            //                                        })
                                            //                                        ->hide()
                                            //                                        ->selectedOptionIds(array_map(function ($v) { return $v->tarif_tindakan_id; }, []))
                                            //                                        ->build();
                                            ?>
                                            <?php
                                            sel('form[dokter][]', 'Nama Dokter Pemeriksa')
                                                ->placeholder('Pilih Dokter')
                                                ->options($dokter->result())
                                                ->display(function ($value) {
                                                    return ucwords($value->nama);
                                                })
                                                ->selectedOptionIds($form['dokter'] ?? [])
                                                ->required()
                                                ->build();
                                            ?>
                                            <div class="text-muted well well-sm no-shadow" style="display: block;">
                                                <div class="form-group" style=" margin-top: 4px">
                                                    <label for="hasil_penunjang_laboratorium" class="col-sm-4 control-label">
                                                        KONSULTASI DOKTER
                                                    </label>
                                                    <button type="button" class="btn btn-success btn-sm pull-right" id="btn-add-konsul" style="margin-right: 14px">
                                                        <span class="fa fa-plus"></span> Tambah Konsul
                                                    </button>
                                                </div>
                                                <div class="row" style="margin-bottom: 14px">
                                                    <div class="col-md-12">
                                                        <input type="hidden" name="max_konsul" id="max_konsul" value="0">
                                                        <table id="tbl-konsul" class="table table-condensed table-bordered" style="margin-bottom: 8px; table-layout:fixed;">
                                                            <tr>
                                                                <th style="width: 50%;">Tarif Tindakan</th>
                                                                <th>Dokter</th>
                                                                <th style="width: 46px"></th>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </div>
                                                <?php sm9('form[catatan_konsultasi]', 'Catatan Konsultasi')->textarea()->break_label()->val($form['catatan_konsultasi'] ?? '')->build(); ?>
                                            </div>
                                            <?php
                                            sel('perawat[]', 'Perawat')
                                                ->placeholder('Pilih perawat')
                                                ->options($listPerawat->result())
                                                ->display(function ($value) {
                                                    return ucwords($value->nama);
                                                })
                                                ->selectedOptionIds(explode(',', $pemeriksaan['detail_perawat_id']))
                                                ->required()
                                                ->build();

                                            sel('perawat_operan[]', 'Perawat Operan')
                                                ->placeholder('Pilih perawat Operan')
                                                ->options($listPerawat->result())
                                                ->display(function ($value) {
                                                    return ucwords($value->nama);
                                                })
                                                ->selectedOptionIds([])
                                                ->hide()
                                                ->build();

                                            sm9('deskripsi_tindakan', 'Tata Laksana')
                                                ->val($pemeriksaan['deskripsi_tindakan'])
                                                ->textarea()
                                                ->build();
                                            sm9('saran_pemeriksaan', 'Rujukan')->val($pemeriksaan['saran_pemeriksaan'])->build();
                                            sel('status_pasien', 'Status Pasien')
                                                ->placeholder('Pilih status pasien')
                                                ->single()
                                                ->id('status_pasien')
                                                ->normal()
                                                ->options([
                                                    (object) ['id' => 'Pilih', 'value' => '--Pilih status pasien--'],
                                                    (object) ['id' => 'Rawat Inap', 'value' => 'Rawat Inap'],
                                                    (object) ['id' => 'Rujuk', 'value' => 'Rujuk'],
                                                    (object) ['id' => 'Rawat Jalan', 'value' => 'Rawat Jalan'],
                                                    (object) ['id' => 'Meniggal', 'value' => 'Meniggal'],
                                                    (object) ['id' => 'Pulang', 'value' => 'Pulang'],
                                                ])
                                                ->selectedOptionIds([$pemeriksaan['status_pasien']])
                                                ->display(function ($v) { return $v->value; })
                                                ->build();
                                            ?>
                                            <div class="row" id="c-is-pakai-ambulance" style="display: none">
                                                <div class="col-md-12">
                                                    <?php

                                                    sel('pakai_ambulance', 'Pakai Ambulance?')
                                                        ->placeholder('Pilih pakai ambulance')
                                                        ->single()
                                                        ->id('pakai_ambulance')
                                                        ->normal()
                                                        ->options([
                                                            (object) ['id' => '0', 'value' => 'Tidak'],
                                                            (object) ['id' => '1', 'value' => 'Iya'],
                                                        ])
                                                        ->display(function ($v) { return $v->value; })
                                                        ->build();
                                                    ?>
                                                </div>
                                            </div>
                                            <div class="row" id="c-pakai-ambulance" style="display: none">
                                                <input type="hidden" name="pasien_id" value="<?=$pemeriksaan['pasien_id']?>">
                                                <input type="hidden" name="nama_pasien" value="<?=$pendaftaran['nama_pasien']?>">
                                                <input type="hidden" name="alamat_pasien" value="<?=$pendaftaran['alamat']?>">
                                                <input type="hidden" name="no_hp_pasien" value="<?=$pendaftaran['telepon']?>">
                                                <input type="hidden" name="no_jaminan" value="<?=$pendaftaran['no_jaminan']?>">
                                                <input type="hidden" name="pasien_baru" value="lama">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label for="" class="col-sm-3 control-label">Jenis Pendaftaran</label>
                                                        <div class="col-sm-9">
                                                            <select class="form-control"
                                                                    name="jaminan"
                                                                    id="jaminan"
                                                                    data-placeholder="--Pilih Jenis Pendaftaran--"
                                                                    style="width: 100%;"
                                                            >
                                                                <option value="">--Pilih Jenis Pendaftaran--</option>
                                                                <?php foreach ($jaminan as $key => $value) { ?>
                                                                    <option value="<?= $key ?>" <?=$pemeriksaan['jaminan'] == $key ? 'selected' : ''?>><?= $value['label'] ?></option>
                                                                <?php } ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="" class="col-sm-3 control-label">Model Kendaraan</label>
                                                        <div class="col-sm-9">
                                                            <select class="form-control"
                                                                    name="vehicle_id"
                                                                    data-placeholder="--Pilih Jenis Pendaftaran--"
                                                                    style="width: 100%;"
                                                            >
                                                                <option value="">--Pilih Ambulance--</option>
                                                                <?php foreach ($vehiclelist as $key => $vehicle) { ?>
                                                                    <option value="<?php echo $vehicle["id"] ?>"><?php echo $vehicle["vehicle_model"] . " - " . $vehicle["vehicle_no"] ?></option>
                                                                <?php } ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <?php
                                                    sm9('driver', 'Nama Sopir')->build();
                                                    sm9('date', 'Tanggal')->val(date('Y-m-d'))->build();
                                                    ?>
                                                    <div class="form-group">
                                                        <label for="" class="col-sm-3 control-label">Tujuan</label>
                                                        <div class="col-sm-9">
                                                            <select class="form-control"
                                                                    name="tarif_ambulance_id"
                                                                    data-placeholder="--Pilih Jenis Pendaftaran--"
                                                                    style="width: 100%;"
                                                            >
                                                                <option value="">--Pilih Tujuan--</option>
                                                                <?php foreach ($tarif_ambulance as $t) { ?>
                                                                    <option value="<?php echo $t->id ?>"><?php echo $t->tujuan ?></option>
                                                                <?php } ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php sel('surat', 'Pilih Surat')
                                                ->placeholder('Pilih Surat')
                                                ->single()
                                                ->normal()
                                                ->id('surat')
                                                ->onchange('change_surat()')
                                                ->options([
                                                    (object)['id' => '', 'value' => '--Pilih Surat--'],
                                                    (object)['id' => 'sehat', 'value' => 'Surat Keterangan Sehat'],
                                                    (object)['id' => 'sakit', 'value' => 'Surat Keterangan Sakit'],
                                                    (object)['id' => 'rujuk', 'value' => 'Surat Keterangan Rujuk'],
                                                    (object)['id' => 'ranap', 'value' => 'Surat Pengantar Rawat Inap'],
                                                ])
                                                ->selectedOptionIds([$pendaftaran['surat']])
                                                ->display(function ($v) {
                                                    return $v->value;
                                                })
                                                ->build(); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <?php $this->load->view('pemeriksaan/partials/surat_sehat'); ?>
                            <?php $this->load->view('pemeriksaan/partials/surat_sakit'); ?>
                            <?php $this->load->view('pemeriksaan/partials/surat_rujuk'); ?>
                            <?php $this->load->view('pemeriksaan/partials/surat_ranap'); ?>
                        </div>
                    </div>
                </div>
                <?php $this->load->view('igd/partials/side_1'); ?>
                <?php $this->load->view('igd/partials/side_2'); ?>
            </div>
        </section>
    </div>
</form>

<div id='ResponseInput'></div>

<!-- Select2 -->
<script src="<?= base_url() ?>assets/bower_components/select2/dist/js/select2.full.min.js"></script>
<!-- InputMask -->

<script type="text/javascript">

    $(function () {
        $('#status_pasien').change(function () {
            if ($(this).val() === 'Rujuk') {
                $('#c-is-pakai-ambulance').css('display', 'block')
            }
            else {
                $('#c-is-pakai-ambulance').css('display', 'none')
                $('#c-pakai-ambulance').css('display', 'none')
            }
        })

        $('#pakai_ambulance').change(function () {
            if ($(this).val() === '1') {
                $('#c-pakai-ambulance').css('display', 'block')
            }
            else {
                $('#c-pakai-ambulance').css('display', 'none')
            }
        })
    })

    function delete_konsul(num) {
        $(`#row-konsul-${num}`).remove();
    }

    function setImt() {
        const tb = (parseInt($('#form_tb').val() || '0') ?? 0) / 100
        const bb = parseInt($('#form_bb').val() || '0') ?? 0
        const imt = bb / (tb * tb)

        $('#form_imt').val(imt.toFixed(2))

        $('#underweight').prop('checked', imt < 18.5)
        $('#normal').prop('checked', imt >= 18.5 && imt <= 24.99)
        $('#overweight').prop('checked', imt >= 25 && imt <= 29.99)
        $('#obese').prop('checked', imt >= 30)
    }

    $(document).ready(function () {
        let konsul_num = 0;
        $('#btn-add-konsul').click(function () {
            konsul_num++;
            $('#max_konsul').val(konsul_num);
            $('#tbl-konsul tr:last').after(`
                <tr id="row-konsul-${konsul_num}">
                    <td style="width: 50%;">
                        <div class="form-group">
                            <div class="col-sm-12">
                                <select class="form-control select2" multiple="multiple" name="tindakan_${konsul_num}[]" id="tindakan_${konsul_num}"
                                        data-placeholder="Pilih tindakan"
                                        style="width: 100%;" required>
                                        <?php foreach ($tindakan->result() as $key => $value) { ?>
                                            <option value="<?= $value->id; ?>"><?= $value->nama . " - Rp." . number_format($value->tarif_pasien, 2, ',', '.') ?></option>
                                        <?php } ?>
                                </select>
                            </div>
                        </div>
                    </td>
                    <td>
                        <div class="form-group">
                            <div class="col-sm-12">
                                <select class="form-control abdush-select" name="dokter_${konsul_num}" style="width: 100%;" required>
                                    <option value="" selected>-- Pilih Dokter --</option>
                                    <?php foreach ($dokter->result() as $key => $value) { ?>
                                        <option value="<?php echo $value->id ?>"><?php echo $value->nama; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                    </td>
                    <td style="width: 46px">
                        <button type="button" class="btn btn-sm btn-danger" onclick="delete_konsul(${konsul_num})">
                            <span class="fa fa-trash"></span>
                        </button>
                    </td>
                </td>
            `);
            $(`#tindakan_${konsul_num}`).select2();
        })

        const tindakan_konsul = <?=json_encode($tindakan_konsul);?>;
        const all_tindakan = <?=json_encode($tindakan->result());?>;
        const all_dokter = <?=json_encode($dokter->result());?>;
        tindakan_konsul.forEach((v, k) => {
            konsul_num++;
            $('#max_konsul').val(konsul_num);
            const tindakan_opts = all_tindakan.map(x => `<option value="${x.id}" ${+x.id === +v.tarif_tindakan_id ? 'selected' : ''}>${x.nama} - ${formatMoney(x.tarif_pasien)}</option>`)
            const dokter_opts = all_dokter.map(x => `<option value="${x.id}" ${+x.id === +v.dokter_id ? 'selected' : ''}>${x.nama}</option>`)
            $('#tbl-konsul tr:last').after(`
                <tr id="row-konsul-${konsul_num}">
                    <td style="width: 50%;">
                        <div class="form-group">
                            <div class="col-sm-12">
                                <select class="form-control select2" multiple="multiple" name="tindakan_${konsul_num}[]" id="tindakan_${konsul_num}"
                                        data-placeholder="Pilih tindakan"
                                        style="width: 100%;" required>
                                        ${tindakan_opts.join('')}
                                </select>
                            </div>
                        </div>
                    </td>
                    <td>
                        <div class="form-group">
                            <div class="col-sm-12">
                                <select class="form-control abdush-select" name="dokter_${konsul_num}" style="width: 100%;" required>
                                    ${dokter_opts.join('')}
                                </select>
                            </div>
                        </div>
                    </td>
                    <td style="width: 46px">
                        <button type="button" class="btn btn-sm btn-danger" onclick="delete_konsul(${konsul_num})">
                            <span class="fa fa-trash"></span>
                        </button>
                    </td>
                </td>
            `);
            $(`#tindakan_${konsul_num}`).select2();
        })
    })

    $('.select2').select2();

    function set_bmi() {
        var tb = $('#tb').val();
        var bb = $('#bb').val();
        var tbm = tb / 100;
        var bmi = bb / (tbm * tbm);

        $('#bmi').val(bmi.toFixed(2));
    }

    function loadData(i) {
        var id = $('#obat' + i).val();
        var jumlah_satuan = $('#jumlah_satuan' + i).val();
        var stok = $('#stok' + i).val();
        var urls = "<?= base_url(); ?>obat/getStokObat";
        var datax = {"id": id};

        if (parseInt(stok) < parseInt(jumlah_satuan)) {
            alert('Stok obat tidak cukup. Silahkan kurangi jumlah atau ganti obat lain!');
            $('#jumlah_satuan' + i).val('');
        }
    }

    for (var i = 1; i < 11; i++) {
        for (var j = 1; j < 3; j++) {
            function loadDataRacikan(i, j) {
                var ij = (i.toString() + j.toString());


                var id = $('#obat_racikan' + ij).val();
                var jumlah_satuan = $('#jumlah_satuan' + ij).val();
                var urls = "<?= base_url(); ?>obat/getStokObat";
                var datax = {"id": id};

                $.ajax({
                    type: 'GET',
                    url: urls,
                    data: datax,

                    success: function (stok) {
                        if (parseInt(stok) < parseInt(jumlah_satuan)) {
                            alert('Stok obat tidak cukup. Silahkan kurangi jumlah atau ganti obat lain!');
                            $('#jumlah_satuan' + ij).val('');
                        }


                    }
                });
            }
        }
    }

</script>

<script type="text/javascript">
    jQuery(function ($) {
        var bahan = <?php echo json_encode($bahan); ?>;
        $('#bahan-option').select2();

        $('#button-add-bahan').on('click', function (e) {
            var id_bahan = $('#bahan-option').val();
            if (id_bahan == '') {
                alert('Anda belum memilih bahan habis pakai !');
            }
            else {
                var theBahan = {};
                var counter = parseInt($('#abdush-counter2').val());
                $.each(bahan, function (i, v) {
                    if (v.id == id_bahan) {
                        theBahan = v;
                        return;
                    }
                });

                var html = `
                <tr>
                  <td>
                    ` + theBahan.nama + `
                    <input type="hidden" name="id[]" value="` + theBahan.id + `">
                  </td>
                  <td>` + theBahan.jumlah + ` ` + theBahan.satuan + `</td>
                  <td>
                    <input style="width:65px;" type="text" class="form-control" name="qty[]" id="bahan[` + counter + `][qty]">
                  </td>
                  <td>
                    <button class="btn btn-sm btn-danger btn-delete-row" type="button"> <i class="fa fa-trash"></i> </button>
                  </td>
                </tr>`;

                $('.form-area tbody').append(html);
                $('#abdush-counter').val(counter + 1);
                $('input[name="bahan[' + counter + '][qty]"]').focus();

                $('.btn-delete-row').unbind('click');
                $('.btn-delete-row').each(function () {
                    $(this).on('click', function () {
                        $(this).parents('tr').remove();
                    });
                });
            }

            $('#bahan-option').val('').trigger('change');
        });

        var obat = <?php echo json_encode($obat1); ?>;
        $('#obat-option').select2();

        $('#button-add-obat').on('click', function (e) {
            var id_Obat = $('#obat-option').val();
            if (id_Obat == '') {
                alert('Anda belum memilih obat !');
            }
            else {
                var theObat = {};
                var counter = parseInt($('#abdush-counter2').val());
                $.each(obat, function (i, v) {
                    if (v.id == id_Obat) {
                        theObat = v;
                        return;
                    }
                });

                var html = `
                <tr>
                  <td>
                    ` + theObat.nama + `
                    <input type="hidden" name="nama_obat[]" value="` + theObat.id + `">
                  </td>
                  <td>` + theObat.stok_obat + ` item </td>
                  <input type="hidden" id="stok` + counter + `" value="` + theObat.stok_obat + `">
                  <td>
                    <input style="width:65px;" type="text" class="form-control" onchange="loadData(` + counter + `);" name="jumlah_satuan[]" id="jumlah_satuan` + counter + `">
                  </td>
                  <td>
                    <input style="width:100px;" type="text" class="form-control"  name="signa_obat[]" id="signa_obat` + counter + `">
                  </td>
                  <td>
                    <button class="btn btn-sm btn-danger btn-delete-row" type="button"> <i class="fa fa-trash"></i> </button>
                  </td>
                </tr>

                `;

                $('.form-area-obat tbody').append(html);
                $('#abdush-counter2').val(counter + 1);
                $('input[id="jumlah_satuan' + counter + '"]').focus();

                $('.btn-delete-row').unbind('click');
                $('.btn-delete-row').each(function () {
                    $(this).on('click', function () {
                        $(this).parents('tr').remove();
                    });
                });
            }

            $('#obat-option').val('').trigger('change');
        });

        let initializer = {
            init: function() {
                for (let i = 1; i < 9; i++) {
                    $(`#obat-racik-option-${i}`).select2();
                    $(`#button-add-obat-racik-${i}`).on('click', e => {
                        this.onBtnAddClick(i);
                    });
                }
            },
            checkHasObat: function(i) {
                let rowCount = $(`.form-area-obat-racik-${i} >table >tbody >tr`).length;
                if (rowCount === 0) {
                    $(`#signa-obat-racik-${i}`).prop('required',false);
                }
            },
            onBtnAddClick: function(i) {
                let that = this;
                let opt = $(`#obat-racik-option-${i}`);
                let id_Obat = opt.val();
                if (id_Obat === '') {
                    alert('Anda belum memilih obat !');
                }
                else {
                    let theObat = {};
                    let counter = parseInt($(`#abdush-counter-${i}`).val());
                    $.each(obat, function (i, v) {
                        if (v.id === id_Obat) {
                            theObat = v;
                            return;
                        }
                    });

                    $(`.form-area-obat-racik-${i} tbody`).append(this.getTableRow(i, theObat, counter));
                    $(`#signa-obat-racik-${i}`).prop('required',true);
                    $(`#abdush-counter-${i}`).val(counter + 1);
                    $('input[id="jumlah_satuan_racik' + counter + '"]').focus();

                    $('.btn-delete-row').unbind('click');
                    $('.btn-delete-row').each(function () {
                        $(this).on('click', function () {
                            $(this).parents('tr').remove();
                            that.checkHasObat(i);
                        });
                    });
                }
                opt.val('').trigger('change');
            },
            getTableRow: (i, obat, counter) => (`
                <tr>
                  <td>
                    ` + obat.nama + `
                    <input type="hidden" name="nama_obat_racikan${i}[]" value="` + obat.id + `">
                  </td>
                  <td>` + obat.stok_obat + ` item </td>
                  <input type="hidden" id="stok` + counter + `" value="` + obat.stok_obat + `">
                  <td>
                    <input style="width:65px;" type="text" class="form-control" onchange="loadData(` + counter + `);" name="jumlah_satuan_racikan${i}[]" id="jumlah_satuan_racikan${i}` + counter + `">
                  </td>
                  <td>
                    <button class="btn btn-sm btn-danger btn-delete-row" type="button"> <i class="fa fa-trash"></i> </button>
                  </td>
                </tr>
            `)
        };

        initializer.init();
    });
</script>

<script>
    let obat_all = <?php echo json_encode($obat1); ?>;
    let obat = <?php echo json_encode($obats); ?> ?? [];
    let racikan = <?php echo json_encode($racikans); ?> ?? [];
    let bahan_all = <?php echo json_encode($bahan); ?>;
    let bahans = <?php echo json_encode($bahans); ?> ?? [];

    let renderObat = (theObat, counter, jumlah = '', signa = '') => `
        <tr>
          <td>
            ${theObat.nama}
            <input type="hidden" name="nama_obat[]" value="${theObat.id}">
          </td>
          <td>${theObat.stok_obat} item </td>
          <input type="hidden" id="stok${counter}" value="${theObat.stok_obat}">
          <td>
            <input style="width:65px;"
                type="text"
                class="form-control"
                onchange="cekObat(${counter});"
                name="jumlah_satuan[]"
                id="jumlah_satuan${counter}"
                value="${jumlah}">
          </td>
          <td>
            <input style="width:100px;"
                type="text"
                class="form-control"
                name="signa_obat[]"
                id="signa_obat${counter}"
                value="${signa}">
          </td>
          <td>
            <button class="btn btn-sm btn-danger btn-delete-row" type="button"> <i class="fa fa-trash"></i> </button>
          </td>
        </tr>
    `;
    let renderObatRacik = (i, obat, counter, jumlah = '') => `
        <tr>
          <td>
            ${obat.nama}
            <input type="hidden" name="nama_obat_racikan${i}[]" value="${obat.id}">
          </td>
          <td>${obat.stok_obat} item </td>
          <input type="hidden" id="stok${i}${counter}" value="${obat.stok_obat}">
          <td>
            <input style="width:65px;"
                type="text"
                class="form-control"
                onchange="cekObatRacik(${i},${counter});"
                name="jumlah_satuan_racikan${i}[]"
                id="jumlah_satuan_racikan${i}${counter}"
                value="${jumlah}">
          </td>
          <td>
            <button class="btn btn-sm btn-danger btn-delete-row" type="button"> <i class="fa fa-trash"></i> </button>
          </td>
        </tr>
    `;
    let renderBahan = (theBahan, counter, jumlah = '') => `
        <tr>
            <td>
            ${theBahan.nama}
                <input type="hidden" name="id[]" value="${theBahan.id}">
            </td>
            <td>${theBahan.jumlah} ${theBahan.satuan}</td>
            <input type="hidden" id="stok_bahan${counter}" value="${theBahan.jumlah}">
            <td>
                <input style="width:65px;"
                    type="text"
                    class="form-control"
                    onchange="cekBahan(${counter});"
                    name="qty[]"
                    id="bahan[${counter}][qty]"
                    value=${jumlah}>
            </td>
            <td>
                <button class="btn btn-sm btn-danger btn-delete-row" type="button"> <i class="fa fa-trash"></i> </button>
            </td>
        </tr>
    `;

    let addObatView = (id_Obat, jumlah = '', signa = '') => {
        let counter = parseInt($('#abdush-counter2').val());
        let theObat = obat_all.find(v => v.id === id_Obat);

        $('.form-area-obat tbody').append(renderObat(theObat, counter, jumlah, signa));
        $('#abdush-counter2').val(counter + 1);
        $('input[id="jumlah_satuan' + counter + '"]').focus();

        $('.btn-delete-row').unbind('click');
        $('.btn-delete-row').each(function () {
            $(this).on('click', function () {
                $(this).parents('tr').remove();
            });
        });
    };
    let addObatRacikView = (id_obat, i, jumlah = '') => {
        let counter = parseInt($(`#abdush-counter-${i}`).val());
        let theObat = obat_all.find(v => v.id === id_obat);

        $(`.form-area-obat-racik-${i} tbody`).append(renderObatRacik(i, theObat, counter, jumlah));
        $(`#abdush-counter-${i}`).val(counter + 1);
        $('input[id="jumlah_satuan_racik' + counter + '"]').focus();

        $('.btn-delete-row').unbind('click');
        $('.btn-delete-row').each(function () {
            $(this).on('click', function () {
                $(this).parents('tr').remove();
            });
        });
    };
    let addBahanView = (id_bahan, jumlah = '') => {
        let counter = parseInt($('#abdush-counter-bahan').val());
        let theBahan = bahan_all.find(v => v.id === id_bahan);

        $('.form-area tbody').append(renderBahan(theBahan, counter, jumlah));
        $('#abdush-counter').val(counter + 1);
        $('input[name="bahan[' + counter + '][qty]"]').focus();

        $('.btn-delete-row').unbind('click');
        $('.btn-delete-row').each(function () {
            $(this).on('click', function () {
                $(this).parents('tr').remove();
            });
        });
    };

    obat.forEach(v => {
        addObatView(v.obat_id, v.jumlah_satuan, v.signa_obat);
    });
    racikan.forEach((v, k) => {
        v.racikan.forEach((vv, kk) => {
            addObatRacikView(vv.obat_id, k + 1, vv.jumlah_satuan);
        });
        $(`#signa${k + 1}`).val(v.signa);
        $(`#catatan${k + 1}`).val(v.catatan);
    });
    bahans.forEach(v => {
        addBahanView(v.bahan_id, v.jumlah);
    });
</script>

<script>
    function cekObat(i) {
        var jumlah_satuan = $('#jumlah_satuan' + i).val();
        var stok = $('#stok' + i).val();

        if (parseInt(stok) < parseInt(jumlah_satuan)) {
            alert('Stok obat tidak cukup. Silahkan kurangi jumlah atau ganti obat lain!');
            $('#jumlah_satuan' + i).val('');
        }
    }
    function cekObatRacik(i, j) {
        var jumlah_satuan = $('#jumlah_satuan_racikan' + i + j).val();
        var stok = $('#stok' + i + j).val();

        if (parseInt(stok) < parseInt(jumlah_satuan)) {
            alert('Stok obat tidak cukup. Silahkan kurangi jumlah atau ganti obat lain!');
            $('#jumlah_satuan_racikan' + i + j).val('');
        }
    }
    function cekBahan(i) {
        var jumlah_satuan = $(`#bahan\\[${i}\\]\\[qty\\]`).val();
        var stok = $('#stok_bahan' + i).val();

        if (parseInt(stok) < parseInt(jumlah_satuan)) {
            alert('Stok bahan tidak cukup. Silahkan kurangi jumlah atau ganti bahan lain!');
            $(`#bahan\\[${i}\\]\\[qty\\]`).val('');
        }
    }
</script>

<?php $this->load->view('pemeriksaan/partials/surat_js'); ?>