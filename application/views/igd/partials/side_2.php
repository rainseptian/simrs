<div class="col-sm-12 col-md-12 col-lg-5">
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">Permintaan Penunjang Medis</h3>
        </div>
        <div class="box-footer" style="display: flex">
            <div style="flex: 1; margin-right: 10px">
                <button type="submit" name="rujuk" value="lab" class="btn btn-success btn-block btn">
                    <i class="fa fas fa-flask"></i>
                    Laboratorium
                </button>
            </div>
            <div style="flex: 1">
                <button type="submit" name="rujuk" value="radio" class="btn btn-success btn-block btn">
                    <i class="fa fas fa-x-ray"></i>
                    Radiologi
                </button>
            </div>
        </div>
    </div>
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">Rujuk Antar Poli</h3>
        </div>
        <div class="box-body">
            <select class="form-control abdush-select" name="poli-rujuk" id="poli-rujuk">
                <option value="">--Pilih Poli Rujuk--</option>
                <?php foreach ($jenis_pendaftaran->result() as $key => $value) { ?>
                    <?php if ($value->id != 19 && $value->id != 59) : ?>
                        <option value="<?php echo $value->id ?>"><?php echo $value->nama ?></option>
                    <?php endif; ?>
                <?php } ?>
            </select>
        </div>
        <div class="box-footer text-right">
            <button type="submit" name="rujuk" value="poli" onclick="return rujuk_poli()" class="btn btn-success">Rujuk</button>
        </div>
    </div>
</div>