<div class="tab-pane <?=$tab == 'diagnosis' ? 'active' : ''?>" id="diagnosis">
    <div class="impbtnview">
        <a href="#" class="btn btn-sm btn-primary dropdown-toggle adddiagnosis"
           onclick="holdModal('tambah_diagnosis')" data-toggle="modal"><i class="fas fa-plus"></i> Tambah Diagnosis
        </a>
    </div>
    <div class="table-responsive">
        <table class="table table-striped table-bordered table-hover example">
            <thead>
            <tr>
                <th>No</th>
                <th>Tgl Diagnosis</th>
                <th>Penyakit</th>
                <th>Diagnosis</th>
                <th>Aksi</th>
            </tr>
            </thead>
            <tbody>
            <?php
            $no = 1;
            foreach ($diagnosis as $r) : ?>
                <tr class="row-diagnosis">
                    <td><?=$no++?></td>
                    <td><?=$r->created_at?></td>
                    <td>
                        <?php foreach ($r->penyakit as $p) : ?>
                            <span style="margin-left:2px; margin-top:1px; display: inline-block">
                                                        <span class="label label-success ml-1"><?=$p->kode.' - '.$p->nama?></span>
                                                    </span>
                        <?php endforeach; ?>
                    </td>
                    <td><?=$r->diagnosis?></td>
                    <td style="width: 50px">
                        <a href="<?=base_url()?>RawatInap/hapus/diagnosis/<?=$rawat_inap->id?>/<?=$r->id?>"
                           onclick="return confirm('Hapus data ini?')"
                           class="btn btn-danger btn-sm">
                            <span class="fa fa-trash"></span> Hapus
                        </a>
                    </td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>