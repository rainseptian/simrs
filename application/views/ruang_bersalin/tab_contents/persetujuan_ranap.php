<div class="tab-pane <?=isset($tab) && $tab == 'persetujuan_ranap' ? 'active' : ''?>" id="persetujuan_ranap">
    <div class="impbtnview">
        <a href="<?=base_url("RawatInap/print_form/persetujuan_ranap/$rawat_inap->id")?>" target="_blank" class="btn btn-primary btn-sm">
            <i class="fa fa-print"></i> Cetak
        </a>
    </div>
    <div class="row">
        <div class="col-md-12">
            <form method="post" action="<?php echo base_url() ?>RawatInap/save_form/persetujuan/persetujuan_ranap">
                <input type="hidden" name="rawat_inap_id" value="<?=$rawat_inap->id?>">
                <h4><b>PERNYATAAN PERSETUJUAN RAWAT INAP</b></h4>
                <table class="table table-bordered" style="table-layout: fixed">
                    <tbody>
                    <tr>
                        <td>
                            <div style="display: flex; flex-direction: column">
                                <span>Yang bertanda tangan di bawah ini :</span>
                                <br>
                                <span>Nama : <input type="text" name="form[persetujuan][nama]" value="<?=$form['persetujuan']['nama']?>" class="form-control"></span>
                                <span>Tgl. Lahir : <input type="date" name="form[persetujuan][tgl_lahir]" value="<?=$form['persetujuan']['tgl_lahir']?>" class="form-control"></span>
                                <span>Alamat : <input type="text" name="form[persetujuan][alamat]" value="<?=$form['persetujuan']['alamat']?>" class="form-control"></span>
                                <br>
                                <span>
                                    Dalam hal ini bertindak sebagai
                                    <input type="radio" name="form[persetujuan][sbg]" value="1" <?=$form['persetujuan']['sbg'] == '1' ? 'checked' : ''?>> diri sendiri /
                                    <input type="radio" name="form[persetujuan][sbg]" value="2" <?=$form['persetujuan']['sbg'] == '2' ? 'checked' : ''?>> Suami /
                                    <input type="radio" name="form[persetujuan][sbg]" value="3" <?=$form['persetujuan']['sbg'] == '3' ? 'checked' : ''?>> Istri /
                                    <input type="radio" name="form[persetujuan][sbg]" value="4" <?=$form['persetujuan']['sbg'] == '4' ? 'checked' : ''?>> Ayah /
                                    <input type="radio" name="form[persetujuan][sbg]" value="5" <?=$form['persetujuan']['sbg'] == '5' ? 'checked' : ''?>> Ibu /
                                    Wali penanggung jawab *
                                    dari
                                </span>
                                <br>
                                <span>Nama : <?=$pasien->nama?></span>
                                <span>Tgl. Lahir : <?=$pasien->tanggal_lahir?></span>
                                <span>Alamat : <?=$pasien->alamat?></span>
                                <br>
                                <span>
                                    Saya telah mendapatkan informasi dari petugas mengenai hak dan kewajiban saya serta tata tertib di KLINIK UTAMA SUKMA WIJAYA,
                                    dengan demikian saya menyatakan :
                                </span>
                                <span>1. Pasien tersebut di atas adalah pasien dengan kepesertaan:</span>
                                <table class="table table-bordered">
                                    <tbody>
                                    <tr>
                                        <th rowspan="2">Kepesertaan</th>
                                        <th colspan="2">Persyaratan</th>
                                        <th rowspan="2">Keterangan</th>
                                    </tr>
                                    <tr>
                                        <th>Lengkap</th>
                                        <th>Menyusul</th>
                                    </tr>
                                    <tr>
                                        <td><input type="checkbox" name="form[persetujuan][umum][waw]" value="1" <?=$form['persetujuan']['umum']['waw'] == 1 ? 'checked' : ''?>> Umum</td>
                                        <td><input type="checkbox" name="form[persetujuan][umum][lengkap]" value="1" <?=$form['persetujuan']['umum']['lengkap'] == 1 ? 'checked' : ''?>></td>
                                        <td><input type="checkbox" name="form[persetujuan][umum][menyusui]" value="1" <?=$form['persetujuan']['umum']['menyusui'] == 1 ? 'checked' : ''?>></td>
                                        <td><input type="text" name="form[persetujuan][umum][ket]" value="<?=$form['persetujuan']['umum']['ket']?>" class="form-control"></td>
                                    </tr>
                                    <tr>
                                        <td><input type="checkbox" name="form[persetujuan][bpjs][waw]" value="1" <?=$form['persetujuan']['bpjs']['waw'] == 1 ? 'checked' : ''?>> BPJS</td>
                                        <td><input type="checkbox" name="form[persetujuan][bpjs][lengkap]" value="1" <?=$form['persetujuan']['bpjs']['lengkap'] == 1 ? 'checked' : ''?>></td>
                                        <td><input type="checkbox" name="form[persetujuan][bpjs][menyusui]" value="1" <?=$form['persetujuan']['bpjs']['menyusui'] == 1 ? 'checked' : ''?>></td>
                                        <td><input type="text" name="form[persetujuan][bpjs][ket]" value="<?=$form['persetujuan']['bpjs']['ket']?>" class="form-control"></td>
                                    </tr>
                                    <tr>
                                        <td><input type="checkbox" name="form[persetujuan][jkd][waw]" value="1" <?=$form['persetujuan']['jkd']['waw'] == 1 ? 'checked' : ''?>> JKD/SKTM</td>
                                        <td><input type="checkbox" name="form[persetujuan][jkd][lengkap]" value="1" <?=$form['persetujuan']['jkd']['lengkap'] == 1 ? 'checked' : ''?>></td>
                                        <td><input type="checkbox" name="form[persetujuan][jkd][menyusui]" value="1" <?=$form['persetujuan']['jkd']['menyusui'] == 1 ? 'checked' : ''?>></td>
                                        <td><input type="text" name="form[persetujuan][jkd][ket]" value="<?=$form['persetujuan']['jkd']['ket']?>" class="form-control"></td>
                                    </tr>
                                    <tr>
                                        <td><input type="checkbox" name="form[persetujuan][lainnya][waw]" value="1" <?=$form['persetujuan']['lainnya']['waw'] == 1 ? 'checked' : ''?>> Asuransi lainnya</td>
                                        <td><input type="checkbox" name="form[persetujuan][lainnya][lengkap]" value="1" <?=$form['persetujuan']['lainnya']['lengkap'] == 1 ? 'checked' : ''?>></td>
                                        <td><input type="checkbox" name="form[persetujuan][lainnya][menyusui]" value="1" <?=$form['persetujuan']['lainnya']['menyusui'] == 1 ? 'checked' : ''?>></td>
                                        <td><input type="text" name="form[persetujuan][lainnya][ket]" value="<?=$form['persetujuan']['lainnya']['ket']?>" class="form-control"></td>
                                    </tr>
                                    </tbody>
                                </table>
                                <span>2. Setuju akan menempati ruang perawatan</span>
                                <div>
                                    <input type="radio" name="form[persetujuan][kelas1]" value="1" <?=$form['persetujuan']['kelas1'] == '1' ? 'checked' : ''?>> Kelas 1
                                    <input type="radio" name="form[persetujuan][kelas1]" value="2" <?=$form['persetujuan']['kelas1'] == '2' ? 'checked' : ''?>> Kelas 2
                                    <input type="radio" name="form[persetujuan][kelas1]" value="3" <?=$form['persetujuan']['kelas1'] == '3' ? 'checked' : ''?>> Kelas 3
                                    <input type="radio" name="form[persetujuan][kelas1]" value="VIP" <?=$form['persetujuan']['kelas1'] == 'VIP' ? 'checked' : ''?>> Kelas VIP
                                </div>
                                <br>
                                <span>3. Kepesertaan pasien BPJS PBI atau PBID (Penerima Bantuan Iuran Daerah) akan GUGUR atau TIDAK BERLAKU apabila :</span>
                                <span style="padding-left: 10px">a. Memilih dirawat kelas II, I, VIP</span>
                                <span style="padding-left: 10px">a. Persyaratan tidak dilengkapi dalam waktu 2x24 jam</span>
                                <span style="padding-left: 10px">a. Mendapatkan pembiayaan dari penjamin lain di luar BPJS</span>
                                <br>
                                <span>4. Sanggup dan bersedia membayar seluruh biaya perawatan sesuai dengan kelas yang saya kehendaki.</span>
                                <span>5. Memberi kuasa kepada Dokter KLINIK UTAMA SUKMA WIJAYA untuk memberi keterangan yang diperlukan oleh pihak penanggung biaya perawatan saya/pasien tersebut di atas.</span>
                                <br>
                                <span>Demikian pernyataan ini saya buat dengan penuh kesadaran dan tanpa paksaan pihak manapun.</span>
                                <br>
                                <br>
                                <div style="display: flex">
                                    <div style="flex: 1"></div>
                                    Sampang, <?=date('d-F-Y')?>
                                </div>
                                <br>
                                <div style="display: flex">
                                    <div style="display: flex; flex-direction: column; align-items: center; flex: 1">
                                        <span>Petugas Pendaftaran</span>
                                        <br>
                                        <br>
                                        <br>
                                        <span>(........................................)</span>
                                        <span>Tanda Tangan & Nama Terang</span>
                                    </div>
                                    <div style="display: flex; flex-direction: column; align-items: center; flex: 1">
                                        <span>Yang Membuat Pernyataan</span>
                                        <br>
                                        <br>
                                        <br>
                                        <span>(........................................)</span>
                                        <span>Tanda Tangan & Nama Terang</span>
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
                <div class="text-center">
                    <?php if (superadmin($this->session->userdata('logged_in')) || perawat($this->session->userdata('logged_in'))) : ?>
                        <button type="submit" class="btn btn-primary">Simpan</button>
                    <?php endif; ?>
                </div>
            </form>
        </div>
    </div>
</div>