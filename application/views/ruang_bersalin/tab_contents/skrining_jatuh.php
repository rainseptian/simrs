<div class="tab-pane <?=isset($tab) && $tab == 'skrining_jatuh' ? 'active' : ''?>" id="skrining_jatuh">
    <div class="impbtnview">
        <a href="<?=base_url("RawatInap/print_form/jatuh/$rawat_inap->id")?>" target="_blank" class="btn btn-primary btn-sm">
            <i class="fa fa-print"></i> Cetak
        </a>
    </div>
    <form method="post" action="<?php echo base_url() ?>RawatInap/save_form/jatuh/skrining_jatuh">
        <input type="hidden" name="rawat_inap_id" value="<?=$rawat_inap->id?>">
        <div class="row">
            <div class="col-md-12">
                <table class="table table-bordered table-hover" style="table-layout: fixed">
                    <tbody>
                    <tr>
                        <td colspan="13" class="text-center">
                            <b>PENGKAJIAN PASIEN RESIKO JATUH KHUSUS ANAK</b>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="7">
                            <div style="display: flex; flex-direction: column">
                                <b>Pengkajian pasien resiko jatuh dilakukan pada:</b>
                                <span>Saat pasien MRS</span>
                                <span>Saat terjadi perubahan kondisi pasien / terapi</span>
                                <span>Saat pasien pindah ruang atau departemen lain</span>
                                <span>Setiap 48 jam pada pasien Resiko tinggi </span>
                                <span>Segera setelah terjadi kasus jatuh</span>
                            </div>
                        </td>
                        <td colspan="6" style="vertical-align: middle">
                            <div style="display: flex; flex-direction: column">
                                <b>SKOR</b>
                                <span>7 - 11 RESIKO RENDAH (RR)</span>
                                <span>>= 12 RESIKO TINGGI (RT)</span>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="7" rowspan="2" class="text-center" style="vertical-align: middle">
                            <b>PENGKAJIAN FAKTOR RESIKO</b>
                        </td>
                        <td><b>TGL</b></td>
                        <td>
                            <input type="date" class="form-control" name="form[jatuh][date][1]" value="<?=$form['jatuh']['date']['1']?>">
                        </td>
                        <td>
                            <input type="date" class="form-control" name="form[jatuh][date][2]" value="<?=$form['jatuh']['date']['2']?>">
                        </td>
                        <td>
                            <input type="date" class="form-control" name="form[jatuh][date][3]" value="<?=$form['jatuh']['date']['3']?>">
                        </td>
                        <td>
                            <input type="date" class="form-control" name="form[jatuh][date][4]" value="<?=$form['jatuh']['date']['4']?>">
                        </td>
                        <td>
                            <input type="date" class="form-control" name="form[jatuh][date][5]" value="<?=$form['jatuh']['date']['5']?>">
                        </td>
                    </tr>
                    <tr>
                        <td><b>JAM</b></td>
                        <td>
                            <input type="time" class="form-control" name="form[jatuh][jam][1]" value="<?=$form['jatuh']['jam']['1']?>">
                        </td>
                        <td>
                            <input type="time" class="form-control" name="form[jatuh][jam][2]" value="<?=$form['jatuh']['jam']['2']?>">
                        </td>
                        <td>
                            <input type="time" class="form-control" name="form[jatuh][jam][3]" value="<?=$form['jatuh']['jam']['3']?>">
                        </td>
                        <td>
                            <input type="time" class="form-control" name="form[jatuh][jam][4]" value="<?=$form['jatuh']['jam']['4']?>">
                        </td>
                        <td>
                            <input type="time" class="form-control" name="form[jatuh][jam][5]" value="<?=$form['jatuh']['jam']['5']?>">
                        </td>
                    </tr>
                    <tr>
                        <td rowspan="5" colspan="2" class="text-center" style="vertical-align: middle">
                            UMUR
                        </td>
                        <td colspan="5">Kurang dari 3 tahun</td>
                        <td>4</td>
                        <td><input type="checkbox" value="4" name="form[jatuh][umur_kurang][1]" <?=$form['jatuh']['umur_kurang']['1'] == 4 ? 'checked' : ''?>></td>
                        <td><input type="checkbox" value="4" name="form[jatuh][umur_kurang][2]" <?=$form['jatuh']['umur_kurang']['2'] == 4 ? 'checked' : ''?>></td>
                        <td><input type="checkbox" value="4" name="form[jatuh][umur_kurang][3]" <?=$form['jatuh']['umur_kurang']['3'] == 4 ? 'checked' : ''?>></td>
                        <td><input type="checkbox" value="4" name="form[jatuh][umur_kurang][4]" <?=$form['jatuh']['umur_kurang']['4'] == 4 ? 'checked' : ''?>></td>
                        <td><input type="checkbox" value="4" name="form[jatuh][umur_kurang][5]" <?=$form['jatuh']['umur_kurang']['5'] == 4 ? 'checked' : ''?>></td>
                    </tr>
                    <tr>
                        <td colspan="5">3 - 7 tahun</td>
                        <td>3</td>
                        <td><input type="checkbox" value="3" name="form[jatuh][3-7][1]" <?=$form['jatuh']['3-7']['1'] == 3 ? 'checked' : ''?>></td>
                        <td><input type="checkbox" value="3" name="form[jatuh][3-7][2]" <?=$form['jatuh']['3-7']['2'] == 3 ? 'checked' : ''?>></td>
                        <td><input type="checkbox" value="3" name="form[jatuh][3-7][3]" <?=$form['jatuh']['3-7']['3'] == 3 ? 'checked' : ''?>></td>
                        <td><input type="checkbox" value="3" name="form[jatuh][3-7][4]" <?=$form['jatuh']['3-7']['4'] == 3 ? 'checked' : ''?>></td>
                        <td><input type="checkbox" value="3" name="form[jatuh][3-7][5]" <?=$form['jatuh']['3-7']['5'] == 3 ? 'checked' : ''?>></td>
                    </tr>
                    <tr>
                        <td colspan="5">7 - 13 tahun</td>
                        <td>2</td>
                        <td><input type="checkbox" value="2" name="form[jatuh][7-13][1]" <?=$form['jatuh']['7-13']['1'] == 2 ? 'checked' : ''?>></td>
                        <td><input type="checkbox" value="2" name="form[jatuh][7-13][2]" <?=$form['jatuh']['7-13']['2'] == 2 ? 'checked' : ''?>></td>
                        <td><input type="checkbox" value="2" name="form[jatuh][7-13][3]" <?=$form['jatuh']['7-13']['3'] == 2 ? 'checked' : ''?>></td>
                        <td><input type="checkbox" value="2" name="form[jatuh][7-13][4]" <?=$form['jatuh']['7-13']['4'] == 2 ? 'checked' : ''?>></td>
                        <td><input type="checkbox" value="2" name="form[jatuh][7-13][5]" <?=$form['jatuh']['7-13']['5'] == 2 ? 'checked' : ''?>></td>
                    </tr>
                    <tr>
                        <td colspan="5">13 - 18 tahun</td>
                        <td>1</td>
                        <td><input type="checkbox" value="1" name="form[jatuh][13-18][1]" <?=$form['jatuh']['13-18']['1'] == 1 ? 'checked' : ''?>></td>
                        <td><input type="checkbox" value="1" name="form[jatuh][13-18][2]" <?=$form['jatuh']['13-18']['2'] == 1 ? 'checked' : ''?>></td>
                        <td><input type="checkbox" value="1" name="form[jatuh][13-18][3]" <?=$form['jatuh']['13-18']['3'] == 1 ? 'checked' : ''?>></td>
                        <td><input type="checkbox" value="1" name="form[jatuh][13-18][4]" <?=$form['jatuh']['13-18']['4'] == 1 ? 'checked' : ''?>></td>
                        <td><input type="checkbox" value="1" name="form[jatuh][13-18][5]" <?=$form['jatuh']['13-18']['5'] == 1 ? 'checked' : ''?>></td>
                    </tr>
                    <tr>
                        <td colspan="5"></td>
                        <td><span style="visibility: hidden">1</span></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td rowspan="2" colspan="2" class="text-center" style="vertical-align: middle">
                            JENIS KELAMIN
                        </td>
                        <td colspan="5">Laki laki</td>
                        <td>2</td>
                        <td><input type="checkbox" value="2" name="form[jatuh][laki][1]" <?=$form['jatuh']['laki']['1'] == 1 ? 'checked' : ''?>></td>
                        <td><input type="checkbox" value="2" name="form[jatuh][laki][2]" <?=$form['jatuh']['laki']['2'] == 1 ? 'checked' : ''?>></td>
                        <td><input type="checkbox" value="2" name="form[jatuh][laki][3]" <?=$form['jatuh']['laki']['3'] == 1 ? 'checked' : ''?>></td>
                        <td><input type="checkbox" value="2" name="form[jatuh][laki][4]" <?=$form['jatuh']['laki']['4'] == 1 ? 'checked' : ''?>></td>
                        <td><input type="checkbox" value="2" name="form[jatuh][laki][5]" <?=$form['jatuh']['laki']['5'] == 1 ? 'checked' : ''?>></td>
                    </tr>
                    <tr>
                        <td colspan="5">Perempuan</td>
                        <td>1</td>
                        <td><input type="checkbox" value="1" name="form[jatuh][pr][1]" <?=$form['jatuh']['pr']['1'] == 2 ? 'checked' : ''?>></td>
                        <td><input type="checkbox" value="1" name="form[jatuh][pr][2]" <?=$form['jatuh']['pr']['2'] == 2 ? 'checked' : ''?>></td>
                        <td><input type="checkbox" value="1" name="form[jatuh][pr][3]" <?=$form['jatuh']['pr']['3'] == 2 ? 'checked' : ''?>></td>
                        <td><input type="checkbox" value="1" name="form[jatuh][pr][4]" <?=$form['jatuh']['pr']['4'] == 2 ? 'checked' : ''?>></td>
                        <td><input type="checkbox" value="1" name="form[jatuh][pr][5]" <?=$form['jatuh']['pr']['5'] == 2 ? 'checked' : ''?>></td>
                    </tr>
                    <tr>
                        <td rowspan="4" colspan="2" class="text-center" style="vertical-align: middle">
                            DIAGNOSA
                        </td>
                        <td colspan="5">Neurologi</td>
                        <td>4</td>
                        <td><input type="checkbox" value="4" name="form[jatuh][neurologi][1]" <?=$form['jatuh']['neurologi']['1'] == 4 ? 'checked' : ''?>></td>
                        <td><input type="checkbox" value="4" name="form[jatuh][neurologi][2]" <?=$form['jatuh']['neurologi']['2'] == 4 ? 'checked' : ''?>></td>
                        <td><input type="checkbox" value="4" name="form[jatuh][neurologi][3]" <?=$form['jatuh']['neurologi']['3'] == 4 ? 'checked' : ''?>></td>
                        <td><input type="checkbox" value="4" name="form[jatuh][neurologi][4]" <?=$form['jatuh']['neurologi']['4'] == 4 ? 'checked' : ''?>></td>
                        <td><input type="checkbox" value="4" name="form[jatuh][neurologi][5]" <?=$form['jatuh']['neurologi']['5'] == 4 ? 'checked' : ''?>></td>
                    </tr>
                    <tr>
                        <td colspan="5">Respiratori, dehidrasi, anemia, anoreksia, syncope</td>
                        <td>3</td>
                        <td><input type="checkbox" value="3" name="form[jatuh][respiratori][1]" <?=$form['jatuh']['respiratori']['1'] == 3 ? 'checked' : ''?>></td>
                        <td><input type="checkbox" value="3" name="form[jatuh][respiratori][2]" <?=$form['jatuh']['respiratori']['2'] == 3 ? 'checked' : ''?>></td>
                        <td><input type="checkbox" value="3" name="form[jatuh][respiratori][3]" <?=$form['jatuh']['respiratori']['3'] == 3 ? 'checked' : ''?>></td>
                        <td><input type="checkbox" value="3" name="form[jatuh][respiratori][4]" <?=$form['jatuh']['respiratori']['4'] == 3 ? 'checked' : ''?>></td>
                        <td><input type="checkbox" value="3" name="form[jatuh][respiratori][5]" <?=$form['jatuh']['respiratori']['5'] == 3 ? 'checked' : ''?>></td>
                    </tr>
                    <tr>
                        <td colspan="5">Perilaku</td>
                        <td>2</td>
                        <td><input type="checkbox" value="2" name="form[jatuh][perilaku][1]" <?=$form['jatuh']['perilaku']['1'] == 2 ? 'checked' : ''?>></td>
                        <td><input type="checkbox" value="2" name="form[jatuh][perilaku][2]" <?=$form['jatuh']['perilaku']['2'] == 2 ? 'checked' : ''?>></td>
                        <td><input type="checkbox" value="2" name="form[jatuh][perilaku][3]" <?=$form['jatuh']['perilaku']['3'] == 2 ? 'checked' : ''?>></td>
                        <td><input type="checkbox" value="2" name="form[jatuh][perilaku][4]" <?=$form['jatuh']['perilaku']['4'] == 2 ? 'checked' : ''?>></td>
                        <td><input type="checkbox" value="2" name="form[jatuh][perilaku][5]" <?=$form['jatuh']['perilaku']['5'] == 2 ? 'checked' : ''?>></td>
                    </tr>
                    <tr>
                        <td colspan="5">Lain lain</td>
                        <td>1</td>
                        <td><input type="checkbox" value="1" name="form[jatuh][lain][1]" <?=$form['jatuh']['lain']['1'] == 1 ? 'checked' : ''?>></td>
                        <td><input type="checkbox" value="1" name="form[jatuh][lain][2]" <?=$form['jatuh']['lain']['2'] == 1 ? 'checked' : ''?>></td>
                        <td><input type="checkbox" value="1" name="form[jatuh][lain][3]" <?=$form['jatuh']['lain']['3'] == 1 ? 'checked' : ''?>></td>
                        <td><input type="checkbox" value="1" name="form[jatuh][lain][4]" <?=$form['jatuh']['lain']['4'] == 1 ? 'checked' : ''?>></td>
                        <td><input type="checkbox" value="1" name="form[jatuh][lain][5]" <?=$form['jatuh']['lain']['5'] == 1 ? 'checked' : ''?>></td>
                    </tr>
                    <tr>
                        <td rowspan="3" colspan="2" class="text-center" style="vertical-align: middle">
                            GANGGUAN KOGNITIF
                        </td>
                        <td colspan="5">Keterbatasan daya pikir</td>
                        <td>3</td>
                        <td><input type="checkbox" value="3" name="form[jatuh][daya_pikir][1]" <?=$form['jatuh']['daya_pikir']['1'] == 3 ? 'checked' : ''?>></td>
                        <td><input type="checkbox" value="3" name="form[jatuh][daya_pikir][2]" <?=$form['jatuh']['daya_pikir']['2'] == 3 ? 'checked' : ''?>></td>
                        <td><input type="checkbox" value="3" name="form[jatuh][daya_pikir][3]" <?=$form['jatuh']['daya_pikir']['3'] == 3 ? 'checked' : ''?>></td>
                        <td><input type="checkbox" value="3" name="form[jatuh][daya_pikir][4]" <?=$form['jatuh']['daya_pikir']['4'] == 3 ? 'checked' : ''?>></td>
                        <td><input type="checkbox" value="3" name="form[jatuh][daya_pikir][5]" <?=$form['jatuh']['daya_pikir']['5'] == 3 ? 'checked' : ''?>></td>
                    </tr>
                    <tr>
                        <td colspan="5">Pelupa</td>
                        <td>2</td>
                        <td><input type="checkbox" value="2" name="form[jatuh][pelupa][1]" <?=$form['jatuh']['pelupa']['1'] == 2 ? 'checked' : ''?>></td>
                        <td><input type="checkbox" value="2" name="form[jatuh][pelupa][2]" <?=$form['jatuh']['pelupa']['2'] == 2 ? 'checked' : ''?>></td>
                        <td><input type="checkbox" value="2" name="form[jatuh][pelupa][3]" <?=$form['jatuh']['pelupa']['3'] == 2 ? 'checked' : ''?>></td>
                        <td><input type="checkbox" value="2" name="form[jatuh][pelupa][4]" <?=$form['jatuh']['pelupa']['4'] == 2 ? 'checked' : ''?>></td>
                        <td><input type="checkbox" value="2" name="form[jatuh][pelupa][5]" <?=$form['jatuh']['pelupa']['5'] == 2 ? 'checked' : ''?>></td>
                    </tr>
                    <tr>
                        <td colspan="5">Dapat menggunakan daya piker tanpa hambatan</td>
                        <td>1</td>
                        <td><input type="checkbox" value="1" name="form[jatuh][piker][1]" <?=$form['jatuh']['piker']['1'] == 1 ? 'checked' : ''?>></td>
                        <td><input type="checkbox" value="1" name="form[jatuh][piker][2]" <?=$form['jatuh']['piker']['2'] == 1 ? 'checked' : ''?>></td>
                        <td><input type="checkbox" value="1" name="form[jatuh][piker][3]" <?=$form['jatuh']['piker']['3'] == 1 ? 'checked' : ''?>></td>
                        <td><input type="checkbox" value="1" name="form[jatuh][piker][4]" <?=$form['jatuh']['piker']['4'] == 1 ? 'checked' : ''?>></td>
                        <td><input type="checkbox" value="1" name="form[jatuh][piker][5]" <?=$form['jatuh']['piker']['5'] == 1 ? 'checked' : ''?>></td>
                    </tr>
                    <tr>
                        <td rowspan="4" colspan="2" class="text-center" style="vertical-align: middle">
                            FAKTOR LINGKUNGAN
                        </td>
                        <td colspan="5">Riwayat jatuh pada bayi/balita yang ditempatkan di tempat tidur</td>
                        <td>4</td>
                        <td><input type="checkbox" value="4" name="form[jatuh][rjpb][1]" <?=$form['jatuh']['rjpb']['1'] == 4 ? 'checked' : ''?>></td>
                        <td><input type="checkbox" value="4" name="form[jatuh][rjpb][2]" <?=$form['jatuh']['rjpb']['2'] == 4 ? 'checked' : ''?>></td>
                        <td><input type="checkbox" value="4" name="form[jatuh][rjpb][3]" <?=$form['jatuh']['rjpb']['3'] == 4 ? 'checked' : ''?>></td>
                        <td><input type="checkbox" value="4" name="form[jatuh][rjpb][4]" <?=$form['jatuh']['rjpb']['4'] == 4 ? 'checked' : ''?>></td>
                        <td><input type="checkbox" value="4" name="form[jatuh][rjpb][5]" <?=$form['jatuh']['rjpb']['5'] == 4 ? 'checked' : ''?>></td>
                    </tr>
                    <tr>
                        <td colspan="5">Pasien yang menggunakan alat bantu/bayi atau balita pada ayunan</td>
                        <td>3</td>
                        <td><input type="checkbox" value="3" name="form[jatuh][pyma][1]" <?=$form['jatuh']['pyma']['1'] == 3 ? 'checked' : ''?>></td>
                        <td><input type="checkbox" value="3" name="form[jatuh][pyma][2]" <?=$form['jatuh']['pyma']['2'] == 3 ? 'checked' : ''?>></td>
                        <td><input type="checkbox" value="3" name="form[jatuh][pyma][3]" <?=$form['jatuh']['pyma']['3'] == 3 ? 'checked' : ''?>></td>
                        <td><input type="checkbox" value="3" name="form[jatuh][pyma][4]" <?=$form['jatuh']['pyma']['4'] == 3 ? 'checked' : ''?>></td>
                        <td><input type="checkbox" value="3" name="form[jatuh][pyma][5]" <?=$form['jatuh']['pyma']['5'] == 3 ? 'checked' : ''?>></td>
                    </tr>
                    <tr>
                        <td colspan="5">Pasien di tempat tidur standar</td>
                        <td>2</td>
                        <td><input type="checkbox" value="2" name="form[jatuh][pdtt][1]" <?=$form['jatuh']['pdtt']['1'] == 2 ? 'checked' : ''?>></td>
                        <td><input type="checkbox" value="2" name="form[jatuh][pdtt][2]" <?=$form['jatuh']['pdtt']['2'] == 2 ? 'checked' : ''?>></td>
                        <td><input type="checkbox" value="2" name="form[jatuh][pdtt][3]" <?=$form['jatuh']['pdtt']['3'] == 2 ? 'checked' : ''?>></td>
                        <td><input type="checkbox" value="2" name="form[jatuh][pdtt][4]" <?=$form['jatuh']['pdtt']['4'] == 2 ? 'checked' : ''?>></td>
                        <td><input type="checkbox" value="2" name="form[jatuh][pdtt][5]" <?=$form['jatuh']['pdtt']['5'] == 2 ? 'checked' : ''?>></td>
                    </tr>
                    <tr>
                        <td colspan="5">Area pasien rawat jalan</td>
                        <td>1</td>
                        <td><input type="checkbox" value="1" name="form[jatuh][aprj][1]" <?=$form['jatuh']['aprj']['1'] == 1 ? 'checked' : ''?>></td>
                        <td><input type="checkbox" value="1" name="form[jatuh][aprj][2]" <?=$form['jatuh']['aprj']['2'] == 1 ? 'checked' : ''?>></td>
                        <td><input type="checkbox" value="1" name="form[jatuh][aprj][3]" <?=$form['jatuh']['aprj']['3'] == 1 ? 'checked' : ''?>></td>
                        <td><input type="checkbox" value="1" name="form[jatuh][aprj][4]" <?=$form['jatuh']['aprj']['4'] == 1 ? 'checked' : ''?>></td>
                        <td><input type="checkbox" value="1" name="form[jatuh][aprj][5]" <?=$form['jatuh']['aprj']['5'] == 1 ? 'checked' : ''?>></td>
                    </tr>
                    <tr>
                        <td rowspan="3" colspan="2" class="text-center" style="vertical-align: middle">
                            RESPON TERHADAP PEMBEDAHAN SEDASI DAN ANESTESI
                        </td>
                        <td colspan="5">Dalam 24 jam</td>
                        <td>3</td>
                        <td><input type="checkbox" value="3" name="form[jatuh][d24][1]" <?=$form['jatuh']['d24']['1'] == 3 ? 'checked' : ''?>></td>
                        <td><input type="checkbox" value="3" name="form[jatuh][d24][2]" <?=$form['jatuh']['d24']['2'] == 3 ? 'checked' : ''?>></td>
                        <td><input type="checkbox" value="3" name="form[jatuh][d24][3]" <?=$form['jatuh']['d24']['3'] == 3 ? 'checked' : ''?>></td>
                        <td><input type="checkbox" value="3" name="form[jatuh][d24][4]" <?=$form['jatuh']['d24']['4'] == 3 ? 'checked' : ''?>></td>
                        <td><input type="checkbox" value="3" name="form[jatuh][d24][5]" <?=$form['jatuh']['d24']['5'] == 3 ? 'checked' : ''?>></td>
                    </tr>
                    <tr>
                        <td colspan="5">Dalam 48 jam</td>
                        <td>2</td>
                        <td><input type="checkbox" value="2" name="form[jatuh][d48][1]" <?=$form['jatuh']['d48']['1'] == 2 ? 'checked' : ''?>></td>
                        <td><input type="checkbox" value="2" name="form[jatuh][d48][2]" <?=$form['jatuh']['d48']['2'] == 2 ? 'checked' : ''?>></td>
                        <td><input type="checkbox" value="2" name="form[jatuh][d48][3]" <?=$form['jatuh']['d48']['3'] == 2 ? 'checked' : ''?>></td>
                        <td><input type="checkbox" value="2" name="form[jatuh][d48][4]" <?=$form['jatuh']['d48']['4'] == 2 ? 'checked' : ''?>></td>
                        <td><input type="checkbox" value="2" name="form[jatuh][d48][5]" <?=$form['jatuh']['d48']['5'] == 2 ? 'checked' : ''?>></td>
                    </tr>
                    <tr>
                        <td colspan="5">Lebih dari 48 jam/ tidak ada respon</td>
                        <td>1</td>
                        <td><input type="checkbox" value="1" name="form[jatuh][l48][1]" <?=$form['jatuh']['l48']['1'] == 1 ? 'checked' : ''?>></td>
                        <td><input type="checkbox" value="1" name="form[jatuh][l48][2]" <?=$form['jatuh']['l48']['2'] == 1 ? 'checked' : ''?>></td>
                        <td><input type="checkbox" value="1" name="form[jatuh][l48][3]" <?=$form['jatuh']['l48']['3'] == 1 ? 'checked' : ''?>></td>
                        <td><input type="checkbox" value="1" name="form[jatuh][l48][4]" <?=$form['jatuh']['l48']['4'] == 1 ? 'checked' : ''?>></td>
                        <td><input type="checkbox" value="1" name="form[jatuh][l48][5]" <?=$form['jatuh']['l48']['5'] == 1 ? 'checked' : ''?>></td>
                    </tr>
                    <tr>
                        <td rowspan="3" colspan="2" class="text-center" style="vertical-align: middle">
                            PENGGUNAAN OBAT-OBATAN
                        </td>
                        <td colspan="5">Penggunaan bersamaan sedativem barbiturate, anti depresan, diuretic, narkotik</td>
                        <td>3</td>
                        <td><input type="checkbox" value="3" name="form[jatuh][narkotix][1]" <?=$form['jatuh']['narkotix']['1'] == 3 ? 'checked' : ''?>></td>
                        <td><input type="checkbox" value="3" name="form[jatuh][narkotix][2]" <?=$form['jatuh']['narkotix']['2'] == 3 ? 'checked' : ''?>></td>
                        <td><input type="checkbox" value="3" name="form[jatuh][narkotix][3]" <?=$form['jatuh']['narkotix']['3'] == 3 ? 'checked' : ''?>></td>
                        <td><input type="checkbox" value="3" name="form[jatuh][narkotix][4]" <?=$form['jatuh']['narkotix']['4'] == 3 ? 'checked' : ''?>></td>
                        <td><input type="checkbox" value="3" name="form[jatuh][narkotix][5]" <?=$form['jatuh']['narkotix']['5'] == 3 ? 'checked' : ''?>></td>
                    </tr>
                    <tr>
                        <td colspan="5">Salah satu dari obat di atas</td>
                        <td>2</td>
                        <td><input type="checkbox" value="2" name="form[jatuh][salah_one][1]" <?=$form['jatuh']['salah_one']['1'] == 2 ? 'checked' : ''?>></td>
                        <td><input type="checkbox" value="2" name="form[jatuh][salah_one][2]" <?=$form['jatuh']['salah_one']['2'] == 2 ? 'checked' : ''?>></td>
                        <td><input type="checkbox" value="2" name="form[jatuh][salah_one][3]" <?=$form['jatuh']['salah_one']['3'] == 2 ? 'checked' : ''?>></td>
                        <td><input type="checkbox" value="2" name="form[jatuh][salah_one][4]" <?=$form['jatuh']['salah_one']['4'] == 2 ? 'checked' : ''?>></td>
                        <td><input type="checkbox" value="2" name="form[jatuh][salah_one][5]" <?=$form['jatuh']['salah_one']['5'] == 2 ? 'checked' : ''?>></td>
                    </tr>
                    <tr>
                        <td colspan="5">Obat-obatan lainnya/tanpa obat</td>
                        <td>1</td>
                        <td><input type="checkbox" value="1" name="form[jatuh][obat_lain_or_without][1]" <?=$form['jatuh']['obat_lain_or_without']['1'] == 1 ? 'checked' : ''?>></td>
                        <td><input type="checkbox" value="1" name="form[jatuh][obat_lain_or_without][2]" <?=$form['jatuh']['obat_lain_or_without']['2'] == 1 ? 'checked' : ''?>></td>
                        <td><input type="checkbox" value="1" name="form[jatuh][obat_lain_or_without][3]" <?=$form['jatuh']['obat_lain_or_without']['3'] == 1 ? 'checked' : ''?>></td>
                        <td><input type="checkbox" value="1" name="form[jatuh][obat_lain_or_without][4]" <?=$form['jatuh']['obat_lain_or_without']['4'] == 1 ? 'checked' : ''?>></td>
                        <td><input type="checkbox" value="1" name="form[jatuh][obat_lain_or_without][5]" <?=$form['jatuh']['obat_lain_or_without']['5'] == 1 ? 'checked' : ''?>></td>
                    </tr>
                    <tr>
                        <td colspan="7" rowspan="1" class="text-center" style="vertical-align: middle">
                            <b>TOTAL SKOR</b>
                        </td>
                        <td><b>51</b></td>
                        <td>
                            <input type="text" class="form-control" name="form[jatuh][total_skor_51][1]" value="<?=$form['jatuh']['total_skor_51']['1']?>">
                        </td>
                        <td>
                            <input type="text" class="form-control" name="form[jatuh][total_skor_51][2]" value="<?=$form['jatuh']['total_skor_51']['2']?>">
                        </td>
                        <td>
                            <input type="text" class="form-control" name="form[jatuh][total_skor_51][3]" value="<?=$form['jatuh']['total_skor_51']['3']?>">
                        </td>
                        <td>
                            <input type="text" class="form-control" name="form[jatuh][total_skor_51][4]" value="<?=$form['jatuh']['total_skor_51']['4']?>">
                        </td>
                        <td>
                            <input type="text" class="form-control" name="form[jatuh][total_skor_51][5]" value="<?=$form['jatuh']['total_skor_51']['5']?>">
                        </td>
                    </tr>
                    <tr>
                        <td colspan="7" rowspan="2" class="text-center" style="vertical-align: middle">
                            <b>LINGKARI RISIKO JATUH BERDASARKAN SKOR</b>
                        </td>
                        <td><b>RR</b></td>
                        <td><b>RR</b></td>
                        <td><b>RR</b></td>
                        <td><b>RR</b></td>
                        <td><b>RR</b></td>
                        <td><b>RR</b></td>
                    </tr>
                    <tr>
                        <td><b>RT</b></td>
                        <td><b>RT</b></td>
                        <td><b>RT</b></td>
                        <td><b>RT</b></td>
                        <td><b>RT</b></td>
                        <td><b>RT</b></td>
                    </tr>
                    <tr>
                        <td colspan="7" rowspan="1" class="text-center" style="vertical-align: middle">
                            <b>PARAF DAN INISIAL PERAWAT</b>
                        </td>
                        <td></td>
                        <td>
                            <input type="text" class="form-control" name="form[jatuh][paraf_and_initial][1]" value="<?=$form['jatuh']['paraf_and_initial']['1']?>">
                        </td>
                        <td>
                            <input type="text" class="form-control" name="form[jatuh][paraf_and_initial][2]" value="<?=$form['jatuh']['paraf_and_initial']['2']?>">
                        </td>
                        <td>
                            <input type="text" class="form-control" name="form[jatuh][paraf_and_initial][3]" value="<?=$form['jatuh']['paraf_and_initial']['3']?>">
                        </td>
                        <td>
                            <input type="text" class="form-control" name="form[jatuh][paraf_and_initial][4]" value="<?=$form['jatuh']['paraf_and_initial']['4']?>">
                        </td>
                        <td>
                            <input type="text" class="form-control" name="form[jatuh][paraf_and_initial][5]" value="<?=$form['jatuh']['paraf_and_initial']['5']?>">
                        </td>
                    </tr>
                    <tr>
                        <td colspan="6" rowspan="2">
                            Beri tanda (V) jika sudah dilakukan, (-) jika belum atau tidak dilakukan, (*) jika pasien menolak sekaligus beriksan penjelasannya
                        </td>
                        <td class="text-center" style="vertical-align: middle">
                            <b>TGL</b>
                        </td>
                        <td>
                            <input type="date" class="form-control" name="form[jatuh][tgl_v][0]" value="<?=$form['jatuh']['tgl_v']['0']?>">
                        </td>
                        <td>
                            <input type="date" class="form-control" name="form[jatuh][tgl_v][1]" value="<?=$form['jatuh']['tgl_v']['1']?>">
                        </td>
                        <td>
                            <input type="date" class="form-control" name="form[jatuh][tgl_v][2]" value="<?=$form['jatuh']['tgl_v']['2']?>">
                        </td>
                        <td>
                            <input type="date" class="form-control" name="form[jatuh][tgl_v][3]" value="<?=$form['jatuh']['tgl_v']['3']?>">
                        </td>
                        <td>
                            <input type="date" class="form-control" name="form[jatuh][tgl_v][4]" value="<?=$form['jatuh']['tgl_v']['4']?>">
                        </td>
                        <td>
                            <input type="date" class="form-control" name="form[jatuh][tgl_v][5]" value="<?=$form['jatuh']['tgl_v']['5']?>">
                        </td>
                    </tr>
                    <tr>
                        <td class="text-center" style="vertical-align: middle">
                            <b>JAM</b>
                        </td>
                        <td>
                            <input type="time" class="form-control" name="form[jatuh][jam_v][0]" value="<?=$form['jatuh']['jam_v']['0']?>">
                        </td>
                        <td>
                            <input type="time" class="form-control" name="form[jatuh][jam_v][1]" value="<?=$form['jatuh']['jam_v']['1']?>">
                        </td>
                        <td>
                            <input type="time" class="form-control" name="form[jatuh][jam_v][2]" value="<?=$form['jatuh']['jam_v']['2']?>">
                        </td>
                        <td>
                            <input type="time" class="form-control" name="form[jatuh][jam_v][3]" value="<?=$form['jatuh']['jam_v']['3']?>">
                        </td>
                        <td>
                            <input type="time" class="form-control" name="form[jatuh][jam_v][4]" value="<?=$form['jatuh']['jam_v']['4']?>">
                        </td>
                        <td>
                            <input type="time" class="form-control" name="form[jatuh][jam_v][5]" value="<?=$form['jatuh']['jam_v']['5']?>">
                        </td>
                    </tr>
                    <tr>
                        <td>a.</td>
                        <td colspan="6">
                            Memastikan tempat tidur/brankard dalam posisi roda terkunci
                        </td>
                        <td><input type="text" name="form[jatuh][input2][a][1]" value="<?=$form['jatuh']['input2']['a']['1']?>" class="form-control"></td>
                        <td><input type="text" name="form[jatuh][input2][a][2]" value="<?=$form['jatuh']['input2']['a']['2']?>" class="form-control"></td>
                        <td><input type="text" name="form[jatuh][input2][a][3]" value="<?=$form['jatuh']['input2']['a']['3']?>" class="form-control"></td>
                        <td><input type="text" name="form[jatuh][input2][a][4]" value="<?=$form['jatuh']['input2']['a']['4']?>" class="form-control"></td>
                        <td><input type="text" name="form[jatuh][input2][a][5]" value="<?=$form['jatuh']['input2']['a']['5']?>" class="form-control"></td>
                        <td><input type="text" name="form[jatuh][input2][a][6]" value="<?=$form['jatuh']['input2']['a']['6']?>" class="form-control"></td>
                    </tr>
                    <tr>
                        <td>b.</td>
                        <td colspan="6">
                            Pagar sisi tempat tidru/brankard dalam posisi berdiri/terpasang
                        </td>
                        <td><input type="text" name="form[jatuh][input2][b][1]" value="<?=$form['jatuh']['input2']['b']['1']?>" class="form-control"></td>
                        <td><input type="text" name="form[jatuh][input2][b][2]" value="<?=$form['jatuh']['input2']['b']['2']?>" class="form-control"></td>
                        <td><input type="text" name="form[jatuh][input2][b][3]" value="<?=$form['jatuh']['input2']['b']['3']?>" class="form-control"></td>
                        <td><input type="text" name="form[jatuh][input2][b][4]" value="<?=$form['jatuh']['input2']['b']['4']?>" class="form-control"></td>
                        <td><input type="text" name="form[jatuh][input2][b][5]" value="<?=$form['jatuh']['input2']['b']['5']?>" class="form-control"></td>
                        <td><input type="text" name="form[jatuh][input2][b][6]" value="<?=$form['jatuh']['input2']['b']['6']?>" class="form-control"></td>
                    </tr>
                    <tr>
                        <td>c.</td>
                        <td colspan="6">
                            Lingkungan bebeas dari peralatan yang tidak digunakan
                        </td>
                        <td><input type="text" name="form[jatuh][input2][c][1]" value="<?=$form['jatuh']['input2']['c']['1']?>" class="form-control"></td>
                        <td><input type="text" name="form[jatuh][input2][c][2]" value="<?=$form['jatuh']['input2']['c']['2']?>" class="form-control"></td>
                        <td><input type="text" name="form[jatuh][input2][c][3]" value="<?=$form['jatuh']['input2']['c']['3']?>" class="form-control"></td>
                        <td><input type="text" name="form[jatuh][input2][c][4]" value="<?=$form['jatuh']['input2']['c']['4']?>" class="form-control"></td>
                        <td><input type="text" name="form[jatuh][input2][c][5]" value="<?=$form['jatuh']['input2']['c']['5']?>" class="form-control"></td>
                        <td><input type="text" name="form[jatuh][input2][c][6]" value="<?=$form['jatuh']['input2']['c']['6']?>" class="form-control"></td>
                    </tr>
                    <tr>
                        <td>d.</td>
                        <td colspan="6">
                            Berikan penjelasan kepada orang tua tentang pencegahan jatuh
                        </td>
                        <td><input type="text" name="form[jatuh][input2][d][1]" value="<?=$form['jatuh']['input2']['d']['1']?>" class="form-control"></td>
                        <td><input type="text" name="form[jatuh][input2][d][2]" value="<?=$form['jatuh']['input2']['d']['2']?>" class="form-control"></td>
                        <td><input type="text" name="form[jatuh][input2][d][3]" value="<?=$form['jatuh']['input2']['d']['3']?>" class="form-control"></td>
                        <td><input type="text" name="form[jatuh][input2][d][4]" value="<?=$form['jatuh']['input2']['d']['4']?>" class="form-control"></td>
                        <td><input type="text" name="form[jatuh][input2][d][5]" value="<?=$form['jatuh']['input2']['d']['5']?>" class="form-control"></td>
                        <td><input type="text" name="form[jatuh][input2][d][6]" value="<?=$form['jatuh']['input2']['d']['6']?>" class="form-control"></td>
                    </tr>
                    <tr>
                        <td>e.</td>
                        <td colspan="6">
                            Pastikan pasien sudah memakai gelang kuning penanda pasien resiko jatuh dan tanda kewaspadaan pada panel informasi pasien
                        </td>
                        <td><input type="text" name="form[jatuh][input2][e][1]" value="<?=$form['jatuh']['input2']['e']['1']?>" class="form-control"></td>
                        <td><input type="text" name="form[jatuh][input2][e][2]" value="<?=$form['jatuh']['input2']['e']['2']?>" class="form-control"></td>
                        <td><input type="text" name="form[jatuh][input2][e][3]" value="<?=$form['jatuh']['input2']['e']['3']?>" class="form-control"></td>
                        <td><input type="text" name="form[jatuh][input2][e][4]" value="<?=$form['jatuh']['input2']['e']['4']?>" class="form-control"></td>
                        <td><input type="text" name="form[jatuh][input2][e][5]" value="<?=$form['jatuh']['input2']['e']['5']?>" class="form-control"></td>
                        <td><input type="text" name="form[jatuh][input2][e][6]" value="<?=$form['jatuh']['input2']['e']['6']?>" class="form-control"></td>
                    </tr>
                    <tr>
                        <td colspan="7" rowspan="1" class="text-center" style="vertical-align: middle">
                            <b>PARAF DAN INISIAL PERAWAT</b>
                        </td>
                        <td>
                            <input type="text" class="form-control" name="form[jatuh][paraf_and_initial2][1]" value="<?=$form['jatuh']['paraf_and_initial2']['1']?>">
                        </td>
                        <td>
                            <input type="text" class="form-control" name="form[jatuh][paraf_and_initial2][2]" value="<?=$form['jatuh']['paraf_and_initial2']['2']?>">
                        </td>
                        <td>
                            <input type="text" class="form-control" name="form[jatuh][paraf_and_initial2][3]" value="<?=$form['jatuh']['paraf_and_initial2']['3']?>">
                        </td>
                        <td>
                            <input type="text" class="form-control" name="form[jatuh][paraf_and_initial2][4]" value="<?=$form['jatuh']['paraf_and_initial2']['4']?>">
                        </td>
                        <td>
                            <input type="text" class="form-control" name="form[jatuh][paraf_and_initial2][5]" value="<?=$form['jatuh']['paraf_and_initial2']['5']?>">
                        </td>
                        <td>
                            <input type="text" class="form-control" name="form[jatuh][paraf_and_initial2][7]" value="<?=$form['jatuh']['paraf_and_initial2']['7']?>">
                        </td>
                    </tr>
                    <tr>
                        <td>a.</td>
                        <td colspan="6">
                            Memastikan tempat tidur/brankard dalam posisi roda terkunci
                        </td>
                        <td><input type="text" name="form[jatuh][input3][a][1]" value="<?=$form['jatuh']['input3']['a']['1']?>" class="form-control"></td>
                        <td><input type="text" name="form[jatuh][input3][a][2]" value="<?=$form['jatuh']['input3']['a']['2']?>" class="form-control"></td>
                        <td><input type="text" name="form[jatuh][input3][a][3]" value="<?=$form['jatuh']['input3']['a']['3']?>" class="form-control"></td>
                        <td><input type="text" name="form[jatuh][input3][a][4]" value="<?=$form['jatuh']['input3']['a']['4']?>" class="form-control"></td>
                        <td><input type="text" name="form[jatuh][input3][a][5]" value="<?=$form['jatuh']['input3']['a']['5']?>" class="form-control"></td>
                        <td><input type="text" name="form[jatuh][input3][a][6]" value="<?=$form['jatuh']['input3']['a']['6']?>" class="form-control"></td>
                    </tr>
                    <tr>
                        <td>b.</td>
                        <td colspan="6">
                            Pagar sisi tempat tidru/brankard dalam posisi berdiri/terpasang
                        </td>
                        <td><input type="text" name="form[jatuh][input3][b][1]" value="<?=$form['jatuh']['input3']['b']['1']?>" class="form-control"></td>
                        <td><input type="text" name="form[jatuh][input3][b][2]" value="<?=$form['jatuh']['input3']['b']['2']?>" class="form-control"></td>
                        <td><input type="text" name="form[jatuh][input3][b][3]" value="<?=$form['jatuh']['input3']['b']['3']?>" class="form-control"></td>
                        <td><input type="text" name="form[jatuh][input3][b][4]" value="<?=$form['jatuh']['input3']['b']['4']?>" class="form-control"></td>
                        <td><input type="text" name="form[jatuh][input3][b][5]" value="<?=$form['jatuh']['input3']['b']['5']?>" class="form-control"></td>
                        <td><input type="text" name="form[jatuh][input3][b][6]" value="<?=$form['jatuh']['input3']['b']['6']?>" class="form-control"></td>
                    </tr>
                    <tr>
                        <td>c.</td>
                        <td colspan="6">
                            Lingkungan bebeas dari peralatan yang tidak digunakan
                        </td>
                        <td><input type="text" name="form[jatuh][input3][c][1]" value="<?=$form['jatuh']['input3']['c']['1']?>" class="form-control"></td>
                        <td><input type="text" name="form[jatuh][input3][c][2]" value="<?=$form['jatuh']['input3']['c']['2']?>" class="form-control"></td>
                        <td><input type="text" name="form[jatuh][input3][c][3]" value="<?=$form['jatuh']['input3']['c']['3']?>" class="form-control"></td>
                        <td><input type="text" name="form[jatuh][input3][c][4]" value="<?=$form['jatuh']['input3']['c']['4']?>" class="form-control"></td>
                        <td><input type="text" name="form[jatuh][input3][c][5]" value="<?=$form['jatuh']['input3']['c']['5']?>" class="form-control"></td>
                        <td><input type="text" name="form[jatuh][input3][c][6]" value="<?=$form['jatuh']['input3']['c']['6']?>" class="form-control"></td>
                    </tr>
                    <tr>
                        <td>d.</td>
                        <td colspan="6">
                            Berikan penjelasan kepada orang tua tentang pencegahan jatuh
                        </td>
                        <td><input type="text" name="form[jatuh][input3][d][1]" value="<?=$form['jatuh']['input3']['d']['1']?>" class="form-control"></td>
                        <td><input type="text" name="form[jatuh][input3][d][2]" value="<?=$form['jatuh']['input3']['d']['2']?>" class="form-control"></td>
                        <td><input type="text" name="form[jatuh][input3][d][3]" value="<?=$form['jatuh']['input3']['d']['3']?>" class="form-control"></td>
                        <td><input type="text" name="form[jatuh][input3][d][4]" value="<?=$form['jatuh']['input3']['d']['4']?>" class="form-control"></td>
                        <td><input type="text" name="form[jatuh][input3][d][5]" value="<?=$form['jatuh']['input3']['d']['5']?>" class="form-control"></td>
                        <td><input type="text" name="form[jatuh][input3][d][6]" value="<?=$form['jatuh']['input3']['d']['6']?>" class="form-control"></td>
                    </tr>
                    <tr>
                        <td colspan="7" rowspan="1" class="text-center" style="vertical-align: middle">
                            <b>PARAF DAN INISIAL PERAWAT</b>
                        </td>
                        <td>
                            <input type="text" class="form-control" name="form[jatuh][paraf_and_initial3][1]" value="<?=$form['jatuh']['paraf_and_initial3']['1']?>">
                        </td>
                        <td>
                            <input type="text" class="form-control" name="form[jatuh][paraf_and_initial3][2]" value="<?=$form['jatuh']['paraf_and_initial3']['2']?>">
                        </td>
                        <td>
                            <input type="text" class="form-control" name="form[jatuh][paraf_and_initial3][3]" value="<?=$form['jatuh']['paraf_and_initial3']['3']?>">
                        </td>
                        <td>
                            <input type="text" class="form-control" name="form[jatuh][paraf_and_initial3][4]" value="<?=$form['jatuh']['paraf_and_initial3']['4']?>">
                        </td>
                        <td>
                            <input type="text" class="form-control" name="form[jatuh][paraf_and_initial3][5]" value="<?=$form['jatuh']['paraf_and_initial3']['5']?>">
                        </td>
                        <td>
                            <input type="text" class="form-control" name="form[jatuh][paraf_and_initial3][7]" value="<?=$form['jatuh']['paraf_and_initial3']['7']?>">
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <table class="table table-bordered table-hover" style="table-layout: fixed">
                    <tbody>
                    <tr>
                        <td colspan="13" class="text-center">
                            <b>PENGKAJIAN PASIEN RESIKO JATUH KHUSUS DEWASA</b>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="7" rowspan="2" class="text-center" style="vertical-align: middle">
                            <b>ASESMEN FAKTOR RESIKO JATUH DEWASA</b>
                        </td>
                        <td><b>TGL/JAM</b></td>
                        <td>
                            <input type="datetime-local" class="form-control" name="form[jatuh][date_dewasa][1]" value="<?=$form['jatuh']['date_dewasa']['1']?>">
                        </td>
                        <td>
                            <input type="datetime-local" class="form-control" name="form[jatuh][date_dewasa][2]" value="<?=$form['jatuh']['date_dewasa']['2']?>">
                        </td>
                        <td>
                            <input type="datetime-local" class="form-control" name="form[jatuh][date_dewasa][3]" value="<?=$form['jatuh']['date_dewasa']['3']?>">
                        </td>
                        <td>
                            <input type="datetime-local" class="form-control" name="form[jatuh][date_dewasa][4]" value="<?=$form['jatuh']['date_dewasa']['4']?>">
                        </td>
                        <td>
                            <input type="datetime-local" class="form-control" name="form[jatuh][date_dewasa][5]" value="<?=$form['jatuh']['date_dewasa']['5']?>">
                        </td>
                    </tr>
                    <tr>
                        <td><b>SKOR</b></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td rowspan="2" colspan="3" class="text-center" style="vertical-align: middle">
                            Riwayat jatuh dalam 3 biulan sebab apapun
                        </td>
                        <td colspan="4">TIDAK</td>
                        <td>0</td>
                        <td><input type="checkbox" value="-1" name="form[jatuh][dd3bt][1]" <?=$form['jatuh']['dd3bt']['1'] == -1 ? 'checked' : ''?>></td>
                        <td><input type="checkbox" value="-1" name="form[jatuh][dd3bt][2]" <?=$form['jatuh']['dd3bt']['2'] == -1 ? 'checked' : ''?>></td>
                        <td><input type="checkbox" value="-1" name="form[jatuh][dd3bt][3]" <?=$form['jatuh']['dd3bt']['3'] == -1 ? 'checked' : ''?>></td>
                        <td><input type="checkbox" value="-1" name="form[jatuh][dd3bt][4]" <?=$form['jatuh']['dd3bt']['4'] == -1 ? 'checked' : ''?>></td>
                        <td><input type="checkbox" value="-1" name="form[jatuh][dd3bt][5]" <?=$form['jatuh']['dd3bt']['5'] == -1 ? 'checked' : ''?>></td>
                    </tr>
                    <tr>
                        <td colspan="4">YA</td>
                        <td>25</td>
                        <td><input type="checkbox" value="25" name="form[jatuh][dd3by][1]" <?=$form['jatuh']['dd3by']['1'] == 25 ? 'checked' : ''?>></td>
                        <td><input type="checkbox" value="25" name="form[jatuh][dd3by][2]" <?=$form['jatuh']['dd3by']['2'] == 25 ? 'checked' : ''?>></td>
                        <td><input type="checkbox" value="25" name="form[jatuh][dd3by][3]" <?=$form['jatuh']['dd3by']['3'] == 25 ? 'checked' : ''?>></td>
                        <td><input type="checkbox" value="25" name="form[jatuh][dd3by][4]" <?=$form['jatuh']['dd3by']['4'] == 25 ? 'checked' : ''?>></td>
                        <td><input type="checkbox" value="25" name="form[jatuh][dd3by][5]" <?=$form['jatuh']['dd3by']['5'] == 25 ? 'checked' : ''?>></td>
                    </tr>
                    <tr>
                        <td rowspan="2" colspan="3" class="text-center" style="vertical-align: middle">
                            Diagnosa Sekunder, apakah memiliki lebih dari satu penyakit?
                        </td>
                        <td colspan="4">TIDAK</td>
                        <td>0</td>
                        <td><input type="checkbox" value="-1" name="form[jatuh][dsamt][1]" <?=$form['jatuh']['dsamt']['1'] == -1 ? 'checked' : ''?>></td>
                        <td><input type="checkbox" value="-1" name="form[jatuh][dsamt][2]" <?=$form['jatuh']['dsamt']['2'] == -1 ? 'checked' : ''?>></td>
                        <td><input type="checkbox" value="-1" name="form[jatuh][dsamt][3]" <?=$form['jatuh']['dsamt']['3'] == -1 ? 'checked' : ''?>></td>
                        <td><input type="checkbox" value="-1" name="form[jatuh][dsamt][4]" <?=$form['jatuh']['dsamt']['4'] == -1 ? 'checked' : ''?>></td>
                        <td><input type="checkbox" value="-1" name="form[jatuh][dsamt][5]" <?=$form['jatuh']['dsamt']['5'] == -1 ? 'checked' : ''?>></td>
                    </tr>
                    <tr>
                        <td colspan="4">YA</td>
                        <td>15</td>
                        <td><input type="checkbox" value="15" name="form[jatuh][dsamy][1]" <?=$form['jatuh']['dsamy']['1'] == 15 ? 'checked' : ''?>></td>
                        <td><input type="checkbox" value="15" name="form[jatuh][dsamy][2]" <?=$form['jatuh']['dsamy']['2'] == 15 ? 'checked' : ''?>></td>
                        <td><input type="checkbox" value="15" name="form[jatuh][dsamy][3]" <?=$form['jatuh']['dsamy']['3'] == 15 ? 'checked' : ''?>></td>
                        <td><input type="checkbox" value="15" name="form[jatuh][dsamy][4]" <?=$form['jatuh']['dsamy']['4'] == 15 ? 'checked' : ''?>></td>
                        <td><input type="checkbox" value="15" name="form[jatuh][dsamy][5]" <?=$form['jatuh']['dsamy']['5'] == 15 ? 'checked' : ''?>></td>
                    </tr>
                    <tr>
                        <td rowspan="3" colspan="3" class="text-center" style="vertical-align: middle">
                            Menggunakan Alat Bantu
                        </td>
                        <td colspan="4">Tidak ada/ Bedrest/ Dibantu Perawat</td>
                        <td>0</td>
                        <td><input type="checkbox" value="-1" name="form[jatuh][dmab1][1]" <?=$form['jatuh']['dmab1']['1'] == -1 ? 'checked' : ''?>></td>
                        <td><input type="checkbox" value="-1" name="form[jatuh][dmab1][2]" <?=$form['jatuh']['dmab1']['2'] == -1 ? 'checked' : ''?>></td>
                        <td><input type="checkbox" value="-1" name="form[jatuh][dmab1][3]" <?=$form['jatuh']['dmab1']['3'] == -1 ? 'checked' : ''?>></td>
                        <td><input type="checkbox" value="-1" name="form[jatuh][dmab1][4]" <?=$form['jatuh']['dmab1']['4'] == -1 ? 'checked' : ''?>></td>
                        <td><input type="checkbox" value="-1" name="form[jatuh][dmab1][5]" <?=$form['jatuh']['dmab1']['5'] == -1 ? 'checked' : ''?>></td>
                    </tr>
                    <tr>
                        <td colspan="4">Kruk/ Tongkat</td>
                        <td>15</td>
                        <td><input type="checkbox" value="15" name="form[jatuh][dmab2][1]" <?=$form['jatuh']['dmab2']['1'] == 15 ? 'checked' : ''?>></td>
                        <td><input type="checkbox" value="15" name="form[jatuh][dmab2][2]" <?=$form['jatuh']['dmab2']['2'] == 15 ? 'checked' : ''?>></td>
                        <td><input type="checkbox" value="15" name="form[jatuh][dmab2][3]" <?=$form['jatuh']['dmab2']['3'] == 15 ? 'checked' : ''?>></td>
                        <td><input type="checkbox" value="15" name="form[jatuh][dmab2][4]" <?=$form['jatuh']['dmab2']['4'] == 15 ? 'checked' : ''?>></td>
                        <td><input type="checkbox" value="15" name="form[jatuh][dmab2][5]" <?=$form['jatuh']['dmab2']['5'] == 15 ? 'checked' : ''?>></td>
                    </tr>
                    <tr>
                        <td colspan="4">Alat sekitar mis : dinding, kursi, meja (perabot)</td>
                        <td>30</td>
                        <td><input type="checkbox" value="30" name="form[jatuh][dmab3][1]" <?=$form['jatuh']['dmab3']['1'] == 30 ? 'checked' : ''?>></td>
                        <td><input type="checkbox" value="30" name="form[jatuh][dmab3][2]" <?=$form['jatuh']['dmab3']['2'] == 30 ? 'checked' : ''?>></td>
                        <td><input type="checkbox" value="30" name="form[jatuh][dmab3][3]" <?=$form['jatuh']['dmab3']['3'] == 30 ? 'checked' : ''?>></td>
                        <td><input type="checkbox" value="30" name="form[jatuh][dmab3][4]" <?=$form['jatuh']['dmab3']['4'] == 30 ? 'checked' : ''?>></td>
                        <td><input type="checkbox" value="30" name="form[jatuh][dmab3][5]" <?=$form['jatuh']['dmab3']['5'] == 30 ? 'checked' : ''?>></td>
                    </tr>
                    <tr>
                        <td rowspan="2" colspan="3" class="text-center" style="vertical-align: middle">
                            Menggunakan infuse / terapi intravena
                        </td>
                        <td colspan="4">TIDAK</td>
                        <td>0</td>
                        <td><input type="checkbox" value="-1" name="form[jatuh][dmitit][1]" <?=$form['jatuh']['dmitit']['1'] == -1 ? 'checked' : ''?>></td>
                        <td><input type="checkbox" value="-1" name="form[jatuh][dmitit][2]" <?=$form['jatuh']['dmitit']['2'] == -1 ? 'checked' : ''?>></td>
                        <td><input type="checkbox" value="-1" name="form[jatuh][dmitit][3]" <?=$form['jatuh']['dmitit']['3'] == -1 ? 'checked' : ''?>></td>
                        <td><input type="checkbox" value="-1" name="form[jatuh][dmitit][4]" <?=$form['jatuh']['dmitit']['4'] == -1 ? 'checked' : ''?>></td>
                        <td><input type="checkbox" value="-1" name="form[jatuh][dmitit][5]" <?=$form['jatuh']['dmitit']['5'] == -1 ? 'checked' : ''?>></td>
                    </tr>
                    <tr>
                        <td colspan="4">YA</td>
                        <td>20</td>
                        <td><input type="checkbox" value="20" name="form[jatuh][dmitiy][1]" <?=$form['jatuh']['dmitiy']['1'] == 20 ? 'checked' : ''?>></td>
                        <td><input type="checkbox" value="20" name="form[jatuh][dmitiy][2]" <?=$form['jatuh']['dmitiy']['2'] == 20 ? 'checked' : ''?>></td>
                        <td><input type="checkbox" value="20" name="form[jatuh][dmitiy][3]" <?=$form['jatuh']['dmitiy']['3'] == 20 ? 'checked' : ''?>></td>
                        <td><input type="checkbox" value="20" name="form[jatuh][dmitiy][4]" <?=$form['jatuh']['dmitiy']['4'] == 20 ? 'checked' : ''?>></td>
                        <td><input type="checkbox" value="20" name="form[jatuh][dmitiy][5]" <?=$form['jatuh']['dmitiy']['5'] == 20 ? 'checked' : ''?>></td>
                    </tr>
                    <tr>
                        <td rowspan="3" colspan="3" class="text-center" style="vertical-align: middle">
                            Gaya Berjalan
                        </td>
                        <td colspan="4">Normal / Bedrest / Kursi roda</td>
                        <td>0</td>
                        <td><input type="checkbox" value="-1" name="form[jatuh][dgb1][1]" <?=$form['jatuh']['dgb1']['1'] == -1 ? 'checked' : ''?>></td>
                        <td><input type="checkbox" value="-1" name="form[jatuh][dgb1][2]" <?=$form['jatuh']['dgb1']['2'] == -1 ? 'checked' : ''?>></td>
                        <td><input type="checkbox" value="-1" name="form[jatuh][dgb1][3]" <?=$form['jatuh']['dgb1']['3'] == -1 ? 'checked' : ''?>></td>
                        <td><input type="checkbox" value="-1" name="form[jatuh][dgb1][4]" <?=$form['jatuh']['dgb1']['4'] == -1 ? 'checked' : ''?>></td>
                        <td><input type="checkbox" value="-1" name="form[jatuh][dgb1][5]" <?=$form['jatuh']['dgb1']['5'] == -1 ? 'checked' : ''?>></td>
                    </tr>
                    <tr>
                        <td colspan="4">Lemah</td>
                        <td>10</td>
                        <td><input type="checkbox" value="10" name="form[jatuh][dgb2][1]" <?=$form['jatuh']['dgb2']['1'] == 10 ? 'checked' : ''?>></td>
                        <td><input type="checkbox" value="10" name="form[jatuh][dgb2][2]" <?=$form['jatuh']['dgb2']['2'] == 10 ? 'checked' : ''?>></td>
                        <td><input type="checkbox" value="10" name="form[jatuh][dgb2][3]" <?=$form['jatuh']['dgb2']['3'] == 10 ? 'checked' : ''?>></td>
                        <td><input type="checkbox" value="10" name="form[jatuh][dgb2][4]" <?=$form['jatuh']['dgb2']['4'] == 10 ? 'checked' : ''?>></td>
                        <td><input type="checkbox" value="10" name="form[jatuh][dgb2][5]" <?=$form['jatuh']['dgb2']['5'] == 10 ? 'checked' : ''?>></td>
                    </tr>
                    <tr>
                        <td colspan="4">Gangguan / tidak normal (pincang, diseret)</td>
                        <td>20</td>
                        <td><input type="checkbox" value="20" name="form[jatuh][dgb3][1]" <?=$form['jatuh']['dgb3']['1'] == 20 ? 'checked' : ''?>></td>
                        <td><input type="checkbox" value="20" name="form[jatuh][dgb3][2]" <?=$form['jatuh']['dgb3']['2'] == 20 ? 'checked' : ''?>></td>
                        <td><input type="checkbox" value="20" name="form[jatuh][dgb3][3]" <?=$form['jatuh']['dgb3']['3'] == 20 ? 'checked' : ''?>></td>
                        <td><input type="checkbox" value="20" name="form[jatuh][dgb3][4]" <?=$form['jatuh']['dgb3']['4'] == 20 ? 'checked' : ''?>></td>
                        <td><input type="checkbox" value="20" name="form[jatuh][dgb3][5]" <?=$form['jatuh']['dgb3']['5'] == 20 ? 'checked' : ''?>></td>
                    </tr>
                    <tr>
                        <td rowspan="2" colspan="3" class="text-center" style="vertical-align: middle">
                            Status Mental
                        </td>
                        <td colspan="4">Menyadari Kemampuan</td>
                        <td>0</td>
                        <td><input type="checkbox" value="-1" name="form[jatuh][dsm1][1]" <?=$form['jatuh']['dsm1']['1'] == -1 ? 'checked' : ''?>></td>
                        <td><input type="checkbox" value="-1" name="form[jatuh][dsm1][2]" <?=$form['jatuh']['dsm1']['2'] == -1 ? 'checked' : ''?>></td>
                        <td><input type="checkbox" value="-1" name="form[jatuh][dsm1][3]" <?=$form['jatuh']['dsm1']['3'] == -1 ? 'checked' : ''?>></td>
                        <td><input type="checkbox" value="-1" name="form[jatuh][dsm1][4]" <?=$form['jatuh']['dsm1']['4'] == -1 ? 'checked' : ''?>></td>
                        <td><input type="checkbox" value="-1" name="form[jatuh][dsm1][5]" <?=$form['jatuh']['dsm1']['5'] == -1 ? 'checked' : ''?>></td>
                    </tr>
                    <tr>
                        <td colspan="4">Sering lupa, gelisah, tidak sadar</td>
                        <td>15</td>
                        <td><input type="checkbox" value="15" name="form[jatuh][dsm2][1]" <?=$form['jatuh']['dsm2']['1'] == 15 ? 'checked' : ''?>></td>
                        <td><input type="checkbox" value="15" name="form[jatuh][dsm2][2]" <?=$form['jatuh']['dsm2']['2'] == 15 ? 'checked' : ''?>></td>
                        <td><input type="checkbox" value="15" name="form[jatuh][dsm2][3]" <?=$form['jatuh']['dsm2']['3'] == 15 ? 'checked' : ''?>></td>
                        <td><input type="checkbox" value="15" name="form[jatuh][dsm2][4]" <?=$form['jatuh']['dsm2']['4'] == 15 ? 'checked' : ''?>></td>
                        <td><input type="checkbox" value="15" name="form[jatuh][dsm2][5]" <?=$form['jatuh']['dsm2']['5'] == 15 ? 'checked' : ''?>></td>
                    </tr>
                    <tr>
                        <td colspan="8" style="vertical-align: middle" class="text-center">
                            <b>TOTAL SKOR</b>
                        </td>
                        <td><input type="text" name="form[jatuh][dtotalsskor1][1]" value="<?=$form['jatuh']['dtotalsskor1']['1']?>" class="form-control"></td>
                        <td><input type="text" name="form[jatuh][dtotalsskor1][2]" value="<?=$form['jatuh']['dtotalsskor1']['2']?>" class="form-control"></td>
                        <td><input type="text" name="form[jatuh][dtotalsskor1][3]" value="<?=$form['jatuh']['dtotalsskor1']['3']?>" class="form-control"></td>
                        <td><input type="text" name="form[jatuh][dtotalsskor1][4]" value="<?=$form['jatuh']['dtotalsskor1']['4']?>" class="form-control"></td>
                        <td><input type="text" name="form[jatuh][dtotalsskor1][5]" value="<?=$form['jatuh']['dtotalsskor1']['5']?>" class="form-control"></td>
                    </tr>
                    <tr>
                        <td colspan="8" rowspan="2" class="text-center" style="vertical-align: middle">
                            <b>LINGKARI RISIKO JATUH BERDASARKAN SKOR</b>
                        </td>
                        <td><b>RR</b></td>
                        <td><b>RR</b></td>
                        <td><b>RR</b></td>
                        <td><b>RR</b></td>
                        <td><b>RR</b></td>
                    </tr>
                    <tr>
                        <td><b>RT</b></td>
                        <td><b>RT</b></td>
                        <td><b>RT</b></td>
                        <td><b>RT</b></td>
                        <td><b>RT</b></td>
                    </tr>
                    <tr>
                        <td colspan="7" rowspan="1" class="text-center" style="vertical-align: middle">
                            <b>PARAF DAN INISIAL PERAWAT</b>
                        </td>
                        <td></td>
                        <td>
                            <input type="text" class="form-control" name="form[jatuh][d_paraf_and_initial][1]" value="<?=$form['jatuh']['d_paraf_and_initial']['1']?>">
                        </td>
                        <td>
                            <input type="text" class="form-control" name="form[jatuh][d_paraf_and_initial][2]" value="<?=$form['jatuh']['d_paraf_and_initial']['2']?>">
                        </td>
                        <td>
                            <input type="text" class="form-control" name="form[jatuh][d_paraf_and_initial][3]" value="<?=$form['jatuh']['d_paraf_and_initial']['3']?>">
                        </td>
                        <td>
                            <input type="text" class="form-control" name="form[jatuh][d_paraf_and_initial][4]" value="<?=$form['jatuh']['d_paraf_and_initial']['4']?>">
                        </td>
                        <td>
                            <input type="text" class="form-control" name="form[jatuh][d_paraf_and_initial][5]" value="<?=$form['jatuh']['d_paraf_and_initial']['5']?>">
                        </td>
                    </tr>
                    <tr>
                        <td colspan="7" rowspan="2">
                            Beri tanda (V) jika sudah dilakukan, (-) jika belum atau tidak dilakukan, (*) jika pasien menolak sekaligus beriksan penjelasannya
                        </td>
                        <td class="text-center" style="vertical-align: middle">
                            <b>TGL</b>
                        </td>
                        <td>
                            <input type="date" class="form-control" name="form[jatuh][d_tgl_v][1]" value="<?=$form['jatuh']['d_tgl_v']['1']?>">
                        </td>
                        <td>
                            <input type="date" class="form-control" name="form[jatuh][d_tgl_v][2]" value="<?=$form['jatuh']['d_tgl_v']['2']?>">
                        </td>
                        <td>
                            <input type="date" class="form-control" name="form[jatuh][d_tgl_v][3]" value="<?=$form['jatuh']['d_tgl_v']['3']?>">
                        </td>
                        <td>
                            <input type="date" class="form-control" name="form[jatuh][d_tgl_v][4]" value="<?=$form['jatuh']['d_tgl_v']['4']?>">
                        </td>
                        <td>
                            <input type="date" class="form-control" name="form[jatuh][d_tgl_v][5]" value="<?=$form['jatuh']['d_tgl_v']['5']?>">
                        </td>
                    </tr>
                    <tr>
                        <td class="text-center" style="vertical-align: middle">
                            <b>JAM</b>
                        </td>
                        <td>
                            <input type="time" class="form-control" name="form[jatuh][d_jam_v][1]" value="<?=$form['jatuh']['d_jam_v']['1']?>">
                        </td>
                        <td>
                            <input type="time" class="form-control" name="form[jatuh][d_jam_v][2]" value="<?=$form['jatuh']['d_jam_v']['2']?>">
                        </td>
                        <td>
                            <input type="time" class="form-control" name="form[jatuh][d_jam_v][3]" value="<?=$form['jatuh']['d_jam_v']['3']?>">
                        </td>
                        <td>
                            <input type="time" class="form-control" name="form[jatuh][d_jam_v][4]" value="<?=$form['jatuh']['d_jam_v']['4']?>">
                        </td>
                        <td>
                            <input type="time" class="form-control" name="form[jatuh][d_jam_v][5]" value="<?=$form['jatuh']['d_jam_v']['5']?>">
                        </td>
                    </tr>
                    <tr>
                        <td>a.</td>
                        <td colspan="6">
                            Memastikan tempat tidur/brankard dalam posisi roda terkunci
                        </td>
                        <td><input type="text" name="form[jatuh][d_input2][a][1]" value="<?=$form['jatuh']['d_input2']['a']['1']?>" class="form-control"></td>
                        <td><input type="text" name="form[jatuh][d_input2][a][2]" value="<?=$form['jatuh']['d_input2']['a']['2']?>" class="form-control"></td>
                        <td><input type="text" name="form[jatuh][d_input2][a][3]" value="<?=$form['jatuh']['d_input2']['a']['3']?>" class="form-control"></td>
                        <td><input type="text" name="form[jatuh][d_input2][a][4]" value="<?=$form['jatuh']['d_input2']['a']['4']?>" class="form-control"></td>
                        <td><input type="text" name="form[jatuh][d_input2][a][5]" value="<?=$form['jatuh']['d_input2']['a']['5']?>" class="form-control"></td>
                        <td><input type="text" name="form[jatuh][d_input2][a][6]" value="<?=$form['jatuh']['d_input2']['a']['6']?>" class="form-control"></td>
                    </tr>
                    <tr>
                        <td>b.</td>
                        <td colspan="6">
                            Pagar sisi tempat tidru/brankard dalam posisi berdiri/terpasang
                        </td>
                        <td><input type="text" name="form[jatuh][d_input2][b][1]" value="<?=$form['jatuh']['d_input2']['b']['1']?>" class="form-control"></td>
                        <td><input type="text" name="form[jatuh][d_input2][b][2]" value="<?=$form['jatuh']['d_input2']['b']['2']?>" class="form-control"></td>
                        <td><input type="text" name="form[jatuh][d_input2][b][3]" value="<?=$form['jatuh']['d_input2']['b']['3']?>" class="form-control"></td>
                        <td><input type="text" name="form[jatuh][d_input2][b][4]" value="<?=$form['jatuh']['d_input2']['b']['4']?>" class="form-control"></td>
                        <td><input type="text" name="form[jatuh][d_input2][b][5]" value="<?=$form['jatuh']['d_input2']['b']['5']?>" class="form-control"></td>
                        <td><input type="text" name="form[jatuh][d_input2][b][6]" value="<?=$form['jatuh']['d_input2']['b']['6']?>" class="form-control"></td>
                    </tr>
                    <tr>
                        <td>c.</td>
                        <td colspan="6">
                            Lingkungan bebeas dari peralatan yang tidak digunakan
                        </td>
                        <td><input type="text" name="form[jatuh][d_input2][c][1]" value="<?=$form['jatuh']['d_input2']['c']['1']?>" class="form-control"></td>
                        <td><input type="text" name="form[jatuh][d_input2][c][2]" value="<?=$form['jatuh']['d_input2']['c']['2']?>" class="form-control"></td>
                        <td><input type="text" name="form[jatuh][d_input2][c][3]" value="<?=$form['jatuh']['d_input2']['c']['3']?>" class="form-control"></td>
                        <td><input type="text" name="form[jatuh][d_input2][c][4]" value="<?=$form['jatuh']['d_input2']['c']['4']?>" class="form-control"></td>
                        <td><input type="text" name="form[jatuh][d_input2][c][5]" value="<?=$form['jatuh']['d_input2']['c']['5']?>" class="form-control"></td>
                        <td><input type="text" name="form[jatuh][d_input2][c][6]" value="<?=$form['jatuh']['d_input2']['c']['6']?>" class="form-control"></td>
                    </tr>
                    <tr>
                        <td>d.</td>
                        <td colspan="6">
                            Berikan penjelasan kepada orang tua tentang pencegahan jatuh
                        </td>
                        <td><input type="text" name="form[jatuh][d_input2][d][1]" value="<?=$form['jatuh']['d_input2']['d']['1']?>" class="form-control"></td>
                        <td><input type="text" name="form[jatuh][d_input2][d][2]" value="<?=$form['jatuh']['d_input2']['d']['2']?>" class="form-control"></td>
                        <td><input type="text" name="form[jatuh][d_input2][d][3]" value="<?=$form['jatuh']['d_input2']['d']['3']?>" class="form-control"></td>
                        <td><input type="text" name="form[jatuh][d_input2][d][4]" value="<?=$form['jatuh']['d_input2']['d']['4']?>" class="form-control"></td>
                        <td><input type="text" name="form[jatuh][d_input2][d][5]" value="<?=$form['jatuh']['d_input2']['d']['5']?>" class="form-control"></td>
                        <td><input type="text" name="form[jatuh][d_input2][d][6]" value="<?=$form['jatuh']['d_input2']['d']['6']?>" class="form-control"></td>
                    </tr>
                    <tr>
                        <td>e.</td>
                        <td colspan="6">
                            Pastikan pasien sudah memakai gelang kuning penanda pasien resiko jatuh dan tanda kewaspadaan pada panel informasi pasien
                        </td>
                        <td><input type="text" name="form[jatuh][d_input2][e][1]" value="<?=$form['jatuh']['d_input2']['e']['1']?>" class="form-control"></td>
                        <td><input type="text" name="form[jatuh][d_input2][e][2]" value="<?=$form['jatuh']['d_input2']['e']['2']?>" class="form-control"></td>
                        <td><input type="text" name="form[jatuh][d_input2][e][3]" value="<?=$form['jatuh']['d_input2']['e']['3']?>" class="form-control"></td>
                        <td><input type="text" name="form[jatuh][d_input2][e][4]" value="<?=$form['jatuh']['d_input2']['e']['4']?>" class="form-control"></td>
                        <td><input type="text" name="form[jatuh][d_input2][e][5]" value="<?=$form['jatuh']['d_input2']['e']['5']?>" class="form-control"></td>
                        <td><input type="text" name="form[jatuh][d_input2][e][6]" value="<?=$form['jatuh']['d_input2']['e']['6']?>" class="form-control"></td>
                    </tr>
                    <tr>
                        <td colspan="7" rowspan="1" class="text-center" style="vertical-align: middle">
                            <b>PARAF DAN INISIAL PERAWAT</b>
                        </td>
                        <td>
                            <input type="text" class="form-control" name="form[jatuh][d_paraf_and_initial2][1]" value="<?=$form['jatuh']['d_paraf_and_initial2']['1']?>">
                        </td>
                        <td>
                            <input type="text" class="form-control" name="form[jatuh][d_paraf_and_initial2][2]" value="<?=$form['jatuh']['d_paraf_and_initial2']['2']?>">
                        </td>
                        <td>
                            <input type="text" class="form-control" name="form[jatuh][d_paraf_and_initial2][3]" value="<?=$form['jatuh']['d_paraf_and_initial2']['3']?>">
                        </td>
                        <td>
                            <input type="text" class="form-control" name="form[jatuh][d_paraf_and_initial2][4]" value="<?=$form['jatuh']['d_paraf_and_initial2']['4']?>">
                        </td>
                        <td>
                            <input type="text" class="form-control" name="form[jatuh][d_paraf_and_initial2][5]" value="<?=$form['jatuh']['d_paraf_and_initial2']['5']?>">
                        </td>
                        <td>
                            <input type="text" class="form-control" name="form[jatuh][d_paraf_and_initial2][7]" value="<?=$form['jatuh']['d_paraf_and_initial2']['7']?>">
                        </td>
                    </tr>
                    <tr>
                        <td>a.</td>
                        <td colspan="6">
                            Memastikan tempat tidur/brankard dalam posisi roda terkunci
                        </td>
                        <td><input type="text" name="form[jatuh][d_input3][a][1]" value="<?=$form['jatuh']['d_input3']['a']['1']?>" class="form-control"></td>
                        <td><input type="text" name="form[jatuh][d_input3][a][2]" value="<?=$form['jatuh']['d_input3']['a']['2']?>" class="form-control"></td>
                        <td><input type="text" name="form[jatuh][d_input3][a][3]" value="<?=$form['jatuh']['d_input3']['a']['3']?>" class="form-control"></td>
                        <td><input type="text" name="form[jatuh][d_input3][a][4]" value="<?=$form['jatuh']['d_input3']['a']['4']?>" class="form-control"></td>
                        <td><input type="text" name="form[jatuh][d_input3][a][5]" value="<?=$form['jatuh']['d_input3']['a']['5']?>" class="form-control"></td>
                        <td><input type="text" name="form[jatuh][d_input3][a][6]" value="<?=$form['jatuh']['d_input3']['a']['6']?>" class="form-control"></td>
                    </tr>
                    <tr>
                        <td>b.</td>
                        <td colspan="6">
                            Pagar sisi tempat tidru/brankard dalam posisi berdiri/terpasang
                        </td>
                        <td><input type="text" name="form[jatuh][d_input3][b][1]" value="<?=$form['jatuh']['d_input3']['b']['1']?>" class="form-control"></td>
                        <td><input type="text" name="form[jatuh][d_input3][b][2]" value="<?=$form['jatuh']['d_input3']['b']['2']?>" class="form-control"></td>
                        <td><input type="text" name="form[jatuh][d_input3][b][3]" value="<?=$form['jatuh']['d_input3']['b']['3']?>" class="form-control"></td>
                        <td><input type="text" name="form[jatuh][d_input3][b][4]" value="<?=$form['jatuh']['d_input3']['b']['4']?>" class="form-control"></td>
                        <td><input type="text" name="form[jatuh][d_input3][b][5]" value="<?=$form['jatuh']['d_input3']['b']['5']?>" class="form-control"></td>
                        <td><input type="text" name="form[jatuh][d_input3][b][6]" value="<?=$form['jatuh']['d_input3']['b']['6']?>" class="form-control"></td>
                    </tr>
                    <tr>
                        <td>c.</td>
                        <td colspan="6">
                            Lingkungan bebeas dari peralatan yang tidak digunakan
                        </td>
                        <td><input type="text" name="form[jatuh][d_input3][c][1]" value="<?=$form['jatuh']['d_input3']['c']['1']?>" class="form-control"></td>
                        <td><input type="text" name="form[jatuh][d_input3][c][2]" value="<?=$form['jatuh']['d_input3']['c']['2']?>" class="form-control"></td>
                        <td><input type="text" name="form[jatuh][d_input3][c][3]" value="<?=$form['jatuh']['d_input3']['c']['3']?>" class="form-control"></td>
                        <td><input type="text" name="form[jatuh][d_input3][c][4]" value="<?=$form['jatuh']['d_input3']['c']['4']?>" class="form-control"></td>
                        <td><input type="text" name="form[jatuh][d_input3][c][5]" value="<?=$form['jatuh']['d_input3']['c']['5']?>" class="form-control"></td>
                        <td><input type="text" name="form[jatuh][d_input3][c][6]" value="<?=$form['jatuh']['d_input3']['c']['6']?>" class="form-control"></td>
                    </tr>
                    <tr>
                        <td>d.</td>
                        <td colspan="6">
                            Berikan penjelasan kepada orang tua tentang pencegahan jatuh
                        </td>
                        <td><input type="text" name="form[jatuh][d_input3][d][1]" value="<?=$form['jatuh']['d_input3']['d']['1']?>" class="form-control"></td>
                        <td><input type="text" name="form[jatuh][d_input3][d][2]" value="<?=$form['jatuh']['d_input3']['d']['2']?>" class="form-control"></td>
                        <td><input type="text" name="form[jatuh][d_input3][d][3]" value="<?=$form['jatuh']['d_input3']['d']['3']?>" class="form-control"></td>
                        <td><input type="text" name="form[jatuh][d_input3][d][4]" value="<?=$form['jatuh']['d_input3']['d']['4']?>" class="form-control"></td>
                        <td><input type="text" name="form[jatuh][d_input3][d][5]" value="<?=$form['jatuh']['d_input3']['d']['5']?>" class="form-control"></td>
                        <td><input type="text" name="form[jatuh][d_input3][d][6]" value="<?=$form['jatuh']['d_input3']['d']['6']?>" class="form-control"></td>
                    </tr>
                    <tr>
                        <td colspan="7" rowspan="1" class="text-center" style="vertical-align: middle">
                            <b>PARAF DAN INISIAL PERAWAT</b>
                        </td>
                        <td>
                            <input type="text" class="form-control" name="form[jatuh][d_paraf_and_initial3][1]" value="<?=$form['jatuh']['d_paraf_and_initial3']['1']?>">
                        </td>
                        <td>
                            <input type="text" class="form-control" name="form[jatuh][d_paraf_and_initial3][2]" value="<?=$form['jatuh']['d_paraf_and_initial3']['2']?>">
                        </td>
                        <td>
                            <input type="text" class="form-control" name="form[jatuh][d_paraf_and_initial3][3]" value="<?=$form['jatuh']['d_paraf_and_initial3']['3']?>">
                        </td>
                        <td>
                            <input type="text" class="form-control" name="form[jatuh][d_paraf_and_initial3][4]" value="<?=$form['jatuh']['d_paraf_and_initial3']['4']?>">
                        </td>
                        <td>
                            <input type="text" class="form-control" name="form[jatuh][d_paraf_and_initial3][5]" value="<?=$form['jatuh']['d_paraf_and_initial3']['5']?>">
                        </td>
                        <td>
                            <input type="text" class="form-control" name="form[jatuh][d_paraf_and_initial3][7]" value="<?=$form['jatuh']['d_paraf_and_initial3']['7']?>">
                        </td>
                    </tr>
                    </tbody>
                </table>
                <div class="text-center">
                    <?php if (superadmin($this->session->userdata('logged_in')) || perawat($this->session->userdata('logged_in'))) : ?>
                    <button class="btn btn-success">
                        Simpan
                    </button>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </form>
</div>