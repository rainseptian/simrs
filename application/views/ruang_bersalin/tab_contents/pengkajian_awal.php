<div class="tab-pane <?=$tab == 'pengkajian_awal' ? 'active' : ''?>" id="pengkajian_awal">
    <div class="impbtnview">
        <a href="<?= base_url(); ?>Laporan/printPemeriksaan/<?= $pemeriksaan_id ?>"
           target="_blank"
           class="btn btn-sm btn-primary dropdown-toggle adddiagnosis">
            <i class="fas fa-print"></i> Cetak Pengkajian Awal
        </a>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <?=$pengkajian_awal?>
        </div>
    </div>
</div>