
</style>
<link rel="stylesheet"
      href="<?php echo base_url() ?>assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
<link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/autocomplete/style-gue.css">
<style type="text/css">
    @import('https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.0/css/bootstrap.min.css');
</style>
<div class="tab-pane <?=isset($tab) && $tab == 'resume_medis' ? 'active' : ''?>" id="resume_medis">
    <div class="impbtnview">
        <a href="<?=base_url("RawatInap/print_form/resume_medis/$rawat_inap->id")?>" target="_blank" class="btn btn-primary btn-sm">
            <i class="fa fa-print"></i> Cetak
        </a>
    </div>
    <?php 
        if(gettype(json_decode(json_encode($form['resume_medis']))) == 'array') {
            $lastIndex = count($form['resume_medis']) - 1;
            $form['resume_medis'] = $form['resume_medis'][$lastIndex];
        }
     ?>
    <div class="row justify-content-center">
        <div class="col-sm-12 col-md-7" style="float: none; margin: 0 auto;">
            <form method="post" action="<?php echo base_url() ?>RawatInap/save_form/resume_medis/resume_medis">
                <input type="hidden" name="rawat_inap_id" value="<?=$rawat_inap->id?>">
                <table class="table table-bordered">
                    <tbody>
                    <tr>
                        <td colspan="4">
                            <h4 class="text-center"><b>RESUME MEDIS</b></h4>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <label>Tgl Masuk</label>
                            <input type="date" class="form-control" name="form[resume_medis][tgl_masuk]" value="<?=$form['resume_medis']['tgl_masuk'] ?: date('Y-m-d', strtotime($rawat_inap->created_at))?>">
                        </td>
                        <td colspan="2" rowspan="2">
                            <label>Dokter yang merawat</label>
                            <!-- <input type="text" class="form-control" name="form[resume_medis][dokter]" value="<?=$form['resume_medis']['dokter']?>"> -->
                            <div class="margin-bottom">
                                <select class="form-control abdush-select margin-bottom" name="form[resume_medis][dokter]">
                                    <option value="" selected>-- Pilih Dokter --</option>
                                    <?php foreach ($dokter->result() as $key => $value) { ?>
                                        <option value="<?php echo $value->nama ?>" <?= $value->nama == $form['resume_medis']['dokter'] ? 'selected': ''?>><?php echo $value->nama; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <label>Tgl Keluar</label>`
                            <input type="datetime-local" class="form-control" name="form[resume_medis][tgl_keluar]" value="<?=$form['resume_medis']['tgl_keluar']?>">
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <label>Diagnosa Masuk</label>
                            <?php
                                $diagnosaMasuk = $form['resume_medis']['diagnosa_masuk'] ? 
                                    $form['resume_medis']['diagnosa_masuk'] : 
                                    implode(',', $penyakit_pengkajian_awal);   
                            ?>
                            <textarea type="text" class="form-control" name="form[resume_medis][diagnosa_masuk]" value=""><?= $diagnosaMasuk ?></textarea>
                            <br>
                            <label>Indikasi Rawat</label>
                            <textarea type="text" class="form-control" name="form[resume_medis][indikasi_rawat]" value=""> <?=$form['resume_medis']['indikasi_rawat']?></textarea>
                            <br>
                            <label>Diagnosa Keluar</label>
                            <textarea type="text" class="form-control" name="form[resume_medis][diagnosa_keluar]" value=""><?=$form['resume_medis']['diagnosa_keluar']?></textarea>
                            <br>
                            <label>Kode ICD 10</label>                           
                            <select class="form-control select2" name="form[resume_medis][icd10][]" multiple="multiple">   
                                <?php
                                    $selectedPenyakit = !is_null($form['resume_medis']['icd10']) ? 
                                    $form['resume_medis']['icd10'] : $penyakit_pengkajian_awal
                                ?>                             
                                <?php foreach ($penyakit as $key => $value) { ?>
                                    <option
                                        value="<?=$value->kode.' - '.$value->nama?>"
                                        <?= in_array($value->kode.' - '.$value->nama, $selectedPenyakit) ? 'selected' : '' ?>
                                    >
                                        <?= $value->kode.' - '.$value->nama ?>
                                    </option>
                                <?php } ?>
                            </select>
                            <br>
                            <br>
                            <label>Komplikasi</label>
                            <textarea type="text" class="form-control" name="form[resume_medis][komplikasi]" value=""><?=$form['resume_medis']['komplikasi']?></textarea>
                            <br>
                            <label>Komorbid</label>
                            <textarea type="text" class="form-control" name="form[resume_medis][komorbid]" value=""><?=$form['resume_medis']['komorbid']?></textarea>
                            <br>
                            <label>Tindakan/Operasi</label>
                            <textarea type="text" class="form-control" name="form[resume_medis][tindakan]" value=""><?=$form['resume_medis']['tindakan']?></textarea>
                            <br>
                            <div class="row">
                                <div class="col-sm-6">
                                    <label>Tgl Dilakukan</label>
                                    <input type="date" class="form-control" name="form[resume_medis][tgl_dilakukan]" value="<?=$form['resume_medis']['tgl_dilakukan']?>">
                                </div>
                                <div class="col-sm-6">
                                    <label>Kode ICD 9</label>                                    
                                    <select class="form-control select2" name="form[resume_medis][icd9][]" multiple="icd9"> 
                                    <?php
                                        $selectedPenyakit = !is_null($form['resume_medis']['icd9']) ? 
                                        $form['resume_medis']['icd9'] : $penyakit_pengkajian_awal
                                    ?>
                                    <?php foreach ($penyakit_icd9 as $key => $value) { ?>
                                    <option
                                        value="<?=$value->kode.' - '.$value->nama?>"
                                        <?= in_array($value->kode.' - '.$value->nama, $selectedPenyakit) ? 'selected' : '' ?>
                                    >
                                        <?= $value->kode.' - '.$value->nama ?>
                                    </option>
                                <?php } ?>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <h4><b>Anamnese</b></h4>
                            <label>• Keluhan Utama</label>
                            <?php
                                $keluhanUtama = $form['resume_medis']['keluhan_utama'] ? $form['resume_medis']['keluhan_utama'] : $pengkajian_awal_pemeriksaan['keluhan_utama'];
                            ?>
                            <input type="text" class="form-control" name="form[resume_medis][keluhan_utama]" value="<?=$keluhanUtama?>">
                            <br>
                            <label>• Gejala Penyerta</label>
                            <textarea type="text" class="form-control" name="form[resume_medis][gejala_penyerta]" value=""><?=$form['resume_medis']['gejala_penyerta']?></textarea>
                            <br>
                            <label>• Riwayat Penyakit Dahulu</label>
                            <?php
                                $rpd = $form['resume_medis']['rpd'] ? $form['resume_medis']['rpd'] : $form_pengkajian_awal['riwayat_penyakit'];
                            ?>
                            <input type="text" class="form-control" name="form[resume_medis][rpd]" value="<?=$rpd?>">
                            <br>
                            <h4><b>Temuan Penting</b></h4>
                            <label>• Pemeriksaan Fisik</label>
                            <textarea type="text" class="form-control" name="form[resume_medis][pemeriksaan_fisik]" value=""><?=$form['resume_medis']['pemeriksaan_fisik']?></textarea>
                            <br>
                            <label>• Pemeriksaan Penunjang</label>
                            <textarea type="text" class="form-control" name="form[resume_medis][pemeriksaan_penunjang]" value=""><?=$form['resume_medis']['pemeriksaan_penunjang']?></textarea>
                            <br>
                            <label>• Laboratorium</label>
                            <textarea type="text" class="form-control" name="form[resume_medis][lab]" value=""><?=$form['resume_medis']['lab']?></textarea>
                            <br>
                            <label>• Pencitraan Diagnosa</label>
                            <textarea type="text" class="form-control" name="form[resume_medis][citra]" value=""><?=$form['resume_medis']['citra']?></textarea>
                            <br>
                            <label>• Lainnya</label>
                            <textarea type="text" class="form-control" name="form[resume_medis][lainnya]" value=""><?=$form['resume_medis']['lainnya']?></textarea>
                            <br>
                            <label>Konsultasi</label>
                            <textarea type="text" class="form-control" name="form[resume_medis][konsultasi]" value=""><?=$form['resume_medis']['konsultasi']?></textarea>
                            <br>
                            <label>Obat Selama rawat</label>
                            <textarea type="text" class="form-control" name="form[resume_medis][obat_rawat]" value=""><?=$form['resume_medis']['obat_rawat']?></textarea>
                            <br>
                            <label>Kondisi Saat Pulang</label>
                            <textarea type="text" class="form-control" name="form[resume_medis][kondisi]" value=""><?=$form['resume_medis']['kondisi']?></textarea>
                            <br>
                            <label>Obat Pulang</label>
                            <textarea type="text" class="form-control" name="form[resume_medis][obat_pulang]" value=""><?=$form['resume_medis']['obat_pulang']?></textarea>
                            <br>
                            <h5><b><u>Intruksi Lanjut</u></b></h5>
                            <label>• Kontrol Ulang</label>
                            <textarea type="text" class="form-control" name="form[resume_medis][kontrol]" value=""><?=$form['resume_medis']['kontrol']?></textarea>
                            <br>
                            <label>• Segera bawa ke RS bila</label>
                            <textarea type="text" class="form-control" name="form[resume_medis][segera]" value=""><?=$form['resume_medis']['segera']?></textarea>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <div style="display: flex; flex-direction: row-reverse">
                                <div class="text-center" style="display: flex; flex-direction: column">
                                    <span>Sampang, <?=date('d-F-Y')?> Jam : <?=date('H:i')?></span>
                                    <span>Dokter yang Merawat</span>
                                    <br>
                                    <br>
                                    <br>
                                    <span>(....................................................)</span>
                                    <span>Tanda Tangan & Nama Terang</span>
                                </div>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
                <div class="text-center">
                    <?php if (superadmin($this->session->userdata('logged_in')) || dokter($this->session->userdata('logged_in'))) : ?>
                    <button class="btn btn-success">
                        Simpan
                    </button>
                    <?php endif; ?>
                </div>
            </form>
        </div>
    </div>
</div>