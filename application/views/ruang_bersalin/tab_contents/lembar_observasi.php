<div class="tab-pane <?=isset($tab) && $tab == 'lembar_observasi' ? 'active' : ''?>" id="lembar_observasi">
    <div class="impbtnview">
        <a href="<?=base_url("RawatInap/print_form/observasi/$rawat_inap->id")?>" target="_blank" class="btn btn-primary btn-sm">
            <i class="fa fa-print"></i> Cetak
        </a>
    </div>
    <form method="post" action="<?php echo base_url() ?>RawatInap/save_form/observasi/lembar_observasi">
        <input type="hidden" name="rawat_inap_id" value="<?=$rawat_inap->id?>">
        <div class="row">
            <div class="col-md-12">
                <table class="table table-bordered table-hover" style="table-layout: fixed">
                    <tbody>
                    <tr>
                        <td colspan="11" class="text-center">
                            <b>LEMBAR OBSERVASI</b>
                        </td>
                    </tr>
                    <tr>
                        <th class="text-center" style="vertical-align: middle" rowspan="2">Tgl</th>
                        <th class="text-center" style="vertical-align: middle" rowspan="2">Jam</th>
                        <th class="text-center" style="vertical-align: middle" rowspan="2">Cairan Intravena Menit</th>
                        <th class="text-center" style="vertical-align: middle" rowspan="1">Tetesan</th>
                        <th class="text-center" style="vertical-align: middle" rowspan="1">Tensi</th>
                        <th class="text-center" style="vertical-align: middle" rowspan="1">Nadi</th>
                        <th class="text-center" style="vertical-align: middle" rowspan="1">Suhu</th>
                        <th class="text-center" style="vertical-align: middle" rowspan="1">Pernafasan</th>
                        <th class="text-center" style="vertical-align: middle" rowspan="2">Input</th>
                        <th class="text-center" style="vertical-align: middle" rowspan="2">Output</th>
                        <th class="text-center" style="vertical-align: middle" rowspan="2">Ket</th>
                    </tr>
                    <tr>
                        <th class="text-center" style="vertical-align: middle">X/menit</th>
                        <th class="text-center" style="vertical-align: middle">mmHg</th>
                        <th class="text-center" style="vertical-align: middle">X/menit</th>
                        <th class="text-center" style="vertical-align: middle">ºC</th>
                        <th class="text-center" style="vertical-align: middle">X/menit</th>
                    </tr>
                    <?php foreach ($form['observasi'] as $v) : ?>
                        <tr>
                            <td><?=date('d-F-Y', strtotime(str_replace('T', ' ', $v['tgl'])))?></td>
                            <td><?=date('H:i', strtotime(str_replace('T', ' ', $v['jam'])))?></td>
                            <td><div style="white-space: pre-wrap;"><?=$v['cairan']?></div></td>
                            <td><div style="white-space: pre-wrap;"><?=$v['tetesan']?></div></td>
                            <td><div style="white-space: pre-wrap;"><?=$v['tensi']?></div></td>
                            <td><div style="white-space: pre-wrap;"><?=$v['nadi']?></div></td>
                            <td><div style="white-space: pre-wrap;"><?=$v['suhu']?></div></td>
                            <td><div style="white-space: pre-wrap;"><?=$v['pernafasan']?></div></td>
                            <td><div style="white-space: pre-wrap;"><?=$v['input']?></div></td>
                            <td><div style="white-space: pre-wrap;"><?=$v['output']?></div></td>
                            <td><div style="white-space: pre-wrap;"><?=$v['ket']?></div></td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
                <div class="text-center">
                    <?php if (superadmin($this->session->userdata('logged_in')) || perawat($this->session->userdata('logged_in'))) : ?>
                    <a href="#" class="btn btn-primary dropdown-toggle addcharges"
                       onclick="holdModal('tambah_observasi')" data-toggle='modal'>Tambah Observasi
                    </a>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </form>
</div>