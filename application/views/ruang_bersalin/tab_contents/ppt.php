<div class="tab-pane <?=isset($tab) && $tab == 'ppt' ? 'active' : ''?>" id="ppt">
    <div class="impbtnview">
        <a href="<?=base_url("RawatInap/print_form/ppt/$rawat_inap->id")?>" target="_blank" class="btn btn-primary btn-sm">
            <i class="fa fa-print"></i> Cetak
        </a>
    </div>
    <form method="post" action="<?php echo base_url() ?>RawatInap/save_form/observasi/ppt">
        <input type="hidden" name="rawat_inap_id" value="<?=$rawat_inap->id?>">
        <div class="row">
            <div class="col-md-12">
                <table class="table table-bordered table-hover" style="table-layout: fixed">
                    <tbody>
                    <tr>
                        <td colspan="5" class="text-center">
                            <b>PENCATATAN DAN PERENCANAAN TERINTEGRASI</b>
                        </td>
                    </tr>
                    <tr>
                        <th class="text-center" style="vertical-align: middle">Tgl/Jam</th>
                        <th class="text-center" style="vertical-align: middle">Profesional Pemberi Asuhan (PPA)</th>
                        <th class="text-center" style="vertical-align: middle">CATATAN PERKEMBANGAN</th>
                        <th class="text-center" style="vertical-align: middle">INTRUKSI PPA</th>
                        <th class="text-center" style="vertical-align: middle">VERIFIKASI DPJP</th>
                    </tr>
                    <?php foreach ($form['ppt'] as $v) : ?>
                        <tr>
                            <td><?=date('d-F-Y H:i', strtotime(str_replace('T', ' ', $v['tgl'])))?></td>
                            <td><div style="white-space: pre-wrap;"><?=$v['ppa']?></div></td>
                            <td><div style="white-space: pre-wrap;"><?=$v['catatan']?></div></td>
                            <td><div style="white-space: pre-wrap;"><?=$v['intruksi']?></div></td>
                            <td><div style="white-space: pre-wrap;"><?=$v['verifikasi']?></div></td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
                <div class="text-center">
                    <?php if (superadmin($this->session->userdata('logged_in')) || perawat($this->session->userdata('logged_in')) || dokter($this->session->userdata('logged_in'))) : ?>
                    <a href="#" class="btn btn-primary dropdown-toggle addcharges"
                       onclick="holdModal('tambah_ppt')" data-toggle='modal'>Tambah PPT
                    </a>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </form>
</div>