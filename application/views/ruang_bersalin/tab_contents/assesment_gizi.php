<div class="tab-pane <?=$tab == 'assesment_gizi' ? 'active' : ''?>" id="assesment_gizi">
    <div class="impbtnview">
        <?php if (superadmin($this->session->userdata('logged_in')) || gizi($this->session->userdata('logged_in'))) : ?>
        <a href="#" class="btn btn-sm btn-primary dropdown-toggle addcharges"
           onclick="holdModal('tambah_makanan')" data-toggle='modal'><i
                    class="fa fa-plus"></i> Tambah Makanan
        </a>
        <?php endif; ?>
    </div><!--./impbtnview-->
    <div class="row">
        <div class="col-sm-12 col-md-7">
            <form method="post" action="<?php echo base_url() ?>RawatInap/save_form/assesment_gizi/assesment_gizi">
                <input type="hidden" name="rawat_inap_id" value="<?=$rawat_inap->id?>">
                <h4 class="text-center"><b>SKRINING NUTRISI AWAL DENGAN MALNUTRITION SCREENING TOOL</b></h4>
                <table class="table table-bordered">
                    <tbody>
                    <tr>
                        <th colspan="2">Indikator Penilaian Malnutrisi</th>
                        <th>Skor</th>
                    </tr>
                    <tr>
                        <th colspan="2">A. DEWASA</th>
                        <th></th>
                    </tr>
                    <tr>
                        <td>1.</td>
                        <td>Apakah pasien mengalami penurunan berat badan yang direncanakan / tidak diinginkan dalam 6 bulan terakhir?</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>
                            <input type="radio" name="form[assesment_gizi][a1]" <?=$form['assesment_gizi']['a1'] == 1 ? 'checked' : ''?> value="1" data-value="0" id="a11"> <label for="a11" style="margin-bottom: 0">Tidak</label>
                        </td>
                        <td>0</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>
                            <input type="radio" name="form[assesment_gizi][a1]" <?=$form['assesment_gizi']['a1'] == 2 ? 'checked' : ''?> value="2" data-value="2" id="a12"> <label for="a12" style="margin-bottom: 0">Tidak yakin (ada tanda bajunya lebih longgar)</label>
                        </td>
                        <td>2</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>
                            <input type="radio" name="form[assesment_gizi][a1]" <?=$form['assesment_gizi']['a1'] == 3 ? 'checked' : ''?> value="3" data-value="0" id="a13"> <label for="a13" style="margin-bottom: 0">Ya, ada penurunan berat badan sebanyak</label>
                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td style="padding-left: 40px">
                            <input type="radio" name="form[assesment_gizi][a11]" <?=$form['assesment_gizi']['a11'] == 1 ? 'checked' : ''?> value="1" data-value="1" id="a111"> <label for="a111" style="margin-bottom: 0">1 - 5 kg</label>
                        </td>
                        <td>1</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td style="padding-left: 40px">
                            <input type="radio" name="form[assesment_gizi][a11]" <?=$form['assesment_gizi']['a11'] == 2 ? 'checked' : ''?> value="2" data-value="2" id="a112"> <label for="a112" style="margin-bottom: 0">6 - 10 kg</label>
                        </td>
                        <td>2</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td style="padding-left: 40px">
                            <input type="radio" name="form[assesment_gizi][a11]" <?=$form['assesment_gizi']['a11'] == 3 ? 'checked' : ''?> value="3" data-value="3" id="a113"> <label for="a113" style="margin-bottom: 0">11 - 15 kg</label>
                        </td>
                        <td>3</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td style="padding-left: 40px">
                            <input type="radio" name="form[assesment_gizi][a11]" <?=$form['assesment_gizi']['a11'] == 4 ? 'checked' : ''?> value="4" data-value="4" id="a114"> <label for="a114" style="margin-bottom: 0">> 15 kg</label>
                        </td>
                        <td>4</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td style="padding-left: 40px">
                            <input type="radio" name="form[assesment_gizi][a11]" <?=$form['assesment_gizi']['a11'] == 5 ? 'checked' : ''?> value="5" data-value="2" id="a115"> <label for="a115" style="margin-bottom: 0">Tidak tahu berapa penurunannya</label>
                        </td>
                        <td>2</td>
                    </tr>
                    <tr>
                        <td>2.</td>
                        <td>Apakah asupan makan pasien berkurang karena nafsu makan / kesulitan menerima makan?</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>
                            <input type="radio" name="form[assesment_gizi][a2]" <?=$form['assesment_gizi']['a2'] == 1 ? 'checked' : ''?> value="1" data-value="0" id="a21"> <label for="a21" style="margin-bottom: 0">Tidak</label>
                        </td>
                        <td>0</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>
                            <input type="radio" name="form[assesment_gizi][a2]" <?=$form['assesment_gizi']['a2'] == 2 ? 'checked' : ''?> value="2" data-value="1" id="a22"> <label for="a22" style="margin-bottom: 0">Ya</label>
                        </td>
                        <td>1</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>
                            TOTAL SKOR<br>
                            Bila skor > 2 maka pasien beresiko malnutrisi, konsul ke ahli gizi
                            <input type="hidden" name="form[assesment_gizi][total_a]" value="<?=$form['assesment_gizi']['total_a'] ?? ''?>" id="total_skor_a_input">
                        </td>
                        <th id="total_skor_a">
                            <?=$form['assesment_gizi']['total_a'] ?? ''?>
                        </th>
                    </tr>


                    <tr>
                        <th colspan="2">B. BAYI / ANAK (Dengan melihat tabel WHO - NCHS)</th>
                        <th></th>
                    </tr>
                    <tr>
                        <td>1.</td>
                        <td>Apakah hasil pengukuran anthropometri menunjukkan hasil BB / TB < -2SD?</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>
                            <input type="radio" name="form[assesment_gizi][b1]" <?=$form['assesment_gizi']['b1'] == 2 ? 'checked' : ''?> value="2" data-value="2" id="b11"> <label for="b11" style="margin-bottom: 0">Ya</label>
                        </td>
                        <td>2</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>
                            <input type="radio" name="form[assesment_gizi][b1]" <?=$form['assesment_gizi']['b1'] == 0 ? 'checked' : ''?> value="0" data-value="0" id="b12"> <label for="b12" style="margin-bottom: 0">Tidak</label>
                        </td>
                        <td>0</td>
                    </tr>
                    <tr>
                        <td>2.</td>
                        <td>Apakah hasil pengukuran anthropometri menunjukkan hasil BB / U < -2SD?</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>
                            <input type="radio" name="form[assesment_gizi][b2]" <?=$form['assesment_gizi']['b2'] == 2 ? 'checked' : ''?> value="2" data-value="2" id="b21"> <label for="b21" style="margin-bottom: 0">Ya</label>
                        </td>
                        <td>2</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>
                            <input type="radio" name="form[assesment_gizi][b2]" <?=$form['assesment_gizi']['b2'] == 0 ? 'checked' : ''?> value="0" data-value="0" id="b22"> <label for="b22" style="margin-bottom: 0">Tidak</label>
                        </td>
                        <td>0</td>
                    </tr>
                    <tr>
                        <td>3.</td>
                        <td>Apakah hasil pengukuran anthropometri menunjukkan hasil TB / U < -2SD?</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>
                            <input type="radio" name="form[assesment_gizi][b3]" <?=$form['assesment_gizi']['b3'] == 2 ? 'checked' : ''?> value="2" data-value="2" id="b31"> <label for="b31" style="margin-bottom: 0">Ya</label>
                        </td>
                        <td>2</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>
                            <input type="radio" name="form[assesment_gizi][b3]" <?=$form['assesment_gizi']['b3'] == 0 ? 'checked' : ''?> value="0" data-value="0" id="b32"> <label for="b32" style="margin-bottom: 0">Tidak</label>
                        </td>
                        <td>0</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>
                            TOTAL SKOR<br>
                            Bila skor > 2 maka pasien beresiko malnutrisi, konsul ke ahli gizi
                            <input type="hidden" name="form[assesment_gizi][total_b]" value="<?=$form['assesment_gizi']['total_b'] ?? ''?>" id="total_skor_b_input">
                        </td>
                        <th id="total_skor_b">
                            <?=$form['assesment_gizi']['total_b'] ?? ''?>
                        </th>
                    </tr>
                    </tbody>
                </table>
                <h4 class="text-center"><b>ASSESMEN GIZI</b></h4>
                <table class="table table-bordered">
                    <tbody>
                    <tr>
                        <th colspan="2">Anthropometri</th>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div style="display: flex">
                                        <b style="flex: 1">Alergi Makanan:</b>
                                        <b style="width: 50px">Ya</b>
                                        <b style="width: 50px">Tidak</b>
                                    </div>
                                    <div style="display: flex">
                                        <span style="flex: 1; padding-left: 30px">• Telur</span>
                                        <span style="width: 50px">
                                        <input type="radio" name="form[assesment_gizi][telur]" <?=$form['assesment_gizi']['telur'] == 0 ? 'checked' : ''?> value="0">
                                    </span>
                                        <span style="width: 50px">
                                        <input type="radio" name="form[assesment_gizi][telur]" <?=$form['assesment_gizi']['telur'] == 1 ? 'checked' : ''?> value="1">
                                    </span>
                                    </div>
                                    <div style="display: flex">
                                        <span style="flex: 1; padding-left: 30px">• Susu sapi & Produk olahannya</span>
                                        <span style="width: 50px">
                                        <input type="radio" name="form[assesment_gizi][susu]" <?=$form['assesment_gizi']['susu'] == 0 ? 'checked' : ''?> value="0">
                                    </span>
                                        <span style="width: 50px">
                                        <input type="radio" name="form[assesment_gizi][susu]" <?=$form['assesment_gizi']['susu'] == 1 ? 'checked' : ''?> value="1">
                                    </span>
                                    </div>
                                    <div style="display: flex">
                                        <span style="flex: 1; padding-left: 30px">• Kacang Kedelai</span>
                                        <span style="width: 50px">
                                        <input type="radio" name="form[assesment_gizi][kacang_kedelai]" <?=$form['assesment_gizi']['kacang_kedelai'] == 0 ? 'checked' : ''?> value="0">
                                    </span>
                                        <span style="width: 50px">
                                        <input type="radio" name="form[assesment_gizi][kacang_kedelai]" <?=$form['assesment_gizi']['kacang_kedelai'] == 1 ? 'checked' : ''?> value="1">
                                    </span>
                                    </div>
                                    <div style="display: flex">
                                        <span style="flex: 1; padding-left: 30px">• Gluten/Gandum</span>
                                        <span style="width: 50px">
                                        <input type="radio" name="form[assesment_gizi][gluten]" <?=$form['assesment_gizi']['gluten'] == 0 ? 'checked' : ''?> value="0">
                                    </span>
                                        <span style="width: 50px">
                                        <input type="radio" name="form[assesment_gizi][gluten]" <?=$form['assesment_gizi']['gluten'] == 1 ? 'checked' : ''?> value="1">
                                    </span>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div style="display: flex">
                                        <b style="flex: 1"></b>
                                        <b style="width: 50px">Ya</b>
                                        <b style="width: 50px">Tidak</b>
                                    </div>
                                    <div style="display: flex">
                                        <span style="flex: 1; padding-left: 30px">• Ikan</span>
                                        <span style="width: 50px">
                                        <input type="radio" name="form[assesment_gizi][ikan]" <?=$form['assesment_gizi']['ikan'] == 0 ? 'checked' : ''?> value="0">
                                    </span>
                                        <span style="width: 50px">
                                        <input type="radio" name="form[assesment_gizi][ikan]" <?=$form['assesment_gizi']['ikan'] == 1 ? 'checked' : ''?> value="1">
                                    </span>
                                    </div>
                                    <div style="display: flex">
                                        <span style="flex: 1; padding-left: 30px">• Udang</span>
                                        <span style="width: 50px">
                                        <input type="radio" name="form[assesment_gizi][udang]" <?=$form['assesment_gizi']['udang'] == 0 ? 'checked' : ''?> value="0">
                                    </span>
                                        <span style="width: 50px">
                                        <input type="radio" name="form[assesment_gizi][udang]" <?=$form['assesment_gizi']['udang'] == 1 ? 'checked' : ''?> value="1">
                                    </span>
                                    </div>
                                    <div style="display: flex">
                                        <span style="flex: 1; padding-left: 30px">• Lainnya, Sebutkan</span>
                                        <span style="width: 50px">
                                        <input type="radio" name="form[assesment_gizi][lainnya]" <?=$form['assesment_gizi']['lainnya'] == 0 ? 'checked' : ''?> value="0">
                                    </span>
                                        <span style="width: 50px">
                                        <input type="radio" name="form[assesment_gizi][lainnya]" <?=$form['assesment_gizi']['lainnya'] == 1 ? 'checked' : ''?> value="1">
                                    </span>
                                    </div>
                                    <div style="display: flex; padding-left: 40px">
                                        <textarea name="form[assesment_gizi][lainnya_text]" class="form-control" placeholder="Lainnya"><?=$form['assesment_gizi']['lainnya_text'] ?? ''?></textarea>
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <label>Pola Makan:</label>
                            <textarea name="form[assesment_gizi][pola_makan]" class="form-control"><?=$form['assesment_gizi']['pola_makan']?></textarea>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Recal Makanan Pasien</label>
                        </td>
                        <td>
                            <label>Kebutuhan Gizi</label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div style="display: flex; align-items: center; margin-bottom: 4px;">
                                <span style="width: 110px">Energi</span>
                                <input style="flex: 1" type="text" name="form[assesment_gizi][recal][energi]" value="<?=$form['assesment_gizi']['recal']['energi'] ?? ''?>" class="form-control">
                                <span style="width: 60px; margin-left: 16px;">Kkal</span>
                            </div>
                            <div style="display: flex; align-items: center; margin-bottom: 4px;">
                                <span style="width: 110px">Protein</span>
                                <input style="flex: 1" type="text" name="form[assesment_gizi][recal][protein]" value="<?=$form['assesment_gizi']['recal']['protein'] ?? ''?>" class="form-control">
                                <span style="width: 60px; margin-left: 16px;">gr</span>
                            </div>
                            <div style="display: flex; align-items: center; margin-bottom: 4px;">
                                <span style="width: 110px">Lemak</span>
                                <input style="flex: 1" type="text" name="form[assesment_gizi][recal][lemak]" value="<?=$form['assesment_gizi']['recal']['lemak'] ?? ''?>" class="form-control">
                                <span style="width: 60px; margin-left: 16px;">gr</span>
                            </div>
                            <div style="display: flex; align-items: center">
                                <span style="width: 110px">Karbohidrat</span>
                                <input style="flex: 1" type="text" name="form[assesment_gizi][recal][karbohidrat]" value="<?=$form['assesment_gizi']['recal']['karbohidrat'] ?? ''?>" class="form-control">
                                <span style="width: 60px; margin-left: 16px;">gr</span>
                            </div>
                        </td>
                        <td>
                            <div style="display: flex; align-items: center; margin-bottom: 4px;">
                                <span style="width: 110px">Energi</span>
                                <input style="flex: 1" type="text" name="form[assesment_gizi][kebutuhan][energi]" value="<?=$form['assesment_gizi']['kebutuhan']['energi'] ?? ''?>" class="form-control">
                                <span style="width: 60px; margin-left: 16px;">Kkal</span>
                            </div>
                            <div style="display: flex; align-items: center; margin-bottom: 4px;">
                                <span style="width: 110px">Protein</span>
                                <input style="flex: 1" type="text" name="form[assesment_gizi][kebutuhan][protein]" value="<?=$form['assesment_gizi']['kebutuhan']['protein'] ?? ''?>" class="form-control">
                                <span style="width: 60px; margin-left: 16px;">gr</span>
                            </div>
                            <div style="display: flex; align-items: center; margin-bottom: 4px;">
                                <span style="width: 110px">Lemak</span>
                                <input style="flex: 1" type="text" name="form[assesment_gizi][kebutuhan][lemak]" value="<?=$form['assesment_gizi']['kebutuhan']['lemak'] ?? ''?>" class="form-control">
                                <span style="width: 60px; margin-left: 16px;">gr</span>
                            </div>
                            <div style="display: flex; align-items: center">
                                <span style="width: 110px">Karbohidrat</span>
                                <input style="flex: 1" type="text" name="form[assesment_gizi][kebutuhan][karbohidrat]" value="<?=$form['assesment_gizi']['kebutuhan']['karbohidrat'] ?? ''?>" class="form-control">
                                <span style="width: 60px; margin-left: 16px;">gr</span>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <label>Riwayat Personal:</label>
                            <textarea name="form[assesment_gizi][riwayat_personal]" class="form-control"><?=$form['assesment_gizi']['riwayat_personal'] ?? ''?></textarea>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" class="text-center">
                            <label>DIAGNOSIS GIZI</label>
                            <textarea name="form[assesment_gizi][diagnosis_gizi]" class="form-control"><?=$form['assesment_gizi']['diagnosis_gizi']?></textarea>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" class="text-center">
                            <label>INTERVENSI GIZI</label>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 50%">
                            <label>Tujuan Intervensi</label>
                            <textarea name="form[assesment_gizi][tujuan_intervensi]" class="form-control"><?=$form['assesment_gizi']['tujuan_intervensi']?></textarea>
                        </td>
                        <td style="width: 50%">
                            <div>
                                <u>Intervensi Makanan</u>
                            </div>
                            <div style="display: flex">
                                <div style="flex: 1; display: flex; flex-direction: column">
                                    <label>Diet</label>
                                    <input type="text" name="form[assesment_gizi][diet]" value="<?=$form['assesment_gizi']['diet']?>" class="form-control">
                                    <label>Cara Pemberian:</label>
                                    <div style="display: flex; align-items: center">
                                        <label style="margin-bottom: 0 !important; margin-right: 4px;">Lewat mulut</label>
                                        <input type="radio" name="form[assesment_gizi][pemberian]" value="1" <?=$form['assesment_gizi']['pemberian'] == 1 ? 'checked' : ''?>>
                                    </div>
                                    <div style="display: flex; align-items: center">
                                        <label style="margin-bottom: 0 !important; margin-right: 4px;">Lewat pipa</label>
                                        <input type="radio" name="form[assesment_gizi][pemberian]" value="2" <?=$form['assesment_gizi']['pemberian'] == 2 ? 'checked' : ''?>>
                                    </div>
                                </div>
                                <div style="flex: 1; display: flex; flex-direction: column; margin-left: 8px">
                                    <label>Bentuk Makanan</label>
                                    <div style="display: flex; align-items: center">
                                        <label style="margin-bottom: 0 !important; margin-right: 4px;">Biasa</label>
                                        <input type="radio" name="form[assesment_gizi][bentuk]" value="1" <?=$form['assesment_gizi']['bentuk'] == 1 ? 'checked' : ''?>>
                                    </div>
                                    <div style="display: flex; align-items: center">
                                        <label style="margin-bottom: 0 !important; margin-right: 4px;">Saring</label>
                                        <input type="radio" name="form[assesment_gizi][bentuk]" value="2" <?=$form['assesment_gizi']['bentuk'] == 2 ? 'checked' : ''?>>
                                    </div>
                                    <div style="display: flex; align-items: center">
                                        <label style="margin-bottom: 0 !important; margin-right: 4px;">Puasa</label>
                                        <input type="radio" name="form[assesment_gizi][bentuk]" value="3" <?=$form['assesment_gizi']['bentuk'] == 3 ? 'checked' : ''?>>
                                    </div>
                                    <div style="display: flex; align-items: center">
                                        <label style="margin-bottom: 0 !important; margin-right: 4px;">Lunak</label>
                                        <input type="radio" name="form[assesment_gizi][bentuk]" value="4" <?=$form['assesment_gizi']['bentuk'] == 4 ? 'checked' : ''?>>
                                    </div>
                                    <div style="display: flex; align-items: center">
                                        <label style="margin-bottom: 0 !important; margin-right: 4px;">Sonde</label>
                                        <input type="radio" name="form[assesment_gizi][bentuk]" value="5" <?=$form['assesment_gizi']['bentuk'] == 5 ? 'checked' : ''?>>
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Monitoring:</label>
                            <textarea name="form[assesment_gizi][monitoring]" class="form-control"><?=$form['assesment_gizi']['monitoring']?></textarea>
                        </td>
                        <td>
                            <label>Evaluasi:</label>
                            <textarea name="form[assesment_gizi][evaluasi]" class="form-control"><?=$form['assesment_gizi']['evaluasi']?></textarea>
                        </td>
                    </tr>
                    </tbody>
                </table>
                <div class="text-center">
                    <?php if (superadmin($this->session->userdata('logged_in')) || gizi($this->session->userdata('logged_in'))) : ?>
                    <button class="btn btn-success">
                        Simpan
                    </button>
                    <?php endif; ?>
                </div>
            </form>
        </div>
        <div class="col-sm-12 col-md-5">
            <h4 class="text-center"><b>MAKANAN</b></h4>
            <div class="table-responsive">
                <table class="table table-bordered example">
                    <thead>
                    <tr>
                        <th>No</th>
                        <th>Item</th>
                        <th>Biaya</th>
                        <th>Aksi</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $no = 1;
                    $total = 0;
                    foreach($biaya as $k => $r) : ?>
                        <?php if ($r->jenis_biaya != 'Makanan') : ?>
                            <?php continue; ?>
                        <?php endif ?>
                        <tr class="row-biaya">
                            <td><?=$no++?></td>
                            <td><?=$r->makanan->nama?></td>
                            <td class="text-right"><?=number_format($r->biaya, 0, ',', '.')?></td>
                            <td style="width: 50px">
                                <a href="<?=base_url()?>RawatInap/hapus/makanan/<?=$rawat_inap->id?>/<?=$r->id?>"
                                   onclick="return confirm('Hapus data ini?')"
                                   class="btn btn-danger btn-sm">
                                    <span class="fa fa-trash"></span> Hapus
                                </a>
                            </td>
                        </tr>
                        <?php
                        $total += $r->biaya;
                    endforeach;
                    ?>
                    </tbody>


                    <tr class="box box-solid total-bg">
                        <td colspan='4' class="text-right">
                            <?php echo $this->lang->line('total') . " : " . $currency_symbol . ""?> <?=number_format($total, 0, ',', '.')?>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>

<script>
    $(function () {
        $(`input[type=radio][name^='form[assesment_gizi]']`).change(function() {
            const a1 = +$(`input[type=radio][name='form[assesment_gizi][a1]']:checked`).data('value') || 0
            const a11 = +$(`input[type=radio][name='form[assesment_gizi][a11]']:checked`).data('value') || 0
            const a2 = +$(`input[type=radio][name='form[assesment_gizi][a2]']:checked`).data('value') || 0
            const total = a1 + a11 + a2
            $('#total_skor_a').html(total)
            $('#total_skor_a_input').val(total)

            const b1 = +$(`input[type=radio][name='form[assesment_gizi][b1]']:checked`).data('value') || 0
            const b2 = +$(`input[type=radio][name='form[assesment_gizi][b2]']:checked`).data('value') || 0
            const b3 = +$(`input[type=radio][name='form[assesment_gizi][b3]']:checked`).data('value') || 0
            const totalb = b1 + b2 + b3
            $('#total_skor_b').html(totalb)
            $('#total_skor_b_input').val(totalb)
        })
    })
</script>