<div class="tab-pane <?=isset($tab) && $tab == 'radiologi' ? 'active' : ''?>" id="radiologi">
    <div class="impbtnview">
        <?php if ($show_action) : ?>
            <a class="btn btn-sm btn-primary dropdown-toggle addconsultant" href="<?= base_url() ?>Permintaan/radio_from_inap/<?=$rawat_inap->pendaftaran_id?>/<?=$rawat_inap->id?>">
                <i class="fa fas fa-x-ray"></i> Rujuk Ke Radiologi
            </a>
        <?php endif; ?>
    </div>
    <div class="table-responsive">
        <table class="table table-bordered example">
            <thead>
            <tr>
                <th>No</th>
                <th>Tanggal Periksa</th>
                <th>Tindakan</th>
                <th>Hasil Tes</th>
                <th>Hasil Bacaan</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($radio as $k => $r) : ?>
                <tr>
                    <td> <?php echo $k + 1; ?></td>
                    <td> <?php echo date('d-F-Y H:i', strtotime($r->waktu_pemeriksaan)); ?></td>
                    <td>
                        <table style="font-size: 10px;" class="table table-bordered">
                            <thead>
                            <tr>
                                <th>Nama Tindakan</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            if ($all_tindakan) {
                                foreach ($all_tindakan->result() as $r3) {
                                    if ($r->id == $r3->pemeriksaan_id) {
                                        ?>
                                        <tr> <td> <?php echo $r3->nama; ?></td></tr>
                                    <?php }
                                }
                            } else {
                                ?>
                                <tr>
                                    <td bgcolor="lightblue" colspan="3"> Data Tidak ada</td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </td>
                    <td>
                        <?php $result = unserialize($r->meta); ?>
                        <?php for ($i = 0; $i <= 4; $i++) : ?>
                            <?php if (isset($result['file_hasil_tes_'.$i])) : ?>
                                <a href="<?=base_url('/assets/img/radiologi/'.$result['file_hasil_tes_'.$i])?>" target="_blank">
                                    <img src="<?=base_url('/assets/img/radiologi/'.$result['file_hasil_tes_'.$i])?>" alt="" height="60"/>
                                </a>
                            <?php endif; ?>
                        <?php endfor; ?>
                    </td>
                    <td>
                        <?php $result = unserialize($r->meta); ?>
                        <p><?=$result['hasil_bacaan'] ?? ''?></p>
                        <?php for ($i = 0; $i <= 4; $i++) : ?>
                            <?php if (isset($result['file_hasil_bacaan_'.$i])) : ?>
                                <?php if (strtolower(explode('.', $result['file_hasil_bacaan_'.$i])[count(explode('.', $result['file_hasil_bacaan_'.$i])) - 1]) == 'pdf') : ?>
                                    <a href="<?=base_url('/assets/img/radiologi/'.$result['file_hasil_bacaan_'.$i])?>" target="_blank">PDF File</a><br>
                                <?php else : ?>
                                    <a href="<?=base_url('/assets/img/radiologi/'.$result['file_hasil_bacaan_'.$i])?>" target="_blank">
                                        <img src="<?=base_url('/assets/img/radiologi/'.$result['file_hasil_bacaan_'.$i])?>" alt="" height="60"/>
                                    </a>
                                <?php endif; ?>
                            <?php endif; ?>
                        <?php endfor; ?>
                    </td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>