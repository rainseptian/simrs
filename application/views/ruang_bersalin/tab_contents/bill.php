<div class="tab-pane <?=$tab == 'bill' ? 'active' : ''?>" id="bill">
    <div class="row">
        <div class="col-md-12">
            <section class="invoice">
                <div class="row">
                    <div class="col-xs-12">
                        <i class="fa fa-user"></i> NO. RM <?= $pasien->no_rm ?>
                        </h2>
                    </div>
                </div>
                <form enctype="multipart-data" method="POST" action="<?= base_url() ?>Administrasi/nota_submit">
                    <div class="row invoice-info">
                        <div class="col-sm-12 invoice-col">
                            Nama Pasien: <strong><?= $pasien->nama ?></strong><br>
                            Jenis Pendaftaran: <?= $rawat_inap->tipe_pasien ?><br>
                            <br>
                            <address>
                                Alamat : <?= $pasien->alamat ?><br>
                                Usia: <?= $pasien->usia ?><br>
                                Telepon: <?= $pasien->telepon ?><br>
                            </address>
                        </div>
                        <div class="col-xs-12">
                            <div class="row">
                                <div class="col-sm-12">
                                    <table class="table table-bordered">
                                        <tbody>
                                        <tr>
                                            <th style="width: 10px">#</th>
                                            <th>Tindakan</th>
                                            <th class="text-right">Biaya</th>
                                        </tr>
                                        <?php
                                        $total_tindakan = 0;
                                        $tindakans = [];
                                        foreach ($biaya as $b) {
                                            if ($b->jenis_biaya == 'Tindakan') {
                                                foreach ($b->tindakan as $t) {
                                                    $tindakans[] = $t;
                                                }
                                            }
                                        }
                                        $no = 1;
                                        foreach ($tindakans as $key => $value) { ?>
                                            <tr>
                                                <td><?=$no++?></td>
                                                <td><?= $value->nama ?>
                                                    <input type="hidden" name="nama_tindakan[]" value="<?= $value->nama ?>">
                                                </td>
                                                <td align="right">
                                                    <?= harga($value->tarif_pasien); ?>
                                                    <input type="hidden" name="tarif_pasien[]" value="<?= $value->tarif_pasien ?>">
                                                </td>
                                            </tr>
                                            <?php
                                            $total_tindakan += $value->tarif_pasien;
                                        } ?>
                                        <tr>
                                            <td align="right" colspan="2"><strong>Total: </strong></td>
                                            <td align="right"><strong><?=harga($total_tindakan)?></strong></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <table class="table table-bordered">
                                        <tbody>
                                        <tr>
                                            <th style="width: 10px">#</th>
                                            <th>Obat</th>
                                            <th>jumlah</th>
                                            <th class="text-right">Harga Satuan</th>
                                            <th class="text-right">Biaya</th>
                                        </tr>
                                        <?php
                                        $total_obat = 0;
                                        $obats = [];
                                        foreach ($resep as $r) {
                                            if ($r->obat) {
                                                foreach ($r->obat as $o) {
                                                    $obats[] = $o;
                                                }
                                            }
                                        }
                                        $no = 1;
                                        foreach ($obats as $key => $value) { ?>
                                            <tr>
                                                <td><?=$no++?></td>
                                                <td><?= $value->nama ?><input type="hidden" name="nama_obat[]" value="<?= $value->nama ?>"></td>
                                                <td><?= $value->jumlah ?><input type="hidden" name="jumlah_satuan[]" value="<?= $value->jumlah ?>"></td>
                                                <td class="text-right"><?= harga($value->harga_jual); ?>
                                                    <input type="hidden" name="harga_jual[]" value="<?= $value->harga_jual ?>">
                                                </td>
                                                <td class="text-right"><?= harga($value->harga_jual * $value->jumlah); ?>
                                                    <input type="hidden" name="subtotal_obat[]" value="<?= $value->harga_jual * $value->jumlah; ?>">
                                                </td>
                                            </tr>
                                            <?php $total_obat += ($value->harga_jual * $value->jumlah);
                                        } ?>
                                        <tr>
                                            <td class="text-right" colspan="4"><strong>Total: </strong></td>
                                            <td class="text-right"><strong><?=harga($total_obat)?></strong></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <table class="table table-bordered">
                                        <tbody>
                                        <tr>
                                            <th style="width: 10px">#</th>
                                            <th>Obat Racik</th>
                                            <th class="text-right">Biaya</th>
                                        </tr>
                                        <?php
                                        $total_obatracik = 0;
                                        $racikans = [];
                                        foreach ($resep as $r) {
                                            if ($r->obat_racik) {
                                                foreach ($r->obat_racik as $o) {
                                                    $racikans[] = $o;
                                                }
                                            }
                                        }
                                        $no = 1;
                                        foreach ($racikans as $key => $value) { ?>
                                            <tr>
                                                <td><?=$no++?></td>
                                                <td>
                                                    <table class="table table-condensed" style="margin-bottom: 0 !important;">
                                                        <tbody>
                                                        <tr>
                                                            <th>Obat</th>
                                                            <th>Jumlah</th>
                                                            <th class="text-right">Harga Satuan</th>
                                                            <th class="text-right">Total</th>
                                                        </tr>
                                                        <?php foreach ($value->detail as $k => $v) { ?>
                                                            <tr>
                                                                <td><?= $v->nama ?></td>
                                                                <td><?= $v->jumlah ?></td>
                                                                <td class="text-right"><?= harga($v->harga_jual); ?></td>
                                                                <td class="text-right"><?= harga($v->harga_jual * $v->jumlah); ?></td>
                                                            </tr>
                                                        <?php } ?>
                                                        </tbody>
                                                    </table>
                                                </td>
                                                <td class="text-right align-bottom" style="vertical-align: bottom !important;">
                                                    <?= harga($value->total); ?>
                                                    <input type="hidden" name="total_racikan[]" value="<?= $value->total; ?>">
                                                </td>
                                            </tr>

                                            <?php $total_obatracik += $value->total; ?>
                                        <?php } ?>
                                        <tr>
                                            <td class="text-right" colspan="2"><strong>Total: </strong></td>
                                            <td class="text-right"><strong><?=harga($total_obatracik)?></strong></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <table class="table table-bordered">
                                        <tbody>
                                        <tr>
                                            <th style="width: 10px">#</th>
                                            <th>Bahan Habis Pakai</th>
                                            <th>jumlah</th>
                                            <th class="text-right">Harga Satuan</th>
                                            <th class="text-right">Biaya</th>
                                        </tr>
                                        <?php
                                        $total_bahan = 0;
                                        $bahans = [];
                                        foreach ($bahan_habis_pakai as $bhp) {
                                            if ($bhp->bahan) {
                                                foreach ($bhp->bahan as $b) {
                                                    $bahans[] = $b;
                                                }
                                            }
                                        }
                                        $no = 1;
                                        foreach ($bahans as $key => $value) { ?>
                                            <tr>
                                                <td><?=$no++?></td>
                                                <td><?= $value->nama ?><input type="hidden" name="nama_bahan[]" value="<?= $value->nama ?>"></td>
                                                <td><?= $value->jumlah ?><input type="hidden" name="jumlah_satuan_bahan[]" value="<?= $value->jumlah ?>"></td>
                                                <td align="right">
                                                    <?= harga($value->harga_jual); ?>
                                                    <input type="hidden" name="harga_jual_bahan[]" value="<?= $value->harga_jual ?>">
                                                </td>
                                                <td align="right">
                                                    <?= harga($value->harga_jual * $value->jumlah); ?>
                                                    <input type="hidden" name="subtotal_bahan[]" value="<?= $value->harga_jual * $value->jumlah; ?>">
                                                </td>
                                            </tr>
                                            <?php $total_bahan += ($value->harga_jual * $value->jumlah);
                                        } ?>
                                        <tr>
                                            <td class="text-right" colspan="4"><strong>Total: </strong></td>
                                            <td class="text-right"><strong><?=harga($total_bahan)?></strong></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <table class="table table-bordered">
                                        <tbody>
                                        <tr>
                                            <th style="width: 10px">#</th>
                                            <th>Makanan</th>
                                            <th>jumlah</th>
                                            <th class="text-right">Harga Satuan</th>
                                            <th class="text-right">Biaya</th>
                                        </tr>
                                        <?php
                                        $total_makanan = 0;
                                        $no = 1;
                                        foreach ($makanan_count_list as $key => $value) { ?>
                                            <tr>
                                                <td><?=$no++?></td>
                                                <td><?= $value->nama ?><input type="hidden" name="nama_bahan[]" value="<?= $value->nama ?>"></td>
                                                <td><?= $value->jumlah ?><input type="hidden" name="jumlah_satuan_bahan[]" value="<?= $value->jumlah ?>"></td>
                                                <td align="right">
                                                    <?= harga($value->harga); ?>
                                                    <input type="hidden" name="harga_jual_bahan[]" value="<?= $value->harga ?>">
                                                </td>
                                                <td align="right">
                                                    <?= harga($value->harga * $value->jumlah); ?>
                                                    <input type="hidden" name="subtotal_bahan[]" value="<?= $value->harga * $value->jumlah; ?>">
                                                </td>
                                            </tr>
                                            <?php $total_makanan += ($value->harga * $value->jumlah);
                                        } ?>
                                        <tr>
                                            <td class="text-right" colspan="4"><strong>Total: </strong></td>
                                            <td class="text-right"><strong><?=harga($total_makanan)?></strong></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <table class="table table-bordered">
                                        <tbody>
                                        <tr>
                                            <th style="width: 10px">#</th>
                                            <th>Bed</th>
                                            <th>Waktu</th>
                                            <th class="text-right">Harga</th>
                                            <th class="text-right">Biaya</th>
                                        </tr>
                                        <?php
                                        function getHowManyDays($start, $end, $add) {
                                            $d1 = date_create(date( 'Y-m-d', strtotime($start)));
                                            $d2 = date_create(date( 'Y-m-d', strtotime($end)));
                                            $waktu = (int) date_diff($d1, $d2)->format("%a");
                                            if ($add || $waktu == 0) {
                                                $waktu++;
                                            }
                                            return $waktu;
                                        }

                                        $total_biaya_bed = 0;
                                        $no = 1;
                                        foreach ($beds as $k => $v) : ?>
                                            <tr>
                                                <td><?=$no++?></td>
                                                <td><?=$v['bed']['name'].' - '.$v['bed']['bedgroup']?></td>
                                                <td>
                                                    <?php
                                                    $count = getHowManyDays(
                                                        $v['start'],
                                                        $v['end'] == 'now' ? date("Y-m-d H:i") : $v['end'],
                                                        $k == 0
                                                    );
                                                    echo $count;
                                                    ?> hari<br>
                                                    <i style="font-size: 12px;">
                                                        Tgl masuk: <?=date( 'd-m-Y H:i', strtotime($v['start']))?><br>
                                                        Tgl keluar: <?=$v['end'] == 'now' ? '-' : date( 'd-m-Y H:i', strtotime($v['end']))?>
                                                    </i>
                                                </td>
                                                <td class="text-right"><?=harga($v['bed']['group_price'])?></td>
                                                <td align="right">
                                                    <?php
                                                    $jml = $v['bed']['group_price'] * $count;
                                                    $total_biaya_bed += $jml;
                                                    echo harga($jml);
                                                    ?>
                                                </td>
                                            </tr>
                                        <?php endforeach; ?>
                                        <tr>
                                            <td class="text-right" colspan="4"><strong>Total: </strong></td>
                                            <td class="text-right"><strong><?=harga($total_biaya_bed)?></strong></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <h4 class="box-title ptt10">Ringkasan Billing</h4>
                            <div class="table-responsive" style="border: 1px solid #dadada;border-radius: 2px; padding: 10px;">
                                <table class="nobordertable table table-striped table-responsive">
                                    <tr>
                                        <th>Total Tindakan</th>
                                        <td class="text-right fontbold20"><?php echo harga($total_tindakan) ?></td>
                                    </tr>
                                    <tr>
                                        <th>Total Obat</th>
                                        <td class="text-right fontbold20"><?php echo harga($total_obat) ?></td>
                                    </tr>
                                    <tr>
                                        <th>Total Obat Racik</th>
                                        <td class="text-right fontbold20"><?php echo harga($total_obatracik) ?></td>
                                    </tr>
                                    <tr>
                                        <th>Total Bahan Habis Pakai</th>
                                        <td class="text-right fontbold20"><?php echo harga($total_bahan) ?></td>
                                    </tr>
                                    <tr>
                                        <th>Total Makanan</th>
                                        <td class="text-right fontbold20"><?php echo harga($total_makanan) ?></td>
                                    </tr>
                                    <tr>
                                        <th>Total Biaya Bed</th>
                                        <td class="text-right fontbold20"><?php echo harga($total_biaya_bed) ?></td>
                                    </tr>
                                    <tr>
                                        <th style="vertical-align: middle !important;">Total Tagihan</th>
                                        <td class="text-right fontbold20">
                                            <h4 style="font-weight: bold">
                                                <?php
                                                $total_tagihan = $total_tindakan + $total_obat + $total_obatracik + $total_bahan + $total_makanan + $total_biaya_bed;
                                                echo harga($total_tagihan);
                                                ?>
                                            </h4>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </form>
            </section>
        </div>
    </div>
</div>