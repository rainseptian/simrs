<div class="tab-pane <?=isset($tab) && $tab == 'lab' ? 'active' : ''?>" id="lab">
    <div class="impbtnview">
        <?php if ($show_action) : ?>
            <a class="btn btn-sm btn-primary dropdown-toggle addconsultant" href="<?= base_url() ?>Permintaan/lab_from_inap/<?=$rawat_inap->pendaftaran_id?>/<?=$rawat_inap->id?>">
                <i class="fa fas fa-flask"></i> Rujuk Ke Lab
            </a>
        <?php endif; ?>
    </div>
    <div class="table-responsive">
        <table class="table table-bordered example">
            <thead>
            <tr>
                <th>No</th>
                <th>Tanggal Periksa</th>
                <th>Jenis Layanan</th>
                <th>Aksi</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($lab as $k => $row) : ?>
                <tr>
                    <td> <?php echo $k + 1; ?></td>
                    <td> <?php echo date('d-F-Y H:i', strtotime($row->waktu_pemeriksaan)); ?></td>
                    <td>
                        <table style="padding: 0px 5px;">
                            <tbody>
                            <?php foreach ($row->layanan as $l) { ?>
                                <tr>
                                    <td><?=$l->nama?></td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </td>
                    <td>
                        <a href="<?php echo base_url(); ?>laboratorium/detail/<?php echo $row->id; ?>/1"
                           target="_blank"
                           class="btn btn-sm btn-success"
                           style="margin-bottom: 5px;" >
                            <i class="fa fa-arrows"></i> Detail
                        </a>
                    </td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>