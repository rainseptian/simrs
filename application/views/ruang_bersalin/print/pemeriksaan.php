<html>
<head>
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/bower_components/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/dist/css/AdminLTE.min.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/dist/css/skins/_all-skins.min.css">
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <style>
        table, td, th {
            border: 1px solid #666666 !important;
        }
        td, th {
            padding: 0 4px !important;
        }
        * {
            font-family: "Times New Roman", serif;
        }

        .table.no-border,
        .table.no-border td,
        .table.no-border th,
        .table.no-border > * > td,
        .table.no-border > * > th {
            border: 0 !important;
        }
    </style>

    <style type="text/css" media="print">
        @page { size: landscape; }
    </style>
</head>
<body>

<table class="table table-striped table-bordered table-hover example">
    <thead>
    <tr>
        <td colspan="3">
            <div style="display: flex; flex-direction: column; align-items: center">
                <img src="<?php echo base_url(); ?>assets/img/klinik/<?php echo $klinik->foto; ?>" height="70px">
            </div>
        </td>
        <td colspan="7">
            <table class="table no-border" style="font-size: 12px">
                <tbody>
                <tr>
                    <td>Nama Pasien</td>
                    <td>:</td>
                    <td><?=$pasien->nama?></td>
                    <td>No RM</td>
                    <td>:</td>
                    <td><?=$pasien->no_rm?></td>
                </tr>
                <tr>
                    <td>Jenis Kelamin</td>
                    <td>:</td>
                    <td><?=$pasien->jk?></td>
                    <td>Tgl Lahir</td>
                    <td>:</td>
                    <td><?=$pasien->tanggal_lahir?> / <?=$pasien->usia?></td>
                </tr>
                <tr>
                    <td>Ruang / Kelas</td>
                    <td>:</td>
                    <td><?=$rawat_inap->bed_name.' - '.$rawat_inap->bedgroup?></td>
                    <td>Tgl Masuk</td>
                    <td>:</td>
                    <td><?=$rawat_inap->created_at?></td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    <tr>
        <th>No</th>
        <th>Tgl Pemeriksaan</th>
        <th>Nakes</th>
        <th>Deteksi Vital</th>
        <th>Subjektif</th>
        <th>Objektif</th>
        <th>Asesmen</th>
        <th>Plan</th>
        <th>ICD 10</th>
        <th>Tindakan</th>
        <th>Resep</th>
    </tr>
    </thead>
    <tbody>
    <?php
    $no = 1;
    if (!empty($pemeriksaan)) {
        foreach ($pemeriksaan as $r) {
            ?>
            <tr class="row-pemeriksaan">
                <td><?=$no++?></td>
                <td>
                    <?=$r->created_at?>
                    <br>
                    Shift <?=$r->shift?>
                </td>
                <td>
                    <?php $nakes = json_decode($r->nakes); ?>
                    <?php if ($r->creator_type == 'perawat') : ?>
                        <b>Perawat:</b> <?=join(', ', array_map(function ($v) { return $v->name; }, $nakes->p))?><br>
                    <?php elseif ($r->creator_type == 'dokter') : ?>
                        <b>Dokter:</b> <?=$nakes->d->name ?? ''?><br>
                    <?php elseif ($r->creator_type == 'apoteker') : ?>
                        <b>Apoteker:</b> <?=$nakes->a->name ?? ''?><br>
                    <?php elseif ($r->creator_type == 'gizi') : ?>
                        <b>Gizi:</b> <?=$nakes->g->name ?? ''?><br>
                    <?php endif; ?>
                </td>
                <td>
                    <table class="table table-bordered" style="margin-bottom: 0 !important;">
                        <?php
                        $obj = unserialize($r->objektif);
                        foreach ($obj as $k => $v) : ?>
                            <tr>
                                <td style="padding-top: 0 !important; padding-bottom: 0 !important; font-weight: bold;">
                                    <?=$k?>
                                </td>
                                <td style="padding-top: 0 !important; padding-bottom: 0 !important;">
                                    <?=$v?>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </table>
                </td>
                <td>
                    <?php $subjectives = json_decode($r->subjectives); ?>
                    <?=$subjectives->p?><br>
                    <?=$subjectives->d?><br>
                    <?=$subjectives->a?><br>
                    <?=$subjectives->g?>
                </td>
                <td>
                    <?php $objectives = json_decode($r->objectives); ?>
                    <?=$objectives->p?><br>
                    <?=$objectives->d?><br>
                    <?=$objectives->a?><br>
                    <?=$objectives->g?>
                </td>
                <td>
                    <?php $assessments = json_decode($r->assessments); ?>
                    <?=$assessments->p?><br>
                    <?=$assessments->d?><br>
                    <?=$assessments->a?><br>
                    <?=$assessments->g?>
                </td>
                <td>
                    <?php $plans = json_decode($r->plans); ?>
                    <?=$plans->p?><br>
                    <?=$plans->d?><br>
                    <?=$plans->a?><br>
                    <?=$plans->g?>
                </td>
                <td>
                    <?= join(', ', array_map(function ($p) { return $p->kode.' - '.$p->nama; }, $r->penyakit)) ?>
                </td>
                <td>
                    <?= join(', ', array_map(function ($p) { return $p->nama; }, $r->tindakan)) ?>
                </td>
                <td style="font-size: 12px">
                    <?php if (count($r->obat)) : ?>
                        <table class="table table-condensed">
                            <thead>
                            <tr>
                                <th>Nama Obat</th>
                                <th>Jumlah</th>
                                <th>Signa</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($r->obat as $obat) : ?>
                                <tr>
                                    <td><?=$obat->nama?></td>
                                    <td><?=$obat->jumlah?></td>
                                    <td><?=$obat->signa?></td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                    <?php endif; ?>
                    <?php if (count($r->obat_racik)) : ?>
                        <table class="table table-condensed">
                            <thead>
                            <tr>
                                <th>Racikan</th>
                                <th>Signa</th>
                                <th>Catatan</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($r->obat_racik as $racik) : ?>
                                <tr>
                                    <td>
                                        <table class="table table-condensed">
                                            <thead>
                                            <tr>
                                                <th>Nama Obat</th>
                                                <th>Jumlah</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php foreach ($racik->detail as $detail) : ?>
                                                <tr>
                                                    <td><?=$detail->nama?></td>
                                                    <td><?=$detail->jumlah?></td>
                                                </tr>
                                            <?php endforeach; ?>
                                            </tbody>
                                        </table>
                                    </td>
                                    <td><?=$racik->signa?></td>
                                    <td><?=$racik->catatan?></td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                    <?php endif; ?>
                </td>
            </tr>
            <?php
        }
    }
    ?>
    </tbody>
</table>

<script>
    $(function () {
        print()
    })
</script>
</body>
</html>