<html>
<head>
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/bower_components/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/dist/css/AdminLTE.min.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/dist/css/skins/_all-skins.min.css">
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <style>
        table, td, th {
            border: 1px solid #666666 !important;
        }
        td, th {
            padding: 0 4px !important;
        }
        * {
            font-family: "Times New Roman", serif;
        }

        .table.no-border,
        .table.no-border td,
        .table.no-border th,
        .table.no-border > * > td,
        .table.no-border > * > th {
            border: 0 !important;
        }
    </style>

    <style type="text/css" media="print">
        @page { size: landscape; }
    </style>
</head>
<body>

<table class="table table-bordered" style="table-layout: fixed; font-size: 12px">
    <tbody>
    <tr>
        <td colspan="5">
            <div style="display: flex; flex-direction: column; align-items: center">
                <img src="<?php echo base_url(); ?>assets/img/klinik/<?php echo $klinik->foto; ?>" height="70px">
                <div style="text-align: center; margin-left: 20px; margin-top: 5px">
                    <small style="font-size: 18px"><?=$klinik->nama?></small><br>
                    <small style="font-size: 11px"><?=$klinik->alamat?></small>
                </div>
            </div>
        </td>
        <td colspan="11">
            <table class="table no-border" style="font-size: 12px">
                <tbody>
                <tr>
                    <td>Nama Pasien</td>
                    <td>:</td>
                    <td><?=$pasien->nama?></td>
                    <td>No RM</td>
                    <td>:</td>
                    <td><?=$pasien->no_rm?></td>
                </tr>
                <tr>
                    <td>Jenis Kelamin</td>
                    <td>:</td>
                    <td><?=$pasien->jk?></td>
                    <td>Tgl Lahir</td>
                    <td>:</td>
                    <td><?=$pasien->tanggal_lahir?> / <?=$pasien->usia?></td>
                </tr>
                <tr>
                    <td>Ruang / Kelas</td>
                    <td>:</td>
                    <td><?=$rawat_inap->bed_name.' - '.$rawat_inap->bedgroup?></td>
                    <td>Tgl Masuk</td>
                    <td>:</td>
                    <td><?=$rawat_inap->created_at?></td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    <tr>
        <th colspan="16" style="text-align: center; vertical-align: middle; font-size: 16px">SURVEILANS INFEKSI NOSOKOMIAL</th>
    </tr>
    <tr>
        <td colspan="5">
            <label>Bulan :</label> <?=$form['surveilans_header']['bulan']?>
        </td>
        <td colspan="5">
            <label>Surveyor :</label> <?=$form['surveilans_header']['surveyor']?>
        </td>
        <td colspan="5">
            <label>Diagnosa :</label> <?=$form['surveilans_header']['diagnosa']?>
        </td>
        <td style="vertical-align: middle">

        </td>
    </tr>
    <tr>
        <th class="text-center" style="vertical-align: middle" rowspan="2">Tgl/Jam</th>
        <th class="text-center" style="vertical-align: middle" rowspan="2">Suhu</th>
        <th class="text-center" style="vertical-align: middle" colspan="4">TINDAKAN</th>
        <th class="text-center" style="vertical-align: middle" colspan="7">INFEKSI NOSOKOMIAL</th>
        <th class="text-center" style="vertical-align: middle" rowspan="2">TIRAH BARING</th>
        <th class="text-center" style="vertical-align: middle" rowspan="2">HASIL KULTUR / LABORAT</th>
        <th class="text-center" style="vertical-align: middle" rowspan="2">ANTIBIOTIK</th>
    </tr>
    <tr>
        <th class="text-center" style="vertical-align: middle">Kateter</th>
        <th class="text-center" style="vertical-align: middle">Vena Perifer</th>
        <th class="text-center" style="vertical-align: middle">Vena Sentral</th>
        <th class="text-center" style="vertical-align: middle">Ventilator</th>
        <th class="text-center" style="vertical-align: middle">VAP</th>
        <th class="text-center" style="vertical-align: middle">HAB</th>
        <th class="text-center" style="vertical-align: middle">ISK</th>
        <th class="text-center" style="vertical-align: middle">LADP</th>
        <th class="text-center" style="vertical-align: middle">PLEB</th>
        <th class="text-center" style="vertical-align: middle">DECUB</th>
        <th class="text-center" style="vertical-align: middle">ILO</th>
    </tr>
    <?php foreach ($form['surveilans'] as $v) : ?>
        <tr>
            <td><?=date('d/M/y H:i', strtotime(str_replace('T', ' ', $v['tgl'])))?></td>
            <td><div style="white-space: pre-wrap;"><?=$v['suhu']?></div></td>
            <td><div style="white-space: pre-wrap;"><?=$v['Kateter']?></div></td>
            <td><div style="white-space: pre-wrap;"><?=$v['VenaPerifer']?></div></td>
            <td><div style="white-space: pre-wrap;"><?=$v['VenaSentral']?></div></td>
            <td><div style="white-space: pre-wrap;"><?=$v['Ventilator']?></div></td>
            <td><div style="white-space: pre-wrap;"><?=$v['VAP']?></div></td>
            <td><div style="white-space: pre-wrap;"><?=$v['HAB']?></div></td>
            <td><div style="white-space: pre-wrap;"><?=$v['ISK']?></div></td>
            <td><div style="white-space: pre-wrap;"><?=$v['LADP']?></div></td>
            <td><div style="white-space: pre-wrap;"><?=$v['PLEB']?></div></td>
            <td><div style="white-space: pre-wrap;"><?=$v['DECUB']?></div></td>
            <td><div style="white-space: pre-wrap;"><?=$v['ILO']?></div></td>
            <td><div style="white-space: pre-wrap;"><?=$v['TIRAHBARING']?></div></td>
            <td><div style="white-space: pre-wrap;"><?=$v['HASIL']?></div></td>
            <td><div style="white-space: pre-wrap;"><?=$v['ANTIBIOTIK']?></div></td>
        </tr>
    <?php endforeach; ?>
    <tr>
        <td colspan="16">
            <div style="display: flex">
                <div style="display: flex; flex-direction: column; padding-right: 10px">
                    <span>VAP : Ventilator Associated Pneomonia</span>
                    <span>HAP : Hospital Acquired Pneomonia</span>
                    <span>ISK : Infeksi Saluran Kemih</span>
                </div>
                <div style="display: flex; flex-direction: column; padding-right: 10px">
                    <span>PLEB : Plebtis</span>
                    <span>DECUB: Decubitus</span>
                    <span>ILO : Infeksi Luka Operasi</span>
                </div>
                <div style="display: flex; flex-direction: column">
                    <span>IADP : Infeksi ALiran DarahPrimer</span>
                </div>
            </div>
        </td>
    </tr>
    </tbody>
</table>

<script>
    $(function () {
        print()
    })
</script>
</body>
</html>