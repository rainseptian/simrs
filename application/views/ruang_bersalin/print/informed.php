<html>
<head>
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/bower_components/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/dist/css/AdminLTE.min.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/dist/css/skins/_all-skins.min.css">
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <style>
        table, td, th {
            border: 1px solid #666666 !important;
        }
        td, th {
            padding: 0 4px !important;
        }
        * {
            font-family: "Times New Roman", serif;
        }
    </style>
</head>
<body>
<table class="table table-bordered">
    <tbody>
    <tr>
        <td colspan="3">
            <div style="display: flex; align-items: stretch">
                <div style="display: flex; flex: 1; border-right: 1px solid #dddddd; align-items: center">
                    <img src="<?php echo base_url(); ?>assets/img/klinik/<?php echo $klinik->foto; ?>" height="70px">
                    <div style="text-align: center; margin-left: 20px; margin-top: 5px">
                        <small style="font-size: 18px"><?=$klinik->nama?></small><br>
                        <small style="font-size: 11px"><?=$klinik->alamat?></small>
                    </div>
                </div>
                <div style="flex: 1; text-align: center;">
                    <h3 style="font-family: 'Times New Roman', serif; font-weight: bold; padding: 0 20px !important;">
                        Informed Consent Tindakan Kedokteran (Operasi/ Tindakan Invasif)
                    </h3>
                </div>
            </div>
        </td>
    </tr>
    <tr>
        <td colspan="3">
            <h4 class="text-center"><b>Pemberian Informasi (INFORM)</b></h4>
        </td>
    </tr>
    <tr>
        <td>Dokter Pelaksana Tindakan</td>
        <td colspan="2">
            <?=$form['informed']['pelaksana']?>
        </td>
    </tr>
    <tr>
        <td>Pemberi Informasi</td>
        <td colspan="2">
            <?=$form['informed']['pemberi_informasi']?>
        </td>
    </tr>
    <tr>
        <td>Penerima Informasi / Pemberi Persetujuan</td>
        <td colspan="2">
            <?=$form['informed']['penerima_informasi']?>
        </td>
    </tr>
    <tr>
        <th style="width: 33%"><h4 style="margin: 0;"><b>Jenis Informasi</b></h4></th>
        <th style="width: 34%"><h4 style="margin: 0;"><b>Isi Informasi</b></h4></th>
        <th style="width: 33%"><h4 style="margin: 0;"><b>Verifikasi (✓)</b></h4></th>
    </tr>
    <tr>
        <td>1. Diagnosis (WD & DD)</td>
        <td><?=$form['informed']['isi_1']?></td>
        <td></td>
    </tr>
    <tr>
        <td>2. Dasar Diagnosis</td>
        <td><?=$form['informed']['isi_2']?></td>
        <td></td>
    </tr>
    <tr>
        <td>3. Tindakan Kedokteran</td>
        <td><?=$form['informed']['isi_3']?></td>
        <td></td>
    </tr>
    <tr>
        <td>4. Indikasi Tindakan</td>
        <td><?=$form['informed']['isi_4']?></td>
        <td></td>
    </tr>
    <tr>
        <td>5. Tata Cara</td>
        <td><?=$form['informed']['isi_5']?></td>
        <td></td>
    </tr>
    <tr>
        <td>6. Tujuan</td>
        <td><?=$form['informed']['isi_6']?></td>
        <td></td>
    </tr>
    <tr>
        <td>7. Resiko</td>
        <td><?=$form['informed']['isi_7']?></td>
        <td></td>
    </tr>
    <tr>
        <td>8. Komplikasi</td>
        <td><?=$form['informed']['isi_8']?></td>
        <td></td>
    </tr>
    <tr>
        <td>9. Prognosis</td>
        <td><?=$form['informed']['isi_9']?></td>
        <td></td>
    </tr>
    <tr>
        <td>10. Alternatif dan Resiko</td>
        <td><?=$form['informed']['isi_10']?></td>
        <td></td>
    </tr>
    <tr>
        <td>11. Pembiayaan</td>
        <td>
            <div style="display: flex; flex-direction: column">
                <?=$form['informed']['pembiayaan'] == 1 ? 'BPJS' : 'Umum'?><br>
                <?=$form['informed']['pembiayaan_input'] ? 'Rp ' . number_format($form['informed']['pembiayaan_input'], 0, ',', '.') : ''?>
            </div>
        </td>
        <td></td>
    </tr>
    <tr>
        <td colspan="3">
            <div style="display: flex; flex-direction: row; align-items: center">
                <div style="flex: 1"></div>
                <span style="margin-right: 4px">Sampang, </span>
                <span style="margin-right: 4px"><?=date('d-F-Y', strtotime($form['informed']['tanggal']))?></span>
                <span style="margin-right: 4px">Pukul</span>
                <?=$form['informed']['jam']?>
            </div>
        </td>
    </tr>
    <tr>
        <td colspan="3">
            <div style="display: flex">
                <div style="flex: 7; border-right: 1px solid #dddddd">
                    Dengan ini menyatakan bahwa Saya telah menerangkan hal-hal di atas secara benar dan jelas serta memberikan kesempatan untuk bertanya dan atau berdiskusi.
                </div>
                <div style="flex: 6; text-align: center">
                    <b>Tanda Tangan Dokter</b>
                    <br>
                    <br>
                    <br>
                </div>
            </div>
        </td>
    </tr>
    <tr>
        <td colspan="3">
            <div style="display: flex">
                <div style="flex: 7; border-right: 1px solid #dddddd">
                    Dengan ini menyatakan bahwa
                    (<span style="<?=$form['informed']['saya'] == 1 ? '' : 'text-decoration: line-through;'?>">saya</span>
                    / <span style="<?=$form['informed']['saya'] == 2 ? '' : 'text-decoration: line-through;'?>">keluarga</span>)
                    pasien <b><?=$pasien->nama?></b> telah menerima informasi sebagaimana di atas yang saya beri tanda paraf di kolom kanannya dan telah memahaminya.
                </div>
                <div style="flex: 6; text-align: center">
                    <b>Tanda Tangan pasien / wali</b>
                    <br>
                    <b>pasien / keluarga</b>
                    <br>
                    <br>
                </div>
            </div>
        </td>
    </tr>
    <tr>
        <td colspan="3">
            Ket: <i>bila pasien tidak kompeten atau tidak mau menerima informasi, maka penerimaan informasi adalah wali atau keluarga terdekatnya</i>
        </td>
    </tr>
    <tr>
        <td colspan="3">
            <h4 class="text-center"><b>PERSETUJUAN/PENOLAKAN TINDAKAN KEDOKTERAN (CONSENT)</b></h4>
        </td>
    </tr>
    <tr>
        <td colspan="3">
            Saya yang bertanda tangan di bawah ini:<br>
            <b>Nama:</b> <?=$form['informed']['nama']?> (<?=$form['informed']['jk'] == 1 ? 'L' : 'P'?>)<br>
            <b>No. KTP/SIM/Passpor:</b> <?=$form['informed']['no_ktp'] ?: '-'?> <b>Tgl Lahir:</b> <?=$form['informed']['tgl_lahir'] ? date('d-F-Y', strtotime($form['informed']['tgl_lahir'])) : '-'?> <b>Umur:</b> <?=$form['informed']['umur'] ?: '-'?> Tahun<br>
            <b>Alamat:</b> <?=$form['informed']['alamat'] ?: '-'?><br>
            <b>Telepon:</b> <?=$form['informed']['telepon'] ?: '-'?><br>
        </td>
    </tr>
    <tr>
        <td colspan="3">
            Hubungan dengan pasien:
            <br>
            <b>
                <?php if ($form['informed']['hubungan'] == 1) : ?>
                    Diri Sendiri
                <?php elseif ($form['informed']['hubungan'] == 2) : ?>
                    Suami
                <?php elseif ($form['informed']['hubungan'] == 3) : ?>
                    Istri
                <?php elseif ($form['informed']['hubungan'] == 4) : ?>
                    Anak
                <?php elseif ($form['informed']['hubungan'] == 5) : ?>
                    Orang Tua
                <?php else : ?>
                    -
                <?php endif; ?>
            </b>
        </td>
    </tr>
    <tr>
        <td colspan="3">
            Dengan ini menyatakan sesungguhnya, bahwa saya telah menerima informasi yang diberikan oleh dokter sebagaimana di atas dan telah memahaminya,
            untuk itu saya memberikan <b>persetujuan</b> untuk dilakukan tindakan kedokteran tersebut terhadap:<br>
            <b>Nama:</b> <?=$pasien->nama?> (<?=$pasien->jk?>)<br>
            <b>No. RM:</b> <?=$pasien->no_rm?> <b>Tgl Lahir / Umur:</b> <?=$pasien->tanggal_lahir?> / <?=$pasien->usia?><br>
            <b>Alamat:</b> <?=$pasien->alamat?>. <br>
            Saya memahami perlunya dan manfaat tindakan tersebut termasuk resiko dan komplikasi yang akan timbul,
            Saya juga menyadari bahwa ilmu Kedokteran bukanlah ilmu pasti, maka keberhasilan tindakan Kedokteran bukanlah keniscayaan,
            melainkan tergantung kepada Tuhan YME.
        </td>
    </tr>
    <tr>
        <td colspan="3">
            <div style="display: flex; flex-direction: row; align-items: center">
                <div style="flex: 1"></div>
                <span style="margin-right: 4px">Sampang, </span>
                <span style="margin-right: 4px"><?=$form['informed']['persetujuan']['tanggal'] ? date('d-F-Y', strtotime($form['informed']['persetujuan']['tanggal'])) : ''?></span>
                <span style="margin-right: 4px">Pukul</span>
                <?=$form['informed']['persetujuan']['jam'] ?: ''?>
            </div>
        </td>
    </tr>
    <tr>
        <td>
            <div class="text-center">
                <span>Saksi Petugas</span>
                <br>
                <br>
                <br>
                <span>(..........................)</span>
            </div>
        </td>
        <td>
            <div class="text-center">
                <span>Saksi Pasien</span>
                <br>
                <br>
                <br>
                <span>(..........................)</span>
            </div>
        </td>
        <td>
            <div class="text-center">
                <span>Yang Menyatakan</span>
                <br>
                <br>
                <br>
                <span>(<?=$form['informed']['nama'] ?: '-'?>)</span>
            </div>
        </td>
    </tr>
    <tr>
        <td colspan="3">
            <i>Bila pasien tidak kompeten, maka penerima informasi dan pemberi pernyataan persetujuan adalah wali atau keluarga terdekat</i>
        </td>
    </tr>
    </tbody>
</table>
<script>
    $(function () {
        print()
    })
</script>
</body>
</html>