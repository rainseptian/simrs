<html>
<head>
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/bower_components/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/dist/css/AdminLTE.min.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/dist/css/skins/_all-skins.min.css">
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <style>
        table, td, th {
            border: 1px solid #666666 !important;
        }
        td, th {
            padding: 0 4px !important;
        }
        * {
            font-family: "Times New Roman", serif;
        }

        .table.no-border,
        .table.no-border td,
        .table.no-border th,
        .table.no-border > * > td,
        .table.no-border > * > th {
            border: 0 !important;
        }
    </style>
</head>
<body>

<table class="table table-bordered" style="table-layout: fixed; font-size: 12px">
    <tbody>
    <tr>
        <td colspan="2">
            <div style="text-align: center; vertical-align: middle">
                <img src="<?php echo base_url(); ?>assets/img/klinik/<?php echo $klinik->foto; ?>" height="70px">
            </div>
        </td>
        <td colspan="3" class="text-center" style="vertical-align: middle">
            <h4><b class="text-center">FORMULIR KONSILIASI OBAT DAN DATA OBAT PASIEN YANG DIGUNAKAN SAAT MASUK KLINIK</b></h4>
        </td>
        <td colspan="3">
            <table class="table no-border" style="font-size: 12px">
                <tbody>
                <tr>
                    <td>No RM</td>
                    <td>:</td>
                    <td><?=$pasien->no_rm?></td>
                </tr>
                <tr>
                    <td>Nama</td>
                    <td>:</td>
                    <td><?=$pasien->nama?></td>
                </tr>
                <tr>
                    <td>Tgl Lahir</td>
                    <td>:</td>
                    <td><?=$pasien->tanggal_lahir?> / <?=$pasien->usia?></td>
                </tr>
                <tr>
                    <td>ALAMAT</td>
                    <td>:</td>
                    <td><?=$pasien->alamat?></td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    <tr>
        <th colspan="8" style="text-align: center; vertical-align: middle">SURVEILANS INFEKSI NOSOKOMIAL</th>
    </tr>
    <tr>
        <td colspan="4">
            <label>TANGGAL WAWANCARA : <?=$form['alergi_header']['tgl'] ? date('d/M/y', strtotime($form['alergi_header']['tgl'])) : ''?></label>
        </td>
        <td colspan="4">
            <label>JAM WAWANCARA : <?=$form['alergi_header']['jam'] ? date('H:i', strtotime($form['alergi_header']['jam'])) : ''?></label>
        </td>
    </tr>
    <tr>
        <td colspan="3">
            <label>ALERGI OBAT : </label>
            <?=$form['alergi_header']['alergi_1'] ? '<div>'.$form['alergi_header']['alergi_1'].'</div>' : ''?>
            <?=$form['alergi_header']['alergi_2'] ? '<div>'.$form['alergi_header']['alergi_2'].'</div>' : ''?>
            <?=$form['alergi_header']['alergi_3'] ? '<div>'.$form['alergi_header']['alergi_3'].'</div>' : ''?>
        </td>
        <td colspan="2">
            <label>MANIFESTASI ALERGI : </label>
            <div style="white-space: pre-wrap">
                <?=$form['alergi_header']['manifestasi']?>
            </div>
        </td>
        <td colspan="3">
            <label>DAMPAK : <?=$form['alergi_header']['dampak']?></label>
        </td>
    </tr>
    <tr>
        <th class="text-center" style="vertical-align: middle" colspan="2">NAMA OBAT</th>
        <th class="text-center" style="vertical-align: middle" rowspan="2">DOSIS</th>
        <th class="text-center" style="vertical-align: middle" rowspan="2">ATURAN PAKAI</th>
        <th class="text-center" style="vertical-align: middle" rowspan="2">RUTE PEMBERIAN</th>
        <th class="text-center" style="vertical-align: middle" rowspan="2">JUMLAH OBAT</th>
        <th class="text-center" style="vertical-align: middle" colspan="2">OBAT YANG DIGUNAKAN SAAT RAWAT</th>
    </tr>
    <tr>
        <th class="text-center" style="vertical-align: middle">NAMA GENERIK</th>
        <th class="text-center" style="vertical-align: middle">NAMA DAGANG</th>
        <th class="text-center" style="vertical-align: middle">YA</th>
        <th class="text-center" style="vertical-align: middle">TIDAK</th>
    </tr>
    <?php foreach ($form['alergi'] as $v) : ?>
        <tr>
            <td><?=$v['nm_generik']?></td>
            <td><?=$v['nm_dagang']?></td>
            <td><?=$v['dosis']?></td>
            <td><?=$v['aturan']?></td>
            <td><?=$v['rute']?></td>
            <td><?=$v['jumlah']?></td>
            <td class="text-center" style="vertical-align: middle"><?=$v['saat_rawat'] == 'y' ? '✔' : ''?></td>
            <td class="text-center" style="vertical-align: middle"><?=$v['saat_rawat'] == 'n' ? '✔' : ''?></td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>

<script>
    $(function () {
        print()
    })
</script>
</body>
</html>