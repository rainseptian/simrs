<?php
$currency_symbol = 'Rp';
?>
<link rel="stylesheet" href="<?php echo base_url() ?>assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
<link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/autocomplete/style-gue.css">
<style type="text/css">
    @import('https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.0/css/bootstrap.min.css');
</style>

<style type="text/css">
    .table-responsive {
        overflow-x: inherit;
    }
</style>
<div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title titlefix"> Rawat Inap Resume Medis</h3>
                    </div><!-- /.box-header -->
                    <?php $warning = $this->session->flashdata('warning');
                    if (!empty($warning)) { ?>
                        <div class="alert alert-warning alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <h4><i class="icon fa fa-warning"></i> Alert!</h4>
                            <?php echo $warning ?>
                        </div>
                    <?php } ?>
                    <?php $success = $this->session->flashdata('success');
                    if (!empty($success)) { ?>
                        <div class="alert alert-success alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <h4><i class="icon fa fa-check"></i> Success!</h4>
                            <?php echo $success ?>
                        </div>
                    <?php } ?>
                    <div class="box-body">
                        <table class="table table-striped table-bordered table-hover data-table" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Tanggal Masuk</th>
                                    <th>Dokter</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $no = 1;
                                $index = 0;
                                foreach ($resumeMedises as $r) : ?>
                                    <tr>
                                        <td><?= $no++ ?></td>
                                        <td><?= date('d-F-Y H:i', strtotime($r['tgl_masuk'])) ?></td>
                                        <td><?= $r['dokter'] ?></td>
                                        <td>
                                            <div class="btn-group" role="group">
                                                <button id="btnGroupDrop1" type="button" class="btn btn-sm btn-success dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    <i class="fa fa-bars"></i>
                                                </button>
                                                <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                                                    <a href="<?= base_url(); ?>RawatInap/resume_medis/<?= $rawatInap->id . '/' . $index ; ?>" style="margin-bottom: 5px;" class="btn-block">
                                                        <button type="button" class="btn btn-sm btn-block btn-primary"><i class="fa fa-pencil"></i> Detail</button>
                                                    </a>
                                                    <a href="<?= base_url(); ?>RawatInap/print_form/resume_medis/<?= $rawatInap->id . '/' . $index ?>" style="margin-bottom: 5px;" class="btn-block">
                                                        <button type="button" class="btn btn-sm btn-block btn-success"><i class="fa fa-print"></i> Cetak</button>
                                                    </a>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                <?php 
                                $index++;
                                endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<script type="text/javascript">
    $(function() {
        $('.data-table').DataTable()
    })
</script>