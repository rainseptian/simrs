<div class="modal fade" id="tambah_ppt" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-mid modal-lg" role="document">
        <div class="modal-content modal-media-content">
            <div class="modal-header modal-media-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="box-title">Tambah PPT</h4>
            </div>

            <form method="post" action="<?php echo base_url() ?>RawatInap/save_form/ppt/ppt">
                <input type="hidden" name="rawat_inap_id" value="<?=$rawat_inap->id?>">
                <input type="hidden" name="pasien_id" value="<?=$pasien->id?>">
                <div class="modal-body pt0 pb0">
                    <div class="ptt10">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="tujuan" class="control-label" style="padding-left: 0 !important;">Tgl/Jam</label>
                                    <input type="datetime-local" name="form[ppt][tgl]" class="form-control" required value="<?=date('Y-m-d H:i')?>">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="tujuan" class="control-label" style="padding-left: 0 !important;">Profesional Pemberi Asuhan (PPA)</label>
                                    <input type="text" name="form[ppt][ppa]" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="tujuan" class="control-label" style="padding-left: 0 !important;">CATATAN PERKEMBANGAN</label>
                                    <input type="text" name="form[ppt][catatan]" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="tujuan" class="control-label" style="padding-left: 0 !important;">INTRUKSI PPA</label>
                                    <input type="text" name="form[ppt][intruksi]" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="tujuan" class="control-label" style="padding-left: 0 !important;">VERIFIKASI DPJP</label>
                                    <input type="text" name="form[ppt][verifikasi]" class="form-control">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    <button type="submit" id="form_diagnosisbtn"
                            data-loading-text="Menyimpan..."
                            class="btn btn-info pull-right">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>