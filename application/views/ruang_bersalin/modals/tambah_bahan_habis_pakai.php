<div class="modal fade" id="tambah_bahan_habis_pakai" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-mid" role="document">
        <div class="modal-content modal-media-content">
            <div class="modal-header modal-media-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="box-title">Tambah Bahan Habis Pakai</h4>
            </div>

            <form id="form_diagnosis" method="post" action="<?php echo base_url() ?>RawatInap/tambah/bahan_habis_pakai">
                <div class="modal-body pt0 pb0">
                    <input type="hidden" name="rawat_inap_id" value="<?=$rawat_inap->id?>">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-sm-8">
                                    <select id="bahan-options" class="form-control">
                                        <option value="">-- Pilih Bahan --</option>
                                        <?php foreach ($bahan as $key => $value) { ?>
                                            <option value="<?= $value->id ?>"><?= $value->nama ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="col-sm-4">
                                    <button type="button" name="button"
                                            class="btn btn-sm btn-block btn-primary"
                                            id="button-add-bahans"><i class="fa fa-plus"></i>
                                        Tambah Bahan
                                    </button>
                                    <input type="hidden" id="abdush-counter2" value="0">
                                </div>
                            </div>
                            <div class="form-area" style="margin-top:15px;">
                                <table class="table table-striped table-hover">
                                    <thead>
                                    <tr>
                                        <th>Nama Bahan</th>
                                        <th>Stok</th>
                                        <th>Jml</th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    <button type="submit" id="form_diagnosisbtn"
                            data-loading-text="Menyimpan..."
                            class="btn btn-info pull-right">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>