<div class="modal fade" id="tambah_makanan" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-mid" role="document">
        <div class="modal-content modal-media-content">
            <div class="modal-header modal-media-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="box-title">Tambah Makanan</h4>
            </div>

            <form method="post" action="<?php echo base_url() ?>RawatInap/tambah/makanan">
                <div class="modal-body pt0 pb0">
                    <div class="ptt10">
                        <div class="row">
                            <div class="col-md-12">
                                <input type="hidden" name="rawat_inap_id" value="<?=$rawat_inap->id?>">
                                <input type="hidden" name="pasien_id" value="<?=$pasien->id?>">

                                <div class="row">
                                    <div class="col-md-12">
                                        <input type="hidden" name="harga_makanan" id="harga_makanan_">
                                        <div class="form-group">
                                            <label for="tujuan" class="col-sm-3 control-label">Tipe Makanan</label>
                                            <div class="col-sm-9 margin-bottom">
                                                <select class="form-control abdush-select margin-bottom" name="tipe_makanan" onchange="getMakananByTipeId_(this.value)">
                                                    <option value="">--Pilih Tipe Makanan--</option>
                                                    <?php foreach ($tipe_makanan as $key => $value) { ?>
                                                        <option value="<?php echo $value->id ?>"><?php echo $value->nama ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="tujuan" class="col-sm-3 control-label margin-bottom">Nama Makanan</label>
                                            <div class="col-sm-9 margin-bottom">
                                                <select class="form-control abdush-select" name="makanan" id="makanan_">

                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    <button type="submit" id="form_diagnosisbtn"
                            data-loading-text="Menyimpan..."
                            class="btn btn-info pull-right">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>