<div class="modal fade" id="tambah_diagnosis" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-mid" role="document">
        <div class="modal-content modal-media-content">
            <div class="modal-header modal-media-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="box-title">Tambah Diagnosis</h4>
                <a href="#" class="btn btn-sm btn-primary dropdown-toggle" style="position: absolute; right: 40px; top: 20px;"
                   onclick="holdModal('tambah_penyakit')" data-toggle="modal"><i class="fas fa-plus"></i> Tambah Penyakit
                </a>
            </div>

            <form id="form_diagnosis" method="post" action="<?php echo base_url() ?>RawatInap/tambah/diagnosis">
                <div class="modal-body pt0 pb0">
                    <div class="ptt10">
                        <div class="row">
                            <div class="col-md-12">
                                <input type="hidden" name="rawat_inap_id" value="<?=$rawat_inap->id?>">
                                <input type="hidden" name="pasien_id" value="<?=$pasien->id?>">
                                <div class="form-group margin-bottom">
                                    <label for="tujuan" class="col-sm-3 control-label">Penyakit</label>
                                    <div class="col-sm-9">
                                        <select class="form-control select2 margin-bottom" multiple="multiple" name="penyakit[]" required data-placeholder="Pilih penyakit">
                                            <?php foreach ($penyakit as $key => $value) { ?>
                                                <option value="<?php echo $value->id ?>"><?php echo $value->kode.' - '.$value->nama ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="diagnosis" class="col-sm-3 control-label">Diagnosis</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" id="diagnosis" name="diagnosis" placeholder="Diagnosis">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    <button type="submit" id="form_diagnosisbtn"
                            data-loading-text="Menyimpan..."
                            class="btn btn-info pull-right">Simpan</button>
                </div>
            </form>


        </div>
    </div>
</div>