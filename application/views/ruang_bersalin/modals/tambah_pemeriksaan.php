<div class="modal fade" id="tambah_pemeriksaan" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-mid" role="document">
        <div class="modal-content modal-media-content">
            <div class="modal-header modal-media-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="box-title">Tambah Pemeriksaan</h4>
            </div>

            <form id="form_diagnosis" method="post" action="<?php echo base_url() ?>RawatInap/tambah/pemeriksaan">
                <div class="modal-body pt0 pb0">
                    <div class="ptt10">
                        <div class="row">
                            <div class="col-md-12">
                                <input type="hidden" name="rawat_inap_id" value="<?=$rawat_inap->id?>">
                                <input type="hidden" name="pasien_id" value="<?=$pasien->id?>">
                                <div class="form-group">
                                    <label for="tanggal_pemeriksaan" class="col-sm-3 control-label">Tgl Pemeriksaan</label>
                                    <div class="col-sm-9">
                                        <input type="date" class="form-control margin-bottom" id="tanggal_pemeriksaan" name="tanggal_pemeriksaan" value="<?=date('Y-m-d')?>" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="shift" class="col-sm-3 control-label">Shift</label>
                                    <div class="col-sm-9 margin-bottom">
                                        <select class="form-control abdush-select margin-bottom" name="shift" required>
                                            <option value="">-- Pilih Shift --</option>
                                            <option value="I">I</option>
                                            <option value="II">II</option>
                                            <option value="III">III</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="tujuan" class="col-sm-3 control-label">Dokter</label>
                                    <div class="col-sm-9 margin-bottom">
                                        <select class="form-control abdush-select margin-bottom" name="dokter_id" required>
                                            <option value="" selected>-- Pilih Dokter --</option>
                                            <?php foreach ($dokter->result() as $key => $value) { ?>
                                                <option value="<?php echo $value->id ?>"><?php echo $value->nama; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="subjektif" class="col-sm-3 control-label">Subjektif</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control margin-bottom" id="subjektif" name="subjektif" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="subjektif" class="col-sm-3 control-label">Objektif</label>
                                    <div class="col-md-12">
                                        <div class="col-sm-6 col-md-6 col-lg-6 form-group">
                                            <label for="td" class="col-sm-4 control-label">TD</label>
                                            <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">
                                                <input type="text" class="form-control requirable" name="objektif[td]" id="td">
                                                <span class="input-group-addon">mmHg</span>
                                            </div>
                                        </div>
                                        <div class="col-sm-6 col-md-6 col-lg-6 form-group">
                                            <label for="inputEmail3" class="col-sm-4 control-label">R</label>
                                            <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">
                                                <input type="text" class="form-control requirable" name="objektif[r]" id="r">
                                                <span class="input-group-addon">K/Min</span>
                                            </div>
                                        </div>
                                        <div class="col-sm-6 col-md-6 col-lg-6 form-group">
                                            <label for="inputEmail3" class="col-sm-4 control-label">BB</label>
                                            <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">
                                                <input type="text" class="form-control requirable" id="bb" name="objektif[bb]"
                                                       onkeyup="set_bmi()">
                                                <span class="input-group-addon">Kg</span>
                                            </div>
                                        </div>
                                        <div class="col-sm-6 col-md-6 col-lg-6 form-group">
                                            <label for="inputEmail3" class="col-sm-4 control-label">N</label>
                                            <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">
                                                <input type="text" class="form-control requirable" name="n" id="objektif[n]">
                                                <span class="input-group-addon">K/Min</span>
                                            </div>
                                        </div>
                                        <div class="col-sm-6 col-md-6 col-lg-6 form-group">
                                            <label for="inputEmail3" class="col-sm-4 control-label">S</label>
                                            <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">
                                                <input type="text" class="form-control requirable" id="s" name="objektif[s]">
                                                <span class="input-group-addon">'0</span>
                                            </div>
                                        </div>
                                        <div class="col-sm-6 col-md-6 col-lg-6 form-group">
                                            <label for="inputEmail3" class="col-sm-4 control-label">TB</label>
                                            <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">
                                                <input type="text" class="form-control requirable" id="tb" name="objektif[tb]"
                                                       onkeyup="set_bmi()">
                                                <span class="input-group-addon">cm</span>
                                            </div>
                                        </div>
                                        <div class="col-sm-6 col-md-6 col-lg-6 form-group">
                                            <label for="inputEmail3" class="col-sm-4 control-label">BMI</label>
                                            <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">
                                                <input type="text" class="form-control requirable" name="objektif[bmi]" id="bmi">

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="asesmen" class="col-sm-3 control-label">Asesmen</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control margin-bottom" id="asesmen" name="asesmen" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="plan" class="col-sm-3 control-label">Plan</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control margin-bottom" id="plan" name="plan" required>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    <button type="submit" id="form_diagnosisbtn"
                            data-loading-text="Menyimpan..."
                            class="btn btn-info pull-right">Simpan</button>
                </div>
            </form>


        </div>
    </div>
</div>