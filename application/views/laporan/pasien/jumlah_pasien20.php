<link rel="stylesheet"
      href="<?php echo base_url() ?>assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Laporan Pasien Top 20
            <small></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a href="#">Laporan Jumlah Pasien Top 20 </a></li>
            <li class="active"> Pasien Top 20</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title"><i class='fa fa-file-text-o fa-fw'></i> Laporan Jumlah Pasien Top 20 </h3>&nbsp;&nbsp;
                    </div>
                    <div class="box-body">
                        <form class="form-horizontal" method="get" action="<?php echo base_url() ?>Laporan/jumlahPasien20">
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="form-horizontal">
                                        <div class="form-group">
                                            <label class="col-sm-4 control-label">Dari Tanggal</label>
                                            <div class="col-sm-8">
                                                <input type='date' name='from' class='form-control' id='tanggal_dari'
                                                       value="<?php echo ($this->input->get('from')) ? $this->input->get('from') : date('Y-m-d') ?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-horizontal">
                                        <div class="form-group">
                                            <label class="col-sm-4 control-label">Sampai Tanggal</label>
                                            <div class="col-sm-8">
                                                <input type='date' name='to' class='form-control' id='tanggal_sampai'
                                                       value="<?php echo ($this->input->get('to')) ? $this->input->get('to') : date('Y-m-d') ?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-horizontal">
                                        <div class="form-group">
                                            <label class="col-sm-4 control-label">Jenis Pendaftaran</label>
                                            <div class="col-sm-8">
                                                <select class="form-control" id="jenis_pendaftaran"
                                                        name="jenis_pendaftaran">
                                                    <option value="">-- Jenis Pendaftaran --</option>
                                                    <?php

                                                    foreach ($listjenispendaftaran->result() as $key => $value) {
                                                        $pilih = ($value->id == $this->input->get('jenis_pendaftaran')) ? "selected" : "";
                                                        ?>

                                                        <option value="<?php echo $value->id ?>" <?php echo $pilih; ?>><?php echo ucwords($value->nama) ?></option>


                                                    <?php } ?>

                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class='row'>
                                <div class="col-sm-12">
                                    <div style="display: flex">
                                        <div style="flex: 1"></div>
                                        <button type="submit" class="btn btn-primary" style='margin-left: 0px;'>
                                            Tampilkan
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <div class="row">
                            <div class="col-sm-12">
                                <div style="display: flex; align-items: center">
                                    <h3 class="box-title">Data Laporan Pasien Top 20</h3>
                                    <div style="flex: 1"></div>
                                    <form class="form-horizontal align-right" method="post"
                                          action="<?php echo base_url() ?>export/laporan/pasien/jumlahPasienTop20Bottom">
                                        <input type="hidden" name="from"
                                               value="<?php echo ($this->input->get('from')) ? $this->input->get('from') : '' ?>">
                                        <input type="hidden" name="to"
                                               value="<?php echo ($this->input->get('to')) ? $this->input->get('to') : '' ?>">
                                        <input type="hidden" name="jenis_pendaftaran"
                                               value="<?php echo ($this->input->get('jenis_pendaftaran')) ? $this->input->get('jenis_pendaftaran') : '' ?>">
                                        <button type="submit" class="btn btn-primary align-self-end"><i
                                                    class="fa fa-print"></i> Export Excel
                                        </button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="box-body">
                        <table id="example2" class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>No</th>
                                <th>Nama Pasien</th>
                                <th>Jenis Pendaftaran</th>
                                <th>No RM</th>
                                <th>Usia</th>
                                <th>Jenis Kelamin</th>
                                <th>Alamat</th>
                                <th>Total Kunjungan</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $no = 1;
                            if ($list_jumlah_pasien20) {
                                foreach ($list_jumlah_pasien20 as $row) { ?>
                                    <tr>
                                        <td> <?php echo $no; ?></td>
                                        <td> <?php echo ucwords($row->nama); ?></td>
                                        <td>
                                            <?php echo ucwords($row->poli); ?>
                                            <br>
                                            <small>
                                                <span class="label <?= $jaminan[$row->jaminan]['class'] ?>"><?= $jaminan[$row->jaminan]['label'] ?></span>
                                                <?php if (!isset($jaminan[$row->jaminan])) { ?>
                                                    <span class="label label-warning">Umum</span>
                                                <?php } ?>
                                            </small>
                                        </td>
                                        <td> <?php echo ucwords($row->no_rm); ?></td>
                                        <td> <?php echo ucwords($row->usia); ?></td>
                                        <td> <?php echo $row->jk == 'L' ? 'Laki-Laki' : 'Perempuan'; ?></td>
                                        <td> <?php echo ucwords($row->alamat); ?></td>
                                        <td> <?php echo ucwords($row->total); ?></td>
                                    </tr>
                                    <?php $no++;
                                }
                            } else {
                                ?>
                                <tr>
                                    <td bgcolor="lightblue" colspan="3"> Data Tidak ada</td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<script>
    $(function () {
        $('#example2').DataTable()
    })
</script>
<script src="<?php echo base_url() ?>assets/bower_components/Flot/jquery.flot.js"></script>
<script src="<?php echo base_url() ?>assets/bower_components/Flot/jquery.flot.categories.js"></script>
