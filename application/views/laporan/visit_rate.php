<link rel="stylesheet"
      href="<?php echo base_url() ?>assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
<link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/autocomplete/style-gue.css">
<style type="text/css">
    @import(

    'https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.0/css/bootstrap.min.css'
    )
    ;
</style>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Laporan Visit Rate Pasien
            <small></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a href="#">Laporan Visit Rate Pasien</a></li>
            <li class="active"> Pasien</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-success">
                    <div class="box-header">
                        <h3 class="box-title"><i class='fa fa-table fa-fw'></i> Data Visit Rate Pasien</h3>&nbsp;&nbsp;
                    </div>
                    <div class="box-header">
                        <div class="panel panel-success">
                            <div class="panel-heading"><h4><b>INFO</b></h4></div>
                            <div class="panel-body">
                                <b>Keterangan Task ID</b>
                                <ul>
                                    <li>Task ID 1: mulai waktu tunggu admisi</li>
                                    <li>Task ID 2: akhir waktu tunggu admisi/mulai waktu layan admisi</li>
                                    <li>Task ID 3: akhir waktu layan admisi/mulai waktu tunggu poli</li>
                                    <li>Task ID 4: akhir waktu tunggu poli/mulai waktu layan poli</li>
                                    <li>Task ID 5: akhir waktu layan poli/mulai waktu tunggu farmasi</li>
                                    <li>Task ID 6: akhir waktu tunggu farmasi/mulai waktu layan farmasi membuat obat</li>
                                    <li>Task ID 7: akhir waktu obat selesai dibuat</li>
                                </ul>
                                <b>Catatan (BPJS): </b>
                                <ul>
                                    <li>Alur Task Id Pasien Baru: 1-2-3-4-5 (apabila ada obat tambah 6-7)</li>
                                    <li>Alur Task Id Pasien Lama: 3-4-5 (apabila ada obat tambah 6-7)</li>
                                    <li>Sisa antrean berkurang pada task 5</li>
                                    <li>Pemanggilan antrean poli pasien muncul pada task 4</li>
                                    <li>Cek in/mulai waktu tunggu untuk pasien baru mulai pada task 1</li>
                                    <li>Cek in/mulai waktu tunggu untuk pasien lama mulai pada task 3</li>
                                </ul>
                                <b>Catatan (SIM Klnik): </b>
                                <ul>
                                    <li>Untuk pasien daftar onsite (Tipe Onsite), Task ID 3 diisi ketika petugas simpan pendaftaran</li>
                                    <li>Untuk pasien kontrol (Tipe Kontrol), Task ID 3 diisi ketika petugas klik tombol checkin di halaman List Kontrol</li>
                                    <li>Untuk pasien daftar online melalui MJKN (Tipe MJKN), Task ID 3 diisi ketika pasien melakukan checkin dari aplikasi MJKN</li>
                                    <li>Untuk pasien daftar online melalui <a href="https://sim.sukmawijaya.com/DaftarOnline" target="_blank">https://sim.sukmawijaya.com/DaftarOnline</a> (Tipe Online), Task ID 3 diisi ketika user berhasil daftar (saat ini belum ada menu checkin untuk pasien tipe ini)</li>
                                    <li>Kolom "Tipe Daftar" berisi: Onsite, Online, Kontrol, dan MJKN</li>
                                    <li>Untuk pendaftaran sebelum tgl 15 Nov 2023, tdk diketahui apakah Onsite atau Online, tetapi diketahui apakah Kontrol dan MJKN</li>
                                    <li>Kolom "Tunggu s/d Layan Poli": Waktu yang dibutuhkan untuk Task ID 3 sampai Task ID 5</li>
                                    <li>Kolom "Tunggu s/d Layan Poli" berwarna merah ketika 1) waktu Tunggu s/d Layan Poli lebih dari atau sama dengan 5 jam, dan 2) ketika ada task id 3 atau task id 5 yang tidak terisi</li>
                                    <li>Kolom "Add Antrian" berisi informasi apakah pengiriman data antrian (kunjungan) ke BPJS sukses atau gagal. Jika pengiriman data antrian gagal, klik text gagal untuk melihat pesan gagal dari BPJS</li>
                                    <li>Alur pengiriman task adalah sebagai berikut: sebelum kirim task, kirim antrian dulu. Jika pengiriman antrian gagal, maka semua pengiriman task akan gagal juga</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="box-body">
                        <form method="get">

                            <div class="row">
                                <div class="col-sm-4">
                                    <div style="margin: 5px 10px">
                                        <label class="control-label">Waktu</label>
                                        <select class="form-control" id="jenis" name="jenis">
                                            <option value="1" <?php if (isset($jenis) && $jenis == "1") echo 'selected'?>>Hari Ini</option>
                                            <option value="2" <?php if (isset($jenis) && $jenis == "2") echo 'selected'?>>Pilih Tanggal</option>
                                            <option value="3" <?php if (isset($jenis) && $jenis == "3") echo 'selected'?>>Pilih Bulan dan Tahun</option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="row" id="jenis-2" style="display: none">
                                <div class="col-sm-4">
                                    <div style="margin: 5px 10px">
                                        <label class="control-label">Dari Tanggal</label>
                                        <input type='date' name='from' class='form-control' id='tanggal_dari' value="<?php echo ($this->input->get('from')) ? $this->input->get('from') : date('Y-m-01') ?>">
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div style="margin: 5px 10px">
                                        <label class="control-label">Sampai Tanggal</label>
                                        <input type='date' name='to' class='form-control' id='tanggal_sampai' value="<?php echo ($this->input->get('to')) ? $this->input->get('to') : date('Y-m-d') ?>">
                                    </div>
                                </div>
                            </div>

                            <div class="row" id="jenis-3" style="display: none">
                                <div class="col-sm-4">
                                    <div style="margin: 5px 10px">
                                        <label class="control-label">Bulan</label>
                                        <select class="form-control" id="bulan" name="bulan">
                                            <option value="01" <?php if (isset($bulan) && $bulan == "01") echo 'selected'?>>Januari</option>
                                            <option value="02" <?php if (isset($bulan) && $bulan == "02") echo 'selected'?>>Februari</option>
                                            <option value="03" <?php if (isset($bulan) && $bulan == "03") echo 'selected'?>>Maret</option>
                                            <option value="04" <?php if (isset($bulan) && $bulan == "04") echo 'selected'?>>April</option>
                                            <option value="05" <?php if (isset($bulan) && $bulan == "05") echo 'selected'?>>Mei</option>
                                            <option value="06" <?php if (isset($bulan) && $bulan == "06") echo 'selected'?>>Juni</option>
                                            <option value="07" <?php if (isset($bulan) && $bulan == "07") echo 'selected'?>>Juli</option>
                                            <option value="08" <?php if (isset($bulan) && $bulan == "08") echo 'selected'?>>Agustus</option>
                                            <option value="09" <?php if (isset($bulan) && $bulan == "09") echo 'selected'?>>September</option>
                                            <option value="10" <?php if (isset($bulan) && $bulan == "10") echo 'selected'?>>Oktober</option>
                                            <option value="11" <?php if (isset($bulan) && $bulan == "11") echo 'selected'?>>November</option>
                                            <option value="12" <?php if (isset($bulan) && $bulan == "12") echo 'selected'?>>Desember</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div style="margin: 5px 10px">
                                        <label class="control-label">Tahun</label>
                                        <select class="form-control" id="tahun" name="tahun">
                                            <?php $year = intval(date('Y')); for ($a = $year; $a > $year - 5; $a--) { ?>
                                                <option value="<?php echo $a?>" <?php if (isset($bulan) && $bulan == $a) echo 'selected'?>><?php echo $a?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class='row'>
                                <div class="col-sm-3">
                                    <div style="margin: 0 10px">
                                        <div class="form-group">
                                            <div class="col-sm-7">
                                                <button type="submit" class="btn btn-primary" style='margin-left: 0px;'>Tampilkan</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </form>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="table-responsive">
                                    <table id="example2" class="table table-bordered table-hover">
                                        <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Poli</th>
                                            <th>Nama Pasien</th>
                                            <th>Tgl Periksa</th>
                                            <th>Kode Booking</th>
                                            <th>Tipe Daftar</th>
                                            <th>Add Antrian</th>
                                            <th>Task ID 1</th>
                                            <th>Task ID 2</th>
                                            <th>Task ID 3</th>
                                            <th>Task ID 4</th>
                                            <th>Task ID 5</th>
                                            <th>Task ID 6</th>
                                            <th>Task ID 7</th>
                                            <th>Tunggu s/d Layan Poli</th>
                                            <th>Aksi</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php $no = 1;
                                        foreach ($list as $row) { ?>
                                            <tr>
                                                <td> <?php echo $no++; ?></td>
                                                <td> <?php echo $row->poli; ?></td>
                                                <td>
                                                    <?php echo ucwords($row->nama); ?>
                                                    <br>
                                                    <small>
                                                        <span class="label <?= $jaminan[$row->jaminan]['class'] ?>"><?= $jaminan[$row->jaminan]['label'] ?></span>
                                                        <?php if (!isset($jaminan[$row->jaminan])) { ?>
                                                            <span class="label label-warning">Umum</span>
                                                        <?php } ?>
                                                    </small>
                                                </td>
                                                <td> <?php echo date('d-M-Y', strtotime($row->waktu_pemeriksaan)); ?></td>
                                                <td>
                                                    <button class="btn btn-info btn-sm" onclick="detail('<?php echo $row->kode_booking; ?>')">
                                                        <?php echo $row->kode_booking; ?>
                                                    </button>
                                                </td>
                                                <td>
                                                    <?php if ($row->penanggungjawab == 'Rencana Kontrol') : ?>
                                                        Kontrol
                                                    <?php elseif ($row->penanggungjawab == 'JKN Mobile') : ?>
                                                        MJKN
                                                    <?php else : ?>
                                                        <?php if ($row->tipe_daftar == 'online') : ?>
                                                            Online
                                                        <?php elseif ($row->tipe_daftar == 'onsite') : ?>
                                                            Onsite
                                                        <?php else : ?>
                                                            Onsite/Online
                                                        <?php endif; ?>
                                                    <?php endif; ?>
                                                </td>
                                                <td>
                                                    <?php if ($row->add_antrian) : ?>
                                                        <label class="label label-success">Sukses</label>
                                                    <?php else : ?>
                                                        <label class="label label-danger"
                                                               style="cursor: pointer"
                                                               onclick='pesan(<?=json_encode($row->add_antrian_response)?>, <?=json_encode($row->add_antrian_request)?>)'
                                                        >
                                                            Gagal
                                                        </label>
                                                        <br>
                                                        <b>Psn gagal:</b> <?=json_decode($row->add_antrian_response)->server_output->metadata->message ?? '-'?>
                                                        <br>
                                                        <b>Jns kunj:</b>
                                                        <?php $jns = json_decode($row->add_antrian_request)->jeniskunjungan ?>
                                                        <?php if ($jns == 1) : ?>
                                                            Rujukan FKTP
                                                        <?php elseif ($jns == 2) : ?>
                                                            Rujukan Internal
                                                        <?php elseif ($jns == 3) : ?>
                                                            Kontrol
                                                        <?php elseif ($jns == 4) : ?>
                                                            Rujukan Antar RS
                                                        <?php else : ?>
                                                            -
                                                        <?php endif; ?>
                                                    <?php endif; ?>
                                                </td>
                                                <td><?=date('H:i:s', substr($row->task_1, 0, 10))?></td>
                                                <td><?=date('H:i:s', substr($row->task_2, 0, 10))?></td>
                                                <td><?=date('H:i:s', substr($row->task_3, 0, 10))?></td>
                                                <td><?=date('H:i:s', substr($row->task_4, 0, 10))?></td>
                                                <td><?=date('H:i:s', substr($row->task_5, 0, 10))?></td>
                                                <td><?=date('H:i:s', substr($row->task_6, 0, 10))?></td>
                                                <td><?=date('H:i:s', substr($row->task_7, 0, 10))?></td>
                                                <td>
                                                    <?php
                                                    $t = (($row->task_5 / 1000) - ($row->task_3 / 1000));
                                                    $h = $t/3600;
                                                    $red = $h >= 5 || $h < 0;
                                                    ?>
                                                    <span <?php if ($red) : ?>style="color: red; font-weight: bold"<?php endif; ?>>
                                                        <?= sprintf('%02d:%02d:%02d', ($t/3600),($t/60%60), $t%60) ?>
                                                    </span>
                                                </td>
                                                <td>
                                                    <?php if ($row->penanggungjawab == 'Rencana Kontrol') : ?>
                                                        <button class="btn btn-success btn-sm" onclick="resend(<?=$row->pendaftaran_id?>)">Kirim Ulang</button>
                                                    <?php endif; ?>
                                                </td>
                                            </tr>
                                        <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<div class="modal fade" id="detail-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header" style="display: flex">
                <h4 class="modal-title" id="choose-title" style="flex: 1">Detail</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <h5><b>Log</b></h5>
                <pre id="log"></pre>
                <h5><b>Antrian</b></h5>
                <pre id="ant"></pre>
                <h5><b>Pendaftaran</b></h5>
                <pre id="pdf"></pre>
                <h5><b>Pemeriksaan</b></h5>
                <pre id="pem"></pre>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="error-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header" style="display: flex">
                <h4 class="modal-title" id="choose-title" style="flex: 1">Pesan Gagal</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <pre id="pesan"></pre>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="resend-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header" style="display: flex">
                <h4 class="modal-title" id="resend-title" style="flex: 1">Kirim Ulang</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <label>Poli</label>
                <p id="resend-poli"></p>
                <label>Nama Pasien</label>
                <p id="resend-nama"></p>
                <label>Tgl Periksa</label>
                <p id="resend-tgl"></p>
                <div class="form-group">
                    <label>No BPJS</label>
                    <input type="text" class="form-control" id="resend-no-bpjs">
                </div>
                <div class="form-group">
                    <label>No Surat Kontrol</label>
                    <input type="text" class="form-control" id="resend-no-kontrol">
                </div>
            </div>
        </div>
    </div>
</div>

<script>

    $(function () {
        $('#example2').DataTable({
            'paging': true,
            'lengthChange': true,
            'searching': true,
            'ordering': true,
            'info': true,
            'autoWidth': true
        })

        const jenis = <?=isset($jenis) ? $jenis : -100?>;
        if (jenis === 2) {
            $('#jenis-2').css('display', 'block')
        }
        else if (jenis === 3) {
            $('#jenis-3').css('display', 'block')
        }

        $("#jenis").change(function () {

            $('#jenis-2').css('display', 'none')
            $('#jenis-3').css('display', 'none')

            if ($(this).val() === '2') {
                $('#jenis-2').css('display', 'block')
            }
            else if ($(this).val() === '3') {
                $('#jenis-3').css('display', 'block')
            }
        })

    })

    const detail = (kode_booking) => {
        $('#detail-modal').modal('show')
        $('#choose-title').html(`Detail ${kode_booking}`)

        $.ajax({
            url: "<?php echo site_url('Laporan/detail_kode_booking'); ?>/" + kode_booking,
            type: "GET",
            cache: false,
            dataType: 'json',
            success: function (json) {
                $('#log').html(json.log.map(v => JSON.stringify({url: v.url, task_id: v.task_id, response: v.response})).join('<br>'))
                $('#ant').html(JSON.stringify(json.antrian, undefined, 2))
                $('#pdf').html(JSON.stringify(json.pendaftaran, undefined, 2))
                $('#pem').html(JSON.stringify(json.pemeriksaan, undefined, 2))
            }
        });
    }

    const pesan = (response, request) => {
        //onclick="pesan('<?php //=json_decode($row->add_antrian_response)->server_output->metadata->message?>//')"
        $('#error-modal').modal('show')
        $('#pesan').html(`${JSON.stringify(JSON.parse(response), undefined, 2)} <br><br><br> ${JSON.stringify(JSON.parse(request), undefined, 2)}`)
    }

    const list = <?=json_encode($list)?>;
    const resend = pendaftaran_id => {
        $('#resend-modal').modal('show')

        const x = list.find(v => +v.pendaftaran_id === +pendaftaran_id)
        if (x) {
            $('#resend-poli').html(x.poli)
            $('#resend-nama').html(x.nama)
            $('#resend-tgl').html(x.waktu_pemeriksaan)
        }
    }

</script>

<script>

</script>

<script src="<?php echo base_url() ?>assets/bower_components/Flot/jquery.flot.js"></script>
<script src="<?php echo base_url() ?>assets/bower_components/Flot/jquery.flot.categories.js"></script>
