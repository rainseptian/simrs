<div class="tab-pane" id="tab_rawat_inap">
    <table class="table table-hover table-bordered example2" style="margin-bottom: 0">
        <thead>
            <tr>
                <th>No</th>
                <th>Tgl Daftar</th>
                <th>Tgl Boleh Pulang</th>
                <th>Dari Poli</th>
                <th>Bed</th>
                <th>Aksi</th>
            </tr>
        </thead>
        <tbody>
            <?php
            $no = 1;
            foreach ($list_rawat_inap as $r) { ?>
                <tr>
                    <td><?= $no++ ?></td>
                    <td><?= date('d-F-Y H:i', strtotime($r['created_at'])) ?></td>
                    <td><?= date('d-F-Y H:i', strtotime($r['tgl_boleh_pulang'])) ?></td>
                    <td><?= $r['nama_poli'] ?></td>
                    <td><?= $r['bed_name'] . ' - ' . $r['bedgroup'] ?></td>
                    <!-- <td>
                    <a href="<?php echo base_url(); ?>RawatInap/detailRekamMedis/<?php echo $r['id']; ?>" class="btn btn-success btn-block"><span class="fa fa-arrows"></span> Detail</a>
                </td> -->
                    <td>
                        <div class="btn-group" role="group">
                            <a href="<?php echo base_url(); ?>RawatInap/resume_medis_list/<?php echo $r['id']; ?>" class="btn btn-success btn-block"><span class="fa fa-arrows"></span> Resume Medis</a>
                            <a href="<?php echo base_url(); ?>RawatInap/pemeriksaan_list/<?php echo $r['id']; ?>" class="btn btn-success btn-block"><span class="fa fa-arrows"></span> CPPT </a>    
                        </div>
                    </td>
                </tr>
            <?php $no++;
            } ?>
        </tbody>
    </table>
</div>