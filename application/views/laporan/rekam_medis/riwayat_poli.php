<div class="tab-pane active" id="tab_1">
    <div class="table-responsive">
        <table class="table table-striped table-bordered example2">
            <thead>
            <tr>
                <th>No</th>
                <th>Tanggal Periksa</th>
                <th>Nama Poli</th>
                <th>Nama Dokter</th>
                <th>Anamnesis</th>
                <th>Tindakan</th>
                <th>Diagnosis ICD-10</th>
                <th>Diagnosis</th>
                <th>Tata Laksana</th>
                <th>Resep Obat</th>
                <th>Obat Racik</th>
                <th>Aksi</th>
            </tr>
            </thead>
            <tbody>
            <?php
            $no = 1;
            foreach ($poli as $the_i => $row) { ?>
                <tr>
                    <td> <?= $no; ?></td>
                    <td> <?= date('d-F-Y', strtotime($row->waktu_pemeriksaan)); ?></td>
                    <td><?= $row->nama_jenis_pendaftaran ?></td>
                    <td><?= $row->nama_dokter ?></td>
                    <td><?= $row->amammesia ?></td>
                    <td>
                        <table class="bor" style="font-size: 12px !important;">
                            <?php foreach ($row->tindakan as $tindakan) : ?>
                                <?php if (strtolower($tindakan->nama) != 'administrasi') : ?>
                                    <tr>
                                        <td><?=$tindakan->nama?></td>
                                    </tr>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        </table>
                    </td>
                    <td>
                        <table class="bor" style="font-size: 12px !important;">
                            <tbody>
                            <?php
                            if ($penyakit) {
                                foreach ($penyakit->result() as $row1) {
                                    if ($row->id == $row1->pemeriksaan_id) {
                                        ?>
                                        <tr>
                                            <td style="padding: 0px 5px;"> <?= $row1->nama; ?></td>
                                            <td style="padding: 0px 5px;"> <?= $row1->kode; ?></td>
                                        </tr>
                                    <?php }
                                }
                            } else {
                                ?>
                                <tr>
                                    <td bgcolor="lightblue" colspan="3"> Data Tidak ada</td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </td>
                    <td><?= $row->diagnosis ?></td>
                    <td><?= $row->deskripsi_tindakan ?></td>
                    <td>
                        <table class="bor" style="font-size: 12px !important;">
                            <thead>
                            <tr>
                                <th class="bor">Nama</th>
                                <th class="bor">Signa</th>
                                <th class="bor">jumlah</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $o = $obat;
                            $o = array_map(function ($v) {
                                return $v->pemeriksaan_id;
                            }, $o->result());
                            if ($obat && in_array($row->id, $o)) {
                                foreach ($obat->result() as $row2) {
                                    if ($row->id == $row2->pemeriksaan_id) { ?>
                                        <tr>
                                            <td class="bor"> <?= $row2->nama; ?></td>
                                            <td class="bor"> <?= $row2->signa_obat; ?></td>
                                            <td class="bor"
                                                class="pull-right"> <?= $row2->jumlah_satuan; ?></td>
                                        </tr>
                                        <?php
                                    }
                                }
                            } else { ?>
                                <tr>
                                    <td bgcolor="#FEFFEF" colspan="3" class="text-center"> Data Tidak ada</td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>

                    </td>
                    <td>
                        <table class="bor" style="font-size: 12px !important;">
                            <thead>
                            <tr>
                                <th class="bor">Nama</th>
                                <th class="bor">Signa</th>
                                <th class="bor">Obat</th>
                                <th class="bor">Catatan</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $or = 0; ?>
                            <?php if (!empty($row->racikans)) : ?>
                                <?php foreach ($row->racikans as $row2) : ?>
                                    <?php if (count($row2->racikan)) : ?>
                                        <tr>
                                            <td class="bor" valign="top"> <?= $row2->nama_racikan; ?></td>
                                            <td class="bor" valign="top"> <?= $row2->signa; ?></td>
                                            <td class="bor">
                                                <table>
                                                    <tbody>
                                                    <?php foreach ($row2->racikan as $v) : ?>
                                                        <tr>
                                                            <td><?= $v->nama ?></td>
                                                            <td><?= $v->jumlah_satuan ?></td>
                                                        </tr>
                                                    <?php endforeach; ?>
                                                    </tbody>
                                                </table>
                                            </td>
                                            <td class="bor cat" valign="top"> <?= $row2->catatan; ?></td>
                                        </tr>
                                        <?php $or++; ?>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                            <?php endif; ?>
                            <?php if (!$or) : ?>
                                <tr>
                                    <td class="no-data text-center" bgcolor="#FEFFEF" colspan="4"> Data tidak ada</td>
                                </tr>
                            <?php endif; ?>
                            </tbody>
                        </table>
                    </td>
                    <td>
                        <div class="btn-group" role="group">
                            <button id="btnGroupDrop1" type="button" class="btn btn-sm btn-success dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="fa fa-bars"></i>
                            </button>
                            <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                                <a href="<?= base_url(); ?>Laporan/EditRekamMedis/<?= $row->id; ?>" style="margin-bottom: 5px;" class="btn-block">
                                    <button type="button" class="btn btn-sm btn-block btn-primary"><i class="fa fa-pencil"></i> Edit</button>
                                </a>
                                <a href="<?= base_url(); ?>Laporan/DetailRiwayatPoli/<?= $row->id; ?>" style="margin-bottom: 5px;" class="btn-block">
                                    <button type="button" class="btn btn-sm btn-block btn-success"><i class="fa fa-arrows"></i> Detail</button>
                                </a>
                                <a href="<?= base_url(); ?>Laporan/edit_resep_rekam_medis/<?= $row->id; ?>" style="margin-bottom: 5px;" class="btn-block">
                                    <button type="button" class="btn btn-sm btn-block btn-warning"><i class="fa fa-pencil-square"></i> Edit Resep</button>
                                </a>
                                <!--                    <a href="--><?php //echo base_url(); ?><!--Laporan/downloadPemeriksaan/--><?php //echo $row->id; ?><!--" style="margin-bottom: 5px;" class="btn-block">-->
                                <!--                        <button type="button" class="btn btn-sm btn-block btn-success"><i class="fa fa-download"></i> Unduh</button>-->
                                <!--                    </a>-->
                                <a href="<?= base_url(); ?>Laporan/printPemeriksaan/<?= $row->id; ?>" target="_blank" rel="noopener noreferrer">
                                    <button type="button" class="btn btn-sm btn-block btn-danger"><i class="fa fa-print"></i> Cetak</button>
                                </a>
                            </div>
                        </div>
                    </td>
                </tr>
                <?php $no++;
            } ?>
            </tbody>
        </table>
    </div>
</div>
