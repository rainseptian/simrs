<div class="tab-pane" id="tab_ruang_bersalin">
    <table class="table table-hover table-bordered example2" style="margin-bottom: 0">
        <thead>
        <tr>
            <th>No</th>
            <th>Tgl Daftar</th>
            <th>Tgl Selesai Bersalin</th>
            <th>Diagnosis Jenis Penyakit</th>
            <th>Aksi</th>
        </tr>
        </thead>
        <tbody>
        <?php
        $no = 1;
        foreach ($list_ruang_bersalin as $r) { ?>
            <tr>
                <td><?=$no++?></td>
                <td><?=date('d-F-Y H:i', strtotime($r['transfered_at']))?></td>
                <td><?=date('d-F-Y H:i', strtotime($r['diperiksa_at']))?></td>
                <td>
                    <table class="table table-condensed table-bordered" style="font-size: 11px; margin-bottom: 0">
                        <tr>
                            <th>Diagnosis</th>
                            <th>Penyakit</th>
                        </tr>
                        <?php foreach ($r['diagnosis'] as $d) : ?>
                            <tr>
                                <td><?=$d->diagnosis?></td>
                                <td>
                                    <table class="table borderless" style="margin-bottom: 0">
                                        <?php foreach ($d->penyakit as $p) : ?>
                                            <tr>
                                                <td><?=$p->kode?></td>
                                                <td><?=$p->nama?></td>
                                            </tr>
                                        <?php endforeach; ?>
                                    </table>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </table>
                </td>
                <td>
                    <a href="<?php echo base_url(); ?>RuangBersalin/detailRekamMedis/<?php echo $r['id']; ?>/<?php echo $r['transfer_id']; ?>" class="btn btn-success btn-block"><span class="fa fa-arrows"></span> Detail</a>
                </td>
            </tr>
            <?php $no++;
        } ?>
        </tbody>
    </table>
</div>