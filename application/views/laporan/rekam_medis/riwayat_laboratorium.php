<div class="tab-pane" id="tab_2">
    <table class="table table-striped table-bordered example2">
        <thead>
        <tr>
            <th>No</th>
            <th>Tanggal Periksa</th>
            <th>Jenis Layanan</th>
            <th style="width: 100px">Aksi</th>
        </tr>
        </thead>
        <tbody>
        <?php
        $no = 1;
        foreach ($laborat as $the_i => $row) { ?>
            <tr>
                <td> <?php echo $no; ?></td>
                <td> <?php echo date('d-F-Y', strtotime($row->waktu_pemeriksaan)); ?></td>
                <td>
                    <table>
                        <tbody>
                        <?php foreach ($row->tindakan as $t) : ?>
                            <tr>
                                <td><?=$t->nama?></td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                </td>
                <td style="width: 100px">
                    <div class="btn-group" role="group">
                        <button id="btnGroupDrop1" type="button" class="btn btn-sm btn-success dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fa fa-bars"></i>
                        </button>
                        <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                            <a href="<?php echo base_url(); ?>Laporan/EditRekamMedis/<?php echo $row->id; ?>" style="margin-bottom: 5px;" class="btn-block">
                                <button type="button" class="btn btn-sm btn-block btn-primary"><i class="fa fa-pencil"></i> Edit</button>
                            </a>
                            <a href="<?php echo base_url(); ?>laboratorium/detail/<?php echo $row->id; ?>" class="btn btn-sm btn-block btn-success" style="margin-bottom: 5px;" ><i class="fa fa-arrows"></i> Detail</a>
                            <a href="<?php echo base_url(); ?>laboratorium/cetak/<?php echo $row->id; ?>"
                               class="btn btn-sm btn-block btn-danger"
                               target="_blank"
                               rel="noopener noreferrer"
                               style="margin-bottom: 5px;" >
                                <i class="fa fa-print"></i> Cetak
                            </a>
                            <a href="<?php echo base_url(); ?>laboratorium/unduh/<?php echo $row->id; ?>" class="btn btn-sm btn-block btn-warning" style="margin-bottom: 5px;" ><i class="fa fa-download"></i> Unduh</a>
                        </div>
                    </div>
                </td>
            </tr>
            <?php $no++;
        } ?>
        </tbody>
    </table>
</div>
