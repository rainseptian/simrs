<?php

include BASEPATH.'../application/views/template/InputBuilder.php';

?>

<style>
    table.bor, th.bor, td.bor {
        font-size: 12px;
        border: 1px solid #e5e5e5;
    }
    th, td {
        padding: 0px 5px;
    }
    td.cat {
        min-width: 100px;
        max-width: 300px;
    }
    td.no-data {
        padding: 5px;
        text-align: center;
    }
</style>

<style media="screen">
    .select2-container {
        width: 100% !important;
    }
</style>
<link rel="stylesheet" href="<?= base_url() ?>assets/plugins/iCheck/all.css">
<form class="form-horizontal" method="post" action="<?= base_url() ?>pemeriksaan/periksa/<?= $pemeriksaan['id'] ?>">
    <div class="content-wrapper">
        <section class="content-header">
            <h1>
                Pemeriksaan Pasien
                <small>Preview</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="<?= base_url() ?>Dashbord"><i class="fa fa-dashboard"></i> Dashbord</a></li>
                <li class="active">Pemeriksaan</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-7">

                    <div class="box box-danger">
                        <div class="box-header">
                            <h3 class="box-title"> No. Rekam Medis : <?= $pendaftaran['no_rm']; ?> </h3>
                        </div>
                        <div class="box-body">
                            <div class="box-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <?php
                                        unit('td', 'TD')->val($pemeriksaan['td'])->unit('mmHg')->readonly()->build();
                                        unit('r', 'R')->val($pemeriksaan['r'])->unit('K/Min')->readonly()->build();
                                        unit('bb', 'BB')->val($pemeriksaan['bb'])->unit('Kg')->onkeyup('set_bmi()')->readonly()->build();
                                        unit('n', 'N')->val($pemeriksaan['n'])->unit('K/Min')->readonly()->build();
                                        unit('s', 'S')->val($pemeriksaan['s'])->unit("'0")->readonly()->build();
                                        unit('tb', 'TB')->val($pemeriksaan['tb'])->unit("cm")->readonly()->build();
                                        ?>
                                    </div>
                                </div>

                                <input type="hidden" class="form-control" name="pendaftaran_id" id="pendaftran_id"
                                       value="<?= $pemeriksaan['id']; ?>">
                                <input type="hidden" class="form-control" name="dokter_id" id="dokter_id"
                                       value="<?= $pemeriksaan['dokter_id']; ?>">
                                <input type="hidden" class="form-control" name="kode_daftar" id="kode_daftar"
                                       value="<?= $pendaftaran['kode_daftar']; ?>">

                                <?php
                                sm4('bmi', 'BMI')->val($pemeriksaan['bmi'])->readonly()->build();
                                sm4('no_rm', 'No Rekam Medis')->val($pemeriksaan['no_rm'])->readonly()->build();
                                ?>
                                <div class="form-group">
                                    <label class="col-sm-3"></label>
                                    <div class="col-sm-9">
                                        <span class="label <?= $jaminan[$pemeriksaan['jaminan']]['class'] ?>"><?= $jaminan[$pemeriksaan['jaminan']]['label'] ?></span>
                                        <?php if (!isset($jaminan[$pemeriksaan['jaminan']])) { ?>
                                            <span class="label label-warning">Umum</span>
                                        <?php } ?>
                                        <button type="button" name="button" class="btn btn-sm btn-primary btn-rekamedis"
                                                data-pasien_id="<?= $pendaftaran['pasien'] ?>"><i
                                                class="fa fa-search"></i> Lihat Rekam Medis
                                        </button>
                                    </div>
                                </div>
                                <?php
                                sm4('nama_dokter', 'Nama Dokter')->val($pendaftaran['nama_dokter'])->readonly()->build();
                                sm4('nama_pasien', 'Nama Pasien')->val($pemeriksaan['nama_pasien'])->readonly()->build();
                                ?>
                                <!--                                form perawat-->
                                <div>
                                    <h4><strong>Alergi</strong></h4>
                                    <p>Apakah pasien mempunyai indikasi alergi ?</p>
                                    <div class="form-check" style="display: inline">
                                        <input class="form-check-input" type="radio" name="form[alergi]" value="ya" id="flexRadioDefault1" <?= isset($form['alergi']) && $form['alergi'] == 'ya' ? 'checked' : '' ?>>
                                        <label class="form-check-label" for="flexRadioDefault1" style="font-weight: normal !important;">
                                            Ya
                                        </label>
                                    </div>
                                    <div class="form-check" style="display: inline; margin-left: 16px">
                                        <input class="form-check-input" type="radio" name="form[alergi]" value="tidak" id="flexRadioDefault2" <?= isset($form['alergi']) && $form['alergi'] == 'tidak' ? 'checked' : '' ?>>
                                        <label class="form-check-label" for="flexRadioDefault2" style="font-weight: normal !important;">
                                            Tidak
                                        </label>
                                    </div>
                                    <?php sm9('form[alergi_detail]', 'Jika Ya, Jelaskan')->val($form['alergi_detail'] ?? '')->build(); ?>
                                </div>
                                <div style="margin-top: 24px">
                                    <h4><strong>Nyeri</strong></h4>
                                    <p style="margin-bottom: 2px !important;">Skala Numerik</p>
                                    <div style="display: flex; width: 100%;">
                                        <div style="flex: 1; display: flex; flex-direction: column; align-items: center">
                                            <p style="margin-bottom: 0 !important;">1</p>
                                            <input class="form-check-input" type="radio" name="form[nyeri]" value="1" style="margin-top: 0 !important;" <?= isset($form['nyeri']) && $form['nyeri'] == '1' ? 'checked' : '' ?>>
                                        </div>
                                        <div style="flex: 1; display: flex; flex-direction: column; align-items: center">
                                            <p style="margin-bottom: 0 !important;">2</p>
                                            <input class="form-check-input" type="radio" name="form[nyeri]" value="2" style="margin-top: 0 !important;" <?= isset($form['nyeri']) && $form['nyeri'] == '2' ? 'checked' : '' ?>>
                                        </div>
                                        <div style="flex: 1; display: flex; flex-direction: column; align-items: center">
                                            <p style="margin-bottom: 0 !important;">3</p>
                                            <input class="form-check-input" type="radio" name="form[nyeri]" value="3" style="margin-top: 0 !important;" <?= isset($form['nyeri']) && $form['nyeri'] == '3' ? 'checked' : '' ?>>
                                        </div>
                                        <div style="flex: 1; display: flex; flex-direction: column; align-items: center">
                                            <p style="margin-bottom: 0 !important;">4</p>
                                            <input class="form-check-input" type="radio" name="form[nyeri]" value="4" style="margin-top: 0 !important;" <?= isset($form['nyeri']) && $form['nyeri'] == '4' ? 'checked' : '' ?>>
                                        </div>
                                        <div style="flex: 1; display: flex; flex-direction: column; align-items: center">
                                            <p style="margin-bottom: 0 !important;">5</p>
                                            <input class="form-check-input" type="radio" name="form[nyeri]" value="5" style="margin-top: 0 !important;" <?= isset($form['nyeri']) && $form['nyeri'] == '5' ? 'checked' : '' ?>>
                                        </div>
                                        <div style="flex: 1; display: flex; flex-direction: column; align-items: center">
                                            <p style="margin-bottom: 0 !important;">6</p>
                                            <input class="form-check-input" type="radio" name="form[nyeri]" value="6" style="margin-top: 0 !important;" <?= isset($form['nyeri']) && $form['nyeri'] == '6' ? 'checked' : '' ?>>
                                        </div>
                                        <div style="flex: 1; display: flex; flex-direction: column; align-items: center">
                                            <p style="margin-bottom: 0 !important;">7</p>
                                            <input class="form-check-input" type="radio" name="form[nyeri]" value="7" style="margin-top: 0 !important;" <?= isset($form['nyeri']) && $form['nyeri'] == '7' ? 'checked' : '' ?>>
                                        </div>
                                        <div style="flex: 1; display: flex; flex-direction: column; align-items: center">
                                            <p style="margin-bottom: 0 !important;">8</p>
                                            <input class="form-check-input" type="radio" name="form[nyeri]" value="8" style="margin-top: 0 !important;" <?= isset($form['nyeri']) && $form['nyeri'] == '8' ? 'checked' : '' ?>>
                                        </div>
                                        <div style="flex: 1; display: flex; flex-direction: column; align-items: center">
                                            <p style="margin-bottom: 0 !important;">9</p>
                                            <input class="form-check-input" type="radio" name="form[nyeri]" value="9" style="margin-top: 0 !important;" <?= isset($form['nyeri']) && $form['nyeri'] == '9' ? 'checked' : '' ?>>
                                        </div>
                                        <div style="flex: 1; display: flex; flex-direction: column; align-items: center">
                                            <p style="margin-bottom: 0 !important;">10</p>
                                            <input class="form-check-input" type="radio" name="form[nyeri]" value="10" style="margin-top: 0 !important;" <?= isset($form['nyeri']) && $form['nyeri'] == '10' ? 'checked' : '' ?>>
                                        </div>
                                    </div>
                                    <p style="margin-bottom: 2px !important; margin-top: 8px !important;">Wong Baker Faces</p>
                                    <div style="display: flex; flex-direction: column">
                                        <img src="<?php echo base_url(); ?>assets/img/wong.jpeg" alt="User Image">
                                        <div style="display: flex; flex: 1; margin-left: 6px; margin-right: 8px;">
                                            <input class="form-check-input" type="radio" name="form[wong]" value="0" style="margin-top: 0 !important; flex: 1" <?= isset($form['wong']) && $form['wong'] == '0' ? 'checked' : '' ?>>
                                            <input class="form-check-input" type="radio" name="form[wong]" value="2" style="margin-top: 0 !important; flex: 1" <?= isset($form['wong']) && $form['wong'] == '2' ? 'checked' : '' ?>>
                                            <input class="form-check-input" type="radio" name="form[wong]" value="4" style="margin-top: 0 !important; flex: 1" <?= isset($form['wong']) && $form['wong'] == '4' ? 'checked' : '' ?>>
                                            <input class="form-check-input" type="radio" name="form[wong]" value="6" style="margin-top: 0 !important; flex: 1" <?= isset($form['wong']) && $form['wong'] == '6' ? 'checked' : '' ?>>
                                            <input class="form-check-input" type="radio" name="form[wong]" value="8" style="margin-top: 0 !important; flex: 1" <?= isset($form['wong']) && $form['wong'] == '8' ? 'checked' : '' ?>>
                                            <input class="form-check-input" type="radio" name="form[wong]" value="10" style="margin-top: 0 !important; flex: 1" <?= isset($form['wong']) && $form['wong'] == '10' ? 'checked' : '' ?>>
                                        </div>
                                    </div>
                                </div>
                                <div style="margin-top: 24px">
                                    <h4><strong>Gizi</strong></h4>
                                    <div class="col-sm-10 form-group">
                                        <label for="td" class="col-sm-6 control-label">Tinggi Badan</label>
                                        <div class="input-group col-sm-4">
                                            <input type="text" class="form-control" name="form[tb]" id="form_tb" onkeyup="setImt()" autocomplete="off" value="<?=$form['tb'] ?? ''?>">
                                            <span class="input-group-addon">cm</span>
                                        </div>
                                    </div>
                                    <div class="col-sm-10 form-group">
                                        <label for="td" class="col-sm-6 control-label">Berat Badan</label>
                                        <div class="input-group col-sm-4">
                                            <input type="text" class="form-control" name="form[bb]" id="form_bb" onkeyup="setImt()" autocomplete="off" value="<?=$form['bb'] ?? ''?>">
                                            <span class="input-group-addon">kg</span>
                                        </div>
                                    </div>
                                    <div class="col-sm-10 form-group">
                                        <label for="td" class="col-sm-6 control-label">Indeks Massa Tubuh (IMT)</label>
                                        <div class="input-group col-sm-4">
                                            <input type="text" class="form-control" name="form[imt]" id="form_imt" autocomplete="off" value="<?=$form['imt'] ?? ''?>">
                                            <span class="input-group-addon">kg/m2</span>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div style="display: flex; flex-direction: column">
                                                <div style="display: flex">
                                                    <div class="checkbox" style="flex: 1">
                                                        <label>
                                                            <input type="radio" name="form[gizi]" value="underweight" id="underweight" <?=isset($form['gizi']) && $form['gizi'] == 'underweight' ? 'checked' : ''?>> Underweight ( < 18.5 )
                                                        </label>
                                                    </div>
                                                    <div class="checkbox" style="flex: 1">
                                                        <label>
                                                            <input type="radio" name="form[gizi]" value="overweight" id="overweight" <?=isset($form['gizi']) && $form['gizi'] == 'overweight' ? 'checked' : ''?>> Overweight (25 – 29.99)
                                                        </label>
                                                    </div>
                                                </div>
                                                <div style="display: flex">
                                                    <div class="checkbox" style="flex: 1">
                                                        <label>
                                                            <input type="radio" name="form[gizi]" value="normal" id="normal" <?=isset($form['gizi']) && $form['gizi'] == 'normal' ? 'checked' : ''?>> Normal ( 18.5 – 24.99)
                                                        </label>
                                                    </div>
                                                    <div class="checkbox" style="flex: 1">
                                                        <label>
                                                            <input type="radio" name="form[gizi]" value="obese" id="obese" <?=isset($form['gizi']) && $form['gizi'] == 'obese' ? 'checked' : ''?>> Obese (≥ 30)
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div style="margin-top: 24px">
                                    <h4><strong>SKRINING RESIKO JATUH</strong></h4>
                                    <div style="display: flex">
                                        <div>
                                            <p style="margin-bottom: 2px !important;">Cara berjalan pasien (salah satu/lebih)</p>
                                            <p style="margin-bottom: 2px !important;">a. Tidak seimbang/sempoyongan/limbung</p>
                                            <p style="margin-bottom: 2px !important;">b. Jalan dengan alat bantu (kruk, tripod, kursi roda, orang lain)</p>
                                        </div>
                                        <div style="margin-left: 26px">
                                            <div class="checkbox">
                                                <label>
                                                    <input type="radio" name="form[skrining_resiko_jatuh]" value="ya" <?=isset($form['skrining_resiko_jatuh']) && $form['skrining_resiko_jatuh'] == 'ya' ? 'checked' : ''?>> Ya
                                                </label>
                                            </div>
                                            <div class="checkbox">
                                                <label>
                                                    <input type="radio" name="form[skrining_resiko_jatuh]" value="tidak" <?=isset($form['skrining_resiko_jatuh']) && $form['skrining_resiko_jatuh'] == 'tidak' ? 'checked' : ''?>> Tidak
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <br>
                                    <p style="margin-bottom: 2px !important;">Ada keterbatasan gerak ?</p>
                                    <div class="checkbox" style="display: inline">
                                        <label>
                                            <input type="radio" name="form[skrining_keterbatasan_gerak]" value="ya" <?=isset($form['skrining_keterbatasan_gerak']) && $form['skrining_keterbatasan_gerak'] == 'ya' ? 'checked' : ''?>> Ya
                                        </label>
                                    </div>
                                    <div class="checkbox" style="display: inline">
                                        <label>
                                            <input type="radio" name="form[skrining_keterbatasan_gerak]" value="tidak" <?=isset($form['skrining_keterbatasan_gerak']) && $form['skrining_keterbatasan_gerak'] == 'tidak' ? 'checked' : ''?>> Tidak
                                        </label>
                                    </div>
                                </div>
                                <hr>
                                <div>
                                    <?php
                                    sm9('diagnosa_perawat', 'Diagnosa Perawat')->val($pemeriksaan['diagnosa_perawat'] ?? '')->build();
                                    sm9('keluhan_utama', 'Keluhan Utama')->val($pemeriksaan['keluhan_utama'] ?? '')->build();
                                    sm9('asuhan_keperawatan', 'Catatan Alergi/Lainnya')->val($pemeriksaan['asuhan_keperawatan'] ?? '')->build();

                                    sm9('form[daftar_masalah_medis]', 'Daftar Masalah Medis')->val($form['daftar_masalah_medis'] ?? '')->hide()->build();
                                    sm9('form[dsdok]', 'Data Subyektif dan Obyektif Keperawatan')->val($form['dsdok'] ?? '')->hide()->build();
                                    sm9('form[dmk]', 'Daftar Masalah Keperawatan')->val($form['dmk'] ?? '')->hide()->build();
                                    ?>
                                </div>
                                <!--                                end form perawat-->
                                <?php
                                //                                sm9('diagnosa_perawat', 'Diagnosa Perawat')->val($pemeriksaan['diagnosa_perawat'])->build();
                                //                                sm9('amammesia', 'Anamnesis')->build();
                                //                                sm9('pemeriksaan_fisik', 'Pemeriksaan Fisik')->textarea()->build();
                                //                                hasil_penunjang();
                                //                                sm9('asuhan_keperawatan', 'Catatan Alergi/Lainnya')->val($pemeriksaan['asuhan_keperawatan'])->build();
                                ?>
                                <input type="hidden" name="kode_daftar" value="<?= $pendaftaran['kode_daftar'] ?>">

                                <div style="margin-top: 24px">
                                    <h3><strong>SURVEY PRIMER</strong></h3>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <?php
                                            sm9('form[mulai_diperiksa_tanggal]', 'Mulai diperiksa di ruang label tanggal')
                                                ->val($form['mulai_diperiksa_tanggal'] ?? '')
                                                ->break_label()
                                                ->build();
                                            ?>
                                        </div>
                                        <div class="col-md-6">
                                            <?php
                                            sm9('form[mulai_diperiksa_jam]', 'Jam')
                                                ->val($form['mulai_diperiksa_jam'] ?? '')
                                                ->break_label()
                                                ->build();
                                            ?>
                                        </div>
                                    </div>
                                    <?php
                                    sm9('form[survey_primer]', 'Survey Primer')
                                        ->val($form['survey_primer'] ?? '')
                                        ->break_label()
                                        ->build();
                                    ?>
                                    <h4><strong>Pemeriksaan Fisik</strong></h4>
                                    <p style="margin-bottom: 2px !important;"><strong>Air Way</strong></p>
                                    <p style="margin-bottom: 2px !important;"><strong>Jalan Nafas</strong></p>
                                    <div class="checkbox" style="display: inline">
                                        <label>
                                            <input type="radio" name="form[jalan_nafas]" value="Bebas" <?=isset($form['jalan_nafas']) && $form['jalan_nafas'] == 'Bebas' ? 'checked' : ''?>> Bebas
                                        </label>
                                    </div>
                                    <div class="checkbox" style="display: inline">
                                        <label>
                                            <input type="radio" name="form[jalan_nafas]" value="Tersumbat sebagian" <?=isset($form['jalan_nafas']) && $form['jalan_nafas'] == 'Tersumbat sebagian' ? 'checked' : ''?>> Tersumbat sebagian
                                        </label>
                                    </div>
                                    <div class="checkbox" style="display: inline">
                                        <label>
                                            <input type="radio" name="form[jalan_nafas]" value="Tersumbat total" <?=isset($form['jalan_nafas']) && $form['jalan_nafas'] == 'Tersumbat total' ? 'checked' : ''?>> Tersumbat total
                                        </label>
                                    </div>
                                    <br>
                                    <br>
                                    <p style="margin-bottom: 2px !important;"><strong>Breathing</strong></p>
                                    <p style="margin-bottom: 2px !important;"><strong>Pengembangan Paru</strong></p>
                                    <div class="checkbox" style="display: inline">
                                        <label>
                                            <input type="radio" name="form[pengembangan_paru]" value="Simetris" <?=isset($form['pengembangan_paru']) && $form['pengembangan_paru'] == 'Simetris' ? 'checked' : ''?>> Simetris
                                        </label>
                                    </div>
                                    <div class="checkbox" style="display: inline">
                                        <label>
                                            <input type="radio" name="form[pengembangan_paru]" value="Asimetris" <?=isset($form['pengembangan_paru']) && $form['pengembangan_paru'] == 'Asimetris' ? 'checked' : ''?>> Asimetris
                                        </label>
                                    </div>
                                    <br>
                                    <br>
                                    <p style="margin-bottom: 2px !important;"><strong>Pola Nafas</strong></p>
                                    <div style="width: 100%; display: flex">
                                        <div>
                                            <div class="checkbox" style="display: inline">
                                                <label>
                                                    <input type="radio" name="form[pola_nafas]" value="Normal/Eupnea"
                                                        <?=isset($form['pola_nafas']) && $form['pola_nafas'] == 'Normal/Eupnea' ? 'checked' : ''?>> Normal/Eupnea
                                                </label>
                                            </div>
                                            <div class="checkbox" style="display: inline">
                                                <label>
                                                    <input type="radio" name="form[pola_nafas]" value="Bradipnoe"
                                                        <?=isset($form['pola_nafas']) && $form['pola_nafas'] == 'Bradipnoe' ? 'checked' : ''?>> Bradipnoe
                                                </label>
                                            </div>
                                        </div>
                                        <div>
                                            <div class="checkbox" style="display: inline">
                                                <label>
                                                    <input type="radio" name="form[pola_nafas]" value="Tachipnoe"
                                                        <?=isset($form['pola_nafas']) && $form['pola_nafas'] == 'Tachipnoe' ? 'checked' : ''?>> Tachipnoe
                                                </label>
                                            </div>
                                            <div class="checkbox" style="display: inline">
                                                <label>
                                                    <input type="radio" name="form[pola_nafas]" value="Dispnoe"
                                                        <?=isset($form['pola_nafas']) && $form['pola_nafas'] == 'Dispnoe' ? 'checked' : ''?>> Dispnoe
                                                </label>
                                            </div>
                                        </div>
                                        <div>
                                            <div class="checkbox" style="display: inline">
                                                <label>
                                                    <input type="radio" name="form[pola_nafas]" value="Apnoe"
                                                        <?=isset($form['pola_nafas']) && $form['pola_nafas'] == 'Apnoe' ? 'checked' : ''?>> Apnoe
                                                </label>
                                            </div>
                                            <div class="checkbox" style="display: inline">
                                                <label>
                                                    <input type="radio" name="form[pola_nafas]" value="Cheynestoke"
                                                        <?=isset($form['pola_nafas']) && $form['pola_nafas'] == 'Cheynestoke' ? 'checked' : ''?>> Cheynestoke
                                                </label>
                                            </div>
                                        </div>
                                        <div>
                                            <div class="checkbox" style="display: inline">
                                                <label>
                                                    <input type="radio" name="form[pola_nafas]" value="Kussmaul"
                                                        <?=isset($form['pola_nafas']) && $form['pola_nafas'] == 'Kussmaul' ? 'checked' : ''?>> Kussmaul
                                                </label>
                                            </div>
                                            <div class="checkbox" style="display: inline">
                                                <label>
                                                    <input type="radio" name="form[pola_nafas]" value="Biot"
                                                        <?=isset($form['pola_nafas']) && $form['pola_nafas'] == 'Biot' ? 'checked' : ''?>> Biot
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <br>
                                    <p style="margin-bottom: 2px !important;"><strong>Suara Nafas</strong></p>
                                    <div class="checkbox" style="display: inline">
                                        <label>
                                            <input type="radio" name="form[suara_nafas]" value="Vesikuler" <?=isset($form['suara_nafas']) && $form['suara_nafas'] == 'Vesikuler' ? 'checked' : ''?>> Vesikuler
                                        </label>
                                    </div>
                                    <div class="checkbox" style="display: inline">
                                        <label>
                                            <input type="radio" name="form[suara_nafas]" value="Ronchi" <?=isset($form['suara_nafas']) && $form['suara_nafas'] == 'Ronchi' ? 'checked' : ''?>> Ronchi
                                        </label>
                                    </div>
                                    <div class="checkbox" style="display: inline">
                                        <label>
                                            <input type="radio" name="form[suara_nafas]" value="Wheezing" <?=isset($form['suara_nafas']) && $form['suara_nafas'] == 'Wheezing' ? 'checked' : ''?>> Wheezing
                                        </label>
                                    </div>
                                    <div class="checkbox" style="display: inline">
                                        <label>
                                            <input type="radio" name="form[suara_nafas]" value="Stridor" <?=isset($form['suara_nafas']) && $form['suara_nafas'] == 'Stridor' ? 'checked' : ''?>> Stridor
                                        </label>
                                    </div>
                                    <br>
                                    <br>
                                    <p style="margin-bottom: 2px !important;"><strong>Circulation</strong></p>
                                    <p style="margin-bottom: 2px !important;"><strong>Kulit</strong></p>
                                    <div class="checkbox" style="display: inline">
                                        <label>
                                            <input type="radio" name="form[kulit]" value="Normal" <?=isset($form['kulit']) && $form['kulit'] == 'Normal' ? 'checked' : ''?>> Normal
                                        </label>
                                    </div>
                                    <div class="checkbox" style="display: inline">
                                        <label>
                                            <input type="radio" name="form[kulit]" value="Pucat" <?=isset($form['kulit']) && $form['kulit'] == 'Pucat' ? 'checked' : ''?>> Pucat
                                        </label>
                                    </div>
                                    <br>
                                    <br>
                                    <p style="margin-bottom: 2px !important;"><strong>Warna Membran</strong></p>
                                    <div class="checkbox" style="display: inline">
                                        <label>
                                            <input type="radio" name="form[warna_membran]" value="Merah" <?=isset($form['warna_membran']) && $form['warna_membran'] == 'Merah' ? 'checked' : ''?>> Merah
                                        </label>
                                    </div>
                                    <div class="checkbox" style="display: inline">
                                        <label>
                                            <input type="radio" name="form[warna_membran]" value="Abu - Abu" <?=isset($form['warna_membran']) && $form['warna_membran'] == 'Abu - Abu' ? 'checked' : ''?>> Abu - Abu
                                        </label>
                                    </div>
                                    <br>
                                    <br>
                                    <p style="margin-bottom: 2px !important;"><strong>Akral Kulit</strong></p>
                                    <div class="checkbox" style="display: inline">
                                        <label>
                                            <input type="radio" name="form[akral]" value="Hangat" <?=isset($form['akral']) && $form['akral'] == 'Hangat' ? 'checked' : ''?>> Hangat
                                        </label>
                                    </div>
                                    <div class="checkbox" style="display: inline">
                                        <label>
                                            <input type="radio" name="form[akral]" value="Dingin" <?=isset($form['akral']) && $form['akral'] == 'Dingin' ? 'checked' : ''?>> Dingin
                                        </label>
                                    </div>
                                    <br>
                                    <br>
                                    <p style="margin-bottom: 2px !important;"><strong>Mukosa</strong></p>
                                    <div class="checkbox" style="display: inline">
                                        <label>
                                            <input type="radio" name="form[mukosa]" value="Basah" <?=isset($form['mukosa']) && $form['mukosa'] == 'Basah' ? 'checked' : ''?>> Basah
                                        </label>
                                    </div>
                                    <div class="checkbox" style="display: inline">
                                        <label>
                                            <input type="radio" name="form[mukosa]" value="Kering" <?=isset($form['mukosa']) && $form['mukosa'] == 'Kering' ? 'checked' : ''?>> Kering
                                        </label>
                                    </div>
                                    <br>
                                    <br>
                                    <p style="margin-bottom: 2px !important;"><strong>Pengisian Kapiler / CRT</strong></p>
                                    <div class="checkbox" style="display: inline">
                                        <label>
                                            <input type="radio" name="form[kapiler_crt]" value="kurang_2" <?=isset($form['kapiler_crt']) && $form['kapiler_crt'] == 'kurang_2' ? 'checked' : ''?>> ≤ 2 detik
                                        </label>
                                    </div>
                                    <div class="checkbox" style="display: inline">
                                        <label>
                                            <input type="radio" name="form[kapiler_crt]" value="lebih_2" <?=isset($form['kapiler_crt']) && $form['kapiler_crt'] == 'lebih_2' ? 'checked' : ''?>> ≥ 2 detik
                                        </label>
                                    </div>
                                    <br>
                                    <br>
                                    <p style="margin-bottom: 2px !important;"><strong>Nadi</strong></p>
                                    <div class="checkbox" style="display: inline">
                                        <label>
                                            <input type="radio" name="form[nadi]" value="Kuat" <?=isset($form['nadi']) && $form['nadi'] == 'Kuat' ? 'checked' : ''?>> Kuat
                                        </label>
                                    </div>
                                    <div class="checkbox" style="display: inline">
                                        <label>
                                            <input type="radio" name="form[nadi]" value="Lemah" <?=isset($form['nadi']) && $form['nadi'] == 'Lemah' ? 'checked' : ''?>> Lemah
                                        </label>
                                    </div>
                                    <br>
                                    <br>
                                    <p style="margin-bottom: 2px !important;"><strong>Ritmen</strong></p>
                                    <div class="checkbox" style="display: inline">
                                        <label>
                                            <input type="radio" name="form[ritmen]" value="Teratur" <?=isset($form['ritmen']) && $form['ritmen'] == 'Teratur' ? 'checked' : ''?>> Teratur
                                        </label>
                                    </div>
                                    <div class="checkbox" style="display: inline">
                                        <label>
                                            <input type="radio" name="form[ritmen]" value="Tidak Teratur" <?=isset($form['ritmen']) && $form['ritmen'] == 'Tidak Teratur' ? 'checked' : ''?>> Tidak Teratur
                                        </label>
                                    </div>
                                    <br>
                                    <br>
                                    <p style="margin-bottom: 2px !important;"><strong>Disability</strong></p>
                                    <?php
                                    sm9('form[disability][GCS]', 'GCS')->val($form['disability']['GCS'] ?? '')->build();
                                    sm9('form[disability][E]', 'E')->val($form['disability']['E'] ?? '')->build();
                                    sm9('form[disability][V]', 'V')->val($form['disability']['V'] ?? '')->build();
                                    sm9('form[disability][M]', 'M')->val($form['disability']['M'] ?? '')->build();
                                    ?>
                                    <p style="margin-bottom: 2px !important;"><strong>Kesadaran</strong></p>
                                    <div style="width: 100%; display: flex">
                                        <div>
                                            <div class="checkbox">
                                                <label>
                                                    <input type="radio" name="form[kesadaran]" value="Composmentis" <?=isset($form['kesadaran']) && $form['kesadaran'] == 'Composmentis' ? 'checked' : ''?>> Composmentis
                                                </label>
                                            </div>
                                            <div class="checkbox">
                                                <label>
                                                    <input type="radio" name="form[kesadaran]" value="Apatis" <?=isset($form['kesadaran']) && $form['kesadaran'] == 'Apatis' ? 'checked' : ''?>> Apatis
                                                </label>
                                            </div>
                                        </div>
                                        <div>
                                            <div class="checkbox">
                                                <label>
                                                    <input type="radio" name="form[kesadaran]" value="Somnolen" <?=isset($form['kesadaran']) && $form['kesadaran'] == 'Somnolen' ? 'checked' : ''?>> Somnolen
                                                </label>
                                            </div>
                                            <div class="checkbox">
                                                <label>
                                                    <input type="radio" name="form[kesadaran]" value="Stupor" <?=isset($form['kesadaran']) && $form['kesadaran'] == 'Stupor' ? 'checked' : ''?>> Stupor
                                                </label>
                                            </div>
                                        </div>
                                        <div>
                                            <div class="checkbox">
                                                <label>
                                                    <input type="radio" name="form[kesadaran]" value="Delirium" <?=isset($form['kesadaran']) && $form['kesadaran'] == 'Delirium' ? 'checked' : ''?>> Delirium
                                                </label>
                                            </div>
                                            <div class="checkbox">
                                                <label>
                                                    <input type="radio" name="form[kesadaran]" value="Coma" <?=isset($form['kesadaran']) && $form['kesadaran'] == 'Coma' ? 'checked' : ''?>> Coma
                                                </label>
                                            </div>
                                        </div>
                                    </div>

                                    <h3 style="margin-top: 24px"><strong>SURVEY SEKUNDER</strong></h3>
                                    <p style="margin-bottom: 2px !important;"><strong>Anamnesis didapatkan dari</strong></p>
                                    <div class="checkbox">
                                        <label>
                                            <input type="radio" name="form[anamnesis_dari]" value="Autoanamnesis" <?=isset($form['anamnesis_dari']) && $form['anamnesis_dari'] == 'Autoanamnesis' ? 'checked' : ''?>> Autoanamnesis
                                        </label>
                                    </div>
                                    <div class="checkbox" style="display: inline">
                                        <label>
                                            <input type="radio" name="form[anamnesis_dari]" value="Alloanamnesis" <?=isset($form['anamnesis_dari']) && $form['anamnesis_dari'] == 'Alloanamnesis' ? 'checked' : ''?>> Alloanamnesis dengan
                                        </label>
                                    </div>
                                    <input type="text" class="form-control" style="display: inline !important; width: 100px !important;" name="form[Alloanamnesis_dari]" value="<?=$form['Alloanamnesis_dari'] ?? ''?>" autocomplete="off">
                                    hubungan dengan pasien
                                    <input type="text" class="form-control" style="display: inline !important; width: 100px !important;" name="form[Alloanamnesis_hubungan]" value="<?=$form['Alloanamnesis_hubungan'] ?? ''?>" autocomplete="off">
                                    <br>
                                    <br>
                                    <?php
                                    sm9('keluhan_utama', 'Keluhan Utama')
                                        ->val($pemeriksaan['keluhan_utama'] ?? '')
                                        ->break_label()
                                        ->build();
                                    sm9('form[riwayat_penyakit]', 'Riwayat Penyakit (lokasi, onset dan kronologis, kualitas, kuantitas, factor memperberat, factor memperingan, gejala penyerta)')
                                        ->val($form['riwayat_penyakit'] ?? '')
                                        ->break_label()
                                        ->build();
                                    sm9('form[obat_obatan]', 'Obat-Obatan yang sedang dikonsumsi dan atau dibawa pasien pada saat ini')
                                        ->val($form['obat_obatan'] ?? '')
                                        ->break_label()
                                        ->build();
                                    sm9('form[sudah_ada]', 'Pemeriksaan dan penunjang hasil yang sudah ada')
                                        ->val($form['sudah_ada'] ?? '')
                                        ->break_label()
                                        ->build();
                                    ?>
                                    <br>
                                    <p style="margin-bottom: 2px !important;"><strong>Status Generalis</strong></p>
                                    <p style="margin-bottom: 2px !important;"><strong>Kondisi Umum</strong></p>
                                    <div class="checkbox" style="display: inline">
                                        <label>
                                            <input type="radio" name="form[kondisi_umum]" value="Baik" <?=isset($form['kondisi_umum']) && $form['kondisi_umum'] == 'Baik' ? 'checked' : ''?>> Baik
                                        </label>
                                    </div>
                                    <div class="checkbox" style="display: inline">
                                        <label>
                                            <input type="radio" name="form[kondisi_umum]" value="Tampak sakit" <?=isset($form['kondisi_umum']) && $form['kondisi_umum'] == 'Tampak sakit' ? 'checked' : ''?>> Tampak sakit
                                        </label>
                                    </div>
                                    <div class="checkbox" style="display: inline">
                                        <label>
                                            <input type="radio" name="form[kondisi_umum]" value="Sesak" <?=isset($form['kondisi_umum']) && $form['kondisi_umum'] == 'Sesak' ? 'checked' : ''?>> Sesak
                                        </label>
                                    </div>
                                    <div class="checkbox" style="display: inline">
                                        <label>
                                            <input type="radio" name="form[kondisi_umum]" value="Pucat" <?=isset($form['kondisi_umum']) && $form['kondisi_umum'] == 'Pucat' ? 'checked' : ''?>> Pucat
                                        </label>
                                    </div>
                                    <div class="checkbox" style="display: inline">
                                        <label>
                                            <input type="radio" name="form[kondisi_umum]" value="Lemah" <?=isset($form['kondisi_umum']) && $form['kondisi_umum'] == 'Lemah' ? 'checked' : ''?>> Lemah
                                        </label>
                                    </div>
                                    <div class="checkbox" style="display: inline">
                                        <label>
                                            <input type="radio" name="form[kondisi_umum]" value="Kejang" <?=isset($form['kondisi_umum']) && $form['kondisi_umum'] == 'Kejang' ? 'checked' : ''?>> Kejang
                                        </label>
                                    </div>
                                    <div class="checkbox" style="display: flex; align-items: center">
                                        <label>
                                            <input type="radio" name="form[kondisi_umum]" value="Lainnya" <?=isset($form['kondisi_umum']) && $form['kondisi_umum'] == 'Lainnya' ? 'checked' : ''?>> Lainnya
                                        </label>
                                        <input type="text" class="form-control" style="flex: 1; margin-left: 10px" name="form[kondisi_umum_lainnya]" value="<?=$form['kondisi_umum_lainnya'] ?? ''?>" autocomplete="off">
                                    </div>
                                    <br>
                                    <p style="margin-bottom: 2px !important;"><strong>Jantung</strong></p>
                                    <img src="<?=base_url('assets/img/jantung.jpeg')?>" alt="" width="200">
                                    <?php
                                    sm9('form[jantung][inspeksi]', 'Inspeksi')
                                        ->val($form['jantung']['inspeksi'] ?? '')
                                        ->break_label()
                                        ->build();
                                    ?>
                                    <p style="margin-bottom: 2px !important;"><strong>Palpasi</strong></p>
                                    <div class="checkbox" style="display: inline">
                                        <label>
                                            <input type="radio" name="form[jantung][palpasi]" value="Ictus Cardis" <?=isset($form['jantung']['palpasi']) && $form['jantung']['palpasi'] == 'Ictus Cardis' ? 'checked' : ''?>> Ictus Cardis
                                        </label>
                                    </div>
                                    <div class="checkbox" style="display: inline">
                                        <label>
                                            <input type="radio" name="form[jantung][palpasi]" value="Thrill" <?=isset($form['jantung']['palpasi']) && $form['jantung']['palpasi'] == 'Thrill' ? 'checked' : ''?>> Thrill
                                        </label>
                                    </div>
                                    <div class="checkbox" style="display: inline">
                                        <label>
                                            <input type="radio" name="form[jantung][palpasi]" value="Lainnya" <?=isset($form['jantung']['palpasi']) && $form['jantung']['palpasi'] == 'Lainnya' ? 'checked' : ''?>> Lainnya
                                        </label>
                                    </div>
                                    <?php
                                    sm9('form[jantung][perkusi]', 'Perkusi')
                                        ->val($form['jantung']['perkusi'] ?? '')
                                        ->break_label()
                                        ->build();
                                    ?>
                                    <p style="margin-bottom: 2px !important;"><strong>Auskultasi</strong></p>
                                    <div class="checkbox" style="display: inline">
                                        <label>
                                            <input type="radio" name="form[jantung][auskultasi][left]" value="S1" <?=isset($form['jantung']['auskultasi']['left']) && $form['jantung']['auskultasi']['left'] == 'S1' ? 'checked' : ''?>> S1
                                        </label>
                                    </div>
                                    <div class="checkbox" style="display: inline; padding-right: 10px">
                                        <label style="padding-left: 10px !important;">
                                            <input type="radio" name="form[jantung][auskultasi][left]" value="S2" <?=isset($form['jantung']['auskultasi']['left']) && $form['jantung']['auskultasi']['left'] == 'S2' ? 'checked' : ''?>> S2
                                        </label>
                                    </div>
                                    /
                                    <div class="checkbox" style="display: inline;">
                                        <label style="padding-left: 10px !important;">
                                            <input type="radio" name="form[jantung][auskultasi][right]" value="A2" <?=isset($form['jantung']['auskultasi']['right']) && $form['jantung']['auskultasi']['right'] == 'A2' ? 'checked' : ''?>> A2
                                        </label>
                                    </div>
                                    <div class="checkbox" style="display: inline;">
                                        <label style="padding-left: 10px !important;">
                                            <input type="radio" name="form[jantung][auskultasi][right]" value="P2" <?=isset($form['jantung']['auskultasi']['right']) && $form['jantung']['auskultasi']['right'] == 'P2' ? 'checked' : ''?>> P2
                                        </label>
                                    </div>
                                    <br>
                                    <div class="checkbox" style="display: inline">
                                        <label>
                                            <input type="radio" name="form[jantung][auskultasi][bottom]" value="Gallop" <?=isset($form['jantung']['auskultasi']['bottom']) && $form['jantung']['auskultasi']['bottom'] == 'Gallop' ? 'checked' : ''?>> Gallop
                                        </label>
                                    </div>
                                    <div class="checkbox" style="display: inline">
                                        <label>
                                            <input type="radio" name="form[jantung][auskultasi][bottom]" value="Bising" <?=isset($form['jantung']['auskultasi']['bottom']) && $form['jantung']['auskultasi']['bottom'] == 'Bising' ? 'checked' : ''?>> Bising
                                        </label>
                                    </div>
                                    <br>
                                    <br>
                                    <br>
                                    <p style="margin-bottom: 2px !important;"><strong>Paru</strong></p>
                                    <img src="<?=base_url('assets/img/paru.jpeg')?>" alt="" width="250">
                                    <?php
                                    sm9('form[paru][inspeksi]', 'Inspeksi')->val($form['paru']['inspeksi'] ?? '')->build();
                                    sm9('form[paru][palpasi]', 'Palpasi')->val($form['paru']['palpasi'] ?? '')->build();
                                    sm9('form[paru][perkusi]', 'Perkusi')->val($form['paru']['perkusi'] ?? '')->build();
                                    sm9('form[paru][auskultasi]', 'Auskultasi')->val($form['paru']['auskultasi'] ?? '')->build();
                                    ?>
                                    <br>
                                    <br>
                                    <p style="margin-bottom: 2px !important;"><strong>Status Lokalis</strong></p>
                                    <img src="<?=base_url('assets/img/lokalis.jpeg')?>" alt="" width="250">
                                    <?php
                                    sm9('form[lokalis][catatan]', 'Catatan')->val($form['lokalis']['catatan'] ?? '')->break_label()->build();
                                    sm9('form[lokalis][dmm]', 'Daftar Masalah Medis')->val($form['lokalis']['dmm'] ?? '')->break_label()->build();
                                    sm9('form[lokalis][dkdb]', 'Diagnosa Kerja / Diagnosa Banding')->val($form['lokalis']['dkdb'] ?? '')->break_label()->build();
                                    sm9('form[lokalis][iad]', 'Instruksi Awal Dokter')->val($form['lokalis']['iad'] ?? '')->break_label()->build();
                                    br();
                                    br();
                                    sel('diagnosis_jenis_penyakit[]', 'Diagnosa ICD-10')
                                        ->placeholder('Pilih Diagnosa ICD-10')
                                        ->options($penyakit)
                                        ->selectedOptions($penyakit)
                                        ->display(function ($value) {
                                            return $value->kode . ' - ' . $value->nama;
                                        })
                                        ->build();
                                    sm9('diagnosis', 'Diagnosa')->val($pemeriksaan['diagnosis'])->build();

                                    sel('tindakan[]', 'Tarif / Tindakan')
                                        ->placeholder('Pilih tindakan untuk pasien')
                                        ->options($tindakan)
                                        ->display(function ($value) use (&$pendaftaran) {
                                            return $value->nama . " - Rp." . number_format($value->tarif_pasien, 2, ',', '.');
                                        })
                                        ->selectedOptions($tindakan)
                                        ->build();

                                    ?>
                                    <?php
                                    sel('form[dokter][]', 'Nama Dokter Pemeriksa')
                                        ->placeholder('Pilih Dokter')
                                        ->options($dokter->result())
                                        ->display(function ($value) {
                                            return ucwords($value->nama);
                                        })
                                        ->selectedOptionIds($form['dokter'] ?? [])
                                        ->required()
                                        ->build();
                                    ?>
                                    <div class="text-muted well well-sm no-shadow" style="display: block;">
                                        <div class="form-group" style=" margin-top: 4px">
                                            <label for="hasil_penunjang_laboratorium" class="col-sm-4 control-label">
                                                KONSULTASI DOKTER
                                            </label>
                                        </div>
                                        <div class="row" style="margin-bottom: 14px">
                                            <div class="col-md-12">
                                                <input type="hidden" name="max_konsul" id="max_konsul" value="0">
                                                <table id="tbl-konsul" class="table table-condensed table-bordered" style="margin-bottom: 8px; table-layout:fixed;">
                                                    <tr>
                                                        <th style="width: 50%;">Tarif Tindakan</th>
                                                        <th>Dokter</th>
                                                    </tr>
                                                    <?php foreach ($tindakan_konsul as $v) : ?>
                                                        <tr>
                                                            <td><?=$v->nama?></td>
                                                            <td><?=$v->nama_dokter?></td>
                                                        </tr>
                                                    <?php endforeach; ?>
                                                </table>
                                            </div>
                                        </div>
                                        <?php sm9('form[catatan_konsultasi]', 'Catatan Konsultasi')->val($form['catatan_konsultasi'] ?? '')->build(); ?>
                                    </div>
                                    <?php
                                    sel('perawat[]', 'Perawat')
                                        ->placeholder('Pilih perawat')
                                        ->options($listPerawat->result())
                                        ->display(function ($value) {
                                            return ucwords($value->nama);
                                        })
                                        ->selectedOptionIds(explode(',', $pemeriksaan['detail_perawat_id']))
                                        ->required()
                                        ->build();

                                    sel('perawat_operan[]', 'Perawat Operan')
                                        ->placeholder('Pilih perawat Operan')
                                        ->options($listPerawat->result())
                                        ->display(function ($value) {
                                            return ucwords($value->nama);
                                        })
                                        ->selectedOptionIds([])
                                        ->hide()
                                        ->build();

                                    sm9('deskripsi_tindakan', 'Tata Laksana')->val($pemeriksaan['deskripsi_tindakan'])->build();
                                    sm9('saran_pemeriksaan', 'Rujukan')->val($pemeriksaan['saran_pemeriksaan'])->build();
                                    sel('status_pasien', 'Status Pasien')
                                        ->placeholder('Pilih status pasien')
                                        ->single()
                                        ->id('status_pasien')
                                        ->normal()
                                        ->options([
                                            (object) ['id' => 'Pilih', 'value' => '--Pilih status pasien--'],
                                            (object) ['id' => 'Rujuk', 'value' => 'Rujuk'],
                                            (object) ['id' => 'Rawat Jalan', 'value' => 'Rawat Jalan'],
                                            (object) ['id' => 'Meniggal', 'value' => 'Meniggal'],
                                            (object) ['id' => 'Pulang', 'value' => 'Pulang'],
                                        ])
                                        ->selectedOptionIds([$pemeriksaan['status_pasien']])
                                        ->display(function ($v) { return $v->value; })
                                        ->build();
                                    ?>
                                    <div class="row" id="c-is-pakai-ambulance" style="display: none">
                                        <div class="col-md-12">
                                            <?php

                                            sel('pakai_ambulance', 'Pakai Ambulance?')
                                                ->placeholder('Pilih pakai ambulance')
                                                ->single()
                                                ->id('pakai_ambulance')
                                                ->normal()
                                                ->options([
                                                    (object) ['id' => '0', 'value' => 'Tidak'],
                                                    (object) ['id' => '1', 'value' => 'Iya'],
                                                ])
                                                ->display(function ($v) { return $v->value; })
                                                ->build();
                                            ?>
                                        </div>
                                    </div>
                                    <div class="row" id="c-pakai-ambulance" style="display: none">
                                        <input type="hidden" name="pasien_id" value="<?=$pemeriksaan['pasien_id']?>">
                                        <input type="hidden" name="nama_pasien" value="<?=$pendaftaran['nama_pasien']?>">
                                        <input type="hidden" name="alamat_pasien" value="<?=$pendaftaran['alamat']?>">
                                        <input type="hidden" name="no_hp_pasien" value="<?=$pendaftaran['telepon']?>">
                                        <input type="hidden" name="no_jaminan" value="<?=$pendaftaran['no_jaminan']?>">
                                        <input type="hidden" name="pasien_baru" value="lama">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="" class="col-sm-3 control-label">Jenis Pendaftaran</label>
                                                <div class="col-sm-9">
                                                    <select class="form-control"
                                                            name="jaminan"
                                                            id="jaminan"
                                                            data-placeholder="--Pilih Jenis Pendaftaran--"
                                                            style="width: 100%;"
                                                    >
                                                        <option value="">--Pilih Jenis Pendaftaran--</option>
                                                        <?php foreach ($jaminan as $key => $value) { ?>
                                                            <option value="<?= $key ?>" <?=$pemeriksaan['jaminan'] == $key ? 'selected' : ''?>><?= $value['label'] ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="" class="col-sm-3 control-label">Model Kendaraan</label>
                                                <div class="col-sm-9">
                                                    <select class="form-control"
                                                            name="vehicle_id"
                                                            data-placeholder="--Pilih Jenis Pendaftaran--"
                                                            style="width: 100%;"
                                                    >
                                                        <option value="">--Pilih Ambulance--</option>
                                                        <?php foreach ($vehiclelist as $key => $vehicle) { ?>
                                                            <option value="<?php echo $vehicle["id"] ?>"><?php echo $vehicle["vehicle_model"] . " - " . $vehicle["vehicle_no"] ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <?php
                                            sm9('driver', 'Nama Sopir')->build();
                                            sm9('date', 'Tanggal')->val(date('Y-m-d'))->build();
                                            ?>
                                            <div class="form-group">
                                                <label for="" class="col-sm-3 control-label">Tujuan</label>
                                                <div class="col-sm-9">
                                                    <select class="form-control"
                                                            name="tarif_ambulance_id"
                                                            data-placeholder="--Pilih Jenis Pendaftaran--"
                                                            style="width: 100%;"
                                                    >
                                                        <option value="">--Pilih Tujuan--</option>
                                                        <?php foreach ($tarif_ambulance as $t) { ?>
                                                            <option value="<?php echo $t->id ?>"><?php echo $t->tujuan ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php
                            $thelabs = array(
                                'PL',
                                'BPJS-PL',
                                'PE',
                                'BPJS-PE',
                                'P-Sirometri',
                                'BPJS-P-Sirometri',
                                'P-Laborat',
                                'BPJS-P-Laborat'
                            );
                            if (in_array($pendaftaran['kode_daftar'], $thelabs)): ?>
                                <div class="box-footer">
                                    <button type="submit" name="submit" value="1"
                                            class="btn btn-primary btn-lg btn-flat pull-right">Simpan
                                    </button>
                                    <a href="<?= base_url() ?>pemeriksaan/listpemeriksaanPasien"
                                       class="btn btn-default btn-lg btn-flat pull-right">Batal</a>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
                <?php
                $thelabs = array(
                    'PL',
                    'BPJS-PL',
                    'PE',
                    'BPJS-PE',
                    'P-Sirometri',
                    'BPJS-P-Sirometri',
                    'P-Laborat',
                    'BPJS-P-Laborat'
                );
                if (in_array($pendaftaran['kode_daftar'], $thelabs)): ?>
                    <br>
                <?php else: ?>
                    <div class="col-sm-12 col-md-12 col-lg-5">
                        <div class="box box-primary">
                            <div class="box-body">
                                <h4 class="box-title">Obat</h4>
                                <table id="example2" class="table table-striped table-hover">
                                    <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Nama </th>
                                        <th>Jumlah</th>
                                        <th>Signa</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php $no=1; foreach ($obat_periksa as $row) { ?>
                                        <tr>
                                            <td> <?php echo $no; ?></td>
                                            <td> <?php echo ucwords($row->nama); ?></td>
                                            <td> <?php echo ucwords($row->jumlah_satuan); ?></td>
                                            <td> <?php echo ucwords($row->signa_obat); ?></td>
                                        </tr>
                                        <?php $no++; } ?>
                                    </tbody>
                                </table>
                            </div>
                            <hr>
                            <div class="box-body">
                                <h4 class="box-title">Obat Racik</h4>
                                <table id="example2" class="table table-striped table-hover">
                                    <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Nama </th>
                                        <th>Obat</th>
                                        <th>Signa</th>
                                        <th>Catatan</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php $no=1; foreach ($obat_racikan_periksa as $row) { ?>
                                        <tr>
                                            <td> <?php echo $no; ?></td>
                                            <td> <?php echo ucwords($row->nama_racikan); ?></td>
                                            <td>
                                                <table class="bor">
                                                    <thead class="bor">
                                                    <tr>
                                                        <th class="bor">Nama</th>
                                                        <th class="bor">Jumlah</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody class="bor">
                                                    <?php foreach ($row->obat as $v) : ?>
                                                        <tr>
                                                            <td class="bor"><?=$v->nama?></td>
                                                            <td class="bor"><?=$v->jumlah_satuan?></td>
                                                        </tr>
                                                    <?php endforeach; ?>
                                                    </tbody>
                                                </table>
                                            </td>
                                            <td> <?php echo ucwords($row->signa); ?></td>
                                            <td> <?php echo ucwords($row->catatan); ?></td>
                                        </tr>
                                        <?php $no++; } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>
            </div>
        </section>
    </div>
</form>

<div id='ResponseInput'></div>

<!-- Select2 -->
<script src="<?= base_url() ?>assets/bower_components/select2/dist/js/select2.full.min.js"></script>
<!-- InputMask -->

<script type="text/javascript">
    $('.select2').select2();

    function set_bmi() {
        var tb = $('#tb').val();
        var bb = $('#bb').val();
        var tbm = tb / 100;
        var bmi = bb / (tbm * tbm);

        $('#bmi').val(bmi.toFixed(2));
    }

    function loadData(i) {
        var id = $('#obat' + i).val();
        var jumlah_satuan = $('#jumlah_satuan' + i).val();
        var stok = $('#stok' + i).val();
        var urls = "<?= base_url(); ?>obat/getStokObat";
        var datax = {"id": id};

        if (parseInt(stok) < parseInt(jumlah_satuan)) {
            alert('Stok obat tidak cukup. Silahkan kurangi jumlah atau ganti obat lain!');
            $('#jumlah_satuan' + i).val('');
        }
    }

    for (var i = 1; i < 11; i++) {
        for (var j = 1; j < 3; j++) {
            function loadDataRacikan(i, j) {
                var ij = (i.toString() + j.toString());


                var id = $('#obat_racikan' + ij).val();
                var jumlah_satuan = $('#jumlah_satuan' + ij).val();
                var urls = "<?= base_url(); ?>obat/getStokObat";
                var datax = {"id": id};

                $.ajax({
                    type: 'GET',
                    url: urls,
                    data: datax,

                    success: function (stok) {
                        if (parseInt(stok) < parseInt(jumlah_satuan)) {
                            alert('Stok obat tidak cukup. Silahkan kurangi jumlah atau ganti obat lain!');
                            $('#jumlah_satuan' + ij).val('');
                        }


                    }
                });
            }
        }
    }

</script>

<script type="text/javascript">
    jQuery(function ($) {
        var bahan = <?php echo json_encode($bahan); ?>;
        $('#bahan-option').select2();

        var obat = <?php echo json_encode($obat1); ?>;
        $('#obat-option').select2();

        $('#button-add-bahan').on('click', function (e) {
            var id_bahan = $('#bahan-option').val();
            if (id_bahan == '') {
                alert('Anda belum memilih bahan habis pakai !');
            }
            else {
                var theBahan = {};
                var counter = parseInt($('#abdush-counter2').val());
                $.each(bahan, function (i, v) {
                    if (v.id == id_bahan) {
                        theBahan = v;
                        return;
                    }
                });

                var html = `
                <tr>
                  <td>
                    ` + theBahan.nama + `
                    <input type="hidden" name="id[]" value="` + theBahan.id + `">
                  </td>
                  <td>` + theBahan.jumlah + ` ` + theBahan.satuan + `</td>
                  <td>
                    <input style="width:65px;" type="text" class="form-control" name="qty[]" id="bahan[` + counter + `][qty]">
                  </td>
                  <td>
                    <button class="btn btn-sm btn-danger btn-delete-row" type="button"> <i class="fa fa-trash"></i> </button>
                  </td>
                </tr>`;

                $('.form-area tbody').append(html);
                $('#abdush-counter').val(counter + 1);
                $('input[name="bahan[' + counter + '][qty]"]').focus();

                $('.btn-delete-row').unbind('click');
                $('.btn-delete-row').each(function () {
                    $(this).on('click', function () {
                        $(this).parents('tr').remove();
                    });
                });
            }

            $('#bahan-option').val('').trigger('change');
        });

        $('#button-add-obat').on('click', function (e) {
            var id_Obat = $('#obat-option').val();
            if (id_Obat == '') {
                alert('Anda belum memilih obat !');
            }
            else {
                var theObat = {};
                var counter = parseInt($('#abdush-counter2').val());
                $.each(obat, function (i, v) {
                    if (v.id == id_Obat) {
                        theObat = v;
                        return;
                    }
                });

                var html = `
                <tr>
                  <td>
                    ` + theObat.nama + `
                    <input type="hidden" name="nama_obat[]" value="` + theObat.id + `">
                  </td>
                  <td>` + theObat.stok_obat + ` item </td>
                  <input type="hidden" id="stok` + counter + `" value="` + theObat.stok_obat + `">
                  <td>
                    <input style="width:65px;" type="text" class="form-control" onchange="loadData(` + counter + `);" name="jumlah_satuan[]" id="jumlah_satuan` + counter + `">
                  </td>
                  <td>
                    <input style="width:100px;" type="text" class="form-control"  name="signa_obat[]" id="signa_obat` + counter + `">
                  </td>
                  <td>
                    <button class="btn btn-sm btn-danger btn-delete-row" type="button"> <i class="fa fa-trash"></i> </button>
                  </td>
                </tr>

                `;

                $('.form-area-obat tbody').append(html);
                $('#abdush-counter2').val(counter + 1);
                $('input[id="jumlah_satuan' + counter + '"]').focus();

                $('.btn-delete-row').unbind('click');
                $('.btn-delete-row').each(function () {
                    $(this).on('click', function () {
                        $(this).parents('tr').remove();
                    });
                });
            }

            $('#obat-option').val('').trigger('change');
        });

        let initializer = {
            init: function() {
                for (let i = 1; i < 9; i++) {
                    $(`#obat-racik-option-${i}`).select2();
                    $(`#button-add-obat-racik-${i}`).on('click', e => {
                        this.onBtnAddClick(i);
                    });
                }
            },
            onBtnAddClick: function(i) {
                let opt = $(`#obat-racik-option-${i}`);
                let id_Obat = opt.val();
                if (id_Obat === '') {
                    alert('Anda belum memilih obat !');
                }
                else {
                    let theObat = {};
                    let counter = parseInt($(`#abdush-counter-${i}`).val());
                    $.each(obat, function (i, v) {
                        if (v.id === id_Obat) {
                            theObat = v;
                            return;
                        }
                    });

                    $(`.form-area-obat-racik-${i} tbody`).append(this.getTableRow(i, theObat, counter));
                    $(`#abdush-counter-${i}`).val(counter + 1);
                    $('input[id="jumlah_satuan_racik' + counter + '"]').focus();

                    $('.btn-delete-row').unbind('click');
                    $('.btn-delete-row').each(function () {
                        $(this).on('click', function () {
                            $(this).parents('tr').remove();
                        });
                    });
                }
                opt.val('').trigger('change');
            },
            getTableRow: (i, obat, counter) => (`
                <tr>
                  <td>
                    ` + obat.nama + `
                    <input type="hidden" name="nama_obat_racikan${i}[]" value="` + obat.id + `">
                  </td>
                  <td>` + obat.stok_obat + ` item </td>
                  <input type="hidden" id="stok` + counter + `" value="` + obat.stok_obat + `">
                  <td>
                    <input style="width:65px;" type="text" class="form-control" onchange="loadData(` + counter + `);" name="jumlah_satuan_racikan${i}[]" id="jumlah_satuan_racikan${i}` + counter + `">
                  </td>
                  <td>
                    <button class="btn btn-sm btn-danger btn-delete-row" type="button"> <i class="fa fa-trash"></i> </button>
                  </td>
                </tr>
            `)
        };

        initializer.init();
    });
</script>
