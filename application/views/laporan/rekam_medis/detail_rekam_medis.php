<link rel="stylesheet"
      href="<?php echo base_url() ?>assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
<style>
    table.bor, th.bor, td.bor {
        font-size: 12px;
        border: 1px solid #e5e5e5;
    }
    th, td {
        padding: 0px 5px;
    }
    td.cat {
        min-width: 100px;
        max-width: 300px;
    }
    td.no-data {
        padding: 5px;
        text-align: center;
    }
    .borderless td, .borderless th, .borderless tr {
        border: none !important;
    }
    .borderless td {
        padding-top: 2px !important;
        padding-bottom: 2px !important;
    }
</style>

<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Data Rekam Medis
            <br>
            <small><?=$pasien_terpilih->no_rm.' - '.$pasien_terpilih->nama?></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a href="#">Master</a></li>
            <li class="active">Data Rekam Medis</li>
        </ol>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <?php $success = $this->session->flashdata('success');
                if (!empty($success)) { ?>
                    <div class="alert alert-success alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">
                            &times;
                        </button>
                        <h4><i class="icon fa fa-check"></i> Success!</h4>
                        <?php echo $success ?>
                    </div>
                <?php } ?>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                        <li class="active">
                            <a href="#tab_1" data-toggle="tab">
                                <span style="font-size: 14px;">Riwayat Periksa Poli</span>
                            </a>
                        </li>
                        <li>
                            <a href="#tab_rawat_inap" data-toggle="tab">
                                <span style="font-size: 14px;">Riwayat Rawat Inap</span>
                            </a>
                        </li>
                        <li>
                            <a href="#tab_ruang_bersalin" data-toggle="tab">
                                <span style="font-size: 14px;">Riwayat Ruang Bersalin</span>
                            </a>
                        </li>
                        <li>
                            <a href="#tab_ruang_operasi" data-toggle="tab">
                                <span style="font-size: 14px;">Riwayat Ruang Operasi</span>
                            </a>
                        </li>
                        <li>
                            <a href="#tab_2" data-toggle="tab">
                                <span style="font-size: 14px;">Riwayat Periksa Laboratorium</span>
                            </a>
                        </li>
                        <li>
                            <a href="#tab_3" data-toggle="tab">
                                <span style="font-size: 14px;">Riwayat Periksa EKG</span>
                            </a>
                        </li>
                        <li>
                            <a href="#tab_5" data-toggle="tab">
                                <span style="font-size: 14px;">Riwayat Periksa Radiologi</span>
                            </a>
                        </li>
                        <li class="pull-right">
                            <a href="<?php echo base_url(); ?>Laporan/RekamMedis" class="btn btn-primary bg-primary"> <i
                                        class="fa fa-arrow-left"></i> Kembali</a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <?php

                        $data['penyakit'] = $penyakit;
                        $data['tindakan'] = $tindakan;
                        $data['laborat'] = [];
                        $data['ekg'] = [];
                        $data['spirometri'] = [];
                        $data['poli'] = [];

                        foreach ($pemeriksaan as $the_i => $row) {
                            if (strpos($row->nama_jenis_pendaftaran, 'aborat') !== false) {
                                $data['laborat'][] = $row;
                            }
                            else if (strpos(strtolower($row->nama_jenis_pendaftaran), 'ekg') !== false) {
                                $data['ekg'][] = $row;
                            }
                            else if (strpos(strtolower($row->nama_jenis_pendaftaran), 'spirometri') !== false) {
                                $data['spirometri'][] = $row;
                            }
                            else if (strpos(strtolower($row->nama_jenis_pendaftaran), 'radiologi') !== false) {
                                $data['radiologi'][] = $row;
                            }
                            else {
                                $data['poli'][] = $row;
                            }
                        }
                        $this->load->view('laporan/rekam_medis/riwayat_poli', $data);
                        $this->load->view('laporan/rekam_medis/riwayat_laboratorium', $data);
                        $this->load->view('laporan/rekam_medis/riwayat_rawat_inap', ['data' => $list_rawat_inap]);
                        $this->load->view('laporan/rekam_medis/riwayat_ruang_bersalin', ['data' => $list_ruang_bersalin]);
                        $this->load->view('laporan/rekam_medis/riwayat_ruang_operasi', ['data' => $list_ruang_operasi]);
                        $this->load->view('laporan/rekam_medis/riwayat_ekg', $data);
                        $this->load->view('laporan/rekam_medis/riwayat_radiologi', $data);
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<script>
    $(function () {
        $('#example1').DataTable()
        $('.example2').DataTable({
            rowReorder: {
                selector: 'td:nth-child(2)'
            },
            'paging': true,
            'lengthChange': true,
            'searching': true,
            'ordering': true,
            'info': true,
            'autoWidth': true
        })
    })
</script>
