<div class="tab-pane" id="tab_5">
    <table class="table table-striped table-hover example2">
        <thead>
        <tr>
            <th>No</th>
            <th>Tanggal Periksa</th>
            <th>Tindakan</th>
            <th>Foto Radiologi</th>
            <th>Hasil Bacaan</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($radiologi as $k => $row) : ?>
            <tr>
                <td><?= $k + 1; ?></td>
                <td><?= date('d-F-Y', strtotime($row->waktu_pemeriksaan)); ?></td>
                <td>
                    <table style="font-size: 10px;" class="bor">
                        <tbody>
                        <?php foreach ($tindakan->result() as $row3) : ?>
                            <?php if ($row->id == $row3->pemeriksaan_id) : ?>
                                <tr><td class="bor"> <?php echo $row3->nama; ?></td></tr>
                            <?php endif; ?>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                </td>
                <td>
                    <?php foreach($row->images as $v) : ?>
                        <a href="<?=base_url()?>/assets/img/hasil_pemeriksaan/<?=$v->image?>" target="_blank">
                            <img src="<?=base_url()?>/assets/img/hasil_pemeriksaan/<?=$v->image?>" alt="" height="60"/>
                        </a>
                    <?php endforeach; ?>
                </td>
                <td width="50px">
                    <a href="<?=base_url()?>Radio/detail/<?=$row->id?>" class="btn btn-warning btn-sm btn-block">
                        <i class="fa fa-arrows"></i> Detail
                    </a>
                    <a href="<?=base_url()?>Radio/print/<?=$row->id?>" class="btn btn-success btn-sm btn-block" target="_blank">
                        <i class="fa fa-print"></i> Cetak
                    </a>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
</div>
