<link rel="stylesheet" href="<?php echo base_url() ?>assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            List Pasien Non Rawat Jalan Saat Ini
            <small></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a href="#">Master</a></li>
            <li class="active">Detail Pasien Non Rawat Jalan Saat Ini</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-body">
                        <table class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <td colspan="9" class="bg-primary text-white" align="center"><strong> List Pasien Non Rawat Jalan Saat Ini (<?=$ruangan?>) </strong></td>
                            </tr>
                            <tr>
                                <th>No</th>
                                <th>Tgl Daftar</th>
                                <th>No RM</th>
                                <th>Nama Pasien</th>
                                <th>Alamat</th>
                                <th>Nama Dokter</th>
                                <th>Dari</th>
                                <?php if ($ruangan == 'Rawat Inap') : ?><th>Bed</th><?php endif; ?>
                                <th>Tipe Pasien</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $no = 1;
                            foreach ($list as $r) :
                                if (isset($r['current_place_type']) && $r['current_place_type'] != 'bangsal') continue;
                            ?>
                                <tr>
                                    <td><?=$no++?></td>
                                    <td><?=date('d-M-Y H:i', strtotime($r['created_at'] ?? $r['transfered_at']))?></td>
                                    <td><?=$r['no_rm']?></td>
                                    <td><?=$r['nama_pasien']?></td>
                                    <td><?=$r['alamat']?></td>
                                    <td><?=$r['nama_dokter']?></td>
                                    <td><?=$r['dari']?></td>
                                    <?php if ($ruangan == 'Rawat Inap') : ?>
                                        <td>
                                            <?php if ($r['current_place_type'] == 'bangsal') : ?>
                                                <?=$r['bed_name'].' - '.$r['bedgroup']?>
                                            <?php else : ?>
                                                <span class="label label-info"><?=$r['tujuan']?></span>
                                            <?php endif; ?>
                                        </td>
                                    <?php endif; ?>
                                    <td><?=$r['tipe_pasien']?></td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<script>
    $(function () {
        $('#example1').DataTable()
        $('#example2').DataTable({
            rowReorder: {
                selector: 'td:nth-child(2)'
            },
            'paging'      : true,
            'lengthChange': true,
            'searching'   : true,
            'ordering'    : true,
            'info'        : true,
            'autoWidth'   : true
        })
    })
</script>
