<?php
$u = $this->session->userdata('logged_in');
?>

<link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/iCheck/all.css">

<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Lengkapi Data Pendaftaran Pasien Online
            <small>Preview</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url() ?>Dashbord"><i class="fa fa-dashboard"></i> Dashbord</a></li>
            <li class="active">Pendaftaran</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <form class="form-horizontal" method="post" action="<?php echo base_url() ?>pendaftaran/lengkapi_pendaftaran/<?php echo $pendaftaran->id; ?>">
                <div class="col-md-12">

                    <div class="box box-danger">
                        <div class="box-header">
                            <h3 class="box-title">Data Pasien</h3>
                        </div>
                        <?php $warning = $this->session->flashdata('warning');
                        if (!empty($warning)) { ?>
                            <div class="alert alert-warning alert-dismissible">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;
                                </button>
                                <h4><i class="icon fa fa-warning"></i> Alert!</h4>
                                <?php echo $warning ?>
                            </div>
                        <?php } ?>
                        <?php $success = $this->session->flashdata('success');
                        if (!empty($success)) { ?>
                            <div class="alert alert-success alert-dismissible">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;
                                </button>
                                <h4><i class="icon fa fa-check"></i> Success!</h4>
                                <?php echo $success ?>
                            </div>
                        <?php } ?>

                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="norm" class="col-sm-4 control-label">No RM</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" id="no_rm"
                                                   name="no_rm" readonly
                                                   placeholder="No RM" value="<?php echo $pendaftaran->no_rm; ?>">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="nama" class="col-sm-4 control-label">Nama</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" id="nama" name="nama"
                                                   placeholder="Masukkan nama pasien" required
                                                   value="<?php echo $pasien->nama; ?>"
                                                   readonly
                                            >
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="tanggal_lahir" class="col-sm-4 control-label">Tanggal Lahir</label>
                                        <div class="col-sm-5">
                                            <input type="date" class="form-control" id="tanggal_lahir"
                                                   name="tanggal_lahir" required
                                                   readonly
                                                   value="<?php echo $pasien->tanggal_lahir; ?>"
                                            >
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="tempat_lahir" class="col-sm-4 control-label">Tempat Lahir</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" id="tempat_lahir"
                                                   name="tempat_lahir" placeholder="masukkan tempat lahir pasien"
                                                   required
                                                   readonly
                                                   value="<?php echo $pasien->tempat_lahir; ?>"
                                            >
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="jenis_kelamin" class="col-sm-4 control-label">Jenis Kelamin</label>
                                        <div class="col-sm-8">
                                            <label class="radio-inline"><input type="radio" value="L"
                                                                               name="jenis_kelamin" <?php if (!$pasien->jk || $pasien->jk === 'L') echo 'checked'; ?> required>Laki - laki</label>
                                            <label class="radio-inline"><input type="radio" value="P"
                                                                                <?php if ($pasien->jk && $pasien->jk === 'P') echo 'checked'; ?>
                                                                               name="jenis_kelamin"
                                                                               required>Perempuan</label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="alamat" class="col-sm-4 control-label">Alamat (Sesuai KTP)</label>
                                        <div class="col-sm-8">
                                            <textarea type="text" class="form-control" id="alamat" name="alamat"
                                                      placeholder="Masukkan alamat pasien" required readonly><?php echo $pasien->alamat; ?></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="telepon" class="col-sm-4 control-label">No. Telepon</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" id="telepon" name="telepon"
                                                   placeholder="Masukkan telepon pasien" required
                                                   readonly
                                                   value="<?php echo $pasien->telepon; ?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="pekerjaan" class="col-sm-4 control-label">Pekerjaan</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" id="pekerjaan" name="pekerjaan"
                                                   placeholder="masukkan pekerjaan Pasien" required value="<?php echo $pasien->pekerjaan; ?>">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="agama" class="col-sm-4 control-label">Agama</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" id="agama" name="agama"
                                                   placeholder="masukkan agama Pasien" required value="<?php echo $pasien->agama; ?>">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="tingkat_pendidikan" class="col-sm-4 control-label">Tingkat Pendidikan</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" id="tingkat_pendidikan" name="tingkat_pendidikan"
                                                   placeholder="masukkan tingkat pendidikan Pasien" required value="<?php echo $pasien->tingkat_pendidikan; ?>">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="penanggungjawab" class="col-sm-4 control-label">Penanggung
                                            Jawab</label>

                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" id="penanggungjawab"
                                                   name="penanggungjawab"
                                                   placeholder="Masukkan penanggung jawab dari pasien"
                                                    value="<?php echo $pasien->penanggungjawab; ?>">
                                        </div>
                                    </div>
                                    <div class="form-group hidden">
                                        <label for="asuhan" class="col-sm-4 control-label">Catatan
                                            Alergi/Lainnya</label>
                                        <!-- 'Asuhan Keperawatan' diganti 'Catatan Alergi/Lainnya' -->

                                        <div class="col-sm-8">
                                            <input type="hidden" class="form-control" id="asuhan" name="asuhan"
                                                   placeholder="Masukkan Catatan Alergi/Lainnya" required>
                                        </div>
                                    </div>
                                    <!-- 'Bio Psiko Sosial' dihapus -->
                                    <input type="hidden" class="form-control" id="biopsikososial" name="biopsikososial"
                                           placeholder="Masukkan bio psiko sosial" value="">
                                    <div class="form-group">
                                        <label for="jenis_pendaftaran" class="col-sm-4 control-label">Poli</label>

                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" id="jenis_pendaftaran"
                                                   name="jenis_pendaftaran" readonly
                                                   placeholder="Jenis Pendaftaran" value="<?php echo $pendaftaran->jenis_pendaftaran; ?>">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputtext3" class="col-sm-4 control-label">Jenis Pendaftaran</label>

                                        <div class="col-sm-8">
                                        <input type="text" class="form-control" id="jaminan"
                                               name="jaminan" readonly
                                               placeholder="Jaminan" value="<?php echo $jaminan; ?>">
                                        </div>
                                    </div>
                                    <div class="form-group" id="nomor">
                                        <label for="asuhan" class="col-sm-4 control-label">No</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" name="no_jaminan"
                                                   placeholder="Nomor">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputtext3" class="col-sm-4 control-label">Pilih Dokter</label>
                                        <div class="col-sm-8">
                                            <select class="form-control abdush-select" name="dokter" required>
                                                <option value="" selected>-- Pilih Dokter --</option>
                                                <?php foreach ($dokter->result() as $key => $value) {
                                                    ?>
                                                    <option value="<?php echo $value->id ?>"><?php echo $value->nama; ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php if ($u->nama_grup == 'front_office') : ?>
                            <div class="box-footer">
                                <button type="submit" name="submit" value="1"
                                        class="btn btn-primary btn-lg btn-flat pull-right">Daftar
                                </button>
                                <a href="<?php echo base_url() ?>pendaftaran/listPendaftaranPasien"
                                   class="btn btn-default btn-lg btn-flat pull-right">Batal</a>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
                <?php if ($u->nama_grup != 'front_office') : ?>
                    <div class="col-md-12">
                        <div class="box box-danger">
                            <div class="box-header">
                                <h3 class="box-title">Pemeriksaan Deteksi Vital</h3>
                            </div>
                            <div class="box-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="col-sm-6 col-md-6 col-lg-4 form-group">
                                            <label for="td" class="col-sm-3 control-label">TD</label>
                                            <div class="input-group col-sm-6 col-md-6 col-sm-6 col-lg-6">
                                                <input type="text" class="form-control requirable" name="td">
                                                <span class="input-group-addon">mmHg</span>
                                            </div>
                                        </div>
                                        <div class="col-sm-6 col-md-6 col-lg-4 form-group">
                                            <label for="inputEmail3" class="col-sm-3 control-label">R</label>
                                            <div class="input-group col-sm-6 col-md-6 col-sm-6 col-lg-6">
                                                <input type="text" class="form-control requirable" name="r">
                                                <span class="input-group-addon">K/Min</span>
                                            </div>
                                        </div>
                                        <div class="col-sm-6 col-md-6 col-lg-4 form-group">
                                            <label for="inputEmail3" class="col-sm-3 control-label">BB</label>
                                            <div class="input-group col-sm-6 col-md-6 col-sm-6 col-lg-6">
                                                <input type="text" class="form-control requirable" id="bb" name="bb"
                                                       onkeyup="set_bmi()">
                                                <span class="input-group-addon">Kg</span>
                                            </div>
                                        </div>
                                        <div class="col-sm-6 col-md-6 col-lg-4 form-group">
                                            <label for="inputEmail3" class="col-sm-3 control-label">N</label>
                                            <div class="input-group col-sm-6 col-md-6 col-sm-6 col-lg-6">
                                                <input type="text" class="form-control requirable" name="n">
                                                <span class="input-group-addon">K/Min</span>
                                            </div>
                                        </div>
                                        <div class="col-sm-6 col-md-6 col-lg-4 form-group">
                                            <label for="inputEmail3" class="col-sm-3 control-label">S</label>
                                            <div class="input-group col-sm-6 col-md-6 col-sm-6 col-lg-6">
                                                <input type="text" class="form-control requirable" id="s" name="s">
                                                <span class="input-group-addon">'0</span>
                                            </div>
                                        </div>
                                        <div class="col-sm-6 col-md-6 col-lg-4 form-group">
                                            <label for="inputEmail3" class="col-sm-3 control-label">TB</label>
                                            <div class="input-group col-sm-6 col-md-6 col-sm-6 col-lg-6">
                                                <input type="text" class="form-control requirable" id="tb" name="tb"
                                                       onkeyup="set_bmi()">
                                                <span class="input-group-addon">cm</span>
                                            </div>
                                        </div>
                                        <div class="col-sm-6 col-md-6 col-lg-4 form-group">
                                            <label for="inputEmail3" class="col-sm-3 control-label">BMI</label>
                                            <div class="input-group col-sm-6 col-md-6 col-sm-6 col-lg-6">
                                                <input type="text" class="form-control requirable" name="bmi" id="bmi">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="box-footer">
                                <button type="submit" name="submit" value="1"
                                        class="btn btn-primary btn-lg btn-flat pull-right">Simpan
                                </button>
                                <a href="<?php echo base_url() ?>pendaftaran/listPendaftaranPasien"
                                   class="btn btn-default btn-lg btn-flat pull-right">Batal</a>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>
            </form>
            <!-- /.col (right) -->
        </div>
        <!-- /.row -->

    </section>
    <!-- /.content -->
</div>


<div id='ResponseInput'></div>
<script type="text/javascript">

    /*function autoComplete(Lebar, KataKunci)
    {
      $('div#hasil_pencarian').hide();
      var Lebar = Lebar + 25;

      $.ajax({
        //url: "<?php //echo site_url('spp/ajax_kode'); ?>//",
    type: "POST",
    cache: false,
    data:'keyword='+KataKunci,
    dataType:'json',
    success: function(json){
      if(json.status == 1)
      {
        $('#FormSettingPenukaranPoin').find('div#hasil_pencarian').css({ 'width' : Lebar+'px' });
        $('#FormSettingPenukaranPoin').find('div#hasil_pencarian').show('fast');
        $('#FormSettingPenukaranPoin').find('div#hasil_pencarian').html(json.datanya);
      }
      if(json.status == 0)
      {
        $('#FormSettingPenukaranPoin').find('div#hasil_pencarian').html('');
      }
    }
  });
}

$(document).on('click', '#daftar-autocomplete li', function(){
  $(this).parent().parent().parent().find('input').val($(this).find('span#kodenya').html());

  $('#FormSettingPenukaranPoin').find('#daftar-autocomplete').hide();
});

document.addEventListener('keydown', autoComplete);*/
</script>
<script>
    /*function TambahSuplier()
    {
      $.ajax({
        url: $('#FormSettingPenukaranPoin').attr('action'),
        type: "POST",
        cache: false,
        data: $('#FormSettingPenukaranPoin').serialize(),
        dataType:'json',
        success: function(json){
          if(json.status == 1){
            $('.modal-dialog').removeClass('modal-lg');
            $('.modal-dialog').addClass('modal-sm');
            $('#ModalHeader').html('Sukses !');
            $('#ModalContent').html(json.pesan);
            $('#ModalFooter').html("<button type='button' class='btn btn-primary' data-dismiss='modal'>Ok</button>");
            $('#my-grid2').DataTable().ajax.reload( null, false );
          }
          else {
            $('#ResponseInput').html(json.pesan);
          }
        }
      });
    }

    $(document).ready(function(){
      var Tombol = "<button type='button' class='btn btn-primary' id='SimpanSettingPenukaranPoin'>Simpan Data</button>";
      Tombol += "<button type='button' class='btn btn-default' data-dismiss='modal'>Tutup</button>";
      $('#ModalFooter').html(Tombol);

      $("#FormSettingPenukaranPoin").find('input[type=text],textarea,select').filter(':visible:first').focus();

      $('#SimpanSettingPenukaranPoin').click(function(e){
        e.preventDefault();
        TambahSuplier();
      });

      $('#FormSettingPenukaranPoin').submit(function(e){
        e.preventDefault();
        TambahSuplier();
      });
    });*/
</script>


<!-- InputMask -->
<script src="<?php echo base_url() ?>assets/plugins/input-mask/jquery.inputmask.js"></script>
<script src="<?php echo base_url() ?>assets/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="<?php echo base_url() ?>assets/plugins/input-mask/jquery.inputmask.extensions.js"></script>

<script type="text/javascript">

    function set_bmi() {
        var tb = $('#tb').val();
        var bb = $('#bb').val();
        var tbm = tb / 100;
        var bmi = bb / (tbm * tbm);

        $('#bmi').val(bmi.toFixed(2));
    }

    $(function () {
        $('#nomor').hide();
        $('#jaminan').on('change', function (e) {
            if (this.value === 'umum') {
                $('#nomor').hide();
            }
            else {
                $('#nomor').show();
            }
        });
        $('#poli').on('change', function() {
            $('.requirable').prop('required', this.value != 19);
        });
    });

</script>
