<link rel="stylesheet" href="<?php echo base_url() ?>assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">

<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Data Pengeluaran
            <small></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a href="#">Master</a></li>
            <li class="active">Data Pengeluaran</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Data Pengeluaran</h3>&nbsp;&nbsp;
                        <a href="<?=base_url()?>Keuangan/create_pengeluaran_rutin_harian">
                            <button type="button" class="btn btn-primary"><span class="glyphicon glyphicon-plus"></span></button>
                        </a>
                    </div>

                    <?php $warning = $this->session->flashdata('warning');
                    if (!empty($warning)){ ?>
                        <div class="alert alert-warning alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <h4><i class="icon fa fa-warning"></i> Alert!</h4>
                            <?php echo $warning ?>
                        </div>
                    <?php } ?>
                    <?php $success = $this->session->flashdata('success');
                    if (!empty($success)){ ?>
                        <div class="alert alert-success alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <h4><i class="icon fa fa-check"></i> Success!</h4>
                            <?php echo $success ?>
                        </div>
                    <?php } ?>

                    <div class="box-body">
                        <table id="example2" class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>No</th>
                                <th>Tanggal</th>
                                <th>Item Pengeluaran</th>
                                <th>Keterangan</th>
                                <th>Jumlah</th>
                                <th style="width: 80px">Aksi</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $no=1; foreach ($list as $row) { ?>
                                <tr>
                                    <td> <?php echo $no; ?></td>
                                    <td> <?php echo date('d-F-Y', strtotime($row->d_date)); ?></td>
                                    <td> <?php echo ucwords($row->nama_item); ?></td>
                                    <td> <?php echo $row->keterangan; ?></td>
                                    <td> <?php echo 'Rp '.number_format($row->jumlah, 0, ',', '.'); ?></td>
                                    <td>
                                        <a href="<?=base_url()?>Keuangan/edit_pengeluaran_rutin_harian/<?=$row->id?>">
                                            <button type="button" class="btn btn-warning btn-block"><i class="fa fa-pencil"></i> Edit</button>
                                        </a>
                                        <div style="height: 4px"></div>
                                        <a onclick="return confirm('Apakah Anda ingin menghapus data ini?');" href="<?=base_url()?>Keuangan/delete_pengeluaran_rutin_harian/<?=$row->id?>">
                                            <button type="button" class="btn btn-danger btn-block"><i class="fa fa-trash"></i> Delete</button>
                                        </a>
                                    </td>
                                </tr>
                                <?php $no++; } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<script>
    $(function () {
        $('#example1').DataTable()
        $('#example2').DataTable({
            'paging'      : true,
            'lengthChange': false,
            'searching'   : false,
            'ordering'    : true,
            'info'        : true,
            'autoWidth'   : false
        })
    })
</script>