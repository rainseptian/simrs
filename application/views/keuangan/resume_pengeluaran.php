<link rel="stylesheet" href="<?= base_url() ?>assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">

<style>
    hr {
        margin-top: 0 !important;
        margin-bottom: 15px !important;
    }
</style>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Resume Pengeluaran
            <small></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a href="#">Master</a></li>
            <li class="active">Resume Pengeluaran</li>
        </ol>
    </section>

    <section class="content">
        <!--Jasa Medis-->
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title"> Filter</h3>&nbsp;&nbsp;
                    </div>
                    <div class="box-body">
                        <form class="form-horizontal" method="get" action="<?php echo base_url()?>Keuangan/resume_pengeluaran">

                            <div class="row">
                                <div class="col-sm-4">
                                    <div style="margin: 5px 10px">
                                        <label class="control-label">Waktu</label>
                                        <select class="form-control" id="jenis" name="jenis">
                                            <option value="1" <?php if (isset($jenis) && $jenis == "1") echo 'selected'?>>Hari Ini</option>
                                            <option value="2" <?php if (isset($jenis) && $jenis == "2") echo 'selected'?>>Pilih Tanggal</option>
                                            <option value="3" <?php if (isset($jenis) && $jenis == "3") echo 'selected'?>>Pilih Bulan dan Tahun</option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="row" id="jenis-2" style="display: none">
                                <div class="col-sm-4">
                                    <div style="margin: 5px 10px">
                                        <label class="control-label">Dari Tanggal</label>
                                        <input type='date' name='from' class='form-control' id='tanggal_dari' value="<?php echo ($this->input->get('from')) ? $this->input->get('from') : date('Y-m-01') ?>">
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div style="margin: 5px 10px">
                                        <label class="control-label">Sampai Tanggal</label>
                                        <input type='date' name='to' class='form-control' id='tanggal_sampai' value="<?php echo ($this->input->get('to')) ? $this->input->get('to') : date('Y-m-d') ?>">
                                    </div>
                                </div>
                            </div>

                            <div class="row" id="jenis-3" style="display: none">
                                <div class="col-sm-4">
                                    <div style="margin: 5px 10px">
                                        <label class="control-label">Bulan</label>
                                        <select class="form-control" id="bulan" name="bulan">
                                            <option value="01" <?php if (isset($bulan) && $bulan == "01") echo 'selected'?>>Januari</option>
                                            <option value="02" <?php if (isset($bulan) && $bulan == "02") echo 'selected'?>>Februari</option>
                                            <option value="03" <?php if (isset($bulan) && $bulan == "03") echo 'selected'?>>Maret</option>
                                            <option value="04" <?php if (isset($bulan) && $bulan == "04") echo 'selected'?>>April</option>
                                            <option value="05" <?php if (isset($bulan) && $bulan == "05") echo 'selected'?>>Mei</option>
                                            <option value="06" <?php if (isset($bulan) && $bulan == "06") echo 'selected'?>>Juni</option>
                                            <option value="07" <?php if (isset($bulan) && $bulan == "07") echo 'selected'?>>Juli</option>
                                            <option value="08" <?php if (isset($bulan) && $bulan == "08") echo 'selected'?>>Agustus</option>
                                            <option value="09" <?php if (isset($bulan) && $bulan == "09") echo 'selected'?>>September</option>
                                            <option value="10" <?php if (isset($bulan) && $bulan == "10") echo 'selected'?>>Oktober</option>
                                            <option value="11" <?php if (isset($bulan) && $bulan == "11") echo 'selected'?>>November</option>
                                            <option value="12" <?php if (isset($bulan) && $bulan == "12") echo 'selected'?>>Desember</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div style="margin: 5px 10px">
                                        <label class="control-label">Tahun</label>
                                        <select class="form-control" id="tahun" name="tahun">
                                            <?php $year = intval(date('Y')); for ($a = $year; $a > $year - 5; $a--) { ?>
                                                <option value="<?php echo $a?>" <?php if (isset($bulan) && $bulan == $a) echo 'selected'?>><?php echo $a?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class='row'>
                                <div class="col-sm-3">
                                    <div style="margin: 15px 10px">
                                        <div class="form-group">
                                            <div class="col-sm-7">
                                                <button type="submit" class="btn btn-primary" style='margin-left: 0px;'>Tampilkan</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title"> Resume Pengeluaran</h3>&nbsp;&nbsp;
                    </div>
                    <div class="box-body">
                        <table id="printable_table" class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>No</th>
                                <th>Tanggal</th>
                                <th>Item Pengeluaran</th>
                                <th>Keterangan</th>
                                <th>Jumlah</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $t = 0; $no=1; foreach ($list as $row) { ?>
                                <tr>
                                    <td> <?php echo $no; ?></td>
                                    <td> <?php echo date('d-F-Y', strtotime($row->d_date)); ?></td>
                                    <td> <?php echo ucwords($row->nama_item); ?></td>
                                    <td> <?php echo $row->keterangan; ?></td>
                                    <td> <?php echo 'Rp '.number_format($row->jumlah, 0, ',', '.'); ?></td>
                                </tr>
                                <?php $t += $row->jumlah; $no++; } ?>
                            </tbody>
                            <tfoot>
                            <tr>
                                <th colspan="4">Total:</th>
                                <th><?php echo 'Rp '.number_format($t, 0, ',', '.'); ?></th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>

</div>

<script src="<?= base_url() ?>assets/bower_components/select2/dist/js/select2.full.min.js"></script>

<script>
    $(function () {
        $('.select2').select2();

        const jenis = <?=isset($jenis) ? $jenis : -100?>;
        if (jenis === 2) {
            $('#jenis-2').css('display', 'block')
        }
        else if (jenis === 3) {
            $('#jenis-3').css('display', 'block')
        }

        $("#jenis").change(function () {

            $('#jenis-2').css('display', 'none')
            $('#jenis-3').css('display', 'none')

            if ($(this).val() === '2') {
                $('#jenis-2').css('display', 'block')
            }
            else if ($(this).val() === '3') {
                $('#jenis-3').css('display', 'block')
            }
        })
    })

    $(function () {
        $('#example2').DataTable()
        // $('#example2').DataTable({
        //     rowReorder: {
        //         selector: 'td:nth-child(2)'
        //     },
        //     'paging'      : true,
        //     'lengthChange': true,
        //     'searching'   : true,
        //     'ordering'    : true,
        //     'info'        : true,
        //     'autoWidth'   : true
        // })
    })
</script>
