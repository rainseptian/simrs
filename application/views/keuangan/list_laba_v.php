<link rel="stylesheet"
      href="<?= base_url() ?>assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <?php if (isset($jpem) && isset($jp_name)) { ?>
                Data Pemasukan <?= $jpem ?> <?= $jp_name ?>
            <?php } else { ?>
                Data Pemasukan <?= $tipe ?>
            <?php } ?>
            <small></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a href="#">Master</a></li>
            <li class="active">Data Pemasukan</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-warning">
                    <div class="box-header">
                        <h3 class="box-title">
                            <?php if (isset($jpem) && isset($jp_name)) { ?>
                                List Pemasukan Dari <?= $jpem ?> - Periode: <?=$periode?>
                            <?php } else { ?>
                                List Pemasukan Dari <?= $tipe ?> - Periode: <?=$periode?>
                            <?php } ?>
                        </h3>&nbsp;&nbsp;
                    </div>
                    <!-- /.box-header -->
                    <?php $warning = $this->session->flashdata('warning');
                    if (!empty($warning)) { ?>
                        <div class="alert alert-warning alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <h4><i class="icon fa fa-warning"></i> Alert!</h4>
                            <?= $warning ?>
                        </div>
                    <?php } ?>
                    <?php $success = $this->session->flashdata('success');
                    if (!empty($success)) { ?>
                        <div class="alert alert-success alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <h4><i class="icon fa fa-check"></i> Success!</h4>
                            <?= $success ?>
                        </div>
                    <?php } ?>
                    <div class="box-body">
                        <table id="printable_table" class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>Waktu Bayar</th>
                                <th>Waktu Pemeriksaan</th>
                                <th>Tipe Pasien</th>
                                <th>No RM</th>
                                <th>Pasien</th>
                                <th>Item</th>
                                <th>Jumlah</th>
                                <th>Harga</th>
                                <th>Sub Total</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $no = 1;
                            $subtotal = 0;
                            foreach ($detail->result() as $row) { ?>
                                <tr>
                                    <td><?= date_format(date_create($row->waktu_bayar), 'd-M-Y H:i'); ?></td>
                                    <td><?= date_format(date_create($row->waktu_pemeriksaan), 'd-M-Y H:i'); ?></td>
                                    <td><?=$row->tipe_pasien?></td>
                                    <td><?=$row->no_rm?></td>
                                    <td><?= ucwords($row->nama_pasien); ?></td>
                                    <td><?= ucwords($row->item); ?></td>
                                    <td><?= $row->jumlah || 1; ?></td>
                                    <td><?= $row->harga; ?></td>
                                    <td align="right"> <?= $row->subtotal; ?></td>
                                </tr>
                                <?php $subtotal += $row->subtotal; ?>
                                <?php $no++;
                            } ?>
                            <tr>
                                <td colspan="8" align="right"><strong>Total Pemasukan : </strong></td>
                                <td align="right"><strong><?= number_format($subtotal, 2, ',', '.'); ?></strong></td>
                                <td style="display: none"></td>
                                <td style="display: none"></td>
                                <td style="display: none"></td>
                                <td style="display: none"></td>
                                <td style="display: none"></td>
                                <td style="display: none"></td>
                                <td style="display: none"></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<script>
    $(function () {
        $('#example1').DataTable()
        $('#example2').DataTable({
            rowReorder: {
                selector: 'td:nth-child(2)'
            },
            'paging': true,
            'lengthChange': true,
            'searching': true,
            'ordering': true,
            'info': true,
            'autoWidth': true
        })
    })
</script>
