<link rel="stylesheet"
      href="<?= base_url() ?>assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
<link rel="stylesheet" href="<?= base_url() ?>assets/plugins/autocomplete/style-gue.css">
<style type="text/css">
    @import(

    'https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.0/css/bootstrap.min.css'
    )
    ;
</style>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Print Nota
            <small></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a href="#">Print Nota</a></li>
            <li class="active"> Pasien</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-danger">
                    <div class="box-body">
                        <h5><i class='fa fa-file-text-o fa-fw'></i> Cari Pasien</h5>
                        <hr/>

                        <form class="form-horizontal" method="post" action="<?= base_url() ?>Keuangan/printNotaAll">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-horizontal">
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">Dari Tanggal</label>
                                            <div class="col-sm-9">
                                                <input type='date' name='from' class='form-control' id='tanggal_dari' value="<?= $from ? $from : '' ?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-horizontal">
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">Sampai Tanggal</label>
                                            <div class="col-sm-9">
                                                <input type='date' name='to' class='form-control' id='tanggal_sampai' value="<?= $to ? $to: '' ?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-horizontal">
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">Nama Pasien</label>
                                            <div class="col-sm-9">
                                                <input type='text' name='nama' class='form-control' placeholder="Nama pasien" value="<?= $nama ? $nama : '' ?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class='row'>
                                <div class="col-sm-3">
                                    <div class="form-horizontal">
                                        <div class="form-group">
                                            <div class="col-sm-8">
                                                <button type="submit" class="btn btn-primary" style='margin-left: 20px;'><span class="fa fa-search"></span> Cari</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-success">
                    <?php $warning = $this->session->flashdata('warning');
                    if (!empty($warning)) { ?>
                        <div class="alert alert-warning alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;
                            </button>
                            <h4><i class="icon fa fa-warning"></i> Alert!</h4>
                            <?= $warning ?>
                        </div>
                    <?php } ?>
                    <?php $success = $this->session->flashdata('success');
                    if (!empty($success)) { ?>
                        <div class="alert alert-success alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;
                            </button>
                            <h4><i class="icon fa fa-check"></i> Success!</h4>
                            <?= $success ?>
                        </div>
                    <?php } ?>
                    <div class="box-body">
                        <h5><i class='fa fa-file-text-o fa-fw'></i> Data Pembayaran</h5>
                        <hr/>
                        <table id="example2" class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>No</th>
                                <th>Tgl Bayar</th>
                                <th>NO RM</th>
                                <th>Nama Pasien</th>
                                <th>Jenis</th>
                                <th></th>
                                <th>Nama Dokter</th>
                                <th>Aksi</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $no=1;
                            foreach ($listPemeriksaan as $row) { $det = $row->detail;?>
                                <tr>
                                    <td> <?php echo $no++; ?></td>
                                    <td> <?= date_format(date_create($row->created_at), 'd-m-Y H:i'); ?></td>
                                    <td style="width: 100px;" <?= $det->obat_luar ? 'bgcolor="#d9ffc4"' : '' ?>>
                                        <?php echo $det->no_rm; ?>
                                    </td>
                                    <td> <?php echo ucwords($det->nama_pasien); ?></td>
                                    <td> <?php echo str_replace('^^^', '<br>', $det->jenis); ?></td>
                                    <td>
                                        <small>
                                            <?php foreach (explode('^^^', $det->jaminan) as $j) {
                                                echo '<span class="label '.$jaminan[$j]['class'].'">'.$jaminan[$j]['label'].'</span><br>';
                                            } ?>
                                        </small>
                                    </td>
                                    <td> <?php echo ucwords(str_replace('^^^', '<br>', $det->nama_dokter)); ?></td>
                                    <td>
                                        <?php
                                        if ($det->obat_luar) : ?>
<!--                                            <a href="--><?php //echo base_url(); ?><!--Administrasi/nota_obat_luar/--><?php //echo $det->id; ?><!--">-->
<!--                                                <button type="button" class="btn btn-primary btn-block"><i class="fa fa-dollar"></i> Bayar</button>-->
<!--                                            </a>-->
<!--                                            <div style="margin-top: 8px"></div>-->
<!--                                            <a href="--><?php //echo base_url(); ?><!--Administrasi/edit_obat_luar/--><?php //echo $det->id; ?><!--">-->
<!--                                                <button type="button" class="btn btn-warning btn-block"><i class="fa fa-pencil"></i> Edit</button>-->
<!--                                            </a>-->
                                        <?php else : ?>
                                            <a target="_blank" href="<?php echo base_url(); ?>Administrasi/print_nota_all/<?=$det->pasien_id?>/<?=$det->jenis?>/<?=$det->id?>/<?=$row->id?>">
                                                <button type="button" class="btn btn-success btn-block"><i class="fa fa-print"></i> Print Nota</button>
                                            </a>
                                        <?php endif; ?>
                                    </td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>

                    <!-- /.box-body -->
                </div>
            </div>
        </div>
    </section>
</div>

<script>

    $(function () {
        $('#example1').DataTable()
        $('#example2').DataTable({
            'paging': true,
            'lengthChange': false,
            'searching': false,
            'ordering': true,
            'info': true,
            'autoWidth': false
        })
    })

</script>

<script>

    $('#pencarian_kode').keypress((event) => {
        if (event.keyCode == 13) {
            event.preventDefault();
        }
    });
    $('#detail_pasien').click(() => {
        window.location.href = "<?=base_url()?>Laporan/RekamMedis/" + $('#id_pasien').val();
    });

    var timer = null;
    $(document).on('keyup', '#pencarian_kode', (e) => {
        clearTimeout(timer);
        timer = setTimeout(() => {
            if ($('#pencarian_kode').val() != '') {
                var charCode = (e.which) ? e.which : event.keyCode;

                if (charCode == 40) { //arrow down

                    if ($('.form-group').find('div#hasil_pencarian li.autocomplete_active').length > 0) {
                        var selanjutnya = $('.form-group').find('div#hasil_pencarian li.autocomplete_active').next();
                        $('.form-group').find('div#hasil_pencarian li.autocomplete_active').removeClass('autocomplete_active');
                        selanjutnya.addClass('autocomplete_active');
                    } else {
                        $('.form-group').find('div#hasil_pencarian li:first').addClass('autocomplete_active');
                    }
                } else if (charCode == 38) { //arrow up

                    if ($('.form-group').find('div#hasil_pencarian li.autocomplete_active').length > 0) {
                        var sebelumnya = $('.form-group').find('div#hasil_pencarian li.autocomplete_active').prev();
                        $('.form-group').find('div#hasil_pencarian li.autocomplete_active').removeClass('autocomplete_active');
                        sebelumnya.addClass('autocomplete_active');
                    } else {
                        $('.form-group').find('div#hasil_pencarian li:first').addClass('autocomplete_active');
                    }
                } else if (charCode == 13) { // enter

                    var Kodenya = $('.form-group').find('div#hasil_pencarian li.autocomplete_active span#kodenya').html();
                    var No_rmnya = $('.form-group').find('div#hasil_pencarian li.autocomplete_active span#no_rmnya').html();
                    var Namanya = $('.form-group').find('div#hasil_pencarian li.autocomplete_active span#namanya').html();
                    if (Kodenya) {
                        $('#pencarian_kode').val(No_rmnya + ' - ' + Namanya);
                        $('#id_pasien').val(Kodenya);
                    } else {
                        alert('data tidak ada! Pilih pilihan yang ada sebelum di enter.');
                    }

                    $('.form-group').find('div#hasil_pencarian').hide();
                } else {
                    var text = $('#form_cari').find('div#hasil_pencarian').html();
                    autoComplete($('#pencarian_kode').width(), $('#pencarian_kode').val());
                }
            } else {
                $('#form_cari').find('div#hasil_pencarian').hide();
            }
        }, 100);
    });

    $(document).on('click', '#daftar-autocomplete li', function () {
        $(this).parent().parent().parent().find('#pencarian_kode').val($(this).find('span#no_rmnya').html() + ' - ' + $(this).find('span#namanya').html());
        $(this).parent().parent().parent().find('#id_pasien').val($(this).find('span#kodenya').html());

        $('.form-group').find('#daftar-autocomplete').hide();
    });

    function autoComplete(Lebar, KataKunci) {
        let $hasil_pencarian = $('div#hasil_pencarian');
        $hasil_pencarian.hide();
        var Lebar = Lebar + 25;

        $.ajax({
            url: "<?= site_url('pendaftaran/ajax_kode'); ?>",
            type: "POST",
            cache: false,
            data: 'keyword=' + KataKunci,
            dataType: 'json',
            success: function (json) {
                if (json.status == 1) {
                    $hasil_pencarian.css({'width': Lebar + 'px'});
                    $hasil_pencarian.css({'display': 'block'});
                    $hasil_pencarian.show('fast');
                    $hasil_pencarian.html(json.datanya);
                }
                if (json.status == 0) {
                    $('#form_cari').find('div#hasil_pencarian').html('');
                }
            }
        });
    }
</script>

<script src="<?= base_url() ?>assets/bower_components/Flot/jquery.flot.js"></script>
<script src="<?= base_url() ?>assets/bower_components/Flot/jquery.flot.categories.js"></script>
