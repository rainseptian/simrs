<?php
$currency_symbol = 'Rp';
?>
<link rel="stylesheet"
      href="<?php echo base_url() ?>assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
<link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/autocomplete/style-gue.css">
<style type="text/css">
    @import('https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.0/css/bootstrap.min.css');
</style>

<style type="text/css">
    #easySelectable {/*display: flex; flex-wrap: wrap;*/}
    #easySelectable li {}
    #easySelectable li.es-selected {background: #2196F3; color: #fff;}
    .easySelectable {-webkit-user-select:none; -moz-user-select:none; -ms-user-select: none;}
</style>
<div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title titlefix"> Rekapitulasi Panggilan Ambulance</h3>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-12">
                                <form class="form-horizontal" method="get" action="<?php echo base_url()?>Ambulance/rekapAmbulance">
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <div style="margin: 5px 10px">
                                                <label class="control-label">Waktu</label>
                                                <select class="form-control" id="jenis" name="jenis">
                                                    <option value="1" <?php if (isset($jenis) && $jenis == "1") echo 'selected'?>>Hari Ini</option>
                                                    <option value="2" <?php if (isset($jenis) && $jenis == "2") echo 'selected'?>>Pilih Tanggal</option>
                                                    <option value="3" <?php if (isset($jenis) && $jenis == "3") echo 'selected'?>>Pilih Bulan dan Tahun</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-4 jenis-2" style="display: none">
                                            <div style="margin: 5px 10px">
                                                <label class="control-label">Dari Tanggal</label>
                                                <input type='date' name='from' class='form-control' id='tanggal_dari' value="<?php echo ($this->input->get('from')) ? $this->input->get('from') : date('Y-m-01') ?>">
                                            </div>
                                        </div>
                                        <div class="col-sm-4 jenis-2" style="display: none">
                                            <div style="margin: 5px 10px">
                                                <label class="control-label">Sampai Tanggal</label>
                                                <input type='date' name='to' class='form-control' id='tanggal_sampai' value="<?php echo ($this->input->get('to')) ? $this->input->get('to') : date('Y-m-d') ?>">
                                            </div>
                                        </div>
                                        <div class="col-sm-4 jenis-3" style="display: none">
                                            <div style="margin: 5px 10px">
                                                <label class="control-label">Bulan</label>
                                                <select class="form-control" id="bulan" name="bulan">
                                                    <option value="01" <?php if (isset($bulan) && $bulan == "01") echo 'selected'?>>Januari</option>
                                                    <option value="02" <?php if (isset($bulan) && $bulan == "02") echo 'selected'?>>Februari</option>
                                                    <option value="03" <?php if (isset($bulan) && $bulan == "03") echo 'selected'?>>Maret</option>
                                                    <option value="04" <?php if (isset($bulan) && $bulan == "04") echo 'selected'?>>April</option>
                                                    <option value="05" <?php if (isset($bulan) && $bulan == "05") echo 'selected'?>>Mei</option>
                                                    <option value="06" <?php if (isset($bulan) && $bulan == "06") echo 'selected'?>>Juni</option>
                                                    <option value="07" <?php if (isset($bulan) && $bulan == "07") echo 'selected'?>>Juli</option>
                                                    <option value="08" <?php if (isset($bulan) && $bulan == "08") echo 'selected'?>>Agustus</option>
                                                    <option value="09" <?php if (isset($bulan) && $bulan == "09") echo 'selected'?>>September</option>
                                                    <option value="10" <?php if (isset($bulan) && $bulan == "10") echo 'selected'?>>Oktober</option>
                                                    <option value="11" <?php if (isset($bulan) && $bulan == "11") echo 'selected'?>>November</option>
                                                    <option value="12" <?php if (isset($bulan) && $bulan == "12") echo 'selected'?>>Desember</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-4 jenis-3" style="display: none">
                                            <div style="margin: 5px 10px">
                                                <label class="control-label">Tahun</label>
                                                <select class="form-control" id="tahun" name="tahun">
                                                    <?php $year = intval(date('Y')); for ($a = $year; $a > $year - 5; $a--) { ?>
                                                        <option value="<?php echo $a?>" <?php if (isset($bulan) && $bulan == $a) echo 'selected'?>><?php echo $a?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class='row'>
                                        <div class="col-sm-3">
                                            <div style="margin: 15px 10px">
                                                <div class="form-group">
                                                    <div class="col-sm-7">
                                                        <button type="submit" class="btn btn-primary" style='margin-left: 0px;'>Tampilkan</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="box-body">
                        <table class="table table-striped table-bordered table-hover" id="printable_table" cellspacing="0" width="100%">
                            <thead>
                            <tr>
                                <th>No</th>
                                <th>Tanggal</th>
                                <th>Nama Pasien</th>
                                <th>Alamat Pasien</th>
                                <th>No Telp Pasien</th>
                                <th>Ambulance</th>
                                <th>Driver</th>
                                <th>Tujuan</th>
                                <th>Biaya</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            foreach ($list as $k => $r) : ?>
                                <tr>
                                    <td><?=++$k?></td>
                                    <td><?=date('d-m-Y', strtotime($r->date))?></td>
                                    <td><?=$r->nama_pasien?></td>
                                    <td><?=$r->alamat_pasien?></td>
                                    <td><?=$r->no_telp_pasien?></td>
                                    <td><?=$r->ambulance?></td>
                                    <td><?=$r->driver_name?></td>
                                    <td><?=$r->tujuan?></td>
                                    <td>Rp <?=number_format($r->tarif, 2, ',', '.')?></td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<script>
    $(function () {
        const jenis = <?=isset($jenis) ? $jenis : -100?>;
        if (jenis === 2) {
            $('.jenis-2').css('display', 'block')
        }
        else if (jenis === 3) {
            $('.jenis-3').css('display', 'block')
        }

        $("#jenis").change(function () {

            $('.jenis-2').css('display', 'none')
            $('.jenis-3').css('display', 'none')

            if ($(this).val() === '2') {
                $('.jenis-2').css('display', 'block')
            }
            else if ($(this).val() === '3') {
                $('.jenis-3').css('display', 'block')
            }
        })
    })
</script>