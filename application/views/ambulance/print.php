<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Smart Hospital</title>
    <link rel="icon" href="<?=base_url()?>/assets/img/profil/logo.png" type="image/gif">
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/bower_components/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/bower_components/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.1/css/all.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.1/css/v4-shims.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/bower_components/Ionicons/css/ionicons.min.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/bower_components/select2/dist/css/select2.min.css">

    <!-- Theme style -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/dist/css/skins/_all-skins.min.css">

    <!-- jvectormap -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/bower_components/jvectormap/jquery-jvectormap.css">
    <!-- Date Picker -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
    <!-- Daterange picker -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/bower_components/bootstrap-daterangepicker/daterangepicker.css">
    <!-- bootstrap wysihtml5 - text editor -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Google Font -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    <!-- Javascript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/mustache.js/2.3.0/mustache.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.bundle.min.js"></script>

    <link rel="stylesheet" href="<?php echo base_url() ?>assets/bower_components/toast-alert/toastr.css">
    <script src="<?php echo base_url(); ?>assets/bower_components/toast-alert/toastr.js"></script>
    <script src="<?php echo base_url(); ?>assets/dist/js/school-custom.js"></script>
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/style-main.css">
</head>
<body>


<link rel="stylesheet"
      href="<?= base_url() ?>assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">

<div class="">
    <section class="invoice">
        <div style="text-align: center; width: 100%;">
            <strong><?=$klinik->nama?></strong><br><?=$klinik->alamat?><br><?=$klinik->telepon?>
            <hr style="width: 100%;">
        </div>
        <br>

        <table class="table table-bordered table-condensed">
            <tr>
                <td>Nama</td>
                <td><?=$bayar->nama_pasien?></td>
            </tr>
            <tr>
                <td>Alamat</td>
                <td><?=$bayar->alamat_pasien?></td>
            </tr>
            <tr>
                <td>No Telp</td>
                <td><?=$bayar->no_telp_pasien?></td>
            </tr>
            <tr>
                <td>Ambulance</td>
                <td><?=$bayar->ambulance?></td>
            </tr>
            <tr>
                <td>Tujuan</td>
                <td><?=$bayar->tujuan?></td>
            </tr>
            <tr>
                <td>Driver</td>
                <td><?=$bayar->driver_name?></td>
            </tr>
            <tr>
                <td>Tanggal</td>
                <td><?=$bayar->date?></td>
            </tr>
            <tr>
                <td>Tarif</td>
                <td><?=$bayar->tarif?></td>
            </tr>
            <tr>
                <td>Diskon</td>
                <td><?=$bayar->diskon?></td>
            </tr>
            <tr>
                <td>Total</td>
                <td><?=$bayar->total?></td>
            </tr>
            <tr>
                <td>Bayar</td>
                <td><?=$bayar->bayar?></td>
            </tr>
            <tr>
                <td>Kembalian</td>
                <td><?=$bayar->kembalian?></td>
            </tr>
        </table>

    </section>
    <div class="clearfix"></div>
</div>

</body>
</html>
