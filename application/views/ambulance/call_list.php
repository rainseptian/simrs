<?php
$currency_symbol = 'Rp';
?>
<link rel="stylesheet"
      href="<?php echo base_url() ?>assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
<link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/autocomplete/style-gue.css">
<style type="text/css">
    @import('https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.0/css/bootstrap.min.css');
</style>

<style type="text/css">
    #easySelectable {/*display: flex; flex-wrap: wrap;*/}
    #easySelectable li {}
    #easySelectable li.es-selected {background: #2196F3; color: #fff;}
    .easySelectable {-webkit-user-select:none; -moz-user-select:none; -ms-user-select: none;}
</style>
<div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title titlefix"> List Panggilan Ambulance</h3>
                        <div class="box-tools pull-right">
                            <a data-toggle="modal" onclick="holdModal('myModal')" class="btn btn-primary btn-sm ambulancecall"><i class="fa fa-plus"></i> Tambah Panggilan Ambulance</a>
                        </div>
                    </div><!-- /.box-header -->
                    <?php $warning = $this->session->flashdata('warning');
                    if (!empty($warning)){ ?>
                        <div class="alert alert-warning alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <h4><i class="icon fa fa-warning"></i> Alert!</h4>
                            <?php echo $warning ?>
                        </div>
                    <?php } ?>
                    <?php $success = $this->session->flashdata('success');
                    if (!empty($success)){ ?>
                        <div class="alert alert-success alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <h4><i class="icon fa fa-check"></i> Success!</h4>
                            <?php echo $success ?>
                        </div>
                    <?php } ?>
                    <div class="box-body">
                        <table class="table table-striped table-bordered table-hover example" cellspacing="0" width="100%">
                            <thead>
                            <tr>
                                <th>No</th>
                                <th>Tanggal</th>
                                <th>NO RM</th>
                                <th>Nama Pasien</th>
                                <th>Alamat Pasien</th>
                                <th>No Telp Pasien</th>
                                <th>Ambulance</th>
                                <th>Tujuan</th>
                                <th>Driver</th>
                                <th>Aksi</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            foreach ($list as $k => $r) : ?>
                                <tr>
                                    <td><?=++$k?></td>
                                    <td><?=date('d-m-Y', strtotime($r->date))?></td>
                                    <td style="width: 100px;">
                                        <?php echo $r->no_rm; ?><br>
                                        <small>
                                            <span class="label <?= $jaminan[$r->jaminan]['class'] ?>"><?= $jaminan[$r->jaminan]['label'] ?></span>
                                            <?php if (!isset($jaminan[$r->jaminan])) { ?>
                                                <span class="label label-warning">Umum</span>
                                            <?php } ?>
                                        </small>
                                    </td>
                                    <td><?=$r->nama_pasien?></td>
                                    <td><?=$r->alamat_pasien?></td>
                                    <td><?=$r->no_telp_pasien?></td>
                                    <td><?=$r->ambulance?></td>
                                    <td><?=$r->tujuan?></td>
                                    <td><?=$r->driver_name?></td>
                                    <td>
                                        <button data-toggle="modal" data-panggilanid="<?=$r->id?>" id="bayar_btn_<?=$r->id?>" class="btn btn-success">
                                            <span class="fa fa-dollar"></span> Bayar
                                        </button>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<div class="modal fade" id="myModal" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content modal-media-content">
            <form id="formcall" method="post" accept-charset="utf-8">
                <div class="modal-header modal-media-header">
                    <button type="button" class="close pt4" data-dismiss="modal">&times;</button>
                    <div class="row">
                        <div class="col-sm-8 col-xs-8">
                            <div class="form-group" id="FormStockOpname">
                                <div>
                                    <label for="cari" class="control-label">Cari Pasien</label>
                                    <br>
                                    <input type="text" class="form-control" name="pencarian_kode" id="pencarian_kode"
                                           placeholder="Masukkan Nama atau No RM" autocomplete="off">
                                    <input type="hidden" name="id_pasien" id='id_pasien'>
                                    <div id='hasil_pencarian'></div>
                                </div>
                                <span class="text-danger"><?php echo form_error('refference'); ?></span>
                            </div>
                        </div><!--./col-sm-8-->
                        <div class="col-sm-4 col-xs-4">
                            <div class="form-group15 pull-right" style="margin-top: 8px; margin-right: 20px">
                                <a data-toggle="modal" id="add" onclick="holdModal('pasienBaruModal')" class="modalbtnpatient btn btn-sm btn-primary"><i class="fa fa-plus"></i> <span>Pasien Baru</span></a>
                            </div>
                        </div><!--./col-sm-4-->
                    </div><!-- ./row -->
                    <hr>
                    <div class="row" id="selectedPasien">
                        <div class="col-sm-4 col-xs-4">
                            <label for="exampleInputEmail1">No RM</label>
                            <input form="formcall" type="text" class="form-control" readonly id="no_rm_pasien" name="no_rm_pasien">
                        </div>
                        <div class="col-sm-4 col-xs-4">
                            <label for="exampleInputEmail1">Nama Pasien</label>
                            <input form="formcall" type="text" class="form-control" readonly id="nama_pasien" name="nama_pasien">
                            <input form="formcall" type="hidden" class="form-control" id="pasien_id" name="pasien_id" value="0">
                            <input form="formcall" type="hidden" class="form-control" id="pasien_baru" name="pasien_baru">
                            <input form="formcall" type="hidden" class="form-control" id="no_hp_pasien" name="no_hp_pasien">
                        </div>
                        <div class="col-sm-4 col-xs-4">
                            <label for="exampleInputEmail1">Alamat Pasien</label>
                            <input form="formcall" type="text" class="form-control" readonly id="alamat_pasien" name="alamat_pasien">
                        </div>
                    </div>
                </div>
                <div class="modal-body pt0 pb0">
                    <div class="ptt10">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="inputtext3">Jenis Pendaftaran</label>
                                    <select id="jaminan" class="form-control" name="jaminan" required>
                                        <option value="">--Pilih Jenis Pendaftaran--</option>
                                        <?php foreach ($jaminan as $key => $value) { ?>
                                            <option value="<?= $key ?>"><?= $value['label'] ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group" id="nomor">
                                    <label for="asuhan" class="control-label">No</label>
                                    <input type="text" class="form-control" name="no_jaminan" placeholder="Nomor">
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="exampleInputEmail1"><?php echo $this->lang->line('vehicle_model'); ?></label><small class="req"> *</small>
                                    <select name="vehicle_id" id="vehicle_no" class="form-control" onchange="getVehicleDetail(this.value)" required>
                                        <option value="">--Pilih Ambulance--</option>
                                        <?php foreach ($vehiclelist as $key => $vehicle) {
                                            ?>
                                            <option value="<?php echo $vehicle["id"] ?>"><?php echo $vehicle["vehicle_model"] . " - " . $vehicle["vehicle_no"] ?></option>
                                        <?php } ?>
                                    </select>
                                    <span class="text-danger"><?php echo form_error('vehicle_no'); ?></span>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="exampleInputEmail1"><?php echo $this->lang->line('driver_name'); ?></label>
                                    <input name="driver" id="driver_search"  type="text" class="form-control" required/>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="exampleInputEmail1"><?php echo $this->lang->line('date'); ?></label><small class="req"> *</small>
                                    <input name="date" type="date" class="form-control datetime" value="<?=date('Y-m-d')?>" required/>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Tujuan</label><small class="req"> *</small>
                                    <select name="tarif_ambulance_id" id="tarif_ambulance_id" class="form-control" required>
                                        <option value="">--Pilih Tujuan--</option>
                                        <?php foreach ($tarif_ambulance as $t) { ?>
                                            <option value="<?php echo $t->id ?>"><?php echo $t->tujuan ?></option>
                                        <?php } ?>
                                    </select>
                                    <span class="text-danger"><?php echo form_error('vehicle_no'); ?></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    <button type="submit" data-loading-text="Memproses..." id="formcallbtn" class="btn btn-success pull-right" form="formcall">Submit</button>
                    <div class="pull-right" style="margin-right:10px;">
                        <button type="button" data-dismiss="modal" class="btn btn-warning pull-right">Cancel</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade" id="pasienBaruModal" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content modal-media-content">
            <form id="form_new_pasien" method="post" accept-charset="utf-8">
                <div class="modal-header modal-media-header">
                    <button type="button" class="close pt4" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Tambah Pasien Baru</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-12 col-md-6">
                            <div class="form-group">
                                <label for="norm" class="control-label">No RM</label>
                                <input type="text" class="form-control" id="no_rm" name="no_rm"
                                       placeholder="Masukkan no rm pasien"
                                       required>
                            </div>
                            <div class="form-group">
                                <label for="nama" class="control-label">Nama</label>
                                <input type="text" class="form-control" id="nama" name="nama"
                                       placeholder="Masukkan nama pasien" required>
                            </div>
                            <div class="form-group">
                                <label for="tanggal_lahir" class="control-label">Tanggal Lahir</label>
                                <input type="date" class="form-control" id="tanggal_lahir"
                                       name="tanggal_lahir">
                            </div>
                            <div class="form-group">
                                <label for="tempat_lahir" class="control-label">Tempat Lahir</label>
                                <input type="text" class="form-control" id="tempat_lahir"
                                       name="tempat_lahir" placeholder="masukkan tempat lahir pasien">
                            </div>
                            <div class="form-group">
                                <label for="jenis_kelamin" class="control-label">Jenis Kelamin</label><br>
                                <label class="radio-inline"><input type="radio" value="L"
                                                                   name="jenis_kelamin" required>Laki - laki</label>
                                <label class="radio-inline"><input type="radio" value="P"
                                                                   name="jenis_kelamin"
                                                                   required>Perempuan</label>
                            </div>
                            <div class="form-group">
                                <label for="alamat" class="control-label">Alamat (Sesuai KTP)</label>
                                <textarea type="text" class="form-control" id="alamat" name="alamat"
                                          placeholder="Masukkan alamat pasien" required></textarea>
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-6">
                            <div class="form-group">
                                <label for="telepon" class="control-label">No. Telepon</label>
                                <input type="text" class="form-control" id="telepon" name="telepon"
                                       placeholder="Masukkan telepon pasien" required>
                            </div>
                            <div class="form-group">
                                <label for="pekerjaan" class="control-label">Pekerjaan</label>
                                <input type="text" class="form-control" id="pekerjaan" name="pekerjaan"
                                       placeholder="masukkan pekerjaan Pasien">
                            </div>
                            <div class="form-group">
                                <label for="agama" class="control-label">Agama</label>
                                <input type="text" class="form-control" id="agama" name="agama"
                                       placeholder="masukkan agama Pasien">
                            </div>
                            <div class="form-group">
                                <label for="tingkat_pendidikan" class="control-label">Tingkat Pendidikan</label>
                                <input type="text" class="form-control" id="tingkat_pendidikan" name="tingkat_pendidikan"
                                       placeholder="masukkan tingkat pendidikan Pasien">
                            </div>
                            <div class="form-group">
                                <label for="penanggungjawab" class="control-label">Penanggung Jawab</label>
                                <input type="text" class="form-control" id="penanggungjawab"
                                       name="penanggungjawab"
                                       placeholder="Masukkan penanggung jawab dari pasien">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" data-loading-text="Menambahkan..." id="submit_new_pasien" class="btn btn-info pull-right">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade" id="bayarModal" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content modal-media-content">
            <form id="bayar_form" method="post" accept-charset="utf-8">
                <div class="modal-header modal-media-header">
                    <button type="button" class="close pt4" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Bayar Ambulance</h4>
                </div>
                <div class="modal-body">
                    <input type="hidden" class="form-control" id="panggilan_id" name="panggilan_id" readonly>
                    <input type="hidden" class="form-control" id="bayar_nama" name="bayar_nama" readonly>
                    <input type="hidden" class="form-control" id="bayar_alamat" name="bayar_alamat" readonly>
                    <input type="hidden" class="form-control" id="bayar_no_telp" name="bayar_no_telp" readonly>
                    <input type="hidden" class="form-control" id="bayar_ambulance" name="bayar_ambulance" readonly>
                    <input type="hidden" class="form-control" id="bayar_tujuan" name="bayar_tujuan" readonly>
                    <input type="hidden" class="form-control" id="bayar_tarif" name="bayar_tarif" readonly>
                    <input type="hidden" class="form-control" id="bayar_driver" name="bayar_driver" readonly>
                    <input type="hidden" class="form-control" id="bayar_tanggal" name="bayar_tanggal" readonly>
                    <table class="table table-bordered table-condensed">
                        <tr>
                            <td>Nama</td>
                            <td id="td_bayar_nama"></td>
                        </tr>
                        <tr>
                            <td>Alamat</td>
                            <td id="td_bayar_alamat"></td>
                        </tr>
                        <tr>
                            <td>No Telp</td>
                            <td id="td_bayar_no_telp"></td>
                        </tr>
                        <tr>
                            <td>Ambulance</td>
                            <td id="td_bayar_ambulance"></td>
                        </tr>
                        <tr>
                            <td>Tujuan</td>
                            <td id="td_bayar_tujuan"></td>
                        </tr>
                        <tr>
                            <td>Driver</td>
                            <td id="td_bayar_driver"></td>
                        </tr>
                        <tr>
                            <td>Tanggal</td>
                            <td id="td_bayar_tanggal"></td>
                        </tr>
                        <tr>
                            <td>Tarif</td>
                            <td id="td_bayar_tarif"></td>
                        </tr>
                        <tr>
                            <td>Diskon</td>
                            <td>
                                <input type="text" name="diskon" id="diskon" class="form-control">
                            </td>
                        </tr>
                        <tr>
                            <td>Total</td>
                            <td>
                                <input type="text" name="total" id="total" class="form-control" readonly>
                            </td>
                        </tr>
                        <tr>
                            <td>Bayar</td>
                            <td>
                                <input type="text" name="bayar" id="bayar" class="form-control">
                            </td>
                        </tr>
                        <tr>
                            <td>Kembalian</td>
                            <td>
                                <input type="text" name="kembalian" id="kembalian" class="form-control" readonly>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="submit"
                            data-loading-text="Memproses..."
                            data-success-text="Berhasil"
                            id="bayar_submit"
                            class="btn btn-info pull-right"><span class="fa fa-dollar"></span> Bayar</button>
                    <button type="button" id="bayar_print" class="btn btn-success pull-right margin-r-5"><span class="fa fa-print"></span> Cetak</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade" id="editModal" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content modal-media-content">
            <div class="modal-header modal-media-header">
                <button type="button" class="close pt4" data-dismiss="modal">&times;</button>
                <div class="row">
                    <div class="col-sm-8">
                        <div>
                            <select onchange="get_PatienteditDetails(this.value)"  style="width: 100%" class="form-control select2" id="addpatientid" name='patient_id' >
                                <option value=""><?php echo $this->lang->line('select') . " " . $this->lang->line('patient') ?></option>
                                <?php foreach ($patients as $dkey => $dvalue) { ?>
                                    <option value="<?php echo $dvalue["id"]; ?>" <?php
                                    if ((isset($patient_select)) && ($patient_select == $dvalue["id"])) {
                                        echo "selected";
                                    }
                                    ?>><?php echo $dvalue["patient_name"] . " ( " . $dvalue["patient_unique_id"] . ")" ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div><!--./col-sm-9-->
                </div><!--./row-->
            </div>
            <form  id="formedit" method="post" accept-charset="utf-8">
                <div class="modal-body pt0 pb0">
                    <div class="ptt10">
                        <div class="row">
                            <input name="patient_name" id="patienteditid" type="hidden" class="form-control" value="<?php echo set_value('patient_name'); ?>"/>
                            <input  name="id" id="id" type="hidden" class="form-control" value="<?php echo set_value('id'); ?>" />
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="exampleInputEmail1"><?php echo $this->lang->line('vehicle_no'); ?></label><small class="req"> *</small>
                                    <select name="vehicle_no"  style="width: 100%" id="vehicleno" class="form-control" onchange="getVehicleDetail(this.value, 'vehicle_model', 'driver_name')">
                                        <option value=""><?php echo $this->lang->line('select') ?></option>
                                        <?php foreach ($vehiclelist as $key => $vehicle) {
                                            ?>
                                            <option value="<?php echo $vehicle["id"] ?>"><?php echo $vehicle["vehicle_model"] . " - " . $vehicle["vehicle_no"] ?></option>
                                        <?php } ?>
                                    </select>
                                    <span class="text-danger"><?php echo form_error('vehicle_model'); ?></span>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="exampleInputEmail1"><?php echo $this->lang->line('driver_name'); ?></label>
                                    <input name="driver_name" id="driver_name" type="text" class="form-control" value="<?php echo set_value('vehicle_model'); ?>"/>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="exampleInputEmail1"><?php echo $this->lang->line('date'); ?></label><small class="req"> *</small>
                                    <input name="date" id="edit_date" type="text" class="form-control datetime" value="<?php echo set_value('amount'); ?>"/>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="exampleInputEmail1"><?php echo $this->lang->line('amount') . " (" . $currency_symbol . ")"; ?> <small class="req"> *</small></label>
                                    <input name="amount" id="amount" type="text" class="form-control" value="<?php echo set_value('amount'); ?>"/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    <button type="submit" data-loading-text="<?php echo $this->lang->line('processing') ?>" id="formeditbtn" class="btn btn-info pull-right"><?php echo $this->lang->line('save'); ?></button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="viewModalBill"  role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content modal-media-content">
            <div class="modal-header modal-media-header">
                <button type="button" class="close" data-toggle="tooltip" title="<?php echo $this->lang->line('clase'); ?>" data-dismiss="modal">&times;</button>
                <div class="modalicon">
                    <div id='edit_deletebill'>
                        <a href="#"  data-target="#edit_prescription"  data-toggle="modal" title="" data-original-title="<?php echo $this->lang->line('edit'); ?>"><i class="fa fa-pencil"></i></a>
                        <a href="#" data-toggle="tooltip" title="" data-original-title="<?php echo $this->lang->line('delete'); ?>"><i class="fa fa-trash"></i></a>
                    </div>
                </div>
                <h4 class="box-title"><?php echo $this->lang->line('bill') . " " . $this->lang->line('details'); ?></h4>
            </div>
            <div class="modal-body pt0 pb0">
                <div id="reportdata"></div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $('#pencarian_kode').keypress(function (event) {
        if (event.keyCode === 13) {
            event.preventDefault();
        }
    });
    $(document).ready(function () {
        $("#form_new_pasien").on('submit', function (e) {
            $("#submit_new_pasien").button('loading');
            e.preventDefault();
            $.ajax({
                url: '<?php echo base_url(); ?>Ambulance/addPasien',
                type: "POST",
                data: new FormData(this),
                dataType: 'json',
                contentType: false,
                cache: false,
                processData: false,
                success: function (data) {
                    if (data.status === "fail") {
                        errorMsg(data.message);
                    } else {
                        successMsg(data.message);
                        $('#pasienBaruModal').modal('toggle');
                        $('#pasien_id').val(data.pasien_id);
                        $('#pasien_baru').val('baru');
                        $('#nama_pasien').val(data.nama);
                        $('#alamat_pasien').val(data.alamat);
                        $('#no_hp_pasien').val(data.no_telp);
                        $('#no_rm_pasien').val(data.no_rm);
                        $('#selectedPasien').show();
                    }
                    $("#submit_new_pasien").button('reset');
                },
                error: function (e) {
                    console.log(e)
                }
            });
        });
        $("#formcall").on('submit', function (e) {
            if ($('#pasien_id').val() === '0') {
                alert('Pilih pasien atau tambah pasien baru dulu')
                return false;
            }

            $("#formcallbtn").button('loading');
            e.preventDefault();
            $.ajax({
                url: '<?php echo base_url(); ?>Ambulance/addCallAmbulance',
                type: "POST",
                data: new FormData(this),
                dataType: 'json',
                contentType: false,
                cache: false,
                processData: false,
                success: function (data) {
                    if (data.status === "fail") {
                        errorMsg(data.message);
                    } else {
                        successMsg(data.message);
                        window.location.reload(true);
                    }
                    $("#formcallbtn").button('reset');
                },
                error: function (e) {
                    console.log(e)
                }
            });
        });

        const panggilans = <?php echo json_encode($list);?>;
        let currentPanggilan = {};
        let bayarId = '';

        panggilans.forEach(p => {
            $('#bayar_btn_'+p.id).click(function () {
                currentPanggilan = p;

                $('#panggilan_id').val(p.id);
                $('#bayar_nama').val(p.nama_pasien);
                $('#bayar_alamat').val(p.alamat_pasien);
                $('#bayar_no_telp').val(p.no_telp_pasien);
                $('#bayar_ambulance').val(p.ambulance);
                $('#bayar_tujuan').val(p.tujuan);
                $('#bayar_tarif').val(p.tarif);
                $('#bayar_driver').val(p.driver_name);
                $('#bayar_tanggal').val(p.date);
                $('#td_bayar_nama').html(p.nama_pasien);
                $('#td_bayar_alamat').html(p.alamat_pasien);
                $('#td_bayar_no_telp').html(p.no_telp_pasien);
                $('#td_bayar_ambulance').html(p.ambulance);
                $('#td_bayar_tujuan').html(p.tujuan);
                $('#td_bayar_tarif').html(p.tarif);
                $('#td_bayar_driver').html(p.driver_name);
                $('#td_bayar_tanggal').html(p.date);
                $('#total').val(p.tarif);

                holdModal('bayarModal');
            });
        });

        $('#diskon').keyup(function () {
            const diskon = parseInt($(this).val()) || 0;
            const total = parseInt(currentPanggilan.tarif) - diskon;
            $('#total').val(total);
            calculateKembalian();
        });
        $('#bayar').keyup(function () {
            calculateKembalian();
        });
        $('#bayar_print').hide().click(function () {
            console.log('<?php echo base_url(); ?>Ambulance/printPembayaranAmbulance/' + bayarId);
            $.ajax({
                url: '<?php echo base_url(); ?>Ambulance/printPembayaranAmbulance/' + bayarId,
                type: "POST",
                dataType: "html",
                contentType: false,
                cache: false,
                processData: false,
                success: function (data) {
                    console.log(data);
                    const myWindow = window.open('', '', 'width=200,height=100');
                    myWindow.document.write(data);
                    myWindow.document.close();
                    myWindow.focus();
                    myWindow.print();
                    window.location.reload(true);
                },
                error: function (e) {
                    console.log(e)
                }
            });
        });

        function calculateKembalian() {
            const total = parseInt($('#total').val()) || 0;
            const bayar = parseInt($('#bayar').val()) || 0;

            if (bayar !== 0) {
                const kembalian = bayar - total;
                $('#kembalian').val(kembalian);
            }
            else {
                $('#kembalian').val('');
            }
        }

        $("#bayar_form").on('submit', function (e) {
            $("#bayar_submit").button('loading');
            e.preventDefault();
            $.ajax({
                url: '<?php echo base_url(); ?>Ambulance/bayarAmbulance',
                type: "POST",
                data: new FormData(this),
                dataType: 'json',
                contentType: false,
                cache: false,
                processData: false,
                success: function (data) {
                    if (data.status === "fail") {
                        errorMsg(data.message);
                    } else {
                        successMsg(data.message);
                        bayarId = data.bayar_id;
                    }
                    $("#bayar_submit").removeClass('btn-info').addClass('btn-secondary').prop('disabled', true).button('success');
                    $('#bayar_print').show();
                },
                error: function (e) {
                    console.log(e)
                }
            });
        });
    })
</script>
<script>
    var timer = null;
    $(document).on('keyup', '#pencarian_kode', function (e) {
        if($('#pencarian_kode').val().length > 0) {
            clearTimeout(timer);
            timer = setTimeout(
                function () {
                    if ($('#pencarian_kode').val() != '') {
                        var charCode = ( e.which ) ? e.which : event.keyCode;
                        if (charCode == 40) //arrow down
                        {

                            if ($('.form-group').find('div#hasil_pencarian li.autocomplete_active').length > 0) {
                                var selanjutnya = $('.form-group').find('div#hasil_pencarian li.autocomplete_active').next();
                                $('.form-group').find('div#hasil_pencarian li.autocomplete_active').removeClass('autocomplete_active');
                                selanjutnya.addClass('autocomplete_active');
                            } else {
                                $('.form-group').find('div#hasil_pencarian li:first').addClass('autocomplete_active');
                            }
                        }
                        else if (charCode == 38) //arrow up
                        {
                            if ($('.form-group').find('div#hasil_pencarian li.autocomplete_active').length > 0) {
                                var sebelumnya = $('.form-group').find('div#hasil_pencarian li.autocomplete_active').prev();
                                $('.form-group').find('div#hasil_pencarian li.autocomplete_active').removeClass('autocomplete_active');
                                sebelumnya.addClass('autocomplete_active');
                            } else {
                                $('.form-group').find('div#hasil_pencarian li:first').addClass('autocomplete_active');
                            }
                        }
                        else if (charCode == 13) // enter
                        {

                            var Kodenya = $('.form-group').find('div#hasil_pencarian li.autocomplete_active span#kodenya').html();
                            var No_rmnya = $('.form-group').find('div#hasil_pencarian li.autocomplete_active span#no_rmnya').html();
                            var Namanya = $('.form-group').find('div#hasil_pencarian li.autocomplete_active span#namanya').html();
                            if (Kodenya) {
                                $('#pencarian_kode').val(No_rmnya + ' - ' + Namanya);
                                $('#id_pasien').val(Kodenya);
                            } else {
                                alert('data tidak ada! Pilih pilihan yang ada sebelum di enter.');
                            }


                            $('.form-group').find('div#hasil_pencarian').hide();

                        }
                        else {
                            var text = $('#FormStockOpname').find('div#hasil_pencarian').html();
                            autoComplete($('#pencarian_kode').width(), $('#pencarian_kode').val());
                        }
                    } else {
                        $('#FormStockOpname').find('div#hasil_pencarian').hide();
                    }
                }, 100);
        }
    });
    $(document).on('click', '#daftar-autocomplete li', function () {
        $(this).parent().parent().parent().find('#pencarian_kode').val($(this).find('span#no_rmnya').html() + ' - ' + $(this).find('span#namanya').html());
        $(this).parent().parent().parent().find('#id_pasien').val($(this).find('span#kodenya').html());

        $('.form-group').find('#daftar-autocomplete').hide();

        $('#selectedPasien').show();
        $('#pasien_id').val($(this).find('span#kodenya').html());
        $('#pasien_baru').val('lama');
        $('#nama_pasien').val($(this).find('span#namanya').html());
        $('#alamat_pasien').val($(this).find('span#alamatnya').html());
        $('#no_hp_pasien').val($(this).find('span#nohpnya').html());
        $('#no_rm_pasien').val($(this).find('span#no_rmnya').html());
    });

    function autoComplete(Lebarr, KataKunci) {
        $('div#hasil_pencarian').hide();
        var Lebar = Lebarr + 25;

        $.ajax({
            url: "<?php echo site_url('pendaftaran/ajax_kode'); ?>",
            type: "POST",
            cache: false,
            data: 'keyword=' + KataKunci,
            dataType: 'json',
            success: function (json) {
                if (json.status === 1) {
                    $('#FormStockOpname').find('div#hasil_pencarian').css({'width': Lebar + 'px'});
                    $('#FormStockOpname').find('div#hasil_pencarian').show('fast');
                    $('#FormStockOpname').find('div#hasil_pencarian').html(json.datanya);
                }
                if (json.status === 0) {
                    $('#FormStockOpname').find('div#hasil_pencarian').html('');
                }
            },
            error: function (e) {
                console.log(e);
            }
        });
    }
</script>
<script type="text/javascript">
    (function ($) {
        //selectable html elements
        $.fn.easySelectable = function (options) {
            var el = $(this);
            var options = $.extend({
                'item': 'li',
                'state': true,
                onSelecting: function (el) {

                },
                onSelected: function (el) {

                },
                onUnSelected: function (el) {

                }
            }, options);
            el.on('dragstart', function (event) {
                event.preventDefault();
            });
            el.off('mouseover');
            el.addClass('easySelectable');
            if (options.state) {
                el.find(options.item).addClass('es-selectable');
                el.on('mousedown', options.item, function (e) {
                    $(this).trigger('start_select');
                    var offset = $(this).offset();
                    var hasClass = $(this).hasClass('es-selected');
                    var prev_el = false;
                    el.on('mouseover', options.item, function (e) {
                        if (prev_el == $(this).index())
                            return true;
                        prev_el = $(this).index();
                        var hasClass2 = $(this).hasClass('es-selected');
                        if (!hasClass2) {
                            $(this).addClass('es-selected').trigger('selected');
                            el.trigger('selected');
                            options.onSelecting($(this));
                            options.onSelected($(this));
                        } else {
                            $(this).removeClass('es-selected').trigger('unselected');
                            el.trigger('unselected');
                            options.onSelecting($(this))
                            options.onUnSelected($(this));
                        }
                    });
                    if (!hasClass) {
                        $(this).addClass('es-selected').trigger('selected');
                        el.trigger('selected');
                        options.onSelecting($(this));
                        options.onSelected($(this));
                    } else {
                        $(this).removeClass('es-selected').trigger('unselected');
                        el.trigger('unselected');
                        options.onSelecting($(this));
                        options.onUnSelected($(this));
                    }
                    var relativeX = (e.pageX - offset.left);
                    var relativeY = (e.pageY - offset.top);
                });
                $(document).on('mouseup', function () {
                    el.off('mouseover');
                });
            } else {
                el.off('mousedown');
            }
        };
    })(jQuery);
</script>
<script type="text/javascript">

    function get_PatientDetails(id) {
        $.ajax({
            url: '<?php echo base_url(); ?>admin/patient/patientDetails',
            type: "POST",
            data: {id: id},
            dataType: 'json',
            success: function (res) {
                if (res) {
                    $('#patientid').val(res.id);

                }
            }
        });
    }

    function viewDetailBill(id) {
        $.ajax({
            url: '<?php echo base_url() ?>admin/vehicle/getBillDetails/' + id,
            type: "GET",
            data: {id: id},
            success: function (data) {
                $('#reportdata').html(data);
                $('#edit_deletebill').html("<a href='#' data-toggle='tooltip' onclick='printData(" + id + ")'   data-original-title='<?php echo $this->lang->line('print'); ?>'><i class='fa fa-print'></i></a> <a href='#'' onclick='getRecord(" + id + ")' data-toggle='tooltip'  data-original-title='<?php echo $this->lang->line('edit'); ?>'><i class='fa fa-pencil'></i></a><a onclick='delete_bill(" + id + ")'  href='#'  data-toggle='tooltip'  data-original-title='<?php echo $this->lang->line('delete'); ?>'><i class='fa fa-trash'></i></a>");
                holdModal('viewModalBill');
            },
        });
    }

    function getRecord(id) {
        $.ajax({
            url: '<?php echo base_url(); ?>admin/vehicle/editCall',
            type: "POST",
            data: {id: id},
            dataType: 'json',
            success: function (data) {
                var patientDestroy = $('#addpatientid').select2();
                var vehicleDestroy = $('#vehicleno').select2();
                patientDestroy.val(data.patient_name).select2('').select2();
                vehicleDestroy.val(data.vehicle_no).select2('').select2();
                $("#id").val(data.id);
                $("#driver_name").val(data.driver);
                $("#patienteditid").val(data.patient_name);
                $("#contact_no").val(data.contact_no);
                $("#edit_date").val(data.date);
                $("#address").val(data.address);
                $("#amount").val(data.amount);
                $("#viewModalBill").modal('hide');
                $('#editModal').modal('show');
            },
        });
    }

    function getVehicleDetail(id, vh = 'vehicle_model_search', dr = 'driver_search') {
        $("#" + dr).val("");
        $("#" + vh).val("");
        $.ajax({
            url: '<?php echo base_url(); ?>admin/vehicle/getVehicleDetail',
            type: "POST",
            data: {id: id},
            dataType: 'json',
            success: function (data) {
                $("#" + dr).val(data.driver_name);
                $("#" + vh).val(data.vehicle_model);
            },
        });
    }

    function get_PatienteditDetails(id) {
        $.ajax({
            url: '<?php echo base_url(); ?>admin/patient/patientDetails',
            type: "POST",
            data: {id: id},
            dataType: 'json',
            success: function (res) {
                if (res) {
                    $('#patienteditid').val(res.id);
                }
            }
        });
    }

    $(document).ready(function (e) {
        $("#formedit").on('submit', (function (e) {
            $("#formeditbtn").button('loading');
            e.preventDefault();
            $.ajax({
                url: '<?php echo base_url(); ?>admin/vehicle/updatecallambulance',
                type: "POST",
                data: new FormData(this),
                dataType: 'json',
                contentType: false,
                cache: false,
                processData: false,
                success: function (data) {
                    if (data.status == "fail") {
                        var message = "";
                        $.each(data.error, function (index, value) {
                            message += value;
                        });
                        errorMsg(message);
                    } else {
                        successMsg(data.message);
                        window.location.reload(true);
                    }
                    $("#formeditbtn").button('reset');
                },
                error: function () {

                }
            });
        }));
    });

    function holdModal(modalId) {
        $('#' + modalId).modal({
            backdrop: 'static',
            keyboard: false,
            show: true
        });
    }

    $(".ambulancecall").click(function(){
        $('#formcall').trigger("reset");
        $('#select2-addpatient_id-container').html("");
    });

    $(".modalbtnpatient").click(function(){
        $('#formaddpa').trigger("reset");
        $(".dropify-clear").trigger("click");
    });

    $(function () {
        $('#selectedPasien').hide();
        $('#easySelectable').easySelectable();
        $('.select2').select2()
    })
</script>
<script>
    $(function () {
        $('#nomor').hide();
        $('#jaminan').on('change', function (e) {
            if (this.value === 'umum') {
                $('#nomor').hide();
            }
            else {
                $('#nomor').show();
            }
        });
    })
</script>
<?php //$this->load->view('admin/patient/patientaddmodal') ?>
