  <link rel="stylesheet" href="<?php echo base_url() ?>assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
 
 <div class="content-wrapper">
 <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Data List insentif Obat Dokter
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="#">List insentif Obat Dokter</a></li>
        <li class="active">Data List insentif Obat Dokter</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Data List Insentif Obat Dokter</h3>&nbsp;&nbsp;
              
            </div>
            <!-- /.box-header -->
                <?php $warning = $this->session->flashdata('warning');
              if (!empty($warning)){ ?>
                <div class="alert alert-warning alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h4><i class="icon fa fa-warning"></i> Alert!</h4>
                    <?php echo $warning ?>
                </div>
            <?php } ?>   
             <?php $success = $this->session->flashdata('success');
              if (!empty($success)){ ?>
                <div class="alert alert-success alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h4><i class="icon fa fa-success"></i> Success!</h4>
                    <?php echo $success ?>
                </div>
            <?php } ?>   
            <div class="box-body">
              <table id="printable_table_e" class="table table-bordered table-hover">
                <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama Dokter</th>
                            <th>No RM</th>
                            <th>Nama Pasien</th>
                            <th>Tanggal Pemeriksaan</th>
                            <th>Nama Obat</th>
                            <th>Jumlah</th>
                            <th>Harga Jual</th>
                            <th>Harga Beli</th>
                            <th>Laba</th>
                            <th>Akumulasi</th>
                            <th>Insentif</th>

                        </tr>
                    </thead>

                    <tbody>
                        <?php $no=1; foreach ($listInsentif->result() as $row) { ?>
                  
                        <tr> 
                            <td> <?php echo $no; ?></td>
                            <td> <?php echo ucwords($row->nama_dokter); ?></td>
                            <td style="width:100px;">
                                <?php echo $row->no_rm; ?><br>
                                <small>
                                    <span class="label <?=$jaminan[$row->jaminan]['class']?>"><?=$jaminan[$row->jaminan]['label']?></span>
                                    <?php if (!isset($jaminan[$row->jaminan])) { ?>
                                        <span class="label label-warning">Umum</span>
                                    <?php } ?>
                                </small>
                            </td>
                            <td> <?php echo $row->nama_pasien; ?></td>
                            <td> <?php echo $row->waktu_pemeriksaan; ?></td>
                            <td> <?php echo $row->nama_obat; ?></td>
                            <td> <?php echo $row->jumlah; ?></td>
                            <td nowrap style="text-align: right">Rp  <?php echo number_format($row->harga_jual, 2,',','.'); ?></td>
                            <td nowrap style="text-align: right">Rp  <?php echo number_format($row->harga_beli, 2,',','.'); ?></td>
                            <td nowrap style="text-align: right">Rp  <?php echo number_format($row->laba, 2,',','.'); ?></td>
                            <td nowrap style="text-align: right">Rp  <?php echo number_format($row->akumulasi, 2,',','.'); ?></td>
                            <td nowrap style="text-align: right">Rp <?php echo number_format($row->insentif, 2,',','.'); ?></td>

                        </tr>   

                       <?php $no++; } ?>
                                     

                    </tbody>
               
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->

</div>

<script>

    $(document).ready(function(){
        var table = $('#printable_table_e').DataTable({
            dom: "Bfrtip",
            buttons: [
                {
                    extend: 'copyHtml5',
                    text: '<i class="fa fa-files-o"></i>',
                    titleAttr: 'Copy',
                    exportOptions: {
                        columns: ':visible'
                    }
                },
                {
                    extend: 'excelHtml5',
                    text: '<i class="fa fa-file-excel-o"></i>',
                    titleAttr: 'Excel',
                    title: 'Data Insentif Obat Dokter',
                    exportOptions: {
                        columns: ':visible'
                    }
                },
                {
                    extend: 'pdfHtml5',
                    text: '<i class="fa fa-file-pdf-o"></i>',
                    titleAttr: 'PDF',
                    exportOptions: {
                        columns: ':visible'
                    }
                },
                {
                    extend: 'print',
                    text: '<i class="fa fa-print"></i>',
                    titleAttr: 'Print',
                    customize: function ( win ) {
                        $(win.document.body)
                            .css( 'font-size', '10pt' );

                        $(win.document.body).find( 'table' )
                            .addClass( 'compact' )
                            .css( 'font-size','inherit');
                    },
                    exportOptions: {
                        columns: ':visible'
                    }
                },
            ]
        });
        table.buttons().container().appendTo( '#printable_table_wrapper .col-md-6:eq(0)' );
    });

  </script>