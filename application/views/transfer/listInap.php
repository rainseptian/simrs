<?php
$currency_symbol = 'Rp';
?>
<link rel="stylesheet"
      href="<?php echo base_url() ?>assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
<link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/autocomplete/style-gue.css">
<style type="text/css">
    @import('https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.0/css/bootstrap.min.css');
</style>

<style type="text/css">
    .table-responsive {overflow-x: inherit;}
</style>
<div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title titlefix"> Transfer pasien dari <?=$dari?> ke <?=$ke?></h3>
                    </div><!-- /.box-header -->

                    <div class="box-body">
                        <table class="table table-striped table-bordered table-hover data-table" cellspacing="0" width="100%">
                            <thead>
                            <tr>
                                <th>No</th>
                                <th>Tgl Daftar</th>
                                <th>No RM</th>
                                <th>Nama Pasien</th>
                                <th>Alamat</th>
                                <th>Nama Dokter</th>
                                <th>Dari</th>
                                <th>Bed</th>
                                <th>Tipe Pasien</th>
                                <th>Aksi</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $no = 1;
                            foreach ($list as $r) : ?>
                                <tr>
                                    <td><?=$no++?></td>
                                    <td><?=date('d-m-Y H:i', strtotime($r['created_at']))?></td>
                                    <td><?=$r['no_rm']?></td>
                                    <td><?=$r['nama_pasien']?></td>
                                    <td><?=$r['alamat']?></td>
                                    <td><?=$r['nama_dokter']?></td>
                                    <td><?=$r['dari']?></td>
                                    <td><?=$r['bed_name'].' - '.$r['bedgroup']?></td>
                                    <td><?=$r['tipe_pasien']?></td>
                                    <td>
                                        <?php if ($jenis == 2 || $jenis == 19) : ?>
                                            <form method="post" action="<?=base_url('TransferPasien/add/'.$jenis)?>" style="float: right; margin-right: 5px">
                                                <input type="hidden" class="form-control" name="rawat_inap_id" value="<?=$r['id']?>">
                                                <button type="submit" class="btn btn-sm btn-success">
                                                    <i class="fa fa-cut"></i> Transfer
                                                </button>
                                            </form>
                                        <?php elseif ($jenis == 3) : ?>
                                            <form method="post" action="<?=base_url('TransferPasien/add/3')?>" style="float: right; margin-right: 5px">
                                                <input type="hidden" class="form-control" name="rawat_inap_id" value="<?=$r['id']?>">
                                                <button type="submit" class="btn btn-sm btn-success">
                                                    <i class="fa fa-random"></i> Transfer
                                                </button>
                                            </form>
                                        <?php elseif ($jenis == 4) : ?>
                                            <form method="post" action="<?=base_url('TransferPasien/add/4')?>" style="float: right; margin-right: 5px">
                                                <input type="hidden" class="form-control" name="rawat_inap_id" value="<?=$r['id']?>">
                                                <button type="submit" class="btn btn-sm btn-success">
                                                    <i class="fa fa-baby-carriage"></i> Transfer
                                                </button>
                                            </form>
                                        <?php elseif ($jenis == 10 || $jenis == 11 || $jenis == 12) : ?>
                                            <form method="post" action="<?=base_url('TransferPasien/add/'.$jenis)?>" style="float: right; margin-right: 5px">
                                                <input type="hidden" class="form-control" name="rawat_inap_id" value="<?=$r['id']?>">
                                                <button type="submit" class="btn btn-sm btn-success">
                                                    <i class="fa fa-baby"></i> Transfer
                                                </button>
                                            </form>
                                        <?php elseif ($jenis == 15 || $jenis == 16 || $jenis == 17) : ?>
                                            <form method="post" action="<?=base_url('TransferPasien/add/'.$jenis)?>" style="float: right; margin-right: 5px">
                                                <input type="hidden" class="form-control" name="rawat_inap_id" value="<?=$r['id']?>">
                                                <button type="submit" class="btn btn-sm btn-success">
                                                    <i class="fa fa-hand-holding-medical"></i> Transfer
                                                </button>
                                            </form>
                                        <?php endif; ?>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<script type="text/javascript">
    $(function () {
        $('.data-table').DataTable()
    })
</script>
