<style media="screen">
    .select2-container {
        width: 100% !important;
    }
</style>
<link rel="stylesheet"
      href="<?php echo base_url() ?>assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
<link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/autocomplete/style-gue.css">
<style type="text/css">
    @import('https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.0/css/bootstrap.min.css');
</style>

<!-- iCheck for checkboxes and radio inputs -->
<link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/iCheck/all.css">

<div class="content-wrapper">

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Edit Transfer Pasien
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url() ?>Dashbord"><i class="fa fa-dashboard"></i> Dashbord</a></li>
            <li class="active">Edit Transfer Pasien</li>
        </ol>
    </section>

    <!-- Main content -->
    <form class="form-horizontal" method="post" action="<?php echo base_url()?>TransferPasien/update">
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-primary">
                        <div class="box-header">
                            <h3 class="box-title"> Edit Data Transfer Pasien</h3>
                        </div>

                        <?php $warning = $this->session->flashdata('warning');
                        if (!empty($warning)){ ?>
                            <div class="alert alert-warning alert-dismissible">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                <h4><i class="icon fa fa-warning"></i> Alert!</h4>
                                <?php echo $warning ?>
                            </div>
                        <?php } ?>
                        <?php $success = $this->session->flashdata('success');
                        if (!empty($success)){ ?>
                            <div class="alert alert-success alert-dismissible">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                <h4><i class="icon fa fa-check"></i> Success!</h4>
                                <?php echo $success ?>
                            </div>
                        <?php } ?>

                        <input type="hidden" name="transfer_id" value="<?=$transfer->id?>">
                        <input type="hidden" name="redirect" value="<?=$redirect?>">
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="tanggal" class="col-sm-2 control-label">Tanggal</label>
                                        <div class="col-sm-9">
                                            <input type="date" class="form-control" id="tanggal" name="tanggal" value="<?=$transfer->tanggal?>" required>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="pukul" class="col-sm-2 control-label">Pukul</label>
                                        <div class="col-sm-9">
                                            <input type="time" class="form-control" id="pukul" name="pukul" value="<?=$transfer->pukul?>" required>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="tujuan" class="col-sm-2 control-label">Pasien</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" name="pencarian_kode" id="pencarian_kode"
                                                   value="<?=$transfer->nama_pasien?>"
                                                   readonly
                                                   placeholder="Masukkan Nama atau No RM" autocomplete="off">
                                            <div id='hasil_pencarian'></div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="tujuan" class="col-sm-2 control-label">Dokter</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" name="dokter"
                                                   value="<?=$transfer->nama_dokter?>"
                                                   readonly
                                                   placeholder="Masukkan Nama atau No RM" autocomplete="off">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="dari" class="col-sm-2 control-label">Dari</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" id="dari" name="dari" readonly value="<?=$dari?>">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="tujuan" class="col-sm-2 control-label">Tujuan</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" name="tujuan" readonly value="<?=$tujuan?>">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="note" class="col-sm-2 control-label"  maxlength="255">Catatan</label>
                                        <div class="col-sm-9">
                                            <textarea name="note" class="form-control" id="note" rows="3" placeholder="catatan khusus"><?=$transfer->catatan?></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="box box-danger">
                        <div class="box-header">
                            <h3 class="box-title"> Form Transfer Internal</h3>
                            <ul class="pagination pagination-sm no-margin pull-right">
                                <li>
<!--                                    <button id="tombol-tambah-obat" type="button" class="btn btn-sm btn-primary" name="button"><i class="fa fa-plus"></i> Tambah Obat</button>-->
                                </li>
                            </ul>
                        </div>
                        <?php
                        $meta = (object) unserialize($transfer->meta);
                        ?>
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-1"></div>
                                <div class="col-md-6">
                                    <label> I. Identitas Pasien</label>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="nama" class="col-sm-2 control-label">Nama</label>

                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" id="nama"
                                                   name="meta[nama]"
                                                   readonly
                                                   value="<?=$meta ? $meta->nama : '' ?>"
                                                   placeholder="Masukkan nama pasien" >
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="jenis_kelamin" class="col-sm-2 control-label">Jenis Kelamin</label>
                                        <div class="col-sm-9">
                                            <label class="radio-inline">
                                                <input type="radio" value="L"
                                                       disabled
                                                       name="meta[jenis_kelamin]"
                                                       <?=$meta && $meta->jk == 'L' ? 'checked' : '' ?>
                                                       id="jenis_kelamin">Laki - laki
                                            </label>
                                            <label class="radio-inline">
                                                <input type="radio" value="P"
                                                       disabled
                                                       name="meta[jenis_kelamin]"
                                                       <?=$meta && $meta->jk == 'P' ? 'checked' : '' ?>
                                                       id="jenis_kelamin" >Perempuan
                                            </label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="tanggal_lahir" class="col-sm-2 control-label">Tanggal Lahir</label>

                                        <div class="col-sm-9">
                                            <input type="date" class="form-control" id="tanggal_lahir"
                                                   value="<?=$meta ? $meta->tanggal_lahir : '' ?>"
                                                   readonly
                                                   name="meta[tanggal_lahir]">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="telepon" class="col-sm-2 control-label">Diagnosis Masuk</label>

                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" id="diagnosis_masuk" name="meta[diagnosis_masuk]"
                                                   value="<?=$meta ? $meta->diagnosis_masuk : '' ?>"
                                                   placeholder="Masukkan diagnosis masuk" >
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="telepon" class="col-sm-2 control-label">DPJP</label>

                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" id="dpjp" name="meta[dpjp]"
                                                   value="<?=$meta ? $meta->dpjp : ''?>"
                                                   placeholder="Masukkan DPJP" >
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="tanggal_masuk" class="col-sm-2 control-label">Tanggal Masuk</label>

                                        <div class="col-sm-9">
                                            <input type="date" class="form-control" id="tanggal_masuk"
                                                   value="<?=$meta ? $meta->tanggal_masuk : date('Y-m-d') ?>"
                                                   name="meta[tanggal_masuk]">
                                        </div>
                                    </div>
                                    <div class="form-group" style="display: none;">
                                        <label for="ruang" class="col-sm-2 control-label">Ruang / Kamar</label>

                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" id="ruang"
                                                   value="<?=$meta ? $meta->ruang : '' ?>"
                                                   name="meta[ruang]">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="status_pasien" class="col-sm-2 control-label">Status Pasien</label>

                                        <div class="col-sm-9">
                                            <div class="col-sm-9">
                                                <label class="radio-inline">
                                                    <input type="radio" value="umum"
                                                        <?=$meta && $meta->status_pasien == 'umum' ? 'checked' : '' ?>
                                                           name="meta[status_pasien]"
                                                           id="status_pasien" required>Umum
                                                </label>
                                                <label class="radio-inline">
                                                    <input type="radio" value="bpjs"
                                                        <?=$meta && $meta->status_pasien == 'bpjs' ? 'checked' : '' ?>
                                                           name="meta[status_pasien]"
                                                           id="status_pasien" >BPJS
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-1"></div>
                                <div class="col-md-6">
                                    <label> II. Ringkasan Medis</label>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="nama" class="col-sm-2 control-label">Keluhan</label>

                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" id="keluhan"
                                                   value="<?=$meta ? $meta->keluhan : '' ?>"
                                                   name="meta[keluhan]"
                                                   placeholder="Masukkan keluhan" >
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="nama" class="col-sm-2 control-label">Riwayat Alergi</label>

                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" id="riwayat_alergi"
                                                   value="<?=$meta ? $meta->riwayat_alergi : '' ?>"
                                                   name="meta[riwayat_alergi]"
                                                   placeholder="Masukkan riwayat alergi" >
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="nama" class="col-sm-2 control-label">Riwayat Penyakit Resiko Tinggi</label>

                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" id="riwayat_penyakit_resiko_tinggi"
                                                   value="<?=$meta ? $meta->riwayat_penyakit_resiko_tinggi : '' ?>"
                                                   name="meta[riwayat_penyakit_resiko_tinggi]"
                                                   placeholder="Masukkan riwayat penyakit resiko tinggi" >
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="telepon" class="col-sm-2 control-label">Laboratorium</label>

                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" id="laboratorium" name="meta[laboratorium]"
                                                   value="<?=$meta ? $meta->laboratorium : '' ?>"
                                                   placeholder="Masukkan Laboratorium" >
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="telepon" class="col-sm-2 control-label">Radiologi</label>

                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" id="radiologi" name="meta[radiologi]"
                                                   value="<?=$meta ? $meta->radiologi : '' ?>"
                                                   placeholder="Masukkan Radiologi" >
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="col-sm-6 col-md-6 col-lg-6 form-group">
                                                <label for="td" class="col-sm-4 control-label">TD</label>
                                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">
                                                    <input type="text" class="form-control requirable" name="meta[td]" id="td" value="<?=$meta ? $meta->td : '' ?>">
                                                    <span class="input-group-addon">mmHg</span>
                                                </div>
                                            </div>
                                            <div class="col-sm-6 col-md-6 col-lg-6 form-group">
                                                <label for="inputEmail3" class="col-sm-4 control-label">R</label>
                                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">
                                                    <input type="text" class="form-control requirable" name="meta[r]" id="r" value="<?=$meta ? $meta->r : '' ?>">
                                                    <span class="input-group-addon">K/Min</span>
                                                </div>
                                            </div>
                                            <div class="col-sm-6 col-md-6 col-lg-6 form-group">
                                                <label for="inputEmail3" class="col-sm-4 control-label">BB</label>
                                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">
                                                    <input type="text" class="form-control requirable" id="bb" name="meta[bb]" value="<?=$meta ? $meta->bb : '' ?>"
                                                           onkeyup="set_bmi()">
                                                    <span class="input-group-addon">Kg</span>
                                                </div>
                                            </div>
                                            <div class="col-sm-6 col-md-6 col-lg-6 form-group">
                                                <label for="inputEmail3" class="col-sm-4 control-label">N</label>
                                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">
                                                    <input type="text" class="form-control requirable" name="meta[n]" id="n" value="<?=$meta && isset($meta->n) ? $meta->n : '' ?>">
                                                    <span class="input-group-addon">K/Min</span>
                                                </div>
                                            </div>
                                            <div class="col-sm-6 col-md-6 col-lg-6 form-group">
                                                <label for="inputEmail3" class="col-sm-4 control-label">S</label>
                                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">
                                                    <input type="text" class="form-control requirable" id="s" name="meta[s]" value="<?=$meta ? $meta->s : '' ?>">
                                                    <span class="input-group-addon">'0</span>
                                                </div>
                                            </div>
                                            <div class="col-sm-6 col-md-6 col-lg-6 form-group">
                                                <label for="inputEmail3" class="col-sm-4 control-label">TB</label>
                                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">
                                                    <input type="text" class="form-control requirable" id="tb" name="meta[tb]" value="<?=$meta ? $meta->tb : '' ?>"
                                                           onkeyup="set_bmi()">
                                                    <span class="input-group-addon">cm</span>
                                                </div>
                                            </div>
                                            <div class="col-sm-6 col-md-6 col-lg-6 form-group">
                                                <label for="inputEmail3" class="col-sm-4 control-label">BMI</label>
                                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">
                                                    <input type="text" class="form-control requirable" name="meta[bmi]" id="bmi" value="<?=$meta ? $meta->bmi : '' ?>">

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-1"></div>
                                <div class="col-md-6">
                                    <label> III. Pemberian Terapi</label>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="infus" class="col-sm-2 control-label">Infus</label>
                                        <div class="col-sm-9">
                                            <textarea name="meta[infus]" class="form-control" id="infus" rows="2" placeholder="Infus"><?=$meta ? $meta->infus : '' ?></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="obat_injeksi" class="col-sm-2 control-label">Obat Injeksi</label>
                                        <div class="col-sm-9">
                                            <textarea name="meta[obat_injeksi]" class="form-control" id="obat_injeksi" rows="2" placeholder="Obat Injeksi"><?=$meta ? $meta->obat_injeksi : '' ?></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="obat_oral" class="col-sm-2 control-label">Obat Oral</label>
                                        <div class="col-sm-9">
                                            <textarea name="meta[obat_oral]" class="form-control" id="obat_oral" rows="2" placeholder="Obat Oral"><?=$meta ? $meta->obat_oral : '' ?></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-1"></div>
                                <div class="col-md-6">
                                    <label> IV. Tindakan Medis dan Observasi</label>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div class="col-sm-1"></div>
                                        <div class="col-sm-10">
                                            <textarea name="meta[tindakan_medis]" class="form-control" id="tindakan_medis" rows="3" placeholder="Tindakan Medis dan Observasi"><?=$meta ? $meta->tindakan_medis : '' ?></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-1"></div>
                                <div class="col-md-6">
                                    <label> V. Kondisi Pasien</label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="table-responsive">
                                        <table class="table table-hover table-striped table-bordered example">
                                            <tbody>
                                            <tr>
                                                <td style="text-align: center"><label class="control-label">Sebelum Transfer</label></td>
                                                <td style="text-align: center"><label class="control-label">Setelah Transfer</label></td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <div class="form-group">
                                                        <label for="before_keadaan_umum" class="col-sm-2 control-label">Keadaan Umum</label>
                                                        <div class="col-sm-9">
                                                            <input type="text" class="form-control" id="before_keadaan_umum" name="meta[before_keadaan_umum]" value="<?=$meta ? $meta->before_keadaan_umum : '' ?>">
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="form-group">
                                                        <label for="after_keadaan_umum" class="col-sm-2 control-label">Keadaan Umum</label>
                                                        <div class="col-sm-9">
                                                            <input type="text" class="form-control" id="after_keadaan_umum" name="meta[after_keadaan_umum]" value="<?=$meta ? $meta->after_keadaan_umum : '' ?>">
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <div class="form-group">
                                                        <label for="before_kesadaran" class="col-sm-2 control-label">Kesadaran</label>
                                                        <div class="col-sm-9">
                                                            <input type="text" class="form-control" id="before_kesadaran" name="meta[before_kesadaran]" value="<?=$meta ? $meta->before_kesadaran : '' ?>">
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="form-group">
                                                        <label for="after_kesadaran" class="col-sm-2 control-label">Kesadaran</label>
                                                        <div class="col-sm-9">
                                                            <input type="text" class="form-control" id="after_kesadaran" name="meta[after_kesadaran]" value="<?=$meta ? $meta->after_kesadaran : '' ?>">
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="col-sm-6 col-md-6 col-lg-6 form-group">
                                                                <label for="td" class="col-sm-4 control-label">TD</label>
                                                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">
                                                                    <input type="text" class="form-control requirable" name="meta[before_td]" id="before_td" value="<?=$meta ? $meta->before_td : '' ?>">
                                                                    <span class="input-group-addon">mmHg</span>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-6 col-md-6 col-lg-6 form-group">
                                                                <label for="inputEmail3" class="col-sm-4 control-label">R</label>
                                                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">
                                                                    <input type="text" class="form-control requirable" name="meta[before_r]" id="before_r" value="<?=$meta ? $meta->before_r : '' ?>">
                                                                    <span class="input-group-addon">K/Min</span>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-6 col-md-6 col-lg-6 form-group">
                                                                <label for="inputEmail3" class="col-sm-4 control-label">BB</label>
                                                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">
                                                                    <input type="text" class="form-control requirable" id="before_bb" name="meta[before_bb]" value="<?=$meta ? $meta->before_bb: '' ?>"
                                                                           onkeyup="set_before_bmi()">
                                                                    <span class="input-group-addon">Kg</span>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-6 col-md-6 col-lg-6 form-group">
                                                                <label for="inputEmail3" class="col-sm-4 control-label">N</label>
                                                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">
                                                                    <input type="text" class="form-control requirable" name="meta[before_n]" id="before_n" value="<?=$meta ? $meta->before_n : '' ?>">
                                                                    <span class="input-group-addon">K/Min</span>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-6 col-md-6 col-lg-6 form-group">
                                                                <label for="inputEmail3" class="col-sm-4 control-label">S</label>
                                                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">
                                                                    <input type="text" class="form-control requirable" id="before_s" name="meta[before_s]" value="<?=$meta ? $meta->before_s : '' ?>">
                                                                    <span class="input-group-addon">'0</span>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-6 col-md-6 col-lg-6 form-group">
                                                                <label for="inputEmail3" class="col-sm-4 control-label">TB</label>
                                                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">
                                                                    <input type="text" class="form-control requirable" id="before_tb" name="meta[before_tb]" value="<?=$meta ? $meta->before_tb : '' ?>"
                                                                           onkeyup="set_before_bmi()">
                                                                    <span class="input-group-addon">cm</span>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-6 col-md-6 col-lg-6 form-group">
                                                                <label for="inputEmail3" class="col-sm-4 control-label">BMI</label>
                                                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">
                                                                    <input type="text" class="form-control requirable" name="meta[before_bmi]" id="before_bmi" value="<?=$meta ? $meta->before_bmi : '' ?>">

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="col-sm-6 col-md-6 col-lg-6 form-group">
                                                                <label for="td" class="col-sm-4 control-label">TD</label>
                                                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">
                                                                    <input type="text" class="form-control requirable" name="meta[after_td]" value="<?=$meta ? $meta->after_td : '' ?>">
                                                                    <span class="input-group-addon">mmHg</span>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-6 col-md-6 col-lg-6 form-group">
                                                                <label for="inputEmail3" class="col-sm-4 control-label">R</label>
                                                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">
                                                                    <input type="text" class="form-control requirable" name="meta[after_r]" value="<?=$meta ? $meta->after_r : '' ?>">
                                                                    <span class="input-group-addon">K/Min</span>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-6 col-md-6 col-lg-6 form-group">
                                                                <label for="inputEmail3" class="col-sm-4 control-label">BB</label>
                                                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">
                                                                    <input type="text" class="form-control requirable" id="after_bb" name="meta[after_bb]" value="<?=$meta ? $meta->after_bb : '' ?>"
                                                                           onkeyup="set_after_bmi()">
                                                                    <span class="input-group-addon">Kg</span>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-6 col-md-6 col-lg-6 form-group">
                                                                <label for="inputEmail3" class="col-sm-4 control-label">N</label>
                                                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">
                                                                    <input type="text" class="form-control requirable" name="meta[after_n]" value="<?=$meta ? $meta->after_n : '' ?>">
                                                                    <span class="input-group-addon">K/Min</span>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-6 col-md-6 col-lg-6 form-group">
                                                                <label for="inputEmail3" class="col-sm-4 control-label">S</label>
                                                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">
                                                                    <input type="text" class="form-control requirable" id="after_s" name="meta[after_s]" value="<?=$meta ? $meta->after_s : '' ?>">
                                                                    <span class="input-group-addon">'0</span>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-6 col-md-6 col-lg-6 form-group">
                                                                <label for="inputEmail3" class="col-sm-4 control-label">TB</label>
                                                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">
                                                                    <input type="text" class="form-control requirable" id="after_tb" name="meta[after_tb]" value="<?=$meta ? $meta->after_tb : '' ?>"
                                                                           onkeyup="set_after_bmi()">
                                                                    <span class="input-group-addon">cm</span>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-6 col-md-6 col-lg-6 form-group">
                                                                <label for="inputEmail3" class="col-sm-4 control-label">BMI</label>
                                                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">
                                                                    <input type="text" class="form-control requirable" name="meta[after_bmi]" id="after_bmi" value="<?=$meta ? $meta->after_bmi : '' ?>">

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <div class="form-group">
                                                        <label for="before_catatan_penting" class="col-sm-2 control-label">Catatan Penting</label>
                                                        <div class="col-sm-9">
                                                            <input type="text" class="form-control" id="before_catatan_penting" name="meta[before_catatan_penting]" value="<?=$meta ? $meta->before_catatan_penting : '' ?>">
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="form-group">
                                                        <label for="after_catatan_penting" class="col-sm-2 control-label">Catatan Penting</label>
                                                        <div class="col-sm-9">
                                                            <input type="text" class="form-control" id="after_catatan_penting" name="meta[after_catatan_penting]" value="<?=$meta ? $meta->after_catatan_penting : '' ?>">
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="box-footer">
                            <button type="submit" name="submit" value="1" class="btn btn-primary btn-lg btn-flat pull-right">Simpan</button>
                            <a href="<?php echo base_url() ?>Apotek/pembelian"  class="btn btn-default btn-lg btn-flat pull-right">Batal</a>
                        </div>
                    </div>
                </div>
        </section>
    </form>
</div>

<!-- Select2 -->
<script src="<?= base_url() ?>assets/bower_components/select2/dist/js/select2.full.min.js"></script>
<!-- InputMask -->
<!-- InputMask -->
<script src="<?php echo base_url() ?>assets/plugins/input-mask/jquery.inputmask.js"></script>
<script src="<?php echo base_url() ?>assets/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="<?php echo base_url() ?>assets/plugins/input-mask/jquery.inputmask.extensions.js"></script>

<script type="text/javascript">
    $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' });
    $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' });
    $('[data-mask]').inputmask();
    $('#nama_obat').select2();

    function set_before_bmi() {
        var tb = $('#before_tb').val();
        var bb = $('#before_bb').val();
        var tbm = tb / 100;
        var bmi = bb / (tbm * tbm);

        $('#before_bmi').val(bmi.toFixed(2));
    }
    function set_after_bmi() {
        var tb = $('#after_tb').val();
        var bb = $('#after_bb').val();
        var tbm = tb / 100;
        var bmi = bb / (tbm * tbm);

        $('#after_bmi').val(bmi.toFixed(2));
    }
</script>