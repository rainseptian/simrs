<style media="screen">
    .select2-container {
        width: 100% !important;
    }
</style>
<link rel="stylesheet"
      href="<?php echo base_url() ?>assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
<link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/autocomplete/style-gue.css">
<style type="text/css">
    @import('https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.0/css/bootstrap.min.css');
</style>

<!-- iCheck for checkboxes and radio inputs -->
<link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/iCheck/all.css">

<div class="content-wrapper">

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Daftar Rawat Inap
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url() ?>Dashbord"><i class="fa fa-dashboard"></i> Dashbord</a></li>
            <li class="active">Daftar Rawat Inap</li>
        </ol>
    </section>

    <!-- Main content -->
    <form class="form-horizontal" method="post" action="<?php echo base_url()?>TransferPasien/submitDaftar/<?=$transfer->jenis?>">
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-primary">
                        <div class="box-header">
                            <h3 class="box-title"> Daftar Rawat Inap</h3>
                        </div>

                        <?php $warning = $this->session->flashdata('warning');
                        if (!empty($warning)){ ?>
                            <div class="alert alert-warning alert-dismissible">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                <h4><i class="icon fa fa-warning"></i> Alert!</h4>
                                <?php echo $warning ?>
                            </div>
                        <?php } ?>
                        <?php $success = $this->session->flashdata('success');
                        if (!empty($success)){ ?>
                            <div class="alert alert-success alert-dismissible">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                <h4><i class="icon fa fa-check"></i> Success!</h4>
                                <?php echo $success ?>
                            </div>
                        <?php } ?>

                        <input type="hidden" name="id" value="<?=$transfer->id?>">
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="tanggal" class="col-sm-2 control-label">Tanggal</label>
                                        <div class="col-sm-9">
                                            <input type="date" class="form-control" id="tanggal" name="tanggal" value="<?=$transfer->tanggal?>" required readonly>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="pukul" class="col-sm-2 control-label">Pukul</label>
                                        <div class="col-sm-9">
                                            <input type="time" class="form-control" id="pukul" name="pukul" value="<?=$transfer->pukul?>" required readonly>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="tujuan" class="col-sm-2 control-label">Pasien</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" name="pencarian_kode" id="pencarian_kode"
                                                   placeholder="Pasien" value="<?=$transfer->nama_pasien?>" readonly>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="tujuan" class="col-sm-2 control-label">Dokter</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" name="dokter" id="dokter"
                                                   placeholder="Pasien" value="<?=$transfer->nama_dokter?>" readonly>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="dari" class="col-sm-2 control-label">Dari</label>
                                        <div class="col-sm-9">
                                            <?php if ($transfer->jenis == '1') : ?>
                                                <?php
                                                if ($transfer->jenis == 1) {
                                                    foreach ($poli as $p) {
                                                        if ($p->id == $transfer->dari) {
                                                            $dari = ucwords($p->nama);
                                                            break;
                                                        }
                                                    }
                                                }
                                                ?>
                                                <input type="text" id="dari" name="dari" class="form-control" value="<?=$dari?>" required readonly>
                                            <?php elseif ($transfer->jenis == '2' || $transfer->jenis == '3' || $transfer->jenis == '4') : ?>
                                                <input type="hidden" id="rawat_inap_id" name="rawat_inap_id" value="<?=$rawat_inap->id?>">
                                                <input type="hidden" id="id_bed_dari" name="id_bed_dari" value="<?=$rawat_inap->bed_id?>">
                                                <input type="text" class="form-control" id="dari" name="dari" readonly value="<?=$rawat_inap->bed_name.' - '.$rawat_inap->bedgroup?>">
                                            <?php elseif (!$rawat_inap->via_bangsal || $transfer->jenis == '5' || $transfer->jenis == '6') : // tak tambahi 5 dan 6 ?>
                                                <?php if ($transfer->jenis == '5') : ?>
                                                    <input type="text" class="form-control" id="dari" name="dari" readonly value="Ruang Operasi">
                                                <?php elseif ($transfer->jenis == '6') : ?>
                                                    <input type="text" class="form-control" id="dari" name="dari" readonly value="Ruang Bersalin">
                                                <?php endif; ?>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="tujuan" class="col-sm-2 control-label">Tujuan</label>
                                        <div class="col-sm-9">
                                            <?php if ($transfer->jenis == '1' || !$rawat_inap->via_bangsal) : ?>
                                                <select class="form-control abdush-select" name="tujuan" required>
                                                    <option value="" selected>-- Pilih Tujuan --</option>
                                                    <?php foreach ($tujuan as $value) { ?>
                                                        <option value="<?php echo $value['id'] ?>"><?php echo $value['name'].' - '.$value['bedgroup']; ?></option>
                                                    <?php } ?>
                                                </select>
                                            <?php elseif ($transfer->jenis == '2') : ?>
                                                <input type="text" id="tujuan" name="tujuan" class="form-control" value="Ruang Operasi" required readonly>
                                            <?php elseif ($transfer->jenis == '4') : ?>
                                                <input type="text" id="tujuan" name="tujuan" class="form-control" value="Ruang Bersalin" required readonly>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="note" class="col-sm-2 control-label"  maxlength="255">Catatan</label>
                                        <div class="col-sm-9">
                                            <textarea name="note" class="form-control" id="note" rows="3" placeholder="catatan khusus"><?=$transfer->catatan?></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="box-footer">
                            <button type="submit" name="submit" value="1" class="btn btn-primary btn-lg btn-flat pull-right">Simpan</button>
                            <a href="<?php echo base_url() ?>TransferPasien/showList"  class="btn btn-default btn-lg btn-flat pull-right">Batal</a>
                        </div>
                    </div>
                </div>
        </section>
    </form>
</div>

<!-- Select2 -->
<script src="<?= base_url() ?>assets/bower_components/select2/dist/js/select2.full.min.js"></script>
<!-- InputMask -->
<!-- InputMask -->
<script src="<?php echo base_url() ?>assets/plugins/input-mask/jquery.inputmask.js"></script>
<script src="<?php echo base_url() ?>assets/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="<?php echo base_url() ?>assets/plugins/input-mask/jquery.inputmask.extensions.js"></script>

<script type="text/javascript">
    $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' });
    $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' });
    $('[data-mask]').inputmask();
    $('#nama_obat').select2();

    function onPasienSelected() {
        const pasien_id = $('#id_pasien').val();
        console.log(pasien_id);
        $.ajax({
            url: "<?php echo site_url('TransferPasien/getPasienInfo/'); ?>" + pasien_id,
            type: "POST",
            cache: false,
            dataType: 'json',
            success: function (r) {
                $('#id_poli_dari').val(r.poli.id);
                $('#dari').val(r.poli.nama);

                const pasien = r.pasien;
                $('#nama').val(pasien.nama);
                $('input:radio[name="meta[jenis_kelamin]"]').filter(`[value="${pasien.jk}"]`).attr('checked', true);
                $('#tanggal_lahir').val(pasien.tanggal_lahir);

                const pemeriksaan = r.pemeriksaan;
                $('#td').val(pemeriksaan.td);
                $('#n').val(pemeriksaan.n);
                $('#r').val(pemeriksaan.r);
                $('#s').val(pemeriksaan.s);
                $('#bb').val(pemeriksaan.bb);
                $('#tb').val(pemeriksaan.tb);
                $('#bmi').val(pemeriksaan.bmi);
                $('#before_td').val(pemeriksaan.td);
                $('#before_n').val(pemeriksaan.n);
                $('#before_r').val(pemeriksaan.r);
                $('#before_s').val(pemeriksaan.s);
                $('#before_bb').val(pemeriksaan.bb);
                $('#before_tb').val(pemeriksaan.tb);
                $('#before_bmi').val(pemeriksaan.bmi);
            },
            error: function (e) {
                console.log(e);
            }
        })
    }
</script>

<script type="text/javascript">
    var timer = null;
    $(document).on('keyup', '#pencarian_kode', function (e) {
        if($('#pencarian_kode').val().length > 0){
            clearTimeout(timer);
            timer = setTimeout(
                function () {
                    if ($('#pencarian_kode').val() != '') {
                        var charCode = ( e.which ) ? e.which : event.keyCode;
                        if (charCode == 40) //arrow down
                        {

                            if ($('.form-group').find('div#hasil_pencarian li.autocomplete_active').length > 0) {
                                var selanjutnya = $('.form-group').find('div#hasil_pencarian li.autocomplete_active').next();
                                $('.form-group').find('div#hasil_pencarian li.autocomplete_active').removeClass('autocomplete_active');
                                selanjutnya.addClass('autocomplete_active');
                            } else {
                                $('.form-group').find('div#hasil_pencarian li:first').addClass('autocomplete_active');
                            }
                        }
                        else if (charCode == 38) //arrow up
                        {
                            if ($('.form-group').find('div#hasil_pencarian li.autocomplete_active').length > 0) {
                                var sebelumnya = $('.form-group').find('div#hasil_pencarian li.autocomplete_active').prev();
                                $('.form-group').find('div#hasil_pencarian li.autocomplete_active').removeClass('autocomplete_active');
                                sebelumnya.addClass('autocomplete_active');
                            } else {
                                $('.form-group').find('div#hasil_pencarian li:first').addClass('autocomplete_active');
                            }
                        }
                        else if (charCode == 13) // enter
                        {

                            var Kodenya = $('.form-group').find('div#hasil_pencarian li.autocomplete_active span#kodenya').html();
                            var No_rmnya = $('.form-group').find('div#hasil_pencarian li.autocomplete_active span#no_rmnya').html();
                            var Namanya = $('.form-group').find('div#hasil_pencarian li.autocomplete_active span#namanya').html();
                            if (Kodenya) {
                                $('#pencarian_kode').val(No_rmnya + ' - ' + Namanya);
                                $('#id_pasien').val(Kodenya);
                            } else {
                                alert('data tidak ada! Pilih pilihan yang ada sebelum di enter.');
                            }


                            $('.form-group').find('div#hasil_pencarian').hide();

                        }
                        else {
                            var text = $('div#hasil_pencarian').html();
                            autoComplete($('#pencarian_kode').width(), $('#pencarian_kode').val());
                        }
                    } else {
                        $('div#hasil_pencarian').hide();
                    }
                }, 100);
        }
    });
    $(document).on('click', '#daftar-autocomplete li', function () {
        $(this).parent().parent().parent().find('#pencarian_kode').val($(this).find('span#no_rmnya').html() + ' - ' + $(this).find('span#namanya').html());
        $(this).parent().parent().parent().find('#id_pasien').val($(this).find('span#kodenya').html());

        $('.form-group').find('#daftar-autocomplete').hide();
        onPasienSelected();
    });

    function autoComplete(Lebar, KataKunci) {
        $('div#hasil_pencarian').hide();
        var Lebar = Lebar + 25;

        $.ajax({
            url: "<?php echo site_url('pendaftaran/ajax_kode'); ?>",
            type: "POST",
            cache: false,
            data: 'keyword=' + KataKunci,
            dataType: 'json',
            success: function (json) {
                if (json.status == 1) {
                    $('div#hasil_pencarian').css({'width': Lebar + 'px'});
                    $('div#hasil_pencarian').show('fast');
                    $('div#hasil_pencarian').html(json.datanya);
                }
                if (json.status == 0) {
                    $('div#hasil_pencarian').html('');
                }
            },
            error: function (e) {
                console.log(e);
            }
        });
    }

    function cekbarcode(KataKunci, Indexnya) {
        //alert(KataKunci+' '+Indexnya);
        var Registered = '';
        $('#TabelTransaksi tbody tr').each(function () {
            if (Indexnya !== $(this).index()) {
                if ($(this).find('td:nth-child(2)').find('#kode').val() !== '') {
                    Registered += $(this).find('td:nth-child(2)').find('#kode').val() + ',';
                }
            }
        });
        var suplieridnya = $('#id_suplier').val();
        if (Registered !== '') {
            Registered = Registered.replace(/,\s*$/, "");
        }
        var pencarian_kode = $('#TabelTransaksi tbody tr:eq(' + Indexnya + ') td:nth-child(2)').find('#pencarian_kode');
        $.ajax({
            url: "<?php echo site_url('pembelian/cek-kode'); ?>",
            type: "POST",
            cache: false,
            data: 'keyword=' + KataKunci,
            dataType: 'json',
            success: function (json) {
                if (json.status == 1) {
                    call(1, KataKunci, Indexnya);
                }
                if (json.status == 0) {
                    call(0, KataKunci, Indexnya);
                }
            }
        });
    }
</script>
