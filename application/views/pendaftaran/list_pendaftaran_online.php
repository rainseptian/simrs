<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Smart Clinic</title>
    <link rel="icon" href="<?= base_url() ?>/assets/img/profil/logo.png" type="image/gif">
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/bower_components/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/bower_components/font-awesome/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/bower_components/Ionicons/css/ionicons.min.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/bower_components/select2/dist/css/select2.min.css">

    <!-- Theme style -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/dist/css/skins/_all-skins.min.css">

    <!-- jvectormap -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/bower_components/jvectormap/jquery-jvectormap.css">
    <!-- Date Picker -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
    <!-- Daterange picker -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/bower_components/bootstrap-daterangepicker/daterangepicker.css">
    <!-- bootstrap wysihtml5 - text editor -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    

    <!-- Google Font -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    <!-- Javascript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/mustache.js/2.3.0/mustache.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.bundle.min.js"></script>

    <style>
        @media (min-width: 1px) {
            .modal-xl {
                width: 100% !important;
                max-width: 768px !important;
            }
        }

        @media (min-width: 768px) {
            .modal-xl {
                width: 100% !important;
                max-width: 1400px !important;
            }
        }

        .btn-group .dropdown-menu {
            padding-left: 10px;
            padding-right: 10px;
            padding-top: 10px;
            box-shadow: 0 4px 3px 0 rgba(0, 0, 0, 0.1);
            left: -80px;
        }

        body {
            font-family: " Helvetica Neue,Helvetica,sans-serif";
            min-height: 100% !important;
            background-color: #ecf0f5 !important;
        }

        {
            font-size: 100% !important;
        }

        .vcenter {
            display: inline-block;
            vertical-align: middle;
            float: none;
        }
    </style>
</head>

<body style="height: 100% !important;" class="skin-blue">
    <link rel="stylesheet" href="<?= base_url() ?>assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/autocomplete/style-gue.css">

    <header class="main-header">
        
    </header>
    
    <div class="content-wrapper" style="margin-left: 0 !important;">
        <section class="content" id="daftar">
        <div class="row col-sm-12 text-center">
            <div class="row col-sm-12 text-center display-inline-flex">
                <img src="<?= base_url() ?>assets/img/profil/logopinggir.png" onerror="this.onerror=null;this.src='https:<?= base_url() ?>assets/img/logopinggir.png';" class="user-image" alt="User Image" height="70px">
                
            </div>
        </div>
            <div class="row">
                <div class='col-sm-12'>
                    <?php echo form_open('DaftarOnline/lama', array('id' => 'FormStockOpname')); ?>
                    <div class="box box-primary">
                            <div class="alert alert-success alert-dismissible" role="alert" >
                                <h3 class="row text-center" style="margin-top:0rem !important">Ketentuan Pendaftaran Online</h3>
                                <div class="row col-sm-12">
                                    <h4>UMUM</h4>
                                    1. Pendaftaran hanya dilakukan pada hari H <br>
                                    2. Tombol Daftar bisa diakses pada jam 00.00 - 07.00 untuk praktek pagi dokter dan akan dibuka kembali pada jam 09.00-18.00 untuk praktek dokter sore <br>
                                    3. PIlih menu "Pasien Baru" jika anda belum pernah terdaftar di Rumah Sakit Sukma Wijaya <br>
                                    4. Silahkan isi biodata lengkap, pilih dokter spesialis dan poli tujuan <br>
                                    5. Jika pernah periksa sebelumnya, masukkan NIK dan klik verifikasi <br>
                                    5. Silahkan pilih nama dan isi poli dan dokter tujuan, Klik Simpan <br>
                                    6. Jika Batal datang silahkan Telp/Whatsapp pada No. Pendaftaran 0823-3319-5446 <br> <br>
                                    <H4>BPJS</H4>
                                    1. Pastikan sudah memiliki rujukan dari faskes pertama (dokter keluarga, Puskesmas) <br>
                                    2. Pendaftaran online pasien BPJS hanya dapat dilakukan menggunakan Aplikasi Mobile JKN <br>
                                </div>
                                <button style="margin-top: 2rem !important;" type="button" data-dismiss="alert" aria-label="Close"><span class="text-danger"> Saya Telah Mengerti</span></button>
                            </div>
                        <div class="box box-primary">
                            <div class="row ">
                                <div class="row col-sm-6 ">
                                    <h4 class="box-title text-center" style="font-family:sans-serif; margin-top: 2rem !important "><b>Pendaftaran Pasien Baru</b></h4>
                                        <div class="text-center" style="margin: 2rem !important;">
                                            <a href="<?php echo base_url(); ?>DaftarOnline/baru">
                                                <button type="button" style="border: 2%" class="btn btn-md btn-primary p-4"><span class="glyphicon glyphicon-plus"></span>Pasien Baru</button>
                                            </a>
                                        </div>
                                </div>
                                <div class="row col-sm-6">
                                    <h4 class="box-title text-center" style="font-family:sans-serif; margin-top: 2rem !important " ><b>Pendaftaran Pasien Lama</b></h4>
                                        <div class="form-group" style="margin-left: 5rem !important; margin-right: 5rem !important;" id='lama' hidden>
                                            <input type="text" class="form-control text-center" style=" border-radius: 6px;" name="pencarian_kode" id="pencarian_kode" placeholder="Masukkan NIK" autocomplete="off">
                                            <input type="hidden" name="id_pasien" id='id_pasien'>
                                                <div id='hasil_pencarian'></div>
                                                <div class="text-center" style="margin-top: 1rem !important;">
                                                    <button type='submit' class='btn btn-danger'>
                                                        <i class='fa fa-floppy-o'></i> Verifikasi Pasien Lama
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="text-center" style="margin: 2rem !important">
                                            <button type='button' class='btn btn-danger' id='btn' value='click'>
                                                <i class='fa fa-floppy-o'></i> Pasien Lama
                                            </button>
                                        </div>
                                    </div>
                                <?php echo form_close(); ?>
                                <br />
                        </div>
                    </div>
                        <div class="box box-primary" style="margin-left: 0.5em !important;">
                            <div class="box-header">
                                <h3 class="box-title">Antrian Pasien</h3>&nbsp;&nbsp;
                            </div>
                            <div class="box-body">
                                <table id="example2" class="table table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th>No Antrian</th>
                                            <th>Nama</th>
                                            <th>Poli Tujuan</th>
                                            <th>Nama Dokter</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($listPendaftaran->result() as $k => $row) : ?>
                                            <?php if (date('Y-m-d', strtotime($row->waktu_pendaftaran)) == date('Y-m-d')) : ?>
                                                <tr>
                                                    <td>
                                                        <?php echo $row->kode_antrian; ?>
                                                        <?php if ($row->is_mobile_jkn) : ?>
                                                            <br>
                                                            <small>
                                                                <span class="label label-danger">Mobile JKN</span>
                                                                <?php if ($row->is_check_in) : ?>
                                                                    <span class="label label-success">Check In</span>
                                                                <?php endif; ?>
                                                            </small>
                                                        <?php endif; ?>
                                                    </td>
                                                    <td>
                                                        <?php echo ucwords($row->nama_pasien); ?>
                                                    </td>
                                                    <td> <?php echo ucwords($row->jenis_pendaftaran); ?></td>
                                                    <td> <?php echo ucwords($row->nama_dokter); ?></td>
                                                </tr>
                                            <?php endif; ?>
                                        <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    
                </div>
        </section>
    </div>
    
    

    <script type="text/javascript">
        const daftar = document.getElementsByClassName('btn');
        const waktu = new Date();
        const jam = waktu.getHours();

        if (jam >= 00 && jam <= 05 || jam >= 09 && jam <= 17) {
            daftar[0].style.display = 'blok';
            daftar[1].style.display = 'blok';
        } else {
            daftar[0].style.display = 'none';
            daftar[1].style.display = 'none';
        }
    </script>

    <script>
        $('#btn').click(function() {
            $('#lama').show();
            $('#btn').hide();
        });
    </script>
    <script>
        $(function() {
            <?php $success = $this->session->flashdata('success');
            if (!empty($success)) { ?>
                Swal.fire({
                    icon: 'success',
                    title: '<?= $success ?>',
                    text: 'Mohon datang 60 menit lebih awal dan lakukan Checkin!'
                }).then((result) => {
                })
            <?php } ?>
            $('#example2').DataTable({
                rowReorder: {
                    selector: 'td:nth-child(2)'
                },
                'paging': true,
                'lengthChange': false,
                'searching': true,
                'ordering': true,
                'info': true,
                'autoWidth': true
            })
        })
    </script>



    <script>
        $('#pencarian_kode').keypress(function(event) {
            if (event.keyCode == 13) {
                event.preventDefault();
            }
        });
    </script>

    <script type="text/javascript">
        var timer = null;
        $(document).on('keyup', '#pencarian_kode', function(e) {
            if ($('#pencarian_kode').val().length > 0) {
                clearTimeout(timer);
                timer = setTimeout(
                    function() {
                        if ($('#pencarian_kode').val() != '') {
                            var charCode = (e.which) ? e.which : event.keyCode;
                            if (charCode == 40) //arrow down
                            {

                                if ($('.form-group').find('div#hasil_pencarian li.autocomplete_active').length > 0) {
                                    var selanjutnya = $('.form-group').find('div#hasil_pencarian li.autocomplete_active').next();
                                    $('.form-group').find('div#hasil_pencarian li.autocomplete_active').removeClass('autocomplete_active');
                                    selanjutnya.addClass('autocomplete_active');
                                } else {
                                    $('.form-group').find('div#hasil_pencarian li:first').addClass('autocomplete_active');
                                }
                            } else if (charCode == 38) //arrow up
                            {
                                if ($('.form-group').find('div#hasil_pencarian li.autocomplete_active').length > 0) {
                                    var sebelumnya = $('.form-group').find('div#hasil_pencarian li.autocomplete_active').prev();
                                    $('.form-group').find('div#hasil_pencarian li.autocomplete_active').removeClass('autocomplete_active');
                                    sebelumnya.addClass('autocomplete_active');
                                } else {
                                    $('.form-group').find('div#hasil_pencarian li:first').addClass('autocomplete_active');
                                }
                            } else if (charCode == 13) // enter
                            {

                                var Kodenya = $('.form-group').find('div#hasil_pencarian li.autocomplete_active span#kodenya').html();
                                var No_rmnya = $('.form-group').find('div#hasil_pencarian li.autocomplete_active span#no_rmnya').html();
                                var Namanya = $('.form-group').find('div#hasil_pencarian li.autocomplete_active span#namanya').html();
                                if (Kodenya) {
                                    $('#pencarian_kode').val(No_rmnya + ' - ' + Namanya);
                                    $('#id_pasien').val(Kodenya);
                                } else {
                                    alert('data tidak ada! Pilih pilihan yang ada sebelum di enter.');
                                }


                                $('.form-group').find('div#hasil_pencarian').hide();

                            } else {
                                var text = $('#FormStockOpname').find('div#hasil_pencarian').html();
                                autoComplete($('#pencarian_kode').width(), $('#pencarian_kode').val());
                            }
                        } else {
                            $('#FormStockOpname').find('div#hasil_pencarian').hide();
                        }
                    }, 100);
            }
        });
        $(document).on('click', '#daftar-autocomplete li', function() {
            $(this).parent().parent().parent().find('#pencarian_kode').val($(this).find('span#no_rmnya').html() + ' - ' + $(this).find('span#namanya').html());
            $(this).parent().parent().parent().find('#id_pasien').val($(this).find('span#kodenya').html());

            $('.form-group').find('#daftar-autocomplete').hide();
        });

        function autoComplete(Lebar, KataKunci) {
            $('div#hasil_pencarian').hide();
            var Lebar = Lebar + 25;

            $.ajax({
                url: "<?php echo site_url('pendaftaran/ajax_kode'); ?>",
                type: "POST",
                cache: false,
                data: 'keyword=' + KataKunci,
                dataType: 'json',
                success: function(json) {
                    if (json.status == 1) {
                        $('#FormStockOpname').find('div#hasil_pencarian').css({
                            'width': Lebar + 'px'
                        });
                        $('#FormStockOpname').find('div#hasil_pencarian').show('fast');
                        $('#FormStockOpname').find('div#hasil_pencarian').html(json.datanya);
                    }
                    if (json.status == 0) {
                        $('#FormStockOpname').find('div#hasil_pencarian').html('');
                    }
                }
            });
        }

        function cekbarcode(KataKunci, Indexnya) {
            //alert(KataKunci+' '+Indexnya);
            var Registered = '';
            $('#TabelTransaksi tbody tr').each(function() {
                if (Indexnya !== $(this).index()) {
                    if ($(this).find('td:nth-child(2)').find('#kode').val() !== '') {
                        Registered += $(this).find('td:nth-child(2)').find('#kode').val() + ',';
                    }
                }
            });
            var suplieridnya = $('#id_suplier').val();
            if (Registered !== '') {
                Registered = Registered.replace(/,\s*$/, "");
            }
            var pencarian_kode = $('#TabelTransaksi tbody tr:eq(' + Indexnya + ') td:nth-child(2)').find('#pencarian_kode');
            $.ajax({
                url: "<?php echo site_url('pembelian/cek-kode'); ?>",
                type: "POST",
                cache: false,
                data: 'keyword=' + KataKunci,
                dataType: 'json',
                success: function(json) {
                    if (json.status == 1) {
                        call(1, KataKunci, Indexnya);
                    }
                    if (json.status == 0) {
                        call(0, KataKunci, Indexnya);
                    }
                }
            });
        }
    </script>


    <script src="<?php echo base_url() ?>assets/bower_components/raphael/raphael.min.js"></script>
    <script src="<?php echo base_url() ?>assets/bower_components/jquery/dist/jquery.min.js"></script>
    <!-- jQuery UI 1.11.4 -->
    <script src="<?php echo base_url() ?>assets/bower_components/jquery-ui/jquery-ui.min.js"></script>
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <script>
        $.widget.bridge('uibutton', $.ui.button);
    </script>
    <!-- Bootstrap 3.3.7 -->
    <script src="<?php echo base_url() ?>assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- DataTables -->
    <script src="<?php echo base_url() ?>assets/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url() ?>assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>

    <!-- Morris.js charts -->
    <!-- <script src="<?php echo base_url() ?>assets/bower_components/morris.js/morris.min.js"></script> -->
    <!-- Sparkline -->
    <script src="<?php echo base_url() ?>assets/bower_components/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
    <!-- jvectormap -->
    <script src="<?php echo base_url() ?>assets/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
    <script src="<?php echo base_url() ?>assets/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
    <!-- jQuery Knob Chart -->
    <script src="<?php echo base_url() ?>assets/bower_components/jquery-knob/dist/jquery.knob.min.js"></script>
    <!-- daterangepicker -->
    <script src="<?php echo base_url() ?>assets/bower_components/moment/min/moment.min.js"></script>
    <script src="<?php echo base_url() ?>assets/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
    <!-- datepicker -->
    <script src="<?php echo base_url() ?>assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
    <!-- Bootstrap WYSIHTML5 -->
    <script src="<?php echo base_url() ?>assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
    <!-- Slimscroll -->
    <script src="<?php echo base_url() ?>assets/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo base_url() ?>assets/bower_components/fastclick/lib/fastclick.js"></script>
    <script src="<?php echo base_url() ?>assets/bower_components/select2/dist/js/select2.full.min.js"></script>
    <!-- AdminLTE App -->
    <script src="<?php echo base_url() ?>assets/dist/js/adminlte.min.js"></script>
    <!-- AdminLTE dashboard demo (This is only for demo purposes) --><!--
<script src="<?php echo base_url() ?>assets/dist/js/pages/dashboard.js"></script>
 --><!-- AdminLTE for demo purposes -->
    <script src="<?php echo base_url() ?>assets/dist/js/demo.js"></script>
</body>

</html>