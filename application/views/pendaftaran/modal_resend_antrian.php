<div class="modal fade" id="resend-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <form id="resend-form" method="post">
            <div class="modal-content">
                <div class="modal-header" style="display: flex">
                    <h4 class="modal-title" id="resend-title" style="flex: 1">Kirim Ulang</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="panel panel-danger">
                        <div class="panel-heading"><h4><b>Pesan eror dari BPJS</b></h4></div>
                        <div class="panel-body" id="resend-msg"></div>
                    </div>
                    <b>Pasien ini gagal tambah antrian BPJS. Mohon masukkan NIK, No BPJS dan No Surat Kontrol:</b>
                    <br>
                    <br>
                    <div class="form-group">
                        <label>NIK</label>
                        <input type="text" class="form-control" id="resend-nik" name="nik" required>
                    </div>
                    <div class="form-group">
                        <label>No BPJS</label>
                        <input type="text" class="form-control" id="resend-no-bpjs" name="nokartu" required>
                    </div>
                    <div class="form-group">
                        <label>No Surat Kontrol</label>
                        <input type="text" class="form-control" id="resend-no-kontrol" name="nomorreferensi" required>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-sm btn-success" value="1">Submit</button>
                </div>
            </div>
        </form>
    </div>
</div>
