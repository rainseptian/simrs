<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Data Stok Bahan Habis Pakai
            <small></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a href="#">Master</a></li>
            <li class="active">Data Stok Bahan Habis Pakai</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <div class="col-sm-8">
                            <h3 class="box-title">Data Master</h3>&nbsp;&nbsp;
                            <button type="button" class="btn btn-primary" id="add_bahan"><span class="glyphicon glyphicon-plus"></span></button>
                        </div>
                        <div class="col-sm-2 pull-right">
                            <form class="form-horizontal" method="post" action="<?php echo base_url()?>export/bhp/stok">
                                <button type="submit" class="btn btn-primary"><i class="fa fa-print"></i> Export Excel</button>
                            </form>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <?php $warning = $this->session->flashdata('warning');
                    if (!empty($warning)){ ?>
                        <div class="alert alert-warning alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <h4><i class="icon fa fa-warning"></i> Alert!</h4>
                            <?php echo $warning ?>
                        </div>
                    <?php } ?>
                    <?php $success = $this->session->flashdata('success');
                    if (!empty($success)){ ?>
                        <div class="alert alert-success alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <h4><i class="icon fa fa-check"></i> Success!</h4>
                            <?php echo $success ?>
                        </div>
                    <?php } ?>
                    <div class="box-body">
                        <table id="example2" class="table table-striped table-hover">
                            <thead>
                            <tr>
                                <th>Nama </th>
                                <th class="text-right">Harga Beli</th>
                                <th class="text-right">Harga Jual</th>
                                <th>Stok</th>
                                <th>Tgl. Update</th>
                                <th></th>
                            </tr>
                            </thead>

                            <tbody>
                            <?php foreach ($bahan as $row) { ?>

                                <tr id="row-<?=$row->id?>">
                                    <td class="nama" data-nama="<?=$row->nama;?>">
                                      <?php echo $row->nama; ?><br>
                                    </td>
                                    <td class="text-right">Rp <?=number_format($row->harga_beli, 2, ',', '.') ?></td>
                                    <td class="text-right">Rp <?=number_format($row->harga_jual, 2, ',', '.') ?></td>
                                    <td> <?php echo $row->jumlah; ?> <span class="satuan" data-satuan="<?=$row->satuan?>"><?=$row->satuan?></span> </td>
                                    <td><?=($row->updated_at ? date('d M Y, H:i', strtotime( $row->updated_at ) ) : date('d M Y, H:i', strtotime( $row->created_at ) ))?></td>
                                    <td class="text-right" style="width: 70px;">
                                        <button type="button" value="<?=$row->id?>" class="btn btn-sm btn-success do_update"> <i class="fa fa-edit"></i></button>
                                        <a onclick="return confirm('Apakah Anda ingin menghapus data ini?');" href="<?php  echo base_url(); ?>BahanHabisPakai/delete/<?php echo $row->id; ?>" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></a>
                                    </td>
                                </tr>

                              <?php } ?>


                            </tbody>

                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->

            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->

</div>

<div class="modal fade" id="form_bahan" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"></h4>
      </div>
      <form method="post" role="form">
        <input type="hidden" name="id" value="0">
        <div class="modal-body">
          <div class="form-group">
            <label for="nama">Nama Barang Habis Pakai</label>
            <input type="text" class="form-control" name="nama" id="nama" value="" required>
          </div>
          <div class="form-group">
            <div class="row">
              <div class="col-xs-6">
                <label for="satuan">Satuan</label>
                <input type="text" class="form-control" name="satuan" id="satuan" value="" required>
              </div>
            </div>
          </div>

        </div>
        <div class="modal-footer">
          <button type="submit" name="do_save" class="btn btn-primary" value="abdush"> <i class="fa fa-floppy-o"></i> Simpan</button>
          <button type="button" name="do_cancel" class="btn btn-danger" data-dismiss="modal"> <i class="fa fa-close"></i> Batal</button>

        </div>
      </form>

    </div>
  </div>
</div>

<script>
    $(function () {
        $('#example1').DataTable()
        $('#example2').DataTable({
            rowReorder: {
                selector: 'td:nth-child(2)'
            },
            'paging'      : true,
            'lengthChange': true,
            'searching'   : true,
            'ordering'    : true,
            'info'        : true,
            'autoWidth'   : true
        });

        $('#add_bahan').on('click', function(){
          $('#form_bahan input[name="id"]').val(0);
          $('#form_bahan input[name="nama"]').val('');
          $('#form_bahan input[name="satuan"]').val('');
          $('#form_bahan .modal-title').text('Tambah Bahan Habis Pakai');
          $('#form_bahan').modal('show');
          $('#form_bahan').on('shown.bs.modal', function(){
            $('#form_bahan').find('input[name="nama"]').focus();
          });
        });
        $('.do_update').each(function(){
          var theButton= $(this);
          theButton.on('click', function(){
            var id= theButton.val();
            var nama = $('#row-'+ id).find('.nama').data('nama');
            var satuan = $('#row-'+ id).find('.satuan').data('satuan');
            $('#form_bahan input[name="id"]').val(id);
            $('#form_bahan input[name="nama"]').val(nama);
            $('#form_bahan input[name="satuan"]').val(satuan);
            $('#form_bahan .modal-title').text('Edit Bahan Habis Pakai');
            $('#form_bahan').modal('show');
            $('#form_bahan').on('shown.bs.modal', function(){
              $('#form_bahan').find('input[name="nama"]').focus();
            });
          });
        });
    })
</script>
