<?php
if ($is_edit) {
    $list_bahan = unserialize($pembelian->list_bahan);
}
?>

<style media="screen">
    .select2-container {
        width: 100% !important;
    }
</style>

<div class="content-wrapper">

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <?= ($is_edit ? 'Edit' : 'Tambah') ?> Data Pembelian Bahan Habis Pakai
            <small>Preview</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url() ?>Dashbord"><i class="fa fa-dashboard"></i> Dashbord</a></li>
            <li class="active">Tambah Pembelian Bahan Habis Pakai</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-3">
                <form class="form-horizontal" method="post"
                      action="<?php echo base_url() ?>BahanHabisPakai/<?= ($is_edit ? 'editPembelian/' . $pembelian->id : 'tambahPembelian') ?>">
                    <input type="hidden" name="id_transaksi" value="<?= ($is_edit ? $pembelian->id : 0) ?>">
                    <div class="box box-primary">
                        <div class="box-header">
                            <h3 class="box-title"> Data Pembelian Bahan Habis Pakai</h3>
                        </div>

                        <?php $warning = $this->session->flashdata('warning');
                        if (!empty($warning)) { ?>
                            <div class="alert alert-warning alert-dismissible">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;
                                </button>
                                <h4><i class="icon fa fa-warning"></i> Alert!</h4>
                                <?php echo $warning ?>
                            </div>
                        <?php } ?>
                        <?php $success = $this->session->flashdata('success');
                        if (!empty($success)) { ?>
                            <div class="alert alert-success alert-dismissible">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;
                                </button>
                                <h4><i class="icon fa fa-check"></i> Success!</h4>
                                <?php echo $success ?>
                            </div>
                        <?php } ?>

                        <div class="box-body">
                            <div class="form-group">
                                <label for="no_faktur" class="col-sm-12">No Faktur</label>
                                <div class="col-sm-12">
                                    <input type="text" class="form-control" id="no_faktur" name="no_faktur"
                                           placeholder="Masukkan No Faktur"
                                           value="<?= ($is_edit ? $pembelian->no_faktur : '') ?>" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="tgl_faktur" class="col-sm-12"> Tanggal Faktur</label>
                                <div class="col-sm-12">
                                    <input type="date" class="form-control" id="tgl_faktur" name="tgl_faktur"
                                           placeholder="Masukkan Tanggal Faktur"
                                           value="<?= ($is_edit ? date('Y-m-d', strtotime($pembelian->tgl_faktur)) : '') ?>"
                                           required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="tgl_jatuh_tempo" class="col-sm-12"> Tanggal Jatuh Tempo</label>
                                <div class="col-sm-12">
                                    <input type="date" class="form-control" id="tgl_jatuh_tempo" name="tgl_jatuh_tempo"
                                           value="<?= ($is_edit ? date('Y-m-d', strtotime($pembelian->tgl_jatuh_tempo)) : '') ?>"
                                           placeholder="Masukkan Tanggal Jatuh Tempo" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="nama_distributor" class="col-sm-12">Nama Distributor</label>
                                <div class="col-sm-12">
                                    <input type="text" class="form-control" id="nama_distributor"
                                           name="nama_distributor"
                                           value="<?= ($is_edit ? $pembelian->nama_distributor : '') ?>"
                                           onkeyup="set_distributor()" placeholder="Masukkan Nama Distributor" required>
                                </div>
                            </div>
                            <div class="box-footer">
                                <button type="submit" name="submit" value="1"
                                        class="btn btn-primary btn-lg btn-flat pull-right">Simpan
                                </button>
                                <a href="<?php echo base_url() ?>BahanHabisPakai/pembelian"
                                   class="btn btn-default btn-lg btn-flat pull-right">Batal</a>
                            </div>
                        </div>
                    </div>
                    <div id="hiddenFormObat">
                        <?php
                        if ($is_edit) {
                            $lastid = 0;
                            foreach ($list_bahan as $key => $value) {
                                $lastid = $key;
                                ?>
                                <div data-rowid="<?= $key ?>">
                                    <input type="hidden" name="obat[row-<?= $key ?>][id_obat]"
                                           value="<?= $value['id_obat'] ?>">
                                    <input type="hidden" name="obat[row-<?= $key ?>][name]"
                                           value="<?= $value['name'] ?>">
                                    <input type="hidden" name="obat[row-<?= $key ?>][distributor]"
                                           value="<?= $value['distributor'] ?>">
                                    <input type="hidden" name="obat[row-<?= $key ?>][harga_beli]"
                                           value="<?= $value['harga_beli'] ?>">
                                    <input type="hidden" name="obat[row-<?= $key ?>][harga_jual]"
                                           value="<?= $value['harga_jual'] ?>">
                                    <input type="hidden" name="obat[row-<?= $key ?>][jumlah]"
                                           value="<?= $value['jumlah'] ?>">
                                    <input type="hidden" name="obat[row-<?= $key ?>][satuan]"
                                           value="<?= $value['satuan'] ?>">
                                    <input type="hidden" name="obat[row-<?= $key ?>][subtotal]"
                                           value="<?= $value['subtotal'] ?>">
                                </div>

                                <?php
                            }
                        }
                        ?>

                        <input type="hidden" name="lastIndex" value="<?= ($is_edit ? ($lastid + 1) : 0) ?>">
                        <input type="hidden" name="total" value="<?= ($is_edit ? $pembelian->total : 0) ?>">

                    </div>
                </form>
            </div>
            <div class="col-md-9">
                <div class="box box-danger">
                    <div class="box-header">
                        <h3 class="box-title"> Bahan</h3>
                        <!-- nama, kategori, nomor_batch, kadaluwarsa, distributor, harga_beli, harga_jual, stok_obat -->
                        <ul class="pagination pagination-sm no-margin pull-right">
                            <li>
                                <button id="tombol-tambah-obat" type="button" class="btn btn-sm btn-primary"
                                        name="button"><i class="fa fa-plus"></i> Tambah Bahan
                                </button>
                            </li>
                        </ul>
                    </div>
                    <div class="box-body no-padding">
                        <div class="table-responsive">
                            <table class="table table-striped table-obat">
                                <tr>
                                    <th>Bahan</th>
                                    <th>Distributor</th>
                                    <th>Expired</th>
                                    <th>Harga</th>
                                    <th>Jml</th>
                                    <th class="text-right">Subtotal</th>
                                    <th class="text-right"></th>
                                </tr>
                                <?php if ($is_edit) {
                                    foreach ($list_bahan as $key => $value) { ?>

                                        <tr class="row-obat" data-rowid="<?= $key ?>">
                                            <td>
                                                <span class="name"><?= $value['name'] ?></span>
                                            </td>
                                            <td><?= $value['distributor'] ?></td>
                                            <td>
                                                Rp <?= number_format($value['harga_beli'], 2, ',', '.') ?>
                                            </td>
                                            <td>
                                                Rp <?= number_format($value['harga_beli'], 2, ',', '.') ?>
                                            </td>
                                            <td><?= $value['jumlah'] ?> <?= $value['satuan'] ?></td>
                                            <td class="text-right">
                                                Rp <?= number_format($value['subtotal'], 2, ',', '.') ?></td>
                                            <td class="text-right">
                                                <button type="button" class="btn btn-sm btn-danger delete"
                                                        data-rowid="<?= $key ?>"><i class="fa fa-trash"></i></button>
                                            </td>
                                        </tr>

                                    <?php }
                                } else { ?>
                                    <tr id="obat-kosong">
                                        <td colspan="7" class="text-center">Masukkan data bahan disini</td>
                                    </tr>
                                <?php } ?>
                                <tr class="calculate">
                                    <th colspan="5" class="text-right">Total</th>
                                    <td class="text-right total">
                                        Rp <?= ($is_edit ? number_format($pembelian->total, 2, ',', '.') : 0) ?></td>
                                    <td></td>
                                </tr>

                            </table>

                        </div>
                        <div class="box-footer">
                            <input type="hidden" name="total_obat" id="total_obat" value="0"/>
                            <div id="btn_tambah_template" style="display:none;">
                                <div class="box-footer">
                                    <a id="tambah_obat" class="btn btn-primary btn-lg btn-flat pull-right">Tambah
                                        Bahan</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal fade" id="form-obat" role="dialog" aria-labelledby="formObat">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i
                                        class="fa fa-times-circle-o"></i></button>
                            <h4 class="modal-title">Form Bahan</h4>
                        </div>
                        <div class="modal-body">
                            <form class="form-horizontal" method="post">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="nama_obat" class="col-sm-4 control-label"
                                                   style="font-size: small">Nama Obat</label>
                                            <div class="col-sm-8">
                                                <select class="form-control" name="nama_obat" id="nama_obat"
                                                        onchange="onObatSelected()">
                                                    <option value="">--Pilih Bahan--</option>
                                                    <?php foreach ($bahan as $key => $value) { ?>
                                                        <option value="<?php echo $value->id ?>"><?php echo $value->nama ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div>
                                        <input type="hidden" name="name" id="name" value="0"/>
                                        <div class="form-group">
                                            <label for="distributor" class="col-sm-4 control-label"
                                                   style="font-size: small">Distributor</label>
                                            <div class="col-sm-8">
                                                <input type="text" class="form-control" id="distributor"
                                                       name="distributor" placeholder="Masukkan Distributor" required
                                                       value="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="harga_beli" class="col-sm-4 control-label"
                                                   style="font-size: small">Harga Beli</label>
                                            <div class="col-sm-8">
                                                <input type="text" class="form-control" id="harga_beli"
                                                       name="harga_beli" onkeyup="set_harga_jual()"
                                                       placeholder="Masukkan Harga Beli" required>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="stok_obat" class="col-sm-4 control-label"
                                                   style="font-size: small">Margin</label>
                                            <div class="col-sm-8">
                                                <div class="input-group">
                                                    <input type="text" class="form-control" id="margin" name="margin" onkeyup="set_harga_jual()" placeholder="Masukkan Margin" required>
                                                    <span class="input-group-addon">%</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="harga_jual" class="col-sm-4 control-label"
                                                   style="font-size: small">Harga Jual</label>
                                            <div class="col-sm-8">
                                                <input type="text" class="form-control" id="harga_jual"
                                                       name="harga_jual" placeholder="Masukkan Harga Jual" required>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="stok_obat" class="col-sm-4 control-label"
                                                   style="font-size: small">Jumlah</label>
                                            <div class="col-sm-3">
                                                <input type="text" class="form-control" id="stok_obat" name="stok_obat"
                                                       required>
                                            </div>
                                            <input type="hidden" class="form-control" id="satuan" name="satuan"
                                                   value="item" placeholder="gr, ml, dll" required>
                                        </div>
                                    </div>
                                </div>

                                <div class="text-right">
                                    <button type="submit" class="btn btn-primary"><i class="fa fa-plus"></i> Tambah
                                    </button>
                                    <button type="button" class="btn btn-default" data-dismiss="modal"><i
                                                class="fa fa-close"></i> Batal
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
    </section>

</div>

<!-- Select2 -->
<script src="<?= base_url() ?>assets/bower_components/select2/dist/js/select2.full.min.js"></script>
<!-- InputMask -->

<script>

    var index = 0;
    var distributor = '';
    var obat = <?php echo json_encode($bahan); ?>;

    function set_harga_jual() {
        var hb = parseInt($('#harga_beli').val()) || 0;
        var persen = parseFloat($('#margin').val().replaceAll(',', '.')) || 0.0;
        var untung = hb*persen/100;
        var harga_jual = hb+untung;
        $('#harga_jual').val(harga_jual);
    }

    function set_harga_beli() {
        //var hj = parseInt($('#harga_jual').val());
        //var persen = <?php //echo $persen->prosentase; ?>//;
        //var persentotal = parseInt(100)+parseInt(persen);
        //var persen_untung = persen/persentotal;
        //var untung = hj*persen_untung;
        //var harga_beli = hj-untung;
        //
        //$('#harga_beli').val(harga_beli);
    }

    function set_distributor() {
        distributor = $('#distributor').val();
    }

    function onObatSelected() {
        for (var i = 0; i < obat.length; i++) {
            var val = $('#nama_obat').val();

            if (val == obat[i].id) {
                $('#name').val(obat[i].nama);
                $('#form-obat input[name="satuan"]').val(obat[i].satuan);
                break;
            }
        }
    }

    function aGetFormData(form) {
        var formData = form.serializeArray();
        var data = [];

        $.each(formData, function (i, item) {
            data[item.name] = item.value;
        });

        return data;
    }

    function number_format(number, decimals, dec_point, thousands_point) {

        if (number == null || !isFinite(number)) {
            throw new TypeError("number is not valid");
        }

        if (!decimals) {
            var len = number.toString().split('.').length;
            decimals = len > 1 ? len : 0;
        }

        if (!dec_point) {
            dec_point = '.';
        }

        if (!thousands_point) {
            thousands_point = ',';
        }

        number = parseFloat(number).toFixed(decimals);

        number = number.replace(".", dec_point);

        var splitNum = number.split(dec_point);
        splitNum[0] = splitNum[0].replace(/\B(?=(\d{3})+(?!\d))/g, thousands_point);
        number = splitNum.join(dec_point);

        return number;
    }

</script>

<!-- InputMask -->
<script src="<?php echo base_url() ?>assets/plugins/input-mask/jquery.inputmask.js"></script>
<script src="<?php echo base_url() ?>assets/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="<?php echo base_url() ?>assets/plugins/input-mask/jquery.inputmask.extensions.js"></script>

<script type="text/javascript">

    //Datemask dd/mm/yyyy
    $('#datemask').inputmask('dd/mm/yyyy', {'placeholder': 'dd/mm/yyyy'})
    //Datemask2 mm/dd/yyyy
    $('#datemask2').inputmask('mm/dd/yyyy', {'placeholder': 'mm/dd/yyyy'})
    //Money Euro
    $('[data-mask]').inputmask()

    $('#nama_obat').select2();

    function initTableObat() {

        if ($('.table-obat .row-obat').length > 0) {
            $('#obat-kosong').remove();
        } else {
            var emptyRow = `
                <tr>
                    <td colspan="7" id="obat-kosong" class="text-center">Masukkan data obat disini</td>
                </tr>
                `;
            $(emptyRow).insertBefore('.table-obat .calculate');
        }
    }

    function initButtonDelete() {

        var deleteButton = $('.table-obat .row-obat .delete');

        deleteButton.each(function (e) {
            $(this).off('click');
        });
        deleteButton.each(function (e) {
            $(this).on('click', function () {
                var c = $(this).data('rowid');
                var currentRow = $('.table-obat .row-obat[data-rowid="' + c + '"]');
                var name = currentRow.find('.name').text();
                var ask = confirm('Apakah anda yakin ingin menghapus?\n' + name);

                if (ask) {
                    var currentSubtotal = parseFloat($('input[name="obat[row-' + c + '][subtotal]"').val());
                    var currentTotal = parseFloat($('#hiddenFormObat input[name="total"]').val()) - currentSubtotal;

                    $('#hiddenFormObat > input[name="total"]').val(currentTotal);
                    $('.table-obat .total').text('Rp ' + number_format(currentTotal, 2, ',', '.'));

                    currentRow.remove();
                    $('#hiddenFormObat div[data-rowid="' + c + '"]').remove();
                    initTableObat();
                }
            });
        });

    }

    $(function () {
        var addButton = $('#tombol-tambah-obat');

        addButton.on('click', function () {
            $('#form-obat').modal({
                backdrop: 'static',
                show: true
            });
        });

        $('#form-obat').on('show.bs.modal', function (e) {
            $('#distributor').val($('#nama_distributor').val());
        });

        $('#form-obat').on('hide.bs.modal', function (e) {
            $('#form-obat form').trigger("reset");
            $('#nama_obat').val('').trigger("change");
            $('#select2-nama_obat-container').text('--Pilih Obat--');
        });

        $('#form-obat form').on('submit', function (e) {
            e.preventDefault();
            var formData = aGetFormData($(this));
            console.log('form data');
            console.log(formData);

            var subtotal = parseFloat(formData['stok_obat']) * parseFloat(formData['harga_beli']);
            var subtotalF = number_format(subtotal, 2, ',', '.');
            var total = parseFloat($('#hiddenFormObat input[name="total"]').val()) + subtotal;

            var rowCounter = $('#hiddenFormObat > input[name="lastIndex"]');
            var currentCount = parseInt(rowCounter.val()) + 1;

            var theRow = `
          <tr class="row-obat" data-rowid="` + currentCount + `">
            <td>
              <span class="name">` + formData['name'] + `</span><br>
            </td>
            <td>` + formData['distributor'] + `</td>
            <td>` + formData['harga_beli'] + `</td>
            <td>` + formData['harga_jual'] + `</td>
            <td class="text-center">` + formData['stok_obat'] + ` ` + formData['satuan'] + `</td>
            <td class="text-right">Rp ` + subtotalF + `</td>
            <td class="text-right">
                <button type="button" class="btn btn-sm btn-danger delete" data-rowid="` + currentCount + `"><i class="fa fa-trash"></i></button>
            </td>
          </tr>
        `;

            var theRowForm = `
            <div data-rowid="` + currentCount + `">
                <input type="hidden" name="obat[row-` + currentCount + `][id_obat]" value="` + formData['nama_obat'] + `">
                <input type="hidden" name="obat[row-` + currentCount + `][name]" value="` + formData['name'] + `">
                <input type="hidden" name="obat[row-` + currentCount + `][distributor]" value="` + formData['distributor'] + `">
                <input type="hidden" name="obat[row-` + currentCount + `][harga_beli]" value="` + formData['harga_beli'] + `">
                <input type="hidden" name="obat[row-` + currentCount + `][margin]" value="` + formData['margin'] + `">
                <input type="hidden" name="obat[row-` + currentCount + `][harga_jual]" value="` + formData['harga_jual'] + `">
                <input type="hidden" name="obat[row-` + currentCount + `][jumlah]" value="` + formData['stok_obat'] + `">
                <input type="hidden" name="obat[row-` + currentCount + `][satuan]" value="` + formData['satuan'] + `">
                <input type="hidden" name="obat[row-` + currentCount + `][subtotal]" value="` + subtotal + `">
            </div>
        `;

            $(theRow).insertBefore('.table-obat .calculate');
            $('#hiddenFormObat').append(theRowForm);

            initButtonDelete();

            $('#hiddenFormObat > input[name="lastIndex"]').val(currentCount);
            $('#hiddenFormObat > input[name="total"]').val(total);
            $('.table-obat .total').text('Rp ' + number_format(total, 2, ',', '.'));

            initTableObat();

            $('#form-obat').modal('hide');
        });

        <?php if ($is_edit) { ?>
        initButtonDelete();
        <?php } ?>
    });
</script>
