<link rel="stylesheet" href="<?php echo base_url() ?>assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Input Resep Ranap
            <small></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a href="#">List Pasien</a></li>
            <li class="active">Input Resep Ranap</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Detail Resep Ranap</h3>&nbsp;&nbsp;
                        <a href="<?=str_replace('detail', 'input', base_url(uri_string()))?>" class="pull-right"> <button type="button" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i> Input Resep</button></a>
                    </div>
                    <!-- /.box-header -->
                    <?php $warning = $this->session->flashdata('warning');
                    if (!empty($warning)){ ?>
                        <div class="alert alert-warning alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <h4><i class="icon fa fa-warning"></i> Alert!</h4>
                            <?php echo $warning ?>
                        </div>
                    <?php } ?>
                    <?php $success = $this->session->flashdata('success');
                    if (!empty($success)){ ?>
                        <div class="alert alert-success alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <h4><i class="icon fa fa-check"></i> Success!</h4>
                            <?php echo $success ?>
                        </div>
                    <?php } ?>
                    <div class="box-body">
                        <div class="table-responsive">
                            <table class="table table-bordered example">
                                <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Tgl Resep</th>
                                    <th>Obat Satuan</th>
                                    <th>Obat Racik</th>
                                    <th width="50"></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                $no = 1;
                                foreach ($resep as $r) : ?>
                                    <tr>
                                        <td><?=$no++?></td>
                                        <td><?=$r->created_at?></td>
                                        <td style="font-size: 12px">
                                            <a onclick="holdModal('tambah_resep', <?=$r->id?>)" class="btn btn-success btn-sm pull-right">
                                                <span class="fa fa-plus"></span> Tambah Obat
                                            </a>
                                            <table class="table table-condensed">
                                                <thead>
                                                <tr>
                                                    <th>Nama Obat</th>
                                                    <th>Jumlah</th>
                                                    <th>Signa</th>
                                                    <th width="20"></th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <?php foreach ($r->obat as $obat) : ?>
                                                    <tr>
                                                        <td><?=$obat->nama?></td>
                                                        <td><?=$obat->jumlah?></td>
                                                        <td><?=$obat->signa?></td>
                                                        <td>
                                                            <form method="post" action="<?=base_url().'Apotek/hapus_obat'?>">
                                                                <input type="hidden" name="current_url" value="<?=base_url(uri_string())?>">
                                                                <input type="hidden" name="ri_resep_obat_id" value="<?=$obat->id?>">
                                                                <input type="hidden" name="obat_id" value="<?=$obat->obat_id?>">
                                                                <button type="submit" onclick="return confirm('Yakin hapus obat ini?')" class="btn btn-danger btn-sm">
                                                                    <span class="fa fa-trash"></span>
                                                                </button>
                                                            </form>
                                                        </td>
                                                    </tr>
                                                <?php endforeach; ?>
                                                </tbody>
                                            </table>
                                        </td>
                                        <td style="font-size: 12px">
                                            <table class="table table-condensed">
                                                <thead>
                                                <tr>
                                                    <th>Racikan</th>
                                                    <th>Signa</th>
                                                    <th>Catatan</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <?php foreach ($r->obat_racik as $racik) : ?>
                                                    <tr>
                                                        <td>
                                                            <table class="table table-condensed">
                                                                <thead>
                                                                <tr>
                                                                    <th>Nama Obat</th>
                                                                    <th>Jumlah</th>
                                                                </tr>
                                                                </thead>
                                                                <tbody>
                                                                <?php foreach ($racik->detail as $detail) : ?>
                                                                    <tr>
                                                                        <td><?=$detail->nama?></td>
                                                                        <td><?=$detail->jumlah?></td>
                                                                    </tr>
                                                                <?php endforeach; ?>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                        <td><?=$racik->signa?></td>
                                                        <td><?=$racik->catatan?></td>
                                                    </tr>
                                                <?php endforeach; ?>
                                                </tbody>
                                            </table>
                                        </td>
                                        <td>
                                            <form method="post" action="<?=base_url().'Apotek/hapus_resep'?>">
                                                <input type="hidden" name="current_url" value="<?=base_url(uri_string())?>">
                                                <input type="hidden" name="ri_resep_id" value="<?=$r->id?>">
                                                <button type="submit" onclick="return confirm('Yakin hapus resep ini?')" class="btn btn-danger btn-sm">
                                                    <span class="fa fa-trash"></span> Hapus Resep
                                                </button>
                                            </form>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-12" id="tess"></div>
        </div>
    </section>
</div>

<div class="modal fade" id="tambah_resep" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-mid" role="document">
        <div class="modal-content modal-media-content">
            <div class="modal-header modal-media-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="box-title">Tambah Resep</h4>
            </div>

            <form id="form_diagnosis" method="post" action="<?php echo base_url() ?>apotek/tambah_obat">
                <div class="modal-body pt0 pb0">
                    <input type="hidden" name="ri_resep_id" id="ri_resep_id">
                    <input type="hidden" name="current_url" value="<?=base_url(uri_string())?>">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-sm-8">
                                    <select id="obat-option" style="width: 100%">
                                        <option value="">-- Pilih Obat --</option>
                                        <?php foreach ($obat1 as $key => $value) { ?>
                                            <option value="<?= $value->id ?>"><?= $value->nama ?></option>
                                        <?php } ?>
                                    </select>
                                </div>

                                <div class="col-sm-4">
                                    <button type="button" name="button"
                                            class="btn btn-sm btn-block btn-primary"
                                            id="button-add-obat"><i class="fa fa-plus"></i>
                                        Tambah obat
                                    </button>
                                    <input type="hidden" id="abdush-counter2" value="0">
                                </div>

                            </div>

                            <div class="form-area-obat" style="margin-top:15px;">
                                <table class="table table-striped table-hover">
                                    <thead>
                                    <tr>
                                        <th>Nama</th>
                                        <th>Stok Obat</th>
                                        <th>Jml</th>
                                        <th>Signa</th>

                                    </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    <button type="submit" id="form_diagnosisbtn"
                            data-loading-text="Menyimpan..."
                            class="btn btn-info pull-right">Simpan</button>
                </div>
            </form>


        </div>
    </div>
</div>

<script src="<?= base_url() ?>assets/bower_components/select2/dist/js/select2.full.min.js"></script>

<script>
    $(function () {
        $('.select2').select2();
        $('#example1').DataTable()
        $('.example2').DataTable({
            rowReorder: {
                selector: 'td:nth-child(2)'
            },
            'paging'      : true,
            'lengthChange': true,
            'searching'   : true,
            'ordering'    : true,
            'info'        : true,
            'autoWidth'   : true
        })

        var obat = <?php echo json_encode($obat1); ?>;
        $('#obat-option').select2();

        $('#button-add-obat').on('click', function (e) {
            var id_Obat = $('#obat-option').val();
            if (id_Obat == '') {
                alert('Anda belum memilih obat !');
            }
            else {
                var theObat = {};
                var counter = parseInt($('#abdush-counter2').val());
                $.each(obat, function (i, v) {
                    if (v.id == id_Obat) {
                        theObat = v;
                        return;
                    }
                });

                var html = `
                <tr>
                  <td>
                    ` + theObat.nama + `
                    <input type="hidden" name="nama_obat[]" value="` + theObat.id + `">
                  </td>
                  <td>` + theObat.stok_obat + ` item </td>
                  <input type="hidden" id="stok` + counter + `" value="` + theObat.stok_obat + `">
                  <td>
                    <input style="width:65px;" type="text" class="form-control" onchange="loadData(` + counter + `);" name="jumlah_satuan[]" id="jumlah_satuan` + counter + `">
                  </td>
                  <td>
                    <input style="width:100px;" type="text" class="form-control"  name="signa_obat[]" id="signa_obat` + counter + `">
                  </td>
                  <td>
                    <button class="btn btn-sm btn-danger btn-delete-row" type="button"> <i class="fa fa-trash"></i> </button>
                  </td>
                </tr>

                `;

                $('.form-area-obat tbody').append(html);
                $('#abdush-counter2').val(counter + 1);
                $('input[id="jumlah_satuan' + counter + '"]').focus();

                $('.btn-delete-row').unbind('click');
                $('.btn-delete-row').each(function () {
                    $(this).on('click', function () {
                        $(this).parents('tr').remove();
                    });
                });
            }

            $('#obat-option').val('').trigger('change');
        });

    })

    function holdModal(modalId, ri_resep_id) {
        const modal = '#' + modalId;
        $(modal).modal({
            backdrop: 'static',
            keyboard: false,
            show: true
        });
        $('#ri_resep_id').val(ri_resep_id);
    }

</script>