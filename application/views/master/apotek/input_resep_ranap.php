
<style media="screen">
    .select2-container {
        width: 100% !important;
    }
</style>
<link rel="stylesheet" href="<?= base_url() ?>assets/plugins/iCheck/all.css">
<form class="form-horizontal" method="post" action="<?= base_url().$redirect ?>">
    <input type="hidden" name="ri_pemeriksaan_id" value="<?=$ri_pemeriksaan_id?>">
    <div class="content-wrapper">
        <section class="content-header">
            <h1>
                Input Resep <?=$jenis_title?>
            </h1>
            <ol class="breadcrumb">
                <li><a href="<?= base_url() ?>Dashbord"><i class="fa fa-dashboard"></i> Dashbord</a></li>
                <li class="active">Input Resep <?=$jenis_title?></li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-12">
                    <div class="box box-success">
                        <div class="box-body">
                            <div class="table-responsive">
                                <table class="table table-striped mb0 font13">
                                    <tbody>
                                    <tr>
                                        <th class="bozerotop">Nama</th>
                                        <td class="bozerotop"><?=$pasien->nama?></td>
                                        <th class="bozerotop">Penanggung Jawab</th>
                                        <td class="bozerotop"><?=$rawat_inap->penanggung_jawab?></td>
                                    </tr>
                                    <tr>
                                        <th class="bozerotop">Jenis Kelamin</th>
                                        <td class="bozerotop"><?=$pasien->jk == 'L' ? 'Laki-laki' : 'Perempuan'?></td>
                                        <th class="bozerotop">Usia</th>
                                        <td class="bozerotop"><?=$pasien->usia?></td>
                                    </tr>
                                    <tr>
                                        <th class="bozerotop">Alamat</th>
                                        <td class="bozerotop"><?=$pasien->alamat?></td>
                                        <th class="bozerotop">No Telp</th>
                                        <td class="bozerotop"><?=$pasien->telepon?></td>
                                    </tr>
                                    <tr>
                                        <th class="bozerotop">Tgl Daftar</th>
                                        <td class="bozerotop"><?=$rawat_inap->created_at?></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-12">
                    <div class="box box-primary">
                        <div class="box-body">
                            <div class="nav-tabs-custom">
                                <ul class="nav nav-tabs">
                                    <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true">Obat
                                            Satuan</a></li>
                                    <li class=""><a href="#tab_2" data-toggle="tab" aria-expanded="false">Obat
                                            Racik</a></li>
                                </ul>
                                <div class="tab-content">
                                    <!-- <div class="tab-pane active" id="tab_1x">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <?php for ($k = 1; $k < 11; $k++) { ?>

                                                    <div class="form-group">
                                                        <label for="obat">Obat <?= $k; ?></label>
                                                        <select class="form-control select2" name="nama_obat[]"
                                                                id="obat<?= $k; ?>">
                                                            <option value="">Pilih Obat</option>
                                                            <?php foreach ($obat->result() as $key => $value) { ?>
                                                                <option value="<?= $value->id ?>"><?= $value->nama ?></option>
                                                            <?php } ?>
                                                        </select>
                                                        <input type="number" class="form-control"
                                                               id="jumlah_satuan<?= $k; ?>"
                                                               onchange="loadData(<?= $k; ?>);"
                                                               name="jumlah_satuan[]" placeholder="Jumlah Satuan">
                                                        <input type="text" class="form-control" name="signa_obat[]"
                                                               placeholder="signa">
                                                    </div>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    </div> -->
                                    <div class="tab-pane active" id="tab_1">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="row">
                                                    <div class="col-sm-8">
                                                        <select id="obat-option">
                                                            <option value="">-- Pilih Obat --</option>
                                                            <?php foreach ($obat1 as $key => $value) { ?>
                                                                <option value="<?= $value->id ?>"><?= $value->nama ?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>

                                                    <div class="col-sm-4">
                                                        <button type="button" name="button"
                                                                class="btn btn-sm btn-block btn-primary"
                                                                id="button-add-obat"><i class="fa fa-plus"></i>
                                                            Tambah obat
                                                        </button>
                                                        <input type="hidden" id="abdush-counter2" value="0">
                                                    </div>

                                                </div>

                                                <div class="form-area-obat" style="margin-top:15px;">
                                                    <table class="table table-striped table-hover">
                                                        <thead>
                                                        <tr>
                                                            <th>Nama</th>
                                                            <th>Stok Obat</th>
                                                            <th>Jml</th>
                                                            <th>Signa</th>

                                                        </tr>
                                                        </thead>
                                                        <tbody>

                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="tab-pane" id="tab_2">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <?php for ($i = 1; $i < 9; $i++) { ?>
                                                    <div class="row ">
                                                        <div class="col-md-12">
                                                            <div style="margin-top: 20px;">
                                                                <label for="obat">Racikan <?= $i; ?></label>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-sm-8">
                                                                    <select name="obat_racikan<?= $i; ?>[]" id="obat-racik-option-<?=$i?>">
                                                                        <option value="">-- Pilih Obat --</option>
                                                                        <?php foreach ($obat1 as $key => $value) { ?>
                                                                            <option value="<?= $value->id ?>"><?= $value->nama ?></option>
                                                                        <?php } ?>
                                                                    </select>
                                                                </div>

                                                                <div class="col-sm-4">
                                                                    <button type="button" name="button"
                                                                            class="btn btn-sm btn-block btn-primary"
                                                                            id="button-add-obat-racik-<?=$i?>"><i class="fa fa-plus"></i>
                                                                        Tambah obat
                                                                    </button>
                                                                    <input type="hidden" id="abdush-counter-<?=$i?>" value="0">
                                                                </div>

                                                            </div>

                                                            <div class="form-area-obat-racik-<?=$i?>" style="margin-top:15px;">
                                                                <table class="table table-striped table-hover">
                                                                    <thead>
                                                                    <tr>
                                                                        <th>Nama</th>
                                                                        <th>Stok Obat</th>
                                                                        <th>Jml</th>
                                                                    </tr>
                                                                    </thead>
                                                                    <tbody>

                                                                    </tbody>
                                                                </table>
                                                                <input type="text"
                                                                       class="form-control"
                                                                       id="signa-obat-racik-<?= $i; ?>"
                                                                       name="signa<?= $i; ?>"
                                                                       placeholder="signa">
                                                                <input type="text"
                                                                       class="form-control"
                                                                       name="catatan<?= $i; ?>"
                                                                       placeholder="catatan">
                                                            </div>
                                                        </div>

                                                    </div>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <label for="jasa_racik">Jasa Racik</label>
                                    <input type="number" autocomplete="off" class="form-control" id="jasa_racik" name="jasa_racik">
                                </div>
                            </div>
                        </div>
                        <input type="hidden" name="insert" value="1">
                        <div class="box-footer">
                            <button type="submit" name="submit" value="1"
                                    class="btn btn-primary btn-lg btn-flat pull-right">Simpan
                            </button>
                            <?php if ($ri_pemeriksaan_id) : ?>
                                <a href="<?= base_url() ?>RawatInap/detail/<?=$rawat_inap->id?>" class="btn btn-default btn-lg btn-flat pull-right">Batal</a>
                            <?php else : ?>
                                <a href="<?= base_url() ?>pemeriksaan/listpemeriksaanPasien" class="btn btn-default btn-lg btn-flat pull-right">Batal</a>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</form>

<div id='ResponseInput'></div>

<!-- Select2 -->
<script src="<?= base_url() ?>assets/bower_components/select2/dist/js/select2.full.min.js"></script>
<!-- InputMask -->

<script type="text/javascript">
    $('.select2').select2();

    function set_bmi() {
        var tb = $('#tb').val();
        var bb = $('#bb').val();
        var tbm = tb / 100;
        var bmi = bb / (tbm * tbm);

        $('#bmi').val(bmi.toFixed(2));
    }

    function loadData(i) {
        var id = $('#obat' + i).val();
        var jumlah_satuan = $('#jumlah_satuan' + i).val();
        var stok = $('#stok' + i).val();
        var urls = "<?= base_url(); ?>obat/getStokObat";
        var datax = {"id": id};

        if (parseInt(stok) < parseInt(jumlah_satuan)) {
            alert('Stok obat tidak cukup. Silahkan kurangi jumlah atau ganti obat lain!');
            $('#jumlah_satuan' + i).val('');
        }
   }

    for (var i = 1; i < 11; i++) {
        for (var j = 1; j < 3; j++) {
            function loadDataRacikan(i, j) {
                var ij = (i.toString() + j.toString());


                var id = $('#obat_racikan' + ij).val();
                var jumlah_satuan = $('#jumlah_satuan' + ij).val();
                var urls = "<?= base_url(); ?>obat/getStokObat";
                var datax = {"id": id};

                $.ajax({
                    type: 'GET',
                    url: urls,
                    data: datax,

                    success: function (stok) {
                        if (parseInt(stok) < parseInt(jumlah_satuan)) {
                            alert('Stok obat tidak cukup. Silahkan kurangi jumlah atau ganti obat lain!');
                            $('#jumlah_satuan' + ij).val('');
                        }


                    }
                });
            }
        }
    }

</script>

<script type="text/javascript">
    jQuery(function ($) {
        var bahan = <?php echo json_encode($bahan); ?>;
        $('#bahan-option').select2();

        var obat = <?php echo json_encode($obat1); ?>;
        $('#obat-option').select2();

        $('#button-add-bahan').on('click', function (e) {
            var id_bahan = $('#bahan-option').val();
            if (id_bahan == '') {
                alert('Anda belum memilih bahan habis pakai !');
            }
            else {
                var theBahan = {};
                var counter = parseInt($('#abdush-counter2').val());
                $.each(bahan, function (i, v) {
                    if (v.id == id_bahan) {
                        theBahan = v;
                        return;
                    }
                });

                var html = `
                <tr>
                  <td>
                    ` + theBahan.nama + `
                    <input type="hidden" name="id[]" value="` + theBahan.id + `">
                  </td>
                  <td>` + theBahan.jumlah + ` ` + theBahan.satuan + `</td>
                  <td>
                    <input style="width:65px;" type="text" class="form-control" name="qty[]" id="bahan[` + counter + `][qty]">
                  </td>
                  <td>
                    <button class="btn btn-sm btn-danger btn-delete-row" type="button"> <i class="fa fa-trash"></i> </button>
                  </td>
                </tr>`;

                $('.form-area tbody').append(html);
                $('#abdush-counter').val(counter + 1);
                $('input[name="bahan[' + counter + '][qty]"]').focus();

                $('.btn-delete-row').unbind('click');
                $('.btn-delete-row').each(function () {
                    $(this).on('click', function () {
                        $(this).parents('tr').remove();
                    });
                });
            }

            $('#bahan-option').val('').trigger('change');
        });

        $('#button-add-obat').on('click', function (e) {
            var id_Obat = $('#obat-option').val();
            if (id_Obat == '') {
                alert('Anda belum memilih obat !');
            }
            else {
                var theObat = {};
                var counter = parseInt($('#abdush-counter2').val());
                $.each(obat, function (i, v) {
                    if (v.id == id_Obat) {
                        theObat = v;
                        return;
                    }
                });

                var html = `
                <tr>
                  <td>
                    ` + theObat.nama + `
                    <input type="hidden" name="nama_obat[]" value="` + theObat.id + `">
                  </td>
                  <td>` + theObat.stok_obat + ` item </td>
                  <input type="hidden" id="stok` + counter + `" value="` + theObat.stok_obat + `">
                  <td>
                    <input style="width:65px;" type="text" class="form-control" onchange="loadData(` + counter + `);" name="jumlah_satuan[]" id="jumlah_satuan` + counter + `">
                  </td>
                  <td>
                    <input style="width:100px;" type="text" class="form-control"  name="signa_obat[]" id="signa_obat` + counter + `">
                  </td>
                  <td>
                    <button class="btn btn-sm btn-danger btn-delete-row" type="button"> <i class="fa fa-trash"></i> </button>
                  </td>
                </tr>

                `;

                $('.form-area-obat tbody').append(html);
                $('#abdush-counter2').val(counter + 1);
                $('input[id="jumlah_satuan' + counter + '"]').focus();

                $('.btn-delete-row').unbind('click');
                $('.btn-delete-row').each(function () {
                    $(this).on('click', function () {
                        $(this).parents('tr').remove();
                    });
                });
            }

            $('#obat-option').val('').trigger('change');
        });

        let initializer = {
            init: function() {
                for (let i = 1; i < 9; i++) {
                    $(`#obat-racik-option-${i}`).select2();
                    $(`#button-add-obat-racik-${i}`).on('click', e => {
                        this.onBtnAddClick(i);
                    });
                }
            },
            checkHasObat: function(i) {
                let rowCount = $(`.form-area-obat-racik-${i} >table >tbody >tr`).length;
                if (rowCount === 0) {
                    $(`#signa-obat-racik-${i}`).prop('required',false);
                }
            },
            onBtnAddClick: function(i) {
                let that = this;
                let opt = $(`#obat-racik-option-${i}`);
                let id_Obat = opt.val();
                if (id_Obat === '') {
                    alert('Anda belum memilih obat !');
                }
                else {
                    let theObat = {};
                    let counter = parseInt($(`#abdush-counter-${i}`).val());
                    $.each(obat, function (i, v) {
                        if (v.id === id_Obat) {
                            theObat = v;
                            return;
                        }
                    });

                    $(`.form-area-obat-racik-${i} tbody`).append(this.getTableRow(i, theObat, counter));
                    $(`#signa-obat-racik-${i}`).prop('required',true);
                    $(`#abdush-counter-${i}`).val(counter + 1);
                    $('input[id="jumlah_satuan_racik' + counter + '"]').focus();

                    $('.btn-delete-row').unbind('click');
                    $('.btn-delete-row').each(function () {
                        $(this).on('click', function () {
                            $(this).parents('tr').remove();
                            that.checkHasObat(i);
                        });
                    });
                }
                opt.val('').trigger('change');
            },
            getTableRow: (i, obat, counter) => (`
                <tr>
                  <td>
                    ` + obat.nama + `
                    <input type="hidden" name="nama_obat_racikan${i}[]" value="` + obat.id + `">
                  </td>
                  <td>` + obat.stok_obat + ` item </td>
                  <input type="hidden" id="stok` + counter + `" value="` + obat.stok_obat + `">
                  <td>
                    <input style="width:65px;" type="text" class="form-control" onchange="loadData(` + counter + `);" name="jumlah_satuan_racikan${i}[]" id="jumlah_satuan_racikan${i}` + counter + `">
                  </td>
                  <td>
                    <button class="btn btn-sm btn-danger btn-delete-row" type="button"> <i class="fa fa-trash"></i> </button>
                  </td>
                </tr>
            `)
        };

        initializer.init();
    });
</script>
