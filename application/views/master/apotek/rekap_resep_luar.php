<link rel="stylesheet" href="<?php echo base_url() ?>assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Rekapitulasi Resep Luar
            <small></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a href="#">Billing</a></li>
            <li class="active"> Rekapitulasi Resep Luar</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Data Rekapitulasi Resep Luar</h3>&nbsp;&nbsp;
                    </div>
                    <form class="form-horizontal" method="get" action="<?php echo base_url()?>apotek/rekapitulasi_resep_luar">
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="form-horizontal">
                                    <div class="form-group">
                                        <div class="col-sm-1"></div>
                                        <div class="col-sm-7">
                                            <label class="control-label">Tanggal</label>
                                            <input type='date' name='tgl' class='form-control' id='tgl' value="<?php echo ($this->input->get('tgl'))?$this->input->get('tgl'):date('Y-m-d') ?>">
                                            <br>
                                            <button type="submit" class="btn btn-primary" style='margin-left: 0px;'>Tampilkan</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                    <?php $warning = $this->session->flashdata('warning');
                    if (!empty($warning)){ ?>
                        <div class="alert alert-warning alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <h4><i class="icon fa fa-warning"></i> Alert!</h4>
                            <?php echo $warning ?>
                        </div>
                    <?php } ?>
                    <?php $success = $this->session->flashdata('success');
                    if (!empty($success)){ ?>
                        <div class="alert alert-success alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <h4><i class="icon fa fa-success"></i> Success!</h4>
                            <?php echo $success ?>
                        </div>
                    <?php } ?>
                    <div class="box-body">
                        <table id="printable_table" class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>No</th>
                                <th>Nama Pasien</th>
                                <th>Alamat</th>
                                <th>Nama Dokter</th>
                                <th>Obat</th>
                                <th>Obat Racikan</th>
                                <th>Jasa Racik</th>
                                <th>Total Bayar</th>
                                <th>Status</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $no=1; foreach ($penjualan as $row) { ?>
                                <tr>
                                    <td> <?php echo $no; ?></td>
                                    <td> <?php echo ucwords($row->nama_pasien); ?></td>
                                    <td> <?php echo ucwords($row->alamat); ?></td>
                                    <td> <?php echo ucwords($row->nama_dokter); ?></td>
                                    <td>
                                        <a style="cursor: pointer" onclick="open_detail(<?=$row->bol_id?>)">
                                            Rp <?= number_format($row->harga, 2, ',', '.') ?>
                                        </a>
                                    </td>
                                    <td>
                                        <a style="cursor: pointer" onclick="open_detail_racikan(<?=$row->id?>)">
                                            Rp <?= number_format($row->harga2, 2, ',', '.') ?>
                                        </a>
                                    </td>
                                    <td><?='Rp '.number_format($row->jasa_racik,2,',','.')?></td>
                                    <td><?='Rp '.number_format($row->total,2,',','.')?></td>
                                    <td>
                                        <span class="pull-right-container">
                                            <small class="label pull-right bg-green">
                                            <?php
                                            echo ucwords(ucwords(str_replace('_', ' ', $row->progress)));
                                            ?>
                                            </small>
                                        </span>
                                    </td>
                                    <td>
                                        <a href="<?=base_url("Administrasi/nota_obat_luar_print/$row->bol_id")?>"
                                           target="_blank"
                                           class="btn btn-sm btn-success">
                                            <i class="fa fa-print"></i>
                                        </a>
                                    </td>
                                </tr>
                                <?php $no++; } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<div class="modal fade" id="detail-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header" style="display: flex">
                <h4 class="modal-title" id="choose-title" style="flex: 1">Detail</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <table class="table table-bordered" id="tbl-detail">
                    <thead>
                    <tr>
                        <td>No</td>
                        <td>Nama</td>
                        <td>Harga</td>
                        <td>Jumlah</td>
                        <td>Subtotal</td>
                    </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="detail-modal-racikan" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header" style="display: flex">
                <h4 class="modal-title" style="flex: 1">Detail Obat Racikan</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p id="loading-text"><i>Mendapatkan data racikan...</i></p>
                <table class="table table-bordered" id="tbl-detail-racikan">
                    <thead>
                    <tr>
                        <th>Nama</th>
                        <th>Obat</th>
                        <th>Signa</th>
                        <th>Catatan</th>
                        <th>Subtotal</th>
                    </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<script>
    function open_detail(bayar_id) {
        $('#choose-title').html(`Detail Obat`)
        $('#detail-modal').modal('show')
        const tbl = $('#tbl-detail tbody')
        tbl.empty()

        $.ajax({
            url: `<?=base_url()?>/Rekapitulasi/get_obat_luar/${bayar_id}`,
            success: function (result) {
                const data = JSON.parse(result)
                let subtotal = 0
                data.forEach((v, k) => {
                    tbl.append(`
                        <tr>
                            <td>${k + 1}</td>
                            <td>${v.item}</td>
                            <td>${formatMoney(+v.harga)}</td>
                            <td>${v.item === 'Bed' ? 1 : v.jumlah}</td>
                            <td>${formatMoney(+v.subtotal)}</td>
                        </tr>
                    `)
                    subtotal += +v.subtotal
                })
                tbl.append(`
                    <tr>
                        <th colspan="4" class="text-right">Total</th>
                        <th><b>${formatMoney(subtotal.toFixed(2))}</b></th>
                    </tr>
                `)
            }
        })
    }

    function open_detail_racikan(pol_id) {
        $('#detail-modal-racikan').modal('show')
        $('#loading-text').show()
        $('#tbl-detail-racikan').hide()

        const tbl = $('#tbl-detail-racikan tbody')
        tbl.empty()

        $.ajax({
            url: `<?=base_url()?>/Rekapitulasi/get_racikan_luar/${pol_id}`,
            success: function(result) {
                const data = JSON.parse(result)

                $('#loading-text').hide()
                $('#tbl-detail-racikan').show()

                let total = 0
                data.forEach((v, k) => {
                    if (v.obat.length) {
                        tbl.append(`
                            <tr>
                                <td>${v.nama_racikan}</td>
                                <td>${buildObatRacikanTable(v)}</td>
                                <td style="max-width: 150px">${v.signa}</td>
                                <td style="max-width: 150px">${v.catatan}</td>
                                <td>${formatMoney(v.total)}</td>
                            </tr>
                        `)
                    }
                    total += +v.total
                })
                tbl.append(`
                    <tr>
                        <th colspan="4" class="text-right">Total</th>
                        <th><b>${formatMoney(total.toFixed(2))}</b></th>
                    </tr>
                `)
            }
        })
    }

    const buildObatRacikanTable = (v) => {
        let subtotal = v.obat.reduce((a, b) => a + (+b.harga_jual * +b.jumlah_satuan), 0)

        const obat = v.obat.map(o => `
            <tr>
                <td>${o.nama}</td>
                <td>${o.jumlah_satuan}</td>
                <td>${formatMoney(o.harga_jual)}</td>
            </tr>
        `).join('')

        return `
            <table class="table">
                <thead>
                    <tr>
                        <th>Nama</th>
                        <th>Jumlah</th>
                        <th>Harga</th>
                    </tr>
                </thead>
                <tbody>${obat}</tbody>
                <tfoot>
                    <tr>
                        <th colspan="2" class="text-right">Total</th>
                        <th><b>${formatMoney(subtotal.toFixed(2))}</b></th>
                    </tr>
                </tfoot>
            </table>
        `
    }
</script>
