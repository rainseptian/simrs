<link rel="stylesheet" href="<?php echo base_url() ?>assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Input Resep VK
            <small></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a href="#">List Pasien</a></li>
            <li class="active">Input Resep VK</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">List Pasien VK</h3>&nbsp;&nbsp;

                    </div>
                    <!-- /.box-header -->
                    <?php $warning = $this->session->flashdata('warning');
                    if (!empty($warning)){ ?>
                        <div class="alert alert-warning alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <h4><i class="icon fa fa-warning"></i> Alert!</h4>
                            <?php echo $warning ?>
                        </div>
                    <?php } ?>
                    <?php $success = $this->session->flashdata('success');
                    if (!empty($success)){ ?>
                        <div class="alert alert-success alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <h4><i class="icon fa fa-check"></i> Success!</h4>
                            <?php echo $success ?>
                        </div>
                    <?php } ?>
                    <div class="box-body">
                        <table class="table table-bordered table-hover example2">
                            <thead>
                            <tr>
                                <th>No</th>
                                <th>Tanggal Daftar</th>
                                <th>NO RM</th>
                                <th>Nama Pasien</th>
                                <th>Alamat</th>
                                <th>Nama Dokter</th>
                                <th>Aksi</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $no=1; foreach ($list as $r) { ?>
                                <tr>
                                    <td> <?php echo $no; ?></td>
                                    <td><?=date('d-F-Y H:i', strtotime($r['transfered_at']))?></td>
                                    <td>
                                        <?=$r['no_rm']?><br>
                                        <?php $label = $r['tipe_pasien'] == 'umum' ? 'label-warning' : 'label-primary'; ?>
                                        <span class="label <?=$label?>"><?=$r['tipe_pasien']?></span>
                                    </td>
                                    <td><?=$r['nama_pasien']?></td>
                                    <td><?=$r['alamat']?></td>
                                    <td><?=$r['nama_dokter']?></td>
                                    <td>
                                        <a href="<?php echo base_url(); ?>apotek/input_resep_ranap/vk/<?php echo $r['id']; ?>/<?php echo $r['transfer_id']; ?>"> <button type="button" class="btn btn-primary"><i class="fa fa-plus"></i> Input Resep</button></a>
                                        <div style="height: 4px"></div>
                                        <a href="<?php echo base_url(); ?>apotek/detail_resep_ranap/vk/<?php echo $r['id']; ?>/<?php echo $r['transfer_id']; ?>"> <button type="button" class="btn btn-warning btn-block"><i class="fa fa-arrows"></i> Detail</button></a>
                                    </td>
                                </tr>
                                <?php $no++; } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-xs-12" id="tess"></div>
        </div>
    </section>
</div>
<script>
    $(function () {
        $('#example1').DataTable()
        $('.example2').DataTable({
            rowReorder: {
                selector: 'td:nth-child(2)'
            },
            'paging'      : true,
            'lengthChange': true,
            'searching'   : true,
            'ordering'    : true,
            'info'        : true,
            'autoWidth'   : true
        })
    })

</script>