<link rel="stylesheet" href="<?php echo base_url() ?>assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Input Resep Rawat Jalan
            <small></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a href="#">List Pasien</a></li>
            <li class="active">Input Resep Rawat Jalan</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">List Pasien Rawat Jalan</h3>&nbsp;&nbsp;

                    </div>
                    <!-- /.box-header -->
                    <?php $warning = $this->session->flashdata('warning');
                    if (!empty($warning)){ ?>
                        <div class="alert alert-warning alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <h4><i class="icon fa fa-warning"></i> Alert!</h4>
                            <?php echo $warning ?>
                        </div>
                    <?php } ?>
                    <?php $success = $this->session->flashdata('success');
                    if (!empty($success)){ ?>
                        <div class="alert alert-success alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <h4><i class="icon fa fa-check"></i> Success!</h4>
                            <?php echo $success ?>
                        </div>
                    <?php } ?>
                    <div class="box-body">
                        <table class="table table-bordered table-hover example2">
                            <thead>
                            <tr>
                                <th>No</th>
                                <th>NO RM</th>
                                <th>Nama Pasien</th>
                                <th>Alamat</th>
                                <th>Tanggal Daftar</th>
                                <th>Nama Poli</th>
                                <th>Status</th>
                                <th>Sudah Obat</th>
                                <th>Aksi</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $no=1; foreach ($listPendaftaran as $row) { ?>
                                <tr>
                                    <td> <?php echo $no; ?></td>
                                    <td style="width:100px;">
                                        <?php echo $row->no_rm; ?><br>
                                        <small>
                                            <span class="label <?=$jaminan[$row->jaminan]['class']?>"><?=$jaminan[$row->jaminan]['label']?></span>
                                            <?php if (!isset($jaminan[$row->jaminan])) { ?>
                                                <span class="label label-warning">Umum</span>
                                            <?php } ?>
                                        </small>
                                    </td>
                                    <td> <?php echo ucwords($row->nama_pasien); ?></td>
                                    <td> <?php echo ucwords($row->alamat); ?></td>
                                    <td> <?php echo date('d-F-Y', strtotime($row->waktu_pemeriksaan)); ?></td>
                                    <td><?=$row->jenis_pendaftaran?></td>
                                    <td>
                                        <span class="pull-right-container">
                                            <small class="label pull-right bg-<?=$row->status == 'sudah_periksa' ? 'green' : 'red'?>">
                                                <?php echo $row->status == 'sudah_periksa' ? 'Sudah Input Pemeriksaan' : 'Belum Input Pemeriksaan'; ?>
                                            </small></span></td>
                                    <td> <span class="pull-right-container"><small class="label pull-right bg-<?=$row->sudah_obat ? 'green' : 'yellow'?>"><?php echo ucwords(str_replace('_', ' ', $row->sudah_obat ? 'Sudah' : 'Belum')); ?></small></span></td>
                                    <td>
                                        <a  href="<?php  echo base_url(); ?>apotek/input_resep/<?php echo $row->id; ?>"> <button type="button" class="btn btn-primary"><i class="fa fa-pencil"></i> Input Resep</button></a>
                                    </td>
                                </tr>
                                <?php $no++; } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-xs-12" id="tess"></div>
        </div>
    </section>
</div>
<script>
    $(function () {
        $('#example1').DataTable()
        $('.example2').DataTable({
            rowReorder: {
                selector: 'td:nth-child(2)'
            },
            'paging'      : true,
            'lengthChange': true,
            'searching'   : true,
            'ordering'    : true,
            'info'        : true,
            'autoWidth'   : true
        })
    })

</script>