<link rel="stylesheet" href="<?= base_url() ?>assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
<style>
    table.bor, th.bor, td.bor {
        font-size: 12px;
        border: 1px solid #e5e5e5;
    }
    th, td {
        padding: 0px 5px;
    }
    td.cat {
        min-width: 100px;
        max-width: 300px;
    }
    td.no-data {
        padding: 5px;
        text-align: center;
    }
</style>
<?php
function harga($num)
{
    return number_format($num, 2, ',', '.');
}
?>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Data Rekapitulasi Resep <?= isset($current_place_type) ? $current_place_type : 'Ranap' ?>
            <small></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a href="#">Resep</a></li>
            <li class="active">Data Rekapitulasi Resep <?= isset($current_place_type) ? $current_place_type : 'Ranap' ?></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Data Rekapitulasi Resep <?= isset($current_place_type) ? $current_place_type : 'Ranap' ?></h3>&nbsp;&nbsp;
                    </div>
                    <?php $warning = $this->session->flashdata('warning');
                    if (!empty($warning)){ ?>
                        <div class="alert alert-warning alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <h4><i class="icon fa fa-warning"></i> Alert!</h4>
                           <?= $warning ?>
                        </div>
                    <?php } ?>
                    <?php $success = $this->session->flashdata('success');
                    if (!empty($success)){ ?>
                        <div class="alert alert-success alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <h4><i class="icon fa fa-check"></i> Success!</h4>
                           <?= $success ?>
                        </div>
                    <?php } ?>
                    <div class="box-body">
                        <table id="RekapResepRanap" class="table table-bordered">
                            <thead>
                            <tr>
                                <th>No</th>
                                <th>Tgl Transfer</th>
                                <th>Nama Pasien</th>
                                <th>Nama Dokter</th>
                                <th>Obat</th>
                                <th>Obat Racik</th>
                                <th>Cetak</th>
                            </tr>
                            </thead>
                            <tbody>
                            
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<div class="modal fade" id="modal-default">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Default Modal</h4>
            </div>
            <div class="modal-body">
                <div class="col-xs-12" id="tess">

                </div>
            </div>

        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<script>
    
$(document).ready(function() {   

    dataTable();
});


function formatCurrency(amount) {
    // Convert to a number and format as currency
    return parseFloat(amount).toLocaleString('id-ID', { style: 'currency', currency: 'IDR' });
}


function dataTable(){
    const currentPlaceType = "<?= $_GET['current_place_type']?>";
    var url = 'rekap_resep_ranap_list';
    var query = '';
    if(
        currentPlaceType == 'OK' || 
        currentPlaceType == 'VK' || 
        currentPlaceType == 'HCU' || 
        currentPlaceType == 'Perinatologi'
    ) {
        url = 'rekap_resep_transfer_list';
        query = '?current_place_type=' + currentPlaceType;
    }
    var table = $('#RekapResepRanap').DataTable({
        "pageLength":10,
        "pagingType": "full_numbers",
        "lengthMenu": [[5, 10, 15, 20, -1], [5, 10, 15, 20, "All"]],
        "order" : [[2, "desc"]],
        responsive: true,
        language: {
        search: "_INPUT_",
        "search": "Cari : "
        },
        "columnDefs": [ 
            {
                targets: 'disabled-sorting', 
                orderable: false
            },
            {
                "className": "td-actions text-right", 
                "targets": [ 6 ] 
            }
        ],
        columns: [
            // { responsivePriority: 7 },
            // { responsivePriority: 1 },
            // { responsivePriority: 3 },
            // { responsivePriority: 4 },
            // { responsivePriority: 5 },
            // { responsivePriority: 6 },
            // { responsivePriority: 2 },
            { "data": "row_number" },
            { "data": "tgl" },
            { "data": "nama_pasien" },
            { "data": "nama_dokter" },
            { "data": "obat" },
            { "data": "obat_racik" },
            { "data": "cetak" }
        ],
        "processing": true,
        "serverSide": true,
        ajax:{
            url : "<?= base_url('Apotek/')?>" + url + query ,
            dataSrc : 'data'
        },
        "createdRow": function (row, data, index) {
            // Modify the content of the cells in the 'name' column
            var obat = $('td', row).eq(4);
            var obatRacik = $('td', row).eq(5);
            obat.html(`
                <table style="font-size: 12px" class="bor">
                    <thead>
                        <tr>
                            <th class="bor" style="padding: 0px 5px;">Nama </th>
                            <th class="bor" style="padding: 0px 5px;">Jumlah </th>
                            <th class="bor" style="padding: 0px 5px;">Harga @</th>
                        </tr>
                    </thead>
                    <tbody>
                        ${data.obat ? data.obat.map(obat => `
                            <tr>
                                <td class="bor" style="padding: 0px 5px;">${obat.nama}</td>
                                <td class="bor" style="padding: 0px 5px;">${obat.jumlah}</td>
                                <td class="bor" style="padding: 0px 5px;" class="pull-right">${formatCurrency(obat.harga)}</td>
                            </tr>
                        `).join('') : `
                            <tr>
                                <td class="bor" bgcolor="#FEFFEF" colspan="3" style="text-align: center"> Data Tidak ada</td>
                            </tr>
                        `}
                    </tbody>
                </table>
            `);

            obatRacik.html(`
                <table class="bor">
                    <thead>
                        <tr>
                            <th class="bor">Nama </th>
                            <th class="bor">Signa </th>
                            <th class="bor">Obat </th>
                            <th class="bor">Catatan </th>
                        </tr>
                    </thead>
                    <tbody>
                        ${data.obat_racik ? data.obat_racik.map(row2 => `
                            <tr>
                                <td class="bor" valign="top">${row2.nama_racikan}</td>
                                <td class="bor" valign="top">${row2.signa}</td>
                                <td class="bor">
                                    <table>
                                        <tbody>
                                            ${row2.obat ? row2.obat.map(v => `
                                                <tr>
                                                    <td>${v.nama}</td>
                                                    <td>${v.jumlah}</td>
                                                </tr>
                                            `).join('') : ''}
                                        </tbody>
                                    </table>
                                </td>
                                <td class="bor cat" valign="top">${row2.catatan}</td>
                            </tr>
                        `).join('') : `
                            <tr>
                                <td class="no-data" bgcolor="#FEFFEF" colspan="4"> Data Tidak ada</td>
                            </tr>
                        `}
                    </tbody>
                </table>`);
        },
        stateSave: true
        
        
        }); 

        // Delete a record
        //  table.on( 'click', '.delete', function (e) {
        //     $tr = $(this).closest('tr');
        //     var data = table.row($tr).data();
        //     var id = data[0] + "~a";
        //     var name = document.getElementById(id).innerHTML;
        //     deleteData(name, function(result){
        //       if (result==true)
        //       {
                
        //         $.ajax({
        //           type : "POST",
        //           url : "<?= base_url('mfood/delete/');?>",
        //           data : {id : data[0]},
        //           success : function(data){
        //             console.log(data);
        //             var status = $.parseJSON(data);
        //             if(status['isforbidden']){
        //               window.location = "<?= base_url('Forbidden');?>";
        //             } else {
        //               if(!status['status']){
        //                 for(var i=0 ; i< status['msg'].length; i++){
        //                   var message = status['msg'][i];
        //                   setNotification(message, 3, "bottom", "right");
        //                 }
        //               } else {
        //                 for(var i=0 ; i< status['msg'].length; i++){
        //                   var message = status['msg'][i];
        //                   setNotification(message, 2, "bottom", "right");
        //                 }
        //                 table.row($tr).remove().draw();
        //                 e.preventDefault();
        //               }
        //             }
        //           }
        //         });
        //       }
        //     });
        //  });

  }
  </script>