<link rel="stylesheet" href="<?= base_url() ?>assets/plugins/iCheck/all.css">
<link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/autocomplete/style-gue.css">
<style>
    .select2 {
        width: 100% !important;
    }
</style>
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Resep Luar
            <small>Preview</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?= base_url() ?>Dashboard"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="active">Resep Luar</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <form class="form-horizontal" method="post" id="FormStockOpname" action="<?= base_url() ?>apotek/simpan_resep_luar">
                <div class="col-md-6">
                    <div class="box box-danger">
                        <div class="box-header">
                            <h3 class="box-title"> Tambah Resep Luar </h3>
                        </div>
                        <div class="box-body">
                            <div class="nav-tabs-custom">
                                <ul class="nav nav-tabs">
                                    <li class="active"><a class="tab-left" href="#tab_10" data-toggle="tab" aria-expanded="true">Resep Luar</a></li>
                                    <li class=""><a class="tab-left" href="#tab_20" data-toggle="tab" aria-expanded="false">Resep Poli</a></li>
                                    <li class=""><a class="tab-left" href="#tab_30" data-toggle="tab" aria-expanded="false">Bahan Habis Pakai</a></li>
                                </ul>
                                <div class="tab-content">
                                    <div class="tab-pane active" id="tab_10">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="nama_pasien" class="col-sm-2 control-label">Nama Pasien</label>
                                                    <div class="col-sm-9">
                                                        <input type="text" class="form-control" name="nama_pasien" id="nama_pasien">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="alamat" class="col-sm-2 control-label">Alamat</label>
                                                    <div class="col-sm-9">
                                                        <input type="text" class="form-control" name="alamat" id="alamat">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="nama_dokter" class="col-sm-2 control-label">Nama Dokter</label>
                                                    <div class="col-sm-9">
                                                        <select class="form-control" name="id_dokter" id="id_dokter">
                                                            <option value="">Pilih Dokter</option>
                                                            <?php foreach ($dokter as $d) { ?>
                                                                <option value="<?php echo $d->id; ?>"><?php echo $d->nama; ?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="tab_20">
                                        <div class="container-fluid">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="panel panel-danger">
                                                        <div class="panel-heading"><h4><b>DISCLAIMER</b></h4></div>
                                                        <div class="panel-body">
                                                            Resep Poli adalah resp kedua yang dibrikan oleh dokter, selain resep utama dalam 1x pemeriksaan.<br>
                                                            Contoh : seroang pasien BPJS melakukan pemeriksaan, lau dokter memberikan resep bpjs, dan resep umum.
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class='row'>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <label for="cari" class="control-label">Cari Pasien</label>
                                                        <input type="text" class="form-control" name="pencarian_kode" id="pencarian_kode"
                                                               placeholder="Masukkan Nama / No RM / NIK" autocomplete="off">
                                                        <input type="hidden" name="id_pasien" id='id_pasien'>
                                                        <div id='hasil_pencarian'></div>
                                                    </div>

                                                    <table class="table table-bordered" id="tbl">
                                                        <thead>
                                                        <tr>
                                                            <th>Poli</th>
                                                            <th>Waktu Pemeriksaan</th>
                                                            <th></th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>

                                                        </tbody>
                                                    </table>

                                                    <div class="form-group">
                                                        <label for="cari" class="control-label">Pemeriksaan</label>
                                                        <input type="text" class="form-control" readonly id="pemeriksaan">
                                                        <input type="hidden" name="pemeriksaan_id" id='pemeriksaan_id'>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="cari" class="control-label">Dokter</label>
                                                        <input type="text" class="form-control" readonly id="dokter">
                                                        <input type="hidden" name="dokter_id" id='dokter_id'>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="tab_30">

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="box box-primary">
                        <div class="box-header">
                            <h3 class="box-title" id="title">Obat</h3>
                        </div>
                        <div class="box-body">
                            <div class="nav-tabs-custom">
                                <ul class="nav nav-tabs">
                                    <li class="active"><a class="tab-right" href="#tab_1" data-toggle="tab" aria-expanded="true">Obat Satuan</a></li>
                                    <li class=""><a class="tab-right" href="#tab_2" data-toggle="tab" aria-expanded="false">Obat Racik</a></li>
                                    <li class="hidden"><a class="tab-right" href="#tab_3" data-toggle="tab" aria-expanded="false">Bahan Habis Pakai</a></li>
                                </ul>
                                <div class="tab-content">
                                    <div class="tab-pane-right tab-pane active" id="tab_1">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="row">
                                                    <div class="col-sm-8">
                                                        <select id="obat-option">
                                                            <option value="">-- Pilih Obat --</option>
                                                            <?php foreach ($obat as $key => $value) { ?>
                                                                <option value="<?= $value->id ?>"><?= $value->nama ?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <button type="button" name="button"
                                                                class="btn btn-sm btn-block btn-primary"
                                                                id="button-add-obat"><i class="fa fa-plus"></i>
                                                            Tambah obat
                                                        </button>
                                                        <input type="hidden" id="abdush-counter2" value="0">
                                                    </div>
                                                </div>
                                                <div class="form-area-obat" style="margin-top:15px;">
                                                    <table class="table table-striped table-hover">
                                                        <thead>
                                                        <tr>
                                                            <th>Nama</th>
                                                            <th>Stok Obat</th>
                                                            <th>Jml</th>
                                                            <th>Signa</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>

                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane-right tab-pane" id="tab_2">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <?php for ($i = 1; $i < 9; $i++) { ?>
                                                    <div class="row ">
                                                        <div class="col-md-12">
                                                            <div style="margin-top: 20px;">
                                                                <label for="obat">Racikan <?= $i; ?></label>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-sm-8">
                                                                    <select name="obat_racikan<?= $i; ?>[]"
                                                                            id="obat-racik-option-<?= $i ?>">
                                                                        <option value="">-- Pilih Obat --</option>
                                                                        <?php foreach ($obat as $key => $value) { ?>
                                                                            <option value="<?= $value->id ?>"><?= $value->nama ?></option>
                                                                        <?php } ?>
                                                                    </select>
                                                                </div>

                                                                <div class="col-sm-4">
                                                                    <button type="button" name="button" class="btn btn-sm btn-block btn-primary" id="button-add-obat-racik-<?= $i ?>"><i class="fa fa-plus"></i>
                                                                        Tambah obat
                                                                    </button>
                                                                    <input type="hidden" id="abdush-counter-<?= $i ?>" value="0">
                                                                </div>
                                                            </div>
                                                            <div class="form-area-obat-racik-<?= $i ?>" style="margin-top:15px;">
                                                                <table class="table table-striped table-hover">
                                                                    <thead>
                                                                    <tr>
                                                                        <th>Nama</th>
                                                                        <th>Stok Obat</th>
                                                                        <th>Jml</th>
                                                                    </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                    </tbody>
                                                                </table>
                                                                <input type="text" class="form-control" name="signa<?= $i; ?>" placeholder="signa">
                                                                <input type="text" class="form-control" name="catatan<?= $i; ?>" placeholder="catatan">
                                                            </div>
                                                        </div>
                                                    </div>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane-right tab-pane" id="tab_3">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="row">
                                                    <div class="col-sm-8">
                                                        <select id="bahan-option">
                                                            <option value="">-- Pilih Bahan --</option>
                                                            <?php foreach ($bahan as $key => $value) { ?>
                                                                <option value="<?= $value->id ?>"><?= $value->nama ?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>

                                                    <div class="col-sm-4">
                                                        <button type="button" name="button"
                                                                class="btn btn-sm btn-block btn-primary"
                                                                id="button-add-bahan"><i class="fa fa-plus"></i>
                                                            Tambah Bahan
                                                        </button>
                                                        <input type="hidden" id="abdush-counter2" value="0">
                                                    </div>

                                                </div>

                                                <div class="form-area" style="margin-top:15px;">
                                                    <table class="table table-striped table-hover">
                                                        <thead>
                                                        <tr>
                                                            <th>Nama Bahan</th>
                                                            <th>Stok</th>
                                                            <th>Jml</th>
                                                            <th></th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>

                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="box-footer">
                            <button type="submit" name="submit" value="1" class="btn btn-primary btn-lg btn-flat pull-right">Simpan</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </section>
</div>

<div id='ResponseInput'></div>

<script src="<?= base_url() ?>assets/bower_components/select2/dist/js/select2.full.min.js"></script>
<script type="text/javascript">
    $('.select2').select2()

    for (var i = 1; i < 9; i++) {
        function loadData(i) {

            var id = $('#obat' + i).val();
            var jumlah_satuan = $('#jumlah_satuan' + i).val();
            var urls = "<?= base_url(); ?>obat/getStokObat";
            var datax = {"id": id};

            $.ajax({
                type: 'GET',
                url: urls,
                data: datax,

                success: function (stok) {
                    if (parseInt(stok) < parseInt(jumlah_satuan)) {
                        alert('Stok obat tidak cukup. Silahkan kurangi jumlah atau ganti obat lain!');
                        $('#jumlah_satuan' + i).val('');
                    }


                }
            });
        }

        for (var j = 1; j < 3; j++) {
            function loadDataRacikan(i, j) {
                var ij = (i.toString() + j.toString());


                var id = $('#obat_racikan' + ij).val();
                var jumlah_satuan = $('#jumlah_satuan' + ij).val();
                var urls = "<?= base_url(); ?>obat/getStokObat";
                var datax = {"id": id};

                $.ajax({
                    type: 'GET',
                    url: urls,
                    data: datax,

                    success: function (stok) {
                        if (parseInt(stok) < parseInt(jumlah_satuan)) {
                            alert('Stok obat tidak cukup. Silahkan kurangi jumlah atau ganti obat lain!');
                            $('#jumlah_satuan' + ij).val('');
                        }


                    }
                });
            }
        }
    }

</script>

<script src="<?= base_url() ?>assets/bower_components/select2/dist/js/select2.full.min.js"></script>
<script type="text/javascript">
    jQuery(function ($) {
        var obat = <?php echo json_encode($obat); ?>;
        $('#obat-option').select2();

        $('#button-add-obat').on('click', function (e) {
            var id_Obat = $('#obat-option').val();
            if (id_Obat == '') {
                alert('Anda belum memilih obat !');
            } else {
                var theObat = {};
                var counter = parseInt($('#abdush-counter2').val());
                $.each(obat, function (i, v) {
                    if (v.id == id_Obat) {
                        theObat = v;
                        return;
                    }
                });

                var html = `
                <tr>
                  <td>
                    ` + theObat.nama + `
                    <input type="hidden" name="nama_obat[]" value="` + theObat.id + `">
                  </td>
                  <td>` + theObat.stok_obat + ` item </td>
                  <input type="hidden" id="stok` + counter + `" value="` + theObat.stok_obat + `">
                  <td>
                    <input style="width:65px;" type="text" class="form-control" onchange="loadData(` + counter + `);" name="jumlah_satuan[]" id="jumlah_satuan` + counter + `">
                  </td>
                  <td>
                    <input style="width:100px;" type="text" class="form-control"  name="signa_obat[]" id="signa_obat` + counter + `">
                  </td>
                  <td>
                    <button class="btn btn-sm btn-danger btn-delete-row" type="button"> <i class="fa fa-trash"></i> </button>
                  </td>
                </tr>

                `;

                $('.form-area-obat tbody').append(html);
                $('#abdush-counter2').val(counter + 1);
                $('input[id="jumlah_satuan' + counter + '"]').focus();

                $('.btn-delete-row').unbind('click');
                $('.btn-delete-row').each(function () {
                    $(this).on('click', function () {
                        $(this).parents('tr').remove();
                    });
                });
            }

            $('#obat-option').val('').trigger('change');
        });

        let initializer = {
            init: function () {
                for (let i = 1; i < 9; i++) {
                    $(`#obat-racik-option-${i}`).select2();
                    $(`#button-add-obat-racik-${i}`).on('click', e => {
                        this.onBtnAddClick(i);
                    });
                }
            },
            onBtnAddClick: function (i) {
                let opt = $(`#obat-racik-option-${i}`);
                let id_Obat = opt.val();
                if (id_Obat === '') {
                    alert('Anda belum memilih obat !');
                } else {
                    let theObat = {};
                    let counter = parseInt($(`#abdush-counter-${i}`).val());
                    $.each(obat, function (i, v) {
                        if (v.id === id_Obat) {
                            theObat = v;
                            return;
                        }
                    });

                    $(`.form-area-obat-racik-${i} tbody`).append(this.getTableRow(i, theObat, counter));
                    $(`#abdush-counter-${i}`).val(counter + 1);
                    $('input[id="jumlah_satuan_racik' + counter + '"]').focus();

                    $('.btn-delete-row').unbind('click');
                    $('.btn-delete-row').each(function () {
                        $(this).on('click', function () {
                            $(this).parents('tr').remove();
                        });
                    });
                }
                opt.val('').trigger('change');
            },
            getTableRow: (i, obat, counter) => (`
                <tr>
                  <td>
                    ` + obat.nama + `
                    <input type="hidden" name="nama_obat_racikan${i}[]" value="` + obat.id + `">
                  </td>
                  <td>` + obat.stok_obat + ` item </td>
                  <input type="hidden" id="stok` + counter + `" value="` + obat.stok_obat + `">
                  <td>
                    <input style="width:65px;" type="text" class="form-control" onchange="loadData(` + counter + `);" name="jumlah_satuan_racikan${i}[]" id="jumlah_satuan_racikan${i}` + counter + `">
                  </td>
                  <td>
                    <button class="btn btn-sm btn-danger btn-delete-row" type="button"> <i class="fa fa-trash"></i> </button>
                  </td>
                </tr>
            `)
        };

        initializer.init();
    });
</script>

<script>
    var timer = null;
    $(document).on('keyup', '#pencarian_kode', function (e) {
        if($('#pencarian_kode').val().length > 0){
            clearTimeout(timer);
            timer = setTimeout(
                function () {
                    if ($('#pencarian_kode').val() != '') {
                        var charCode = ( e.which ) ? e.which : event.keyCode;
                        if (charCode == 40) //arrow down
                        {

                            if ($('.form-group').find('div#hasil_pencarian li.autocomplete_active').length > 0) {
                                var selanjutnya = $('.form-group').find('div#hasil_pencarian li.autocomplete_active').next();
                                $('.form-group').find('div#hasil_pencarian li.autocomplete_active').removeClass('autocomplete_active');
                                selanjutnya.addClass('autocomplete_active');
                            } else {
                                $('.form-group').find('div#hasil_pencarian li:first').addClass('autocomplete_active');
                            }
                        }
                        else if (charCode == 38) //arrow up
                        {
                            if ($('.form-group').find('div#hasil_pencarian li.autocomplete_active').length > 0) {
                                var sebelumnya = $('.form-group').find('div#hasil_pencarian li.autocomplete_active').prev();
                                $('.form-group').find('div#hasil_pencarian li.autocomplete_active').removeClass('autocomplete_active');
                                sebelumnya.addClass('autocomplete_active');
                            } else {
                                $('.form-group').find('div#hasil_pencarian li:first').addClass('autocomplete_active');
                            }
                        }
                        else if (charCode == 13) // enter
                        {

                            var Kodenya = $('.form-group').find('div#hasil_pencarian li.autocomplete_active span#kodenya').html();
                            var No_rmnya = $('.form-group').find('div#hasil_pencarian li.autocomplete_active span#no_rmnya').html();
                            var Namanya = $('.form-group').find('div#hasil_pencarian li.autocomplete_active span#namanya').html();
                            if (Kodenya) {
                                $('#pencarian_kode').val(No_rmnya + ' - ' + Namanya);
                                $('#id_pasien').val(Kodenya);
                                get_pemeriksaan()
                            } else {
                                alert('data tidak ada! Pilih pilihan yang ada sebelum di enter.');
                            }


                            $('.form-group').find('div#hasil_pencarian').hide();

                        }
                        else {
                            var text = $('#FormStockOpname').find('div#hasil_pencarian').html();
                            autoComplete($('#pencarian_kode').width(), $('#pencarian_kode').val());
                        }
                    } else {
                        $('#FormStockOpname').find('div#hasil_pencarian').hide();
                    }
                }, 100);
        }
    });
    $(document).on('click', '#daftar-autocomplete li', function () {
        $(this).parent().parent().parent().find('#pencarian_kode').val($(this).find('span#no_rmnya').html() + ' - ' + $(this).find('span#namanya').html());
        $(this).parent().parent().parent().find('#id_pasien').val($(this).find('span#kodenya').html());

        $('.form-group').find('#daftar-autocomplete').hide();
        get_pemeriksaan()
    });

    function autoComplete(Lebar, KataKunci) {
        $('div#hasil_pencarian').hide();
        var Lebar = Lebar + 25;

        $.ajax({
            url: "<?php echo site_url('pendaftaran/ajax_kode'); ?>",
            type: "POST",
            cache: false,
            data: 'keyword=' + KataKunci,
            dataType: 'json',
            success: function (json) {
                if (json.status == 1) {
                    $('#FormStockOpname').find('div#hasil_pencarian').css({'width': Lebar + 'px'});
                    $('#FormStockOpname').find('div#hasil_pencarian').show('fast');
                    $('#FormStockOpname').find('div#hasil_pencarian').html(json.datanya);
                }
                if (json.status == 0) {
                    $('#FormStockOpname').find('div#hasil_pencarian').html('');
                }
            }
        });
    }

    $(function () {
        $("a[href='#tab_10']").on('shown.bs.tab', function(e) {

            $('#pencarian_kode').prop('required', false)
            $('#pemeriksaan_id').prop('required', false)
        })
        $("a[href='#tab_20']").on('shown.bs.tab', function(e) {

            $('#pencarian_kode').prop('required', true)
            $('#pemeriksaan_id').prop('required', true)
        })
    })

    function get_pemeriksaan() {
        const v = $('#id_pasien').val()
        if (v) {
            $.ajax({
                url: `<?php echo base_url(); ?>/pendaftaran/get_pemeriksaan/${v}`,
                type: "POST",
                cache: false,
                dataType: 'json',
                success: function (json) {
                    $('#tbl tbody').empty()
                    json.data.forEach((v, k) => {
                        $('#tbl tbody').append(`
                            <tr>
                                <td>${v.nama_poli}</td>
                                <td>${toIndoDateName(v.created_at, true)}</td>
                                <td>
                                    <button
                                        type="button"
                                        class="btn btn-success btn-sm"
                                        onclick="pilih_poli(${v.id}, '${v.nama_poli}', '${toIndoDateName(v.created_at, true)}', ${v.dokter_id}, '${v.nama_dokter}')">
                                            Pilih
                                        </button>
                                </td>
                            </tr>
                        `)
                    })
                }
            })
        }
    }

    function pilih_poli(id, poli, waktu, dokter_id, nama_dokter) {
        $('#pemeriksaan_id').val(id)
        $('#pemeriksaan').val(`${poli} - ${waktu}`)
        $('#dokter_id').val(dokter_id)
        $('#dokter').val(nama_dokter)
    }

</script>

<script>
    $(function () {
        $('a.tab-left[data-toggle="tab"]').on('shown.bs.tab', function (e) {
            let target = $(e.target).attr("href")
            $('a.tab-right[data-toggle="tab"]').parent().removeClass('active')
            $('.tab-pane-right').removeClass('active')

            if (target === '#tab_30') {
                $('a.tab-right[href="#tab_1"]').parent().addClass('hidden')
                $('a.tab-right[href="#tab_2"]').parent().addClass('hidden')
                $('a.tab-right[href="#tab_3"]').parent().removeClass('hidden')

                $('a.tab-right[href="#tab_3"]').parent().addClass('active')
                $('#tab_3').addClass('active')

                $('#title').html('Bahan Habis Pakai')
            }
            else {
                $('a.tab-right[href="#tab_1"]').parent().removeClass('hidden')
                $('a.tab-right[href="#tab_2"]').parent().removeClass('hidden')
                $('a.tab-right[href="#tab_3"]').parent().addClass('hidden')

                $('a.tab-right[href="#tab_1"]').parent().addClass('active')
                $('#tab_1').addClass('active')

                $('#title').html('Obat')
            }
        });
    })

    $(function () {

        var bahan = <?php echo json_encode($bahan); ?>;
        $('#bahan-option').select2();

        $('#button-add-bahan').on('click', function (e) {
            var id_bahan = $('#bahan-option').val();
            if (id_bahan == '') {
                alert('Anda belum memilih bahan habis pakai !');
            }
            else {
                var theBahan = {};
                var counter = parseInt($('#abdush-counter2').val());
                $.each(bahan, function (i, v) {
                    if (v.id == id_bahan) {
                        theBahan = v;
                        return;
                    }
                });

                var html = `
                <tr>
                  <td>
                    ` + theBahan.nama + `
                    <input type="hidden" name="id[]" value="` + theBahan.id + `">
                  </td>
                  <td>` + theBahan.jumlah + ` ` + theBahan.satuan + `</td>
                  <td>
                    <input style="width:65px;" type="text" class="form-control" name="qty[]" id="bahan[` + counter + `][qty]">
                  </td>
                  <td>
                    <button class="btn btn-sm btn-danger btn-delete-row" type="button"> <i class="fa fa-trash"></i> </button>
                  </td>
                </tr>`;

                $('.form-area tbody').append(html);
                $('#abdush-counter').val(counter + 1);
                $('input[name="bahan[' + counter + '][qty]"]').focus();

                $('.btn-delete-row').unbind('click');
                $('.btn-delete-row').each(function () {
                    $(this).on('click', function () {
                        $(this).parents('tr').remove();
                    });
                });
            }

            $('#bahan-option').val('').trigger('change');
        });

    })
</script>