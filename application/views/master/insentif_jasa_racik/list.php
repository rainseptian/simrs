<link rel="stylesheet" href="<?php echo base_url() ?>assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">

<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Insentif Jasa Racik
            <small></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a href="#">Master</a></li>
            <li class="active">Data Shift</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <form class="form-horizontal" method="get" action="<?php echo base_url()?>Insentif/listInsentifJasaRacik">
                            <div class="row">
                                <div class="col-sm-5">
                                    <div class="form-horizontal">
                                        <div class="form-group">
                                            <label class="col-sm-4 control-label">Dari Tanggal</label>
                                            <div class="col-sm-8">
                                                <input type='date' name='from' class='form-control' id='tanggal_dari' value="<?php echo ($this->input->get('from')) ? $this->input->get('from') : date('Y-m-01') ?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-5">
                                    <div class="form-horizontal">
                                        <div class="form-group">
                                            <label class="col-sm-4 control-label">Sampai Tanggal</label>
                                            <div class="col-sm-8">
                                                <input type='date' name='to' class='form-control' id='tanggal_sampai' value="<?php echo ($this->input->get('to')) ? $this->input->get('to') : date('Y-m-d') ?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class='row'>
                                <div class="col-sm-3">
                                    <div class="form-horizontal">
                                        <div class="form-group">
                                            <div class="col-sm-4"></div>
                                            <div class="col-sm-8">
                                                <button type="submit" class="btn btn-primary" style='margin-left: 0px;'>Tampilkan</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-success">
                    <div class="box-header">
                        <h3 class="box-title"> Insentif Jasa Racik</h3>&nbsp;&nbsp;
                    </div>
                    <div class="box-body">
                        <table id="printable_table5" class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>No</th>
                                <th>Jenis Pemasukan</th>
                                <th>Jumlah (Rp)</th>
                                <th>Aksi</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $no=1;
                            $subtotal =0;
                            foreach ($total_jasa_racik->result() as $row) { ?>
                                <tr>
                                    <td> <?= $no; ?></td>
                                    <td> <?= ucwords($row->jenis_pendaftaran); ?></td>
                                    <td align="right"> <?=  number_format($row->total,2,',','.'); ?></td>
                                    <td>
                                        <a href="<?= base_url().
                                        'Keuangan/listPemasukan?item='.$row->jenis_item.
                                        '&jp_id='.$row->jenis_pendaftaran_id.
                                        '&jp_name='.urlencode($row->jenis_pendaftaran).
                                        '&jpem='.urlencode('Jasa Obat Racik').
                                        '&from='.$from.
                                        '&to='.$to.
                                        '&table_id='.$row->table_id; ?> ">
                                            <button type="button" class="btn btn-primary pull-right"><i class="fa fa-search"></i> Detail</button>
                                        </a>
                                    </td>
                                </tr>
                                <?php
                                $subtotal = $subtotal +$row->total;
                                $no++; } ?>
                            <tr >
                                <td colspan="2" align="right"><strong>Total Pemasukan Jasa Obat Racik : </strong></td>
                                <td align="right"><strong><?= number_format($subtotal,2,',','.'); ?></strong></td>
                                <td></td>
                                <td style="display: none;"></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<script>
    $(function () {
        $('#example1').DataTable()
        $('#example2').DataTable({
            rowReorder: {
                selector: 'td:nth-child(2)'
            },
            'paging'      : true,
            'lengthChange': true,
            'searching'   : true,
            'ordering'    : true,
            'info'        : true,
            'autoWidth'   : true
        })
    })
</script>