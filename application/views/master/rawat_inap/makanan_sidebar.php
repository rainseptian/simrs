<style type="text/css">
    .tablists{margin: 0; padding: 0; list-style: none;}
    .tablists li{display: block; border-bottom: 1px solid #ddd;}
    .tablists li a{color: #444; padding: 10px;display: block;}
    .tablists li a.active{color: #0084B4; text-decoration: none; font-weight: bold;}
    .tablists li a:hover {color: #0084B4; text-decoration: none;}
</style>
<div class="box box-primary border0">
    <ul class="tablists">
        <li><a href="<?php echo site_url('master/Gizi') ?>" class="<?=$this->uri->segment(2) == 'Gizi' && $this->uri->segment(3) == '' ? 'active' : '' ?>">Makanan</a></li>
        <li><a href="<?php echo site_url('master/Gizi/tipe') ?>" class="<?=$this->uri->segment(2) == 'Gizi' && $this->uri->segment(3) == 'tipe' ? 'active' : ''?>">Tipe Makanan</a></li>
    </ul>
</div>
