<div class="content-wrapper" style="min-height: 348px;">
    <section class="content">
        <div class="row">
            <div class="col-md-2">
                <?php
                $this->load->view('master/rawat_inap/makanan_sidebar');
                ?>
            </div>
            <div class="col-md-10">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title titlefix">Data Makanan</h3>
                        <div class="box-tools pull-right">
                            <a data-toggle="modal" data-target="#myModal" class="btn btn-primary btn-sm add_makanan"><i
                                        class="fa fa-plus"></i> <?php echo $this->lang->line('add'); ?></a>
                        </div><!-- /.box-tools -->
                    </div>
                    <?php $warning = $this->session->flashdata('warning');
                    if (!empty($warning)) { ?>
                        <div class="alert alert-warning alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <h4><i class="icon fa fa-warning"></i> Alert!</h4>
                            <?php echo $warning ?>
                        </div>
                    <?php } ?>
                    <?php $success = $this->session->flashdata('success');
                    if (!empty($success)) { ?>
                        <div class="alert alert-success alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <h4><i class="icon fa fa-check"></i> Success!</h4>
                            <?php echo $success ?>
                        </div>
                    <?php } ?>
                    <div class="box-body">
                        <div class="table-responsive mailbox-messages">
                            <table class="table table-hover table-striped table-bordered example">
                                <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Tipe Makanan</th>
                                    <th>Nama Makanan</th>
                                    <th>Harga</th>
                                    <th>Aksi</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                $no = 1;
                                foreach ($list as $d) : ?>
                                    <tr>
                                        <td><?= $no++ ?></td>
                                        <td><?= $d->tipe ?></td>
                                        <td><?= $d->nama ?></td>
                                        <td><?= $d->harga ?></td>
                                        <td>
                                            <a href="#" onclick="getRecord('<?= $d->id ?>')"
                                               class="btn btn-default btn-xs" data-target="#myModalEdit"
                                               data-toggle="tooltip" title="" data-original-title="Edit">
                                                <i class="fa fa-pencil"></i>
                                            </a>
                                            <a class="btn btn-default btn-xs" data-toggle="tooltip" title=""
                                               onclick="return confirm('Apakah Anda ingin menghapus data ini?');"
                                               href="<?= base_url(); ?>master/Gizi/deleteMakanan/<?=$d->id?>"
                                               data-original-title="Hapus">
                                                <i class="fa fa-trash"></i>
                                            </a>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                                </tbody>
                            </table><!-- /.table -->
                        </div><!-- /.mail-box-messages -->
                    </div><!-- /.box-body -->
                </div>
            </div><!--/.col (left) -->
        </div>
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->
<!-- new END -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-sm400" role="document">
        <div class="modal-content modal-media-content">
            <div class="modal-header modal-media-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="box-title"> Tambah Makanan</h4>
            </div>
            <div class="modal-body pt0 pb0">
                <form id="add_makanan" action="Gizi/addMakanan" method="post" accept-charset="utf-8" enctype="multipart/form-data">
                    <div class="row">
                        <div class="" id="edit_bedtypedata">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Tipe Makanan</label><span class="req"> *</span>
                                    <select name="tipe_makanan_id" class="form-control">
                                        <?php foreach ($tipe as $value) { ?>
                                            <option value="<?php echo $value->id; ?>"><?php echo $value->nama; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Nama</label>
                                    <span class="req"> *</span>
                                    <input name="nama" placeholder="" type="text" class="form-control"/>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Harga</label>
                                    <span class="req"> *</span>
                                    <input name="harga" placeholder="" type="text" class="form-control"/>
                                </div>
                            </div>

                            <div class="box-footer clear">
                                <div class="pull-right">
                                    <button type="submit" id="add_makananbtn"
                                            data-loading-text="<?php echo $this->lang->line('processing') ?>"
                                            class="btn btn-info pull-right"><?php echo $this->lang->line('save'); ?></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="myModalEdit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-sm400" role="document">
        <div class="modal-content modal-media-content">
            <div class="modal-header modal-media-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="box-title">Edit Makanan</h4>
            </div>
            <form id="edit_makanan" action="Gizi/editMakanan" method="post" accept-charset="utf-8" enctype="multipart/form-data">
                <div class="modal-body pt0 pb0">
                    <div class="ptt10 row" id="edit_bedtypedata">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Tipe Makanan</label><span class="req"> *</span>
                                <select name="tipe_makanan_id" id="tipe_makanan" class="form-control">
                                    <?php foreach ($tipe as $value) { ?>
                                        <option value="<?php echo $value->id; ?>"><?php echo $value->nama; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Nama</label>
                                <span class="req"> *</span>
                                <input id="nama_makanan" name="nama" placeholder="" type="text" class="form-control"/>
                                <input id="id_makanan" name="id" placeholder="" type="hidden" class="form-control"/>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Harga</label>
                                <span class="req"> *</span>
                                <input id="harga_makanan" name="harga" placeholder="" type="text" class="form-control"/>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="box-footer clear">
                    <div class="pull-right">
                        <button type="submit" data-loading-text="<?php echo $this->lang->line('processing') ?>"
                                id="edit_makanan_btn"
                                class="btn btn-info pull-right"><?php echo $this->lang->line('save'); ?></button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        $('.detail_popover').popover({
            placement: 'right',
            trigger: 'hover',
            container: 'body',
            html: true,
            content: function () {
                return $(this).closest('td').find('.fee_detail_popover').html();
            }
        });
    });

    function getbedcat(cate) {

        $.ajax({
            url: '<?php echo base_url() ?>master/bed/getbed_categore_type/' + cate,
            success: function (data) {
                //alert(data);
                $('#bed_categore_type').html(data);

            }
        });
    }

    function getRecord(id) {
        $('#myModalEdit').modal('show');
        $.ajax({
            url: '<?php echo base_url(); ?>master/Gizi/getMakanan/' + id,
            type: "POST",
            dataType: "json",
            success: function (data) {
                const {makanan} = data;

                $("#id_makanan").val(makanan.id);
                $("#nama_makanan").val(makanan.nama);
                $("#tipe_makanan").val(makanan.tipe_makanan_id);
                $("#harga_makanan").val(makanan.harga);
            },
            error: function (e) {
                console.log(e);
                alert("Fail")
            }
        });
    }

    $(".add_makanan").click(function () {
        $('#add_makanan').trigger("reset");
    });
</script>
