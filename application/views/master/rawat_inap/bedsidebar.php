<style type="text/css">
    .tablists{margin: 0; padding: 0; list-style: none;}
    .tablists li{display: block; border-bottom: 1px solid #ddd;}
    .tablists li a{color: #444; padding: 10px;display: block;}
    .tablists li a.active{color: #0084B4; text-decoration: none; font-weight: bold;}
    .tablists li a:hover {color: #0084B4; text-decoration: none;}
</style>
<div class="box box-primary border0">
    <ul class="tablists">
        <li><a href="<?php echo site_url('master/Bed/status') ?>" class="<?=$this->uri->segment(2) == 'Bed' && $this->uri->segment(3) == 'status' ? 'active' : '' ?>"><?php echo $this->lang->line('bed') . " " . $this->lang->line('status'); ?></a></li>
        <li><a href="<?php echo site_url('master/Bed') ?>" class="<?=$this->uri->segment(2) == 'Bed' && $this->uri->segment(3) == '' ? 'active' : ''?>"><?php echo $this->lang->line('bed'); ?></a></li>
        <li><a href="<?php echo site_url('master/bedtype') ?>" class="<?=$this->uri->segment(2) == 'bedtype' ? 'active' : ''?>"><?php echo $this->lang->line('bed') . " " . $this->lang->line('type'); ?></a></li>
        <li><a href="<?php echo site_url('master/bedgroup') ?>" class="<?=$this->uri->segment(2) == 'bedgroup' ? 'active' : ''?>"><?php echo $this->lang->line('bed') . " " . $this->lang->line('group'); ?></a></li>
        <li><a href="<?php echo site_url('master/floor') ?>" class="<?=$this->uri->segment(2) == 'floor' ? 'active' : ''?>"><?php echo $this->lang->line('floor'); ?></a></li>
    </ul>
</div>
