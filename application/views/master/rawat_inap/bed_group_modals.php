<div class="modal fade" id="modal-bridging" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-sm400" role="document">
        <div class="modal-content modal-media-content">
            <div class="modal-header modal-media-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="box-title"> Bridging</h4>
            </div>
            <div class="modal-body pt0 pb0">
                <form method="post" action="<?=base_url()?>/master/bedgroup/bpjs_update_tempat_tidur" id="form-bpjs">
                    <input type="hidden" name="id" id="id-bed">
                    <input type="hidden" name="is_bridging" id="is-bridging">
                    <div class="row">
                        <div class="col-sm-12" id="warning" style="display: none">
                            <small><i>* Pastikan <b>Kode Kelas</b> dan <b>Kode Ruang</b> diisi dengan benar, karena setelah data dibridging, <b>Kode Kelas</b> dan <b>Kode Ruang</b> tidak bisa diedit lagi.</i></small>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Kode Kelas</label>
                                <select class="form-control" name="kodekelas" id="kodekelas">
                                    <?php foreach ($app as $v) : ?>
                                        <option value="<?=$v['kodekelas']?>"><?=$v['namakelas']?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Kode Ruang <small><i>(Contoh: RG01)</i></small></label>
                                <input name="koderuang" id="koderuang" placeholder="" type="text" class="form-control" required>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Nama Ruang</label>
                                <input name="namaruang" id="namaruang" placeholder="" type="text" class="form-control" required>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Kapasitas</label>
                                <input name="kapasitas" id="kapasitas" placeholder="" type="number" class="form-control" required>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Tersedia</label>
                                <input name="tersedia" id="tersedia" placeholder="" type="number" class="form-control" required>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <small><i>* Jika Rumah Sakit ingin mencantumkan informasi ketersediaan tempat tidur untuk pasien laki – laki, perempuan, laki – laki atau perempuan, isi kolom di bawah ini. Jika tidak, biarkan terisi 0 saja.</i></small>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Tersedia Pria</label>
                                <input name="tersediapria" id="tersediapria" placeholder="" type="number" class="form-control" value="0">
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Tersedia Wanita</label>
                                <input name="tersediawanita" id="tersediawanita" placeholder="" type="number" class="form-control" value="0">
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Tersedia Pria Wanita</label>
                                <input name="tersediapriawanita" id="tersediapriawanita" placeholder="" type="number" class="form-control" value="0">
                            </div>
                        </div>
                    </div><!-- /.box-body -->
                    <div class="box-footer">
                        <div class="pull-right">
                            <button type="submit" data-loading-text="<?php echo $this->lang->line('processing') ?>" id="addwardbtn" class="btn btn-info pull-right"><?php echo $this->lang->line('save'); ?></button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-sm400" role="document">
        <div class="modal-content modal-media-content">
            <div class="modal-header modal-media-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="box-title"> Tambah Bed Group</h4>
            </div>
            <div class="modal-body pt0 pb0">
                <div class="">
                    <div class=""   id="edit_expensedata">
                        <form id="addward"  class="ptt10" method="post"  accept-charset="utf-8" enctype="multipart/form-data" action="<?php echo base_url(); ?>master/bedgroup/add_bed_group">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Nama</label>
                                        <span class="req"> *</span>
                                        <input  name="name" placeholder="" type="text" class="form-control"  value="<?php echo set_value('invoice_no'); ?>" />
                                        <span class="text-danger name"></span>

                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Lantai</label>
                                        <span class="req"> *</span>
                                        <select name="floor" class="form-control">
                                            <option value="">--Pilih--</option>
                                            <?php foreach ($floor as $key => $floorvalue) {
                                                ?>
                                                <option value="<?php echo $floorvalue["id"] ?>"><?php echo $floorvalue["name"] ?></option>
                                            <?php } ?>
                                        </select>
                                        <span class="text-danger floor"></span>

                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Deskripsi</label>
                                        <textarea class="form-control"  name="description" placeholder="" rows="2" placeholder="Enter ..."><?php echo set_value('description'); ?><?php echo set_value('description') ?></textarea>
                                        <span class="text-danger description"></span>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Harga</label>
                                        <input name="harga" placeholder="" type="text" class="form-control"  value="<?php echo set_value('price'); ?>" />
                                        <span class="text-danger description"></span>
                                    </div>
                                </div>
                            </div><!-- /.box-body -->
                            <div class="box-footer">
                                <div class="pull-right">
                                    <button type="submit" data-loading-text="<?php echo $this->lang->line('processing') ?>" id="addwardbtn" class="btn btn-info pull-right"><?php echo $this->lang->line('save'); ?></button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

<div class="modal fade" id="myeditModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-sm400" role="document">
        <div class="modal-content modal-media-content">
            <div class="modal-header modal-media-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="box-title"> <?php echo $this->lang->line('edit') . " " . $this->lang->line('bed') . " " . $this->lang->line('group'); ?></h4>
            </div>

            <div class="modal-body pt0 pb0">
                <div class="row">
                    <form id="editbedgroup"  class="ptt10" method="post"  accept-charset="utf-8" enctype="multipart/form-data">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Nama</label>
                                <span class="req"> *</span>
                                <input id="name" name="name" placeholder="" type="text" class="form-control"  value="<?php echo set_value('name'); ?>" />
                                <input type="hidden" id="id" name="id">
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Lantai</label>
                                <span class="req"> *</span>
                                <select name="floor" id="floor" class="form-control">
                                    <option value="">--Pilih--</option>
                                    <?php foreach ($floor as $key => $floorvalue) {
                                        ?>
                                        <option value="<?php echo $floorvalue["id"] ?>"><?php echo $floorvalue["name"] ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label for="exampleInputEmail1"><?php echo $this->lang->line('description'); ?></label>
                                <textarea class="form-control" id="description" name="description" placeholder="" rows="2" placeholder="Enter ..."><?php echo set_value('description'); ?><?php echo set_value('description') ?></textarea>

                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Harga</label>
                                <input id="harga" name="harga" placeholder="" type="text" class="form-control"  value="<?php echo set_value('price'); ?>" />
                                <span class="text-danger description"></span>
                            </div>
                        </div>
                        <div class="box-footer">
                            <div class="pull-right">
                                <button type="submit" id="editbedgroupbtn" data-loading-text="<?php echo $this->lang->line('processing') ?>" class="btn btn-info pull-right"><?php echo $this->lang->line('save'); ?></button>
                            </div>
                        </div>
                    </form>
                </div><!-- /.box-body -->
            </div>

        </div>
    </div>
</div>