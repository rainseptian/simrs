
<div class="content-wrapper" style="min-height: 348px;">

    <section class="content">
        <div class="row">
            <div class="col-md-2">
                <?php $this->load->view('master/rawat_inap/bedsidebar'); ?>
            </div>
            <div class="col-md-10">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title titlefix"><?= $this->lang->line('bed') . " " . $this->lang->line('group') . " " . $this->lang->line('list'); ?></h3>
                        <div class="box-tools pull-right">
                            <button type="button" id="b-add" class="btn btn-primary btn-sm bedgroup"><i class="fa fa-plus"></i> Tambah</button>
                        </div><!-- /.box-tools -->
                    </div>

                    <div class="box-body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover example">
                                <thead>
                                    <tr>
                                        <th>Nama</th>
                                        <th>Lantai</th>
                                        <th>Deskripsi</th>
                                        <th>Harga</th>
                                        <th>Bridging</th>
                                        <th class="text-right">Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    foreach ($bedgroup_list as $key => $value) {
                                        ?>
                                        <tr>
                                            <td>
                                                <?= $value['name'] ?><br>
                                                <?php if ($value['is_bridging']) : ?>
                                                    <span class="label label-success">Sudah Bridging</span>
                                                <?php else : ?>
                                                    <span class="label label-danger">Belum Bridging</span>
                                                <?php endif; ?>
                                            </td>
                                            <td><?= $value["floor_name"] ?></td>
                                            <td>
                                                <?= $value["description"] ?>
                                                <?php if ($value['is_bridging']) : ?>
                                                    <?php $req = json_decode($value['request']) ?>
                                                    <br>
                                                    <b>
                                                        Kapasitas: <?=$req->kapasitas?><br>
                                                        Tersedia: <?=$req->tersedia?>
                                                    </b>
                                                <?php endif; ?>
                                            </td>
                                            <td>Rp <?= number_format($value["price"], 0, ',', '.') ?></td>
                                            <td>
                                                <a data-target="#myeditModal"
                                                   onclick="bridging(<?= $value['id']; ?>)"
                                                   class="btn <?=$value['is_bridging'] ? 'btn-success' : 'btn-danger'?> btn-sm"
                                                   data-toggle="tooltip" title="" data-original-title="Bridging">
                                                    <?php if ($value['is_bridging']) : ?>
                                                        <i class="fa fa-pencil"></i>
                                                    <?php else : ?>
                                                        <i class="fa fa-plus"></i>
                                                    <?php endif; ?>
                                                </a>
                                            </td>
                                            <td class="text-right">
                                                <a data-target="#myeditModal" onclick="edit(<?= $value['id']; ?>)"  class="btn btn-warning btn-sm" data-toggle="tooltip" title="" data-original-title="<?= $this->lang->line('edit'); ?>">
                                                    <i class="fa fa-pencil"></i>
                                                </a>
                                                <a class="btn btn-danger btn-sm"
                                                   href="<?=base_url()?>master/bedgroup/delete_bedgroup/<?= $value['id']; ?>"
                                                    <?php if ($value['is_bridging']) : ?>
                                                        onclick="return confirm('Menghapus data ini akan melakukan bridging ke BPJS untuk menghapus data ini. Lanjut hapus data ini?')"
                                                    <?php else : ?>
                                                        onclick="return confirm('Yakin hapus data ini?')"
                                                    <?php endif; ?>
                                                   data-toggle="tooltip"
                                                   title=""
                                                   data-original-title="<?= $this->lang->line('delete') ?>">
                                                    <i class="fa fa-trash"></i>
                                                </a>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                    ?>
                                </tbody>
                            </table><!-- /.table -->
                        </div><!-- /.mail-box-messages -->
                    </div><!-- /.box-body -->
                </div>
            </div><!--/.col (left) -->
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title titlefix">Ketersediaan Kamar (Bridging BPJS)</h3>
                    </div>
                    <div class="box-body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover example">
                                <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Nama Ruang</th>
                                    <th>Kode Ruang</th>
                                    <th>Kelas</th>
                                    <th>Kapasitas</th>
                                    <th>Tersedia</th>
                                    <th>Tersedia Pria</th>
                                    <th>Tersedia Wanita</th>
                                    <th>Tersedia Pria Wanita</th>
                                    <th>Last Update</th>
                                    <th>Aksi</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($ketersediaan as $v) : ?>
                                    <tr>
                                        <td><?=$v->rownumber?></td>
                                        <td><?=$v->namaruang?></td>
                                        <td><?=$v->koderuang?></td>
                                        <td><?=$v->kodekelas?> - <?=$v->namakelas?></td>
                                        <td><?=$v->kapasitas?></td>
                                        <td><?=$v->tersedia?></td>
                                        <td><?=$v->tersediapria?></td>
                                        <td><?=$v->tersediawanita?></td>
                                        <td><?=$v->tersediapriawanita?></td>
                                        <td><?=$v->lastupdate?></td>
                                        <td>
                                            <a class="btn btn-danger btn-sm"
                                               href="<?=base_url()?>master/bedgroup/delete_bridging/<?= urlencode($v->kodekelas) ?>/<?= urlencode($v->koderuang) ?>"
                                               onclick="return confirm('Yakin hapus data bridging ini?')"
                                               data-toggle="tooltip"
                                               title=""
                                               data-original-title="<?= $this->lang->line('delete') ?>">
                                                <i class="fa fa-trash"></i>
                                            </a>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                                </tbody>
                            </table><!-- /.table -->
                        </div><!-- /.mail-box-messages -->
                    </div><!-- /.box-body -->
                </div>
            </div>
        </div>
    </section>
</div><!-- /.content-wrapper -->

<?php $this->load->view('master/rawat_inap/bed_group_modals'); ?>

<script>
    $(document).ready(function (e) {
        $('#b-add').click(function () {
            $('#myModal').modal('show')
        })
    });

    function edit(id) {
        $('#myeditModal').modal('show');
        $.ajax({
            url: '<?= base_url(); ?>master/bedgroup/getbedgroupdata/' + id,
            dataType: 'json',
            success: function (data) {
                $('#id').val(data.id);
                $('#name').val(data.name);
                $('#floor').val(data.floor);
                $('#description').val(data.description);
                $('#harga').val(data.price);
            }
        });
    }

    $(document).ready(function (e) {
        $("#editbedgroup").on('submit', (function (e) {
            $("#editbedgroupbtn").button('loading');
            e.preventDefault();
            $.ajax({
                url: '<?= base_url(); ?>master/bedgroup/update_bedgroup',
                type: "POST",
                data: new FormData(this),
                dataType: 'json',
                contentType: false,
                cache: false,
                processData: false,
                success: function (data) {
                    //alert(data);
                    if (data.status == "fail") {
                        var message = "";
                        $.each(data.error, function (index, value) {
                            message += value;
                        });
                        errorMsg(message);
                    } else {
                        successMsg(data.message);
                        window.location.reload(true);
                    }
                    $("#editbedgroupbtn").button('reset');
                },
                error: function () {
                    alert("Fail")
                }
            });
        }));
    });

    $(".bedgroup").click(function(){
        $('#addward').trigger("reset");
    });
</script>

<script>
    $(function () {
        $('#form-bpjs').submit(function () {
            $('#kodekelas').prop('disabled', false)
        })
    })

    function bridging(id) {
        $.ajax({
            url: '<?= base_url(); ?>master/bedgroup/getbedgroupdata/' + id,
            dataType: 'json',
            success: function (data) {
                $('#id-bed').val(data.id);
                $('#is-bridging').val(data.is_bridging);
                $('#namaruang').val(data.name);
                $('#kapasitas').val(data.jumlah_bed);
                $('#tersedia').val(data.jumlah_bed_active);
                $('#kodekelas').prop('disabled', false)
                $('#warning').show()

                if (data.request) {
                    const req = JSON.parse(data.request)
                    $('#warning').hide()
                    $('#kodekelas').val(req.kodekelas).change().prop('disabled', 'disabled')
                    $('#koderuang').val(req.koderuang).prop('readonly', true)
                    $('#namaruang').val(req.namaruang);
                    $('#kapasitas').val(req.kapasitas)
                    $('#tersedia').val(req.tersedia)
                    $('#tersediapria').val(req.tersediapria)
                    $('#tersediawanita').val(req.tersediawanita)
                    $('#tersediapriawanita').val(req.tersediapriawanita)
                    //{"kodekelas":"VVP","koderuang":"UTAMA","namaruang":"Tes Bed Group 2","kapasitas":"0","tersedia":"0","tersediapria":"0","tersediawanita":"0","tersediapriawanita":"0"}
                }
            }
        });

        $('#modal-bridging').modal('show')
    }
</script>

<script>
    $(function () {
        <?php $warning = $this->session->flashdata('warning');
        if (!empty($warning)) { ?>
        Swal.fire({
            icon: 'error',
            title: '<?=$warning?>',
        })
        <?php } ?>
        <?php $success = $this->session->flashdata('success');
        if (!empty($success)) { ?>
        Swal.fire({
            icon: 'success',
            title: '<?=$success?>',
        })
        <?php } ?>
    })
</script>
