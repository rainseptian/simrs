<link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/iCheck/all.css">
<link rel="stylesheet" href="<?php echo base_url() ?>assets/bower_components/select2/dist/css/select2.min.css">
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Data Tarif Ambulance
            <small>Preview</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url() ?>Dashbord"><i class="fa fa-dashboard"></i> Dashbord</a></li>
            <li class="active"><?=isset($tarif) ? 'Edit' : 'Tambah'?> Tarif Ambulance</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-6">
                <div class="box box-danger">
                    <div class="box-header">
                        <h3 class="box-title"><?=isset($tarif) ? 'Edit' : 'Tambah'?> Tarif Ambulance</h3>
                    </div>
                    <?php $warning = $this->session->flashdata('warning');
                    if (!empty($warning)){ ?>
                        <div class="alert alert-warning alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <h4><i class="icon fa fa-warning"></i> Alert!</h4>
                            <?php echo $warning ?>
                        </div>
                    <?php } ?>
                    <?php $success = $this->session->flashdata('success');
                    if (!empty($success)){ ?>
                        <div class="alert alert-success alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <h4><i class="icon fa fa-check"></i> Success!</h4>
                            <?php echo $success ?>
                        </div>
                    <?php } ?>
                    <div class="box-body">
                        <form class="form-horizontal" method="post" action="<?php echo base_url()?>TarifAmbulance/<?=isset($tarif) ? 'update' : 'insert'?>">
                            <div class="box-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <input type="hidden" name="id" value="<?=isset($tarif) ? $tarif->id : ''?>">
                                        <div class="form-group">
                                            <label for="tujuan" class="col-sm-4 control-label" >Tujuan</label>
                                            <div class="col-sm-8">
                                                <input type="text" class="form-control" id="tujuan" name="tujuan" value="<?=isset($tarif) ? $tarif->tujuan : ''?>" placeholder="Masukkan tujuan" required>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="tarif" class="col-sm-4 control-label" >Tarif UMUM</label>
                                            <div class="col-sm-8">
                                                <input type="text" class="form-control" id="tarif" name="tarif" value="<?=isset($tarif) ? $tarif->tarif : '0'?>" placeholder="Masukkan tarif" required>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="tarif" class="col-sm-4 control-label" >Tarif BPJS</label>
                                            <div class="col-sm-8">
                                                <input type="text" class="form-control" id="tarif_bpjs" name="tarif_bpjs" value="<?=isset($tarif) ? $tarif->tarif_bpjs : '0'?>" placeholder="Masukkan tarif bpjs" required>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="box-footer">
                                    <button type="submit" name="submit" value="1" class="btn btn-primary btn-lg btn-flat pull-right">Simpan</button>
                                    <a href="<?php echo base_url() ?>TarifAmbulance"  class="btn btn-default btn-lg btn-flat pull-right">Batal</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
