<link rel="stylesheet"
      href="<?php echo base_url() ?>assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Laporan Harian Shift
            <small></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a href="#">Laporan Harian Shift</a></li>
            <li class="active">Data Laporan Harian Shift</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <form class="form-horizontal" method="get" action="<?php echo base_url() ?>LaporanHarianShift/laporan_harian">
                            <div class='row'>
                                <div class="col-sm-5">
                                    <div class="form-horizontal">
                                        <div class="form-group">
                                            <label class="col-sm-5 control-label"></label>
                                            <label class="col-sm-5 control-label">Tanggal Laporan</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-5">
                                    <div class="form-horizontal">
                                        <div class="form-group">
                                            <label class="col-sm-5 control-label">Dari Tanggal</label>
                                            <div class="col-sm-7">
                                                <input type='date' name='from' class='form-control' id='tanggal_dari'
                                                       value="<?php echo ($this->input->get('from')) ? $this->input->get('from') : date('Y-m-01') ?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-5">
                                    <div class="form-horizontal">
                                        <div class="form-group">
                                            <label class="col-sm-5 control-label">Sampai Tanggal</label>
                                            <div class="col-sm-7">
                                                <input type='date' name='to' class='form-control' id='tanggal_sampai'
                                                       value="<?php echo ($this->input->get('to')) ? $this->input->get('to') : date('Y-m-d') ?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class='row'>
                                <div class="col-sm-5">
                                    <div class="form-horizontal">
                                        <div class="form-group">
                                            <div class="col-sm-5"></div>
                                            <div class="col-sm-7">
                                                <button type="submit" class="btn btn-primary" style='margin-left: 0px;'>
                                                    Tampilkan
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <div class="col-sm-8">
                            <h3 class="box-title">Data Laporan Harian Shift</h3>&nbsp;&nbsp;
                            <a href="<?php echo base_url(); ?>LaporanHarianShift/input_laporan_harian">
                                <button type="button" class="btn btn-primary"><span
                                            class="glyphicon glyphicon-plus"></span></button>
                            </a>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <?php $warning = $this->session->flashdata('warning');
                    if (!empty($warning))
                    { ?>
                        <div class="alert alert-warning alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">
                                &times;
                            </button>
                            <h4><i class="icon fa fa-warning"></i> Alert!</h4>
                            <?php echo $warning ?>
                        </div>
                    <?php } ?>
                    <?php $success = $this->session->flashdata('success');
                    if (!empty($success))
                    { ?>
                        <div class="alert alert-success alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">
                                &times;
                            </button>
                            <h4><i class="icon fa fa-check"></i> Success!</h4>
                            <?php echo $success ?>
                        </div>
                    <?php } ?>
                    <div class="box-body">
                        <table class="table table-bordered table-hover example2">
                            <thead>
                            <tr>
                                <th>No</th>
                                <th>Tanggal Laporan</th>
                                <th>Shift</th>
                                <th>Koordinator</th>
                                <th>Anggota</th>
                                <th>Magang</th>
                                <th>Dokter Jaga</th>
                                <th>Aksi</th>
                            </tr>
                            </thead>

                            <tbody>
                            <?php $no = 1;

                            foreach ($list as $row) { ?>

                                <tr>
                                    <td><?php echo $no; ?></td>
                                    <td><?php echo date('d-F-Y H:i', strtotime($row->tanggal_laporan)); ?></td>
                                    <td><?php echo $row->shift; ?></td>
                                    <td class="nama"> <?php echo ucwords($row->nama_koordinator); ?></td>
                                    <td><?php echo implode("<br>", $row->nama_anggotas); ?></td>
                                    <td><?php echo $row->magang; ?></td>
                                    <td class="nama"> <?php echo ucwords($row->nama_dokter_jaga); ?></td>
                                    <td>
                                        <a href="<?php echo base_url(); ?>LaporanHarianShift/edit_laporan_harian/<?php echo $row->id; ?>" style="margin-bottom: 5px;" class="btn-block">
                                            <button type="button" class="btn btn-sm btn-block btn-primary"><i class="fa fa-pencil"></i> Edit</button>
                                        </a>
                                        <a href="<?php echo base_url(); ?>LaporanHarianShift/detail/<?php echo $row->id; ?>" style="margin-bottom: 5px;" class="btn-block">
                                            <button type="button" class="btn btn-sm btn-block btn-warning"><i class="fa fa-arrows"></i> Detail</button>
                                        </a>
                                        <a href="<?php echo base_url(); ?>LaporanHarianShift/download/<?php echo $row->id; ?>" style="margin-bottom: 5px;" class="btn-block">
                                            <button type="button" class="btn btn-sm btn-block btn-success"><i class="fa fa-download"></i> Unduh</button>
                                        </a>
                                        <a href="<?php echo base_url(); ?>LaporanHarianShift/cetak/<?php echo $row->id; ?>" target="_blank" rel="noopener noreferrer">
                                            <button type="button" class="btn btn-sm btn-block btn-danger"><i class="fa fa-print"></i> Cetak</button>
                                        </a>
                                    </td>
                                </tr>
                                <?php $no++;
                            } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-xs-12" id="tess"></div>
        </div>
    </section>
    <!-- /.content -->

</div>

<script>
    $(function () {
        $('#example1').DataTable()
        $('.example2').DataTable({
            rowReorder: {
                selector: 'td:nth-child(2)'
            },
            'paging': true,
            'lengthChange': true,
            'searching': true,
            'ordering': true,
            'info': true,
            'autoWidth': true
        })
    });

</script>
