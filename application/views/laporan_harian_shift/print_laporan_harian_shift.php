<link rel="stylesheet"
      href="<?php echo base_url() ?>assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">

<div class="content-wrapper">

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <div class="col-sm-8">
                            <h3 class="box-title">Detail Laporan Harian Shift</h3>&nbsp;&nbsp;
                        </div>
                    </div>
                    <!-- /.box-header -->

                    <div class="box-body">
                        <table class="table table-bordered">
                            <tbody>
                            <tr>
                                <td>Tanggal Laporan</td>
                                <td><?= $detail->tanggal_laporan ?></td>
                            </tr>
                            <tr>
                                <td>Shift</td>
                                <td><?= $detail->shift ?></td>
                            </tr>
                            <tr>
                                <td>Koordinator</td>
                                <td><?= $detail->nama_koordinator ?></td>
                            </tr>
                            <tr>
                                <td>Anggota</td>
                                <td><?= implode('<br>', $detail->nama_anggotas) ?></td>
                            </tr>
                            <tr>
                                <td>Magang</td>
                                <td><?= $detail->magang ?></td>
                            </tr>
                            <tr>
                                <td>Dokter Jaga</td>
                                <td><?= $detail->nama_dokter_jaga ?></td>
                            </tr>
                            <tr><td colspan="2" style="background: #eeeeee"></td></tr>
                            <tr>
                                <td colspan="2"><strong>Rincian Pasien</strong></td>
                            </tr>
                            <tr>
                                <td>Pasien Ranap</td>
                                <td><?= $detail->pasien_ranap ?></td>
                            </tr>
                            <tr>
                                <td>Pasien Poli Umum</td>
                                <td><?= $detail->pasien_poli_umum ?></td>
                            </tr>
                            <tr>
                                <td>Pasien Poli IGD</td>
                                <td><?= $detail->pasien_poli_igd ?></td>
                            </tr>
                            <tr>
                                <td>Pasien Poli Obgyn</td>
                                <td><?= $detail->pasien_poli_obgyn ?></td>
                            </tr>
                            <tr>
                                <td>Pasien Poli Bedah</td>
                                <td><?= $detail->pasien_poli_bedah ?></td>
                            </tr>
                            <tr>
                                <td>Pasien Poli Anak</td>
                                <td><?= $detail->pasien_poli_anak ?></td>
                            </tr>
                            <tr>
                                <td>Pasien Poli Penyakit Dalam</td>
                                <td><?= $detail->pasien_poli_penyakit_dalam ?></td>
                            </tr>
                            <tr>
                                <td>Pasien Poli Bersalin</td>
                                <td><?= $detail->pasien_poli_bersalin ?></td>
                            </tr>
                            <tr>
                                <td>Pasien Poli Operasi</td>
                                <td><?= $detail->pasien_poli_operasi ?></td>
                            </tr>
                            <tr>
                                <td>Pasien Poli HCU</td>
                                <td><?= $detail->pasien_poli_hcu ?></td>
                            </tr>
                            <tr>
                                <td>Pasien Poli Perinatologi</td>
                                <td><?= $detail->pasien_poli_perinatologi ?></td>
                            </tr>
                            <tr>
                                <td>Pasien Poli Radiologi</td>
                                <td><?= $detail->pasien_poli_radiologi ?></td>
                            </tr>

                            <tr><td colspan="2" style="background: #eeeeee"></td></tr>
                            <tr>
                                <td colspan="2"><strong>Tindakan</strong></td>
                            </tr>
                            <tr>
                                <td>APS Antibody</td>
                                <td><?= $detail->aps_antibody ?></td>
                            </tr>
                            <tr>
                                <td>APS Antigen</td>
                                <td><?= $detail->aps_antigen ?></td>
                            </tr>
                            <tr>
                                <td>MRS Antibody</td>
                                <td><?= $detail->mrs_antibody ?></td>
                            </tr>
                            <tr>
                                <td>MRS Antigen</td>
                                <td><?= $detail->mrs_antigen ?></td>
                            </tr>

                            <tr><td colspan="2" style="background: #eeeeee"></td></tr>
                            <tr>
                                <td>KRS</td>
                                <td><?= $detail->krs ?></td>
                            </tr>
                            <tr>
                                <td>Pasien Rujuk</td>
                                <td><?= $detail->pasien_rujuk ?></td>
                            </tr>
                            <tr>
                                <td>Ambulance</td>
                                <td><?= $detail->ambulance ?></td>
                            </tr>
                            <tr>
                                <td>Obat Emergency</td>
                                <td><?= $detail->obat_emergency ?></td>
                            </tr>
                            <tr>
                                <td>RM Lengkap</td>
                                <td><?= $detail->rm_lengkap ?></td>
                            </tr>
                            <tr>
                                <td>Kejadian Penting</td>
                                <td><?= $detail->kejadian_penting ?></td>
                            </tr>

                            <tr><td colspan="2" style="background: #eeeeee"></td></tr>
                            <tr>
                                <td colspan="2"><strong>Stok</strong></td>
                            </tr>
                            <tr>
                                <td>Antigen</td>
                                <td><?= $detail->stok_antigen ?></td>
                            </tr>
                            <tr>
                                <td>Antibody</td>
                                <td><?= $detail->stok_antibody ?></td>
                            </tr>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->

</div>

<script>
    (function() {
        window.print();
    })();
</script>
