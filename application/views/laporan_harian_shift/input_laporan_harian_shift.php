<?php

class InputBuilder {

    var $name;
    var $label;
    var $value = '';
    var $sm = 9;
    var $placeholder = '';
    var $unit = '';
    var $options;

    var $optdisplay;
    var $optVal;
    var $onkeyup;
    var $selectedValues;

    var $united = false;
    var $textarea = false;
    var $select = false;
    var $selectMulti = false;
    var $required = false;
    var $readonly = false;
    var $hide = false;
    var $noMarginBottom = false;

    function __construct($name, $label, $sm) {
        $this->name = $name;
        $this->label = $label;
        $this->sm = $sm;
    }

    public function val($v) {
        $this->value = $v;
        return $this;
    }

    public function placeholder($v) {
        $this->placeholder = $v;
        return $this;
    }

    public function unit($v) {
        $this->unit = $v;
        return $this;
    }

    public function united() {
        $this->united = true;
        return $this;
    }

    public function textarea() {
        $this->textarea = true;
        return $this;
    }

    public function select() {
        $this->select = true;
        return $this;
    }

    public function selectMulti() {
        $this->selectMulti = true;
        return $this;
    }

    public function options($v) {
        $this->options = $v;
        return $this;
    }

    public function display($v) {
        $this->optdisplay = $v;
        return $this;
    }

    public function optVal($v) {
        $this->optVal = $v;
        return $this;
    }

    public function onkeyup($v) {
        $this->onkeyup = $v;
        return $this;
    }

    public function selectedValues($v) {
        $this->selectedValues = $v;
        return $this;
    }

    public function required($b = true) {
        $this->required = $b;
        return $this;
    }

    public function readonly() {
        $this->readonly = true;
        return $this;
    }

    public function hide() {
        $this->hide = true;
        return $this;
    }

    public function noMarginBottom() {
        $this->noMarginBottom = true;
        return $this;
    }

    public function build() {
        if ($this->select) {
            $this->buildSelect($this->hide, $this->selectedValues);
        } else if ($this->selectMulti) {
            $this->buildSelectMulti($this->hide, $this->selectedValues);
        } else if ($this->united) {
            $this->buildUnited($this->hide);
        } else {
            $this->buildTypeText($this->hide, $this->noMarginBottom);
        }
    }

    public function buildTypeText($hide, $noMarginBottom) { ?>
        <div class="form-group <?=$hide ? 'hidden' : ''?>" <?= $noMarginBottom ? 'style="margin-bottom: 0' : '' ?>">
            <label for="<?= $this->name ?>" class="col-sm-3 control-label"><?= $this->label ?></label>
            <div class="col-sm-<?= $this->sm ?>">
                <?php if ($this->textarea) : ?>
                    <textarea type="text" class="form-control" name="<?= $this->name ?>" id="<?= $this->name ?>"
                              value="<?= $this->value ?>" <?= $this->required ? 'required' : '' ?> <?= $this->readonly ? 'readonly' : '' ?>></textarea>
                <?php else : ?>
                    <input type="text" class="form-control" name="<?= $this->name ?>" id="<?= $this->name ?>"
                           value="<?= $this->value ?>" <?= $this->required ? 'required' : '' ?> <?= $this->readonly ? 'readonly' : '' ?>>
                <?php endif; ?>
            </div>
        </div>
    <?php }

    public function buildSelect($hide, $selectedValues) { ?>
        <div class="form-group <?=$hide ? 'hidden' : ''?>">
            <label for="<?= $this->name ?>" class="col-sm-3 control-label"><?= $this->label ?></label>
            <div class="col-sm-<?= $this->sm ?>">
                <select class="form-control select2" name="<?= $this->name ?>"
                        data-placeholder="<?= $this->placeholder ?>"
                        style="width: 100%;" <?= $this->required ? 'required' : '' ?>>

                    <?php foreach ($this->options as $key => $value) { ?>
                        <option <?= in_array(call_user_func($this->optVal, $value), $selectedValues) ? 'selected' : '' ?> value="<?= call_user_func($this->optVal, $value); ?>"><?= call_user_func($this->optdisplay, $value) ?></option>
                    <?php } ?>
                </select>
            </div>
        </div>
    <?php
    }

    public function buildSelectMulti($hide, $selectedValues) { ?>
        <div class="form-group <?=$hide ? 'hidden' : ''?>">
            <label for="<?= $this->name ?>" class="col-sm-3 control-label"><?= $this->label ?></label>
            <div class="col-sm-<?= $this->sm ?>">
                <select class="form-control select2" multiple="multiple" name="<?= $this->name ?>"
                        data-placeholder="<?= $this->placeholder ?>"
                        style="width: 100%;" <?= $this->required ? 'required' : '' ?>>

                    <?php foreach ($this->options as $key => $value) { ?>
                        <option <?= in_array(call_user_func($this->optVal, $value), $selectedValues) ? 'selected' : '' ?> value="<?= call_user_func($this->optVal, $value); ?>"><?= call_user_func($this->optdisplay, $value) ?></option>
                    <?php } ?>
                </select>
            </div>
        </div>
    <?php
    }

    public function buildUnited($hide) { ?>
        <div class="col-sm-6 col-md-6 col-lg-6 form-group <?=$hide ? 'hidden' : ''?>">
            <label for="<?= $this->name ?>" class="col-sm-3 control-label"><?= $this->label ?></label>
            <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">
                <input
                        type="text"
                        class="form-control"
                        id="<?= $this->name ?>"
                        name="<?= $this->name ?>"
                        value="<?= $this->value ?>"
                    <?= $this->required ? 'required' : '' ?>
                    <?= $this->readonly ? 'readonly' : '' ?>
                    <?= isset($this->onkeyup) && $this->onkeyup != '' ? 'onkeyup=' . $this->onkeyup : '' ?>
                >
                <span class="input-group-addon"><?= $this->unit ?></span>
            </div>
        </div>
    <?php }
}

function unit($name, $label) {
    $i = new InputBuilder($name, $label, 9);
    $i->united();
    return $i;
}

function sel($name, $label) {
    $i = new InputBuilder($name, $label, 9);
    $i->select();
    return $i;
}

function selMulti($name, $label) {
    $i = new InputBuilder($name, $label, 9);
    $i->selectMulti();
    return $i;
}

function sm4($name, $label) {
    return new InputBuilder($name, $label, 4);
}

function sm9($name, $label) {
    return new InputBuilder($name, $label, 9);
}

function br() {
    echo '<br>';
}

?>

<style media="screen">
    .select2-container {
        width: 100% !important;
    }
</style>
<link rel="stylesheet" href="<?= base_url() ?>assets/plugins/iCheck/all.css">
<form class="form-horizontal" method="post" action="<?= base_url() ?>LaporanHarianShift/<?= $edit ? "edit" : "tambah" ?>">
    <?php if ($edit) { ?>
        <input type="hidden" name="id" value="<?= $detail->id ?>">
    <?php } ?>

    <div class="content-wrapper">
        <section class="content-header">
            <h1>
                Laporan Harian Shift
            </h1>
            <ol class="breadcrumb">
                <li><a href="<?= base_url() ?>Dashbord"><i class="fa fa-dashboard"></i> Dashbord</a></li>
                <li class="active">Laporan Harian Shift</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-12">

                    <div class="box box-danger">
                        <div class="box-header">
                            <h3 class="box-title"> <?= $edit ? "Edit" : "Tambah" ?> Laporan Harian Shift</h3>
                        </div>
                        <div class="box-body">
                            <div class="box-body">

                                <?php

                                sel('shift', 'Shift')
                                    ->placeholder('Pilih Shift')
                                    ->options([
                                        "Shift I Jam 07.00 – 14.00",
                                        "Shift II Jam 14.00 – 21.00",
                                        "Shift III Jam 21.00 – 07.00",
                                    ])
                                    ->display(function ($value) {
                                        return $value;
                                    })
                                    ->optVal(function ($value) {
                                        return $value;
                                    })
                                    ->selectedValues([$detail->shift])
                                    ->required()
                                    ->build();

                                sel('koordinator_id', 'Koordinator')
                                    ->placeholder('Pilih Koordinator')
                                    ->options($perawats)
                                    ->display(function ($value) {
                                        return $value->nama;
                                    })
                                    ->optVal(function ($value) {
                                        return $value->id;
                                    })
                                    ->selectedValues([$detail->koordinator_id])
                                    ->required()
                                    ->build();

                                selMulti('anggota_ids[]', 'Anggota')
                                    ->placeholder('Pilih Anggota')
                                    ->options($perawats)
                                    ->display(function ($value) {
                                        return $value->nama;
                                    })
                                    ->optVal(function ($value) {
                                        return $value->id;
                                    })
                                    ->selectedValues($detail->anggota_ids)
                                    ->required()
                                    ->build();

                                sm9('magang', 'Magang')->val($detail->magang)->build();

                                sel('dokter_jaga_id', 'Dokter Jaga')
                                    ->placeholder('Pilih Dokter Jaga')
                                    ->options($dokters)
                                    ->display(function ($value) {
                                        return $value->nama;
                                    })
                                    ->optVal(function ($value) {
                                        return $value->id;
                                    })
                                    ->selectedValues([$detail->dokter_jaga_id])
                                    ->required()
                                    ->build();
                                ?>

                                <div class="row">
                                    <label class="col-sm-3 control-label" style="color: #1f6791; font-size: 1.7rem">Rincian Pasien</label>
                                    <div class="col-sm-12" style="background: #f8f8f8; border: 1px solid #dddddd; padding: 10px; margin-top: 10px">
                                        <?php

                                        sm9('pasien_ranap', 'Pasien Ranap')->val($detail->pasien_ranap)->required()->build();
                                        sm9('pasien_poli_umum', 'Pasien Poli Umum')->val($detail->pasien_poli_umum)->required()->build();
                                        sm9('pasien_poli_igd', "Pasien Poli IGD")->val($detail->pasien_poli_igd)->required()->build();
                                        sm9('pasien_poli_obgyn', "Pasien Poli Obgyn")->val($detail->pasien_poli_obgyn)->required()->build();
                                        sm9('pasien_poli_bedah', "Pasien Poli Bedah")->val($detail->pasien_poli_bedah)->required()->build();
                                        sm9('pasien_poli_anak', "Pasien Poli Anak")->val($detail->pasien_poli_anak)->required()->build();
                                        sm9('pasien_poli_penyakit_dalam', "Pasien Poli Penyakit Dalam")->val($detail->pasien_poli_penyakit_dalam)->required()->build();
                                        sm9('pasien_poli_bersalin', "Pasien Poli Bersalin")->val($detail->pasien_poli_bersalin)->required()->build();
                                        sm9('pasien_poli_operasi', "Pasien Poli Operasi")->val($detail->pasien_poli_operasi)->required()->build();
                                        sm9('pasien_poli_hcu', "Pasien Poli HCU")->val($detail->pasien_poli_hcu)->required()->build();
                                        sm9('pasien_poli_perinatologi', "Pasien Poli Perinatologi")->val($detail->pasien_poli_perinatologi)->required()->build();
                                        sm9('pasien_poli_radiologi', "Pasien Poli radiologi")->val($detail->pasien_poli_radiologi)->required()->noMarginBottom()->build();

                                        ?>
                                    </div>
                                </div>

                                <div class="row" style="margin-top: 10px; margin-bottom: 10px">
                                    <label class="col-sm-3 control-label" style="color: #1f6791; font-size: 1.7rem">Tindakan</label>
                                    <div class="col-sm-12" style="background: #f8f8f8; border: 1px solid #dddddd; padding: 10px; margin-top: 10px">
                                        <?php

                                        sm9('aps_antibody', 'APS Antibody')->val($detail->aps_antibody)->required()->build();
                                        sm9('aps_antigen', 'APS Antigen')->val($detail->aps_antigen)->required()->build();
                                        sm9('mrs_antibody', 'MRS Antibody')->val($detail->mrs_antibody)->required()->build();
                                        sm9('mrs_antigen', "MRS Antigen")->val($detail->mrs_antigen)->required()->noMarginBottom()->build();

                                        ?>
                                    </div>
                                </div>

                                <?php

                                sm9('krs', 'KRS')->val($detail->krs)->required()->build();
                                sm9('pasien_rujuk', 'Pasien Rujuk')->val($detail->pasien_rujuk)->required()->build();
                                sm9('ambulance', 'Ambulance')->val($detail->ambulance)->required()->build();
                                sm9('obat_emergency', 'Obat Emergency')->val($detail->obat_emergency)->required()->build();
                                sm9('rm_lengkap', 'RM Lengkap')->val($detail->rm_lengkap)->required()->build();
                                sm9('kejadian_penting', 'Kejadian Penting')->val($detail->kejadian_penting)->required()->build();

                                ?>

                                <div class="row" style="margin-top: 10px; margin-bottom: 10px">
                                    <label class="col-sm-3 control-label" style="color: #1f6791; font-size: 1.7rem">Stok</label>
                                    <div class="col-sm-12" style="background: #f8f8f8; border: 1px solid #dddddd; padding: 10px; margin-top: 10px">
                                        <?php

                                        sm9('stok_antigen', 'Antigen')->val($detail->stok_antigen)->required()->build();
                                        sm9('stok_antibody', "Antibody")->val($detail->stok_antibody)->required()->noMarginBottom()->build();

                                        ?>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="box-footer">
                            <button type="submit" name="submit"
                                    class="btn btn-primary btn-lg btn-flat pull-right">Simpan
                            </button>
                            <a href="<?= base_url() ?>LaporanHarianShift/laporan_harian"
                               class="btn btn-default btn-lg btn-flat pull-right">Batal</a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</form>

<div id='ResponseInput'></div>

<!-- Select2 -->
<script src="<?= base_url() ?>assets/bower_components/select2/dist/js/select2.full.min.js"></script>
<!-- InputMask -->

<script type="text/javascript">
    $('.select2').select2();
</script>