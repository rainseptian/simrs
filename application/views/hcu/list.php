<?php
$currency_symbol = 'Rp';
?>
<link rel="stylesheet"
      href="<?php echo base_url() ?>assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
<link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/autocomplete/style-gue.css">
<style type="text/css">
    @import('https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.0/css/bootstrap.min.css');
</style>

<style type="text/css">
    .table-responsive {overflow-x: inherit;}
</style>
<div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title titlefix"> Pasien HCU</h3>
                    </div><!-- /.box-header -->
                    <?php $warning = $this->session->flashdata('warning');
                    if (!empty($warning)){ ?>
                        <div class="alert alert-warning alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <h4><i class="icon fa fa-warning"></i> Alert!</h4>
                            <?php echo $warning ?>
                        </div>
                    <?php } ?>
                    <?php $success = $this->session->flashdata('success');
                    if (!empty($success)){ ?>
                        <div class="alert alert-success alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <h4><i class="icon fa fa-check"></i> Success!</h4>
                            <?php echo $success ?>
                        </div>
                    <?php } ?>
                    <div class="box-body">
                        <table class="table table-striped table-bordered table-hover data-table" cellspacing="0" width="100%">
                            <thead>
                            <tr>
                                <th>No</th>
                                <th>Tgl Daftar</th>
                                <th>No RM</th>
                                <th>Nama Pasien</th>
                                <th>Alamat</th>
                                <th>Nama Dokter</th>
                                <th>Dari</th>
                                <th>Tipe Pasien</th>
                                <th>Aksi</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $no = 1;
                            foreach ($list as $r) : ?>
                                <tr>
                                    <td><?=$no++?></td>
                                    <td><?=date('d-F-Y H:i', strtotime($r['transfered_at']))?></td>
                                    <td><?=$r['no_rm']?></td>
                                    <td><?=$r['nama_pasien']?></td>
                                    <td><?=$r['alamat']?></td>
                                    <td><?=$r['nama_dokter']?></td>
                                    <td><?=$r['dari']?></td>
                                    <td><?=$r['tipe_pasien']?></td>
                                    <td>
                                        <a href="<?php echo base_url(); ?>HCU/detail/<?php echo $r['id']; ?>/<?php echo $r['transfer_id']; ?>">
                                            <button type="button" class="btn btn-primary btn-block"><i class="fa fa-arrows"></i> Periksa</button>
                                        </a>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<script type="text/javascript">
    $(function () {
        $('.data-table').DataTable()
    })
</script>
