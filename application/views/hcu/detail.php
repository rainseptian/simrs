<?php
function harga($num)
{
    return number_format($num, 2, ',', '.');
}
?>
<style media="screen">
    .select2-container {
        width: 100% !important;
    }

    .impbtn{position: absolute;right: 14px;top: 13px;}
    .impbtnview{position: absolute;right: 10px;top: 13px;}
    .impbtnview20{position: absolute;right: 20px;top: 11px;}
    .impbtnview-tindakan{position: absolute;left: 110px;top: 6px;}
    .margin-bottom {margin-bottom:15px;}
</style>
<link rel="stylesheet"
      href="<?php echo base_url() ?>assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
<link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/autocomplete/style-gue.css">
<style type="text/css">
    @import('https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.0/css/bootstrap.min.css');
</style>

<?php $currency_symbol = 'Rp'; ?>
<div class="content-wrapper">
    <section class="content">
        <div class="box box-primary">
            <div class="row">
                <div class="box-body pb0">
                    <div class="col-lg-2 col-md-2 col-sm-3 text-center">
                        <img width="115" height="115" class="" src="<?php echo base_url() ?>assets/img/logo.png" alt="No Image">
                    </div>
                    <div class="col-md-10 col-lg-10 col-sm-9">
                        <div class="table-responsive">
                            <table class="table table-striped mb0 font13">
                                <tbody>
                                <tr>
                                    <th class="bozerotop">Nama</th>
                                    <td class="bozerotop"><?=$pasien->nama?></td>
                                    <th class="bozerotop">Penanggung Jawab</th>
                                    <td class="bozerotop"><?=$rawat_inap->penanggung_jawab?></td>
                                </tr>
                                <tr>
                                    <th class="bozerotop">Jenis Kelamin</th>
                                    <td class="bozerotop"><?=$pasien->jk == 'L' ? 'Laki-laki' : 'Perempuan'?></td>
                                    <th class="bozerotop">Usia</th>
                                    <td class="bozerotop"><?=$pasien->usia?></td>
                                </tr>
                                <tr>
                                    <th class="bozerotop">Alamat</th>
                                    <td class="bozerotop"><?=$pasien->alamat?></td>
                                    <th class="bozerotop">No Telp</th>
                                    <td class="bozerotop"><?=$pasien->telepon?></td>
                                </tr>
                                <tr>
                                    <th class="bozerotop">Tgl Daftar</th>
                                    <td class="bozerotop"><?=$rawat_inap->created_at?></td>
                                    <th class="bozerotop">Dari</th>
                                    <td class="bozerotop"><?=$dari?></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="col-md-12 col-lg-12 col-sm-12 pull-right">
                        <form id="form-selesai" method="post" action="<?=base_url('HCU/selesai/'.$rawat_inap->id.'/'.$transfer_id)?>" style="float: right">
                            <input type="hidden" name="total_biaya" id="total_biaya">
                            <input type="hidden" name="belum_ada_bangsal" value="<?=!$rawat_inap->via_bangsal?>">
                            <button type="submit" class="btn btn-sm btn-success"
                                    onclick="return checkIsian();">
                                <i class="fa fa-check"></i> Selesai
                            </button>
                        </form>
                    </div>
                </div>
            </div>
            <div>
                <div class="box border0">
                    <div style="background: #dadada; height: 1px; width: 100%; clear: both; margin-top:5px;"></div>
                    <div class="nav-tabs-custom border0" id="tabs">
                        <ul class="nav nav-tabs">
                            <li <?=$tab == 'pemeriksaan' ? 'class="active"' : ''?>>
                                <a href="#pemeriksaan" data-toggle="tab" aria-expanded="true"><i
                                            class="fas fa-file-prescription"></i> Pemeriksaan
                                </a>
                            </li>
                            <li <?=$tab == 'diagnosis' ? 'class="active"' : ''?>>
                                <a href="#diagnosis" data-toggle="tab" aria-expanded="true"><i
                                            class="fas fa-diagnoses"></i> Diagnosis
                                </a>
                            </li>
                            <li <?=$tab == 'resep' ? 'class="active"' : ''?>>
                                <a href="#resep" data-toggle="tab" aria-expanded="true"><i
                                            class="far fa-calendar-check"></i> Resep
                                </a>
                            </li>
                            <li <?=$tab == 'bahan_habis_pakai' ? 'class="active"' : ''?>>
                                <a href="#bahan_habis_pakai" data-toggle="tab" aria-expanded="true"><i
                                            class="far fa-calendar-check"></i> Bahan Habis Pakai
                                </a>
                            </li>
                            <li <?=$tab == 'biaya' ? 'class="active"' : ''?>>
                                <a href="#biaya" data-toggle="tab" aria-expanded="true"><i
                                            class="fas fa-donate"></i> Biaya
                                </a>
                            </li>
                            <li <?=$tab == 'radiologi' ? 'class="active"' : ''?>>
                                <a href="#radiologi" data-toggle="tab" aria-expanded="true"><i
                                            class="fa fas fa-x-ray"></i> Radiologi
                                </a>
                            </li>
                            <li <?=$tab == 'lab' ? 'class="active"' : ''?>>
                                <a href="#lab" data-toggle="tab" aria-expanded="true"><i
                                            class="fa fas fa-flask"></i> Laboratorium
                                </a>
                            </li>
                            <li <?=$tab == 'bill' ? 'class="active"' : ''?>>
                                <a href="#bill" class="bill" data-toggle="tab" aria-expanded="true"><i
                                            class="fas fa-file-invoice-dollar"></i> Bill
                                </a>
                            </li>
                        </ul>

                        <div class="tab-content">
                            <?php $this->load->view('rawat_inap/tab_contents/pemeriksaan', ['show_action' => true]); ?>
                            <?php $this->load->view('rawat_inap/tab_contents/diagnosis', ['show_action' => true]); ?>
                            <?php $this->load->view('rawat_inap/tab_contents/resep', ['show_action' => true]); ?>
                            <?php $this->load->view('rawat_inap/tab_contents/bahan_habis_pakai', ['show_action' => true]); ?>
                            <?php $this->load->view('rawat_inap/tab_contents/biaya', ['show_action' => true]); ?>
                            <?php $this->load->view('rawat_inap/tab_contents/radiologi', ['show_action' => true]); ?>
                            <?php $this->load->view('rawat_inap/tab_contents/lab', ['show_action' => true]); ?>
                            <?php $this->load->view('rawat_inap/tab_contents/bill', ['show_action' => true]); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<div class="modal fade" id="tambah_pemeriksaan" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-mid" role="document">
        <div class="modal-content modal-media-content">
            <div class="modal-header modal-media-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="box-title">Tambah Pemeriksaan</h4>
            </div>

            <form id="form_diagnosis" method="post" action="<?php echo base_url() ?>HCU/tambah/pemeriksaan">
                <div class="modal-body pt0 pb0">
                    <div class="ptt10">
                        <div class="row">
                            <div class="col-md-12">
                                <input type="hidden" name="rawat_inap_id" value="<?=$rawat_inap->id?>">
                                <input type="hidden" name="transfer_id" value="<?=$transfer_id?>">
                                <input type="hidden" name="pasien_id" value="<?=$pasien->id?>">
                                <div class="form-group">
                                    <label for="tanggal_pemeriksaan" class="col-sm-3 control-label">Tgl Pemeriksaan</label>
                                    <div class="col-sm-9">
                                        <input type="date" class="form-control margin-bottom" id="tanggal_pemeriksaan" name="tanggal_pemeriksaan" value="<?=date('Y-m-d')?>" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="tujuan" class="col-sm-3 control-label">Dokter</label>
                                    <div class="col-sm-9 margin-bottom">
                                        <select class="form-control abdush-select margin-bottom" name="dokter_id" required>
                                            <option value="" selected>-- Pilih Dokter --</option>
                                            <?php foreach ($dokter->result() as $key => $value) { ?>
                                                <option value="<?php echo $value->id ?>"><?php echo $value->nama; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="subjektif" class="col-sm-3 control-label">Subjektif</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control margin-bottom" id="subjektif" name="subjektif" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="subjektif" class="col-sm-3 control-label">Objektif</label>
                                    <div class="col-md-12">
                                        <div class="col-sm-6 col-md-6 col-lg-6 form-group">
                                            <label for="td" class="col-sm-4 control-label">TD</label>
                                            <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">
                                                <input type="text" class="form-control requirable" name="objektif[td]" id="td">
                                                <span class="input-group-addon">mmHg</span>
                                            </div>
                                        </div>
                                        <div class="col-sm-6 col-md-6 col-lg-6 form-group">
                                            <label for="inputEmail3" class="col-sm-4 control-label">R</label>
                                            <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">
                                                <input type="text" class="form-control requirable" name="objektif[r]" id="r">
                                                <span class="input-group-addon">K/Min</span>
                                            </div>
                                        </div>
                                        <div class="col-sm-6 col-md-6 col-lg-6 form-group">
                                            <label for="inputEmail3" class="col-sm-4 control-label">BB</label>
                                            <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">
                                                <input type="text" class="form-control requirable" id="bb" name="objektif[bb]"
                                                       onkeyup="set_bmi()">
                                                <span class="input-group-addon">Kg</span>
                                            </div>
                                        </div>
                                        <div class="col-sm-6 col-md-6 col-lg-6 form-group">
                                            <label for="inputEmail3" class="col-sm-4 control-label">N</label>
                                            <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">
                                                <input type="text" class="form-control requirable" name="n" id="objektif[n]">
                                                <span class="input-group-addon">K/Min</span>
                                            </div>
                                        </div>
                                        <div class="col-sm-6 col-md-6 col-lg-6 form-group">
                                            <label for="inputEmail3" class="col-sm-4 control-label">S</label>
                                            <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">
                                                <input type="text" class="form-control requirable" id="s" name="objektif[s]">
                                                <span class="input-group-addon">'0</span>
                                            </div>
                                        </div>
                                        <div class="col-sm-6 col-md-6 col-lg-6 form-group">
                                            <label for="inputEmail3" class="col-sm-4 control-label">TB</label>
                                            <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">
                                                <input type="text" class="form-control requirable" id="tb" name="objektif[tb]"
                                                       onkeyup="set_bmi()">
                                                <span class="input-group-addon">cm</span>
                                            </div>
                                        </div>
                                        <div class="col-sm-6 col-md-6 col-lg-6 form-group">
                                            <label for="inputEmail3" class="col-sm-4 control-label">BMI</label>
                                            <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">
                                                <input type="text" class="form-control requirable" name="objektif[bmi]" id="bmi">

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="asesmen" class="col-sm-3 control-label">Asesmen</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control margin-bottom" id="asesmen" name="asesmen" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="plan" class="col-sm-3 control-label">Plan</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control margin-bottom" id="plan" name="plan" required>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    <button type="submit" id="form_diagnosisbtn"
                            data-loading-text="Menyimpan..."
                            class="btn btn-info pull-right">Simpan</button>
                </div>
            </form>


        </div>
    </div>
</div>

<div class="modal fade" id="tambah_diagnosis" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-mid" role="document">
        <div class="modal-content modal-media-content">
            <div class="modal-header modal-media-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="box-title">Tambah Diagnosis</h4>
                <a href="#" class="btn btn-sm btn-primary dropdown-toggle" style="position: absolute; right: 40px; top: 20px;"
                   onclick="holdModal('tambah_penyakit')" data-toggle="modal"><i class="fas fa-plus"></i> Tambah Penyakit
                </a>
            </div>

            <form id="form_diagnosis" method="post" action="<?php echo base_url() ?>HCU/tambah/diagnosis">
                <div class="modal-body pt0 pb0">
                    <div class="ptt10">
                        <div class="row">
                            <div class="col-md-12">
                                <input type="hidden" name="rawat_inap_id" value="<?=$rawat_inap->id?>">
                                <input type="hidden" name="transfer_id" value="<?=$transfer_id?>">
                                <input type="hidden" name="pasien_id" value="<?=$pasien->id?>">
                                <div class="form-group margin-bottom">
                                    <label for="tujuan" class="col-sm-3 control-label">Penyakit</label>
                                    <div class="col-sm-9">
                                        <select class="form-control select2 margin-bottom" multiple="multiple" name="penyakit[]" required data-placeholder="Pilih penyakit">
                                            <?php foreach ($penyakit as $key => $value) { ?>
                                                <option value="<?php echo $value->id ?>"><?php echo $value->kode.' - '.$value->nama ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="diagnosis" class="col-sm-3 control-label">Diagnosis</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" id="diagnosis" name="diagnosis" placeholder="Diagnosis">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    <button type="submit" id="form_diagnosisbtn"
                            data-loading-text="Menyimpan..."
                            class="btn btn-info pull-right">Simpan</button>
                </div>
            </form>


        </div>
    </div>
</div>

<div class="modal fade" id="tambah_resep" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-mid" role="document">
        <div class="modal-content modal-media-content">
            <div class="modal-header modal-media-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="box-title">Tambah Resep</h4>
            </div>

            <form id="form_diagnosis" method="post" action="<?php echo base_url() ?>HCU/tambah/resep">
                <div class="modal-body pt0 pb0">
                    <input type="hidden" name="rawat_inap_id" value="<?=$rawat_inap->id?>">
                    <input type="hidden" name="transfer_id" value="<?=$transfer_id?>">
                    <div class="nav-tabs-custom">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true">Obat Satuan</a></li>
                            <li class=""><a href="#tab_2" data-toggle="tab" aria-expanded="false">Obat Racik</a></li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active" id="tab_1">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-sm-8">
                                                <select id="obat-option">
                                                    <option value="">-- Pilih Obat --</option>
                                                    <?php foreach ($obat1 as $key => $value) { ?>
                                                        <option value="<?= $value->id ?>"><?= $value->nama ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>

                                            <div class="col-sm-4">
                                                <button type="button" name="button"
                                                        class="btn btn-sm btn-block btn-primary"
                                                        id="button-add-obat"><i class="fa fa-plus"></i>
                                                    Tambah obat
                                                </button>
                                                <input type="hidden" id="abdush-counter2" value="0">
                                            </div>

                                        </div>

                                        <div class="form-area-obat" style="margin-top:15px;">
                                            <table class="table table-striped table-hover">
                                                <thead>
                                                <tr>
                                                    <th>Nama</th>
                                                    <th>Stok Obat</th>
                                                    <th>Jml</th>
                                                    <th>Signa</th>

                                                </tr>
                                                </thead>
                                                <tbody>

                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /.tab-pane -->
                            <div class="tab-pane" id="tab_2">
                                <div class="row">
                                    <div class="col-md-12">
                                        <?php for ($i = 1; $i < 9; $i++) { ?>
                                            <div class="row ">
                                                <div class="col-md-12">
                                                    <div style="margin-top: 20px;">
                                                        <label for="obat">Racikan <?= $i; ?></label>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-8">
                                                            <select name="obat_racikan<?= $i; ?>[]" id="obat-racik-option-<?=$i?>">
                                                                <option value="">-- Pilih Obat --</option>
                                                                <?php foreach ($obat1 as $key => $value) { ?>
                                                                    <option value="<?= $value->id ?>"><?= $value->nama ?></option>
                                                                <?php } ?>
                                                            </select>
                                                        </div>

                                                        <div class="col-sm-4">
                                                            <button type="button" name="button"
                                                                    class="btn btn-sm btn-block btn-primary"
                                                                    id="button-add-obat-racik-<?=$i?>"><i class="fa fa-plus"></i>
                                                                Tambah obat
                                                            </button>
                                                            <input type="hidden" id="abdush-counter-<?=$i?>" value="0">
                                                        </div>

                                                    </div>

                                                    <div class="form-area-obat-racik-<?=$i?>" style="margin-top:15px;">
                                                        <table class="table table-striped table-hover">
                                                            <thead>
                                                            <tr>
                                                                <th>Nama</th>
                                                                <th>Stok Obat</th>
                                                                <th>Jml</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>

                                                            </tbody>
                                                        </table>
                                                        <input type="text"
                                                               class="form-control"
                                                               id="signa-obat-racik-<?= $i; ?>"
                                                               name="signa<?= $i; ?>"
                                                               placeholder="signa">
                                                        <input type="text"
                                                               class="form-control"
                                                               name="catatan<?= $i; ?>"
                                                               placeholder="catatan">
                                                    </div>
                                                </div>
                                            </div>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    <button type="submit" id="form_diagnosisbtn"
                            data-loading-text="Menyimpan..."
                            class="btn btn-info pull-right">Simpan</button>
                </div>
            </form>


        </div>
    </div>
</div>

<div class="modal fade" id="tambah_bahan_habis_pakai" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-mid" role="document">
        <div class="modal-content modal-media-content">
            <div class="modal-header modal-media-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="box-title">Tambah Bahan Habis Pakai</h4>
            </div>

            <form id="form_diagnosis" method="post" action="<?php echo base_url() ?>HCU/tambah/bahan_habis_pakai">
                <div class="modal-body pt0 pb0">
                    <input type="hidden" name="rawat_inap_id" value="<?=$rawat_inap->id?>">
                    <input type="hidden" name="transfer_id" value="<?=$transfer_id?>">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-sm-8">
                                    <select id="bahan-option">
                                        <option value="">-- Pilih Bahan --</option>
                                        <?php foreach ($bahan as $key => $value) { ?>
                                            <option value="<?= $value->id ?>"><?= $value->nama ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="col-sm-4">
                                    <button type="button" name="button"
                                            class="btn btn-sm btn-block btn-primary"
                                            id="button-add-bahan"><i class="fa fa-plus"></i>
                                        Tambah Bahan
                                    </button>
                                    <input type="hidden" id="abdush-counter2" value="0">
                                </div>
                            </div>
                            <div class="form-area" style="margin-top:15px;">
                                <table class="table table-striped table-hover">
                                    <thead>
                                    <tr>
                                        <th>Nama Bahan</th>
                                        <th>Stok</th>
                                        <th>Jml</th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    <button type="submit" id="form_diagnosisbtn"
                            data-loading-text="Menyimpan..."
                            class="btn btn-info pull-right">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="tambah_biaya" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-mid" role="document">
        <div class="modal-content modal-media-content">
            <div class="modal-header modal-media-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="box-title">Tambah Biaya</h4>
            </div>

            <form method="post" action="<?php echo base_url() ?>HCU/tambah/biaya">
                <div class="modal-body pt0 pb0">
                    <div class="ptt10">
                        <div class="row">
                            <div class="col-md-12">
                                <input type="hidden" name="transfer_id" value="<?=$transfer_id?>">
                                <input type="hidden" name="rawat_inap_id" value="<?=$rawat_inap->id?>">
                                <input type="hidden" name="pasien_id" value="<?=$pasien->id?>">
                                <input type="hidden" id="jenis_biaya" name="jenis_biaya" value="#biaya_3">
                                <!--                                <div class="form-group">-->
                                <!--                                    <label for="tangal" class="col-sm-3 control-label">Tanggal</label>-->
                                <!--                                    <div class="form-group">-->
                                <!--                                        <div class='input-group date' id='datetimepicker1'>-->
                                <!--                                            <input type='text' class="form-control" name="tanggal" value="--><?//=date('Y-m-d H:i')?><!--" required/>-->
                                <!--                                            <span class="input-group-addon">-->
                                <!--                                                <span class="glyphicon glyphicon-calendar"></span>-->
                                <!--                                            </span>-->
                                <!--                                        </div>-->
                                <!--                                    </div>-->
                                <!--                                </div>-->

                                <div class="form-group">
                                    <div class="nav-tabs-custom">
                                        <ul class="nav nav-tabs" id="tab_biaya">
                                            <!--                                            <li class="active"><a href="#biaya_1" data-toggle="tab" aria-expanded="true">Biaya Kamar</a></li>-->
<!--                                            <li class="active"><a href="#biaya_2" data-toggle="tab" aria-expanded="false">Makanan</a></li>-->
                                            <li class="active"><a href="#biaya_3" data-toggle="tab" aria-expanded="false">Tindakan</a></li>
                                        </ul>
                                        <div class="tab-content">
                                            <div class="tab-pane" id="biaya_1">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <input type="hidden" name="pasien_id" value="<?=$pasien->id?>">
                                                        <div class="form-group mt-2">
                                                            <label for="bed_group" class="col-sm-3 control-label">Bed Group</label>
                                                            <div class="col-sm-9">
                                                                <input type="text" class="form-control margin-bottom" id="bed_group"
                                                                       value="<?=$rawat_inap->bedgroup?>"
                                                                       name="bed_group" readonly>
                                                            </div>
                                                        </div>
                                                        <div class="mt-2">
                                                            <div class="form-group">
                                                                <label for="bed_type" class="col-sm-3 control-label">Bed Type</label>
                                                                <div class="col-sm-9">
                                                                    <input type="text" class="form-control margin-bottom" id="bed_type"
                                                                           value="<?=$rawat_inap->bed_type_name?>"
                                                                           name="bed_type" readonly>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="mt-2">
                                                            <div class="form-group">
                                                                <label for="bed" class="col-sm-3 control-label">Bed</label>
                                                                <div class="col-sm-9">
                                                                    <input type="text" class="form-control margin-bottom"
                                                                           name="bed" value="<?=$rawat_inap->bed_name?>" readonly>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="mt-2">
                                                            <div class="form-group">
                                                                <label for="harga_bed" class="col-sm-3 control-label">Harga</label>
                                                                <div class="col-sm-9">
                                                                    <input type="text" class="form-control margin-bottom" id="harga_bed"
                                                                           name="harga_bed" value="<?=$rawat_inap->bedgroup_price?>" readonly>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="tab-pane" id="biaya_2">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <input type="hidden" name="harga_makanan" id="harga_makanan">
                                                        <div class="form-group">
                                                            <label for="tujuan" class="col-sm-3 control-label">Tipe Makanan</label>
                                                            <div class="col-sm-9 margin-bottom">
                                                                <select class="form-control abdush-select margin-bottom" name="tipe_makanan" onchange="getMakananByTipeId(this.value)">
                                                                    <option value="">--Pilih Tipe Makanan--</option>
                                                                    <?php foreach ($tipe_makanan as $key => $value) { ?>
                                                                        <option value="<?php echo $value->id ?>"><?php echo $value->nama ?></option>
                                                                    <?php } ?>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="tujuan" class="col-sm-3 control-label margin-bottom">Nama Makanan</label>
                                                            <div class="col-sm-9 margin-bottom">
                                                                <select class="form-control abdush-select" name="makanan" id="makanan">

                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="tab-pane active" id="biaya_3">
                                                <div class="impbtnview-tindakan">
                                                    <a href="#" class="btn btn-sm btn-primary dropdown-toggle"
                                                       onclick="holdModal('tambah_tindakan')" data-toggle="modal"><i class="fas fa-plus"></i> Tambah Tindakan
                                                    </a>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label for="tindakan[]" class="col-sm-3 control-label">Tindakan</label>
                                                            <div class="col-sm-9" style="margin-bottom: 10px;">
                                                                <select class="form-control select2" multiple="multiple"
                                                                        data-placeholder="Tindakan"
                                                                        id="tindakan" name="tindakan[]">
                                                                    <?php foreach ($tindakan as $key => $value) { ?>
                                                                        <option value="<?php echo $value->id ?>"><?php echo $value->nama . " - Rp." . harga($value->tarif_pasien) ?></option>
                                                                    <?php } ?>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <div class="col-sm-12" style="margin-bottom: 10px; height: 3px !important;">
                                                                <hr style="margin: 0 !important;">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="perawat" class="col-sm-3 control-label">Dokter Anestesi</label>
                                                            <div class="col-sm-9" style="margin-bottom: 10px;">
                                                                <select class="form-control select2"
                                                                        data-placeholder="Dokter Anestesi"
                                                                        id="dokter_anestesi" name="dokter_anestesi">
                                                                    <option value="">--Pilih dokter anestesi--</option>
                                                                    <?php foreach ($dokter->result() as $key => $value) { ?>
                                                                        <option value="<?php echo $value->id ?>"><?php echo ucwords($value->nama) ?></option>
                                                                    <?php } ?>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="tindakan_anestesi[]" class="col-sm-3 control-label">Tindakan Dokter Anestesi</label>
                                                            <div class="col-sm-9" style="margin-bottom: 10px;">
                                                                <select class="form-control select2" multiple="multiple"
                                                                        data-placeholder="Tindakan"
                                                                        id="tindakan_anestesi" name="tindakan_anestesi[]">
                                                                    <?php foreach ($tindakan as $key => $value) { ?>
                                                                        <option value="<?php echo $value->id ?>"><?php echo $value->nama . " - Rp." . harga($value->tarif_pasien) ?></option>
                                                                    <?php } ?>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <div class="col-sm-12" style="margin-bottom: 10px; height: 3px !important;">
                                                                <hr style="margin: 0 !important;">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="perawat" class="col-sm-3 control-label">Dokter Tamu</label>
                                                            <div class="col-sm-9" style="margin-bottom: 10px;">
                                                                <select class="form-control select2" multiple="multiple"
                                                                        data-placeholder="Dokter Tamu"
                                                                        id="dokter_tamu" name="dokter_tamu[]">
                                                                    <?php foreach ($dokter->result() as $key => $value) { ?>
                                                                        <option value="<?php echo $value->id ?>"><?php echo ucwords($value->nama) ?></option>
                                                                    <?php } ?>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="tindakan_tamu[]" class="col-sm-3 control-label">Tindakan Dokter Tamu</label>
                                                            <div class="col-sm-9" style="margin-bottom: 10px;">
                                                                <select class="form-control select2" multiple="multiple"
                                                                        data-placeholder="Tindakan"
                                                                        id="tindakan_tamu" name="tindakan_tamu[]">
                                                                    <?php foreach ($tindakan as $key => $value) { ?>
                                                                        <option value="<?php echo $value->id ?>"><?php echo $value->nama . " - Rp." . harga($value->tarif_pasien) ?></option>
                                                                    <?php } ?>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <div class="col-sm-12" style="margin-bottom: 10px; height: 3px !important;">
                                                                <hr style="margin: 0 !important;">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="perawat" class="col-sm-3 control-label">Perawat</label>
                                                            <div class="col-sm-9" style="margin-bottom: 10px;">
                                                                <select class="form-control select2" multiple="multiple"
                                                                        data-placeholder="Perawat"
                                                                        id="perawat" name="perawat[]">
                                                                    <?php foreach ($listPerawat->result() as $key => $value) { ?>
                                                                        <option value="<?php echo $value->id ?>"><?php echo ucwords($value->nama) ?></option>
                                                                    <?php } ?>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="perawat" class="col-sm-3 control-label">Perawat Tamu</label>
                                                            <div class="col-sm-9">
                                                                <select class="form-control select2" multiple="multiple"
                                                                        data-placeholder="Perawat Tamu"
                                                                        id="perawat_tamu" name="perawat_tamu[]">
                                                                    <?php foreach ($listPerawat->result() as $key => $value) { ?>
                                                                        <option value="<?php echo $value->id ?>"><?php echo ucwords($value->nama) ?></option>
                                                                    <?php } ?>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    <button type="submit" id="form_diagnosisbtn"
                            data-loading-text="Menyimpan..."
                            class="btn btn-info pull-right">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="tambah_tindakan" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-mid" role="document">
        <div class="modal-content modal-media-content">
            <div class="modal-header modal-media-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="box-title">Tambah Master Tindakan HCU</h4>
            </div>

            <form id="form_add_tindakan" method="post" action="<?php echo base_url() ?>MasterPoli/addTindakanRanap">
                <div class="modal-body pt0 pb0">
                    <div class="row">
                        <div class="col-md-12">
                            <input type="hidden" name="redirect_to" value="HCU/detail/<?=$rawat_inap->id?>/<?=$transfer_id?>">
                            <input type="hidden" name="jenis" value="hcu">
                            <div class="form-group">
                                <label for="nama" class="col-sm-4 control-label" >Nama Tindakan</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" id="nama_tindakan" name="nama_tindakan" onkeyup="set_tarifpasien()" placeholder="Masukkan nama tindakan" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="nama" class="col-sm-4 control-label" >JM Perawat</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" id="tarif_perawat" name="tarif_perawat" value="0" onkeyup="set_tarifpasien()" placeholder="Masukkan JM perawat" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="nama" class="col-sm-4 control-label" >JM Dokter</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" id="tarif_dokter" name="tarif_dokter"  value="0" onkeyup="set_tarifpasien()" placeholder="Masukkan jm dokter">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="tarif_apoteker" class="col-sm-4 control-label" >JM Apoteker</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" id="tarif_apoteker" name="tarif_apoteker"  value="0" onkeyup="set_tarifpasien()" placeholder="Masukkan jm apoteker">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="nama" class="col-sm-4 control-label" >Jasa lain-lain</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" id="tarif_lain" name="tarif_lain"  value="0" onkeyup="set_tarifpasien()" placeholder="Masukkan Jasa lain-lain " required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="kategori" class="col-sm-4 control-label">Klinik</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" id="klinik" name="klinik"  value="0" onkeyup="set_tarifpasien()" placeholder="Masukkan tarif untuk klinik" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="tempat_lahir" class="col-sm-4 control-label">Total Tarif Pasien</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" id="tarif_pasien" name="tarif_pasien" placeholder="masukkan total tarif pasien" required>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    <button type="submit" id="form_diagnosisbtn"
                            data-loading-text="Menyimpan..."
                            class="btn btn-info pull-right">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="tambah_penyakit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-mid" role="document">
        <div class="modal-content modal-media-content">
            <div class="modal-header modal-media-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="box-title">Tambah Master Penyakit HCU</h4>
            </div>

            <form id="form_add_tindakan" method="post" action="<?php echo base_url() ?>MasterPoli/addPenyakitRanap">
                <div class="modal-body pt0 pb0">
                    <div class="row">
                        <div class="col-md-12">
                            <input type="hidden" name="redirect_to" value="HCU/detail/<?=$rawat_inap->id?>/<?=$transfer_id?>">
                            <input type="hidden" name="jenis" value="hcu">
                            <div class="form-group">
                                <label for="nama" class="col-sm-4 control-label" >Kode</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" id="kode" name="kode"  placeholder="Masukkan kode" required>
                                </div>
                            </div>
                            <div class="form-group" style="margin-top: 40px">
                                <label for="nama" class="col-sm-4 control-label" >Nama Penyakit</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" id="nama" name="nama"  placeholder="Masukkan nama penyakit" required>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    <button type="submit" id="form_diagnosisbtn"
                            data-loading-text="Menyimpan..."
                            class="btn btn-info pull-right">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div id="selesai-modal" class="modal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header modal-media-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="box-title">Selesaikan HCU</h4>
            </div>
            <div class="modal-body">
                <form id="form-selesai-langsung-bayar" method="post" action="<?=base_url('HCU/selesaiLangsungBayar/'.$rawat_inap->id.'/'.$transfer_id)?>" style="float: right">
                    <input type="hidden" name="total_biaya" id="total_biaya_2">
                    <p>Pasien ini ditransfer dari rawat jalan. Transfer ke rawat inap atau langsung ke pembayaran?</p>
                    <hr>
                    <button type="button" class="btn btn-primary btn-block" id="transfer-ke-ranap">Transfer ke ranap</button>
                    <button type="button" class="btn btn-success btn-block" id="langsung-ke-pembayaran">Langsung ke pembayaran</button>
                </form>
            </div>
            <div class="modal-footer">
            </div>
        </div>
    </div>
</div>

<script src="<?= base_url() ?>assets/bower_components/select2/dist/js/select2.full.min.js"></script>

<script>

    $(document).ready(function () {
        $('#transfer-ke-ranap').click(function () {
            $('#selesai-modal').modal('toggle');
            $('#form-selesai').submit();
        })
        $('#langsung-ke-pembayaran').click(function () {
            $('#selesai-modal').modal('toggle');
            $('#form-selesai-langsung-bayar').submit();
        })
    })

    function checkIsian() {
        if (!$('.row-pemeriksaan').length) {
            alert('Isi pemeriksaan dahulu');
            return false;
        }
        if (!$('.row-diagnosis').length) {
            alert('Isi diagnosis dahulu');
            return false;
        }
        if (!$('.row-resep').length) {
            alert('Isi resep dahulu');
            return false;
        }
        // if (!$('.row-bhp').length) {
        //     alert('Isi bahan habis pakai dahulu');
        //     return false;
        // }
        if (!$('.row-biaya').length) {
            alert('Isi biaya tindakan dahulu');
            return false;
        }

        const via_bangsal = <?=$rawat_inap->via_bangsal?>;

        if (via_bangsal) {
            return confirm('Apakah HCU ini sudah selesai?')
        }
        else {
            $('#selesai-modal').modal('show');
            return false;
        }
    }

    function set_tarifpasien() {
        var tp = parseInt($('#tarif_perawat').val());
        var td = parseInt($('#tarif_dokter').val());
        var ta = parseInt($('#tarif_apoteker').val());
        var tl = parseInt($('#tarif_lain').val());
        var k = parseInt($('#klinik').val());

        var tarifpasien = tp+td+ta+tl+k;
        $('#tarif_pasien').val(tarifpasien);
    }

    function set_bmi() {
        var tb = $('#tb').val();
        var bb = $('#bb').val();
        var tbm = tb / 100;
        var bmi = bb / (tbm * tbm);

        $('#bmi').val(bmi.toFixed(2));
    }

    function getMakananByTipeId(id) {
        $('#makanan').html("<option value='l'>Loading...</option>");
        $.ajax({
            url: '<?php echo base_url(); ?>HCU/getMakananByTipeId/'+id,
            type: "POST",
            dataType: 'json',
            success: function (res) {
                let div_data = '';
                $.each(res, function (i, obj) {
                    div_data += "<option value='" + obj.id + "'>" + obj.nama + ' - ' + obj.harga + "</option>";
                });
                $('#makanan').html("<option value=''>--Pilih Makanan--</option>");
                $('#makanan').append(div_data);
                $("#makanan").select2("val", '');
            },
            error: function (e) {
                console.log(e);
            }
        });
    }

    $(function () {
        $('.select2').select2();
        $('#tab_biaya a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
            var target = $(e.target).attr("href");
            $('#jenis_biaya').val(target);
            $('#makanan').prop('required', target === '#biaya_2');
            $('#tindakan').prop('required', target === '#biaya_3');
        });
        $('#makanan').on('change', function () {
            const txt = this.options[this.selectedIndex].text;
            const harga = txt.split(' - ')[1];
            $('#harga_makanan').val(harga);
        });

        $('#total_biaya').val(<?=$total_tagihan?>);
        $('#total_biaya_2').val(<?=$total_tagihan?>);

        $('#tambah_tindakan')
            .on('show.bs.modal', function () {
                $('#tambah_biaya').modal('toggle');
            })
            .on('hide.bs.modal', function () {
                $('#tambah_biaya').modal('toggle');
            });
        $('#tambah_penyakit')
            .on('show.bs.modal', function () {
                $('#tambah_diagnosis').modal('toggle');
            })
            .on('hide.bs.modal', function () {
                $('#tambah_diagnosis').modal('toggle');
            });
    });

    function holdModal(modalId) {
        const modal = '#' + modalId;
        $(modal).modal({
            backdrop: 'static',
            keyboard: false,
            show: true
        });
    }

    // $('.select2').select2();

    jQuery(function ($) {
        var bahan = <?php echo json_encode($bahan); ?>;
        $('#bahan-option').select2();

        var obat = <?php echo json_encode($obat1); ?>;
        $('#obat-option').select2();

        $('#button-add-bahan').on('click', function (e) {
            var id_bahan = $('#bahan-option').val();
            if (id_bahan == '') {
                alert('Anda belum memilih bahan habis pakai !');
            }
            else {
                var theBahan = {};
                var counter = parseInt($('#abdush-counter2').val());
                $.each(bahan, function (i, v) {
                    if (v.id == id_bahan) {
                        theBahan = v;
                        return;
                    }
                });

                var html = `
                <tr>
                  <td>
                    ` + theBahan.nama + `
                    <input type="hidden" name="id[]" value="` + theBahan.id + `">
                  </td>
                  <td>` + theBahan.jumlah + ` ` + theBahan.satuan + `</td>
                  <td>
                    <input style="width:65px;" type="text" class="form-control" name="qty[]" id="bahan[` + counter + `][qty]">
                  </td>
                  <td>
                    <button class="btn btn-sm btn-danger btn-delete-row" type="button"> <i class="fa fa-trash"></i> </button>
                  </td>
                </tr>`;

                $('.form-area tbody').append(html);
                $('#abdush-counter').val(counter + 1);
                $('input[name="bahan[' + counter + '][qty]"]').focus();

                $('.btn-delete-row').unbind('click');
                $('.btn-delete-row').each(function () {
                    $(this).on('click', function () {
                        $(this).parents('tr').remove();
                    });
                });
            }

            $('#bahan-option').val('').trigger('change');
        });

        $('#button-add-obat').on('click', function (e) {
            var id_Obat = $('#obat-option').val();
            if (id_Obat == '') {
                alert('Anda belum memilih obat !');
            }
            else {
                var theObat = {};
                var counter = parseInt($('#abdush-counter2').val());
                $.each(obat, function (i, v) {
                    if (v.id == id_Obat) {
                        theObat = v;
                        return;
                    }
                });

                var html = `
                <tr>
                  <td>
                    ` + theObat.nama + `
                    <input type="hidden" name="nama_obat[]" value="` + theObat.id + `">
                  </td>
                  <td>` + theObat.stok_obat + ` item </td>
                  <input type="hidden" id="stok` + counter + `" value="` + theObat.stok_obat + `">
                  <td>
                    <input style="width:65px;" type="text" class="form-control" onchange="loadData(` + counter + `);" name="jumlah_satuan[]" id="jumlah_satuan` + counter + `">
                  </td>
                  <td>
                    <input style="width:100px;" type="text" class="form-control"  name="signa_obat[]" id="signa_obat` + counter + `">
                  </td>
                  <td>
                    <button class="btn btn-sm btn-danger btn-delete-row" type="button"> <i class="fa fa-trash"></i> </button>
                  </td>
                </tr>

                `;

                $('.form-area-obat tbody').append(html);
                $('#abdush-counter2').val(counter + 1);
                $('input[id="jumlah_satuan' + counter + '"]').focus();

                $('.btn-delete-row').unbind('click');
                $('.btn-delete-row').each(function () {
                    $(this).on('click', function () {
                        $(this).parents('tr').remove();
                    });
                });
            }

            $('#obat-option').val('').trigger('change');
        });

        let initializer = {
            init: function() {
                for (let i = 1; i < 9; i++) {
                    $(`#obat-racik-option-${i}`).select2();
                    $(`#button-add-obat-racik-${i}`).on('click', e => {
                        this.onBtnAddClick(i);
                    });
                }
            },
            checkHasObat: function(i) {
                let rowCount = $(`.form-area-obat-racik-${i} >table >tbody >tr`).length;
                if (rowCount === 0) {
                    $(`#signa-obat-racik-${i}`).prop('required',false);
                }
            },
            onBtnAddClick: function(i) {
                let that = this;
                let opt = $(`#obat-racik-option-${i}`);
                let id_Obat = opt.val();
                if (id_Obat === '') {
                    alert('Anda belum memilih obat !');
                }
                else {
                    let theObat = {};
                    let counter = parseInt($(`#abdush-counter-${i}`).val());
                    $.each(obat, function (i, v) {
                        if (v.id === id_Obat) {
                            theObat = v;
                            return;
                        }
                    });

                    $(`.form-area-obat-racik-${i} tbody`).append(this.getTableRow(i, theObat, counter));
                    $(`#signa-obat-racik-${i}`).prop('required',true);
                    $(`#abdush-counter-${i}`).val(counter + 1);
                    $('input[id="jumlah_satuan_racik' + counter + '"]').focus();

                    $('.btn-delete-row').unbind('click');
                    $('.btn-delete-row').each(function () {
                        $(this).on('click', function () {
                            $(this).parents('tr').remove();
                            that.checkHasObat(i);
                        });
                    });
                }
                opt.val('').trigger('change');
            },
            getTableRow: (i, obat, counter) => (`
                <tr>
                  <td>
                    ` + obat.nama + `
                    <input type="hidden" name="nama_obat_racikan${i}[]" value="` + obat.id + `">
                  </td>
                  <td>` + obat.stok_obat + ` item </td>
                  <input type="hidden" id="stok` + counter + `" value="` + obat.stok_obat + `">
                  <td>
                    <input style="width:65px;" type="text" class="form-control" onchange="loadData(` + counter + `);" name="jumlah_satuan_racikan${i}[]" id="jumlah_satuan_racikan${i}` + counter + `">
                  </td>
                  <td>
                    <button class="btn btn-sm btn-danger btn-delete-row" type="button"> <i class="fa fa-trash"></i> </button>
                  </td>
                </tr>
            `)
        };

        initializer.init();
    });
</script>
