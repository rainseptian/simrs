<div class="modal fade" id="tambah_edukasi" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-mid" role="document">
        <div class="modal-content modal-media-content">
            <div class="modal-header modal-media-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="box-title">Tambah Edukasi</h4>
            </div>

            <form method="post" action="<?php echo base_url() ?>RawatInap/save_form/edukasi/form_edukasi">
                <div class="modal-body pt0 pb0">
                    <div class="ptt10">
                        <div class="row">
                            <div class="col-md-12">
                                <input type="hidden" name="rawat_inap_id" value="<?=$rawat_inap->id?>">
                                <input type="hidden" name="pasien_id" value="<?=$pasien->id?>">

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="tujuan" class="col-sm-3 control-label" style="padding-left: 0 !important;">Tanggal/Jam</label>
                                            <input type="datetime-local" name="form[edukasi][date]" class="form-control" required value="<?=date('Y-m-d H:i')?>">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="tujuan" class="col-sm-3 control-label" style="padding-left: 0 !important;">Edukasi</label>
                                            <textarea name="form[edukasi][edukasi]" class="form-control" required></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    <button type="submit" id="form_diagnosisbtn"
                            data-loading-text="Menyimpan..."
                            class="btn btn-info pull-right">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>