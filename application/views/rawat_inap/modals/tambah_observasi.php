<div class="modal fade" id="tambah_observasi" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-mid modal-lg" role="document">
        <div class="modal-content modal-media-content">
            <div class="modal-header modal-media-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="box-title">Tambah Observasi</h4>
            </div>

            <form method="post" action="<?php echo base_url() ?>RawatInap/save_form/observasi/lembar_observasi">
                <input type="hidden" name="rawat_inap_id" value="<?=$rawat_inap->id?>">
                <input type="hidden" name="pasien_id" value="<?=$pasien->id?>">
                <div class="modal-body pt0 pb0">
                    <div class="ptt10">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="tujuan" class="control-label" style="padding-left: 0 !important;">Tgl</label>
                                    <input type="date" name="form[observasi][tgl]" class="form-control" required value="<?=date('Y-m-d')?>">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="tujuan" class="control-label" style="padding-left: 0 !important;">Jam</label>
                                    <input type="time" name="form[observasi][jam]" class="form-control" required value="<?=date('H:i')?>">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="tujuan" class="control-label" style="padding-left: 0 !important;">Cairan Intravena Menit</label>
                                    <input type="text" name="form[observasi][cairan]" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="tujuan" class="control-label" style="padding-left: 0 !important;">Tetesan (X/menit)</label>
                                    <input type="text" name="form[observasi][tetesan]" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="tujuan" class="control-label" style="padding-left: 0 !important;">Tensi (mmHg)</label>
                                    <input type="text" name="form[observasi][tensi]" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="tujuan" class="control-label" style="padding-left: 0 !important;">Nadi (X/menit)</label>
                                    <input type="text" name="form[observasi][nadi]" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="tujuan" class="control-label" style="padding-left: 0 !important;">Suhu (ºC)</label>
                                    <input type="text" name="form[observasi][suhu]" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="tujuan" class="control-label" style="padding-left: 0 !important;">Pernafasan (X/menit)</label>
                                    <input type="text" name="form[observasi][pernafasan]" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="tujuan" class="control-label" style="padding-left: 0 !important;">Input</label>
                                    <input type="text" name="form[observasi][input]" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="tujuan" class="control-label" style="padding-left: 0 !important;">Output</label>
                                    <input type="text" name="form[observasi][output]" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="tujuan" class="control-label" style="padding-left: 0 !important;">Ket</label>
                                    <input type="text" name="form[observasi][ket]" class="form-control">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    <button type="submit" id="form_diagnosisbtn"
                            data-loading-text="Menyimpan..."
                            class="btn btn-info pull-right">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>