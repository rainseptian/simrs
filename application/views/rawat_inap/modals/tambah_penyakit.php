<div class="modal fade" id="tambah_penyakit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-mid" role="document">
        <div class="modal-content modal-media-content">
            <div class="modal-header modal-media-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="box-title">Tambah Master Penyakit Rawat Inap</h4>
            </div>

            <form id="form_add_tindakan" method="post" action="<?php echo base_url() ?>MasterPoli/addPenyakitRanap">
                <div class="modal-body pt0 pb0">
                    <div class="row">
                        <div class="col-md-12">
                            <input type="hidden" name="redirect_to" value="RawatInap/detail/<?=$rawat_inap->id?>">
                            <input type="hidden" name="jenis" value="rawat_inap">
                            <div class="form-group">
                                <label for="nama" class="col-sm-4 control-label" >Kode</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" id="kode" name="kode"  placeholder="Masukkan kode" required>
                                </div>
                            </div>
                            <div class="form-group" style="margin-top: 40px">
                                <label for="nama" class="col-sm-4 control-label" >Nama Penyakit</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" id="nama" name="nama"  placeholder="Masukkan nama penyakit" required>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    <button type="submit" id="form_diagnosisbtn"
                            data-loading-text="Menyimpan..."
                            class="btn btn-info pull-right">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>