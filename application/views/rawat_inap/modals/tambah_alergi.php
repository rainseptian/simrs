<div class="modal fade" id="tambah_alergi" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-mid modal-lg" role="document">
        <div class="modal-content modal-media-content">
            <div class="modal-header modal-media-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="box-title">Tambah Rekonsiliasi</h4>
            </div>

            <form method="post" action="<?php echo base_url() ?>RawatInap/save_form/alergi/alergi">
                <input type="hidden" name="rawat_inap_id" value="<?=$rawat_inap->id?>">
                <input type="hidden" name="pasien_id" value="<?=$pasien->id?>">
                <div class="modal-body pt0 pb0">
                    <div class="ptt10">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="tujuan" class="control-label" style="padding-left: 0 !important;">
                                        Nama Obat (Nama Generik)
                                    </label>
                                    <input type="text" name="form[alergi][nm_generik]" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="tujuan" class="control-label" style="padding-left: 0 !important;">
                                        Nama Obat (Nama Dagang)
                                    </label>
                                    <input type="text" name="form[alergi][nm_dagang]" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="tujuan" class="control-label" style="padding-left: 0 !important;">
                                        Dosis
                                    </label>
                                    <input type="text" name="form[alergi][dosis]" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="tujuan" class="control-label" style="padding-left: 0 !important;">
                                        Aturan Pakai
                                    </label>
                                    <input type="text" name="form[alergi][aturan]" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="tujuan" class="control-label" style="padding-left: 0 !important;">
                                        Rute Pemberian
                                    </label>
                                    <input type="text" name="form[alergi][rute]" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="tujuan" class="control-label" style="padding-left: 0 !important;">
                                        Jumlah Obat
                                    </label>
                                    <input type="text" name="form[alergi][jumlah]" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="tujuan" class="control-label" style="padding-left: 0 !important;">
                                        Obat Yang Digunakan Saat Rawat
                                    </label>
                                    <br>
                                    <input type="radio" name="form[alergi][saat_rawat]" value="y">
                                    <label>Ya</label>
                                    <br>
                                    <input type="radio" name="form[alergi][saat_rawat]" value="n">
                                    <label>Tidak</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    <button type="submit" id="form_diagnosisbtn"
                            data-loading-text="Menyimpan..."
                            class="btn btn-info pull-right">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>