<div class="modal fade" id="tambah_surveilans" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-mid modal-lg" role="document">
        <div class="modal-content modal-media-content">
            <div class="modal-header modal-media-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="box-title">Tambah Surveilans</h4>
            </div>

            <form method="post" action="<?php echo base_url() ?>RawatInap/save_form/surveilans/surveilans">
                <input type="hidden" name="rawat_inap_id" value="<?=$rawat_inap->id?>">
                <input type="hidden" name="pasien_id" value="<?=$pasien->id?>">
                <div class="modal-body pt0 pb0">
                    <div class="ptt10">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="tujuan" class="control-label" style="padding-left: 0 !important;">Tgl/Jam</label>
                                    <input type="datetime-local" name="form[surveilans][tgl]" class="form-control" required value="<?=date('Y-m-d H:i')?>">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="tujuan" class="control-label" style="padding-left: 0 !important;">Suhu</label>
                                    <input type="text" name="form[surveilans][suhu]" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="tujuan" class="control-label" style="padding-left: 0 !important;">Kateter</label>
                                    <input type="text" name="form[surveilans][Kateter]" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="tujuan" class="control-label" style="padding-left: 0 !important;">Vena Perifer</label>
                                    <input type="text" name="form[surveilans][VenaPerifer]" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="tujuan" class="control-label" style="padding-left: 0 !important;">Vena Sentral</label>
                                    <input type="text" name="form[surveilans][VenaSentral]" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="tujuan" class="control-label" style="padding-left: 0 !important;">Ventilator</label>
                                    <input type="text" name="form[surveilans][Ventilator]" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="tujuan" class="control-label" style="padding-left: 0 !important;">VAP</label>
                                    <input type="text" name="form[surveilans][VAP]" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="tujuan" class="control-label" style="padding-left: 0 !important;">HAB</label>
                                    <input type="text" name="form[surveilans][HAB]" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="tujuan" class="control-label" style="padding-left: 0 !important;">ISK</label>
                                    <input type="text" name="form[surveilans][ISK]" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="tujuan" class="control-label" style="padding-left: 0 !important;">LADP</label>
                                    <input type="text" name="form[surveilans][LADP]" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="tujuan" class="control-label" style="padding-left: 0 !important;">PLEB</label>
                                    <input type="text" name="form[surveilans][PLEB]" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="tujuan" class="control-label" style="padding-left: 0 !important;">DECUB</label>
                                    <input type="text" name="form[surveilans][DECUB]" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="tujuan" class="control-label" style="padding-left: 0 !important;">ILO</label>
                                    <input type="text" name="form[surveilans][ILO]" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="tujuan" class="control-label" style="padding-left: 0 !important;">TIRAH BARING</label>
                                    <input type="text" name="form[surveilans][TIRAHBARING]" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="tujuan" class="control-label" style="padding-left: 0 !important;">HASIL KULTUR / LABORAT</label>
                                    <input type="text" name="form[surveilans][HASIL]" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="tujuan" class="control-label" style="padding-left: 0 !important;">ANTIBIOTIK</label>
                                    <input type="text" name="form[surveilans][ANTIBIOTIK]" class="form-control">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    <button type="submit" id="form_diagnosisbtn"
                            data-loading-text="Menyimpan..."
                            class="btn btn-info pull-right">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>