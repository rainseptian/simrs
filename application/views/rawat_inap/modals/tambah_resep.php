<div class="modal fade" id="tambah_resep" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-mid" role="document">
        <div class="modal-content modal-media-content">
            <div class="modal-header modal-media-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="box-title">Tambah Resep</h4>
            </div>

            <form id="form_diagnosis" method="post" action="<?php echo base_url() ?>RawatInap/tambah/resep">
                <div class="modal-body pt0 pb0">
                    <input type="hidden" name="rawat_inap_id" value="<?=$rawat_inap->id?>">
                    <div class="nav-tabs-custom">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true">Obat Satuan</a></li>
                            <li class=""><a href="#tab_2" data-toggle="tab" aria-expanded="false">Obat Racik</a></li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active" id="tab_1">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-sm-8">
                                                <select id="obat-option">
                                                    <option value="">-- Pilih Obat --</option>
                                                    <?php foreach ($obat1 as $key => $value) { ?>
                                                        <option value="<?= $value->id ?>"><?= $value->nama ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>

                                            <div class="col-sm-4">
                                                <button type="button" name="button"
                                                        class="btn btn-sm btn-block btn-primary"
                                                        id="button-add-obat"><i class="fa fa-plus"></i>
                                                    Tambah obat
                                                </button>
                                                <input type="hidden" id="abdush-counter2" value="0">
                                            </div>

                                        </div>

                                        <div class="form-area-obat" style="margin-top:15px;">
                                            <table class="table table-striped table-hover">
                                                <thead>
                                                <tr>
                                                    <th>Nama</th>
                                                    <th>Stok Obat</th>
                                                    <th>Jml</th>
                                                    <th>Signa</th>

                                                </tr>
                                                </thead>
                                                <tbody>

                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /.tab-pane -->
                            <div class="tab-pane" id="tab_2">
                                <div class="row">
                                    <div class="col-md-12">
                                        <?php for ($i = 1; $i < 9; $i++) { ?>
                                            <div class="row ">
                                                <div class="col-md-12">
                                                    <div style="margin-top: 20px;">
                                                        <label for="obat">Racikan <?= $i; ?></label>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-8">
                                                            <select name="obat_racikan<?= $i; ?>[]" id="obat-racik-option-<?=$i?>">
                                                                <option value="">-- Pilih Obat --</option>
                                                                <?php foreach ($obat1 as $key => $value) { ?>
                                                                    <option value="<?= $value->id ?>"><?= $value->nama ?></option>
                                                                <?php } ?>
                                                            </select>
                                                        </div>

                                                        <div class="col-sm-4">
                                                            <button type="button" name="button"
                                                                    class="btn btn-sm btn-block btn-primary"
                                                                    id="button-add-obat-racik-<?=$i?>"><i class="fa fa-plus"></i>
                                                                Tambah obat
                                                            </button>
                                                            <input type="hidden" id="abdush-counter-<?=$i?>" value="0">
                                                        </div>

                                                    </div>

                                                    <div class="form-area-obat-racik-<?=$i?>" style="margin-top:15px;">
                                                        <table class="table table-striped table-hover">
                                                            <thead>
                                                            <tr>
                                                                <th>Nama</th>
                                                                <th>Stok Obat</th>
                                                                <th>Jml</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>

                                                            </tbody>
                                                        </table>
                                                        <input type="text"
                                                               class="form-control"
                                                               id="signa-obat-racik-<?= $i; ?>"
                                                               name="signa<?= $i; ?>"
                                                               placeholder="signa">
                                                        <input type="text"
                                                               class="form-control"
                                                               name="catatan<?= $i; ?>"
                                                               placeholder="catatan">
                                                    </div>
                                                </div>
                                            </div>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    <button type="submit" id="form_diagnosisbtn"
                            data-loading-text="Menyimpan..."
                            class="btn btn-info pull-right">Simpan</button>
                </div>
            </form>


        </div>
    </div>
</div>