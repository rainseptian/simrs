<?php $u = $this->session->userdata('logged_in'); ?>

<div class="modal fade" id="tambah_pemeriksaan" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-xl modal-mid" role="document">
        <div class="modal-content modal-media-content">
            <div class="modal-header modal-media-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="box-title">Tambah Pemeriksaan</h4>
            </div>

            <form id="form_diagnosis" method="post" action="<?php echo base_url() ?>RawatInap/tambah/pemeriksaan">
                <?php if (superadmin($u)) : ?>
                    <input type="hidden" name="creator_type" value="superadmin">
                <?php elseif (perawat($u)) : ?>
                    <input type="hidden" name="creator_type" value="perawat">
                <?php elseif (dokter($u)) : ?>
                    <input type="hidden" name="creator_type" value="dokter">
                <?php elseif (apoteker($u)) : ?>
                    <input type="hidden" name="creator_type" value="apoteker">
                <?php elseif (gizi($u)) : ?>
                    <input type="hidden" name="creator_type" value="gizi">
                <?php endif; ?>
                <div class="modal-body pt0 pb0">
                    <div class="ptt10">
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="row">
                                    <div class="col-md-12">
                                        <input type="hidden" name="rawat_inap_id" value="<?=$rawat_inap->id?>">
                                        <input type="hidden" name="pasien_id" value="<?=$pasien->id?>">
                                        <div class="form-group">
                                            <label for="tanggal_pemeriksaan" class="control-label">Tgl Pemeriksaan</label>
                                            <input type="date" class="form-control margin-bottom" id="tanggal_pemeriksaan" name="tanggal_pemeriksaan" value="<?=date('Y-m-d')?>" required>
                                        </div>
                                        <div class="form-group">
                                            <label for="shift" class="control-label">Shift</label>
                                            <div class="margin-bottom">
                                                <select class="form-control abdush-select margin-bottom" name="shift">
                                                    <option value="">-- Pilih Shift --</option>
                                                    <option value="I">I</option>
                                                    <option value="II">II</option>
                                                    <option value="III">III</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="tujuan" class="control-label">Dokter</label>
                                            <div class="margin-bottom">
                                                <select class="form-control abdush-select margin-bottom" name="dokter_id">
                                                    <option value="" selected>-- Pilih Dokter --</option>
                                                    <?php foreach ($dokter->result() as $key => $value) { ?>
                                                        <option value="<?php echo $value->id ?>"><?php echo $value->nama; ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="perawat" class="control-label">Perawat</label>
                                            <div class="" style="margin-bottom: 10px;">
                                                <select class="form-control select2" multiple="multiple" data-placeholder="Perawat" id="perawat" name="perawat[]">
                                                    <?php foreach ($listPerawat->result() as $key => $value) { ?>
                                                        <option value="<?php echo $value->id ?>"><?php echo ucwords($value->nama) ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="perawat" class="control-label">Perawat Tamu</label>
                                            <div class="">
                                                <select class="form-control select2" multiple="multiple" data-placeholder="Perawat Tamu" id="perawat_tamu" name="perawat_tamu[]">
                                                    <?php foreach ($listPerawat->result() as $key => $value) { ?>
                                                        <option value="<?php echo $value->id ?>"><?php echo ucwords($value->nama) ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="tujuan" class="control-label">Apoteker</label>
                                            <div class="margin-bottom">
                                                <select class="form-control abdush-select margin-bottom" name="apoteker_id">
                                                    <option value="" selected>-- Pilih Apoteker --</option>
                                                    <?php foreach ($apoteker->result() as $key => $value) { ?>
                                                        <option value="<?php echo $value->id ?>"><?php echo $value->nama; ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="tujuan" class="control-label">Petugas Gizi</label>
                                            <div class="margin-bottom">
                                                <select class="form-control abdush-select margin-bottom" name="gizi_id">
                                                    <option value="" selected>-- Pilih Petugas Gizi --</option>
                                                    <?php foreach ($gizi->result() as $key => $value) { ?>
                                                        <option value="<?php echo $value->id ?>"><?php echo $value->nama; ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="subjektif" class="control-label">Deteksi Vital</label>
                                            <div class="row">
                                                <div class="col-sm-6 col-md-6 col-lg-6 form-group">
                                                    <label for="td" class="control-label">TD</label>
                                                    <div class="input-group">
                                                        <input type="text" class="form-control requirable" name="objektif[td]" id="td">
                                                        <span class="input-group-addon">mmHg</span>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6 col-md-6 col-lg-6 form-group">
                                                    <label for="inputEmail3" class="control-label">R</label>
                                                    <div class="input-group">
                                                        <input type="text" class="form-control requirable" name="objektif[r]" id="r">
                                                        <span class="input-group-addon">K/Min</span>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6 col-md-6 col-lg-6 form-group">
                                                    <label for="inputEmail3" class="control-label">BB</label>
                                                    <div class="input-group">
                                                        <input type="text" class="form-control requirable" id="bb" name="objektif[bb]"
                                                               onkeyup="set_bmi()">
                                                        <span class="input-group-addon">Kg</span>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6 col-md-6 col-lg-6 form-group">
                                                    <label for="inputEmail3" class="control-label">N</label>
                                                    <div class="input-group">
                                                        <input type="text" class="form-control requirable" name="n" id="objektif[n]">
                                                        <span class="input-group-addon">K/Min</span>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6 col-md-6 col-lg-6 form-group">
                                                    <label for="inputEmail3" class="control-label">S</label>
                                                    <div class="input-group">
                                                        <input type="text" class="form-control requirable" id="s" name="objektif[s]">
                                                        <span class="input-group-addon">'0</span>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6 col-md-6 col-lg-6 form-group">
                                                    <label for="inputEmail3" class="control-label">TB</label>
                                                    <div class="input-group">
                                                        <input type="text" class="form-control requirable" id="tb" name="objektif[tb]"
                                                               onkeyup="set_bmi()">
                                                        <span class="input-group-addon">cm</span>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6 col-md-6 col-lg-6 form-group">
                                                    <label for="inputEmail3" class="control-label">BMI</label>
                                                    <div class="input-group">
                                                        <input type="text" class="form-control requirable" name="objektif[bmi]" id="bmi">

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="row">
                                    <div class="col-md-12">
                                        <label for="subjektif" class="control-label" style="margin-top: 10px">Subjektif</label>
                                        <div class="form-group">
                                            <label for="subjektif" class="control-label">Perawat</label>
                                            <textarea class="form-control margin-bottom" id="subjektif" name="subjektif" <?=superadmin($u) || perawat($u) ? '' : 'disabled'?>></textarea>
                                        </div>
                                        <div class="form-group">
                                            <label for="subjektif" class="control-label">Dokter</label>
                                            <textarea class="form-control margin-bottom" id="subjektif_d" name="subjektif_d" <?=superadmin($u) || dokter($u) ? '' : 'disabled'?>></textarea>
                                        </div>
                                        <div class="form-group">
                                            <label for="subjektif" class="control-label">Apoteker</label>
                                            <textarea class="form-control margin-bottom" id="subjektif_a" name="subjektif_a" <?=superadmin($u) || apoteker($u) ? '' : 'disabled'?>></textarea>
                                        </div>
                                        <div class="form-group">
                                            <label for="subjektif" class="control-label">Gizi</label>
                                            <textarea class="form-control margin-bottom" id="subjektif_g" name="subjektif_g" <?=superadmin($u) || gizi($u) ? '' : 'disabled'?>></textarea>
                                        </div>
                                        <label for="objektif" class="control-label" style="margin-top: 10px">Objektif</label>
                                        <div class="form-group">
                                            <label for="objektif" class="control-label">Perawat</label>
                                            <textarea class="form-control margin-bottom" id="objektif_p" name="objektif_p" <?=superadmin($u) || perawat($u) ? '' : 'disabled'?>></textarea>
                                        </div>
                                        <div class="form-group">
                                            <label for="objektif" class="control-label">Dokter</label>
                                            <textarea class="form-control margin-bottom" id="objektif_d" name="objektif_d" <?=superadmin($u) || dokter($u) ? '' : 'disabled'?>></textarea>
                                        </div>
                                        <div class="form-group">
                                            <label for="objektif" class="control-label">Apoteker</label>
                                            <textarea class="form-control margin-bottom" id="objektif_a" name="objektif_a" <?=superadmin($u) || apoteker($u) ? '' : 'disabled'?>></textarea>
                                        </div>
                                        <div class="">
                                            <label for="objektif" class="control-label">Gizi</label>
                                            <textarea class="form-control margin-bottom" id="objektif_g" name="objektif_g" <?=superadmin($u) || gizi($u) ? '' : 'disabled'?>></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <label for="asesmen" class="control-label" style="margin-top: 10px">Asesmen</label>
                                <div class="form-group">
                                    <label for="asesmen" class="control-label">Perawat</label>
                                    <textarea class="form-control margin-bottom" id="asesmen" name="asesmen" <?=superadmin($u) || perawat($u) ? '' : 'disabled'?>></textarea>
                                </div>
                                <div class="form-group">
                                    <label for="asesmen" class="control-label">Dokter</label>
                                    <textarea class="form-control margin-bottom" id="asesmen_d" name="asesmen_d" <?=superadmin($u) || dokter($u) ? '' : 'disabled'?>></textarea>
                                </div>
                                <div class="form-group">
                                    <label for="asesmen" class="control-label">Apoteker</label>
                                    <textarea class="form-control margin-bottom" id="asesmen_a" name="asesmen_a" <?=superadmin($u) || apoteker($u) ? '' : 'disabled'?>></textarea>
                                </div>
                                <div class="form-group">
                                    <label for="asesmen" class="control-label">Gizi</label>
                                    <textarea class="form-control margin-bottom" id="asesmen_g" name="asesmen_g" <?=superadmin($u) || gizi($u) ? '' : 'disabled'?>></textarea>
                                </div>
                                <label for="asesmen" class="control-label" style="margin-top: 10px">Plan</label>
                                <div class="form-group">
                                    <label for="plan" class="control-label">Perawat</label>
                                    <textarea class="form-control margin-bottom" id="plan" name="plan" <?=superadmin($u) || perawat($u) ? '' : 'disabled'?>></textarea>
                                </div>
                                <div class="form-group">
                                    <label for="plan" class="control-label">Dokter</label>
                                    <textarea class="form-control margin-bottom" id="plan_d" name="plan_d" <?=superadmin($u) || dokter($u) ? '' : 'disabled'?>></textarea>
                                </div>
                                <div class="form-group">
                                    <label for="plan" class="control-label">Apoteker</label>
                                    <textarea class="form-control margin-bottom" id="plan_a" name="plan_a" <?=superadmin($u) || apoteker($u) ? '' : 'disabled'?>></textarea>
                                </div>
                                <div class="form-group">
                                    <label for="plan" class="control-label">Gizi</label>
                                    <textarea class="form-control margin-bottom" id="plan_g" name="plan_g" <?=superadmin($u) || gizi($u) ? '' : 'disabled'?>></textarea>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="tujuan" class="control-label">Diagnosis Primer (ICD 10)</label>
                                            <div style="display: flex">
                                                <select class="form-control select2 margin-bottom" multiple="multiple" name="penyakit[]" data-placeholder="Pilih penyakit" style="flex: 1">
                                                    <?php foreach ($penyakit as $key => $value) { ?>
                                                        <option value="<?php echo $value->id ?>"><?php echo $value->kode.' - '.$value->nama ?></option>
                                                    <?php } ?>
                                                </select>
                                                <button class="btn btn-sm btn-success" style="margin-left: 10px" onclick="x()" data-toggle="modal">
                                                    <i class="fa fa-plus"></i>
                                                </button>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="diagnosis" class="control-label">Diagnosis Sekunder</label>
                                            <input type="text" class="form-control" id="diagnosis" name="diagnosis" placeholder="Diagnosis">
                                        </div>
                                        <div class="form-group">
                                            <label for="tindakan[]" class="control-label">Tindakan (Khusus DPJP, Perawat, Apoteker, Gizi)</label>
                                            <div class="" style="margin-bottom: 10px;">
                                                <select class="form-control select2" multiple="multiple"
                                                        data-placeholder="Tindakan"
                                                        id="tindakan" name="tindakan[]">
                                                    <?php foreach ($tindakan as $key => $value) { ?>
                                                        <option value="<?php echo $value->id ?>"><?php echo $value->nama . " - Rp." . harga($value->tarif_pasien) ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-12" style="margin-bottom: 10px; height: 3px !important;">
                                                <hr style="margin: 0 !important;">
                                            </div>
                                        </div>
                                        <!-- <div class="form-group">
                                            <label for="perawat" class="control-label">DPJP</label>
                                            <div class="" style="margin-bottom: 10px;">
                                                <select class="form-control select2"
                                                        data-placeholder="Pilih Tindakan DPJP"
                                                        id="dokter_anestesi" name="dokter_anestesi">
                                                    <option value="">--Pilih DPJP--</option>
                                                    <?//php //foreach ($dokter->result() as $key => $value) { ?>
                                                        <option value="<?//php //echo $value->id ?>"><?//php echo ucwords($value->nama) ?></option>
                                                    <?//php //} ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="tindakan_anestesi[]" class="control-label">Tindakan DPJP</label>
                                            <div class="" style="margin-bottom: 10px;">
                                                <select class="form-control select2" multiple="multiple"
                                                        data-placeholder="Tindakan"
                                                        id="tindakan_anestesi" name="tindakan_anestesi[]">
                                                    <?php foreach ($tindakan as $key => $value) { ?>
                                                        <option value="<?php echo $value->id ?>"><?php echo $value->nama . " - Rp." . harga($value->tarif_pasien) ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div> -->
                                        <div class="form-group">
                                            <label for="perawat" class="control-label">Dokter Pengganti</label>
                                            <div class="" style="margin-bottom: 10px;">
                                                <select class="form-control select2" multiple="multiple"
                                                        data-placeholder="Dokter Tamu"
                                                        id="dokter_tamu" name="dokter_tamu[]">
                                                    <?php foreach ($dokter->result() as $key => $value) { ?>
                                                        <option value="<?php echo $value->id ?>"><?php echo ucwords($value->nama) ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="tindakan_tamu[]" class="control-label">Tindakan Dokter Pengganti</label>
                                            <div class="" style="margin-bottom: 10px;">
                                                <select class="form-control select2" multiple="multiple"
                                                        data-placeholder="Tindakan"
                                                        id="tindakan_tamu" name="tindakan_tamu[]">
                                                    <?php foreach ($tindakan as $key => $value) { ?>
                                                        <option value="<?php echo $value->id ?>"><?php echo $value->nama . " - Rp." . harga($value->tarif_pasien) ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="text-muted well well-sm no-shadow" style="display: block;">
                                            <div class="form-group" style=" margin-top: 4px">
                                                <label for="hasil_penunjang_laboratorium" class="col-sm-4 control-label">
                                                    KONSULTASI DOKTER
                                                </label>
                                                <button type="button" class="btn btn-success btn-sm pull-right" id="btn-add-konsul" style="margin-right: 14px">
                                                    <span class="fa fa-plus"></span> Tambah Konsul
                                                </button>
                                            </div>
                                            <div class="row" style="margin-bottom: 14px">
                                                <div class="col-md-12">
                                                    <input type="hidden" name="max_konsul_pemeriksaan_test" id="max_konsul_pemeriksaan_test" value="0">
                                                    <table class="tbl-konsul table table-condensed table-bordered" style="margin-bottom: 8px; table-layout:fixed;">
                                                        <tbody>
                                                        <tr>
                                                            <th style="width: 50%;">Tarif Tindakan</th>
                                                            <th>Dokter</th>
                                                            <th style="width: 46px"></th>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            <textarea class="form-control margin-bottom" id="catatan_konsultasi" name="catatan_konsultasi" <?=superadmin($u) || perawat($u) ? '' : 'disabled'?>></textarea>
                                        </div>
                                        <?php if (!(gizi($u) || perawat($u))) : ?>
                                            <div class="form-group">
                                                <label for="tujuan" class="control-label">Resep</label>
                                                <div class="" style="display: flex">
                                                    <button type="submit"
                                                            name="resep" value="1"
                                                            style="margin-right: 8px"
                                                            data-loading-text="Menyimpan..."
                                                            class="btn btn-success pull-right">Input Resep</button>
                                                </div>
                                            </div>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    <button type="submit" id="form_diagnosisbtn"
                            data-loading-text="Menyimpan..."
                            class="btn btn-info pull-right">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script>
    function x() {
        $('#tambah_pemeriksaan').modal('hide')
        holdModal('tambah_penyakit')
    }
</script>

<script>

    let konsul_num = 0;
    function delete_konsul(num) {
        konsul_num--;
        $('#max_konsul_pemeriksaan_test').val(konsul_num);
        $(`#row-konsul-${num}`).remove();
    }
    $(function () {
        $('#btn-add-konsul').click(function () {
            konsul_num++;
            console.log(konsul_num);
            $('#max_konsul_pemeriksaan_test').val(konsul_num);
            $('.tbl-konsul tr:last').after(`
                <tr id="row-konsul-${konsul_num}">
                    <td style="width: 50%;">
                        <select class="form-control select2" multiple="multiple" name="tindakan_${konsul_num}[]" id="tindakan_${konsul_num}"
                                data-placeholder="Pilih tindakan"
                                style="width: 100%;" required>
                                <?php foreach ($tindakan as $key => $value) { ?>
                                    <option value="<?= $value->id; ?>"><?= $value->nama . " - Rp." . number_format($value->tarif_pasien, 2, ',', '.') ?></option>
                                <?php } ?>
                        </select>
                    </td>
                    <td>
                        <select class="form-control abdush-select" name="dokter_${konsul_num}" style="width: 100%;" required>
                            <option value="" selected>-- Pilih Dokter --</option>
                            <?php foreach ($dokter->result() as $key => $value) { ?>
                                <option value="<?php echo $value->id ?>"><?php echo $value->nama; ?></option>
                            <?php } ?>
                        </select>
                    </td>
                    <td style="width: 46px">
                        <button type="button" class="btn btn-sm btn-danger" onclick="delete_konsul(${konsul_num})">
                            <span class="fa fa-trash"></span>
                        </button>
                    </td>
                </td>
            `);
            $(`#tindakan_${konsul_num}`).select2();
        })

        //const tindakan_konsul = <?php //=json_encode($tindakan_konsul);?>//;
        //const all_tindakan = <?php //=json_encode($tindakan->result());?>//;
        //const all_dokter = <?php //=json_encode($dokter->result());?>//;
        //tindakan_konsul.forEach((v, k) => {
        //    konsul_num++;
        //    $('#max_konsul_pemeriksaan_test').val(konsul_num);
        //    const tindakan_opts = all_tindakan.map(x => `<option value="${x.id}" ${+x.id === +v.tarif_tindakan_id ? 'selected' : ''}>${x.nama} - ${formatMoney(x.tarif_pasien)}</option>`)
        //    const dokter_opts = all_dokter.map(x => `<option value="${x.id}" ${+x.id === +v.dokter_id ? 'selected' : ''}>${x.nama}</option>`)
        //    $('#tbl-konsul tr:last').after(`
        //        <tr id="row-konsul-${konsul_num}">
        //            <td style="width: 50%;">
        //                <div class="form-group">
        //                    <div class="col-sm-12">
        //                        <select class="form-control select2" multiple="multiple" name="tindakan_${konsul_num}[]" id="tindakan_${konsul_num}"
        //                                data-placeholder="Pilih tindakan"
        //                                style="width: 100%;" required>
        //                                ${tindakan_opts.join('')}
        //                        </select>
        //                    </div>
        //                </div>
        //            </td>
        //            <td>
        //                <div class="form-group">
        //                    <div class="col-sm-12">
        //                        <select class="form-control abdush-select" name="dokter_${konsul_num}" style="width: 100%;" required>
        //                            ${dokter_opts.join('')}
        //                        </select>
        //                    </div>
        //                </div>
        //            </td>
        //            <td style="width: 46px">
        //                <button type="button" class="btn btn-sm btn-danger" onclick="delete_konsul(${konsul_num})">
        //                    <span class="fa fa-trash"></span>
        //                </button>
        //            </td>
        //        </td>
        //    `);
        //    $(`#tindakan_${konsul_num}`).select2();
        //})
    })
</script>