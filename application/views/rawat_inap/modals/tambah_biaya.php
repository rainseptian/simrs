<div class="modal fade" id="tambah_biaya" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-mid" role="document">
        <div class="modal-content modal-media-content">
            <div class="modal-header modal-media-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="box-title">Tambah Biaya</h4>
            </div>

            <form method="post" action="<?php echo base_url() ?>RawatInap/tambah/biaya">
                <div class="modal-body pt0 pb0">
                    <div class="ptt10">
                        <div class="row">
                            <div class="col-md-12">
                                <input type="hidden" name="rawat_inap_id" value="<?=$rawat_inap->id?>">
                                <input type="hidden" name="pasien_id" value="<?=$pasien->id?>">
                                <input type="hidden" id="jenis_biaya" name="jenis_biaya" value="#biaya_2">
                                <!--                                <div class="form-group">-->
                                <!--                                    <label for="tangal" class="col-sm-3 control-label">Tanggal</label>-->
                                <!--                                    <div class="form-group">-->
                                <!--                                        <div class='input-group date' id='datetimepicker1'>-->
                                <!--                                            <input type='text' class="form-control" name="tanggal" value="--><?//=date('Y-m-d H:i')?><!--" required/>-->
                                <!--                                            <span class="input-group-addon">-->
                                <!--                                                <span class="glyphicon glyphicon-calendar"></span>-->
                                <!--                                            </span>-->
                                <!--                                        </div>-->
                                <!--                                    </div>-->
                                <!--                                </div>-->

                                <div class="form-group">
                                    <div class="nav-tabs-custom">
                                        <ul class="nav nav-tabs" id="tab_biaya">
                                            <!--                                            <li class="active"><a href="#biaya_1" data-toggle="tab" aria-expanded="true">Biaya Kamar</a></li>-->
                                            <li class="active"><a href="#biaya_2" data-toggle="tab" aria-expanded="false">Makanan</a></li>
                                            <li class=""><a href="#biaya_3" data-toggle="tab" aria-expanded="false">Tindakan</a></li>
                                        </ul>
                                        <div class="tab-content">
                                            <div class="tab-pane" id="biaya_1">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <input type="hidden" name="pasien_id" value="<?=$pasien->id?>">
                                                        <div class="form-group mt-2">
                                                            <label for="bed_group" class="col-sm-3 control-label">Bed Group</label>
                                                            <div class="col-sm-9">
                                                                <input type="text" class="form-control margin-bottom" id="bed_group"
                                                                       value="<?=$rawat_inap->bedgroup?>"
                                                                       name="bed_group" readonly>
                                                            </div>
                                                        </div>
                                                        <div class="mt-2">
                                                            <div class="form-group">
                                                                <label for="bed_type" class="col-sm-3 control-label">Bed Type</label>
                                                                <div class="col-sm-9">
                                                                    <input type="text" class="form-control margin-bottom" id="bed_type"
                                                                           value="<?=$rawat_inap->bed_type_name?>"
                                                                           name="bed_type" readonly>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="mt-2">
                                                            <div class="form-group">
                                                                <label for="bed" class="col-sm-3 control-label">Bed</label>
                                                                <div class="col-sm-9">
                                                                    <input type="text" class="form-control margin-bottom"
                                                                           name="bed" value="<?=$rawat_inap->bed_name?>" readonly>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="mt-2">
                                                            <div class="form-group">
                                                                <label for="harga_bed" class="col-sm-3 control-label">Harga</label>
                                                                <div class="col-sm-9">
                                                                    <input type="text" class="form-control margin-bottom" id="harga_bed"
                                                                           name="harga_bed" value="<?=$rawat_inap->bedgroup_price?>" readonly>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="tab-pane active" id="biaya_2">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <input type="hidden" name="harga_makanan" id="harga_makanan">
                                                        <div class="form-group">
                                                            <label for="tujuan" class="col-sm-3 control-label">Tipe Makanan</label>
                                                            <div class="col-sm-9 margin-bottom">
                                                                <select class="form-control abdush-select margin-bottom" name="tipe_makanan" onchange="getMakananByTipeId(this.value)">
                                                                    <option value="">--Pilih Tipe Makanan--</option>
                                                                    <?php foreach ($tipe_makanan as $key => $value) { ?>
                                                                        <option value="<?php echo $value->id ?>"><?php echo $value->nama ?></option>
                                                                    <?php } ?>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="tujuan" class="col-sm-3 control-label margin-bottom">Nama Makanan</label>
                                                            <div class="col-sm-9 margin-bottom">
                                                                <select class="form-control abdush-select" name="makanan" id="makanan">

                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="tab-pane" id="biaya_3">
<!--                                                <div class="impbtnview-tindakan">-->
<!--                                                    <a href="#" class="btn btn-sm btn-primary dropdown-toggle"-->
<!--                                                       onclick="holdModal('tambah_tindakan')" data-toggle="modal"><i class="fas fa-plus"></i> Tambah Tindakan-->
<!--                                                    </a>-->
<!--                                                </div>-->
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label for="tindakan[]" class="col-sm-3 control-label">Tindakan</label>
                                                            <div class="col-sm-9" style="margin-bottom: 10px;">
                                                                <select class="form-control select2" multiple="multiple"
                                                                        data-placeholder="Tindakan"
                                                                        id="tindakan" name="tindakan[]">
                                                                    <?php foreach ($tindakan as $key => $value) { ?>
                                                                        <option value="<?php echo $value->id ?>"><?php echo $value->nama . " - Rp." . harga($value->tarif_pasien) ?></option>
                                                                    <?php } ?>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <div class="col-sm-12" style="margin-bottom: 10px; height: 3px !important;">
                                                                <hr style="margin: 0 !important;">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="perawat" class="col-sm-3 control-label">Dokter Anestesi</label>
                                                            <div class="col-sm-9" style="margin-bottom: 10px;">
                                                                <select class="form-control select2"
                                                                        data-placeholder="Dokter Anestesi"
                                                                        id="dokter_anestesi" name="dokter_anestesi">
                                                                    <option value="">--Pilih dokter anestesi--</option>
                                                                    <?php foreach ($dokter->result() as $key => $value) { ?>
                                                                        <option value="<?php echo $value->id ?>"><?php echo ucwords($value->nama) ?></option>
                                                                    <?php } ?>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="tindakan_anestesi[]" class="col-sm-3 control-label">Tindakan Dokter Anestesi</label>
                                                            <div class="col-sm-9" style="margin-bottom: 10px;">
                                                                <select class="form-control select2" multiple="multiple"
                                                                        data-placeholder="Tindakan"
                                                                        id="tindakan_anestesi" name="tindakan_anestesi[]">
                                                                    <?php foreach ($tindakan as $key => $value) { ?>
                                                                        <option value="<?php echo $value->id ?>"><?php echo $value->nama . " - Rp." . harga($value->tarif_pasien) ?></option>
                                                                    <?php } ?>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <div class="col-sm-12" style="margin-bottom: 10px; height: 3px !important;">
                                                                <hr style="margin: 0 !important;">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="perawat" class="col-sm-3 control-label">Dokter Tamu</label>
                                                            <div class="col-sm-9" style="margin-bottom: 10px;">
                                                                <select class="form-control select2" multiple="multiple"
                                                                        data-placeholder="Dokter Tamu"
                                                                        id="dokter_tamu" name="dokter_tamu[]">
                                                                    <?php foreach ($dokter->result() as $key => $value) { ?>
                                                                        <option value="<?php echo $value->id ?>"><?php echo ucwords($value->nama) ?></option>
                                                                    <?php } ?>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="tindakan_tamu[]" class="col-sm-3 control-label">Tindakan Dokter Tamu</label>
                                                            <div class="col-sm-9" style="margin-bottom: 10px;">
                                                                <select class="form-control select2" multiple="multiple"
                                                                        data-placeholder="Tindakan"
                                                                        id="tindakan_tamu" name="tindakan_tamu[]">
                                                                    <?php foreach ($tindakan as $key => $value) { ?>
                                                                        <option value="<?php echo $value->id ?>"><?php echo $value->nama . " - Rp." . harga($value->tarif_pasien) ?></option>
                                                                    <?php } ?>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <div class="col-sm-12" style="margin-bottom: 10px; height: 3px !important;">
                                                                <hr style="margin: 0 !important;">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="perawat" class="col-sm-3 control-label">Perawat</label>
                                                            <div class="col-sm-9" style="margin-bottom: 10px;">
                                                                <select class="form-control select2" multiple="multiple"
                                                                        data-placeholder="Perawat"
                                                                        id="perawat" name="perawat[]">
                                                                    <?php foreach ($listPerawat->result() as $key => $value) { ?>
                                                                        <option value="<?php echo $value->id ?>"><?php echo ucwords($value->nama) ?></option>
                                                                    <?php } ?>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="perawat" class="col-sm-3 control-label">Perawat Tamu</label>
                                                            <div class="col-sm-9">
                                                                <select class="form-control select2" multiple="multiple"
                                                                        data-placeholder="Perawat Tamu"
                                                                        id="perawat_tamu" name="perawat_tamu[]">
                                                                    <?php foreach ($listPerawat->result() as $key => $value) { ?>
                                                                        <option value="<?php echo $value->id ?>"><?php echo ucwords($value->nama) ?></option>
                                                                    <?php } ?>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    <button type="submit" id="form_diagnosisbtn"
                            data-loading-text="Menyimpan..."
                            class="btn btn-info pull-right">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>