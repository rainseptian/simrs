<div class="modal fade" id="tambah_tindakan" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-mid" role="document">
        <div class="modal-content modal-media-content">
            <div class="modal-header modal-media-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="box-title">Tambah Master Tindakan Rawat Inap</h4>
            </div>

            <form id="form_add_tindakan" method="post" action="<?php echo base_url() ?>MasterPoli/addTindakanRanap">
                <div class="modal-body pt0 pb0">
                    <div class="row">
                        <div class="col-md-12">
                            <input type="hidden" name="redirect_to" value="RawatInap/detail/<?=$rawat_inap->id?>">
                            <input type="hidden" name="jenis" value="rawat_inap">
                            <div class="form-group">
                                <label for="nama" class="col-sm-4 control-label" >Nama Tindakan</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" id="nama_tindakan" name="nama_tindakan" onkeyup="set_tarifpasien()" placeholder="Masukkan nama tindakan" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="nama" class="col-sm-4 control-label" >JM Perawat</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" id="tarif_perawat" name="tarif_perawat" value="0" onkeyup="set_tarifpasien()" placeholder="Masukkan JM perawat" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="nama" class="col-sm-4 control-label" >JM Dokter</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" id="tarif_dokter" name="tarif_dokter"  value="0" onkeyup="set_tarifpasien()" placeholder="Masukkan jm dokter">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="tarif_apoteker" class="col-sm-4 control-label" >JM Apoteker</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" id="tarif_apoteker" name="tarif_apoteker"  value="0" onkeyup="set_tarifpasien()" placeholder="Masukkan jm apoteker">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="nama" class="col-sm-4 control-label" >Jasa lain-lain</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" id="tarif_lain" name="tarif_lain"  value="0" onkeyup="set_tarifpasien()" placeholder="Masukkan Jasa lain-lain " required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="kategori" class="col-sm-4 control-label">Klinik</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" id="klinik" name="klinik"  value="0" onkeyup="set_tarifpasien()" placeholder="Masukkan tarif untuk klinik" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="tempat_lahir" class="col-sm-4 control-label">Total Tarif Pasien</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" id="tarif_pasien" name="tarif_pasien" placeholder="masukkan total tarif pasien" required>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    <button type="submit" id="form_diagnosisbtn"
                            data-loading-text="Menyimpan..."
                            class="btn btn-info pull-right">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>