<script src="<?= base_url() ?>assets/bower_components/select2/dist/js/select2.full.min.js"></script>

<script type="text/javascript">
    function set_tarifpasien() {
        var tp = parseInt($('#tarif_perawat').val());
        var td = parseInt($('#tarif_dokter').val());
        var ta = parseInt($('#tarif_apoteker').val());
        var tl = parseInt($('#tarif_lain').val());
        var k = parseInt($('#klinik').val());

        var tarifpasien = tp+td+ta+tl+k;
        $('#tarif_pasien').val(tarifpasien);
    }

    function set_bmi() {
        var tb = $('#tb').val();
        var bb = $('#bb').val();
        var tbm = tb / 100;
        var bmi = bb / (tbm * tbm);

        $('#bmi').val(bmi.toFixed(2));
    }

    function getMakananByTipeId(id) {
        $('#makanan').html("<option value='l'>Loading...</option>");
        $.ajax({
            url: '<?php echo base_url(); ?>RawatInap/getMakananByTipeId/'+id,
            type: "POST",
            dataType: 'json',
            success: function (res) {
                let div_data = '';
                $.each(res, function (i, obj) {
                    div_data += "<option value='" + obj.id + "'>" + obj.nama + ' - ' + obj.harga + "</option>";
                });
                $('#makanan').html("<option value=''>--Pilih Makanan--</option>");
                $('#makanan').append(div_data);
                $("#makanan").select2("val", '');
            },
            error: function (e) {
                console.log(e);
            }
        });
    }

    function getMakananByTipeId_(id) {
        $('#makanan_').html("<option value='l'>Loading...</option>");
        $.ajax({
            url: '<?php echo base_url(); ?>RawatInap/getMakananByTipeId/'+id,
            type: "POST",
            dataType: 'json',
            success: function (res) {
                let div_data = '';
                $.each(res, function (i, obj) {
                    div_data += "<option value='" + obj.id + "'>" + obj.nama + ' - ' + obj.harga + "</option>";
                });
                $('#makanan_').html("<option value=''>--Pilih Makanan--</option>");
                $('#makanan_').append(div_data);
                $("#makanan_").select2("val", '');
            },
            error: function (e) {
                console.log(e);
            }
        });
    }

    $(function () {
        $('.select2').select2();

        // $('select:not(.normal)').each(function () {
        //     $(this).select2({
        //         dropdownParent: $(this).parent()
        //     });
        // });

        $('#tab_biaya a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
            var target = $(e.target).attr("href");
            $('#jenis_biaya').val(target);
            $('#makanan').prop('required', target === '#biaya_2');
        });
        $('#makanan').on('change', function () {
            const txt = this.options[this.selectedIndex].text;
            const harga = txt.split(' - ')[1];
            $('#harga_makanan').val(harga);
        });
        $('#makanan_').on('change', function () {
            const txt = this.options[this.selectedIndex].text;
            const harga = txt.split(' - ')[1];
            $('#harga_makanan_').val(harga);
        });

        $('#total_biaya').val(<?=isset($total_tagihan) ? $total_tagihan : 0?>);

        $('#tambah_tindakan')
            .on('show.bs.modal', function () {
                $('#tambah_biaya').modal('toggle');
            })
            .on('hide.bs.modal', function () {
                $('#tambah_biaya').modal('toggle');
            });
        $('#tambah_penyakit')
            .on('show.bs.modal', function () {
                $('#tambah_diagnosis').modal('toggle');
            })
            .on('hide.bs.modal', function () {
                $('#tambah_diagnosis').modal('toggle');
            });
    });

    function holdModal(modalId) {
        const modal = '#' + modalId;
        $(modal).modal({
            backdrop: 'static',
            keyboard: false,
            show: true
        });
    }

    // $('.select2').select2();

    jQuery(function ($) {
        var bahan = <?php echo json_encode($bahan); ?>;
        $('#bahan-options').select2();

        var obat = <?php echo json_encode($obat1); ?>;
        $('#obat-option').select2();

        $('#button-add-bahans').on('click', function (e) {
            var id_bahan = $('#bahan-options').val();
            if (id_bahan == '') {
                alert('Anda belum memilih bahan habis pakai !');
            }
            else {
                var theBahan = {};
                var counter = parseInt($('#abdush-counter2').val());
                $.each(bahan, function (i, v) {
                    if (v.id == id_bahan) {
                        theBahan = v;
                        return;
                    }
                });

                var html = `
                <tr>
                  <td>
                    ` + theBahan.nama + `
                    <input type="hidden" name="id[]" value="` + theBahan.id + `">
                  </td>
                  <td>` + theBahan.jumlah + ` ` + theBahan.satuan + `</td>
                  <td>
                    <input style="width:65px;" type="text" class="form-control" name="qty[]" id="bahan[` + counter + `][qty]">
                  </td>
                  <td>
                    <button class="btn btn-sm btn-danger btn-delete-row" type="button"> <i class="fa fa-trash"></i> </button>
                  </td>
                </tr>`;

                $('.form-area tbody').append(html);
                $('#abdush-counter').val(counter + 1);
                $('input[name="bahan[' + counter + '][qty]"]').focus();

                $('.btn-delete-row').unbind('click');
                $('.btn-delete-row').each(function () {
                    $(this).on('click', function () {
                        $(this).parents('tr').remove();
                    });
                });
            }

            $('#bahan-options').val('').trigger('change');
        });

        $('#button-add-obat').on('click', function (e) {
            var id_Obat = $('#obat-option').val();
            if (id_Obat == '') {
                alert('Anda belum memilih obat !');
            }
            else {
                var theObat = {};
                var counter = parseInt($('#abdush-counter2').val());
                $.each(obat, function (i, v) {
                    if (v.id == id_Obat) {
                        theObat = v;
                        return;
                    }
                });

                var html = `
                <tr>
                  <td>
                    ` + theObat.nama + `
                    <input type="hidden" name="nama_obat[]" value="` + theObat.id + `">
                  </td>
                  <td>` + theObat.stok_obat + ` item </td>
                  <input type="hidden" id="stok` + counter + `" value="` + theObat.stok_obat + `">
                  <td>
                    <input style="width:65px;" type="text" class="form-control" onchange="loadData(` + counter + `);" name="jumlah_satuan[]" id="jumlah_satuan` + counter + `">
                  </td>
                  <td>
                    <input style="width:100px;" type="text" class="form-control"  name="signa_obat[]" id="signa_obat` + counter + `">
                  </td>
                  <td>
                    <button class="btn btn-sm btn-danger btn-delete-row" type="button"> <i class="fa fa-trash"></i> </button>
                  </td>
                </tr>

                `;

                $('.form-area-obat tbody').append(html);
                $('#abdush-counter2').val(counter + 1);
                $('input[id="jumlah_satuan' + counter + '"]').focus();

                $('.btn-delete-row').unbind('click');
                $('.btn-delete-row').each(function () {
                    $(this).on('click', function () {
                        $(this).parents('tr').remove();
                    });
                });
            }

            $('#obat-option').val('').trigger('change');
        });

        let initializer = {
            init: function() {
                for (let i = 1; i < 9; i++) {
                    $(`#obat-racik-option-${i}`).select2();
                    $(`#button-add-obat-racik-${i}`).on('click', e => {
                        this.onBtnAddClick(i);
                    });
                }
            },
            checkHasObat: function(i) {
                let rowCount = $(`.form-area-obat-racik-${i} >table >tbody >tr`).length;
                if (rowCount === 0) {
                    $(`#signa-obat-racik-${i}`).prop('required',false);
                }
            },
            onBtnAddClick: function(i) {
                let that = this;
                let opt = $(`#obat-racik-option-${i}`);
                let id_Obat = opt.val();
                if (id_Obat === '') {
                    alert('Anda belum memilih obat !');
                }
                else {
                    let theObat = {};
                    let counter = parseInt($(`#abdush-counter-${i}`).val());
                    $.each(obat, function (i, v) {
                        if (v.id === id_Obat) {
                            theObat = v;
                            return;
                        }
                    });

                    $(`.form-area-obat-racik-${i} tbody`).append(this.getTableRow(i, theObat, counter));
                    $(`#signa-obat-racik-${i}`).prop('required',true);
                    $(`#abdush-counter-${i}`).val(counter + 1);
                    $('input[id="jumlah_satuan_racik' + counter + '"]').focus();

                    $('.btn-delete-row').unbind('click');
                    $('.btn-delete-row').each(function () {
                        $(this).on('click', function () {
                            $(this).parents('tr').remove();
                            that.checkHasObat(i);
                        });
                    });
                }
                opt.val('').trigger('change');
            },
            getTableRow: (i, obat, counter) => (`
                <tr>
                  <td>
                    ` + obat.nama + `
                    <input type="hidden" name="nama_obat_racikan${i}[]" value="` + obat.id + `">
                  </td>
                  <td>` + obat.stok_obat + ` item </td>
                  <input type="hidden" id="stok` + counter + `" value="` + obat.stok_obat + `">
                  <td>
                    <input style="width:65px;" type="text" class="form-control" onchange="loadData(` + counter + `);" name="jumlah_satuan_racikan${i}[]" id="jumlah_satuan_racikan${i}` + counter + `">
                  </td>
                  <td>
                    <button class="btn btn-sm btn-danger btn-delete-row" type="button"> <i class="fa fa-trash"></i> </button>
                  </td>
                </tr>
            `)
        };

        initializer.init();
    });
</script>
