<div class="tab-pane <?=isset($tab) && $tab == 'alergi' ? 'active' : ''?>" id="alergi">
    <div class="impbtnview">
        <a href="<?=base_url("RawatInap/print_form/alergi/$rawat_inap->id")?>" target="_blank" class="btn btn-primary btn-sm">
            <i class="fa fa-print"></i> Cetak
        </a>
    </div>
    <div class="row">
        <div class="col-md-12">
            <form method="post" action="<?php echo base_url() ?>RawatInap/save_form/alergi_header/alergi">
                <input type="hidden" name="rawat_inap_id" value="<?=$rawat_inap->id?>">
                <table class="table table-bordered" style="table-layout: fixed">
                    <tbody>
                    <tr>
                        <th colspan="8" style="text-align: center; vertical-align: middle">REKONSILIASI OBAT</th>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <label>TANGGAL WAWANCARA</label>
                            <input type="date" class="form-control" name="form[alergi_header][tgl]" value="<?=$form['alergi_header']['tgl']?>">
                        </td>
                        <td colspan="4">
                            <label>JAM WAWANCARA</label>
                            <input type="time" class="form-control" name="form[alergi_header][jam]" value="<?=$form['alergi_header']['jam']?>">
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <label>ALERGI OBAT</label>
                            <input type="text" placeholder="1)" class="form-control form-control-sm" name="form[alergi_header][alergi_1]" value="<?=$form['alergi_header']['alergi_1']?>">
                            <input type="text" placeholder="2)" class="form-control form-control-sm" name="form[alergi_header][alergi_2]" value="<?=$form['alergi_header']['alergi_2']?>">
                            <input type="text" placeholder="3)" class="form-control form-control-sm" name="form[alergi_header][alergi_3]" value="<?=$form['alergi_header']['alergi_3']?>">
                        </td>
                        <td colspan="2">
                            <label>MANIFESTASI ALERGI</label>
                            <textarea class="form-control" name="form[alergi_header][manifestasi]" rows="4"><?=$form['alergi_header']['manifestasi']?></textarea>
                        </td>
                        <td colspan="3">
                            <div style="display: flex">
                                <div style="flex: 1">
                                    <label>DAMPAK</label>
                                    <br>
                                    <input type="checkbox" name="form[alergi_header][dampak]" value="Ringan" <?=$form['alergi_header']['dampak'] == 'Ringan' ? 'checked' : ''?>>
                                    <label>Ringan</label>
                                    <br>
                                    <input type="checkbox" name="form[alergi_header][dampak]" value="Sedang" <?=$form['alergi_header']['dampak'] == 'Sedang' ? 'checked' : ''?>>
                                    <label>Sedang</label>
                                    <br>
                                    <input type="checkbox" name="form[alergi_header][dampak]" value="Berat" <?=$form['alergi_header']['dampak'] == 'Berat' ? 'checked' : ''?>>
                                    <label>Berat</label>
                                </div>
                                <div style="display: flex; flex-direction: column-reverse">
                                    <button type="submit" class="btn btn-success btn-sm">Simpan</button>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th class="text-center" style="vertical-align: middle" colspan="2">NAMA OBAT</th>
                        <th class="text-center" style="vertical-align: middle" rowspan="2">DOSIS</th>
                        <th class="text-center" style="vertical-align: middle" rowspan="2">ATURAN PAKAI</th>
                        <th class="text-center" style="vertical-align: middle" rowspan="2">RUTE PEMBERIAN</th>
                        <th class="text-center" style="vertical-align: middle" rowspan="2">JUMLAH OBAT</th>
                        <th class="text-center" style="vertical-align: middle" colspan="2">OBAT YANG DIGUNAKAN SAAT RAWAT</th>
                    </tr>
                    <tr>
                        <th class="text-center" style="vertical-align: middle">NAMA GENERIK</th>
                        <th class="text-center" style="vertical-align: middle">NAMA DAGANG</th>
                        <th class="text-center" style="vertical-align: middle">YA</th>
                        <th class="text-center" style="vertical-align: middle">TIDAK</th>
                    </tr>
                    <?php foreach ($form['alergi'] as $v) : ?>
                    <tr>
                        <td><?=$v['nm_generik']?></td>
                        <td><?=$v['nm_dagang']?></td>
                        <td><?=$v['dosis']?></td>
                        <td><?=$v['aturan']?></td>
                        <td><?=$v['rute']?></td>
                        <td><?=$v['jumlah']?></td>
                        <td class="text-center" style="vertical-align: middle"><?=$v['saat_rawat'] == 'y' ? '✔' : ''?></td>
                        <td class="text-center" style="vertical-align: middle"><?=$v['saat_rawat'] == 'n' ? '✔' : ''?></td>
                    </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            </form>
            <div class="text-center">
                <?php if (superadmin($this->session->userdata('logged_in')) || perawat($this->session->userdata('logged_in')) || dokter($this->session->userdata('logged_in')) || apoteker($this->session->userdata('logged_in'))) : ?>
                <a href="#" class="btn btn-primary dropdown-toggle addcharges"
                   onclick="holdModal('tambah_alergi')" data-toggle='modal'>Tambah Rekonsiliasi
                </a>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>