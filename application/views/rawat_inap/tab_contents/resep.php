<div class="tab-pane <?=$tab == 'resep' ? 'active' : ''?>" id="resep">
    <div class="impbtnview">
        <!--                                    <a href="#" class="btn btn-sm btn-primary dropdown-toggle addprescription"-->
        <!--                                       onclick="holdModal('tambah_resep')" data-toggle="modal"><i-->
        <!--                                                class="fas fa-plus"></i> Tambah Resep-->
        <!--                                    </a>-->
    </div><!--./impbtnview-->
    <div class="table-responsive">
        <table class="table table-bordered example">
            <thead>
            <tr>
                <th>No</th>
                <th>Tgl Resep</th>
                <th>Obat Satuan</th>
                <th>Obat Racik</th>
            </tr>
            </thead>
            <tbody>
            <?php
            $no = 1;
            foreach ($resep as $r) : ?>
                <tr class="row-resep">
                    <td><?=$no++?></td>
                    <td><?=$r->created_at?></td>
                    <td style="font-size: 12px">
                        <table class="table table-condensed">
                            <thead>
                            <tr>
                                <th>Nama Obat</th>
                                <th>Jumlah</th>
                                <th>Signa</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($r->obat as $obat) : ?>
                                <tr>
                                    <td><?=$obat->nama?></td>
                                    <td><?=$obat->jumlah?></td>
                                    <td><?=$obat->signa?></td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                    </td>
                    <td style="font-size: 12px">
                        <table class="table table-condensed">
                            <thead>
                            <tr>
                                <th>Racikan</th>
                                <th>Signa</th>
                                <th>Catatan</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($r->obat_racik as $racik) : ?>
                                <tr>
                                    <td>
                                        <table class="table table-condensed">
                                            <thead>
                                            <tr>
                                                <th>Nama Obat</th>
                                                <th>Jumlah</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php foreach ($racik->detail as $detail) : ?>
                                                <tr>
                                                    <td><?=$detail->nama?></td>
                                                    <td><?=$detail->jumlah?></td>
                                                </tr>
                                            <?php endforeach; ?>
                                            </tbody>
                                        </table>
                                    </td>
                                    <td><?=$racik->signa?></td>
                                    <td><?=$racik->catatan?></td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                    </td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>