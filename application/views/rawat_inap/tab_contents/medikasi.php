<div class="tab-pane <?=isset($tab) && $tab == 'medikasi' ? 'active' : ''?>" id="medikasi">
    <div class="impbtnview">
        <a href="<?=base_url("RawatInap/print_form/medikasi/$rawat_inap->id")?>" target="_blank" class="btn btn-primary btn-sm">
            <i class="fa fa-print"></i> Cetak
        </a>
    </div>
    <div class="row">
        <div class="col-md-12">
            <form method="post" action="<?php echo base_url() ?>RawatInap/save_form/medikasi/medikasi">
                <input type="hidden" name="rawat_inap_id" value="<?=$rawat_inap->id?>">
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <tbody>
                        <tr>
                            <th colspan="24" style="text-align: center; vertical-align: middle">LEMBAR MEDIKASI</th>
                        </tr>
                        <tr>
                            <th class="text-center" style="vertical-align: middle" rowspan="3">Hari/Tgl</th>
                            <th class="text-center" style="vertical-align: middle" rowspan="3">Nama Obat</th>
                            <th class="text-center" style="vertical-align: middle" colspan="21">PEMBERIAN OBAT</th>
                            <th class="text-center" style="vertical-align: middle" rowspan="3">KET</th>
                        </tr>
                        <tr>
                            <th class="text-center" style="vertical-align: middle" colspan="3">TGL: <input type="date" name="form[medikasi][tgl_1]" class="form-control"></th>
                            <th class="text-center" style="vertical-align: middle" colspan="3">TGL: <input type="date" name="form[medikasi][tgl_2]" class="form-control"></th>
                            <th class="text-center" style="vertical-align: middle" colspan="3">TGL: <input type="date" name="form[medikasi][tgl_3]" class="form-control"></th>
                            <th class="text-center" style="vertical-align: middle" colspan="3">TGL: <input type="date" name="form[medikasi][tgl_4]" class="form-control"></th>
                            <th class="text-center" style="vertical-align: middle" colspan="3">TGL: <input type="date" name="form[medikasi][tgl_5]" class="form-control"></th>
                            <th class="text-center" style="vertical-align: middle" colspan="3">TGL: <input type="date" name="form[medikasi][tgl_6]" class="form-control"></th>
                            <th class="text-center" style="vertical-align: middle" colspan="3">TGL: <input type="date" name="form[medikasi][tgl_7]" class="form-control"></th>
                        </tr>
                        <tr>
                            <th class="text-center" style="vertical-align: middle">Jam</th>
                            <th class="text-center" style="vertical-align: middle">Pr<br>wt</th>
                            <th class="text-center" style="vertical-align: middle">Klg/px</th>
                            <th class="text-center" style="vertical-align: middle">Jam</th>
                            <th class="text-center" style="vertical-align: middle">Pr<br>wt</th>
                            <th class="text-center" style="vertical-align: middle">Klg/px</th>
                            <th class="text-center" style="vertical-align: middle">Jam</th>
                            <th class="text-center" style="vertical-align: middle">Pr<br>wt</th>
                            <th class="text-center" style="vertical-align: middle">Klg/px</th>
                            <th class="text-center" style="vertical-align: middle">Jam</th>
                            <th class="text-center" style="vertical-align: middle">Pr<br>wt</th>
                            <th class="text-center" style="vertical-align: middle">Klg/px</th>
                            <th class="text-center" style="vertical-align: middle">Jam</th>
                            <th class="text-center" style="vertical-align: middle">Pr<br>wt</th>
                            <th class="text-center" style="vertical-align: middle">Klg/px</th>
                            <th class="text-center" style="vertical-align: middle">Jam</th>
                            <th class="text-center" style="vertical-align: middle">Pr<br>wt</th>
                            <th class="text-center" style="vertical-align: middle">Klg/px</th>
                            <th class="text-center" style="vertical-align: middle">Jam</th>
                            <th class="text-center" style="vertical-align: middle">Pr<br>wt</th>
                            <th class="text-center" style="vertical-align: middle">Klg/px</th>
                        </tr>
                        <?php foreach ([1,2,3,4,5,6,7] as $v) : ?>
                            <tr>
                                <td rowspan="3">
                                    <input type="date" name="form[medikasi][<?=$v?>][tgl]" value="<?=$form['medikasi'][$v]['tgl']?>" class="form-control">
                                </td>
                                <td rowspan="3">
                                    <input type="text" name="form[medikasi][<?=$v?>][nama_obat]" value="<?=$form['medikasi'][$v]['nama_obat']?>" class="form-control">
                                </td>
                                <?php foreach ([1,2,3,4,5,6,7] as $vv) : ?>
                                    <td>
                                        <input type="text" name="form[medikasi][<?=$v?>][1][<?=$vv?>][jam]" class="form-control">
                                    </td>
                                    <td>
                                        <input type="text" name="form[medikasi][<?=$v?>][2][<?=$vv?>][prwt]" class="form-control">
                                    </td>
                                    <td>
                                        <input type="text" name="form[medikasi][<?=$v?>][3][<?=$vv?>][klg]" class="form-control">
                                    </td>
                                <?php endforeach; ?>
                                <td rowspan="3">
                                    <input type="text" name="form[medikasi][<?=$v?>][nama_obat]" value="<?=$form['medikasi'][$v]['nama_obat']?>" class="form-control">
                                </td>
                            </tr>
                            <tr>
                                <?php foreach ([1,2,3,4,5,6,7] as $vv) : ?>
                                    <td>
                                        <input type="text" name="form[medikasi][<?=$v?>][4][<?=$vv?>][jam]" class="form-control">
                                    </td>
                                    <td>
                                        <input type="text" name="form[medikasi][<?=$v?>][5][<?=$vv?>][prwt]" class="form-control">
                                    </td>
                                    <td>
                                        <input type="text" name="form[medikasi][<?=$v?>][6][<?=$vv?>][klg]" class="form-control">
                                    </td>
                                <?php endforeach; ?>
                            </tr>
                            <tr>
                                <?php foreach ([1,2,3,4,5,6,7] as $vv) : ?>
                                    <td>
                                        <input type="text" name="form[medikasi][<?=$v?>][7][<?=$vv?>][jam]" class="form-control">
                                    </td>
                                    <td>
                                        <input type="text" name="form[medikasi][<?=$v?>][8][<?=$vv?>][prwt]" class="form-control">
                                    </td>
                                    <td>
                                        <input type="text" name="form[medikasi][<?=$v?>][9][<?=$vv?>][klg]" class="form-control">
                                    </td>
                                <?php endforeach; ?>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
                <div class="text-center">
                    <?php if (superadmin($this->session->userdata('logged_in')) || apoteker($this->session->userdata('logged_in'))) : ?>
                    <button class="btn btn-primary" type="submit">
                        Simpan
                    </button>
                    <?php endif; ?>
                </div>
            </form>
        </div>
    </div>
</div>