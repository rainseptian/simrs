<div class="tab-pane <?=$tab == 'biaya' ? 'active' : ''?>" id="biaya">
    <div class="impbtnview">
        <a href="#" class="btn btn-sm btn-primary dropdown-toggle addcharges"
           onclick="holdModal('tambah_biaya')" data-toggle='modal'><i
                class="fa fa-plus"></i> Tambah Biaya
        </a>
    </div>
    <div class="table-responsive">
        <table class="table table-bordered example">
            <thead>
            <tr>
                <th>No</th>
                <th>Jenis Biaya</th>
                <th>Item</th>
                <th>Biaya</th>
                <th>Aksi</th>
            </tr>
            </thead>
            <tbody>
            <?php
            $total = 0;
            foreach($biaya as $r) : ?>
                <tr class="row-biaya">
                    <td><?=$no++?></td>
                    <td><?=$r->jenis_biaya?></td>
                    <td>
                        <?php
                        if ($r->jenis_biaya == 'Biaya Kamar') {
                            echo $rawat_inap->bed_name.' - '.$rawat_inap->bedgroup;
                        }
                        else if ($r->jenis_biaya == 'Makanan') {
                            echo $r->makanan->nama;
                        }
                        else if ($r->jenis_biaya == 'Tindakan') : ?>
                            <table class="table table-condensed">
                                <thead>
                                <tr>
                                    <th>Nama</th>
                                    <th>Tarif</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($r->tindakan as $t) : ?>
                                    <tr>
                                        <td><?=$t->nama?></td>
                                        <td><?=$t->tarif_pasien?></td>
                                    </tr>
                                <?php endforeach; ?>
                                </tbody>
                            </table>
                        <?php endif; ?>
                    </td>
                    <td class="text-right"><?=$r->biaya?></td>
                    <td style="width: 50px">
                        <a href="<?=base_url()?>RawatInap/hapus/biaya/<?=$rawat_inap->id?>/<?=$r->id?>"
                           onclick="return confirm('Hapus data ini?')"
                           class="btn btn-danger btn-sm">
                            <span class="fa fa-trash"></span> Hapus
                        </a>
                    </td>
                </tr>
                <?php
                $total += $r->biaya;
            endforeach;
            ?>
            </tbody>


            <tr class="box box-solid total-bg">
                <td colspan='4' class="text-right">
                    <?php echo $this->lang->line('total') . " : " . $currency_symbol . "" . $total ?>
                    <input type="hidden" id="charge_total" name="charge_total"
                           value="<?php echo $total ?>">
                </td>
            </tr>
        </table>
    </div>
</div>