<div class="tab-pane <?=isset($tab) && $tab == 'form_edukasi' ? 'active' : ''?>" id="form_edukasi">
    <div class="impbtnview">
        <a href="<?=base_url("RawatInap/print_form/edukasi/$rawat_inap->id")?>" target="_blank" class="btn btn-primary btn-sm">
            <i class="fa fa-print"></i> Cetak
        </a>
    </div>
    <div class="row">
        <div class="col-md-12">
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th rowspan="2" style="text-align: center; vertical-align: middle">TGL/JAM</th>
                    <th rowspan="2" style="text-align: center; vertical-align: middle">EDUKASI</th>
                    <th colspan="3" style="text-align: center">TANDA TANGAN DAN NAMA JELAS</th>
                </tr>
                <tr>
                    <th style="text-align: center">PASIEN</th>
                    <th style="text-align: center">KELUARGA</th>
                    <th style="text-align: center">STAFF RS</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($form['edukasi'] as $v) : ?>
                    <tr>
                        <td><?=date('d-F-Y H:i', strtotime(str_replace('T', ' ', $v['date'])))?></td>
                        <td>
                            <div style="white-space: pre-wrap;"><?=$v['edukasi']?></div>
                        </td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
            <div class="text-center">
                <?php if (superadmin($this->session->userdata('logged_in')) || perawat($this->session->userdata('logged_in'))) : ?>
                    <a href="#" class="btn btn-primary dropdown-toggle addcharges"
                       onclick="holdModal('tambah_edukasi')" data-toggle='modal'>Tambah Edukasi
                    </a>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>