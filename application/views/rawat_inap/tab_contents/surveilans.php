<div class="tab-pane <?=isset($tab) && $tab == 'surveilans' ? 'active' : ''?>" id="surveilans">
    <div class="impbtnview">
        <a href="<?=base_url("RawatInap/print_form/surveilans/$rawat_inap->id")?>" target="_blank" class="btn btn-primary btn-sm">
            <i class="fa fa-print"></i> Cetak
        </a>
    </div>
    <div class="row">
        <div class="col-md-12">
            <form method="post" action="<?php echo base_url() ?>RawatInap/save_form/surveilans_header/surveilans">
                <input type="hidden" name="rawat_inap_id" value="<?=$rawat_inap->id?>">
                <table class="table table-bordered" style="table-layout: fixed">
                    <tbody>
                    <tr>
                        <th colspan="16" style="text-align: center; vertical-align: middle">SURVEILANS INFEKSI NOSOKOMIAL</th>
                    </tr>
                    <tr>
                        <td colspan="5">
                            <label>Bulan</label>
                            <input type="text" class="form-control" name="form[surveilans_header][bulan]" value="<?=$form['surveilans_header']['bulan']?>">
                        </td>
                        <td colspan="5">
                            <label>Surveyor</label>
                            <input type="text" class="form-control" name="form[surveilans_header][surveyor]" value="<?=$form['surveilans_header']['surveyor']?>">
                        </td>
                        <td colspan="5">
                            <label>Diagnosa</label>
                            <input type="text" class="form-control" name="form[surveilans_header][diagnosa]" value="<?=$form['surveilans_header']['diagnosa']?>">
                        </td>
                        <td style="vertical-align: middle">
                            <button type="submit" class="btn btn-success btn-sm">Simpan</button>
                        </td>
                    </tr>
                    <tr>
                        <th class="text-center" style="vertical-align: middle" rowspan="2">Tgl/Jam</th>
                        <th class="text-center" style="vertical-align: middle" rowspan="2">Suhu</th>
                        <th class="text-center" style="vertical-align: middle" colspan="4">TINDAKAN</th>
                        <th class="text-center" style="vertical-align: middle" colspan="7">INFEKSI NOSOKOMIAL</th>
                        <th class="text-center" style="vertical-align: middle" rowspan="2">TIRAH BARING</th>
                        <th class="text-center" style="vertical-align: middle" rowspan="2">HASIL KULTUR / LABORAT</th>
                        <th class="text-center" style="vertical-align: middle" rowspan="2">ANTIBIOTIK</th>
                    </tr>
                    <tr>
                        <th class="text-center" style="vertical-align: middle">Kateter</th>
                        <th class="text-center" style="vertical-align: middle">Vena Perifer</th>
                        <th class="text-center" style="vertical-align: middle">Vena Sentral</th>
                        <th class="text-center" style="vertical-align: middle">Ventilator</th>
                        <th class="text-center" style="vertical-align: middle">VAP</th>
                        <th class="text-center" style="vertical-align: middle">HAB</th>
                        <th class="text-center" style="vertical-align: middle">ISK</th>
                        <th class="text-center" style="vertical-align: middle">LADP</th>
                        <th class="text-center" style="vertical-align: middle">PLEB</th>
                        <th class="text-center" style="vertical-align: middle">DECUB</th>
                        <th class="text-center" style="vertical-align: middle">ILO</th>
                    </tr>
                    <?php foreach ($form['surveilans'] as $v) : ?>
                        <tr>
                            <td><?=date('d-F-Y H:i', strtotime(str_replace('T', ' ', $v['tgl'])))?></td>
                            <td><div style="white-space: pre-wrap;"><?=$v['suhu']?></div></td>
                            <td><div style="white-space: pre-wrap;"><?=$v['Kateter']?></div></td>
                            <td><div style="white-space: pre-wrap;"><?=$v['VenaPerifer']?></div></td>
                            <td><div style="white-space: pre-wrap;"><?=$v['VenaSentral']?></div></td>
                            <td><div style="white-space: pre-wrap;"><?=$v['Ventilator']?></div></td>
                            <td><div style="white-space: pre-wrap;"><?=$v['VAP']?></div></td>
                            <td><div style="white-space: pre-wrap;"><?=$v['HAB']?></div></td>
                            <td><div style="white-space: pre-wrap;"><?=$v['ISK']?></div></td>
                            <td><div style="white-space: pre-wrap;"><?=$v['LADP']?></div></td>
                            <td><div style="white-space: pre-wrap;"><?=$v['PLEB']?></div></td>
                            <td><div style="white-space: pre-wrap;"><?=$v['DECUB']?></div></td>
                            <td><div style="white-space: pre-wrap;"><?=$v['ILO']?></div></td>
                            <td><div style="white-space: pre-wrap;"><?=$v['TIRAHBARING']?></div></td>
                            <td><div style="white-space: pre-wrap;"><?=$v['HASIL']?></div></td>
                            <td><div style="white-space: pre-wrap;"><?=$v['ANTIBIOTIK']?></div></td>
                        </tr>
                    <?php endforeach; ?>
                    <tr>
                        <td colspan="16">
                            <div style="display: flex">
                                <div style="display: flex; flex-direction: column; padding-right: 10px">
                                    <span>VAP : Ventilator Associated Pneomonia</span>
                                    <span>HAP : Hospital Acquired Pneomonia</span>
                                    <span>ISK : Infeksi Saluran Kemih</span>
                                </div>
                                <div style="display: flex; flex-direction: column; padding-right: 10px">
                                    <span>PLEB : Plebtis</span>
                                    <span>DECUB: Decubitus</span>
                                    <span>ILO : Infeksi Luka Operasi</span>
                                </div>
                                <div style="display: flex; flex-direction: column">
                                    <span>IADP : Infeksi ALiran DarahPrimer</span>
                                </div>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </form>
            <div class="text-center">
                <?php if (superadmin($this->session->userdata('logged_in')) || perawat($this->session->userdata('logged_in'))) : ?>
                <a href="#" class="btn btn-primary dropdown-toggle addcharges"
                   onclick="holdModal('tambah_surveilans')" data-toggle='modal'>Tambah Surveilans
                </a>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>