<div class="tab-pane <?=$tab == 'bahan_habis_pakai' ? 'active' : ''?>" id="bahan_habis_pakai">
    <div class="impbtnview">
        <a href="#" class="btn btn-sm btn-primary dropdown-toggle addprescription"
           onclick="holdModal('tambah_bahan_habis_pakai')" data-toggle="modal"><i
                class="fas fa-plus"></i> Tambah Bahan Habis Pakai
        </a>
    </div><!--./impbtnview-->
    <div class="table-responsive">
        <table class="table table-bordered example">
            <thead>
            <tr>
                <th>No</th>
                <th>Tanggal</th>
                <th>Bahan Habis Pakai</th>
                <th>Aksi</th>
            </tr>
            </thead>
            <tbody>
            <?php
            $no = 1;
            foreach ($bahan_habis_pakai as $r) : ?>
                <tr>
                    <td><?=$no++?></td>
                    <td><?=$r->created_at?></td>
                    <td style="font-size: 12px">
                        <table class="table table-condensed">
                            <thead>
                            <tr>
                                <th>Nama</th>
                                <th>Jumlah</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($r->bahan as $b) : ?>
                                <tr>
                                    <td><?=$b->nama?></td>
                                    <td><?=$b->jumlah?></td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                    </td>
                    <td style="width: 50px">
                        <a href="<?=base_url()?>RawatInap/hapus/bahan_habis_pakai/<?=$rawat_inap->id?>/<?=$r->id?>"
                           onclick="return confirm('Hapus data ini?')"
                           class="btn btn-danger btn-sm">
                            <span class="fa fa-trash"></span> Hapus
                        </a>
                    </td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>