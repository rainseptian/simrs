<div class="tab-pane <?=$tab == 'pemeriksaan' ? 'active' : ''?>" id="pemeriksaan">
    <div class="impbtnview">
        <a href="#" class="btn btn-sm btn-primary dropdown-toggle addconsultant"
           onclick="holdModal('tambah_pemeriksaan')" data-toggle="modal">
            <i class="fas fa-plus"></i>Tambah Pemeriksaan
        </a>
    </div>
    <div class="table-responsive">
        <table class="table table-striped table-bordered table-hover example">
            <thead>
            <tr>
                <th>No</th>
                <th>Tgl Pemeriksaan</th>
                <th>Shift</th>
                <th>Dokter</th>
                <th>Subjektif</th>
                <th>Objektif</th>
                <th>Asesmen</th>
                <th>Plan</th>
                <th>Aksi</th>
            </tr>
            </thead>
            <tbody>
            <?php
            $no = 1;
            if (!empty($pemeriksaan)) {
                foreach ($pemeriksaan as $r) {
                    ?>
                    <tr class="row-pemeriksaan">
                        <td><?=$no++?></td>
                        <td><?=$r->created_at?></td>
                        <td><?=$r->shift?></td>
                        <td><?=$r->nama_dokter?></td>
                        <td><?=$r->subjektif?></td>
                        <td>
                            <table class="table table-bordered" style="margin-bottom: 0 !important;">
                                <?php
                                $obj = unserialize($r->objektif);
                                foreach ($obj as $k => $v) : ?>
                                    <tr>
                                        <td style="padding-top: 0 !important; padding-bottom: 0 !important; font-weight: bold;">
                                            <?=$k?>
                                        </td>
                                        <td style="padding-top: 0 !important; padding-bottom: 0 !important;">
                                            <?=$v?>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            </table>
                        </td>
                        <td><?=$r->asesmen?></td>
                        <td><?=$r->plan?></td>
                        <td style="width: 50px">
                            <a href="<?=base_url()?>RawatInap/hapus/pemeriksaan/<?=$rawat_inap->id?>/<?=$r->id?>"
                               onclick="return confirm('Hapus data ini?')"
                               class="btn btn-danger btn-sm">
                                <span class="fa fa-trash"></span> Hapus
                            </a>
                        </td>
                    </tr>
                    <?php
                }
            }
            ?>
            </tbody>
        </table>
    </div>
</div>