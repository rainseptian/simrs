<div class="tab-pane <?=$tab == 'pemeriksaan' ? 'active' : ''?>" id="pemeriksaan">
    <div class="impbtnview">
        <?php if (
                superadmin($this->session->userdata('logged_in')) ||
                perawat($this->session->userdata('logged_in')) ||
                dokter($this->session->userdata('logged_in')) ||
                gizi($this->session->userdata('logged_in')) ||
                apoteker($this->session->userdata('logged_in'))
        ) : ?>
        <a href="#" class="btn btn-sm btn-primary dropdown-toggle addconsultant"
           onclick="holdModal('tambah_pemeriksaan')" data-toggle="modal">
            <i class="fas fa-plus"></i>Tambah Pemeriksaan
        </a>
        <br>
        <a target="_blank" href="<?=base_url("RawatInap/print_form/pemeriksaan/$rawat_inap->id")?>" class="btn btn-sm btn-primary"
           style="margin-top: 4px">
            <i class="fas fa-print"></i>Cetak
        </a>
        <?php endif; ?>
    </div>
    <div class="table-responsive">
        <table class="table table-striped table-bordered table-hover example">
            <thead>
            <tr>
                <th>No</th>
                <th>Tgl Pemeriksaan</th>
                <th>Nakes</th>
                <th>Deteksi Vital</th>
                <th>Subjektif</th>
                <th>Objektif</th>
                <th>Asesmen</th>
                <th>Plan</th>
                <th>ICD 10</th>
                <th>Tindakan</th>
                <th>Konsultasi</th>
                <th>Resep</th>
                <th>Aksi</th>
            </tr>
            </thead>
            <tbody>
            <?php
            $no = 1;
            if (!empty($pemeriksaan)) {
                foreach ($pemeriksaan as $r) {
                    ?>
                    <tr class="row-pemeriksaan">
                        <td><?=$no++?></td>
                        <td>
                            <?=$r->created_at?>
                            <br>
                            <span class="label label-danger ml-1">Shift <?=$r->shift?></span>
                        </td>
                        <td>
                            <?php $nakes = json_decode($r->nakes); ?>
                            <?php if ($r->creator_type == 'perawat') : ?>
                                <b>Perawat:</b> <?=join(', ', array_map(function ($v) { return $v->name; }, $nakes->p))?><br>
                            <?php elseif ($r->creator_type == 'dokter') : ?>
                                <b>Dokter:</b> <?=$nakes->d->name ?? ''?><br>
                            <?php elseif ($r->creator_type == 'apoteker') : ?>
                                <b>Apoteker:</b> <?=$nakes->a->name ?? ''?><br>
                            <?php elseif ($r->creator_type == 'gizi') : ?>
                                <b>Gizi:</b> <?=$nakes->g->name ?? ''?><br>
                            <?php endif; ?>
                        </td>
                        <td>
                            <table class="table table-bordered" style="margin-bottom: 0 !important;">
                                <?php
                                $obj = unserialize($r->objektif);
                                foreach ($obj as $k => $v) : ?>
                                    <tr>
                                        <td style="padding-top: 0 !important; padding-bottom: 0 !important; font-weight: bold;">
                                            <?=$k?>
                                        </td>
                                        <td style="padding-top: 0 !important; padding-bottom: 0 !important;">
                                            <?=$v?>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            </table>
                        </td>
                        <td>
                            <?php $subjectives = json_decode($r->subjectives); ?>
                            <?php if ($r->creator_type == 'perawat') : ?>
                                <?=$subjectives->p?><br>
                            <?php elseif ($r->creator_type == 'dokter') : ?>
                                <?=$subjectives->d?><br>
                            <?php elseif ($r->creator_type == 'apoteker') : ?>
                                <?=$subjectives->a?><br>
                            <?php elseif ($r->creator_type == 'gizi') : ?>
                                <?=$subjectives->g?>
                            <?php endif; ?>
                        </td>
                        <td>
                            <?php $objectives = json_decode($r->objectives); ?>
                            <?php if ($r->creator_type == 'perawat') : ?>
                                <?=$objectives->p?><br>
                            <?php elseif ($r->creator_type == 'dokter') : ?>
                                <?=$objectives->d?><br>
                            <?php elseif ($r->creator_type == 'apoteker') : ?>
                                <?=$objectives->a?><br>
                            <?php elseif ($r->creator_type == 'gizi') : ?>
                                <?=$objectives->g?>
                            <?php endif; ?>
                        </td>
                        <td>
                            <?php $assessments = json_decode($r->assessments); ?>
                            <?php if ($r->creator_type == 'perawat') : ?>
                                <?=$assessments->p?><br>
                            <?php elseif ($r->creator_type == 'dokter') : ?>
                                <?=$assessments->d?><br>
                            <?php elseif ($r->creator_type == 'apoteker') : ?>
                                <?=$assessments->a?><br>
                            <?php elseif ($r->creator_type == 'gizi') : ?>
                                <?=$assessments->g?>
                            <?php endif; ?>
                        </td>
                        <td>
                            <?php $plans = json_decode($r->plans); ?>
                            <?php if ($r->creator_type == 'perawat') : ?>
                                <?=$plans->p?><br>
                            <?php elseif ($r->creator_type == 'dokter') : ?>
                                <?=$plans->d?><br>
                            <?php elseif ($r->creator_type == 'apoteker') : ?>
                                <?=$plans->a?><br>
                            <?php elseif ($r->creator_type == 'gizi') : ?>
                                <?=$plans->g?>
                            <?php endif; ?>
                        </td>
                        <td>
                            <?php foreach ($r->penyakit as $p) : ?>
                                <span style="margin-left:2px; margin-top:1px;">
                                    <span class="label label-success ml-1"><?=$p->kode.' - '.$p->nama?></span>
                                </span>
                                <br>
                            <?php endforeach; ?>
                        </td>
                        <td>
                            <?php foreach ($r->tindakan as $p) : ?>
                                <span style="margin-left:2px; margin-top:1px;">
                                    <span class="label label-primary ml-1"><?=$p->nama?></span>
                                </span>
                                <br>
                            <?php endforeach; ?>
                        </td>
                        </td>
                        <td>
                            <?php foreach ($r->$riTindakanKonsul as $p) : ?>
                                <span style="margin-left:2px; margin-top:1px;">
                                    <span class="label label-primary ml-1"><?=$p->nama?></span>
                                </span>
                                <br>
                            <?php endforeach; ?>
                        </td>
                        <td style="font-size: 12px">
                            <?php if (count($r->obat)) : ?>
                                <table class="table table-condensed">
                                    <thead>
                                    <tr>
                                        <th>Nama Obat</th>
                                        <th>Jumlah</th>
                                        <th>Signa</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach ($r->obat as $obat) : ?>
                                        <tr>
                                            <td><?=$obat->nama?></td>
                                            <td><?=$obat->jumlah?></td>
                                            <td><?=$obat->signa?></td>
                                        </tr>
                                    <?php endforeach; ?>
                                    </tbody>
                                </table>
                            <?php endif; ?>
                            <?php if (count($r->obat_racik)) : ?>
                                <table class="table table-condensed">
                                    <thead>
                                    <tr>
                                        <th>Racikan</th>
                                        <th>Signa</th>
                                        <th>Catatan</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach ($r->obat_racik as $racik) : ?>
                                        <tr>
                                            <td>
                                                <table class="table table-condensed">
                                                    <thead>
                                                    <tr>
                                                        <th>Nama Obat</th>
                                                        <th>Jumlah</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <?php foreach ($racik->detail as $detail) : ?>
                                                        <tr>
                                                            <td><?=$detail->nama?></td>
                                                            <td><?=$detail->jumlah?></td>
                                                        </tr>
                                                    <?php endforeach; ?>
                                                    </tbody>
                                                </table>
                                            </td>
                                            <td><?=$racik->signa?></td>
                                            <td><?=$racik->catatan?></td>
                                        </tr>
                                    <?php endforeach; ?>
                                    </tbody>
                                </table>
                            <?php endif; ?>
                        </td>
                        <td style="width: 50px">
                            <a href="<?=base_url()?>RawatInap/hapus/pemeriksaan/<?=$rawat_inap->id?>/<?=$r->id?>"
                               onclick="return confirm('Hapus data ini?')"
                               class="btn btn-danger btn-sm">
                                <span class="fa fa-trash"></span> Hapus
                            </a>
                        </td>
                    </tr>
                    <?php
                }
            }
            ?>
            </tbody>
        </table>
    </div>
</div>