<div class="tab-pane <?=isset($tab) && $tab == 'informed_consent' ? 'active' : ''?>" id="informed_consent">
    <div class="impbtnview">
        <a href="<?=base_url("RawatInap/print_form/informed/$rawat_inap->id")?>" target="_blank" class="btn btn-primary btn-sm">
            <i class="fa fa-print"></i> Cetak
        </a>
    </div>
    <div class="row justify-content-center">
        <div class="col-sm-12" style="float: none; margin: 0 auto;">
            <form method="post" action="<?php echo base_url() ?>RawatInap/save_form/informed/informed_consent">
                <input type="hidden" name="rawat_inap_id" value="<?=$rawat_inap->id?>">
                <table class="table table-bordered">
                    <tbody>
                    <tr>
                        <td colspan="3">
                            <h4 class="text-center"><b>Pemberian Informasi (INFORM)</b></h4>
                        </td>
                    </tr>
                    <tr>
                        <td>Dokter Pelaksana Tindakan</td>
                        <td colspan="2">
                            <input type="text" class="form-control" name="form[informed][pelaksana]" value="<?=$form['informed']['pelaksana']?>">
                        </td>
                    </tr>
                    <tr>
                        <td>Pemberi Informasi</td>
                        <td colspan="2">
                            <input type="text" class="form-control" name="form[informed][pemberi_informasi]" value="<?=$form['informed']['pemberi_informasi']?>">
                        </td>
                    </tr>
                    <tr>
                        <td>Penerima Informasi / Pemberi Persetujuan</td>
                        <td colspan="2">
                            <input type="text" class="form-control" name="form[informed][penerima_informasi]" value="<?=$form['informed']['penerima_informasi']?>">
                        </td>
                    </tr>
                    <tr>
                        <th style="width: 33%"><h4 style="margin: 0;"><b>Jenis Informasi</b></h4></th>
                        <th style="width: 34%"><h4 style="margin: 0;"><b>Isi Informasi</b></h4></th>
                        <th style="width: 33%"><h4 style="margin: 0;"><b>Verifikasi (✓)</b></h4></th>
                    </tr>
                    <tr>
                        <td>1. Diagnosis (WD & DD)</td>
                        <td><textarea class="form-control" name="form[informed][isi_1]"><?=$form['informed']['isi_1']?></textarea></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>2. Dasar Diagnosis</td>
                        <td><textarea class="form-control" name="form[informed][isi_2]"><?=$form['informed']['isi_2']?></textarea></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>3. Tindakan Kedokteran</td>
                        <td><textarea class="form-control" name="form[informed][isi_3]"><?=$form['informed']['isi_3']?></textarea></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>4. Indikasi Tindakan</td>
                        <td><textarea class="form-control" name="form[informed][isi_4]"><?=$form['informed']['isi_4']?></textarea></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>5. Tata Cara</td>
                        <td><textarea class="form-control" name="form[informed][isi_5]"><?=$form['informed']['isi_5']?></textarea></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>6. Tujuan</td>
                        <td><textarea class="form-control" name="form[informed][isi_6]"><?=$form['informed']['isi_6']?></textarea></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>7. Resiko</td>
                        <td><textarea class="form-control" name="form[informed][isi_7]"><?=$form['informed']['isi_7']?></textarea></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>8. Komplikasi</td>
                        <td><textarea class="form-control" name="form[informed][isi_8]"><?=$form['informed']['isi_8']?></textarea></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>9. Prognosis</td>
                        <td><textarea class="form-control" name="form[informed][isi_9]"><?=$form['informed']['isi_9']?></textarea></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>10. Alternatif dan Resiko</td>
                        <td><textarea class="form-control" name="form[informed][isi_10]"><?=$form['informed']['isi_10']?></textarea></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>11. Pembiayaan</td>
                        <td>
                            <div style="display: flex; flex-direction: column">
                                <div style="display: flex; align-items: center">
                                    <label style="margin-bottom: 0">BPJS</label>
                                    <input type="radio" name="form[informed][pembiayaan]" value="1" <?=$form['informed']['pembiayaan'] == 1 ? 'checked' : ''?> style="margin-top: 0; margin-left: 4px">
                                </div>
                                <div style="display: flex; align-items: center">
                                    <label style="margin-bottom: 0">Umum</label>
                                    <input type="radio" name="form[informed][pembiayaan]" value="2" <?=$form['informed']['pembiayaan'] == 2 ? 'checked' : ''?> style="margin-top: 0; margin-left: 4px">
                                </div>
                                <input type="text" placeholder="Rp" class="form-control" name="form[informed][pembiayaan_input]" value="<?=$form['informed']['pembiayaan_input']?>">
                            </div>
                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <div style="display: flex; flex-direction: row; align-items: center">
                                <div style="flex: 1"></div>
                                <span>Sampang, </span>
                                <input type="date" class="form-control" name="form[informed][tanggal]" value="<?=$form['informed']['tanggal']?>" style="width: 150px; margin-left: 4px; margin-right: 4px">
                                <span>Pukul</span>
                                <input type="time" class="form-control" name="form[informed][jam]" value="<?=$form['informed']['jam']?>" style="width: 100px; margin-left: 4px; margin-right: 4px">
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <div style="display: flex">
                                <div style="flex: 7; border-right: 1px solid #eeeeee">
                                    Dengan ini menyatakan bahwa Saya telah menerangkan hal-hal di atas secara benar dan jelas serta memberikan kesempatan untuk bertanya dan atau berdiskusi.
                                </div>
                                <div style="flex: 6; text-align: center">
                                    <b>Tanda Tangan Dokter</b>
                                    <br>
                                    <br>
                                    <br>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <div style="display: flex">
                                <div style="flex: 7; border-right: 1px solid #eeeeee">
                                    Dengan ini menyatakan bahwa
                                    (saya <input type="radio" name="form[informed][saya]" value="1" <?=$form['informed']['saya'] == 1 ? 'checked' : ''?> style="transform: translateY(4px)">
                                    / keluarga <input type="radio" name="form[informed][saya]" value="2" <?=$form['informed']['saya'] == 2 ? 'checked' : ''?> style="transform: translateY(4px)">)
                                    pasien <b><?=$pasien->nama?></b> telah menerima informasi sebagaimana di atas yang saya beri tanda paraf di kolom kanannya dan telah memahaminya.
                                </div>
                                <div style="flex: 6; text-align: center">
                                    <b>Tanda Tangan pasien / wali</b>
                                    <br>
                                    <b>pasien / keluarga</b>
                                    <br>
                                    <br>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            Ket: <i>bila pasien tidak kompeten atau tidak mau menerima informasi, maka penerimaan informasi adalah wali atau keluarga terdekatnya</i>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <h4 class="text-center"><b>PERSETUJUAN/PENOLAKAN TINDAKAN KEDOKTERAN (CONSENT)</b></h4>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            Saya yang bertanda tangan di bawah ini:<br>
                            <label>Nama</label> <input type="text" name="form[informed][nama]" value="<?=$form['informed']['nama']?>" class="form-control">
                            <label style="margin-top: 8px">Jenis Kelamin</label><br>
                            <input type="radio" name="form[informed][jk]" value="1" <?=$form['informed']['jk'] == 1 ? 'checked' : ''?>>
                            <label>L</label>
                            <input type="radio" name="form[informed][jk]" value="2" <?=$form['informed']['jk'] == 2 ? 'checked' : ''?>>
                            <label>P</label><br>
                            <label>No. KTP/SIM/Passpor</label> <input type="text" name="form[informed][no_ktp]" value="<?=$form['informed']['no_ktp']?>" class="form-control">
                            <label>Tgl Lahir</label> <input type="date" name="form[informed][tgl_lahir]" value="<?=$form['informed']['tgl_lahir']?>" class="form-control">
                            <label>Umur</label> <input type="text" name="form[informed][umur]" value="<?=$form['informed']['umur']?>" class="form-control">
                            <label>Alamat</label> <input type="text" name="form[informed][alamat]" value="<?=$form['informed']['alamat']?>" class="form-control">
                            <label>Telepon</label> <input type="text" name="form[informed][telepon]" value="<?=$form['informed']['telepon']?>" class="form-control">
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            Hubungan dengan pasien:
                            <br>
                            <input type="radio" id="hub1" name="form[informed][hubungan]" value="1" <?=$form['informed']['hubungan'] == 1 ? 'checked' : ''?>>
                            <label for="hub1" style="padding-right: 10px">Diri Sendiri</label>
                            <input type="radio" id="hub2" name="form[informed][hubungan]" value="2" <?=$form['informed']['hubungan'] == 2 ? 'checked' : ''?>>
                            <label for="hub2" style="padding-right: 10px">Suami</label>
                            <input type="radio" id="hub3" name="form[informed][hubungan]" value="3" <?=$form['informed']['hubungan'] == 3 ? 'checked' : ''?>>
                            <label for="hub3" style="padding-right: 10px">Istri</label>
                            <input type="radio" id="hub4" name="form[informed][hubungan]" value="4" <?=$form['informed']['hubungan'] == 4 ? 'checked' : ''?>>
                            <label for="hub4" style="padding-right: 10px">Anak</label>
                            <input type="radio" id="hub5" name="form[informed][hubungan]" value="5" <?=$form['informed']['hubungan'] == 5 ? 'checked' : ''?>>
                            <label for="hub5" style="padding-right: 10px">Orang Tua</label>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            Dengan ini menyatakan sesungguhnya, bahwa saya telah menerima informasi yang diberikan oleh dokter sebagaimana di atas dan telah memahaminya,
                            untuk itu saya memberikan <b>persetujuan</b> untuk dilakukan tindakan kedokteran tersebut terhadap:<br>
                            <b>Nama:</b> <?=$pasien->nama?> (<?=$pasien->jk?>)<br>
                            <b>No. RM:</b> <?=$pasien->no_rm?> <b>Tgl Lahir / Umur:</b> <?=$pasien->tanggal_lahir?> / <?=$pasien->usia?><br>
                            <b>Alamat:</b> <?=$pasien->alamat?>. <br>
                            Saya memahami perlunya dan manfaat tindakan tersebut termasuk resiko dan komplikasi yang akan timbul,
                            Saya juga menyadari bahwa ilmu Kedokteran bukanlah ilmu pasti, maka keberhasilan tindakan Kedokteran bukanlah keniscayaan,
                            melainkan tergantung kepada Tuhan YME.
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <div style="display: flex; flex-direction: row; align-items: center">
                                <div style="flex: 1"></div>
                                <span>Sampang, </span>
                                <input type="date" class="form-control" name="form[informed][persetujuan][tanggal]" value="<?=$form['informed']['persetujuan']['tanggal']?>" style="width: 150px; margin-left: 4px; margin-right: 4px">
                                <span>Pukul</span>
                                <input type="time" class="form-control" name="form[informed][persetujuan][jam]" value="<?=$form['informed']['persetujuan']['jam']?>" style="width: 100px; margin-left: 4px; margin-right: 4px">
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="text-center">
                                <span>Saksi Petugas</span>
                                <br>
                                <br>
                                <br>
                                <span>(..........................)</span>
                            </div>
                        </td>
                        <td>
                            <div class="text-center">
                                <span>Saksi Pasien</span>
                                <br>
                                <br>
                                <br>
                                <span>(..........................)</span>
                            </div>
                        </td>
                        <td>
                            <div class="text-center">
                                <span>Yang Menyatakan</span>
                                <br>
                                <br>
                                <br>
                                <span>(..........................)</span>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <i>Bila pasien tidak kompeten, maka penerima informasi dan pemberi pernyataan persetujuan adalah wali atau keluarga terdekat</i>
                        </td>
                    </tr>
                    </tbody>
                </table>
                <div class="text-center">
                    <?php if (superadmin($this->session->userdata('logged_in')) || perawat($this->session->userdata('logged_in'))) : ?>
                        <button class="btn btn-success">
                            Simpan
                        </button>
                    <?php endif; ?>
                </div>
            </form>
        </div>
    </div>
</div>