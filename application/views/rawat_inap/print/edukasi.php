<html>
<head>
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/bower_components/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/dist/css/AdminLTE.min.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/dist/css/skins/_all-skins.min.css">
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <style>
        table, td, th {
            border: 1px solid #666666 !important;
        }
        td, th {
            padding: 0 4px !important;
        }
        * {
            font-family: "Times New Roman", serif;
        }

        .table.no-border,
        .table.no-border td,
        .table.no-border th,
        .table.no-border > * > td,
        .table.no-border > * > th {
            border: 0 !important;
        }
    </style>
</head>
<body>

<div style="display: flex; margin-bottom: 20px">
    <img src="<?php echo base_url(); ?>assets/img/klinik/<?php echo $klinik->foto; ?>" height="70px">
    <div style="flex: 1">
        <h4 style="text-align: center"><b>FORMULIR PEMBERIAN EDUKASI</b></h4>
    </div>
</div>

<table class="table no-border">
    <thead>
    <tr>
        <td>No. RM</td>
        <td> : </td>
        <td><?=$pasien->no_rm?></td>
        <td>DIAGNOSA</td>
        <td> : </td>
        <td></td>
    </tr>
    <tr>
        <td>NAMA</td>
        <td> : </td>
        <td><?=$pasien->nama?></td>
        <td>RUANG</td>
        <td> : </td>
        <td><?=$rawat_inap->bed_name.' - '.$rawat_inap->bedgroup?></td>
    </tr>
    <tr>
        <td>TGL LAHIR / UMUR</td>
        <td> : </td>
        <td><?=$pasien->tanggal_lahir?> / <?=$pasien->usia?></td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    </thead>
</table>

<table class="table table-bordered">
    <thead>
    <tr>
        <th rowspan="2" style="text-align: center; vertical-align: middle">TGL/JAM</th>
        <th rowspan="2" style="text-align: center; vertical-align: middle">EDUKASI</th>
        <th colspan="3" style="text-align: center">TANDA TANGAN DAN NAMA JELAS</th>
    </tr>
    <tr>
        <th style="text-align: center">PASIEN</th>
        <th style="text-align: center">KELUARGA</th>
        <th style="text-align: center">STAFF RS</th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($form['edukasi'] as $v) : ?>
        <tr>
            <td><?=date('d-F-Y H:i', strtotime(str_replace('T', ' ', $v['date'])))?></td>
            <td>
                <div style="white-space: pre-wrap;"><?=$v['edukasi']?></div>
            </td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>

<script>
    $(function () {
        print()
    })
</script>
</body>
</html>