<html>
<head>
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/bower_components/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/dist/css/AdminLTE.min.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/dist/css/skins/_all-skins.min.css">
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <style>
        table, td, th {
            border: 1px solid #666666 !important;
        }
        td, th {
            padding: 0 4px !important;
        }
        * {
            font-family: "Times New Roman", serif;
        }

        .table.no-border,
        .table.no-border td,
        .table.no-border th,
        .table.no-border > * > td,
        .table.no-border > * > th {
            border: 0 !important;
        }
    </style>
</head>
<body>

<table class="table table-bordered table-hover" style="table-layout: fixed">
    <tbody>
    <tr>
        <td>
            <div style="display: flex">
                <img src="<?php echo base_url(); ?>assets/img/klinik/<?php echo $klinik->foto; ?>" height="80px">
                <div style="display: flex; flex-direction: column; align-items: center; flex: 1; padding-left: 8px">
                    <h4 class="text-center"><b class="text-center"><?=$klinik->nama?></b></h4>
                    <small class="text-center"><?=$klinik->alamat?></small>
                    <small class="text-center">Email <?=$klinik->email?></small>
                    <small class="text-center">Kabupaten <?=$klinik->kabupaten?></small>
                </div>
            </div>
        </td>
        <td rowspan="2">
            <table class="table no-border">
                <tbody>
                <tr>
                    <td>Nama</td>
                    <td>:</td>
                    <td><?=$pasien->nama?></td>
                </tr>
                <tr>
                    <td>Tgl Lahir</td>
                    <td>:</td>
                    <td><?=$pasien->tanggal_lahir?> / <?=$pasien->usia?></td>
                </tr>
                <tr>
                    <td>No RM</td>
                    <td>:</td>
                    <td><?=$pasien->no_rm?></td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    <tr>
        <td class="text-center">
            <b>PENCATATAN DAN PERENCANAAN TERINTEGRASI</b>
        </td>
    </tr>
    </tbody>
</table>

<table class="table table-bordered table-hover" style="table-layout: fixed">
    <tbody>
    <tr>
        <th class="text-center" style="vertical-align: middle">Tgl/Jam</th>
        <th class="text-center" style="vertical-align: middle">Profesional Pemberi Asuhan (PPA)</th>
        <th class="text-center" style="vertical-align: middle">CATATAN PERKEMBANGAN</th>
        <th class="text-center" style="vertical-align: middle">INTRUKSI PPA</th>
        <th class="text-center" style="vertical-align: middle">VERIFIKASI DPJP</th>
    </tr>
    <?php foreach ($form['ppt'] as $v) : ?>
        <tr>
            <td><?=date('d-F-Y H:i', strtotime(str_replace('T', ' ', $v['tgl'])))?></td>
            <td><div style="white-space: pre-wrap;"><?=$v['ppa']?></div></td>
            <td><div style="white-space: pre-wrap;"><?=$v['catatan']?></div></td>
            <td><div style="white-space: pre-wrap;"><?=$v['intruksi']?></div></td>
            <td><div style="white-space: pre-wrap;"><?=$v['verifikasi']?></div></td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>

<script>
    $(function () {
        print()
    })
</script>
</body>
</html>