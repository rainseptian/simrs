<html>
<head>
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/bower_components/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/dist/css/AdminLTE.min.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/dist/css/skins/_all-skins.min.css">
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <style>
        table, td, th {
            border: 1px solid #666666 !important;
        }
        td, th {
            padding: 0 4px !important;
        }
        * {
            font-family: "Times New Roman", serif;
        }

        .table.no-border,
        .table.no-border td,
        .table.no-border th,
        .table.no-border > * > td,
        .table.no-border > * > th {
            border: 0 !important;
        }
    </style>
</head>
<body>

<div style="display: flex;">
    <div style="display: flex; flex: 1">
        <img src="<?php echo base_url(); ?>assets/img/klinik/<?php echo $klinik->foto; ?>" height="90px">
        <div style="display: flex; flex-direction: column; align-items: center; flex: 1">
            <h3 style="margin: 0 !important;"><b><?=$klinik->nama?></b></h3>
            <span><?=$klinik->alamat?></span>
            <span>Email : <?=$klinik->email?></span>
            <span>Kabupaten <?=$klinik->kabupaten?></span>
        </div>
    </div>
</div>

<table class="table table-bordered" style="margin-top: 12px">
    <tbody>
    <tr>
        <td>Nama : <?=$pasien->nama?></td>
        <td>Ruang Kelas : <?=$rawat_inap->bed_name.' - '.$rawat_inap->bedgroup?></td>
    </tr>
    <tr>
        <td>Umur/Jenis Kelamin : <?=$pasien->usia?>/<?=$pasien->jenis_kelamin?></td>
        <td>Tgl Masuk/Jam : <?=date('d/F/Y H:i', strtotime($rawat_inap->created_at))?></td>
    </tr>
    </tbody>
</table>

<div style="display: flex; flex-direction: column; align-items: center">
    <h4 style="margin: 0 !important;"><b>PERNYATAAN PERSETUJUAN RAWAT INAP</b></h4>
</div>

<div style="display: flex; flex-direction: column">
    <span>Yang bertanda tangan di bawah ini :</span>
    <span>Nama : <?=$form['persetujuan']['nama']?></span>
    <span>Tgl. Lahir : <?=$form['persetujuan']['tgl_lahir']?></span>
    <span>Alamat : <?=$form['persetujuan']['alamat']?></span>
    <br>
    <span>Dalam hal ini bertindak sebagai <b><?=$form['persetujuan']['sbg'] == 1 ? 'diri sendiri' : ($form['persetujuan']['sbg'] == 2 ? 'Suami' : ($form['persetujuan']['sbg'] == 3 ? 'Istri' : ($form['persetujuan']['sbg'] == 4 ? 'Ayah' : 'Ibu')))?></b> penanggung jawab * dari</span>
    <span>Nama : <?=$pasien->nama?></span>
    <span>Tgl. Lahir : <?=$pasien->tanggal_lahir?></span>
    <span>Alamat : <?=$pasien->alamat?></span>
    <br>
    <span>
        Saya telah mendapatkan informasi dari petugas mengenai hak dan kewajiban saya serta tata tertib di KLINIK UTAMA SUKMA WIJAYA,
        dengan demikian saya menyatakan :
    </span>
    <span>1. Pasien tersebut di atas adalah pasien dengan kepesertaan:</span>
    <table class="table table-bordered">
        <tbody>
        <tr>
            <th rowspan="2">Kepesertaan</th>
            <th colspan="2">Persyaratan</th>
            <th rowspan="2">Keterangan</th>
        </tr>
        <tr>
            <th>Lengkap</th>
            <th>Menyusul</th>
        </tr>
        <tr>
            <td><input type="checkbox" name="form[persetujuan][umum][waw]" value="1" <?=$form['persetujuan']['umum']['waw'] == 1 ? 'checked' : ''?>> Umum</td>
            <td><input type="checkbox" name="form[persetujuan][umum][lengkap]" value="1" <?=$form['persetujuan']['umum']['lengkap'] == 1 ? 'checked' : ''?>></td>
            <td><input type="checkbox" name="form[persetujuan][umum][menyusui]" value="1" <?=$form['persetujuan']['umum']['menyusui'] == 1 ? 'checked' : ''?>></td>
            <td><?=$form['persetujuan']['umum']['ket']?></td>
        </tr>
        <tr>
            <td><input type="checkbox" name="form[persetujuan][bpjs][waw]" value="1" <?=$form['persetujuan']['bpjs']['waw'] == 1 ? 'checked' : ''?>> BPJS</td>
            <td><input type="checkbox" name="form[persetujuan][bpjs][lengkap]" value="1" <?=$form['persetujuan']['bpjs']['lengkap'] == 1 ? 'checked' : ''?>></td>
            <td><input type="checkbox" name="form[persetujuan][bpjs][menyusui]" value="1" <?=$form['persetujuan']['bpjs']['menyusui'] == 1 ? 'checked' : ''?>></td>
            <td><?=$form['persetujuan']['bpjs']['ket']?></td>
        </tr>
        <tr>
            <td><input type="checkbox" name="form[persetujuan][jkd][waw]" value="1" <?=$form['persetujuan']['jkd']['waw'] == 1 ? 'checked' : ''?>> JKD/SKTM</td>
            <td><input type="checkbox" name="form[persetujuan][jkd][lengkap]" value="1" <?=$form['persetujuan']['jkd']['lengkap'] == 1 ? 'checked' : ''?>></td>
            <td><input type="checkbox" name="form[persetujuan][jkd][menyusui]" value="1" <?=$form['persetujuan']['jkd']['menyusui'] == 1 ? 'checked' : ''?>></td>
            <td><?=$form['persetujuan']['jkd']['ket']?></td>
        </tr>
        <tr>
            <td><input type="checkbox" name="form[persetujuan][lainnya][waw]" value="1" <?=$form['persetujuan']['lainnya']['waw'] == 1 ? 'checked' : ''?>> Asuransi lainnya</td>
            <td><input type="checkbox" name="form[persetujuan][lainnya][lengkap]" value="1" <?=$form['persetujuan']['lainnya']['lengkap'] == 1 ? 'checked' : ''?>></td>
            <td><input type="checkbox" name="form[persetujuan][lainnya][menyusui]" value="1" <?=$form['persetujuan']['lainnya']['menyusui'] == 1 ? 'checked' : ''?>></td>
            <td><?=$form['persetujuan']['lainnya']['ket']?></td>
        </tr>
        </tbody>
    </table>
    <span>2. Setuju akan menempati ruang perawatan <b>Kelas <?=$form['persetujuan']['kelas1']?></b></span>
    <span>3. Kepesertaan pasien BPJS PBI atau PBID (Penerima Bantuan Iuran Daerah) akan GUGUR atau TIDAK BERLAKU apabila :</span>
    <span style="padding-left: 10px">a. Memilih dirawat kelas II, I, VIP</span>
    <span style="padding-left: 10px">a. Persyaratan tidak dilengkapi dalam waktu 2x24 jam</span>
    <span style="padding-left: 10px">a. Mendapatkan pembiayaan dari penjamin lain di luar BPJS</span>
    <span>4. Sanggup dan bersedia membayar seluruh biaya perawatan sesuai dengan kelas yang saya kehendaki.</span>
    <span>5. Memberi kuasa kepada Dokter KLINIK UTAMA SUKMA WIJAYA untuk memberi keterangan yang diperlukan oleh pihak penanggung biaya perawatan saya/pasien tersebut di atas.</span>
    <br>
    <span>Demikian pernyataan ini saya buat dengan penuh kesadaran dan tanpa paksaan pihak manapun.</span>
    <br>
    <div style="display: flex">
        <div style="flex: 1"></div>
        Sampang, <?=date('d-F-Y')?>
    </div>
    <br>
    <div style="display: flex">
        <div style="display: flex; flex-direction: column; align-items: center; flex: 1">
            <span>Petugas Pendaftaran</span>
            <br>
            <br>
            <br>
            <span>(........................................)</span>
            <span>Tanda Tangan & Nama Terang</span>
        </div>
        <div style="display: flex; flex-direction: column; align-items: center; flex: 1">
            <span>Yang Membuat Pernyataan</span>
            <br>
            <br>
            <br>
            <span>(........................................)</span>
            <span>Tanda Tangan & Nama Terang</span>
        </div>
    </div>
</div>

<script>
    $(function () {
        print()
    })
</script>
</body>
</html>