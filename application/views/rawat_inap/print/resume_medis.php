<html>
<head>
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/bower_components/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/dist/css/AdminLTE.min.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/dist/css/skins/_all-skins.min.css">
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <style>
        table, td, th {
            border: 1px solid #666666 !important;
        }
        td, th {
            padding: 0 4px !important;
        }
        * {
            font-family: "Times New Roman", serif;
        }

        .table.no-border,
        .table.no-border td,
        .table.no-border th,
        .table.no-border > * > td,
        .table.no-border > * > th {
            border: 0 !important;
        }
    </style>
</head>
<body>

<table class="table table-bordered" style="table-layout: fixed">
    <tbody>
    <tr>
        <td>
            <div style="display: flex; flex-direction: column; align-items: center">
                <img src="<?php echo base_url(); ?>assets/img/klinik/<?php echo $klinik->foto; ?>" height="70px">
                <div style="text-align: center; margin-left: 20px; margin-top: 5px">
                    <small style="font-size: 18px"><?=$klinik->nama?></small><br>
                    <small style="font-size: 11px"><?=$klinik->alamat?></small>
                </div>
            </div>
        </td>
        <td colspan="3">
            <table class="table no-border" style="font-size: 12px">
                <tbody>
                <tr>
                    <td>Nama Pasien</td>
                    <td>:</td>
                    <td><?=$pasien->nama?></td>
                    <td>No RM</td>
                    <td>:</td>
                    <td><?=$pasien->no_rm?></td>
                </tr>
                <tr>
                    <td>Jenis Kelamin</td>
                    <td>:</td>
                    <td><?=$pasien->jk?></td>
                    <td>Tgl Lahir</td>
                    <td>:</td>
                    <td><?=$pasien->tanggal_lahir?> / <?=$pasien->usia?></td>
                </tr>
                <tr>
                    <td>Ruang / Kelas</td>
                    <td>:</td>
                    <td><?=$rawat_inap->bed_name.' - '.$rawat_inap->bedgroup?></td>
                    <td>Tgl Masuk</td>
                    <td>:</td>
                    <td><?=$rawat_inap->created_at?></td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    <tr>
        <td colspan="4">
            <h4 class="text-center"><b>RESUME MEDIS</b></h4>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <span>Tgl Masuk : <?=$form['resume_medis']['tgl_masuk'] ?: date('Y-m-d', strtotime($rawat_inap->created_at))?></span>
        </td>
        <td colspan="2" rowspan="2">
            <span>Dokter yang merawat : <?=$form['resume_medis']['dokter']?></span>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <span>Tgl Keluar : <?=$form['resume_medis']['tgl_keluar'] ?: date('Y-m-d')?></span>
        </td>
    </tr>
    <tr>
        <td colspan="4">
            <span>Diagnosa Masuk : <?=$form['resume_medis']['diagnosa_masuk']?></span>
            <br>
            <span>Indikasi Rawat : <?=$form['resume_medis']['indikasi_rawat']?></span>
            <br>
            <span>Diagnosa Keluar : <?=$form['resume_medis']['diagnosa_keluar']?></span>
            <br>
            <span>Kode ICD 10 : <?=implode(', ', $form['resume_medis']['icd10'])?></span>
            <br>
            <span>Komplikasi : <?=$form['resume_medis']['komplikasi']?></span>
            <br>
            <span>Komorbid : <?=$form['resume_medis']['komorbid']?></span>
            <br>
            <span>Tindakan/Operasi : <?=$form['resume_medis']['tindakan']?></span>
            <br>
            <div class="row">
                <div class="col-sm-6">
                    <span>Tgl Dilakukan : <?=$form['resume_medis']['tgl_dilakukan']?></span>
                </div>
                <div class="col-sm-6">
                    <span>Kode ICD 9 : <?=isset($form['resume_medis']['icd9']) ? $form['resume_medis']['icd9'] : '' ?></span>
                </div>
            </div>
        </td>
    </tr>
    <tr>
        <td colspan="4">
            <h4><b>Anamnese</b></h4>
            <span>• Keluhan Utama : <?=$form['resume_medis']['keluhan_utama']?></span>
            <br>
            <span>• Gejala Penyerta : <?=$form['resume_medis']['gejala_penyerta']?></span>
            <br>
            <span>• Riwayat Penyakit Dahulu : <?=$form['resume_medis']['rpd']?></span>
            <br>
            <h4><b>Temuan Penting</b></h4>
            <span>• Pemeriksaan Fisik : <?=$form['resume_medis']['pemeriksaan_fisik']?></span>
            <br>
            <span>• Pemeriksaan Penunjang : <?=$form['resume_medis']['pemeriksaan_penunjang']?></span>
            <br>
            <span>• Laboratorium : <?=$form['resume_medis']['lab']?></span>
            <br>
            <span>• Pencitraan Diagnosa : <?=$form['resume_medis']['citra']?></span>
            <br>
            <span>• Lainnya : <?=$form['resume_medis']['lainnya']?></span>
            <br>
            <br>
            <span>Konsultasi : <?=$form['resume_medis']['konsultasi']?></span>
            <br>
            <span>Obat Selama rawat : <?=$form['resume_medis']['obat_rawat']?></span>
            <br>
            <span>Kondisi Saat Pulang : <?=$form['resume_medis']['kondisi']?></span>
            <br>
            <span>Obat Pulang : <?=$form['resume_medis']['obat_pulang']?></span>
            <br>
            <h5><b><u>Intruksi Lanjut</u></b></h5>
            <span>• Kontrol Ulang : <?=$form['resume_medis']['kontrol']?></span>
            <br>
            <span>• Segera bawa ke RS bila : <?=$form['resume_medis']['segera']?></span>
        </td>
    </tr>
    <tr>
        <td colspan="4">
            <div style="display: flex; flex-direction: row-reverse">
                <div class="text-center" style="display: flex; flex-direction: column">
                    <span>Sampang, <?=date('d-F-Y')?> Jam : <?=date('H:i')?></span>
                    <span>Dokter yang Merawat</span>
                    <br>
                    <br>
                    <br>
                    <span><?=$form['resume_medis']['dokter']?></span>
                    <span>Tanda Tangan & Nama Terang</span>
                </div>
            </div>
        </td>
    </tr>
    </tbody>
</table>

<script>
    $(function () {
        print()
    })
</script>
</body>
</html>