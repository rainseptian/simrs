<html>
<head>
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/bower_components/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/dist/css/AdminLTE.min.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/dist/css/skins/_all-skins.min.css">
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <style>
        table, td, th {
            border: 1px solid #666666 !important;
        }
        td, th {
            padding: 0 4px !important;
        }
        * {
            font-family: "Times New Roman", serif;
        }

        .table.no-border,
        .table.no-border td,
        .table.no-border th,
        .table.no-border > * > td,
        .table.no-border > * > th {
            border: 0 !important;
        }
    </style>
</head>
<body>

<table class="table table-bordered table-hover" style="table-layout: fixed">
    <tbody>
    <tr>
        <td colspan="11" class="text-center">
            <b>LEMBAR OBSERVASI</b>
        </td>
    </tr>
    <tr>
        <td colspan="2" class="text-center" style="vertical-align: middle">
            <img src="<?php echo base_url(); ?>assets/img/klinik/<?php echo $klinik->foto; ?>" height="100px">
        </td>
        <td colspan="9">
            <table class="table no-border">
                <tbody>
                <tr>
                    <td>Nama Pasien</td>
                    <td>:</td>
                    <td><?=$pasien->nama?></td>
                    <td>No RM</td>
                    <td>:</td>
                    <td><?=$pasien->no_rm?></td>
                </tr>
                <tr>
                    <td>Jenis Kelamin</td>
                    <td>:</td>
                    <td><?=$pasien->jk?></td>
                    <td>Tgl Lahir</td>
                    <td>:</td>
                    <td><?=$pasien->tanggal_lahir?> / <?=$pasien->usia?></td>
                </tr>
                <tr>
                    <td>Ruang / Kelas</td>
                    <td>:</td>
                    <td><?=$rawat_inap->bed_name.' - '.$rawat_inap->bedgroup?></td>
                    <td>Tgl Masuk</td>
                    <td>:</td>
                    <td><?=$rawat_inap->created_at?></td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    <tr>
        <td colspan="11" class="text-center">
            <b>LEMBAR OBSERVASI</b>
        </td>
    </tr>
    <tr>
        <th class="text-center" style="vertical-align: middle" rowspan="2">Tgl</th>
        <th class="text-center" style="vertical-align: middle" rowspan="2">Jam</th>
        <th class="text-center" style="vertical-align: middle" rowspan="2">Cairan Intravena Menit</th>
        <th class="text-center" style="vertical-align: middle" rowspan="1">Tetesan</th>
        <th class="text-center" style="vertical-align: middle" rowspan="1">Tensi</th>
        <th class="text-center" style="vertical-align: middle" rowspan="1">Nadi</th>
        <th class="text-center" style="vertical-align: middle" rowspan="1">Suhu</th>
        <th class="text-center" style="vertical-align: middle" rowspan="1">Pernafasan</th>
        <th class="text-center" style="vertical-align: middle" rowspan="2">Input</th>
        <th class="text-center" style="vertical-align: middle" rowspan="2">Output</th>
        <th class="text-center" style="vertical-align: middle" rowspan="2">Ket</th>
    </tr>
    <tr>
        <th class="text-center" style="vertical-align: middle">X/menit</th>
        <th class="text-center" style="vertical-align: middle">mmHg</th>
        <th class="text-center" style="vertical-align: middle">X/menit</th>
        <th class="text-center" style="vertical-align: middle">ºC</th>
        <th class="text-center" style="vertical-align: middle">X/menit</th>
    </tr>
    <?php foreach ($form['observasi'] as $v) : ?>
        <tr>
            <td><?=date('d-F-Y', strtotime(str_replace('T', ' ', $v['tgl'])))?></td>
            <td><?=date('H:i', strtotime(str_replace('T', ' ', $v['jam'])))?></td>
            <td><div style="white-space: pre-wrap;"><?=$v['cairan']?></div></td>
            <td><div style="white-space: pre-wrap;"><?=$v['tetesan']?></div></td>
            <td><div style="white-space: pre-wrap;"><?=$v['tensi']?></div></td>
            <td><div style="white-space: pre-wrap;"><?=$v['nadi']?></div></td>
            <td><div style="white-space: pre-wrap;"><?=$v['suhu']?></div></td>
            <td><div style="white-space: pre-wrap;"><?=$v['pernafasan']?></div></td>
            <td><div style="white-space: pre-wrap;"><?=$v['input']?></div></td>
            <td><div style="white-space: pre-wrap;"><?=$v['output']?></div></td>
            <td><div style="white-space: pre-wrap;"><?=$v['ket']?></div></td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>

<script>
    $(function () {
        print()
    })
</script>
</body>
</html>