<?php function harga($num)
{
    return number_format($num, 2, ',', '.');
}
?>
<script src="<?= base_url() ?>assets/bower_components/select2/dist/js/select2.full.min.js"></script>
<div class="content-wrapper">
    <section class="content">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title titlefix"> Rawat Inap Pemeriksaan</h3>
            </div><!-- /.box-header -->
            <div class="box-body">
                <div class="row">
                    <div class="col-sm-12" style="float: none; margin: 0">
                        <form id="form_diagnosis" method="post" action="<?php echo base_url() ?>RawatInap/tambah/pemeriksaan">
                            <?php if (superadmin($this->session->userdata('logged_in'))) : ?>
                                <input type="hidden" name="creator_type" value="superadmin">
                            <?php elseif (perawat($this->session->userdata('logged_in'))) : ?>
                                <input type="hidden" name="creator_type" value="perawat">
                            <?php elseif (dokter($this->session->userdata('logged_in'))) : ?>
                                <input type="hidden" name="creator_type" value="dokter">
                            <?php elseif (apoteker($this->session->userdata('logged_in'))) : ?>
                                <input type="hidden" name="creator_type" value="apoteker">
                            <?php elseif (gizi($this->session->userdata('logged_in'))) : ?>
                                <input type="hidden" name="creator_type" value="gizi">
                            <?php endif; ?>
                            <div class="modal-body pt0 pb0">
                                <div class="ptt10">
                                    <div class="row">
                                        <div class="col-sm-3">
                                            <div class="row">
                                                <?php
                                                $nakes = json_decode($pemeriksaan->nakes);
                                                $selectedPerawat = [];
                                                $selectedPerawatTamu = [];
                                                $apoteker_id = isset($nakes->a) ? $nakes->a : 0;
                                                $gizi_id = isset($nakes->g) ? $nakes->g : 0;

                                                if (isset($nakes->p)) {
                                                    $selectedPerawat = $nakes->p;
                                                }

                                                if (isset($nakes->pt)) {
                                                    $selectedPerawatTamu = $nakes->pt;
                                                }
                                                ?>
                                                <div class="col-md-12">
                                                    <input type="hidden" name="rawat_inap_id" value="<?= $rawat_inap->id ?>">
                                                    <input type="hidden" name="pasien_id" value="<?= $pasien->id ?>">
                                                    <div class="form-group">
                                                        <label for="tanggal_pemeriksaan" class="control-label">Tgl Pemeriksaan</label>
                                                        <input type="date" class="form-control margin-bottom" id="tanggal_pemeriksaan" name="tanggal_pemeriksaan" value="<?= $pemeriksaan->created_at ?>" required>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="shift" class="control-label">Shift</label>
                                                        <div class="margin-bottom">
                                                            <select class="form-control abdush-select margin-bottom" name="shift">
                                                                <option value="">-- Pilih Shift --</option>
                                                                <option value="I" <?= $pemeriksaan->shift == 'I' ? 'selected' : '' ?>>I</option>
                                                                <option value="II" <?= $pemeriksaan->shift == 'II' ? 'selected' : '' ?>>II</option>
                                                                <option value="III" <?= $pemeriksaan->shift == 'II' ? 'selected' : '' ?>>III</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="tujuan" class="control-label">Dokter</label>
                                                        <div class="margin-bottom">
                                                            <select class="form-control abdush-select margin-bottom" name="dokter_id">
                                                                <option value="" selected>-- Pilih Dokter --</option>
                                                                <?php foreach ($dokter->result() as $key => $value) { ?>
                                                                    <option value="<?php echo $value->id ?>" <?= $pemeriksaan->dokter_id == $value->id ? 'selected' : '' ?>><?php echo $value->nama; ?></option>
                                                                <?php } ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="perawat" class="control-label">Perawat</label>
                                                        <div class="" style="margin-bottom: 10px;">
                                                            <select class="form-control select2" multiple="multiple" data-placeholder="Perawat" id="perawat" name="perawat[]">
                                                                <?php foreach ($listPerawat->result() as $key => $value) {
                                                                    $selected = '';
                                                                    foreach ($selectedPerawat as $selected) {
                                                                        echo $selected->id;
                                                                        if ($value->id == $selected->id) {
                                                                            $selected = 'selected';
                                                                            break;
                                                                        }
                                                                    }
                                                                ?>
                                                                    <option value="<?php echo $value->id ?>" <?= $selected ?>><?php echo ucwords($value->nama) ?></option>
                                                                <?php } ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="perawat" class="control-label">Perawat Tamu</label>
                                                        <div class="">
                                                            <select class="form-control select2" multiple="multiple" data-placeholder="Perawat Tamu" id="perawat_tamu" name="perawat_tamu[]">
                                                                <?php foreach ($listPerawat->result() as $key => $value)
                                                                    $selected = '';
                                                                foreach ($selectedPerawatTamu as $selected) {
                                                                    echo $selected->id;
                                                                    if ($value->id == $selected->id) {
                                                                        $selected = 'selected';
                                                                        break;
                                                                    }
                                                                } { ?>
                                                                    <option value="<?php echo $value->id ?>" <?= $selected ?>><?php echo ucwords($value->nama) ?></option>
                                                                <?php } ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="tujuan" class="control-label">Apoteker</label>
                                                        <div class="margin-bottom">
                                                            <select class="form-control abdush-select margin-bottom" name="apoteker_id">
                                                                <option value="" selected>-- Pilih Apoteker --</option>
                                                                <?php foreach ($apoteker->result() as $key => $value) { ?>
                                                                    <option value="<?php echo $value->id ?>" <?= $apoteker_id = $value->id ? 'selected' : '' ?>><?php echo $value->nama; ?></option>
                                                                <?php } ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="tujuan" class="control-label">Petugas Gizi</label>
                                                        <div class="margin-bottom">
                                                            <select class="form-control abdush-select margin-bottom" name="gizi_id">
                                                                <option value="" selected>-- Pilih Petugas Gizi --</option>
                                                                <?php foreach ($gizi->result() as $key => $value) { ?>
                                                                    <option value="<?php echo $value->id ?>" <?= $gizi_id = $value->id ? 'selected' : '' ?>><?php echo $value->nama; ?></option>
                                                                <?php } ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <?php
                                                        $objective = unserialize($pemeriksaan->objektif);
                                                        ?>
                                                        <label for="subjektif" class="control-label">Deteksi Vital</label>
                                                        <div class="row">
                                                            <div class="col-sm-6 col-md-6 col-lg-6 form-group">
                                                                <label for="td" class="control-label">TD</label>
                                                                <div class="input-group">
                                                                    <input type="text" class="form-control requirable" name="objektif[td]" id="td" value="<?= isset($objective['td']) ? $objective['td'] : '' ?>">
                                                                    <span class="input-group-addon">mmHg</span>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-6 col-md-6 col-lg-6 form-group">
                                                                <label for="inputEmail3" class="control-label">R</label>
                                                                <div class="input-group">
                                                                    <input type="text" class="form-control requirable" name="objektif[r]" id="r" value="<?= isset($objective['r']) ? $objective['r'] : '' ?>">
                                                                    <span class="input-group-addon">K/Min</span>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-6 col-md-6 col-lg-6 form-group">
                                                                <label for="inputEmail3" class="control-label">BB</label>
                                                                <div class="input-group">
                                                                    <input type="text" class="form-control requirable" id="bb" name="objektif[bb]" onkeyup="set_bmi()" value="<?= isset($objective['bb']) ? $objective['bb'] : '' ?>">
                                                                    <span class="input-group-addon">Kg</span>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-6 col-md-6 col-lg-6 form-group">
                                                                <label for="inputEmail3" class="control-label">N</label>
                                                                <div class="input-group">
                                                                    <input type="text" class="form-control requirable" name="n" id="objektif[n]" value="<?= isset($objective['n']) ? $objective['n'] : '' ?>">
                                                                    <span class="input-group-addon">K/Min</span>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-6 col-md-6 col-lg-6 form-group">
                                                                <label for="inputEmail3" class="control-label">S</label>
                                                                <div class="input-group">
                                                                    <input type="text" class="form-control requirable" id="s" name="objektif[s]" value="<?= isset($objective['s']) ? $objective['s'] : '' ?>">
                                                                    <span class="input-group-addon">'0</span>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-6 col-md-6 col-lg-6 form-group">
                                                                <label for="inputEmail3" class="control-label">TB</label>
                                                                <div class="input-group">
                                                                    <input type="text" class="form-control requirable" id="tb" name="objektif[tb]" onkeyup="set_bmi()" value="<?= isset($objective['tb']) ? $objective['tb'] : '' ?>">
                                                                    <span class="input-group-addon">cm</span>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-6 col-md-6 col-lg-6 form-group">
                                                                <label for="inputEmail3" class="control-label">BMI</label>
                                                                <div class="input-group">
                                                                    <input type="text" class="form-control requirable" name="objektif[bmi]" id="bmi" value="<?= isset($objective['bmi']) ? $objective['bmi'] : '' ?>">

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <?php
                                                    $objective = json_decode($pemeriksaan->objectives);
                                                    $subjektif = json_decode($pemeriksaan->subjectives);
                                                    ?>
                                                    <label for="subjektif" class="control-label" style="margin-top: 10px">Subjektif</label>
                                                    <div class="form-group">
                                                        <label for="subjektif" class="control-label">Perawat</label>
                                                        <textarea class="form-control margin-bottom" id="subjektif" name="subjektif" <?= superadmin($this->session->userdata('logged_in')) || perawat($this->session->userdata('logged_in')) ? '' : 'disabled' ?>>
                                                         <?= isset($subjektif->p) ? $subjektif->p : '' ?>
                                                    </textarea>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="subjektif" class="control-label">Dokter</label>
                                                        <textarea class="form-control margin-bottom" id="subjektif_d" name="subjektif_d" <?= superadmin($this->session->userdata('logged_in')) || dokter($this->session->userdata('logged_in')) ? '' : 'disabled' ?>>
                                                        <?= isset($subjektif->d) ? $subjektif->d : '' ?>
                                                    </textarea>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="subjektif" class="control-label">Apoteker</label>
                                                        <textarea class="form-control margin-bottom" id="subjektif_a" name="subjektif_a" <?= superadmin($this->session->userdata('logged_in')) || apoteker($this->session->userdata('logged_in')) ? '' : 'disabled' ?>>
                                                        <?= isset($subjektif->a) ? $subjektif->a : '' ?>
                                                    </textarea>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="subjektif" class="control-label">Gizi</label>
                                                        <textarea class="form-control margin-bottom" id="subjektif_g" name="subjektif_g" <?= superadmin($this->session->userdata('logged_in')) || gizi($this->session->userdata('logged_in')) ? '' : 'disabled' ?>>
                                                        <?= isset($subjektif->g) ? $subjektif->g : '' ?>
                                                    </textarea>
                                                    </div>
                                                    <label for="objektif" class="control-label" style="margin-top: 10px">Objektif</label>
                                                    <div class="form-group">
                                                        <label for="objektif" class="control-label">Perawat</label>
                                                        <textarea class="form-control margin-bottom" id="objektif_p" name="objektif_p" <?= superadmin($this->session->userdata('logged_in')) || perawat($this->session->userdata('logged_in')) ? '' : 'disabled' ?>>
                                                        <?= isset($subjektif->p) ? $subjektif->p : '' ?>
                                                    </textarea>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="objektif" class="control-label">Dokter</label>
                                                        <textarea class="form-control margin-bottom" id="objektif_d" name="objektif_d" <?= superadmin($this->session->userdata('logged_in')) || dokter($this->session->userdata('logged_in')) ? '' : 'disabled' ?>>
                                                        <?= isset($subjektif->d) ? $subjektif->d : '' ?>
                                                    </textarea>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="objektif" class="control-label">Apoteker</label>
                                                        <textarea class="form-control margin-bottom" id="objektif_a" name="objektif_a" <?= superadmin($this->session->userdata('logged_in')) || apoteker($this->session->userdata('logged_in')) ? '' : 'disabled' ?>>
                                                        <?= isset($subjektif->a) ? $subjektif->a : '' ?>
                                                    </textarea>
                                                    </div>
                                                    <div class="">
                                                        <label for="objektif" class="control-label">Gizi</label>
                                                        <textarea class="form-control margin-bottom" id="objektif_g" name="objektif_g" <?= superadmin($this->session->userdata('logged_in')) || gizi($this->session->userdata('logged_in')) ? '' : 'disabled' ?>>
                                                        <?= isset($subjektif->g) ? $subjektif->g : '' ?>
                                                    </textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <?php
                                            $assesment = json_decode($pemeriksaan->assessments);
                                            $plan = json_decode($pemeriksaan->plans);
                                            ?>
                                            <label for="asesmen" class="control-label" style="margin-top: 10px">Asesmen</label>
                                            <div class="form-group">
                                                <label for="asesmen" class="control-label">Perawat</label>
                                                <textarea class="form-control margin-bottom" id="asesmen" name="asesmen" <?= superadmin($this->session->userdata('logged_in')) || perawat($this->session->userdata('logged_in')) ? '' : 'disabled' ?>>
                                                <?= isset($assesment->p) ? $assesment->p : '' ?>
                                            </textarea>
                                            </div>
                                            <div class="form-group">
                                                <label for="asesmen" class="control-label">Dokter</label>
                                                <textarea class="form-control margin-bottom" id="asesmen_d" name="asesmen_d" <?= superadmin($this->session->userdata('logged_in')) || dokter($this->session->userdata('logged_in')) ? '' : 'disabled' ?>>
                                                <?= isset($assesment->d) ? $assesment->d : '' ?>
                                            </textarea>
                                            </div>
                                            <div class="form-group">
                                                <label for="asesmen" class="control-label">Apoteker</label>
                                                <textarea class="form-control margin-bottom" id="asesmen_a" name="asesmen_a" <?= superadmin($this->session->userdata('logged_in')) || apoteker($this->session->userdata('logged_in')) ? '' : 'disabled' ?>>
                                                <?= isset($assesment->a) ? $assesment->a : '' ?>
                                            </textarea>
                                            </div>
                                            <div class="form-group">
                                                <label for="asesmen" class="control-label">Gizi</label>
                                                <textarea class="form-control margin-bottom" id="asesmen_g" name="asesmen_g" <?= superadmin($this->session->userdata('logged_in')) || gizi($this->session->userdata('logged_in')) ? '' : 'disabled' ?>>
                                                <?= isset($assesment->g) ? $assesment->g : '' ?>
                                            </textarea>
                                            </div>
                                            <label for="asesmen" class="control-label" style="margin-top: 10px">Plan</label>
                                            <div class="form-group">
                                                <label for="plan" class="control-label">Perawat</label>
                                                <textarea class="form-control margin-bottom" id="plan" name="plan" <?= superadmin($this->session->userdata('logged_in')) || perawat($this->session->userdata('logged_in')) ? '' : 'disabled' ?>>
                                                <?= isset($plan->p) ? $plan->p : '' ?>
                                            </textarea>
                                            </div>
                                            <div class="form-group">
                                                <label for="plan" class="control-label">Dokter</label>
                                                <textarea class="form-control margin-bottom" id="plan_d" name="plan_d" <?= superadmin($this->session->userdata('logged_in')) || dokter($this->session->userdata('logged_in')) ? '' : 'disabled' ?>>
                                                <?= isset($plan->d) ? $plan->d : '' ?>
                                            </textarea>
                                            </div>
                                            <div class="form-group">
                                                <label for="plan" class="control-label">Apoteker</label>
                                                <textarea class="form-control margin-bottom" id="plan_a" name="plan_a" <?= superadmin($this->session->userdata('logged_in')) || apoteker($this->session->userdata('logged_in')) ? '' : 'disabled' ?>>
                                                <?= isset($plan->a) ? $plan->a : '' ?>
                                            </textarea>
                                            </div>
                                            <div class="form-group">
                                                <label for="plan" class="control-label">Gizi</label>
                                                <textarea class="form-control margin-bottom" id="plan_g" name="plan_g" <?= superadmin($this->session->userdata('logged_in')) || gizi($this->session->userdata('logged_in')) ? '' : 'disabled' ?>>
                                                <?= isset($plan->g) ? $plan->g : '' ?>
                                            </textarea>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label for="tujuan" class="control-label">Penyakit</label>
                                                        <div style="display: flex">
                                                            <select class="form-control select2 margin-bottom" multiple="multiple" name="penyakit[]" data-placeholder="Pilih penyakit" style="flex: 1">
                                                                <?php foreach ($penyakit as $key => $value) {
                                                                    $selected = '';
                                                                    foreach ($ri_diagnosis_penyakit as $diagnosis) {
                                                                        if ($diagnosis->penyakit_id == $value->id) {
                                                                            $selected = 'selected';
                                                                        }
                                                                    }
                                                                ?>
                                                                    <option value="<?php echo $value->id ?>" <?= $selected ?>><?php echo $value->kode . ' - ' . $value->nama ?></option>
                                                                <?php } ?>
                                                            </select>
                                                            <button class="btn btn-sm btn-success" style="margin-left: 10px" onclick="x()" data-toggle="modal">
                                                                <i class="fa fa-plus"></i>
                                                            </button>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="diagnosis" class="control-label">Diagnosis</label>
                                                        <input type="text" class="form-control" id="diagnosis" name="diagnosis" placeholder="Diagnosis" value="<?= $pemeriksaan->diagnosis ?>">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="tindakan[]" class="control-label">Tindakan</label>
                                                        <div class="" style="margin-bottom: 10px;">
                                                            <select class="form-control select2" multiple="multiple" data-placeholder="Tindakan" id="tindakan" name="tindakan[]">
                                                                <?php foreach ($tindakan as $key => $value) {
                                                                    $selected = '';
                                                                    foreach ($ri_tindakan as $ritindakan) {
                                                                        if ($ritindakan->tindakan_id == $value->id) {
                                                                            $selected = 'selected';
                                                                        }
                                                                    }
                                                                ?>
                                                                    <option value="<?php echo $value->id ?>" <?= $selected ?>><?php echo $value->nama . " - Rp." . harga($value->tarif_pasien) ?></option>
                                                                <?php } ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="col-sm-12" style="margin-bottom: 10px; height: 3px !important;">
                                                            <hr style="margin: 0 !important;">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="perawat" class="control-label">Dokter Anestesi</label>
                                                        <div class="" style="margin-bottom: 10px;">
                                                            <select class="form-control select2" data-placeholder="Dokter Anestesi" id="dokter_anestesi" name="dokter_anestesi">
                                                                <option value="">--Pilih dokter anestesi--</option>
                                                                <?php foreach ($dokter->result() as $key => $value) { ?>
                                                                    <option value="<?php echo $value->id ?>" <?= $value->id == $dokter_anestesi_id ? 'selected' : '' ?>><?php echo ucwords($value->nama) ?></option>
                                                                <?php } ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="tindakan_anestesi[]" class="control-label">Tindakan Dokter Anestesi</label>
                                                        <div class="" style="margin-bottom: 10px;">
                                                            <select class="form-control select2" multiple="multiple" data-placeholder="Tindakan" id="tindakan_anestesi" name="tindakan_anestesi[]">
                                                                <?php foreach ($tindakan as $key => $value) {
                                                                    $selected = '';
                                                                    foreach ($ri_tindakan_anestesi as $ri_tindakan_a) {
                                                                        if ($ri_tindakan_a->tindakan_id == $value->id) {
                                                                            $selected = 'selected';
                                                                        }
                                                                    }
                                                                ?>
                                                                    <option value="<?php echo $value->id ?>" <?= $selected ?>><?php echo $value->nama . " - Rp." . harga($value->tarif_pasien) ?></option>
                                                                <?php } ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="perawat" class="control-label">Dokter Tamu</label>
                                                        <div class="" style="margin-bottom: 10px;">
                                                            <select class="form-control select2" multiple="multiple" data-placeholder="Dokter Tamu" id="dokter_tamu" name="dokter_tamu[]">
                                                                <?php foreach ($dokter->result() as $key => $value) {
                                                                    $selected = '';
                                                                    if (in_array($value->id, explode(',', $dokcter_tamu_ids))) {
                                                                        $selected = 'selected';
                                                                    }
                                                                ?>
                                                                    <option value="<?php echo $value->id ?>" <?= $selected ?>><?php echo ucwords($value->nama) ?></option>
                                                                <?php } ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="tindakan_tamu[]" class="control-label">Tindakan Dokter Tamu</label>
                                                        <div class="" style="margin-bottom: 10px;">
                                                            <select class="form-control select2" multiple="multiple" data-placeholder="Tindakan" id="tindakan_tamu" name="tindakan_tamu[]">
                                                                <?php foreach ($tindakan as $key => $value) {
                                                                    $selected = '';
                                                                    foreach ($ri_tindakan_dokter_tamu as $ri_tindakan_a) {
                                                                        if ($ri_tindakan_a->tindakan_id == $value->id) {
                                                                            $selected = 'selected';
                                                                        }
                                                                    }
                                                                ?>
                                                                    <option value="<?php echo $value->id ?>" <?= $selected ?>><?php echo $value->nama . " - Rp." . harga($value->tarif_pasien) ?></option>
                                                                <?php } ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="text-muted well well-sm no-shadow" style="display: block;">
                                                        <div class="form-group" style=" margin-top: 4px">
                                                            <label for="hasil_penunjang_laboratorium" class="col-sm-4 control-label">
                                                                KONSULTASI DOKTER
                                                            </label>
                                                            <!-- <button type="button" class="btn btn-success btn-sm pull-right" id="btn-add-konsul" style="margin-right: 14px">
                                                                <span class="fa fa-plus"></span> Tambah Konsul
                                                            </button> -->
                                                        </div>
                                                        <div class="row" style="margin-bottom: 14px">
                                                            <div class="col-md-12">
                                                                <input type="hidden" name="max_konsul_pemeriksaan_test" id="max_konsul_pemeriksaan_test" value="0">
                                                                <table class="tbl-konsul table table-condensed table-bordered" style="margin-bottom: 8px; table-layout:fixed;">
                                                                    <tbody>
                                                                        <tr>
                                                                            <th style="width: 50%;">Tarif Tindakan</th>
                                                                            <th>Dokter</th>
                                                                            <th style="width: 46px"></th>
                                                                        </tr>
                                                                        <?php
                                                                            $index = 1;
                                                                        foreach($ri_detail_tindakan_konsul as $riTindakanKonsul) :?>
                                                                        <tr id="row-konsul-<?=$index?>">
                                                                            <td style="width: 50%;">
                                                                                <select class="form-control select2" multiple="multiple" name="tindakan_<?=$index?>[]" id="tindakan_<?=$index?>" data-placeholder="Pilih tindakan" style="width: 100%;" required>
                                                                                    <?php foreach ($tindakan as $key => $value) { 
                                                                                         $selected = '';
                                                                                         if($value->id == $riTindakanKonsul->tarif_tindakan_id) {
                                                                                             $selected = 'selected';
                                                                                         }
                                                                                    ?>
                                                                                        <option value="<?= $value->id; ?>" <?=$selected?>><?= $value->nama . " - Rp." . number_format($value->tarif_pasien, 2, ',', '.') ?></option>
                                                                                    <?php } ?>
                                                                                </select>
                                                                            </td>
                                                                            <td>
                                                                                <select class="form-control abdush-select" name="dokter_<?=$index?>" style="width: 100%;" required>
                                                                                    <option value="" selected>-- Pilih Dokter --</option>
                                                                                    <?php foreach ($dokter->result() as $key => $value) { 
                                                                                        $selected = '';
                                                                                        if($value->id == $riTindakanKonsul->dokter_id) {
                                                                                            $selected = 'selected';
                                                                                        }
                                                                                    ?>
                                                                                        <option value="<?php echo $value->id ?>" <?=$selected?>><?php echo $value->nama; ?></option>
                                                                                    <?php } ?>
                                                                                </select>
                                                                            </td>
                                                                            <td style="width: 46px">
                                                                                <button type="button" class="btn btn-sm btn-danger" onclick="delete_konsul($<?=$index?>)">
                                                                                    <span class="fa fa-trash"></span>
                                                                                </button>
                                                                            </td>
                                                                        </tr>                                                                        
                                                                        <?php endforeach;?>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                        <textarea class="form-control margin-bottom" id="catatan_konsultasi" name="catatan_konsultasi" <?= superadmin($this->session->userdata('logged_in')) || perawat($this->session->userdata('logged_in')) ? '' : 'disabled' ?>></textarea>
                                                    </div>
                                                    <?php if (!(gizi($this->session->userdata('logged_in')) || perawat($this->session->userdata('logged_in')))) : ?>
                                                        <div class="form-group">
                                                            <label for="tujuan" class="control-label">Resep</label>
                                                            <div class="" style="display: flex">
                                                                <button type="submit" name="resep" value="1" style="margin-right: 8px" data-loading-text="Menyimpan..." class="btn btn-success pull-right">Input Resep</button>
                                                            </div>
                                                        </div>
                                                    <?php endif; ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- <div class="box-footer">
                                <button type="submit" id="form_diagnosisbtn" data-loading-text="Menyimpan..." class="btn btn-info pull-right">Simpan</button>
                            </div> -->
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<script>
    $('.select2').select2()
</script>