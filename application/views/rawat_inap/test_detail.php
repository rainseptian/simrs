<?php
function harga($num)
{
    return number_format($num, 2, ',', '.');
}
?>
<style media="screen">
    .select2-container {
        width: 100% !important;
    }
    span.select2-container {
        z-index:10050;
    }
    .impbtn{position: absolute;right: 14px;top: 13px;}
    .impbtnview{position: absolute;right: 10px;top: 13px;}
    .impbtnview20{position: absolute;right: 20px;top: 11px;}
    .impbtnview-tindakan{position: absolute;left: 200px;top: 6px;}
    .margin-bottom {margin-bottom:15px;}
    .nav.nav-tabs {
        padding-right: 200px;
    }

    #pengkajian_awal * .content-wrapper {
        margin: 0 !important;
    }
    #pengkajian_awal * .content-header {
        display: none !important;
    }
    #assesment_gizi * label {
        font-weight: normal !important;
    }
</style>
<link rel="stylesheet"
      href="<?php echo base_url() ?>assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
<link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/autocomplete/style-gue.css">
<style type="text/css">
    @import('https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.0/css/bootstrap.min.css');
</style>

<?php $currency_symbol = 'Rp'; ?>
<div class="content-wrapper">
    <section class="content">
        <div class="box box-primary">
            <div class="row">
                <div class="box-header">
                    <h1 class="text-center">Testing</h1>
                </div>
                <div class="box-body pb0">
                    <div class="col-lg-2 col-md-2 col-sm-3 text-center">
                        <img width="115" height="115" class="" src="<?php echo base_url() ?>assets/img/logo.png" alt="No Image">
                    </div>
                    <div class="col-md-10 col-lg-10 col-sm-9">
                        <div class="table-responsive">
                            <table class="table table-striped mb0 font13">
                                <tbody>
                                <tr>
                                    <th class="bozerotop">Nama</th>
                                    <td class="bozerotop"><?=$pasien->nama?></td>
                                    <th class="bozerotop">Penanggung Jawab</th>
                                    <td class="bozerotop"><?=$rawat_inap->penanggung_jawab?></td>
                                </tr>
                                <tr>
                                    <th class="bozerotop">Jenis Kelamin</th>
                                    <td class="bozerotop"><?=$pasien->jk == 'L' ? 'Laki-laki' : 'Perempuan'?></td>
                                    <th class="bozerotop">Usia</th>
                                    <td class="bozerotop"><?=$pasien->usia?></td>
                                </tr>
                                <tr>
                                    <th class="bozerotop">Alamat</th>
                                    <td class="bozerotop"><?=$pasien->alamat?></td>
                                    <th class="bozerotop">No Telp</th>
                                    <td class="bozerotop"><?=$pasien->telepon?></td>
                                </tr>
                                <tr>
                                    <th class="bozerotop">Tgl Daftar</th>
                                    <td class="bozerotop"><?=$rawat_inap->created_at?></td>
                                    <th class="bozerotop">Bed</th>
                                    <td class="bozerotop"><?=$rawat_inap->bed_name.' - '.$rawat_inap->bedgroup?></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="col-md-12 col-lg-12 col-sm-12 pull-right">
                        <form method="post" action="<?=base_url('RawatInap/bolehPulang/'.$rawat_inap->id)?>" style="float: right">
                            <input type="hidden" name="total_biaya" id="total_biaya">
                            <button type="submit" class="btn btn-sm btn-success"
                                    onclick="return confirm('Apakah pasien ini sudah boleh pulang?');">
                                <i class="fa fa-check"></i> Boleh Pulang
                            </button>
                        </form>
                        <form method="post" action="<?=base_url('TransferPasien/add/3')?>" style="float: right; margin-right: 5px">
                            <input type="hidden" class="form-control" name="rawat_inap_id" value="<?=$rawat_inap->id?>">
                            <button type="submit" class="btn btn-sm btn-warning">
                                <i class="fa fa-random"></i> Transfer Bangsal
                            </button>
                        </form>
                        <form method="post" action="<?=base_url('TransferPasien/add/4')?>" style="float: right; margin-right: 5px">
                            <input type="hidden" class="form-control" name="rawat_inap_id" value="<?=$rawat_inap->id?>">
                            <button type="submit" class="btn btn-sm btn-danger">
                                <i class="fa fa-baby-carriage"></i> Transfer ke Ruang Bersalin
                            </button>
                        </form>
                        <form method="post" action="<?=base_url('TransferPasien/add/2')?>" style="float: right; margin-right: 5px">
                            <input type="hidden" class="form-control" name="rawat_inap_id" value="<?=$rawat_inap->id?>">
                            <button type="submit" class="btn btn-sm btn-primary">
                                <i class="fa fa-cut"></i> Transfer ke Ruang Operasi
                            </button>
                        </form>
                    </div>
                </div>
            </div>
            <div>
                <div class="box border0">
                    <div style="background: #dadada; height: 1px; width: 100%; clear: both; margin-top:5px;"></div>
                    <div class="nav-tabs-custom border0" id="tabs">
                        <ul class="nav nav-tabs">
                            <li <?=$tab == 'persetujuan_ranap' ? 'class="active"' : ''?>>
                                <a href="#persetujuan_ranap" data-toggle="tab" aria-expanded="true"><i
                                            class="fas fa-file-prescription"></i> Persetujuan Ranap
                                </a>
                            </li>
                            <li <?=$tab == 'informed_consent' ? 'class="active"' : ''?>>
                                <a href="#informed_consent" data-toggle="tab" aria-expanded="true"><i
                                            class="fas fa-file-prescription"></i> Informed Consent
                                </a>
                            </li>
                            <li <?=$tab == 'form_edukasi' ? 'class="active"' : ''?>>
                                <a href="#form_edukasi" data-toggle="tab" aria-expanded="true"><i
                                            class="fas fa-file-prescription"></i> Form Edukasi
                                </a>
                            </li>
                            <li <?=$tab == 'pengkajian_awal' ? 'class="active"' : ''?>>
                                <a href="#pengkajian_awal" data-toggle="tab" aria-expanded="true"><i
                                            class="fas fa-file-prescription"></i> Pengkajian Awal
                                </a>
                            </li>
                            <li <?=$tab == 'pemeriksaan' ? 'class="active"' : ''?>>
                                <a href="#pemeriksaan" data-toggle="tab" aria-expanded="true"><i
                                            class="fas fa-file-prescription"></i> Pemeriksaan Harian
                                </a>
                            </li>
                            <li <?=$tab == 'assesment_gizi' ? 'class="active"' : ''?>>
                                <a href="#assesment_gizi" data-toggle="tab" aria-expanded="true"><i
                                            class="fas fa-file-prescription"></i> Assesment Gizi
                                </a>
                            </li>
                            <li <?=$tab == 'skrining_jatuh' ? 'class="active"' : ''?>>
                                <a href="#skrining_jatuh" data-toggle="tab" aria-expanded="true"><i
                                            class="fas fa-file-prescription"></i> Skrining Jatuh
                                </a>
                            </li>
                            <li <?=$tab == 'lembar_observasi' ? 'class="active"' : ''?>>
                                <a href="#lembar_observasi" data-toggle="tab" aria-expanded="true"><i
                                            class="fas fa-file-prescription"></i> Lembar Observasi
                                </a>
                            </li>
                            <li <?=$tab == 'ppt' ? 'class="active"' : ''?>>
                                <a href="#ppt" data-toggle="tab" aria-expanded="true"><i
                                            class="fas fa-file-prescription"></i> PPT
                                </a>
                            </li>
                            <li <?=$tab == 'resume_medis' ? 'class="active"' : ''?>>
                                <a href="#resume_medis" data-toggle="tab" aria-expanded="true"><i
                                            class="fas fa-file-prescription"></i> Resume Medis
                                </a>
                            </li>
                            <li <?=$tab == 'surveilans' ? 'class="active"' : ''?>>
                                <a href="#surveilans" data-toggle="tab" aria-expanded="true"><i
                                            class="fas fa-file-prescription"></i> Surveilans
                                </a>
                            </li>
                            <li <?=$tab == 'alergi' ? 'class="active"' : ''?>>
                                <a href="#alergi" data-toggle="tab" aria-expanded="true"><i
                                            class="fas fa-file-prescription"></i> Manifestasi Alergi
                                </a>
                            </li>
                            <li <?=$tab == 'medikasi' ? 'class="active"' : ''?>>
                                <a href="#medikasi" data-toggle="tab" aria-expanded="true"><i
                                            class="fas fa-file-prescription"></i> Lembar Medikasi
                                </a>
                            </li>
                            <li <?=$tab == 's' ? 'class="active"' : ''?>>
                                <a href="#pemeriksaan" data-toggle="tab" aria-expanded="true"><i
                                            class="fas fa-file-prescription"></i> Laporan Anestesi
                                </a>
                            </li>
                            <li <?=$tab == 'a' ? 'class="active"' : ''?>>
                                <a href="#pemeriksaan" data-toggle="tab" aria-expanded="true"><i
                                            class="fas fa-file-prescription"></i> Laporan Operasi
                                </a>
                            </li>

                            <li <?=$tab == 'bahan_habis_pakai' ? 'class="active"' : ''?>>
                                <a href="#bahan_habis_pakai" data-toggle="tab" aria-expanded="true"><i
                                            class="far fa-calendar-check"></i> Bahan Habis Pakai
                                </a>
                            </li>

                            <li <?=$tab == 'biaya' ? 'class="active"' : ''?>>
                                <a href="#biaya" data-toggle="tab" aria-expanded="true"><i
                                            class="fas fa-donate"></i> Biaya
                                </a>
                            </li>

                            <li <?=$tab == 'radiologi' ? 'class="active"' : ''?>>
                                <a href="#radiologi" data-toggle="tab" aria-expanded="true"><i
                                            class="fa fas fa-x-ray"></i> Radiologi
                                </a>
                            </li>

                            <li <?=$tab == 'lab' ? 'class="active"' : ''?>>
                                <a href="#lab" data-toggle="tab" aria-expanded="true"><i
                                            class="fa fas fa-flask"></i> Laboratorium
                                </a>
                            </li>

                            <li <?=$tab == 'bill' ? 'class="active"' : ''?>>
                                <a href="#bill" class="bill" data-toggle="tab" aria-expanded="true"><i
                                            class="fas fa-file-invoice-dollar"></i> Bill
                                </a>
                            </li>

                        </ul>

                        <div class="tab-content">
                            <?php $this->load->view('rawat_inap/tab_contents/persetujuan_ranap', ['show_action' => false]); ?>
                            <?php $this->load->view('rawat_inap/tab_contents/informed_consent', ['show_action' => false]); ?>
                            <?php $this->load->view('rawat_inap/tab_contents/form_edukasi', ['show_action' => false]); ?>
                            <?php $this->load->view('rawat_inap/tab_contents/pengkajian_awal', ['show_action' => false]); ?>
                            <?php $this->load->view('rawat_inap/tab_contents/pemeriksaan_test', ['show_action' => true]); ?>
                            <?php $this->load->view('rawat_inap/tab_contents/assesment_gizi', ['show_action' => true]); ?>
                            <?php $this->load->view('rawat_inap/tab_contents/skrining_jatuh', ['show_action' => true]); ?>
                            <?php $this->load->view('rawat_inap/tab_contents/lembar_observasi', ['show_action' => true]); ?>
                            <?php $this->load->view('rawat_inap/tab_contents/ppt', ['show_action' => true]); ?>
                            <?php $this->load->view('rawat_inap/tab_contents/resume_medis', ['show_action' => true]); ?>
                            <?php $this->load->view('rawat_inap/tab_contents/surveilans', ['show_action' => true]); ?>
                            <?php $this->load->view('rawat_inap/tab_contents/alergi', ['show_action' => true]); ?>
                            <?php $this->load->view('rawat_inap/tab_contents/medikasi', ['show_action' => true]); ?>
                            <?php $this->load->view('rawat_inap/tab_contents/bahan_habis_pakai', ['show_action' => true]); ?>
                            <?php $this->load->view('rawat_inap/tab_contents/biaya', ['show_action' => true]); ?>
                            <?php $this->load->view('rawat_inap/tab_contents/radiologi', ['show_action' => true]); ?>
                            <?php $this->load->view('rawat_inap/tab_contents/lab', ['show_action' => true]); ?>
                            <?php $this->load->view('rawat_inap/tab_contents/bill', ['show_action' => true]); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<?php $this->load->view('rawat_inap/modals/tambah_bahan_habis_pakai'); ?>
<?php $this->load->view('rawat_inap/modals/tambah_biaya'); ?>
<?php $this->load->view('rawat_inap/modals/tambah_pemeriksaan_test'); ?>
<?php $this->load->view('rawat_inap/modals/tambah_penyakit'); ?>
<?php $this->load->view('rawat_inap/modals/tambah_resep'); ?>
<?php $this->load->view('rawat_inap/modals/tambah_tindakan'); ?>
<?php $this->load->view('rawat_inap/modals/tambah_makanan'); ?>
<?php $this->load->view('rawat_inap/modals/tambah_edukasi'); ?>
<?php $this->load->view('rawat_inap/modals/tambah_observasi'); ?>
<?php $this->load->view('rawat_inap/modals/tambah_ppt'); ?>
<?php $this->load->view('rawat_inap/modals/tambah_surveilans'); ?>
<?php $this->load->view('rawat_inap/modals/tambah_alergi'); ?>

<?php $this->load->view('rawat_inap/detail_js'); ?>