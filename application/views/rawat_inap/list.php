<?php
$currency_symbol = 'Rp';
?>

<link rel="stylesheet"
      href="<?= base_url() ?>assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
<link rel="stylesheet" href="<?= base_url() ?>assets/plugins/autocomplete/style-gue.css">
<style type="text/css">
    @import('https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.0/css/bootstrap.min.css');
</style>

<style type="text/css">
    .table-responsive {overflow-x: inherit;}
</style>
<div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title titlefix"> Pasien Rawat Inap</h3>
                        <div class="box-tools pull-right">
                            <a  href="<?= base_url() ?>RawatInap/listBolehPulang" class="btn btn-primary btn-sm"><i class="fa fa-reorder"></i> Pasien Boleh Pulang</a>
                        </div>
                    </div><!-- /.box-header -->

                    <div class="box-body">
                        <table class="table table-striped table-bordered table-hover data-table display" id="printable_table" cellspacing="0" width="100%">
                            <thead>
                            <tr>
                                <th>No</th>
                                <th>Tgl Daftar</th>
                                <th>No RM</th>
                                <th>Nama Pasien</th>
                                <th>Alamat</th>
                                <th>Nama Dokter</th>
                                <th>Dari</th>
                                <th>Bed</th>
                                <th>Tipe Pasien</th>
                                <th>Aksi</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $no = 1;
                            foreach ($list as $r) : ?>
                                <tr>
                                    <td>
                                        <a href="<?= base_url(); ?>RawatInap/test_detail/<?= $r['id']; ?>">
                                            <?=$no++?>
                                        </a>
                                    </td>
                                    <td><?=date('d-F-Y H:i', strtotime($r['created_at']))?></td>
                                    <td><?=$r['no_rm']?></td>
                                    <td><?=$r['nama_pasien']?></td>
                                    <td><?=$r['alamat']?></td>
                                    <td><?=$r['nama_dokter']?></td>
                                    <td><?=$r['dari']?></td>
                                    <td>
                                        <?php if ($r['current_place_type'] == 'bangsal') : ?>
                                            <?=$r['bed_name'].' - '.$r['bedgroup']?>
                                        <?php else : ?>
                                            <span class="label label-info"><?=$r['tujuan']?></span>
                                        <?php endif; ?>
                                    </td>
                                    <td><?=$r['tipe_pasien']?></td>
                                    <td>
                                        <a href="<?= base_url(); ?>RawatInap/detail/<?= $r['id']; ?>">
                                            <button type="button" class="btn btn-primary btn-block"><i class="fa fa-arrows"></i> Periksa Harian</button>
                                        </a>
                                        <?php if ($is_superadmin) : ?>
                                            <a href="<?= base_url(); ?>RawatInap/hapusPasien/<?= $r['id']; ?>" onclick="return confirm('Yakin hapus pasien ini?')" style="margin-top: 4px" type="button" class="btn btn-danger btn-block">
                                                <i class="fa fa-trash"></i> Hapus
                                            </a>
                                        <?php endif; ?>
                                        <?php if ($r['current_place_type'] == 'bangsal') : ?>

                                        <?php else : ?>
<!--                                            <form method="post" action="--><?//=base_url('RawatInap/bolehPulang/'.$r['id'])?><!--">-->
<!--                                                <input type="hidden" name="total_biaya" id="total_biaya">-->
<!--                                                <button type="submit" class="btn btn-success"-->
<!--                                                        onclick="return confirm('Apakah pasien ini sudah boleh pulang?');">-->
<!--                                                    <i class="fa fa-check"></i> Boleh Pulang-->
<!--                                                </button>-->
<!--                                            </form>-->
                                        <?php endif; ?>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
