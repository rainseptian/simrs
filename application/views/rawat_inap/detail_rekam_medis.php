<?php
function harga($num)
{
    return number_format($num, 2, ',', '.');
}
?>
<style media="screen">
    .select2-container {
        width: 100% !important;
    }

    .impbtn{position: absolute;right: 14px;top: 13px;}
    .impbtnview{position: absolute;right: 10px;top: 13px;}
    .impbtnview20{position: absolute;right: 20px;top: 11px;}
    .impbtnview-tindakan{position: absolute;left: 200px;top: 6px;}
    .margin-bottom {margin-bottom:15px;}
</style>
<link rel="stylesheet"
      href="<?php echo base_url() ?>assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
<link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/autocomplete/style-gue.css">
<style type="text/css">
    @import('https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.0/css/bootstrap.min.css');
</style>

<?php $currency_symbol = 'Rp'; ?>
<div class="content-wrapper">
    <section class="content">
        <div class="box box-primary">
            <div class="row">
                <div class="box-body pb0">
                    <div class="col-lg-2 col-md-2 col-sm-3 text-center">
                        <img width="115" height="115" class="" src="<?php echo base_url() ?>assets/img/logo.png" alt="No Image">
                    </div>
                    <div class="col-md-10 col-lg-10 col-sm-9">
                        <div class="table-responsive">
                            <table class="table table-striped mb0 font13">
                                <tbody>
                                <tr>
                                    <th class="bozerotop">Nama</th>
                                    <td class="bozerotop"><?=$pasien->nama?></td>
                                    <th class="bozerotop">Penanggung Jawab</th>
                                    <td class="bozerotop"><?=$rawat_inap->penanggung_jawab?></td>
                                </tr>
                                <tr>
                                    <th class="bozerotop">Jenis Kelamin</th>
                                    <td class="bozerotop"><?=$pasien->jk == 'L' ? 'Laki-laki' : 'Perempuan'?></td>
                                    <th class="bozerotop">Usia</th>
                                    <td class="bozerotop"><?=$pasien->usia?></td>
                                </tr>
                                <tr>
                                    <th class="bozerotop">Alamat</th>
                                    <td class="bozerotop"><?=$pasien->alamat?></td>
                                    <th class="bozerotop">No Telp</th>
                                    <td class="bozerotop"><?=$pasien->telepon?></td>
                                </tr>
                                <tr>
                                    <th class="bozerotop">Tgl Daftar</th>
                                    <td class="bozerotop"><?=$rawat_inap->created_at?></td>
                                    <th class="bozerotop">Bed</th>
                                    <td class="bozerotop"><?=$rawat_inap->bed_name.' - '.$rawat_inap->bedgroup?></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div>
                <div class="box border0">
                    <div style="background: #dadada; height: 1px; width: 100%; clear: both; margin-top:5px;"></div>
                    <div class="nav-tabs-custom border0" id="tabs">
                        <ul class="nav nav-tabs">
                            <li class="active">
                                <a href="#pemeriksaan" data-toggle="tab" aria-expanded="true"><i
                                            class="fas fa-file-prescription"></i> Pemeriksaan
                                </a>
                            </li>
                            <li>
                                <a href="#diagnosis" data-toggle="tab" aria-expanded="true"><i
                                            class="fas fa-diagnoses"></i> Diagnosis
                                </a>
                            </li>
                            <li>
                                <a href="#resep" data-toggle="tab" aria-expanded="true"><i
                                            class="far fa-calendar-check"></i> Resep
                                </a>
                            </li>
                            <li>
                                <a href="#bahan_habis_pakai" data-toggle="tab" aria-expanded="true"><i
                                            class="far fa-calendar-check"></i> Bahan Habis Pakai
                                </a>
                            </li>
                            <li>
                                <a href="#biaya" data-toggle="tab" aria-expanded="true"><i
                                            class="fas fa-donate"></i> Biaya
                                </a>
                            </li>
                            <li>
                                <a href="#bill" class="bill" data-toggle="tab" aria-expanded="true"><i
                                            class="fas fa-file-invoice-dollar"></i> Bill
                                </a>
                            </li>
                        </ul>

                        <div class="tab-content">
                            <div class="tab-pane active" id="pemeriksaan">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered table-hover example">
                                        <thead>
                                        <th>No</th>
                                        <th>Tgl Pemeriksaan</th>
                                        <th>Shift</th>
                                        <th>Dokter</th>
                                        <th>Subjektif</th>
                                        <th>Objektif</th>
                                        <th>Asesmen</th>
                                        <th>Plan</th>
                                        </thead>
                                        <tbody>
                                        <?php
                                        $no = 1;
                                        if (!empty($pemeriksaan)) {
                                            foreach ($pemeriksaan as $r) {
                                                ?>
                                                <tr>
                                                    <td><?=$no++?></td>
                                                    <td><?=$r->created_at?></td>
                                                    <td><?=$r->shift?></td>
                                                    <td><?=$r->nama_dokter?></td>
                                                    <td><?=$r->subjektif?></td>
                                                    <td>
                                                        <table class="table table-bordered" style="margin-bottom: 0 !important;">
                                                        <?php
                                                        $obj = unserialize($r->objektif);
                                                        foreach ($obj as $k => $v) : ?>
                                                            <tr>
                                                                <td style="padding-top: 0 !important; padding-bottom: 0 !important; font-weight: bold;">
                                                                    <?=$k?>
                                                                </td>
                                                                <td style="padding-top: 0 !important; padding-bottom: 0 !important;">
                                                                    <?=$v?>
                                                                </td>
                                                            </tr>
                                                        <?php endforeach; ?>
                                                        </table>
                                                    </td>
                                                    <td><?=$r->asesmen?></td>
                                                    <td><?=$r->plan?></td>
                                                </tr>
                                                <?php
                                            }
                                        }
                                        ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="tab-pane" id="diagnosis">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered table-hover example">
                                        <thead>
                                        <th>No</th>
                                        <th>Tgl Diagnosis</th>
                                        <th>Penyakit</th>
                                        <th>Diagnosis</th>
                                        </thead>
                                        <tbody>
                                        <?php
                                        $no = 1;
                                        foreach ($diagnosis as $r) : ?>
                                            <tr>
                                                <td><?=$no++?></td>
                                                <td><?=$r->created_at?></td>
                                                <td>
                                                    <?php foreach ($r->penyakit as $p) : ?>
                                                        <span style="margin-left:2px; margin-top:1px; display: inline-block">
                                                        <span class="label label-success ml-1"><?=$p->kode.' - '.$p->nama?></span>
                                                    </span>
                                                    <?php endforeach; ?>
                                                </td>
                                                <td><?=$r->diagnosis?></td>
                                            </tr>
                                        <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="tab-pane" id="resep">
                                <div class="table-responsive">
                                    <table class="table table-bordered example">
                                        <thead>
                                        <th>No</th>
                                        <th>Tgl Resep</th>
                                        <th>Obat Satuan</th>
                                        <th>Obat Racik</th>
                                        </thead>
                                        <tbody>
                                        <?php
                                        $no = 1;
                                        foreach ($resep as $r) : ?>
                                            <tr>
                                                <td><?=$no++?></td>
                                                <td><?=$r->created_at?></td>
                                                <td style="font-size: 12px">
                                                    <table class="table table-condensed">
                                                        <thead>
                                                            <th>Nama Obat</th>
                                                            <th>Jumlah</th>
                                                            <th>Signa</th>
                                                        </thead>
                                                        <tbody>
                                                        <?php foreach ($r->obat as $obat) : ?>
                                                            <tr>
                                                                <td><?=$obat->nama?></td>
                                                                <td><?=$obat->jumlah?></td>
                                                                <td><?=$obat->signa?></td>
                                                            </tr>
                                                        <?php endforeach; ?>
                                                        </tbody>
                                                    </table>
                                                </td>
                                                <td style="font-size: 12px">
                                                    <table class="table table-condensed">
                                                        <thead>
                                                            <th>Racikan</th>
                                                            <th>Signa</th>
                                                            <th>Catatan</th>
                                                        </thead>
                                                        <tbody>
                                                        <?php foreach ($r->obat_racik as $racik) : ?>
                                                            <tr>
                                                                <td>
                                                                    <table class="table table-condensed">
                                                                        <thead>
                                                                            <th>Nama Obat</th>
                                                                            <th>Jumlah</th>
                                                                        </thead>
                                                                        <tbody>
                                                                        <?php foreach ($racik->detail as $detail) : ?>
                                                                            <tr>
                                                                                <td><?=$detail->nama?></td>
                                                                                <td><?=$detail->jumlah?></td>
                                                                            </tr>
                                                                        <?php endforeach; ?>
                                                                        </tbody>
                                                                    </table>
                                                                </td>
                                                                <td><?=$racik->signa?></td>
                                                                <td><?=$racik->catatan?></td>
                                                            </tr>
                                                        <?php endforeach; ?>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                        <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="tab-pane" id="bahan_habis_pakai">
                                <div class="table-responsive">
                                    <table class="table table-bordered example">
                                        <thead>
                                        <th>No</th>
                                        <th>Tanggal</th>
                                        <th>Bahan Habis Pakai</th>
                                        </thead>
                                        <tbody>
                                        <?php
                                        $no = 1;
                                        foreach ($bahan_habis_pakai as $r) : ?>
                                            <tr>
                                                <td><?=$no++?></td>
                                                <td><?=$r->created_at?></td>
                                                <td style="font-size: 12px">
                                                    <table class="table table-condensed">
                                                        <thead>
                                                            <th>Nama</th>
                                                            <th>Jumlah</th>
                                                        </thead>
                                                        <tbody>
                                                        <?php foreach ($r->bahan as $b) : ?>
                                                            <tr>
                                                                <td><?=$b->nama?></td>
                                                                <td><?=$b->jumlah?></td>
                                                            </tr>
                                                        <?php endforeach; ?>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                        <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="tab-pane" id="biaya">
                                <div class="table-responsive">
                                    <table class="table table-bordered example">
                                        <thead>
                                        <th>No</th>
                                        <th>Jenis Biaya</th>
                                        <th>Item</th>
                                        <th>Biaya</th>
                                        </thead>
                                        <tbody>
                                        <?php
                                        $total = 0;
                                        foreach($biaya as $r) : ?>
                                            <tr>
                                                <td><?=$no++?></td>
                                                <td><?=$r->jenis_biaya?></td>
                                                <td>
                                                    <?php
                                                    if ($r->jenis_biaya == 'Biaya Kamar') {
                                                        echo $rawat_inap->bed_name.' - '.$rawat_inap->bedgroup;
                                                    }
                                                    else if ($r->jenis_biaya == 'Makanan') {
                                                        echo $r->makanan->nama;
                                                    }
                                                    else if ($r->jenis_biaya == 'Tindakan') : ?>
                                                        <table class="table table-condensed">
                                                            <thead>
                                                            <th>Nama</th>
                                                            <th>Tarif</th>
                                                            </thead>
                                                            <tbody>
                                                            <?php foreach ($r->tindakan as $t) : ?>
                                                            <tr>
                                                                <td><?=$t->nama?></td>
                                                                <td><?=$t->tarif_pasien?></td>
                                                            </tr>
                                                            <?php endforeach; ?>
                                                            </tbody>
                                                        </table>
                                                    <?php endif; ?>
                                                </td>
                                                <td class="text-right"><?=$r->biaya?></td>
                                            </tr>
                                        <?php
                                        $total += $r->biaya;
                                        endforeach;
                                        ?>
                                        </tbody>


                                        <tr class="box box-solid total-bg">
                                            <td colspan='4' class="text-right">
                                                <?php echo $this->lang->line('total') . " : " . $currency_symbol . "" . $total ?>
                                                <input type="hidden" id="charge_total" name="charge_total"
                                                       value="<?php echo $total ?>">
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <div class="tab-pane" id="bill">
                                <div class="row">
                                    <div class="col-md-12">
                                        <section class="invoice">
                                            <div class="row">
                                                <div class="col-xs-12">
                                                        <i class="fa fa-user"></i> NO. RM <?= $pasien->no_rm ?>
                                                    </h2>
                                                </div>
                                            </div>
                                            <form enctype="multipart-data" method="POST" action="<?= base_url() ?>Administrasi/nota_submit">
                                                <div class="row invoice-info">
                                                    <div class="col-sm-12 invoice-col">
                                                        Nama Pasien: <strong><?= $pasien->nama ?></strong><br>
                                                        Jenis Pendaftaran: <?= $rawat_inap->tipe_pasien ?><br>
                                                        <br>
                                                        <address>
                                                            Alamat : <?= $pasien->alamat ?><br>
                                                            Usia: <?= $pasien->usia ?><br>
                                                            Telepon: <?= $pasien->telepon ?><br>
                                                        </address>
                                                    </div>
                                                    <div class="col-xs-12">
                                                        <div class="row">
                                                            <div class="col-sm-12">
                                                                <table class="table table-bordered">
                                                                    <tbody>
                                                                    <tr>
                                                                        <th style="width: 10px">#</th>
                                                                        <th>Tindakan</th>
                                                                        <th class="text-right">Biaya</th>
                                                                    </tr>
                                                                    <?php
                                                                    $total_tindakan = 0;
                                                                    $tindakans = [];
                                                                    foreach ($biaya as $b) {
                                                                        if ($b->jenis_biaya == 'Tindakan') {
                                                                            foreach ($b->tindakan as $t) {
                                                                                $tindakans[] = $t;
                                                                            }
                                                                        }
                                                                    }
                                                                    $no = 1;
                                                                    foreach ($tindakans as $key => $value) { ?>
                                                                        <tr>
                                                                            <td><?=$no++?></td>
                                                                            <td><?= $value->nama ?>
                                                                                <input type="hidden" name="nama_tindakan[]" value="<?= $value->nama ?>">
                                                                            </td>
                                                                            <td align="right">
                                                                                <?= harga($value->tarif_pasien); ?>
                                                                                <input type="hidden" name="tarif_pasien[]" value="<?= $value->tarif_pasien ?>">
                                                                            </td>
                                                                        </tr>
                                                                        <?php
                                                                        $total_tindakan += $value->tarif_pasien;
                                                                    } ?>
                                                                    <tr>
                                                                        <td align="right" colspan="2"><strong>Total: </strong></td>
                                                                        <td align="right"><strong><?=harga($total_tindakan)?></strong></td>
                                                                    </tr>
                                                                    </tbody>
                                                                </table>
                                                                <table class="table table-bordered">
                                                                    <tbody>
                                                                    <tr>
                                                                        <th style="width: 10px">#</th>
                                                                        <th>Obat</th>
                                                                        <th>jumlah</th>
                                                                        <th class="text-right">Harga Satuan</th>
                                                                        <th class="text-right">Biaya</th>
                                                                    </tr>
                                                                    <?php
                                                                    $total_obat = 0;
                                                                    $obats = [];
                                                                    foreach ($resep as $r) {
                                                                        if ($r->obat) {
                                                                            foreach ($r->obat as $o) {
                                                                                $obats[] = $o;
                                                                            }
                                                                        }
                                                                    }
                                                                    $no = 1;
                                                                    foreach ($obats as $key => $value) { ?>
                                                                        <tr>
                                                                            <td><?=$no++?></td>
                                                                            <td><?= $value->nama ?><input type="hidden" name="nama_obat[]" value="<?= $value->nama ?>"></td>
                                                                            <td><?= $value->jumlah ?><input type="hidden" name="jumlah_satuan[]" value="<?= $value->jumlah ?>"></td>
                                                                            <td class="text-right"><?= harga($value->harga_jual); ?>
                                                                                <input type="hidden" name="harga_jual[]" value="<?= $value->harga_jual ?>">
                                                                            </td>
                                                                            <td class="text-right"><?= harga($value->harga_jual * $value->jumlah); ?>
                                                                                <input type="hidden" name="subtotal_obat[]" value="<?= $value->harga_jual * $value->jumlah; ?>">
                                                                            </td>
                                                                        </tr>
                                                                        <?php $total_obat += ($value->harga_jual * $value->jumlah);
                                                                    } ?>
                                                                    <tr>
                                                                        <td class="text-right" colspan="4"><strong>Total: </strong></td>
                                                                        <td class="text-right"><strong><?=harga($total_obat)?></strong></td>
                                                                    </tr>
                                                                    </tbody>
                                                                </table>
                                                                <table class="table table-bordered">
                                                                    <tbody>
                                                                    <tr>
                                                                        <th style="width: 10px">#</th>
                                                                        <th>Obat Racik</th>
                                                                        <th class="text-right">Biaya</th>
                                                                    </tr>
                                                                    <?php
                                                                    $total_obatracik = 0;
                                                                    $racikans = [];
                                                                    foreach ($resep as $r) {
                                                                        if ($r->obat_racik) {
                                                                            foreach ($r->obat_racik as $o) {
                                                                                $racikans[] = $o;
                                                                            }
                                                                        }
                                                                    }
                                                                    $no = 1;
                                                                    foreach ($racikans as $key => $value) { ?>
                                                                        <tr>
                                                                            <td><?=$no++?></td>
                                                                            <td>
                                                                                <table class="table table-condensed" style="margin-bottom: 0 !important;">
                                                                                <tbody>
                                                                                <tr>
                                                                                    <th>Obat</th>
                                                                                    <th>Jumlah</th>
                                                                                    <th class="text-right">Harga Satuan</th>
                                                                                    <th class="text-right">Total</th>
                                                                                </tr>
                                                                                <?php foreach ($value->detail as $k => $v) { ?>
                                                                                    <tr>
                                                                                        <td><?= $v->nama ?></td>
                                                                                        <td><?= $v->jumlah ?></td>
                                                                                        <td class="text-right"><?= harga($v->harga_jual); ?></td>
                                                                                        <td class="text-right"><?= harga($v->harga_jual * $v->jumlah); ?></td>
                                                                                    </tr>
                                                                                <?php } ?>
                                                                                </tbody>
                                                                                </table>
                                                                            </td>
                                                                            <td class="text-right align-bottom" style="vertical-align: bottom !important;">
                                                                                <?= harga($value->total); ?>
                                                                                <input type="hidden" name="total_racikan[]" value="<?= $value->total; ?>">
                                                                            </td>
                                                                        </tr>

                                                                        <?php $total_obatracik += $value->total; ?>
                                                                    <?php } ?>
                                                                    <tr>
                                                                        <td class="text-right" colspan="2"><strong>Total: </strong></td>
                                                                        <td class="text-right"><strong><?=harga($total_obatracik)?></strong></td>
                                                                    </tr>
                                                                    </tbody>
                                                                </table>
                                                                <table class="table table-bordered">
                                                                    <tbody>
                                                                    <tr>
                                                                        <th style="width: 10px">#</th>
                                                                        <th>Bahan Habis Pakai</th>
                                                                        <th>jumlah</th>
                                                                        <th class="text-right">Harga Satuan</th>
                                                                        <th class="text-right">Biaya</th>
                                                                    </tr>
                                                                    <?php
                                                                    $total_bahan = 0;
                                                                    $bahans = [];
                                                                    foreach ($bahan_habis_pakai as $bhp) {
                                                                        if ($bhp->bahan) {
                                                                            foreach ($bhp->bahan as $b) {
                                                                                $bahans[] = $b;
                                                                            }
                                                                        }
                                                                    }
                                                                    $no = 1;
                                                                    foreach ($bahans as $key => $value) { ?>
                                                                        <tr>
                                                                            <td><?=$no++?></td>
                                                                            <td><?= $value->nama ?><input type="hidden" name="nama_bahan[]" value="<?= $value->nama ?>"></td>
                                                                            <td><?= $value->jumlah ?><input type="hidden" name="jumlah_satuan_bahan[]" value="<?= $value->jumlah ?>"></td>
                                                                            <td align="right">
                                                                                <?= harga($value->harga_jual); ?>
                                                                                <input type="hidden" name="harga_jual_bahan[]" value="<?= $value->harga_jual ?>">
                                                                            </td>
                                                                            <td align="right">
                                                                                <?= harga($value->harga_jual * $value->jumlah); ?>
                                                                                <input type="hidden" name="subtotal_bahan[]" value="<?= $value->harga_jual * $value->jumlah; ?>">
                                                                            </td>
                                                                        </tr>
                                                                        <?php $total_bahan += ($value->harga_jual * $value->jumlah);
                                                                    } ?>
                                                                    <tr>
                                                                        <td class="text-right" colspan="4"><strong>Total: </strong></td>
                                                                        <td class="text-right"><strong><?=harga($total_bahan)?></strong></td>
                                                                    </tr>
                                                                    </tbody>
                                                                </table>
                                                                <table class="table table-bordered">
                                                                    <tbody>
                                                                    <tr>
                                                                        <th style="width: 10px">#</th>
                                                                        <th>Makanan</th>
                                                                        <th>jumlah</th>
                                                                        <th class="text-right">Harga Satuan</th>
                                                                        <th class="text-right">Biaya</th>
                                                                    </tr>
                                                                    <?php
                                                                    $total_makanan = 0;
                                                                    $no = 1;
                                                                    foreach ($makanan_count_list as $key => $value) { ?>
                                                                        <tr>
                                                                            <td><?=$no++?></td>
                                                                            <td><?= $value->nama ?><input type="hidden" name="nama_bahan[]" value="<?= $value->nama ?>"></td>
                                                                            <td><?= $value->jumlah ?><input type="hidden" name="jumlah_satuan_bahan[]" value="<?= $value->jumlah ?>"></td>
                                                                            <td align="right">
                                                                                <?= harga($value->harga); ?>
                                                                                <input type="hidden" name="harga_jual_bahan[]" value="<?= $value->harga ?>">
                                                                            </td>
                                                                            <td align="right">
                                                                                <?= harga($value->harga * $value->jumlah); ?>
                                                                                <input type="hidden" name="subtotal_bahan[]" value="<?= $value->harga * $value->jumlah; ?>">
                                                                            </td>
                                                                        </tr>
                                                                        <?php $total_makanan += ($value->harga * $value->jumlah);
                                                                    } ?>
                                                                    <tr>
                                                                        <td class="text-right" colspan="4"><strong>Total: </strong></td>
                                                                        <td class="text-right"><strong><?=harga($total_makanan)?></strong></td>
                                                                    </tr>
                                                                    </tbody>
                                                                </table>
                                                                <table class="table table-bordered">
                                                                    <tbody>
                                                                    <tr>
                                                                        <th style="width: 10px">#</th>
                                                                        <th>Bed</th>
                                                                        <th>Waktu</th>
                                                                        <th class="text-right">Harga</th>
                                                                        <th class="text-right">Biaya</th>
                                                                    </tr>
                                                                    <?php
                                                                    function getHowManyDays($start, $end, $add) {
                                                                        $d1 = date_create(date( 'Y-m-d', strtotime($start)));
                                                                        $d2 = date_create(date( 'Y-m-d', strtotime($end)));
                                                                        $waktu = (int) date_diff($d1, $d2)->format("%a");
                                                                        if ($add || $waktu == 0) {
                                                                            $waktu++;
                                                                        }
                                                                        return $waktu;
                                                                    }

                                                                    $total_biaya_bed = 0;
                                                                    $no = 1;
                                                                    foreach ($beds as $k => $v) : ?>
                                                                    <tr>
                                                                        <td><?=$no++?></td>
                                                                        <td><?=$v['bed']['name'].' - '.$v['bed']['bedgroup']?></td>
                                                                        <td>
                                                                            <?php
                                                                            $count = getHowManyDays(
                                                                                $v['start'],
                                                                                $v['end'] == 'now' ? date("Y-m-d H:i") : $v['end'],
                                                                                $k == 0
                                                                            );
                                                                            echo $count;
                                                                            ?> hari<br>
                                                                            <i style="font-size: 12px;">
                                                                                Tgl masuk: <?=date( 'd-m-Y H:i', strtotime($v['start']))?><br>
                                                                                Tgl keluar: <?=$v['end'] == 'now' ? '-' : date( 'd-m-Y H:i', strtotime($v['end']))?>
                                                                            </i>
                                                                        </td>
                                                                        <td class="text-right"><?=harga($v['bed']['group_price'])?></td>
                                                                        <td align="right">
                                                                            <?php
                                                                            $jml = $v['bed']['group_price'] * $count;
                                                                            $total_biaya_bed += $jml;
                                                                            echo harga($jml);
                                                                            ?>
                                                                        </td>
                                                                    </tr>
                                                                    <?php endforeach; ?>
                                                                    <tr>
                                                                        <td class="text-right" colspan="4"><strong>Total: </strong></td>
                                                                        <td class="text-right"><strong><?=harga($total_biaya_bed)?></strong></td>
                                                                    </tr>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                        <h4 class="box-title ptt10">Ringkasan Billing</h4>
                                                        <div class="table-responsive" style="border: 1px solid #dadada;border-radius: 2px; padding: 10px;">
                                                            <table class="nobordertable table table-striped table-responsive">
                                                                <tr>
                                                                    <th>Total Tindakan</th>
                                                                    <td class="text-right fontbold20"><?php echo harga($total_tindakan) ?></td>
                                                                </tr>
                                                                <tr>
                                                                    <th>Total Obat</th>
                                                                    <td class="text-right fontbold20"><?php echo harga($total_obat) ?></td>
                                                                </tr>
                                                                <tr>
                                                                    <th>Total Obat Racik</th>
                                                                    <td class="text-right fontbold20"><?php echo harga($total_obatracik) ?></td>
                                                                </tr>
                                                                <tr>
                                                                    <th>Total Bahan Habis Pakai</th>
                                                                    <td class="text-right fontbold20"><?php echo harga($total_bahan) ?></td>
                                                                </tr>
                                                                <tr>
                                                                    <th>Total Makanan</th>
                                                                    <td class="text-right fontbold20"><?php echo harga($total_makanan) ?></td>
                                                                </tr>
                                                                <tr>
                                                                    <th>Total Biaya Bed</th>
                                                                    <td class="text-right fontbold20"><?php echo harga($total_biaya_bed) ?></td>
                                                                </tr>
                                                                <tr>
                                                                    <th style="vertical-align: middle !important;">Total Tagihan</th>
                                                                    <td class="text-right fontbold20">
                                                                        <h4 style="font-weight: bold">
                                                                            <?php
                                                                            $total_tagihan = $total_tindakan + $total_obat + $total_obatracik + $total_bahan + $total_biaya_bed;
                                                                            echo harga($total_tagihan);
                                                                            ?>
                                                                        </h4>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </section>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
