<div class="row">
    <div class="col-sm-12">
        <h4 style="text-align: center"><strong><?=$title?></strong></h4>

        <table class="table table-bordered">
            <tbody>
            <tr>
                <th style="width: 10px">#</th>
                <th><?=$ambulance->vehicle_model?> - <?=$ambulance->vehicle_no?><br>Tujuan: <?=$ambulance->tujuan?></th>
                <th>
                    <input type="text" style="text-align: right;" autocomplete="off"
                           class="form-control" id="<?=$amb_id?>" name="<?=$amb_id?>"
                           value="<?=$ambulance->tarif?>"
                           readonly>
                </th>
            </tr>
            </tbody>
        </table>

    </div>
</div>