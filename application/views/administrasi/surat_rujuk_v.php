<link rel="stylesheet"
      href="<?php echo base_url() ?>assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">

<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Surat Keterangan Rujuk
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a href="#">Surat Sehat </a></li>
            <li class="active"> Pasien</li>
        </ol>
    </section>
    <section class="invoice col-md-6">
        <!-- title row -->
        <div class="row">
            <div class="col-xs-12">
                <h2 class="page-header">
                    <i class="fa fa-user"></i> NO. RM <?php echo $pemeriksaan->no_rm ?>
                    <small class="pull-right"><strong>Tanggal: <?php echo $pemeriksaan->waktu_pemeriksaan ?></strong></small>
                </h2>
            </div>
        </div>
        <form enctype="multipart-data" method="POST" action="<?php echo base_url() ?>Administrasi/surat_rujuk_print/<?php echo $pemeriksaan->id ?>">
            <div class="row">
                <div class="col-sm-12">
                    <p>Kepada YTH : <input type="text" class="form-control" name="yth" value="<?=$surat_form->yth?>"></p>
                    <p>Dokter : <input type="text" class="form-control" name="dokter" value="<?=$surat_form->dokter?>"></p>
                    <p>Di : <input type="text" class="form-control" name="di" value="<?=$surat_form->di?>"></p>
                    <br>
                    <p>Bersama ini kami konsultasikan untuk penanganan lebih lanjut penderita</p>
                    <table class="table">
                        <tr>
                            <td>Nama</td>
                            <td>:</td>
                            <td><?= $pemeriksaan->nama_pasien ?></td>
                        </tr>
                        <tr>
                            <td>Umur</td>
                            <td>:</td>
                            <td><?= $pasien->usia?></td>
                        </tr>
                        <tr>
                            <td>Jenis Kelamin</td>
                            <td>:</td>
                            <td><?= $pasien->jk == 'L' ? 'Laki-Laki' : 'Perempuan'?></td>
                        </tr>
                        <tr>
                            <td>Alamat</td>
                            <td>:</td>
                            <td><?= $pasien->alamat?></td>
                        </tr>
                        <tr>
                            <td>Pekerjaan</td>
                            <td>:</td>
                            <td><?= $pasien->pekerjaan?></td>
                        </tr>
                    </table>

                    <div>
                        <p>Dengan keluhan </p>
                        <textarea class="form-control" name="keluhan" rows="3" id="surat-rujuk-keluhan"><?=$surat_form->keluhan?></textarea>
                        <p>Sementara kami diagnosa:  </p>
                        <textarea class="form-control" name="diagnosa" rows="3" id="surat-rujuk-diagnosa"><?=$surat_form->diagnosa?></textarea>
                        <p>Dan kami beri terapi </p>
                        <textarea class="form-control" name="terapi" rows="3" id="surat-rujuk-terapi"><?=$surat_form->terapi?></textarea>
                        <p>Atas bantuan dan kerjasamanya kami sampaikan terima kasih.</p>
                    </div>
                </div>
            </div>
            <div class="row no-print">
                <div class="col-xs-12">
                    <button type="submit" class="btn btn-default"><i class="fa fa-print"></i> Print</button>
                </div>
            </div>
        </form>
    </section>
    <div class="clearfix"></div>
</div>