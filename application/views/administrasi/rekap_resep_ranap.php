<link rel="stylesheet"
      href="<?= base_url() ?>assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Rekapitulasi Billing
            <small></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a href="#">Billing</a></li>
            <li class="active"> Rekapitulasi</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Data Rekapitulasi Billing</h3>&nbsp;&nbsp;
                    </div>
                    <form class="form-horizontal" method="get" action="<?= base_url() ?>Administrasi/rekap_resep_ranap">
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="form-horizontal">
                                    <div class="form-group">
                                        <div class="col-sm-1"></div>
                                        <div class="col-sm-7">
                                            <label class="control-label">Tanggal</label>
                                            <input type='date' name='tgl' class='form-control' id='tgl'
                                                   value="<?= ($this->input->get('tgl')) ? $this->input->get('tgl') : date('Y-m-d') ?>">
                                            <br>
                                            <button type="submit" class="btn btn-primary" style='margin-left: 0px;'>
                                                Tampilkan
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                    <!-- /.box-header -->
                    <?php $warning = $this->session->flashdata('warning');
                    if (!empty($warning)) { ?>
                        <div class="alert alert-warning alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <h4><i class="icon fa fa-warning"></i> Alert!</h4>
                            <?= $warning ?>
                        </div>
                    <?php } ?>
                    <?php $success = $this->session->flashdata('success');
                    if (!empty($success)) { ?>
                        <div class="alert alert-success alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <h4><i class="icon fa fa-success"></i> Success!</h4>
                            <?= $success ?>
                        </div>
                    <?php } ?>
                    <div class="box-body">
                        <table id="printable_table" class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>No</th>
                                <th>NO RM</th>
                                <th>Nama Pasien</th>
                                <th>Tindakan</th>
                                <th>Obat</th>
                                <th>Obat Racikan</th>
                                <th>Status</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($list as $k => $row) { ?>
                                <tr>
                                    <td> <?= $k + 1; ?></td>
                                    <td style="width: 100px;">
                                        <?= $row->no_rm; ?><br>
                                        <small>
                                            <span class="label <?= $jaminan[$row->tipe_pasien]['class'] ?>"><?= $jaminan[$row->tipe_pasien]['label'] ?></span>
                                        </small>
                                    </td>
                                    <td> <?= ucwords($row->nama_pasien); ?></td>
                                    <td> <?= 'Rp ' . number_format($row->tarif_tindakan, 2, ',', '.') ?> </td>
                                    <td> <?= 'Rp ' . number_format($row->harga_obat, 2, ',', '.') ?> </td>
                                    <td>
                                        <span class="pull-right-container">
                                            <small class="label pull-right bg-green">
                                            <?php
                                            echo ucwords($row->status);
                                            ?>
                                            </small>
                                        </span>
                                    </td>
                                </tr>
                                <?php $no++;
                            } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<script>
    $(function () {
        // $('#example1').DataTable()
        // $('#example2').DataTable({
        //     rowReorder: {
        //         selector: 'td:nth-child(2)'
        //     },
        //     'paging'      : true,
        //     'lengthChange': true,
        //     'searching'   : true,
        //     'ordering'    : true,
        //     'info'        : true,
        //     'autoWidth'   : true
        // })
    })
</script>
