<link rel="stylesheet"
      href="<?php echo base_url() ?>assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Surat Sehat
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a href="#">Surat Sehat </a></li>
            <li class="active"> Pasien</li>
        </ol>
    </section>
    <section class="invoice col-md-6">
        <div class="row">
            <div class="col-xs-12">
                <h2 class="page-header">
                    <i class="fa fa-user"></i> NO. RM <?php echo $pemeriksaan->no_rm ?>
                    <small class="pull-right"><strong>Tanggal: <?php echo $pemeriksaan->waktu_pemeriksaan ?></strong></small>
                </h2>
            </div>
        </div>
        <form enctype="multipart-data" method="POST" action="<?php echo base_url() ?>Administrasi/surat_sehat_print/<?php echo $pemeriksaan->id ?>">
            <div class="row invoice-info">
                <div class="col-sm-12 invoice-col">
                    <p>Yang bertanda tangan dibawah ini, <?=ucwords($pemeriksaan->nama_dokter)?> selaku dokter pemeriksa di <?=$klinik->nama?>, dengan ini menerangkan</p>
                    <table class="table">
                        <tr>
                            <td>Nama</td>
                            <td>:</td>
                            <td><strong><?php echo $pemeriksaan->nama_pasien ?></strong></td>
                        </tr>
                        <tr>
                            <td>Tempat tanggal lahir</td>
                            <td>:</td>
                            <td><?php echo $pasien->tempat_lahir ?>, <?php echo DateIndo($pasien->tanggal_lahir) ?></td>
                        </tr>
                        <tr>
                            <td>Jenis kelamin</td>
                            <td>:</td>
                            <td><?php echo $pasien->jk == 'L' ? 'Laki-laki' : 'Perempuan' ?></td>
                        </tr>
                        <tr>
                            <td>Pekerjaan</td>
                            <td>:</td>
                            <td><?php echo $pemeriksaan->pekerjaan ?></td>
                        </tr>
                        <tr>
                            <td>Alamat</td>
                            <td>:</td>
                            <td><?php echo $pemeriksaan->alamat ?></td>
                        </tr>
                        <tr>
                            <td>Pada pemeriksan tanggal</td>
                            <td>:</td>
                            <td><?php echo DateIndo($pemeriksaan->waktu_pemeriksaan) ?></td>
                        </tr>
                        <tr>
                            <td>Dinyatakan dalam keadaan</td>
                            <td>:</td>
                            <td>Sehat</td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="row invoice-info">
                <div class="col-sm-12 invoice-col">
                    <p>
                        Surat keterangan ini dibuat dipergunakan untuk:
                        <input type="text" name="untuk" class="form-control" value="<?=$surat_form->untuk?>">
                    </p>
                    <p>
                        Demikian surat keterangan ini kami buat sebenarnya untuk dapat digunakan sebagaimana mestinya.
                    </p>

                    <table class="table">
                        <thead>
                        <tr>
                            <th colspan="3">Keterangan</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>Visus mata kanan VOD</td>
                            <td>:</td>
                            <td><input type="text" class="form-control" name="vm_kanan" value="<?=$surat_form->vm_kanan?>"></td>
                        </tr>
                        <tr>
                            <td>Visus mata kiri VOS</td>
                            <td>:</td>
                            <td><input type="text" class="form-control" name="vm_kiri" value="<?=$surat_form->vm_kiri?>"></td>
                        </tr>
                        <tr>
                            <td>Telinga Kanan</td>
                            <td>:</td>
                            <td><input type="text" class="form-control" name="telinga_kanan" value="<?=$surat_form->telinga_kanan?>"></td>
                        </tr>
                        <tr>
                            <td>Telinga Kiri</td>
                            <td>:</td>
                            <td><input type="text" class="form-control" name="telinga_kiri" value="<?=$surat_form->telinga_kiri?>"></td>
                        </tr>
                        <tr>
                            <td>Tes Buta Warna</td>
                            <td>:</td>
                            <td><input type="text" class="form-control" name="tes_buta_warna" value="<?=$surat_form->tes_buta_warna?>"></td>
                        </tr>
                        <tr>
                            <td>Golongan Darah</td>
                            <td>:</td>
                            <td><input type="text" class="form-control" name="goldar" value="<?=$surat_form->goldar?>"></td>
                        </tr>
                        <tr>
                            <td>Tinggi Badan</td>
                            <td>:</td>
                            <td><?php echo $pemeriksaan->tb ?>cm</td>
                        </tr>
                        <tr>
                            <td>Berat Badan</td>
                            <td>:</td>
                            <td><?php echo $pemeriksaan->bb ?> Kg</td>
                        </tr>
                        <tr>
                            <td>Tensi</td>
                            <td>:</td>
                            <td><?php echo $pemeriksaan->td ?></td>
                        </tr>
                        <tr>
                            <td>Nadi</td>
                            <td>:</td>
                            <td><?php echo $pemeriksaan->n ?>x/mnt</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="row no-print">
                <div class="col-xs-12">
                    <button type="submit" class="btn btn-default"><i class="fa fa-print"></i> Print</button>
                </div>
            </div>
        </form>
    </section>
    <div class="clearfix"></div>
</div>

<script>

    function set_kembalian() {
        var total = parseInt($('#total').val());
        var bayar = parseInt($('#bayar').val());
        var kembalian = bayar - total;
        $('#kembalian').val(kembalian);

    }

    $(function () {
        $('#example1').DataTable()
        $('#example2').DataTable({
            'paging': true,
            'lengthChange': false,
            'searching': false,
            'ordering': true,
            'info': true,
            'autoWidth': false
        })
    })
</script>