<script>

    $(function () {
        $('#example1').DataTable();
        $('#example2').DataTable({
            'paging'      : true,
            'lengthChange': true,
            'searching'   : true,
            'ordering'    : true,
            'info'        : true,
            'autoWidth'   : true
        })

        const by = '<?=$by?>';
        $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
            const target = $(e.target).attr("href") // activated tab
            if (target !== `#${by}`) {
                $('.toggleable').hide()
            }
            else {
                $('.toggleable').show()
            }
        });

        $('.select2').select2();

        const jenis = <?=isset($jenis) ? $jenis : -100?>;
        if (jenis === 2) {
            $('.jenis-2').css('display', 'block')
        }
        else if (jenis === 3) {
            $('.jenis-3').css('display', 'block')
        }

        $("#jenis").change(function () {

            $('.jenis-2').css('display', 'none')
            $('.jenis-3').css('display', 'none')

            if ($(this).val() === '2') {
                $('.jenis-2').css('display', 'block')
            }
            else if ($(this).val() === '3') {
                $('.jenis-3').css('display', 'block')
            }
        })
    })

    const list = <?=json_encode($listPemeriksaan)?>;

    function open_detail(id, bayar_id, pemeriksaan_id, type, jenis_ruangan_id = '') {
        const v = list.find(v => +v.id === +id)
        if (v) {
            const b = v.bayar.find(v => +v.bayar_id === +bayar_id)
            if (b) {
                $('#choose-title').html(`Detail ${type}`)
                $('#detail-modal').modal('show')
                const tbl = $('#tbl-detail tbody')
                tbl.empty()

                let url = ''
                if (jenis_ruangan_id === 'obat_luar')
                    url = `${type === 'Jasa Medis' ? 'ga ada ges' : type === 'Bahan Habis Pakai' ? 'ga ada ges' : 'get_obat_luar'}/${bayar_id}`
                else if (+b.rawat_inap_id !== -1)
                    url = `${type === 'Jasa Medis' ? 'get_tindakan_ranap' : type === 'Bahan Habis Pakai' ? 'get_bhp_ranap' : 'get_obat_ranap'}/${bayar_id}`
                else
                    url = `${type === 'Jasa Medis' ? 'get_tindakan' : type === 'Bahan Habis Pakai' ? 'get_bhp' : 'get_obat'}/${bayar_id}`

                $.ajax({
                    url: url,
                    success: function (result) {
                        const data = JSON.parse(result)
                        let subtotal = 0
                        data.forEach((v, k) => {
                            tbl.append(`
                                <tr>
                                    <td>${k + 1}</td>
                                    <td>${v.item}</td>
                                    <td>${formatMoney(+v.harga)}</td>
                                    <td>${v.item === 'Bed' ? 1 : v.jumlah}</td>
                                    <td>${formatMoney(+v.subtotal)}</td>
                                </tr>
                            `)
                            subtotal += +v.subtotal
                        })
                        tbl.append(`
                            <tr>
                                <th colspan="4" class="text-right">Total</th>
                                <th><b>${formatMoney(subtotal.toFixed(2))}</b></th>
                            </tr>
                        `)
                    }
                })
            }
        }
    }

    function open_detail_racikan(nama_poli, pemeriksaan_id, rawat_inap_id, transfer_id, jenis_ruangan_id) {
        $('#detail-modal-racikan').modal('show')
        $('#loading-text').show()
        $('#tbl-detail-racikan').hide()

        const tbl = $('#tbl-detail-racikan tbody')
        tbl.empty()

        let url = ''
        if (jenis_ruangan_id === 'obat_luar')
            url = `get_racikan_luar/${pemeriksaan_id}`
        else if (pemeriksaan_id === -1) {
            if (nama_poli === 'Ranap')
                url = `get_racikan_ranap/1/${rawat_inap_id}/${transfer_id}/${jenis_ruangan_id}`
            else
                url = `get_racikan_ranap/0/${rawat_inap_id}/${transfer_id}/${jenis_ruangan_id}`
        }
        else
            url = `get_racikan/${pemeriksaan_id}`

        $.ajax({
            url: url,
            success: function(result) {
                const data = JSON.parse(result)

                $('#loading-text').hide()
                $('#tbl-detail-racikan').show()

                let total = 0
                data.forEach((v, k) => {
                    if (v.obat.length) {
                        tbl.append(`
                            <tr>
                                <td>${v.nama_racikan}</td>
                                <td>${buildObatRacikanTable(v)}</td>
                                <td style="max-width: 150px">${v.signa}</td>
                                <td style="max-width: 150px">${v.catatan}</td>
                                <td>${formatMoney(v.total)}</td>
                            </tr>
                        `)
                    }
                    total += +v.total
                })
                tbl.append(`
                    <tr>
                        <th colspan="4" class="text-right">Total</th>
                        <th><b>${formatMoney(total.toFixed(2))}</b></th>
                    </tr>
                `)
            }
        })
    }

    const buildObatRacikanTable = (v) => {
        let subtotal = v.obat.reduce((a, b) => a + (+b.harga_jual * +b.jumlah_satuan), 0)

        const obat = v.obat.map(o => `
            <tr>
                <td>${o.nama}</td>
                <td>${o.jumlah_satuan}</td>
                <td>${formatMoney(o.harga_jual)}</td>
            </tr>
        `).join('')

        return `
            <table class="table">
                <thead>
                    <tr>
                        <th>Nama</th>
                        <th>Jumlah</th>
                        <th>Harga</th>
                    </tr>
                </thead>
                <tbody>${obat}</tbody>
                <tfoot>
                    <tr>
                        <th colspan="2" class="text-right">Total</th>
                        <th><b>${formatMoney(subtotal.toFixed(2))}</b></th>
                    </tr>
                </tfoot>
            </table>
        `
    }

    function formatMoney(number) {
        const formatter = new Intl.NumberFormat('id-ID', {
            style: 'currency',
            currency: 'IDR',
        })
        return formatter.format(number)
    }

</script>

<script>
    $('#pencarian_kode').keypress(function (event) {
        if (event.keyCode === 13) {
            event.preventDefault();
        }
    });
</script>

<script type="text/javascript">
    var timer = null;
    $(document).on('keyup', '#pencarian_kode', function (e) {
        if($('#pencarian_kode').val().length > 0){
            clearTimeout(timer);
            timer = setTimeout(
                function () {
                    if ($('#pencarian_kode').val() != '') {
                        var charCode = ( e.which ) ? e.which : event.keyCode;
                        if (charCode == 40) //arrow down
                        {

                            if ($('.form-group').find('div#hasil_pencarian li.autocomplete_active').length > 0) {
                                var selanjutnya = $('.form-group').find('div#hasil_pencarian li.autocomplete_active').next();
                                $('.form-group').find('div#hasil_pencarian li.autocomplete_active').removeClass('autocomplete_active');
                                selanjutnya.addClass('autocomplete_active');
                            } else {
                                $('.form-group').find('div#hasil_pencarian li:first').addClass('autocomplete_active');
                            }
                        }
                        else if (charCode == 38) //arrow up
                        {
                            if ($('.form-group').find('div#hasil_pencarian li.autocomplete_active').length > 0) {
                                var sebelumnya = $('.form-group').find('div#hasil_pencarian li.autocomplete_active').prev();
                                $('.form-group').find('div#hasil_pencarian li.autocomplete_active').removeClass('autocomplete_active');
                                sebelumnya.addClass('autocomplete_active');
                            } else {
                                $('.form-group').find('div#hasil_pencarian li:first').addClass('autocomplete_active');
                            }
                        }
                        else if (charCode == 13) // enter
                        {

                            var Kodenya = $('.form-group').find('div#hasil_pencarian li.autocomplete_active span#kodenya').html();
                            var No_rmnya = $('.form-group').find('div#hasil_pencarian li.autocomplete_active span#no_rmnya').html();
                            var Namanya = $('.form-group').find('div#hasil_pencarian li.autocomplete_active span#namanya').html();
                            if (Kodenya) {
                                $('#pencarian_kode').val(No_rmnya + ' - ' + Namanya);
                                $('#id_pasien').val(Kodenya);
                            } else {
                                alert('data tidak ada! Pilih pilihan yang ada sebelum di enter.');
                            }


                            $('.form-group').find('div#hasil_pencarian').hide();

                        }
                        else {
                            var text = $('#FormStockOpname').find('div#hasil_pencarian').html();
                            autoComplete($('#pencarian_kode').width(), $('#pencarian_kode').val());
                        }
                    } else {
                        $('#FormStockOpname').find('div#hasil_pencarian').hide();
                    }
                }, 100);
        }
    });
    $(document).on('click', '#daftar-autocomplete li', function () {
        $(this).parent().parent().parent().find('#pencarian_kode').val($(this).find('span#no_rmnya').html() + ' - ' + $(this).find('span#namanya').html());
        $(this).parent().parent().parent().find('#id_pasien').val($(this).find('span#kodenya').html());

        $('.form-group').find('#daftar-autocomplete').hide();
    });

    function autoComplete(Lebar, KataKunci) {
        $('div#hasil_pencarian').hide();
        var Lebar = Lebar + 25;

        $.ajax({
            url: "<?php echo site_url('pendaftaran/ajax_kode'); ?>",
            type: "POST",
            cache: false,
            data: 'keyword=' + KataKunci,
            dataType: 'json',
            success: function (json) {
                if (json.status == 1) {
                    $('#FormStockOpname').find('div#hasil_pencarian').css({'width': Lebar + 'px'});
                    $('#FormStockOpname').find('div#hasil_pencarian').show('fast');
                    $('#FormStockOpname').find('div#hasil_pencarian').html(json.datanya);
                }
                if (json.status == 0) {
                    $('#FormStockOpname').find('div#hasil_pencarian').html('');
                }
            }
        });
    }

    function cekbarcode(KataKunci, Indexnya) {
        //alert(KataKunci+' '+Indexnya);
        var Registered = '';
        $('#TabelTransaksi tbody tr').each(function () {
            if (Indexnya !== $(this).index()) {
                if ($(this).find('td:nth-child(2)').find('#kode').val() !== '') {
                    Registered += $(this).find('td:nth-child(2)').find('#kode').val() + ',';
                }
            }
        });
        var suplieridnya = $('#id_suplier').val();
        if (Registered !== '') {
            Registered = Registered.replace(/,\s*$/, "");
        }
        var pencarian_kode = $('#TabelTransaksi tbody tr:eq(' + Indexnya + ') td:nth-child(2)').find('#pencarian_kode');
        $.ajax({
            url: "<?php echo site_url('pembelian/cek-kode'); ?>",
            type: "POST",
            cache: false,
            data: 'keyword=' + KataKunci,
            dataType: 'json',
            success: function (json) {
                if (json.status == 1) {
                    call(1, KataKunci, Indexnya);
                }
                if (json.status == 0) {
                    call(0, KataKunci, Indexnya);
                }
            }
        });
    }
</script>
