<head>
    <title>Nota</title>
</head>

<?php

function DateToIndo($date)
{
    $BulanIndo = array("Januari", "Februari", "Maret", "April",
        "Mei", "Juni", "Juli", "Agustus", "September", "Oktober",
        "November", "Desember");

    $split = explode('-', explode(' ', $date)[0]);
    $tgl = $split[2];
    $bulan = $split[1];
    $tahun = $split[0];

    $result = $tgl . " " . $BulanIndo[(int)$bulan - 1] . " " . $tahun;

    return ($result);
}

ini_set("memory_limit", "20000M");
$mpdf = new mPDF('c', 'A4', '', '', 15, 15, 10, 5, 5, 5);
//$mpdf=new mPDF('c','A5-L','','',12,12,2,5,5,5);

$mpdf->mirrorMargins = 10;  // Use different Odd/Even headers and footers and mirror margins
$tanggal = date("d-m-Y", strtotime($pemeriksaan->waktu_pemeriksaan));

$html = '<table class="sakit" style="text-align: center;">
   <tr >
      <td width="19%"><img width="90px" style="padding-right:20px;"  src="' . base_url() . 'assets/img/klinik/' . $klinik->foto . '" class="user-image" alt="User Image"></td>l
      <td width="82%" style="text-align: center;">
         <p style="text-align: center; font-size: 20px"><strong>' . strtoupper($klinik->nama) . '</strong></p>
         <p style="text-align: center;"><strong>' . $klinik->alamat . ' Telp. ' . $klinik->telepon . '</strong></p>
         <p style="text-align: center;"><strong>Email: ' . $klinik->email . '</strong></p>
         <p style="text-align: center;"><strong>Kabupaten ' . $klinik->kabupaten . '</strong></p>
      </td>
   </tr>
</table>
<hr style="padding-bottom: -20px;"></hr>

<p style="text-align: center;"><strong><u>SURAT KETERANGAN DOKTER</u></strong></p>
<p style="padding-bottom: -15px;">Yang bertanda tangan dibawah ini, ' . $pemeriksaan->nama_dokter . ' selaku dokter pemeriksa di ' . $klinik->nama . ', dengan ini menerangkan :</p><br>
<table>
    <tr>
        <td>Nama</td>    
        <td>:</td>    
        <td>' . $pemeriksaan->nama_pasien . '</td>    
    </tr>
    <tr>
        <td>Tempat tanggal lahir</td>    
        <td>:</td>    
        <td>' . ucwords($pasien->tempat_lahir) . ', ' . DateToIndo($pasien->tanggal_lahir) . '</td>    
    </tr>
    <tr>
        <td>Jenis kelamin</td>    
        <td>:</td>    
        <td>' . ($pasien->jk == 'L' ? 'Laki-laki' : 'Perempuan') . '</td>    
    </tr>
    <tr>
        <td>Pekerjaan</td>    
        <td>:</td>    
        <td>' . $pemeriksaan->pekerjaan . '</td>    
    </tr>
    <tr>
        <td>Alamat</td>    
        <td>:</td>    
        <td>' . $pemeriksaan->alamat . '</td>    
    </tr>
    <tr>
        <td>Pada pemeriksan tanggal</td>    
        <td>:</td>    
        <td>' . DateToIndo($pemeriksaan->waktu_pemeriksaan) . '</td>    
    </tr>
    <tr>
        <td>Dinyatakan dalam keadaan</td>    
        <td>:</td>    
        <td>Sehat</td>    
    </tr>
</table>

<p>Surat keterangan ini dibuat dipergunakan untuk: ' . $untuk . '.</p>
<p>Demikian surat keterangan ini kami buat sebenarnya untuk dapat digunakan sebagaimana mestinya.</p>

<table style="width: 100%; margin-top: 20px">
    <tr>
        <td>Tanda tangan yang diperiksa</td>
        <td></td>
        <td style="text-align: center">Dokter Pemeriksa</td>
    </tr>
    <tr>
        <td><br></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td><br></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td><br></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td>(' . $pemeriksaan->nama_pasien . ')</td>
        <td></td>
        <td style="text-align: center">(' . $pemeriksaan->nama_dokter . ')</td>
    </tr>
</table>

<table style="margin-top: 20px">
    <tr>
        <th colspan="3" style="text-align: left"><b>Keterangan</b></th>
    </tr>
    <tr>
        <td>Visus mata kanan VOD</td> 
        <td>:</td> 
        <td>' . $vm_kanan . '</td> 
    </tr>
    <tr>
        <td>Visus mata kiri VOS</td> 
        <td>:</td> 
        <td>' . $vm_kiri . '</td> 
    </tr>
    <tr>
        <td>Telinga Kanan</td> 
        <td>:</td> 
        <td>' . $telinga_kanan . '</td> 
    </tr>
    <tr>
        <td>Telinga Kiri</td> 
        <td>:</td> 
        <td>' . $telinga_kiri . '</td> 
    </tr>
    <tr>
        <td>Tes Buta Warna</td> 
        <td>:</td> 
        <td>' . $tes_buta_warna . '</td> 
    </tr>
    <tr>
        <td>Golongan Darah</td> 
        <td>:</td> 
        <td>' . $goldar . '</td> 
    </tr>          
    <tr>
        <td>Tinggi Badan</td> 
        <td>:</td> 
        <td>' . $pemeriksaan->tb . 'cm</td> 
    </tr>      
    <tr>
        <td>Berat Badan</td> 
        <td>:</td> 
        <td>' . $pemeriksaan->bb . ' Kg</td> 
    </tr>        
    <tr>
        <td>Tensi</td> 
        <td>:</td> 
        <td>' . $pemeriksaan->td . '</td>
    </tr>            
    <tr>
        <td>Nadi</td> 
        <td>:</td> 
        <td>' . $pemeriksaan->n . 'x/mnt</td>
    </tr>
</table>  
       
<p><b>Catatan</b>: Surat Keterangan Sehat ini Berlaku selama <b>1 Bulan</b> Mulai saat dibuat. Di nyatakan Sehat Jasmani dan Rohani</p>
';

$mpdf->WriteHTML($html);
//$mpdf->SetHTMLFooter($footer);
$mpdf->SetHTMLFooter($footer, 'E');

//$mpdf->Output();
$mpdf->Output("Surat Sehat $pemeriksaan->nama_pasien.pdf", 'D');
exit;

//echo $html;
?>