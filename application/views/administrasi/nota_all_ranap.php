<div class="row">
    <div class="col-sm-12">
        <h4 style="text-align: center"><strong><?=$title?></strong></h4>
        <?php
            $total_tindakan = 0;
            $tindakans = [];
            foreach ($biaya as $b) {
                if ($b->jenis_biaya == 'Tindakan') {
                    foreach ($b->tindakan as $t) {
                        $tindakans[] = $t;
                    }
                }
            }
        ?>
        <?php if ($tindakans) : ?>
            <table class="table table-bordered">
                <tbody>
                <tr>
                    <th style="width: 10px">#</th>
                    <th>Tindakan</th>
                    <th class="text-right">Biaya</th>
                </tr>
                <?php foreach ($tindakans as $key => $value) { ?>
                    <tr>
                        <td><?=$key + 1?></td>
                        <td><?= $value->nama ?>
                            <input type="hidden" name="nama_tindakan_<?=$suffix?>[]" value="<?= $value->nama ?>">
                        </td>
                        <td align="right">
                            <?= harga($value->tarif_pasien); ?>
                            <input type="hidden" name="tarif_pasien_<?=$suffix?>[]" value="<?= $value->tarif_pasien ?>">
                        </td>
                    </tr>
                    <?php
                    $total_tindakan += $value->tarif_pasien;
                }
                ?>
                <tr>
                    <td align="right" colspan="2"><strong>Total: </strong></td>
                    <td align="right"><strong><?=harga($total_tindakan)?></strong></td>
                </tr>
                </tbody>
            </table>
        <?php endif; ?>

        <?php
            $total_obat = 0;
            $obats = [];
            foreach ($resep as $r) {
                if ($r->obat) {
                    foreach ($r->obat as $o) {
                        $obats[] = $o;
                    }
                }
            }
        ?>
        <?php if ($obats) : ?>
            <table class="table table-bordered">
                <tbody>
                <tr>
                    <th style="width: 10px">#</th>
                    <th>Obat</th>
                    <th>jumlah</th>
                    <th class="text-right">Harga Satuan</th>
                    <th class="text-right">Biaya</th>
                </tr>
                <?php foreach ($obats as $key => $value) { ?>
                    <tr>
                        <td><?=$key + 1?></td>
                        <td><?= $value->nama ?><input type="hidden" name="nama_obat_<?=$suffix?>[]" value="<?= $value->nama ?>"></td>
                        <td><?= $value->jumlah ?><input type="hidden" name="jumlah_satuan_<?=$suffix?>[]" value="<?= $value->jumlah ?>"></td>
                        <td class="text-right"><?= harga($value->harga_jual); ?>
                            <input type="hidden" name="harga_jual_<?=$suffix?>[]" value="<?= $value->harga_jual ?>">
                        </td>
                        <td class="text-right"><?= harga($value->harga_jual * $value->jumlah); ?>
                            <input type="hidden" name="subtotal_obat_<?=$suffix?>[]" value="<?= $value->harga_jual * $value->jumlah; ?>">
                        </td>
                    </tr>
                    <?php $total_obat += ($value->harga_jual * $value->jumlah);
                } ?>
                <tr>
                    <td class="text-right" colspan="4"><strong>Total: </strong></td>
                    <td class="text-right"><strong><?=harga($total_obat)?></strong></td>
                </tr>
                </tbody>
            </table>
        <?php endif; ?>

        <?php
            $total_obatracik = 0;
            $racikans = [];
            foreach ($resep as $r) {
                if ($r->obat_racik) {
                    foreach ($r->obat_racik as $o) {
                        $racikans[] = $o;
                    }
                }
            }
        ?>
        <?php if ($racikans) : ?>
            <table class="table table-bordered">
            <tbody>
            <tr>
                <th style="width: 10px">#</th>
                <th>Obat Racik</th>
                <th class="text-right">Biaya</th>
            </tr>
            <?php foreach ($racikans as $key => $value) { ?>
                <tr>
                    <td><?=$key + 1?></td>
                    <td>
                        <table class="table table-condensed" style="margin-bottom: 0 !important;">
                            <tbody>
                            <tr>
                                <th>Obat</th>
                                <th>Jumlah</th>
                                <th class="text-right">Harga Satuan</th>
                                <th class="text-right">Total</th>
                            </tr>
                            <?php foreach ($value->detail as $k => $v) { ?>
                                <tr>
                                    <td><?= $v->nama ?></td>
                                    <td><?= $v->jumlah ?></td>
                                    <td class="text-right"><?= harga($v->harga_jual); ?></td>
                                    <td class="text-right"><?= harga($v->harga_jual * $v->jumlah); ?></td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </td>
                    <td class="text-right align-bottom" style="vertical-align: bottom !important;">
                        <?= harga($value->total); ?>
                        <input type="hidden" name="total_racikan_<?=$suffix?>[]" value="<?= $value->total; ?>">
                    </td>
                </tr>

                <?php $total_obatracik += $value->total; ?>
            <?php } ?>
            <tr>
                <td class="text-right" colspan="2"><strong>Total: </strong></td>
                <td class="text-right"><strong><?=harga($total_obatracik)?></strong></td>
            </tr>
            </tbody>
        </table>
        <?php endif; ?>

        <table class="table table-bordered">
            <tbody>
            <tr>
                <th style="width: 10px">#</th>
                <th>Jasa Racik</th>
                <th>
                    <input type="text" style="text-align: right;" autocomplete="off"
                           class="form-control" id="<?=$jasa_racik_id?>" name="<?=$jasa_racik_name?>"
                           value="<?= $bayar ? $bayar->jasa_racik : $jasa_racik ?>"
                           <?= $bayar ? ' readonly ' : '' ?>
                           onkeyup="setTotal();">
                </th>
            </tr>
            </tbody>
        </table>

        <?php
            $total_bahan = 0;
            $bahans = [];
            foreach ($bahan_habis_pakai as $bhp) {
                if ($bhp->bahan) {
                    foreach ($bhp->bahan as $b) {
                        $bahans[] = $b;
                    }
                }
            }
        ?>
        <?php if ($bahans) : ?>
            <table class="table table-bordered">
            <tbody>
            <tr>
                <th style="width: 10px">#</th>
                <th>Bahan Habis Pakai</th>
                <th>jumlah</th>
                <th class="text-right">Harga Satuan</th>
                <th class="text-right">Biaya</th>
            </tr>
            <?php foreach ($bahans as $key => $value) { ?>
                <tr>
                    <td><?=$key + 1?></td>
                    <td><?= $value->nama ?><input type="hidden" name="nama_bahan_<?=$suffix?>[]" value="<?= $value->nama ?>"></td>
                    <td><?= $value->jumlah ?><input type="hidden" name="jumlah_satuan_bahan_<?=$suffix?>[]" value="<?= $value->jumlah ?>"></td>
                    <td align="right">
                        <?= harga($value->harga_jual); ?>
                        <input type="hidden" name="harga_jual_bahan_<?=$suffix?>[]" value="<?= $value->harga_jual ?>">
                    </td>
                    <td align="right">
                        <?= harga($value->harga_jual * $value->jumlah); ?>
                        <input type="hidden" name="subtotal_bahan_<?=$suffix?>[]" value="<?= $value->harga_jual * $value->jumlah; ?>">
                    </td>
                </tr>
                <?php $total_bahan += ($value->harga_jual * $value->jumlah);
            } ?>
            <tr>
                <td class="text-right" colspan="4"><strong>Total: </strong></td>
                <td class="text-right"><strong><?=harga($total_bahan)?></strong></td>
            </tr>
            </tbody>
        </table>
        <?php endif; ?>

        <?php if ($makanan_count_list) : ?>
            <table class="table table-bordered">
            <tbody>
            <tr>
                <th style="width: 10px">#</th>
                <th>Makanan</th>
                <th>jumlah</th>
                <th class="text-right">Harga Satuan</th>
                <th class="text-right">Biaya</th>
            </tr>
            <?php
            $total_makanan = 0;
            $no = 1;
            foreach ($makanan_count_list as $key => $value) { ?>
                <tr>
                    <td><?=$no++?></td>
                    <td><?= $value->nama ?><input type="hidden" name="nama_makanan_<?=$suffix?>[]" value="<?= $value->nama ?>"></td>
                    <td><?= $value->jumlah ?><input type="hidden" name="jumlah_satuan_makanan_<?=$suffix?>[]" value="<?= $value->jumlah ?>"></td>
                    <td align="right">
                        <?= harga($value->harga); ?>
                        <input type="hidden" name="harga_jual_makanan_<?=$suffix?>[]" value="<?= $value->harga ?>">
                    </td>
                    <td align="right">
                        <?= harga($value->harga * $value->jumlah); ?>
                        <input type="hidden" name="subtotal_makanan_<?=$suffix?>[]" value="<?= $value->harga * $value->jumlah; ?>">
                    </td>
                </tr>
                <?php $total_makanan += ($value->harga * $value->jumlah);
            } ?>
            <tr>
                <td class="text-right" colspan="4"><strong>Total: </strong></td>
                <td class="text-right"><strong><?=harga($total_makanan)?></strong></td>
            </tr>
            </tbody>
        </table>
        <?php endif; ?>

        <?php if ($title == 'Rawat Inap' || $title == 'Perinatologi' || $title == 'HCU') : ?>
            <table class="table table-bordered">
            <tbody>
            <tr>
                <th style="width: 10px">#</th>
                <th>Bed</th>
                <th>Waktu</th>
                <th class="text-right">Harga</th>
                <th class="text-right">Biaya</th>
            </tr>
            <?php
            if (!function_exists('getHowManyDays')) {
                function getHowManyDays($start, $end, $add)
                {
                    $d1 = date_create(date('Y-m-d', strtotime($start)));
                    $d2 = date_create(date('Y-m-d', strtotime($end)));
                    $waktu = (int)date_diff($d1, $d2)->format("%a");
                    if ($add || $waktu == 0) {
                        $waktu++;
                    }
                    return $waktu;
                }
            }

            $total_biaya_bed = 0;
            $no = 1;
            foreach ($beds as $k => $v) : ?>
                <tr>
                    <td><?=$no++?></td>
                    <td><?=$v['bed']['name'].' - '.$v['bed']['bedgroup']?></td>
                    <td>
                        <?php
                        $count = getHowManyDays(
                            $v['start'],
                            $v['end'] == 'now' ? date("Y-m-d H:i") : $v['end'],
                            $k == 0
                        );
                        echo $count;
                        ?> hari<br>
                        <i style="font-size: 12px;">
                            Tgl masuk: <?=date( 'd-m-Y H:i', strtotime($v['start']))?><br>
                            Tgl keluar: <?=$v['end'] == 'now' ? '-' : date( 'd-m-Y H:i', strtotime($v['end']))?>
                        </i>
                    </td>
                    <td class="text-right"><?=harga($v['bed']['group_price'])?></td>
                    <td align="right">
                        <?php
                        $jml = $v['bed']['group_price'] * $count;
                        $total_biaya_bed += $jml;
                        echo harga($jml);
                        ?>
                    </td>
                </tr>
            <?php endforeach; ?>
            <tr>
                <td class="text-right" colspan="4"><strong>Total: </strong></td>
                <td class="text-right">
                    <strong><?=harga($total_biaya_bed)?></strong>
                    <input type="hidden" name="biaya_bed_<?=$suffix?>" value="<?=$total_biaya_bed?>">
                </td>
            </tr>
            </tbody>
        </table>
        <?php endif; ?>
    </div>
</div>