<head>
    <title>Nota</title>
</head>

<?php

function DateToIndo($date)
{
    $BulanIndo = array("Januari", "Februari", "Maret", "April",
        "Mei", "Juni", "Juli", "Agustus", "September", "Oktober",
        "November", "Desember");

    $split = explode('-', explode(' ', $date)[0]);
    $tgl = $split[2];
    $bulan = $split[1];
    $tahun = $split[0];

    $result = $tgl . " " . $BulanIndo[(int)$bulan - 1] . " " . $tahun;

    return ($result);
}

ini_set("memory_limit", "20000M");
$mpdf = new mPDF('c', 'A4', '', '', 20, 15, 60, 5, 5, 5);
//$mpdf=new mPDF('c','A5-L','','',12,12,2,5,5,5);

$mpdf->mirrorMargins = 10;  // Use different Odd/Even headers and footers and mirror margins
$tanggal = DateToIndo($pemeriksaan->waktu_pemeriksaan);


$mpdf->SetHTMLHeader('<img src="'. base_url() . 'assets/img/klinik/header2.png"/>');
$mpdf->SetHTMLFooter('<img src="'. base_url() . 'assets/img/klinik/footer.png"/>');
$html = 
'
<p style="text-align: center;"><strong>PENGANTAR RAWAT INAP</strong></p>
<p style="padding-bottom: -15px;">Kepada Yang Terhormat</p>
<p style="padding-bottom: -15px;">Dokter / Bidan / Perawat jaga</p>
<p>'.$klinik->nama.'</p>
<p style="padding-bottom: -15px;">Dengan Hormat,</p>
<table>
    <tr>
        <td>Mohon MRS</td>    
        <td>:</td>    
        <td></td>    
    </tr>
    <tr>
        <td>Nama</td>    
        <td>:</td>    
        <td>' . $pemeriksaan->nama_pasien . ', Umur: '.$pemeriksaan->usia. '</td>    
    </tr>
    <tr>
        <td style="vertical-align: top">Diagnosa</td>    
        <td style="vertical-align: top">:</td>    
        <td>' . $diagnosa . '</td>    
    </tr>
    <tr>
        <td style="vertical-align: top">Rencana</td>    
        <td style="vertical-align: top">:</td>    
        <td>' . $rencana . '</td>    
    </tr>
</table>
<p style="padding-left: 450px;">' . $klinik->kabupaten . ', ' . $tanggal . '</p>
<p style="padding-left: 450px;">Dokter Pemeriksa</p>
<br><br><br>
<p style="padding-left: 450px;">( ' . $pemeriksaan->nama_dokter . ' )</p>
';

$mpdf->WriteHTML($html);
//$mpdf->SetHTMLFooter($footer);
$mpdf->SetHTMLFooter($footer, 'E');

//$mpdf->Output();
$mpdf->Output("Surat Pengantar Rawat Inap $pemeriksaan->nama_pasien.pdf", 'D');
exit;

//echo $html;
?>