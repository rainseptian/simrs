<?php function DateToIndo($date)
{
    $BulanIndo = array("Januari", "Februari", "Maret", "April",
        "Mei", "Juni", "Juli", "Agustus", "September", "Oktober",
        "November", "Desember");

// memisahkan format tahun menggunakan substring
    $tgl = substr($date, 0, 2);

// memisahkan format bulan menggunakan substring
    $bulan = substr($date, 3, 2);

// memisahkan format tanggal menggunakan substring
    $tahun = substr($date, 6, 4);

    $result = $tgl . " " . $BulanIndo[(int)$bulan - 1] . " " . $tahun;

    return ($result);
} ?>
<link rel="stylesheet"
      href="<?php echo base_url() ?>assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">

<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Surat Sakit
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a href="#">Surat Sehat </a></li>
            <li class="active"> Pasien</li>
        </ol>
    </section>
    <section class="invoice col-md-6">
        <!-- title row -->
        <div class="row">
            <div class="col-xs-12">
                <h2 class="page-header">
                    <i class="fa fa-user"></i> NO. RM <?php echo $pemeriksaan->no_rm ?>
                    <small class="pull-right"><strong>Tanggal: <?php echo $pemeriksaan->waktu_pemeriksaan ?></strong></small>
                </h2>
            </div>
        </div>
        <form enctype="multipart-data" method="POST" action="<?php echo base_url() ?>Administrasi/surat_sakit_print/<?php echo $pemeriksaan->id ?>">
            <div class="row invoice-info">
                <div class="col-sm-12 invoice-col">
                    <p>Yang bertanda tangan di bawah ini, menerangkan dengan sebenarnya bahwa:</p>
                    <table class="table">
                        <tr>
                            <td>Nama Pasien</td>
                            <td>:</td>
                            <td><strong><?php echo $pemeriksaan->nama_pasien ?></strong></td>
                        </tr>
                        <address>
                            <tr>
                                <td>Alamat</td>
                                <td>:</td>
                                <td><?php echo $pemeriksaan->alamat ?></td>
                            </tr>
                            <tr>
                                <td>Umur</td>
                                <td>:</td>
                                <td><?php echo $pemeriksaan->usia ?></td>
                            </tr>
                            <tr>
                                <td>Pekerjaan</td>
                                <td>:</td>
                                <td><?php echo $pemeriksaan->pekerjaan ?></td>
                            </tr>
                        </address>
                    </table>
                </div>
            </div>
            <div class="row invoice-info">
                <div class="col-sm-12 invoice-col">
                    <p>
                        Saat ini benar-benar memerlukan istirahat selama <input class="form-control" type="text" name="jumlah_hari" id="jumlah_hari" value="<?=$surat_form->jumlah_hari?>"> hari,
                        terhitung mulai tanggal <input class="form-control" type="date" name="start_date" id="start_date" value="<?=$surat_form->start_date?>">
                        s/d <input class="form-control" type="date" name="end_date" id="end_date" value="<?=$surat_form->end_date?>">
                    </p>
                    <p>
                        Dengan demikian keterangan ini dibuat untuk digunakan sebagaimana mestinya.
                    </p>
                </div>
            </div>
            <div class="row no-print">
                <div class="col-xs-12">
                    <button type="submit" class="btn btn-default"><i class="fa fa-print"></i> Print</button>
                </div>
            </div>
        </form>
    </section>
    <div class="clearfix"></div>
</div>

<script>
    Date.prototype.addDays = function(days) {
        let date = new Date(this.valueOf());
        date.setDate(date.getDate() + days);
        return date;
    }

    $(function () {
        $('#jumlah_hari').on('keyup', function () {
            const start_date = $('#start_date').val()
            let parts = start_date.split('-')
            let myDate = new Date(parts[0], parts[1] - 1, parts[2]);
            myDate = myDate.addDays(+$(this).val())
            $('#end_date').val(
                myDate.getUTCFullYear() + '-' +
                ('00' + (myDate.getUTCMonth()+1)).slice(-2) + '-' +
                ('00' + myDate.getUTCDate()).slice(-2)
            )
        })
    })
</script>