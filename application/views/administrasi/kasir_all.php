<link rel="stylesheet" href="<?php echo base_url() ?>assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
<style>
    .lbl-jmn {
        cursor: pointer;
    }
</style>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Kasir All
            <small></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a href="#">List Kasir All</a></li>
            <li class="active"> Pasien</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Data Kasir All</h3>&nbsp;&nbsp;
                    </div>
                    <div class="box-body">
                        <?php $warning = $this->session->flashdata('warning');
                        if (!empty($warning)){ ?>
                            <div class="alert alert-warning alert-dismissible">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                <h4><i class="icon fa fa-warning"></i> Alert!</h4>
                                <?php echo $warning ?>
                            </div>
                        <?php } ?>
                        <?php $success = $this->session->flashdata('success');
                        if (!empty($success)){ ?>
                            <div class="alert alert-success alert-dismissible">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                <h4><i class="icon fa fa-success"></i> Success!</h4>
                                <?php echo $success ?>
                            </div>
                        <?php } ?>
                    </div>
                    <div class="box-body">
                        <table id="example2" class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>No</th>
                                <th>NO RM</th>
                                <th>Nama Pasien</th>
                                <th>Jenis</th>
                                <th></th>
                                <th>Nama Dokter</th>
                                <th></th>
                                <th>Status</th>
                                <th>Aksi</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $no=1;
                            foreach ($listPemeriksaan as $key => $row) { ?>
                                <tr>
                                    <td> <?php echo $no++; ?></td>
                                    <td style="width: 100px;" <?= $row->obat_luar ? 'bgcolor="#d9ffc4"' : '' ?>>
                                      <?php echo $row->no_rm; ?>
                                    </td>
                                    <td> <?php echo ucwords($row->nama_pasien); ?></td>
                                    <td> <?php echo str_replace('^^^', '<br>', $row->jenis); ?></td>
                                    <td>
                                        <small>
                                            <?php
                                            if ($row->obat_luar) {
                                                if ($row->jaminan == 'resep_luar') {
                                                    $label = 'label-info';
                                                    $txt = 'Resep Luar';
                                                }
                                                else if ($row->jaminan == 'obat_bebas') {
                                                    $label = 'label-success';
                                                    $txt = 'Obat Bebas';
                                                }
                                                else {
                                                    $label = 'label-danger';
                                                    $txt = 'Obat Internal';
                                                }
                                                echo '<span class="label '.$label.'">'.$txt.'</span>';
                                            }
                                            else { ?>
                                                <?php if (isset($jaminan[$row->jaminan])) : ?>
                                                    <span class="lbl-jmn label <?=$row->jaminan == 'bpjs' ? 'label-primary' : 'label-warning'?>" onclick="change_jaminan(<?=$key?>, 0, '<?=$row->jaminan?>')"><?=strtoupper($row->jaminan)?></span>
                                                <?php else :
                                                    foreach (explode('^^^', $row->jaminan) as $k => $j) {
                                                        echo '<span class="lbl-jmn label '.$jaminan[$j]['class'].'" onclick="change_jaminan('.$key.','.$k.",'".$jaminan[$j]['label']."'".')">'.$jaminan[$j]['label'].'</span><br>';
                                                    } ?>
                                                <?php endif; ?>
                                            <?php } ?>
                                        </small>
                                    </td>
                                    <td> <?php echo ucwords(str_replace('^^^', '<br>', $row->nama_dokter)); ?></td>
                                    <td class="text-center">
                                        <button class="btn btn-success btn-sm" onclick="choose_poli('<?=$row->id?>')">
                                            <i class="fa fa-plus"></i>
                                        </button>
                                    </td>
                                    <td>
                                        <span class="pull-right-container">
                                            <?php
                                            if ($row->status == 'sudah_obat') {
                                                $bg = 'bg-red';
                                                $status = 'sudah periksa & obat';
                                            }
                                            else if ($row->status == 'sudah_periksa') {
                                                $bg = 'bg-orange';
                                                $status = 'sudah lengkap';
                                            }
                                            else if ($row->status == 'sudah_bayar') {
                                                $bg = 'bg-green';
                                                $status = 'sudah bayar';
                                            }
                                            else {
                                                $bg = 'bg-blue';
                                                $status = 'belum';
                                            }

                                            if ($row->obat_luar) {
                                                if ($row->status == 'sudah_obat') {
                                                    $bg = 'bg-red';
                                                    $status = 'belum bayar';
                                                }
                                                else if ($row->status == 'sudah_bayar') {
                                                    $bg = 'bg-green';
                                                    $status = 'sudah bayar';
                                                }
                                            }
                                            ?>
                                            <small class="label pull-right <?=$bg?>">
                                            <?= trim($status) ?>
                                            </small>
                                        </span>
                                    </td>
                                    <td>
                                        <?php
                                        if ($row->obat_luar) : ?>
                                            <a href="<?php echo base_url(); ?>Administrasi/nota_obat_luar/<?php echo $row->id; ?>">
                                                <button type="button" class="btn btn-primary btn-block"><i class="fa fa-dollar"></i> Bayar</button>
                                            </a>
                                            <div style="margin-top: 8px"></div>
                                            <a href="<?php echo base_url(); ?>Administrasi/edit_obat_luar/<?php echo $row->id; ?>">
                                                <button type="button" class="btn btn-warning btn-block"><i class="fa fa-pencil"></i> Edit</button>
                                            </a>
                                        <?php else : ?>
                                            <a href="<?php echo base_url(); ?>Administrasi/nota_all/<?=$row->pasien_id?>/<?=$row->jenis?>/<?=$row->id?>">
                                                <button type="button" class="btn btn-primary btn-block"><i class="fa fa-dollar"></i> Bayar</button>
                                            </a>
                                        <?php endif; ?>
                                    </td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<div class="modal fade" id="choose-jaminan" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header" style="display: flex">
                <h4 class="modal-title" style="flex: 1">Pilih Jenis Pendaftaran</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="container" style="width: 100%">
                    <div class="row">
                        <div class="col-12">
                            <table class="table table-bordered">
                                <tbody>
                                <tr>
                                    <td>Nama Pasien</td>
                                    <td id="nama_pasien"></td>
                                </tr>
                                <tr>
                                    <td>Jenis</td>
                                    <td id="jenis"></td>
                                </tr>
                                </tbody>
                            </table>
                            <div class="form-group">
                                <label for="inputtext3" class="control-label">Jenis Pendaftaran</label>
                                <select id="jaminan" class="abdush-select form-control" name="jaminan" style="width: 100%">
                                    <option value="">--Pilih Jenis Pendaftaran--</option>
                                    <?php foreach ($jaminan as $key => $value) { ?>
                                        <option value="<?= $key ?>"><?= $value['label'] ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-success" id="b-simpan-jenis">
                    Simpan
                </button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="choose" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header" style="display: flex">
                <h4 class="modal-title" id="choose-title" style="flex: 1">Modal title</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <table class="table table-hover" id="tbl-choose">
                    <thead>
                    <tr>
                        <td>No</td>
                        <td>Nama</td>
                        <td>Tambah Tindakan</td>
                        <td>Edit Obat, Racikan, BHP, Tindakan</td>
                    </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="tindakan" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <form id="form-add-tindakan" method="post">
            <div class="modal-content">
                <div class="modal-header" style="display: flex">
                    <h4 class="modal-title" id="tindakan-title" style="flex: 1">Edit Tindakan</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group" style="margin-bottom: 30px">
                        <label for="tindakan[]" class="col-sm-3 control-label">Tindakan</label>
                        <div class="col-sm-9" style="margin-bottom: 10px;">
                            <select class="form-control select2" multiple="multiple"
                                    style="width: 100%"
                                    data-placeholder="Tindakan"
                                    id="tindakan-select" name="tindakan[]">
                            </select>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-primary">Simpan</button>
                </div>
            </div>
        </form>
    </div>
</div>

<script src="<?= base_url() ?>assets/bower_components/select2/dist/js/select2.full.min.js"></script>

<script>
    function format(n) {
        return n.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
    }

    $(function () {
        $('#example1').DataTable();
        $('#example2').DataTable({
            rowReorder: {
                selector: 'td:nth-child(2)'
            },
            'paging'      : true,
            'lengthChange': true,
            'searching'   : true,
            'ordering'    : true,
            'info'        : true,
            'autoWidth'   : true
        })
        $('.select2').select2();
    })

    const list = <?=json_encode($listPemeriksaan)?>;
    const base_url = '<?=base_url()?>'

    function choose_poli(id) {
        const row = list.find(v => v.id === id)
        if (row) {
            $('#choose-title').html(`Edit Tarif Pemeriksaan ${row.nama_pasien}`)
            $('#tbl-choose > tbody').empty()

            row.jenis.split('^^^').forEach((v, k) => {
                const id = row.id.split('^^^')[k]
                const is_poli = v.startsWith('Poli')
                $('#tbl-choose > tbody').append(`
                    <tr>
                        <td>${k + 1}</td>
                        <td>${v}</td>
                        <td>
                            <button class="btn btn-primary btn-sm" id="btn-choose-${id}" data-id="${id}">
                                Pilih
                            </button>
                        </td>
                        <td>
                            ${is_poli ? `<a href="${base_url}Apotek/editResep/${id}/kasir_all" class="btn btn-primary btn-sm">Pilih</a>` : ''}
                        </td>
                    </tr>
                `)

                $(`#btn-choose-${id}`).click(function () {
                    const id = $(this).data('id')

                    $.get(`<?=base_url()?>Administrasi/get_tindakan/${id}/${v.startsWith('Poli') ? 'Poli' : v}`, function(data) {
                        $('#tindakan-select').empty()
                        JSON.parse(data).forEach(v => {
                            $('#tindakan-select').append(`<option value="${v.id}">${v.nama} - Rp ${format(v.tarif_pasien)}</option>`)
                        })
                    })

                    $('#tindakan-title').html(`Tambah Tindakan ${v}`)
                    $('#choose').modal('hide')
                    $('#tindakan').modal('show')

                    const url = `<?=base_url()?>Administrasi/add_tindakan/${id}/${v.startsWith('Poli') ? 'Poli' : v}`
                    $('#form-add-tindakan').attr('action', url)
                })
            })

            $('#choose').modal('show')
        }
    }

    let id = '', jenis = ''

    function change_jaminan(key, k, jaminan) {
        $('#jaminan').val(jaminan.toLowerCase()).change()
        $('#choose-jaminan').modal('show')
        const v = list[key]
        if (v) {
            $('#nama_pasien').html(v.nama_pasien)
            $('#jenis').html(v.jenis.split('^^^')[k])

            id = v.id.split('^^^')[k]
            jenis = v.jenis.split('^^^')[k]
            jenis = jenis.startsWith('Poli -') ? 'Poli' : jenis
        }
    }

    $(function () {
        $('#b-simpan-jenis').click(function () {
            $('#choose-jaminan').modal('hide')
            window.open(`<?=base_url()?>/Pendaftaran/set_jenis/${id}/${$('#jaminan').val()}/${jenis}`,"_self")
        })
    })

</script>
