<head>
    <title>Nota</title>


</head>
<style type="text/css">
    p {
        font-size: 25px;
    }
</style>
<?php

function DateToIndo($date)
{
    $BulanIndo = array("Januari", "Februari", "Maret", "April",
        "Mei", "Juni", "Juli", "Agustus", "September", "Oktober",
        "November", "Desember");

    $split = explode('-', explode(' ', $date)[0]);
    $tgl = $split[2];
    $bulan = $split[1];
    $tahun = $split[0];

    $result = $tgl . " " . $BulanIndo[(int)$bulan - 1] . " " . $tahun;

    return ($result);
}

ini_set("memory_limit", "20000M");
$mpdf=new mPDF('c','A4','','',15,15,10,5,5,5);
//$mpdf = new mPDF('c', 'A5-L', '', '', 10, 10, 4, 4, 4, 4);

$mpdf->mirrorMargins = 10;  // Use different Odd/Even headers and footers and mirror margins
$tanggal = DateToIndo($pemeriksaan->waktu_pemeriksaan);

$html = ' 
      </style>
<table class="sakit" style="text-align: center;">
   <tr >
      <td width="19%"><img width="90px" style="padding-right:20px;"  src="' . base_url() . 'assets/img/klinik/' . $klinik->foto . '" class="user-image" alt="User Image"></td>l
      <td width="82%" style="text-align: center;">
         <p style="text-align: center; font-size: 20px"><strong>' . strtoupper($klinik->nama) . '</strong></p>
         <p style="text-align: center;"><strong>' . $klinik->alamat . ' Telp. ' . $klinik->telepon . '</strong></p>
         <p style="text-align: center;"><strong>Email: ' . $klinik->email . '</strong></p>
         <p style="text-align: center;"><strong>Kabupaten ' . $klinik->kabupaten . '</strong></p>
        
      </td>
        
   </tr>
</table>
<hr />

<p style="text-align: center; padding-bottom: -15px;"><strong><u>SURAT KETERANGAN ISTIRAHAT</u></strong></p>
<p style="padding-left: 450px; padding-top: 15px; padding-bottom: 15px;">' . $klinik->kota . ', ' . $tanggal . '</p>
<p style="padding-bottom: -15px;">Yang bertanda tangan di bawah ini, menerangkan dengan sebenarnya bahwa:</p>
<p style="padding-left: 30px; padding-bottom: -15px;">Nama&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; : ' . $pemeriksaan->nama_pasien . '</p>
<p style="padding-left: 30px;  padding-bottom: -15px;">Umur&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; : ' . $pemeriksaan->usia . '</p>
<p style="padding-left: 30px;  padding-bottom: -15px;">Pekerjaan&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;: ' . $pemeriksaan->pekerjaan . '</p>
<p style="padding-left: 30px;  padding-bottom: -15px;">Alamat&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;: ' . $pemeriksaan->alamat . '</p>
<p>
    Saat ini benar-benar memerlukan istirahat selama '.$jumlah_hari.' hari,
    terhitung mulai tanggal '.DateToIndo($start_date).' s/d '.DateToIndo($end_date).'
</p>
<p>
    Dengan demikian keterangan ini dibuat untuk digunakan sebagaimana mestinya.
</p>

<p style="padding-left: 450px; padding-top: 25px">Dokter Pemeriksa</p>
<p style="padding-left: 450px;">&nbsp;</p>

<p style="padding-left: 450px;">( ' . $pemeriksaan->nama_dokter . ' )</p>
';

$mpdf->WriteHTML($html);
//$mpdf->SetHTMLFooter($footer);
$mpdf->SetHTMLFooter($footer, 'E');

//$mpdf->Output();
$mpdf->Output("Surat Sakit $pemeriksaan->nama_pasien.pdf", 'D');
//exit;
?>