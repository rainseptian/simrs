<?php
function harga($num)
{
    return number_format($num, 2, ',', '.');
}
?>
<link rel="stylesheet" href="<?= base_url() ?>assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Kwitansi
            <small>#007612</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a href="#">Kwitansi Pembayaran </a></li>
            <li class="active"> Pasien</li>
        </ol>
    </section>
    <?php $warning = $this->session->flashdata('warning');
    if (!empty($warning)){ ?>
        <div class="alert alert-warning alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <h4><i class="icon fa fa-warning"></i> Alert!</h4>
            <?php echo $warning ?>
        </div>
    <?php } ?>
    <?php $success = $this->session->flashdata('success');
    if (!empty($success)){ ?>
        <div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <h4><i class="icon fa fa-success"></i> Success!</h4>
            <?php echo $success ?>
        </div>
    <?php } ?>
    <section class="invoice">
        <div class="row">
            <div class="col-xs-12">
                <i class="fa fa-user"></i> NO. RM <?= $pasien->no_rm ?>
                </h2>
            </div>
        </div>
        <form enctype="multipart-data" method="POST" action="<?= base_url() ?>Administrasi/submit_nota_all">
            <input type="hidden" name="total_input" id="total_input" value="">
            <input type="hidden" name="kembalian_input" id="kembalian_input" value="">
            <input type="hidden" name="total_jasa_racik" id="total_jasa_racik">
            <input type="hidden" name="jenis" value="<?=$jenis?>">
            <input type="hidden" name="id" value="<?=$id?>">
            <input type="hidden" name="pasien_id" value="<?=$pasien->id?>">

            <div class="row invoice-info">
                <div class="col-sm-12 invoice-col">
                    Nama Pasien: <strong><?= $pasien->nama ?></strong><br>
                    Jenis Pendaftaran: <?= $jenis_pendaftaran ?><br>
                    Tanggal Pembayaran: <?= $sudah_bayar ? date_format(date_create($bayar->created_at), "d-F-Y H:i") : date('d-F-Y') ?><br>
                    <br>
                    <address>
                        Alamat : <?= $pasien->alamat ?><br>
                        Usia: <?= $pasien->usia ?><br>
                        Telepon: <?= $pasien->telepon ?><br>
                    </address>
                </div>
                <div class="col-xs-12">
                    <?php
                    $jasa_racik_ids = [];
                    if (isset($Poli)) {
                        foreach ($Poli as $k => $v) {
                            foreach ($v as $kk => $vv) {
                                $str = str_replace(" ", "", $k);
                                $str = str_replace("-", "", $str);
                                $jr = 'jr_poli_'.$str.'_'.$kk;
                                $jasa_racik_ids[] = $jr;
                                $this->load->view('administrasi/nota_all_poli', array_merge($vv['detail'], ['jasa_racik_id' => $jr]));
                            }
                        }
                    }
                    if (isset($Ruang)) {
                        foreach ($Ruang as $k => $v) {
                            foreach ($v as $kk => $vv) {
                                $id_jr = "jr_{$k}_{$kk}";
                                $jasa_racik_ids[] = $id_jr;
                                $this->load->view('administrasi/nota_all_ranap', array_merge($vv['detail'], ['jasa_racik_id' => $id_jr]));
                            }
                        }
                    }
                    if (isset($Ambulance)) {
                        foreach ($Ambulance as $k => $v) {
                            foreach ($v as $kk => $vv) {
                                $amb_id = "amb_{$k}_{$kk}";
                                $this->load->view('administrasi/nota_all_ambulance', array_merge($vv['detail'], ['amb_id' => $amb_id]));
                            }
                        }
                    }

//                    if (isset($Ranap)) {
//                        $jasa_racik_ids[] = 'jr_ranap';
//                        $this->load->view('administrasi/nota_all_ranap', array_merge($Ranap['detail'], ['jasa_racik_id' => 'jr_ranap']));
//                    }
//                    if (isset($VK)) {
//                        $jasa_racik_ids[] = 'jr_vk';
//                        $this->load->view('administrasi/nota_all_ranap', array_merge($VK['detail'], ['jasa_racik_id' => 'jr_vk']));
//                    }
//                    if (isset($OK)) {
//                        $jasa_racik_ids[] = 'jr_ok';
//                        $this->load->view('administrasi/nota_all_ranap', array_merge($OK['detail'], ['jasa_racik_id' => 'jr_ok']));
//                    }
//                    if (isset($HCU)) {
//                        $jasa_racik_ids[] = 'jr_hcu';
//                        $this->load->view('administrasi/nota_all_ranap', array_merge($HCU['detail'], ['jasa_racik_id' => 'jr_hcu']));
//                    }
//                    if (isset($Perinatologi)) {
//                        $jasa_racik_ids[] = 'jr_perinatologi';
//                        $this->load->view('administrasi/nota_all_ranap', array_merge($Perinatologi['detail'], ['jasa_racik_id' => 'jr_perinatologi']));
//                    }
                    ?>
                    <h4 class="box-title ptt10" style="text-align: center"><strong>Ringkasan Billing</strong></h4>
                    <div class="table-responsive" style="border: 1px solid #dadada;border-radius: 2px; padding: 10px;">
                        <table class="nobordertable table table-striped table-responsive">
                            <tr>
                                <th>Total Tindakan</th>
                                <td class="text-right fontbold20"><?php echo harga($total_tindakan) ?></td>
                            </tr>
                            <tr>
                                <th>Total Obat</th>
                                <td class="text-right fontbold20"><?php echo harga($total_obat) ?></td>
                            </tr>
                            <tr>
                                <th>Total Obat Racik</th>
                                <td class="text-right fontbold20"><?php echo harga($total_obatracik) ?></td>
                            </tr>
                            <tr>
                                <th>Jasa Racik</th>
                                <td class="text-right fontbold20" id="jasa_racik_txt">0</td>
                            </tr>
                            <tr>
                                <th>Total Bahan Habis Pakai</th>
                                <td class="text-right fontbold20"><?php echo harga($total_bahan) ?></td>
                            </tr>
                            <tr>
                                <th>Total Makanan</th>
                                <td class="text-right fontbold20"><?php echo harga($total_makanan) ?></td>
                            </tr>
                            <tr>
                                <th>Total Biaya Bed</th>
                                <td class="text-right fontbold20"><?php echo harga($total_biaya_bed) ?></td>
                            </tr>
                            <?php if (isset($Ambulance)) : ?>
                                <tr>
                                    <th>Total Biaya Ambulance</th>
                                    <td class="text-right fontbold20">
                                        <?php echo harga($Ambulance['Ambulance'][0]['detail']['tarif']) ?>
                                        <input type="hidden" name="biaya_ambulance" value="<?=$Ambulance['Ambulance'][0]['detail']['tarif']?>">
                                    </td>
                                </tr>
                            <?php endif; ?>
                            <tr>
                                <th style="vertical-align: middle !important;">Jumlah Tagihan</th>
                                <td class="text-right fontbold20" id="jumlah"></td>
                            </tr>
                            <tr>
                                <th style="vertical-align: middle !important;">Diskon</th>
                                <td class="text-right fontbold20">
                                    <input type="text" style="text-align: right;" autocomplete="off"
                                           class="form-control" id="diskon" name="diskon"
                                        <?= $sudah_bayar ? ' value="'.$bayar->diskon.'" readonly ' : '' ?>
                                           onkeyup="setTotal();">
                                </td>
                            </tr>
                            <tr>
                                <th style="vertical-align: middle !important;">Total Tagihan</th>
                                <td class="text-right fontbold20" id="total">
                                    <?= $sudah_bayar ? $bayar->total : '' ?>
                                </td>
                            </tr>
                            <tr>
                                <th style="vertical-align: middle !important;">Bayar</th>
                                <td class="text-right fontbold20">
                                    <input type="text" style="text-align: right;" autocomplete="off"
                                           class="form-control" id="bayar" name="bayar"
                                           required
                                        <?= $sudah_bayar ? ' value="'.$bayar->bayar.'" readonly ' : '' ?>
                                           onkeyup="setKembalian();">
                                </td>
                            </tr>
                            <tr>
                                <th style="vertical-align: middle !important;">Kembalian</th>
                                <td class="text-right fontbold20" id="kembalian">
                                    <?= $sudah_bayar ? $bayar->kembalian : ''?>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="row no-print">
                        <div class="col-xs-12">
                            <a href="<?=base_url().'Administrasi/kasir_all'?>" class="btn btn-default" style="margin: 3px;">
                                <i class="fa fa-back"></i>
                                Kembali
                            </a>
                            <button type="submit" class="btn <?=$sudah_bayar ? 'btn-disabled' : 'btn-success'?> pull-right" style="margin: 3px;"
                                <?=$sudah_bayar ? 'disabled' : ''?>>
                                <i class="fa fa-credit-card"></i>
                                Submit Pembayaran
                            </button>
                            <a id="bprint" <?= $sudah_bayar ? 'href="'.base_url()."Administrasi/print_nota_all/$pasien->id/$jenis/$id/$bayar->id".'"' : '' ?>
                               class="btn <?=$sudah_bayar ? 'btn-primary' : 'btn-disabled'?> pull-right"
                               style="margin: 3px;"
                               target="_blank"
                               rel="noopener noreferrer">
                                <i class="fa fa-print"></i>
                                Cetak Pembayaran
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </section>
    <div class="clearfix"></div>
</div>

<script>

    $(function () {
        setTotal();
        // set_total();
    });

    function getTotalJasaRacik() {
        const jasa_racik_ids = <?php echo json_encode($jasa_racik_ids);?>;
        let tot = 0;

        jasa_racik_ids.forEach(v => {
            tot += parseInt($('#' + v).val()) || 0;
        });

        $('#jasa_racik_txt').html(formatMoney(tot));
        $('#total_jasa_racik').val(tot);

        return tot;
    }

    function setTotal() {
        const total_tindakan = <?=$total_tindakan?>;
        const total_obat = <?=$total_obat?>;
        const total_obatracik = <?=$total_obatracik?>;
        const total_bahan = <?=$total_bahan?>;
        const total_makanan = <?=$total_makanan?>;
        const total_biaya_bed = <?=$total_biaya_bed?>;
        const total_biaya_ambulance = <?=isset($Ambulance) ? $Ambulance['Ambulance'][0]['detail']['tarif'] : 0?>;
        let jasa_racik = getTotalJasaRacik();
        let diskon = parseInt($('#diskon').val()) || 0;
        let jumlah = total_tindakan + total_obat + total_obatracik + total_bahan + total_makanan + total_biaya_bed + jasa_racik + total_biaya_ambulance

        let total = jumlah - diskon;
        $('#jumlah').html(formatMoney(jumlah));
        $('#total').html(formatMoney(total));
        $('#total_input').val(total);

        setKembalian();
    }

    function setKembalian() {
        var total = +$('#total_input').val()
        var bayar = +$('#bayar').val()
        var kembalian = bayar - total;

        $('#kembalian').html(formatMoney(kembalian) || 0);
        $('#kembalian_input').val(kembalian);
    }

    function set_jumlah() {
        var jasa_racik = parseInt($('#jasa_racik').val());
        if (isNaN(jasa_racik)) {
            jasa_racik = 0;
        }
        var tindakan_n_obat_n_obat_racik = parseInt($('#tindakan_n_obat_n_obat_racik').val());
        if (isNaN(tindakan_n_obat_n_obat_racik)) {
            jumlah = 0;
        }
        $('#jumlah').val(jasa_racik + tindakan_n_obat_n_obat_racik);
        set_total();
    }

    function set_total() {
        var jumlah = parseInt($('#jumlah').val());
        if (isNaN(jumlah)) {
            jumlah = 0;
        }
        var diskon = parseInt($('#diskon').val());
        if (isNaN(diskon)) {
            diskon = 0;
        }
        var total = jumlah - diskon;
        $('#total').val(total);

        set_kembalian();
    }

    function set_kembalian() {
        var total = parseInt($('#total').val());
        var bayar = parseInt($('#bayar').val());
        var kembalian = bayar - total;

        if (!isNaN(kembalian)) {
            $('#kembalian').val(kembalian);
        }
    }

    $(function () {
        $('#example1').DataTable()
        $('#example2').DataTable({
            'paging': true,
            'lengthChange': false,
            'searching': false,
            'ordering': true,
            'info': true,
            'autoWidth': false
        })
    })
</script>
