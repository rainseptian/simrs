<link rel="stylesheet"
      href="<?php echo base_url() ?>assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">

<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Surat Sakit
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a href="#">Surat Sehat </a></li>
            <li class="active"> Pasien</li>
        </ol>
    </section>
    <section class="invoice col-md-6">
        <!-- title row -->
        <div class="row">
            <div class="col-xs-12">
                <h2 class="page-header">
                    <i class="fa fa-user"></i> NO. RM <?php echo $pemeriksaan->no_rm ?>
                    <small class="pull-right"><strong>Tanggal: <?php echo $pemeriksaan->waktu_pemeriksaan ?></strong></small>
                </h2>
            </div>
        </div>
        <form enctype="multipart-data" method="POST" action="<?php echo base_url() ?>Administrasi/surat_ranap_print/<?php echo $pemeriksaan->id ?>">
            <div class="row">
                <div class="col-sm-12">
                    <p>Kepada Yang Terhormat</p>
                    <p>Dokter / Bidan / Perawat jaga</p>
                    <p><?=$klinik->nama?></p>
                    <br>
                    <p>Dengan Hormat,</p>
                    <table class="table">
                        <tr>
                            <td>Mohon MRS</td>
                            <td>:</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>Nama</td>
                            <td>:</td>
                            <td><?= $pemeriksaan->nama_pasien ?></td>
                        </tr>
                        <tr>
                            <td>Diagnosa</td>
                            <td>:</td>
                            <td><textarea class="form-control" name="diagnosa" rows="3"><?=$surat_form->diagnosa?></textarea></td>
                        </tr>
                        <tr>
                            <td>Rencana</td>
                            <td>:</td>
                            <td><textarea class="form-control" name="rencana" rows="3"><?=$surat_form->rencana?></textarea></td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="row no-print">
                <div class="col-xs-12">
                    <button type="submit" class="btn btn-default"><i class="fa fa-print"></i> Print</button>
                </div>
            </div>
        </form>
    </section>
    <div class="clearfix"></div>
</div>