<head>
    <title>Nota</title>
</head>

<?php

function DateToIndo($date)
{
    $BulanIndo = array("Januari", "Februari", "Maret", "April",
        "Mei", "Juni", "Juli", "Agustus", "September", "Oktober",
        "November", "Desember");

    $split = explode('-', explode(' ', $date)[0]);
    $tgl = $split[2];
    $bulan = $split[1];
    $tahun = $split[0];

    $result = $tgl . " " . $BulanIndo[(int)$bulan - 1] . " " . $tahun;

    return ($result);
}

ini_set("memory_limit", "20000M");
$mpdf = new mPDF('c', 'A4', '', '', 15, 15, 10, 5, 5, 5);
//$mpdf=new mPDF('c','A5-L','','',12,12,2,5,5,5);

$mpdf->mirrorMargins = 10;  // Use different Odd/Even headers and footers and mirror margins
$tanggal = DateToIndo($pemeriksaan->waktu_pemeriksaan);

$html = '<table class="sakit" style="text-align: center;">
   <tr >
      <td width="19%"><img width="90px" style="padding-right:20px;"  src="' . base_url() . 'assets/img/klinik/' . $klinik->foto . '" class="user-image" alt="User Image"></td>l
      <td width="82%" style="text-align: center;">
         <p style="text-align: center; font-size: 20px"><strong>' . strtoupper($klinik->nama) . '</strong></p>
         <p style="text-align: center;"><strong>' . $klinik->alamat . ' Telp. ' . $klinik->telepon . '</strong></p>
         <p style="text-align: center;"><strong>Email: ' . $klinik->email . '</strong></p>
         <p style="text-align: center;"><strong>Kabupaten ' . $klinik->kabupaten . '</strong></p>
      </td>
   </tr>
</table>
<hr style="padding-bottom: -20px;"></hr>

<p style="text-align: center;"><strong><u>SURAT RUJUKAN</u></strong></p>
<p>
'.$klinik->kabupaten.', '.$tanggal.'<br><br>
Kepada YTH : '.$yth.'<br>
Dokter : '.$dokter.'<br>
Di : '.$di.'
</p>
<p>Bersama ini kami konsultasikan untuk penanganan lebih lanjut penderita</p>
<table class="table">
    <tr>
        <td>Nama</td>
        <td>:</td>
        <td>' . $pemeriksaan->nama_pasien . '</td>
    </tr>
    <tr>
        <td>Umur</td>
        <td>:</td>
        <td>'.$pemeriksaan->usia. '</td>
    </tr>
    <tr>
        <td>Jenis Kelamin</td>
        <td>:</td>
        <td>' . ($pasien->jk == 'L' ? 'Laki-laki' : 'Perempuan') . '</td>
    </tr>
    <tr>
        <td>Alamat</td>
        <td>:</td>
        <td>' . $pemeriksaan->alamat . '</td>
    </tr>
    <tr>
        <td>Pekerjaan</td>
        <td>:</td>
        <td>' . $pemeriksaan->pekerjaan . '</td>
    </tr>
</table>

<div>
    <p>Dengan keluhan: '.$keluhan.'. </p>
    <p>Sementara kami diagnosa: '.$diagnosa.' </p>
    <p>Dan kami beri terapi: '.$terapi.'.</p>
    <p>Atas bantuan dan kerjasamanya kami sampaikan terima kasih.</p>
</div>

<table style="width: 100%; margin-top: 20px">
    <tr>
        <td style="width: 300px"></td>
        <td></td>
        <td style="text-align: center">Dokter Pemeriksa</td>
    </tr>
    <tr>
        <td><br></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td><br></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td><br></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td></td>
        <td></td>
        <td style="text-align: center">(' . $pemeriksaan->nama_dokter . ')</td>
    </tr>
</table>

';

$mpdf->WriteHTML($html);
//$mpdf->SetHTMLFooter($footer);
$mpdf->SetHTMLFooter($footer, 'E');

//$mpdf->Output();
$mpdf->Output("Surat Rujukan $pemeriksaan->nama_pasien.pdf", 'D');
exit;

//echo $html;
?>