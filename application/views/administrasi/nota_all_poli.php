<div class="row">
    <div class="col-xs-12 table-responsive">
        <h4 style="text-align: center"><strong><?=$title?></strong></h4>
        <table class="table table-striped">
            <thead>
            <tr>
                <td><strong> TARIF</strong></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <th></th>
            </tr>
            </thead>
            <tbody>
            <?php
            $total_tindakan = 0;//var_dump($pemeriksaan->kode);die();
            if (strpos($pemeriksaan->kode, 'BPJS-') !== false) {
                $tindakan = array_map(function (&$v) {
                    $v->tarif_pasien = 0;
                    return $v;
                }, $tindakan->result());
            } else {
                $tindakan = $tindakan->result();
            }
            foreach ($tindakan as $key => $value) { ?>
                <tr>
                    <td></td>
                    <td><?= $value->nama ?><input type="hidden" name="nama_tindakan[]"
                                                  value="<?= $value->nama ?>"></td>
                    <td></td>
                    <td></td>
                    <td align="right"><?= number_format($value->tarif_pasien, 2, ',', '.'); ?>
                        <input type="hidden" name="tarif_pasien[]"
                               value="<?= $value->tarif_pasien ?>"></td>
                </tr>
                <?php
                $total_tindakan += $value->tarif_pasien;
            } ?>

            <?php if ($konsul && $pemeriksaan->jenis_pendaftaran_id == '58') : ?>
                <?php foreach ($konsul->result() as $key => $value) : ?>
                    <tr>
                        <td></td>
                        <td>
                            <?= $value->nama ?>
                            <input type="hidden" name="nama_tindakan[]" value="<?= $value->nama ?>"></td>
                        <td></td>
                        <td></td>
                        <td align="right"><?= number_format($value->tarif_pasien, 2, ',', '.'); ?>
                            <input type="hidden" name="tarif_pasien[]" value="<?= $value->tarif_pasien ?>">
                        </td>
                    </tr>
                    <?php $total_tindakan += $value->tarif_pasien; ?>
                <?php endforeach; ?>
            <?php endif; ?>

            <tr>
                <td><strong>OBAT</strong></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <?php
            $total_obat = 0;
            if (strpos($pemeriksaan->kode, 'BPJS-') !== false) {
                $obat = array_map(function (&$v) {
                    $v->harga_jual = 0;
                    return $v;
                }, $obat->result());
            } else {
                $obat = $obat->result();
            }
            foreach ($obat as $key => $value) { ?>
                <tr>
                    <td></td>
                    <td><?= $value->nama ?><input type="hidden" name="nama_obat[]" value="<?= $value->nama ?>"></td>
                    <td><?= $value->jumlah_satuan ?><input type="hidden" name="jumlah_satuan[]" value="<?= $value->jumlah_satuan ?>"></td>
                    <td align="right"><?= number_format($value->harga_jual, 2, ',', '.'); ?>
                        <input type="hidden" name="harga_jual[]" value="<?= $value->harga_jual ?>">
                    </td>
                    <td align="right"><?= number_format($value->harga_jual * $value->jumlah_satuan, 2, ',', '.'); ?>
                        <input type="hidden" name="subtotal_obat[]" value="<?= $value->harga_jual * $value->jumlah_satuan; ?>">
                    </td>
                </tr>
                <?php $total_obat += ($value->harga_jual * $value->jumlah_satuan);
            } ?>
            <tr>
                <td><strong>OBAT RACIK</strong></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <?php
            $total_obatracik = 0;
            foreach ($racikan as $key => $value) { ?>
                <tr>
                    <td><?= $value->nama_racikan ?><input type="hidden" name="nama_racikan[]" value="<?= $value->nama_racikan ?>"></td>
                    <td><?= 'Signa: '.$value->signa ?></td>
                    <td></td>
                    <td></td>
                    <td align="right"><?= number_format($value->total, 2, ',', '.'); ?>
                        <input type="hidden" name="total_racikan[]" value="<?= $value->total; ?>">
                    </td>
                </tr>
                <?php foreach ($value->obat as $k => $v) { ?>
                    <tr>
                        <td></td>
                        <td><?= $v->nama ?></td>
                        <td><?= $v->jumlah_satuan ?></td>
                        <td align="right"><?= number_format($v->harga_jual, 2, ',', '.'); ?></td>
                        <td align="right"><?= number_format($v->harga_jual * $v->jumlah_satuan, 2, ',', '.'); ?></td>
                    </tr>
                <?php } ?>
                <?php $total_obatracik += ($value->total); ?>
            <?php } ?>
            <tr>
                <td><strong>Jasa Racik</strong></td>
                <td></td>
                <td></td>
                <td></td>
                <td align="right"><input type="text" style="text-align: right;" autocomplete="off"
                                         class="form-control" id="<?=$jasa_racik_id?>" name="jasa_racik_<?=$pemeriksaan->id?>"
                                         value="<?= $pembayaran_result ? $pembayaran_result->jasa_racik : $jasa_racik ?>"
                                         <?= $pembayaran_result ? ' readonly ' : '' ?>
                                         onkeyup="setTotal();"></td>
            </tr>
            <tr>
                <td><strong>Bahan Habis Pakai</strong></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <?php
            foreach ($bahan->result() as $key => $value) { ?>
                <tr>
                    <td></td>
                    <td><?= $value->nama ?><input type="hidden" name="nama_bahan[]" value="<?= $value->nama ?>"></td>
                    <td><?= $value->jumlah ?><input type="hidden" name="jumlah_satuan_bahan[]" value="<?= $value->jumlah ?>"></td>
                    <td align="right">
                        <?= number_format($value->harga_jual, 2, ',', '.'); ?>
                        <input type="hidden" name="harga_jual_bahan[]" value="<?= $value->harga_jual ?>">
                    </td>
                    <td align="right">
                        <?= number_format($value->harga_jual * $value->jumlah, 2, ',', '.'); ?>
                        <input type="hidden" name="subtotal_bahan[]" value="<?= $value->harga_jual * $value->jumlah; ?>">
                    </td>
                </tr>
                <?php $total_bahan += ($value->harga_jual * $value->jumlah);
            } ?>
            </tbody>
        </table>
    </div>
    <!-- /.col -->
</div>