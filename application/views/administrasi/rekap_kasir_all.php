<link rel="stylesheet" href="<?php echo base_url() ?>assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
<link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/autocomplete/style-gue.css">
<style> br {mso-data-placement:same-cell;} </style>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Rekapitulasi Kasir All
            <small></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a href="#">Rekapitulasi Kasir All</a></li>
            <li class="active"> Pasien</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-success">
                    <div class="box-header with-border">
                        <h3 class="box-title">Rekapitulasi Kasir All</h3>&nbsp;&nbsp;
                    </div>
                    <div class="box-body">
                        <div class="nav-tabs-custom">
                            <ul class="nav nav-tabs">
                                <li class="<?=$by == 'waktu' ? 'active' : ''?>"><a href="#waktu" data-toggle="tab" aria-expanded="false">Berdasarkan Waktu</a></li>
                                <li class="<?=$by == 'pasien' ? 'active' : ''?>"><a href="#pasien" data-toggle="tab" aria-expanded="false">Berdasarkan Pasien</a></li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane <?=$by == 'waktu' ? 'active' : ''?>" id="waktu">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <form class="form-horizontal" method="get" action="<?php echo base_url()?>Rekapitulasi/kasir_all">
                                                <input type="hidden" name="by" value="waktu">
                                                <div class="row">
                                                    <div class="col-sm-4">
                                                        <div style="margin: 5px 10px">
                                                            <label class="control-label">Waktu</label>
                                                            <select class="form-control" id="jenis" name="jenis">
                                                                <option value="1" <?php if (isset($jenis) && $jenis == "1") echo 'selected'?>>Hari Ini</option>
                                                                <option value="2" <?php if (isset($jenis) && $jenis == "2") echo 'selected'?>>Pilih Tanggal</option>
                                                                <option value="3" <?php if (isset($jenis) && $jenis == "3") echo 'selected'?>>Pilih Bulan dan Tahun</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-4 jenis-2" style="display: none">
                                                        <div style="margin: 5px 10px">
                                                            <label class="control-label">Dari Tanggal</label>
                                                            <input type='date' name='from' class='form-control' id='tanggal_dari' value="<?php echo ($this->input->get('from')) ? $this->input->get('from') : date('Y-m-01') ?>">
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-4 jenis-2" style="display: none">
                                                        <div style="margin: 5px 10px">
                                                            <label class="control-label">Sampai Tanggal</label>
                                                            <input type='date' name='to' class='form-control' id='tanggal_sampai' value="<?php echo ($this->input->get('to')) ? $this->input->get('to') : date('Y-m-d') ?>">
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-4 jenis-3" style="display: none">
                                                        <div style="margin: 5px 10px">
                                                            <label class="control-label">Bulan</label>
                                                            <select class="form-control" id="bulan" name="bulan">
                                                                <option value="01" <?php if (isset($bulan) && $bulan == "01") echo 'selected'?>>Januari</option>
                                                                <option value="02" <?php if (isset($bulan) && $bulan == "02") echo 'selected'?>>Februari</option>
                                                                <option value="03" <?php if (isset($bulan) && $bulan == "03") echo 'selected'?>>Maret</option>
                                                                <option value="04" <?php if (isset($bulan) && $bulan == "04") echo 'selected'?>>April</option>
                                                                <option value="05" <?php if (isset($bulan) && $bulan == "05") echo 'selected'?>>Mei</option>
                                                                <option value="06" <?php if (isset($bulan) && $bulan == "06") echo 'selected'?>>Juni</option>
                                                                <option value="07" <?php if (isset($bulan) && $bulan == "07") echo 'selected'?>>Juli</option>
                                                                <option value="08" <?php if (isset($bulan) && $bulan == "08") echo 'selected'?>>Agustus</option>
                                                                <option value="09" <?php if (isset($bulan) && $bulan == "09") echo 'selected'?>>September</option>
                                                                <option value="10" <?php if (isset($bulan) && $bulan == "10") echo 'selected'?>>Oktober</option>
                                                                <option value="11" <?php if (isset($bulan) && $bulan == "11") echo 'selected'?>>November</option>
                                                                <option value="12" <?php if (isset($bulan) && $bulan == "12") echo 'selected'?>>Desember</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-4 jenis-3" style="display: none">
                                                        <div style="margin: 5px 10px">
                                                            <label class="control-label">Tahun</label>
                                                            <select class="form-control" id="tahun" name="tahun">
                                                                <?php $year = intval(date('Y')); for ($a = $year; $a > $year - 5; $a--) { ?>
                                                                    <option value="<?php echo $a?>" <?php if (isset($bulan) && $bulan == $a) echo 'selected'?>><?php echo $a?></option>
                                                                <?php } ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class='row'>
                                                    <div class="col-sm-3">
                                                        <div style="margin: 15px 10px">
                                                            <div class="form-group">
                                                                <div class="col-sm-7">
                                                                    <button type="submit" class="btn btn-primary" style='margin-left: 0px;'>Tampilkan</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane <?=$by == 'pasien' ? 'active' : ''?>" id="pasien">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <form id="FormStockOpname" class="form-horizontal" method="get" action="<?php echo base_url()?>Rekapitulasi/kasir_all">
                                                <div style="margin: 8px 24px">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <input type="hidden" name="by" value="pasien">
                                                            <div class="form-group">
                                                                <label for="cari" class="control-label">Cari</label>
                                                                <input type="text"
                                                                       class="form-control"
                                                                       name="pencarian_kode" id="pencarian_kode"
                                                                       placeholder="Masukkan Nama / No RM / NIK"
                                                                       value="<?=$pasien ? $pasien->no_rm.' - '.$pasien->nama : ''?>"
                                                                       autocomplete="off">
                                                                <input type="hidden" name="id_pasien" id='id_pasien' value="<?=$pasien ? $pasien->id : ''?>">
                                                                <div id='hasil_pencarian'></div>
                                                            </div>
                                                            <button type='submit' class='btn btn-primary' style="transform: translateX(-14px)">Tampilkan</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div style="display: flex; margin-bottom: 12px" class="toggleable">
                            <div style="flex: 1"></div>
                            <form class="form-horizontal" method="get" action="<?php echo base_url()?>export/kasir_all">
                                <input type="hidden" name="jenis" value="<?=$jenis?>">
                                <input type="hidden" name="from" value="<?=$from?>">
                                <input type="hidden" name="to" value="<?=$to?>">
                                <input type="hidden" name="bulan" value="<?=$bulan?>">
                                <input type="hidden" name="tahun" value="<?=$tahun?>">
                                <input type="hidden" name="by" value="<?=$by?>">
                                <input type="hidden" name="id_pasien" value="<?=$pasien ? $pasien->id : ''?>">
                                <button type="submit" class="btn btn-success">
                                    <i class="fa fa-print"></i> Download Excel
                                </button>
                            </form>
                        </div>
                        <div class="toggleable">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Waktu Bayar</th>
                                    <th>Pasien</th>
                                    <th>Jenis Pendaftaran</th>
                                    <th></th>
                                    <th>Jasa Medis</th>
                                    <th>Obat</th>
                                    <th>Obat Racikan</th>
                                    <th>BHP</th>
                                    <th>Jasa Racik</th>
                                    <th>Diskon</th>
                                    <th>Total Bayar</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                $no=1;
                                foreach ($listPemeriksaan as $row) { ?>
                                    <?php $tot = 0 ?>
                                    <tr>
                                        <td> <?php echo $no++; ?></td>
                                        <td><small><?php echo date('d-M-Y H:i', strtotime($row->created_at)); ?></small></td>
                                        <td>
                                            <?php echo ucwords($row->bayar[0]->nama_pasien); ?>
                                            <br>
                                            <i><small><?php echo $row->bayar[0]->no_rm; ?></small></i>
                                        </td>
                                        <td>
                                            <?php foreach ($row->bayar as $k => $v) : ?>
                                                <?= ucwords($v->nama_poli) ?>
                                                <?= $k == count($row->bayar) - 1 ? '' : '<br>' ?>
                                            <?php endforeach; ?>
                                        </td>
                                        <td>
                                            <?php foreach ($row->bayar as $k => $v) : ?>
                                                <span class="label <?= $jaminan[$v->jaminan]['class']  ?>"><?= $jaminan[$v->jaminan]['label']  ?></span>
                                                <?= $k == count($row->bayar) - 1 ? '' : '<br>' ?>
                                            <?php endforeach; ?>
                                        </td>
                                        <td>
                                            <?php $jm = 0 ?>
                                            <?php foreach ($row->bayar as $k => $v) : ?>
                                                <a style="cursor: pointer" onclick="open_detail(<?=$row->id?>, <?=$v->bayar_id?>, <?=$v->pemeriksaan_id?>, 'Jasa Medis')">
                                                    Rp <?= number_format($v->jasa_medis, 2, ',', '.') ?>
                                                </a>
                                                <?= $k == count($row->bayar) - 1 ? '' : '<br>' ?>
                                                <?php $jm += $v->jasa_medis ?>
                                            <?php endforeach; ?>
                                            <?php if (count($row->bayar) > 1) : ?>
                                                <hr style="margin: 4px 0 !important;">
                                                Rp <?= number_format($jm, 2, ',', '.') ?>
                                            <?php endif; ?>
                                            <?php $tot += $jm ?>
                                        </td>
                                        <td>
                                            <?php $jm = 0 ?>
                                            <?php foreach ($row->bayar as $k => $v) : ?>
                                                <a style="cursor: pointer" onclick="open_detail(<?=$row->id?>, <?=$v->bayar_id?>, <?=$v->pemeriksaan_id?>, 'Obat', '<?=$v->jenis_ruangan_id?>')">
                                                    Rp <?= number_format($v->obat, 2, ',', '.') ?>
                                                </a>
                                                <?= $k == count($row->bayar) - 1 ? '' : '<br>' ?>
                                                <?php $jm += $v->obat ?>
                                            <?php endforeach; ?>
                                            <?php if (count($row->bayar) > 1) : ?>
                                                <hr style="margin: 4px 0 !important;">
                                                Rp <?= number_format($jm, 2, ',', '.') ?>
                                            <?php endif; ?>
                                            <?php $tot += $jm ?>
                                        </td>
                                        <td>
                                            <?php $jm = 0 ?>
                                            <?php foreach ($row->bayar as $k => $v) : ?>
                                                <a style="cursor: pointer" onclick="open_detail_racikan('<?=$v->nama_poli?>', <?=$v->pemeriksaan_id?>, <?=$v->rawat_inap_id?>, <?=$v->transfer_id?>, '<?=$v->jenis_ruangan_id?>')">
                                                    Rp <?= number_format($v->obat_racik, 2, ',', '.') ?>
                                                </a>
                                                <?= $k == count($row->bayar) - 1 ? '' : '<br>' ?>
                                                <?php $jm += $v->obat_racik ?>
                                            <?php endforeach; ?>
                                            <?php if (count($row->bayar) > 1) : ?>
                                                <hr style="margin: 4px 0 !important;">
                                                Rp <?= number_format($jm, 2, ',', '.') ?>
                                            <?php endif; ?>
                                            <?php $tot += $jm ?>
                                        </td>
                                        <td>
                                            <?php $jm = 0 ?>
                                            <?php foreach ($row->bayar as $k => $v) : ?>
                                                <a style="cursor: pointer" onclick="open_detail(<?=$row->id?>, <?=$v->bayar_id?>, <?=$v->pemeriksaan_id?>, 'Bahan Habis Pakai')">
                                                    Rp <?= number_format($v->bhp, 2, ',', '.') ?>
                                                </a>
                                                <?= $k == count($row->bayar) - 1 ? '' : '<br>' ?>
                                                <?php $jm += $v->bhp ?>
                                            <?php endforeach; ?>
                                            <?php if (count($row->bayar) > 1) : ?>
                                                <hr style="margin: 4px 0 !important;">
                                                Rp <?= number_format($jm, 2, ',', '.') ?>
                                            <?php endif; ?>
                                            <?php $tot += $jm ?>
                                            <?php $tot += $row->jasa_racik ?>
                                            <?php $tot -= $row->diskon ?>
                                        </td>
                                        <td>Rp <?= number_format($row->jasa_racik, 2, ',', '.') ?></td>
                                        <td>Rp <?= number_format($row->diskon, 2, ',', '.') ?></td>
                                        <td>Rp <?= number_format($row->total, 2, ',', '.') ?></td>
                                        <td>
                                            <?php if ($row->jenis == 'resep_luar') : ?>
                                                <a target="_blank" href="<?=base_url("/Administrasi/nota_obat_luar_print/$row->id")?>"
                                                   class="btn btn-sm btn-success"
                                                >
                                                    <i class="fa fa-print"></i>
                                                </a>
                                            <?php else : ?>
                                                <a target="_blank" href="<?=base_url("/Administrasi/print_nota_all/$row->pasien_id/$row->jenis/$row->print_id/$row->id")?>"
                                                   class="btn btn-sm btn-success"
                                                >
                                                    <i class="fa fa-print"></i>
                                                </a>
                                            <?php endif; ?>
                                        </td>
                                    </tr>
                                <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<div class="modal fade" id="detail-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header" style="display: flex">
                <h4 class="modal-title" id="choose-title" style="flex: 1">Detail</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <table class="table table-bordered" id="tbl-detail">
                    <thead>
                    <tr>
                        <td>No</td>
                        <td>Nama</td>
                        <td>Harga</td>
                        <td>Jumlah</td>
                        <td>Subtotal</td>
                    </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="detail-modal-racikan" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header" style="display: flex">
                <h4 class="modal-title" style="flex: 1">Detail Obat Racikan</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p id="loading-text"><i>Mendapatkan data racikan...</i></p>
                <table class="table table-bordered" id="tbl-detail-racikan">
                    <thead>
                    <tr>
                        <th>Nama</th>
                        <th>Obat</th>
                        <th>Signa</th>
                        <th>Catatan</th>
                        <th>Subtotal</th>
                    </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<script src="<?= base_url() ?>assets/bower_components/select2/dist/js/select2.full.min.js"></script>

<?php $this->load->view('administrasi/rekap_kasir_all_js'); ?>