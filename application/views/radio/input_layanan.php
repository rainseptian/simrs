<?php

include BASEPATH.'../application/views/template/InputBuilder.php';
?>

<style media="screen">
    .select2-container {
        width: 100% !important;
    }

    .ck-editor__editable {
        min-height: 400px;
        margin-bottom: 12px;
    }

</style>
<link rel="stylesheet" href="<?= base_url() ?>assets/plugins/iCheck/all.css">

<form class="form-horizontal" method="post" action="<?= base_url() ?>radio/input_pemeriksaan/<?= $pemeriksaan['id'] ?>">

    <div class="content-wrapper">
        <section class="content-header">
            <h1>
                Pemeriksaan Radiologi
                <small>Preview</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="<?= base_url() ?>Dashbord"><i class="fa fa-dashboard"></i> Dashbord</a></li>
                <li class="active">Pemeriksaan Radiologi</li>
            </ol>
        </section>

        <?php $warning = $this->session->flashdata('warning');
        if (!empty($warning)){ ?>
            <div class="alert alert-warning alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-warning"></i> Alert!</h4>
                <?php echo $warning ?>
            </div>
        <?php } ?>
        <?php $success = $this->session->flashdata('success');
        if (!empty($success)){ ?>
            <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-check"></i> Success!</h4>
                <?php echo $success ?>
            </div>
        <?php } ?>

        <section class="content">
            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-7">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="box box-danger">
                                <div class="box-header">
                                    <h3 class="box-title"> Pemeriksaan Radiologi</h3>
                                </div>
                                <div class="box-body">
                                    <div class="box-body">

                                        <?php sm4('no_rm', 'No Rekam Medis')->val($pemeriksaan['no_rm'])->readonly()->build(); ?>
                                        <?php sm4('nama_pasien', 'Nama Pasien')->val($pemeriksaan['nama_pasien'])->readonly()->build(); ?>
                                        <?php sm4('usia', 'Usia')->val($pasien->usia)->readonly()->build(); ?>
                                        <?php sm4('jk', 'Jenis Kelamin')->val($pasien->jk == 'L' ? 'Laki-laki' : 'Perempuan')->readonly()->build(); ?>

                                        <?php sel('tindakan[]', 'Layanan')
                                            ->id('tindakan')
                                            ->onchange('on_tindakan_change()')
                                            ->placeholder('Pilih layanan untuk pasien')
                                            ->options($tindakan->result())
                                            ->selectedOptions($s_tindakan)
                                            ->display(function ($value) use (&$pendaftaran) {
                                                return $value->nama . " - Rp." . number_format($value->tarif_pasien, 2, ',', '.');
                                            })
                                            ->build(); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12" style="display: none" id="c-surat">
                            <div class="nav-tabs-custom">
                                <ul class="nav nav-tabs" id="tabs">

                                </ul>
                                <div class="tab-content" id="vp">

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-12 col-lg-5">
                    <div class="box box-primary">
                        <div class="box-header">
                            <h4>Upload Foto Radiologi</h4>
                        </div>
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <?php $this->load->view('pemeriksaan/partials/tab_upload'); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="box">
                        <div class="box-body">
                            <button type="submit" name="submit" value="1" class="btn btn-primary btn-lg btn-flat pull-right">
                                Simpan
                            </button>
                            <button onclick="history.back()" class="btn btn-default btn-lg btn-flat pull-right">
                                Batal
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</form>

<div id='ResponseInput'></div>

<script src="<?= base_url() ?>assets/bower_components/select2/dist/js/select2.full.min.js"></script>

<script type="text/javascript">
    $('.select2').select2();

    const ext_tindakan = <?=json_encode($s_tindakan)?>;
    const tindakan = <?=json_encode($tindakan->result())?>;
    const pasien = <?=json_encode($pasien)?>;

    $(function () {
        $('#c-surat').css('display', ext_tindakan.length ? 'block' : 'none')
        ext_tindakan.forEach((v, k) => {
            render(+v.id, k)
        })
    })

    function on_tindakan_change() {
        const t = ($('#tindakan').val()).filter(v => +v !== 1646)
        $('#c-surat').css('display', t.length ? 'block' : 'none')
        $('#tabs').empty()
        t.forEach((v, k) => {
            render(v, k)
        })

        setEditor()
    }

    const render = (tindakan_id, index) => {
        const name = tindakan.find(r => +r.id === +tindakan_id)?.nama ?? ''
        const id = name.replaceAll(' ', '_').toLowerCase()

        $('#tabs').append(`
            <li ${index === 0 ? 'class="active"' : ''}>
                <a href="#${id}" data-toggle="tab" aria-expanded="false">${name} ${+tindakan_id === 696 ? `(${pasien.jk})` : ''}</a>
            </li>
        `)

        const cat = tindakan.find(r => +r.id === +tindakan_id)['Column 6'] ?? '0'
        $('#vp').append(usg(index, id, +cat))

        if (index === 0)
            $(`#${id}`).addClass('active')
        else
            $(`#${id}`).removeClass('active')
    }

    const usg = (k, id, category) => {
        if ($('#vp').find(`div#${id}`).length !== 0) {
            return ''
        }

        if (category === 1 && pasien.jk === 'L')
            return usg_abdomen_l(k, id)
        else if (category === 1 && pasien.jk === 'P')
            return usg_abdomen_p(k, id)
        else if (category === 2)
            return usg_testis(k, id)
        else if (category === 3)
            return usg_urologi(k, id)
        else if (category === 4)
            return usg_mamae(k, id)
        else if (category === 5)
            return usg_thyroid(k, id)
        else if (category === 6)
            return usg_lesi(k, id)
        else if (category === 7)
            return usg_kandungan_2d(k, id)
        return ''
    }

    const usg_abdomen_l = (k, id) => `
        <div class="tab-pane ${k === 0 ? 'active' : ''}" id="${id}">
            <div class="row">
                <div class="col-md-12">
                    <table class="table">
                        <tbody>
                        <tr>
                            <td>Klinis</td>
                            <td>:</td>
                            <td><input type="text" class="form-control" name="meta[${id}][l][klinis]"></td>
                        </tr>
                        <tr>
                            <td>Pengirim</td>
                            <td>:</td>
                            <td><input type="text" class="form-control" name="meta[${id}][l][pengirim]"></td>
                        </tr>
                        </tbody>
                    </table>
                    <p><b>Yth. Hasil Pemeriksaan USG Abdomen :</b></p>
                    <label>Deskripsi</label>
                    <textarea class="form-control editor-radio" name="meta[${id}][l][deskripsi]" id="${id}_l_deskripsi" rows="3" style="margin-bottom: 10px"></textarea>
                    <label>Kesimpulan</label>
                    <textarea class="form-control editor-radio" name="meta[${id}][l][kesimpulan]" id="${id}_l_kesimpulan" rows="3" style="margin-bottom: 10px"></textarea>
                </div>
            </div>
        </div>
    `

    const usg_abdomen_p = (k, id) => `
        <div class="tab-pane ${k === 0 ? 'active' : ''}" id="${id}">
            <div class="row">
                <div class="col-md-12">
                    <table class="table">
                        <tbody>
                        <tr>
                            <td>Klinis</td>
                            <td>:</td>
                            <td><input type="text" class="form-control" name="meta[${id}][p][klinis]"></td>
                        </tr>
                        <tr>
                            <td>Pengirim</td>
                            <td>:</td>
                            <td><input type="text" class="form-control" name="meta[${id}][p][pengirim]"></td>
                        </tr>
                        </tbody>
                    </table>
                    <p><b>Yth. Hasil Pemeriksaan USG Abdomen :</b></p>
                    <label>Deskripsi</label>
                    <textarea class="form-control editor-radio" name="meta[${id}][p][deskripsi]" id="${id}_p_deskripsi" rows="3" style="margin-bottom: 10px"></textarea>
                    <label>Kesimpulan</label>
                    <textarea class="form-control editor-radio" name="meta[${id}][p][kesimpulan]" id="${id}[p][kesimpulan]" rows="3" style="margin-bottom: 10px"></textarea>
                </div>
            </div>
        </div>
    `

    const usg_testis = (k, id) => `
        <div class="tab-pane ${k === 0 ? 'active' : ''}" id="${id}">
            <div class="row">
                <div class="col-md-12">
                    <table class="table">
                        <tbody>
                        <tr>
                            <td>Klinis</td>
                            <td>:</td>
                            <td><input type="text" class="form-control" name="meta[${id}][klinis]"></td>
                        </tr>
                        <tr>
                            <td>Pengirim</td>
                            <td>:</td>
                            <td><input type="text" class="form-control" name="meta[${id}][pengirim]"></td>
                        </tr>
                        </tbody>
                    </table>
                    <p><b>TS, Yth. Hasil Pemeriksaan USG Scrotum / Testis :</b></p>
                    <label>Deskripsi</label>
                    <textarea class="form-control editor-radio" name="meta[${id}][deskripsi]" id="${id}_deskripsi" rows="3" style="margin-bottom: 10px"></textarea>
                    <label>Kesimpulan</label>
                    <textarea class="form-control editor-radio" name="meta[${id}][kesimpulan]" id="${id}_kesimpulan" rows="3" style="margin-bottom: 10px"></textarea>
                </div>
            </div>
        </div>
    `

    const usg_urologi = (k, id) => `
        <div class="tab-pane ${k === 0 ? 'active' : ''}" id="${id}">
            <div class="row">
                <div class="col-md-12">
                    <table class="table">
                        <tbody>
                        <tr>
                            <td>Klinis</td>
                            <td>:</td>
                            <td><input type="text" class="form-control" name="meta[${id}][klinis]"></td>
                        </tr>
                        <tr>
                            <td>Pengirim</td>
                            <td>:</td>
                            <td><input type="text" class="form-control" name="meta[${id}][pengirim]"></td>
                        </tr>
                        </tbody>
                    </table>
                    <p><b>TS, Yth. Hasil Pemeriksaan USG Urology :</b></p>
                    <label>Deskripsi</label>
                    <textarea class="form-control editor-radio" name="meta[${id}][deskripsi]" id="${id}_deskripsi" rows="3" style="margin-bottom: 10px"></textarea>
                    <label>Kesimpulan</label>
                    <textarea class="form-control editor-radio" name="meta[${id}][kesimpulan]" id="${id}_kesimpulan" rows="3" style="margin-bottom: 10px"></textarea>
                </div>
            </div>
        </div>
    `

    const usg_mamae = (k, id) => `
        <div class="tab-pane ${k === 0 ? 'active' : ''}" id="${id}">
            <div class="row">
                <div class="col-md-12">
                    <table class="table">
                        <tbody>
                        <tr>
                            <td>Klinis</td>
                            <td>:</td>
                            <td><input type="text" class="form-control" name="meta[${id}][klinis]"></td>
                        </tr>
                        <tr>
                            <td>Pengirim</td>
                            <td>:</td>
                            <td><input type="text" class="form-control" name="meta[${id}][pengirim]"></td>
                        </tr>
                        </tbody>
                    </table>
                    <p><b>TS, Yth. Hasil Pemeriksaan USG Mammae :</b></p>
                    <label>Deskripsi</label>
                    <textarea class="form-control editor-radio" name="meta[${id}][deskripsi]" id="${id}_deskripsi" rows="3" style="margin-bottom: 10px"></textarea>
                    <label>Kesimpulan</label>
                    <textarea class="form-control editor-radio" name="meta[${id}][kesimpulan]" id="${id}_kesimpulan" rows="3" style="margin-bottom: 10px"></textarea>
                </div>
            </div>
        </div>
    `

    const usg_thyroid = (k, id) => `
        <div class="tab-pane ${k === 0 ? 'active' : ''}" id="${id}">
            <div class="row">
                <div class="col-md-12">
                    <table class="table">
                        <tbody>
                        <tr>
                            <td>Klinis</td>
                            <td>:</td>
                            <td><input type="text" class="form-control" name="meta[${id}][klinis]"></td>
                        </tr>
                        <tr>
                            <td>Pengirim</td>
                            <td>:</td>
                            <td><input type="text" class="form-control" name="meta[${id}][pengirim]"></td>
                        </tr>
                        </tbody>
                    </table>
                    <p><b>TS, Yth. Hasil Pemeriksaan USG Thyroid :</b></p>
                    <label>Deskripsi</label>
                    <textarea class="form-control editor-radio" name="meta[${id}][deskripsi]" id="${id}_deskripsi" rows="3" style="margin-bottom: 10px"></textarea>
                    <label>Kesimpulan</label>
                    <textarea class="form-control editor-radio" name="meta[${id}][kesimpulan]" id="${id}_kesimpulan" rows="3" style="margin-bottom: 10px"></textarea>
                </div>
            </div>
        </div>
    `

    const usg_lesi = (k, id) => `
        <div class="tab-pane ${k === 0 ? 'active' : ''}" id="${id}">
            <div class="row">
                <div class="col-md-12">
                    <table class="table">
                        <tbody>
                        <tr>
                            <td>Klinis</td>
                            <td>:</td>
                            <td><input type="text" class="form-control" name="meta[${id}][klinis]"></td>
                        </tr>
                        <tr>
                            <td>Pengirim</td>
                            <td>:</td>
                            <td><input type="text" class="form-control" name="meta[${id}][pengirim]"></td>
                        </tr>
                        </tbody>
                    </table>
                    <p><b>TS, Yth. Hasil Pemeriksaan USG Fokus Lesi (Regio Umbilical) :</b></p>
                    <label>Deskripsi</label>
                    <textarea class="form-control editor-radio" name="meta[${id}][deskripsi]" id="${id}_deskripsi" rows="3" style="margin-bottom: 10px"></textarea>
                    <label>Kesimpulan</label>
                    <textarea class="form-control editor-radio" name="meta[${id}][kesimpulan]" id="${id}_kesimpulan" rows="3" style="margin-bottom: 10px"></textarea>
                </div>
            </div>
        </div>
    `

    const usg_kandungan_2d = (k, id) => `
        <div class="tab-pane ${k === 0 ? 'active' : ''}" id="${id}">
            <div class="row">
                <div class="col-md-12">
                    <table class="table">
                        <tbody>
                        <tr>
                            <td>Klinis</td>
                            <td>:</td>
                            <td><input type="text" class="form-control" name="meta[${id}][klinis]"></td>
                        </tr>
                        <tr>
                            <td>Pengirim</td>
                            <td>:</td>
                            <td><input type="text" class="form-control" name="meta[${id}][pengirim]"></td>
                        </tr>
                        </tbody>
                    </table>
                    <p><b>TS, Yth. Hasil Pemeriksaan USG Kandungan 2D :</b></p>
                    <label>Deskripsi</label>
                    <textarea class="form-control editor-radio" name="meta[${id}][deskripsi]" id="${id}_deskripsi" rows="3" style="margin-bottom: 10px"></textarea>
                    <label>Kesimpulan</label>
                    <textarea class="form-control editor-radio" name="meta[${id}][kesimpulan]" id="${id}_kesimpulan" rows="3" style="margin-bottom: 10px"></textarea>
                </div>
            </div>
        </div>
    `

</script>

<script>
    class MyUploadAdapter {
        constructor( loader ) {
            this.loader = loader;
        }

        upload() {
            return this.loader.file
                .then( file => new Promise( ( resolve, reject ) => {
                    this._initRequest();
                    this._initListeners( resolve, reject, file );
                    this._sendRequest( file );
                } ) );
        }

        abort() {
            if ( this.xhr ) {
                this.xhr.abort();
            }
        }

        _initRequest() {
            const xhr = this.xhr = new XMLHttpRequest();

            xhr.open( 'POST', '{{ url('/') }}/ck/upload', true );
            xhr.responseType = 'json';
        }

        _initListeners( resolve, reject, file ) {
            const xhr = this.xhr;
            const loader = this.loader;
            const genericErrorText = `Couldn't upload file: ${ file.name }.`;

            xhr.addEventListener( 'error', () => reject( genericErrorText ) );
            xhr.addEventListener( 'abort', () => reject() );
            xhr.addEventListener( 'load', () => {
                const response = xhr.response;
                if ( !response || response.error ) {
                    return reject( response && response.error ? response.error.message : genericErrorText );
                }
                resolve( {
                    default: response.url
                } );
            } );

            if ( xhr.upload ) {
                xhr.upload.addEventListener( 'progress', evt => {
                    if ( evt.lengthComputable ) {
                        loader.uploadTotal = evt.total;
                        loader.uploaded = evt.loaded;
                    }
                } );
            }
        }

        _sendRequest( file ) {
            const data = new FormData();

            data.append( 'upload', file );

            // Important note: This is the right place to implement security mechanisms
            // like authentication and CSRF protection. For instance, you can use
            // XMLHttpRequest.setRequestHeader() to set the request headers containing
            // the CSRF token generated earlier by your application.

            this.xhr.setRequestHeader('X-CSRF-TOKEN', '{{ @csrf_token() }}');
            this.xhr.send( data );
        }
    }

    function MyCustomUploadAdapterPlugin( editor ) {
        editor.plugins.get( 'FileRepository' ).createUploadAdapter = ( loader ) => {
            // Configure the URL to the upload script in your back-end here!
            return new MyUploadAdapter( loader );
        };
    }

    $(function () {
        setEditor()
    })

    let editors = {}

    const setEditor = () => {
        document.querySelectorAll( '.editor-radio' ).forEach( ( node, index ) => {
            if (!editors[ node.id ]) {
                ClassicEditor
                    .create( node, {
                        extraPlugins: [ MyCustomUploadAdapterPlugin ],
                    } )
                    .then( newEditor => {
                        editors[ node.id ] = newEditor
                    } )
                    .catch( error => {
                        console.error( error );
                    } );
            }
        });
    }
</script>

<?php $this->load->view('pemeriksaan/partials/tab_upload_js'); ?>