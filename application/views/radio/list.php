<?php
$currency_symbol = 'Rp';
?>

<link rel="stylesheet" href="<?= base_url() ?>assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
<link rel="stylesheet" href="<?= base_url() ?>assets/plugins/autocomplete/style-gue.css">
<style type="text/css">
    @import('https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.0/css/bootstrap.min.css');
</style>

<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Data List Pasien Radiologi
            <small></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a href="#">List Pasien</a></li>
            <li class="active">Data List Pasien Radiologi</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Data Pasien Belum Diperiksa</h3>&nbsp;&nbsp;
                    </div>
                    <?php $warning = $this->session->flashdata('warning');
                    if (!empty($warning)) { ?>
                        <div class="alert alert-warning alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <h4><i class="icon fa fa-warning"></i> Alert!</h4>
                            <?= $warning ?>
                        </div>
                    <?php } ?>
                    <?php $success = $this->session->flashdata('success');
                    if (!empty($success)) { ?>
                        <div class="alert alert-success alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <h4><i class="icon fa fa-check"></i> Success!</h4>
                            <?= $success ?>
                        </div>
                    <?php } ?>
                    <div class="box-body">
                        <table id="example2" class="table table-striped table-hover">
                            <thead>
                            <tr>
                                <th>No Antrian</th>
                                <th>NO RM</th>
                                <th>Nama Pasien</th>
                                <th>Alamat</th>
                                <th>Tanggal Daftar</th>
                                <th>Jenis Pendaftaran</th>
                                <th>Nama Dokter Perujuk</th>
                                <th>Aksi</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $no = 1;
                            foreach ($listPendaftaran as $row) { ?>
                                <tr>
                                    <td>
                                        <?= $row->kode_antrian; ?>
                                        <?php if ($row->is_mobile_jkn) : ?>
                                            <br>
                                            <small>
                                                <span class="label label-danger">Mobile JKN</span>
                                                <?php if ($row->is_check_in) : ?>
                                                    <span class="label label-success">Check In</span>
                                                <?php endif; ?>
                                            </small>
                                        <?php endif; ?>
                                    </td>
                                    <td style="width: 100px;">
                                        <span class="norm"><?= $row->no_rm; ?></span>
                                        <br>
                                        <small>
                                            <span class="label <?= $jaminan[$row->jaminan]['class'] ?>"><?= $jaminan[$row->jaminan]['label'] ?></span>
                                            <?php if (!isset($jaminan[$row->jaminan])) { ?>
                                                <span class="label label-warning">Umum</span>
                                            <?php } ?>
                                        </small>
                                    </td>
                                    <td class="nama"> <?= ucwords($row->nama_pasien); ?></td>
                                    <td> <?= ucwords($row->alamat); ?></td>
                                    <td> <?= date('d-F-Y H:i', strtotime($row->waktu_pemeriksaan)); ?></td>
                                    <td> <?= ucwords($row->jenis_pendaftaran); ?></td>
                                    <td> <?= ucwords($row->nama_dokter); ?></td>
                                    <td>
                                        <div class="btn-group" role="group">

                                            <button id="btnGroupDrop1" type="button" class="btn btn-sm btn-success dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="fa fa-bars"></i>
                                            </button>
                                            <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                                                <button onclick="panggil(<?=$row->antrian_id?>, '<?=$row->kode_antrian?>', 'Radiologi')"
                                                        class=" btn btn-sm btn-block btn-warning" style="margin-bottom: 5px;">
                                                    <i class="fa fa-volume-up"></i> Panggil
                                                </button>
                                                <a href="<?=base_url().'Radio/input_pemeriksaan/'?><?=$row->id?>" class="btn btn-sm btn-block btn-success" style="margin-bottom: 5px;">
                                                    <i class="fa fa-pencil"></i> Periksa
                                                </a>
                                                <button type="button" class="btn btn-sm btn-primary btn-block btn-rekamedis"
                                                        data-pasien_id="<?= $row->pasien_id ?>"><i class="fa fa-search"></i>
                                                    Rekam Medis
                                                </button>
                                                <a href="<?= base_url(); ?>Radio/hapus/<?= $row->id; ?>"
                                                   onclick="return confirm('Yakin hapus data pemeriksaan ini?')"
                                                   class="btn btn-sm btn-block btn-danger" style="margin-bottom: 5px;">
                                                    <i class="fa fa-trash"></i> Hapus
                                                </a>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <?php $no++;
                            } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<div class="modal fade" id="addTestReportModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content modal-media-content">
            <div class="modal-header modal-media-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="box-title">Detail Tambah Tes</h4>
            </div>
            <form id="formadd" accept-charset="utf-8" method="post" class="ptt10" enctype="multipart/form-data">
                <div class="modal-body pt0 pb0">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <div class="row">
                                <input type="hidden" name="pemeriksaan_id" id="pemeriksaan_id">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Tindakan</label>
                                        <small class="req"> *</small>
                                        <select name="tindakan[]" class="form-control select2" multiple="multiple" data-placeholder="Pilih tindakan" style="width: 100%;" id="s-tindakan">
                                            <?php foreach ($tindakan->result() as $value) : ?>
                                                <option value="<?= $value->id; ?>">
                                                    <?= $value->nama . " - Rp." . number_format($value->tarif_pasien, 2, ',', '.') ?>
                                                </option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleFormControlFile1">Hasil Tes</label>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <input type="file" class="form-control-file" id="hasil-tes-0" name="file_hasil_tes_0" accept="image/png, image/gif, image/jpeg">
                                                <div style="height: 8px"></div>
                                                <input type="file" class="form-control-file" id="hasil-tes-1" name="file_hasil_tes_1" accept="image/png, image/gif, image/jpeg">
                                            </div>
                                            <div class="col-sm-6">
                                                <input type="file" class="form-control-file" id="hasil-tes-2" name="file_hasil_tes_2" accept="image/png, image/gif, image/jpeg">
                                                <div style="height: 8px"></div>
                                                <input type="file" class="form-control-file" id="hasil-tes-3" name="file_hasil_tes_3" accept="image/png, image/gif, image/jpeg">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>Staff Radiologi</label>
                                        <small class="req"> *</small>
                                        <select name="perawat[]" class="form-control select2" multiple="multiple" data-placeholder="Pilih staff" style="width: 100%;" required>
                                            <?php foreach ($listStaff->result() as $value) : ?>
                                                <option value="<?= $value->id; ?>"><?= ucwords($value->nama) ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Hasil Bacaan</label>
                                        <small class="req"> *</small>
                                        <input type="text" name="meta[hasil_bacaan]" class="form-control">
                                        <span class="text-danger"><?= form_error('short_name'); ?></span>
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleFormControlFile1">Hasil Bacaan</label>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <input type="file" class="form-control-file" id="hasil-bacaan-0" name="file_hasil_bacaan_0" accept="application/pdf, image/png, image/gif, image/jpeg">
                                                <div style="height: 8px"></div>
                                                <input type="file" class="form-control-file" id="hasil-bacaan-1" name="file_hasil_bacaan_1" accept="application/pdf, image/png, image/gif, image/jpeg">
                                            </div>
                                            <div class="col-sm-6">
                                                <input type="file" class="form-control-file" id="hasil-bacaan-2" name="file_hasil_bacaan_2" accept="application/pdf, image/png, image/gif, image/jpeg">
                                                <div style="height: 8px"></div>
                                                <input type="file" class="form-control-file" id="hasil-bacaan-3" name="file_hasil_bacaan_3" accept="application/pdf, image/png, image/gif, image/jpeg">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    <div class="pull-right">
                        <button type="submit" data-loading-text="Memproses..."
                                id="formaddbtn"
                                class="btn btn-info pull-right"><?= $this->lang->line('save'); ?></button>

                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<script src="<?= base_url() ?>assets/bower_components/select2/dist/js/select2.full.min.js"></script>
<script>
    $(document).ready(function () {
        $('#example2').dataTable()

        const all_pemeriksaan = <?=json_encode($listPendaftaran);?>;
        const all_tindakan = <?=json_encode($tindakan->result());?>;

        $('.btn-periksa').click(function () {
            $("#addTestReportModal").modal({
                backdrop: 'static',
                keyboard: false,
                show: true
            });

            const pemeriksaan_id = $(this).data('pendaftaranid');
            $("#pemeriksaan_id").val(pemeriksaan_id);

            const p = all_pemeriksaan.find(v => v.id == pemeriksaan_id)
            $('#s-tindakan').find('option').remove()

            all_tindakan.forEach(v => {
                const sel = p.tindakan.map(v => v.tarif_tindakan_id).includes(v.id) ? 'selected' : ''
                $('#s-tindakan').append(`<option value="${v.id}" ${sel}>${v.nama}</option>`)
            })
        });

        $("#formadd").on('submit', (function (e) {
            $("#formaddbtn").button('loading');
            e.preventDefault();
            $.ajax({
                url: '<?= base_url(); ?>Radio/periksa',
                type: "POST",
                data: new FormData(this),
                enctype: 'multipart/form-data',
                // dataType: 'json',
                contentType: false,
                cache: false,
                processData: false,
                success: function (data) {
                    console.log(data);
                    const g = JSON.parse(data)
                    if (g.status === "success") {
                        successMsg(data.message);
                        window.open("<?= base_url(); ?>Radio/sudahPeriksa","_self")
                    }
                    $("#formaddbtn").button('reset');
                },
                error: function (e) {
                    console.log(e)
                    console.log(e.message)
                }
            });
        }));
    })
    $('.select2').select2();

</script>

<script type="text/javascript">
    function holdModal(modalId) {
        $('#' + modalId).modal({
            backdrop: 'static',
            keyboard: false,
            show: true
        });
    }
</script>

<script>
    let speech = new SpeechSynthesisUtterance();
    speech.lang = "id";
    speech.rate = 0.8

    function panggil(id, code, poli) {
        const kode = code.split('').join(' ... ')
        speech.text = `Nomor antrian.... ${kode}.... Silahkan ke ${poli}`
        window.speechSynthesis.speak(speech)
        $.ajax({url: `<?=base_url()?>AntrianPoli/call_next/${id}`}).done(function() { })
    }
</script>
