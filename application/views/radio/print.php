<head>
    <title>Cetak</title>
    <style>
        @media print {
            .pagebreak { page-break-before: always; } /* page-break-after works, as well */
        }
        p {
            margin-top: 4px;
            margin-bottom: 4px;
        }
        ul {
            margin-top: 0;
        }
    </style>
</head>

<?php

function DateToIndo($date)
{
    $BulanIndo = array("Januari", "Februari", "Maret", "April",
        "Mei", "Juni", "Juli", "Agustus", "September", "Oktober",
        "November", "Desember");

    $split = explode('-', explode(' ', $date)[0]);
    $tgl = $split[2];
    $bulan = $split[1];
    $tahun = $split[0];

    $result = $tgl . " " . $BulanIndo[(int)$bulan - 1] . " " . $tahun;

    return ($result);
}

function create_yth($cat) {
    if ($cat == 1)
        return 'Yth. Hasil pemeriksaan USG Abdomen :';
    else if ($cat == 2)
        return 'TS, Yth. Hasil Pemeriksaan USG Scrotum / Testis :';
    else if ($cat == 3)
        return 'TS, Yth. Hasil Pemeriksaan USG Urology :';
    else if ($cat == 4)
        return 'TS, Yth. Hasil Pemeriksaan USG Mammae :';
    else if ($cat == 5)
        return 'TS, Yth. Hasil Pemeriksaan USG Thyroid :';
    else if ($cat == 6)
        return 'TS, Yth. Hasil Pemeriksaan USG Fokus Lesi (Regio Umbilical) :';
    else if ($cat == 7)
        return 'TS, Yth. Hasil Pemeriksaan USG Kandungan 2D :';
    return '';
}

function get_data($cat, $meta) {
    if ($cat == 1)
        return $meta['usg_abdomen'];
    else if ($cat == 2)
        return $meta['usg_testis'];
    else if ($cat == 3)
        return $meta['usg_urologi'];
    else if ($cat == 4)
        return $meta['usg_mamae'];
    else if ($cat == 5)
        return $meta['usg_thyroid'];
    else if ($cat == 6)
        return $meta['usg_fokus_lesi'];
    else if ($cat == 7)
        return $meta['usg_kandungan_2d'];
    return '';
}

function build_surat($yth, $data, $klinik, $pasien, $nama_dokter) {
    return '<table class="sakit" style="text-align: center;">
       <tr>
          <td width="19%"><img width="90px" style="padding-right:20px;"  src="' . base_url() . 'assets/img/klinik/' . $klinik->foto . '" class="user-image" alt="User Image"></td>
          <td width="82%" style="text-align: center;">
             <p style="text-align: center; font-size: 20px"><strong>' . strtoupper($klinik->nama) . '</strong></p>
             <p style="text-align: center;"><strong>' . $klinik->alamat . ' Telp. ' . $klinik->telepon . '</strong></p>
             <p style="text-align: center;"><strong>Email: ' . $klinik->email . '</strong></p>
             <p style="text-align: center;"><strong>Kabupaten ' . $klinik->kabupaten . '</strong></p>
          </td>
       </tr>
    </table>
    <hr style="padding-bottom: -20px;"/>
    
    <p style="text-align: right">Sampang, '.DateToIndo(date('Y-m-d')).'</p>
    
    <table>
        <tr>
            <td>No RM</td>    
            <td>:</td>    
            <td>' . $pasien->no_rm . '</td>    
        </tr>
        <tr>
            <td>Nama / Tgl Lahir</td>    
            <td>:</td>    
            <td>' . $pasien->nama . ' / ' . DateToIndo($pasien->tanggal_lahir) . '</td>    
        </tr>
        <tr> 
            <td>Klinis</td>    
            <td>:</td>    
            <td>'.$data['klinis'].'</td>
        </tr>
        <tr>
            <td>Pengirim</td>    
            <td>:</td>    
            <td>'.$data['pengirim'].'</td>    
        </tr>
    </table>
    <br>
    <p><strong>'.$yth.'</strong></p>
    <br>
    '.$data['deskripsi'].'
    <b>Kesimpulan:
    '.$data['kesimpulan'].'</b>
    <br>
    
    <div style="padding-left: 450px;">
        <div style="width: 300px; text-align: center; font-weight: bold">
            PEMERIKSA,
            <br>
            <br>
            <br>
            <br>
            <u>'.$nama_dokter.'</u><br>
            RADIOLOGIST
        </div>
    </div>
    ';
}

//ini_set("memory_limit", "20000M");
//$mpdf = new mPDF('c', 'A4', '', '', 15, 15, 10, 5, 5, 5);
//$mpdf->mirrorMargins = 10;  // Use different Odd/Even headers and footers and mirror margins
$html = '';

foreach ($tindakan as $k => $v) {
    $cat = $v->{"Column 6"} ?? '0';
    $yth = create_yth($cat);

    $data = '';
    if ($cat == 1 && $pasien->jk == 'L')
        $data = $meta['usg_abdomen']['l'];
    else if ($cat == 1 && $pasien->jk == 'P')
        $data = $meta['usg_abdomen']['p'];
    else
        $data = $meta[strtolower(str_replace(' ', '_', $v->nama))];

//    if ($k > 0)
//        $mpdf->AddPage();
    if ($k > 0)
        $html .= '<div class="pagebreak"></div>';

//    $mpdf->WriteHTML(build_surat($yth, $data, $klinik, $pasien, $pemeriksaan->nama_dokter));
    $html .= build_surat($yth, $data, $klinik, $pasien, $nama_dokter);
}

//$mpdf->SetHTMLFooter($footer, 'E');
//$mpdf->Output("Radiologi $pemeriksaan->nama_pasien.pdf", 'D');
?>

<body>
<?=$html?>
<script>
    window.onload = function () {
        window.print()
    }
</script>
</body>
