<div id="bed" class="modal fade bedmodal" role="dialog">
    <div class="modal-dialog modal100per">
        <!-- Modal content-->
        <div class="modal-content fullshadow">
            <button type="button" class="ukclose" data-dismiss="modal">&times;</button>
            <div class="modal-body">
                <div id="ajaxbedstatus"></div>
            </div>
        </div>
    </div>
</div>

<footer class="main-footer">
    <div class="pull-right hidden-xs">
        <b>Version</b> 1.0.0
    </div>
    <strong>Copyright &copy; Padmanegara Digital Inovasi</strong> All rights
    reserved.
</footer>

<script>
    function get_bed_status() {
        $("#beddata").button('loading');
        $.ajax({
            url: '<?php echo base_url() ?>master/Bed/getBedStatus/',
            type: 'POST',
            data: '',
            //dataType: "json",
            success: function (res) {
                $("#ajaxbedstatus").html(res);
                $("#bed").modal('show');
                $("#beddata").button('reset');
            }
        })
    }
</script>

<script>
    $(document).ready(function(){
        var table = $('#printable_table').DataTable({
            dom: "Bfrtip",
            buttons: [
                {
                    extend: 'copyHtml5',
                    text: '<i class="fa fa-files-o"></i>',
                    titleAttr: 'Copy',
                    exportOptions: {
                        columns: ':visible'
                    }
                },
                {
                    extend: 'excelHtml5',
                    text: '<i class="fa fa-file-excel-o"></i>',
                    titleAttr: 'Excel',
                    exportOptions: {
                        columns: ':visible',
                        // format: {
                        //     body: function ( data, row, column, node ) {
                        //         //need to change double quotes to single
                        //         data = data.replace( /"/g, "'" );
                        //         //split at each new line
                        //         let splitData = data.split('<br>');
                        //         data = '';
                        //         for (i=0; i < splitData.length; i++) {
                        //             //add escaped double quotes around each line
                        //             data += '\"' + splitData[i] + '\"';
                        //             //if its not the last line add CHAR(13)
                        //             if (i + 1 < splitData.length) {
                        //                 data += ', CHAR(10), ';
                        //             }
                        //         }
                        //         //Add concat function
                        //         data = 'CONCATENATE(' + data + ')';
                        //         return data;
                        //     }
                        // }
                    },
                    // customize: function( xlsx ) {
                    //     var sheet = xlsx.xl.worksheets['sheet1.xml'];
                    //     var col = $('col', sheet);
                    //     //set the column width otherwise it will be the length of the line without the newlines
                    //     $(col[3]).attr('width', 50);
                    //
                    //     // Apply cell styling for wrap text and to make the cell a function
                    //     // For the last row column E and F
                    //     $('row:last c[r^="E"], row:last c[r^="F"]', sheet).each(function() {
                    //         if ($('is t', this).text()) {
                    //             // //wrap text
                    //             // $(this).attr('s', '55');
                    //             // //change the type to `str` which is a formula
                    //             // $(this).attr('t', 'str');
                    //             // //append the concat formula
                    //             // $(this).append('<f>' + $('is t', this).text() + '</f>');
                    //         //     //remove the inlineStr
                    //         //     $('is', this).remove();
                    //         }
                    //     })
                    //
                    //     // // Set the row hieght of the last ro(footer)
                    //     // $('row:last', sheet).each(function(index) {
                    //     //     $(this).attr('ht', 90);
                    //     //     $(this).attr('customHeight', 1);
                    //     // });
                    //
                    // }
                },
                {
                    extend: 'pdfHtml5',
                    text: '<i class="fa fa-file-pdf-o"></i>',
                    titleAttr: 'PDF',
                    exportOptions: {
                        columns: ':visible'
                    },
                    orientation: 'landscape',
                    pageSize: 'LEGAL'
                },
                {
                    extend: 'print',
                    text: '<i class="fa fa-print"></i>',
                    titleAttr: 'Print',
                    customize: function ( win ) {
                        $(win.document.body)
                            .css( 'font-size', '10pt' );

                        $(win.document.body).find( 'table' )
                            .addClass( 'compact' )
                            .css( 'font-size','inherit');
                    },
                    exportOptions: {
                        columns: ':visible'
                    }
                },
            ]
        });
        table.buttons().container().appendTo( '#printable_table_wrapper .col-md-6:eq(0)' );
    });
</script>
