<?php
$u = $this->session->userdata('logged_in');
$non_rajal = $this->session->userdata('non_rajal');

function seq($seg, $val, $ignore_case = true) {
    $ci =& get_instance();
    return $ignore_case ?
        strtolower($ci->uri->segment($seg)) == strtolower($val) :
        $ci->uri->segment($seg) == $val;
}

function sneq($seg, $val, $ignore_case = true) {
    $ci =& get_instance();
    return $ignore_case ?
        strtolower($ci->uri->segment($seg)) != strtolower($val) :
        $ci->uri->segment($seg) != $val;
}

function seq_array($seg, $val, $ignore_case = true) {
    $ci =& get_instance();
    return $ignore_case ?
        in_array(strtolower($ci->uri->segment($seg)), $val) :
        in_array($ci->uri->segment($seg), $val);
}

function superadmin($u) {
    return $u->nama_grup == 'superadmin';
}

function admin($u) {
    return $u->nama_grup == 'admin';
}

function dokter($u) {
    return $u->nama_grup == 'dokter' && $u->id_jenis_pendaftaran != 59;
}

function dokter_radiologi($u) {
    return $u->nama_grup == 'dokter' && $u->id_jenis_pendaftaran != 59;
}

function perawat($u) {
    return $u->nama_grup == 'perawat';
}

function perawat_rawat_jalan($u) {
    return perawat($u) && ($u->jenis_perawat == 'rawat_jalan' || $u->jenis_perawat == '' || $u->jenis_perawat == null);
}

function perawat_igd($u) {
    return perawat($u) && $u->jenis_perawat == 'igd';
}

function perawat_rawat_inap($u) {
    return perawat($u) && $u->jenis_perawat == 'rawat_inap';
}

function perawat_ruang_bersalin($u) {
    return perawat($u) && $u->jenis_perawat == 'ruang_bersalin';
}

function perawat_ruang_operasi($u) {
    return perawat($u) && $u->jenis_perawat == 'ruang_operasi';
}

function perawat_hcu($u) {
    return perawat($u) && $u->jenis_perawat == 'hcu';
}

function perawat_perinatologi($u) {
    return perawat($u) && $u->jenis_perawat == 'perinatologi';
}

function dokter_rawat_inap($u, $non_rajal) {
    $yes = false;
    foreach ($non_rajal as $v) {
        if ($v->ruangan_id == 1) {
            $yes = true;
            break;
        }
    }
    return dokter($u) && $yes;
}

function dokter_ruang_bersalin($u, $non_rajal) {
    $yes = false;
    foreach ($non_rajal as $v) {
        if ($v->ruangan_id == 2) {
            $yes = true;
            break;
        }
    }
    return dokter($u) && $yes;
}

function dokter_ruang_operasi($u, $non_rajal) {
    $yes = false;
    foreach ($non_rajal as $v) {
        if ($v->ruangan_id == 3) {
            $yes = true;
            break;
        }
    }
    return dokter($u) && $yes;
}

function dokter_hcu($u, $non_rajal) {
    $yes = false;
    foreach ($non_rajal as $v) {
        if ($v->ruangan_id == 5) {
            $yes = true;
            break;
        }
    }
    return dokter($u) && $yes;
}

function dokter_perinatologi($u, $non_rajal) {
    $yes = false;
    foreach ($non_rajal as $v) {
        if ($v->ruangan_id == 4) {
            $yes = true;
            break;
        }
    }
    return dokter($u) && $yes;
}

function apoteker($u) {
    return $u->nama_grup == 'apoteker';
}

function gizi($u) {
    return $u->nama_grup == 'gizi';
}

function frontOffice($u) {
    return $u->nama_grup == 'front_office';
}

function adminKeuangan($u) {
    return $u->nama_grup == 'administrasi_keuangan';
}

function kasir($u) {
    return $u->nama_grup == 'kasir';
}

function pimpinan($u) {
    return $u->nama_grup == 'pimpinan';
}

function laborat($u) {
    return $u->nama_grup == 'laborat';
}

function ekg($u) {
    return $u->nama_grup == 'ekg';
}

function spirometri($u) {
    return $u->nama_grup == 'spirometri';
}

function kabag_farmasi($u) {
    return $u->nama_grup == 'kabag_farmasi';
}

function kabag_layanan_medis($u) {
    return $u->nama_grup == 'kabag_layanan_medis';
}

function kabag_keuangan($u) {
    return $u->nama_grup == 'kabag_keuangan';
}

function admin_obat($u) {
    return $u->nama_grup == 'admin_obat';
}
?>

<aside class="main-sidebar">
    <section class="sidebar">
        <div class="user-panel">
            <div class="pull-left image">
                <img src="<?= base_url() ?>assets/img/profil/<?= $u->foto; ?>"
                     onerror="this.onerror=null;this.src='<?php echo base_url() ?>assets/img/logo.png';"
                     class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p><?= $u->nama;  ?></p>
                <?= $u->nama_grup;  ?>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>

        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">MAIN NAVIGATION</li>
            <?php

            if (superadmin($u) || adminKeuangan($u) || admin($u) || kabag_farmasi($u) || kabag_keuangan($u) || kabag_layanan_medis($u) || pimpinan($u)) {
                dashboard();
            }
            if (superadmin($u) || frontOffice($u) || admin($u) || kabag_layanan_medis($u) || perawat_igd($u)) {
                pendaftaran_pasien();
            }
            if (superadmin($u) || frontOffice($u) || admin($u)) {
                bpjs();
            }
            if (superadmin($u) || frontOffice($u) || admin($u) || perawat_igd($u) || kabag_layanan_medis($u)) {
                igd();
            }
            if (superadmin($u) || perawat_rawat_jalan($u) || admin($u) || kabag_layanan_medis($u)) {
                pemeriksaan_awal();
            }
            if (superadmin($u) || perawat_rawat_jalan($u) || dokter($u) || admin($u) || ekg($u) || spirometri($u) || kabag_layanan_medis($u)) {
                pemeriksaan_pasien();
            }
            if (superadmin($u) || admin($u) || perawat_rawat_jalan($u)|| perawat_igd($u)|| perawat_rawat_inap($u)|| perawat_ruang_bersalin($u) || perawat_ruang_operasi($u)|| perawat_hcu($u)|| perawat_perinatologi($u) || kabag_layanan_medis($u)) {
                transfer_pasien();
            }
            if (superadmin($u) || admin($u) || perawat_rawat_inap($u)|| kabag_keuangan($u) || kabag_layanan_medis($u) || pimpinan($u) || dokter_rawat_inap($u, $non_rajal) || gizi($u) || apoteker($u)) {
                rawat_inap($u);
            }
            if (superadmin($u) || admin($u) || perawat_ruang_bersalin($u) || kabag_layanan_medis($u) || pimpinan($u) || dokter_ruang_bersalin($u, $non_rajal)) {
                ruang_bersalin($u);
            }
            if (superadmin($u) || admin($u) || perawat_ruang_operasi($u) || kabag_layanan_medis($u) || pimpinan($u) || dokter_ruang_operasi($u, $non_rajal)) {
                ruang_operasi($u);
            }
            if (superadmin($u) || admin($u) || perawat_hcu($u) || kabag_layanan_medis($u) || pimpinan($u) || dokter_hcu($u, $non_rajal)) {
                hcu($u);
            }
            if (superadmin($u) || admin($u) || perawat_perinatologi($u) || kabag_layanan_medis($u) || pimpinan($u) || dokter_perinatologi($u, $non_rajal)) {
                perinatologi($u);
            }
            if (superadmin($u) || dokter_radiologi($u) || admin($u) || kabag_layanan_medis($u) || perawat_rawat_jalan($u)) {
                radiologi();
            }
            if (superadmin($u) || admin($u) || laborat($u)) {
                pemeriksaan_lab();
            }
            if (superadmin($u) || frontOffice($u) || admin($u) || kabag_layanan_medis($u)) {
                ambulance();
            }
//            inventory();
            if (superadmin($u) || frontOffice($u) || admin($u) || ekg($u) || spirometri($u)) {
                master_data($u);
            }
            if (superadmin($u) || apoteker($u) || admin($u) || kabag_farmasi($u) || admin_obat($u)) {
                apotek($u);
            }
            if (superadmin($u) || admin($u) || kasir($u) || kabag_farmasi($u) || kabag_keuangan($u)) { // BILLING billing
                administrasi_periksa($u);
            }
            if (superadmin($u) || dokter($u) || dokter_radiologi($u) || admin($u)) {
                surat_ket_sehat();
            }
            if (superadmin($u) || dokter($u) || dokter_radiologi($u) || admin($u)) {
                surat_ket_sakit();
            }
            if (superadmin($u) || dokter($u) || dokter_radiologi($u) || admin($u)) {
                surat_ket_rujuk();
            }
            if (superadmin($u) || dokter($u) || dokter_radiologi($u) || admin($u)) {
                surat_ranap();
            }
            if (superadmin($u) || admin($u) || perawat_igd($u) || perawat_rawat_jalan($u) || perawat_ruang_operasi($u) || perawat_ruang_bersalin($u)
                || perawat_rawat_inap($u) || perawat_hcu($u) || perawat_perinatologi($u))
            {
                laporan_harian_shift();
            }
            if (superadmin($u) || adminKeuangan($u) || admin($u) || kabag_keuangan($u) || pimpinan($u)) {
                intensif_karyawan();
            }
            if (superadmin($u) || dokter($u) || perawat_rawat_jalan($u) || dokter_radiologi($u) || admin($u) || kabag_farmasi($u) || kabag_keuangan($u) || perawat_rawat_inap($u) || pimpinan($u)) {
                executive_summary($u);
            }
            if (superadmin($u) || adminKeuangan($u) || kabag_farmasi($u) || kabag_keuangan($u) || pimpinan($u)) {
                laporan_keuangan($u);
            }
            if (superadmin($u) || admin($u)) {
                pengaturan_akun();
            }

            logout();

            ?>
        </ul>
    </section>
</aside>

<?php function dashboard() { ?>
    <li class="<?= seq(1, 'dashboard') ? 'active' : ''; ?>">
        <a href="<?= base_url();?>dashboard">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
        </a>
    </li>
<?php } ?>

<?php function pendaftaran_pasien() { ?>
    <li class="treeview <?= (seq(1, 'AntrianFrontOffice') || seq(1, 'AntrianPoli') || seq(1, 'pendaftaran') || (seq(1, 'TransferPasien') && seq(2, 'showList'))) ? 'active menu-open' : ''; ?>">
        <a href="#">
            <i class="fa  fa-user-plus"></i> <span>Pendaftaran Pasien</span>
            <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
        </a>
        <ul class="treeview-menu">
            <li class="<?= seq(1, 'AntrianFrontOffice') ? 'active' : ''; ?>"><a href="<?= base_url();?>AntrianFrontOffice"><i class="fa fa-circle-o"></i> Antrian Front Office</a></li>
            <li class="<?= seq(1, 'AntrianPoli') ? 'active' : ''; ?>"><a href="<?= base_url();?>AntrianPoli"><i class="fa fa-circle-o"></i> Antrian Poli</a></li>
            <li class="treeview <?= seq(2, 'listPendaftaranPasien') || seq(2, 'listKontrol') ? 'active menu-open' : ''; ?>">
                <a href="#">
                    <i class="fa fa-circle-o"></i> Rawat Jalan <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
                </a>
                <ul class="treeview-menu">
                    <li class="<?= seq(2, 'listPendaftaranPasien') ? 'active' : ''; ?>"><a href="<?= base_url();?>pendaftaran/listPendaftaranPasien"><i class="fa fa-circle-o"></i> List Pendaftaran</a></li>
                    <li class="<?= seq(2, 'listKontrol') ? 'active' : ''; ?>"><a href="<?= base_url();?>pendaftaran/listKontrol"><i class="fa fa-circle-o"></i> List Kontrol</a></li>
                </ul>
            </li>
            <li class="treeview <?= seq(1, 'TransferPasien') && seq(2, 'showList') && seq(3, 1) ? 'active menu-open' : ''; ?>">
                <a href="#">
                    <i class="fa fa-circle-o"></i> Rawat Inap <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
                </a>
                <ul class="treeview-menu">
                    <li class="<?= seq(1, 'TransferPasien') && seq(2, 'showList') && seq(3, 1) ? 'active' : ''; ?>"><a href="<?= base_url();?>TransferPasien/showList/1"><i class="fa fa-circle-o"></i> List Transfer</a></li>
                </ul>
            </li>
            <li class="treeview <?= seq(1, 'TransferPasien') && seq(2, 'showList') && seq(3, 4) ? 'active menu-open' : ''; ?>">
                <a href="#">
                    <i class="fa fa-circle-o"></i> Ruang Bersalin <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
                </a>
                <ul class="treeview-menu">
                    <li class="<?= seq(1, 'TransferPasien') && seq(2, 'showList') && seq(3, 4) ? 'active' : ''; ?>"><a href="<?= base_url();?>TransferPasien/showList/4"><i class="fa fa-circle-o"></i> List Transfer</a></li>
                </ul>
            </li>
            <li class="treeview <?= seq(1, 'TransferPasien') && seq(2, 'showList') && seq(3, 2) ? 'active menu-open' : ''; ?>">
                <a href="#">
                    <i class="fa fa-circle-o"></i> Ruang Operasi <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
                </a>
                <ul class="treeview-menu">
                    <li class="<?= seq(1, 'TransferPasien') && seq(2, 'showList') && seq(3, 2) ? 'active' : ''; ?>"><a href="<?= base_url();?>TransferPasien/showList/2"><i class="fa fa-circle-o"></i> List Transfer</a></li>
                </ul>
            </li>
        </ul>
    </li>
<?php } ?>

<?php function bpjs() { ?>
    <li class="treeview <?= (seq(1, 'Bpjs')) ? 'active menu-open' : ''; ?>">
        <a href="#">
            <i class="fa fa-plus-square"></i> <span>Administrasi BPJS</span>
            <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
        </a>
        <ul class="treeview-menu">
            <li class="<?= seq(1, 'bpjs') && (seq(2, 'peserta')) ? 'active' : ''; ?>"><a href="<?= base_url();?>bpjs/peserta/index"><i class="fa fa-circle-o"></i> Cek Peserta</a></li>
            <li class="treeview <?= seq(1, 'bpjs') && (seq(2, 'sep') || seq(2, 'sep_persetujuan') || seq(2, 'update_pulang')) ? 'active menu-open' : ''; ?>">
                <a href="#">
                    <i class="fa fa-circle-o"></i> SEP <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
                </a>
                <ul class="treeview-menu">
                    <li class="<?= seq(1, 'bpjs') && seq(2, 'sep') && seq(3, 'create') ? 'active' : ''; ?>"><a href="<?= base_url();?>bpjs/sep/create"><i class="fa fa-circle-o"></i> Pembuatan SEP</a></li>
                    <li class="<?= seq(1, 'bpjs') && seq(2, 'sep') && seq(3, 'daftar') ? 'active' : ''; ?>"><a href="<?= base_url();?>bpjs/sep/daftar"><i class="fa fa-circle-o"></i> Daftar SEP</a></li>
                    <li class="<?= seq(1, 'bpjs') && seq(2, 'sep') && seq(3, 'persetujuan') ? 'active' : ''; ?>"><a href="<?= base_url();?>bpjs/sep/persetujuan"><i class="fa fa-circle-o"></i> Persetujuan SEP</a></li>
<!--<li class="--><?//= seq(1, 'bpjs') && seq(2, 'sep') && seq(3, 'update_pulang') ? 'active' : ''; ?><!--"><a href="--><?//= base_url();?><!--bpjs/sep/update_pulang"><i class="fa fa-circle-o"></i> Update Pulang SEP</a></li>-->
                </ul>
            </li>
            <li class="treeview <?= seq(1, 'bpjs') && seq(2, 'rujukan') ? 'active menu-open' : ''; ?>">
                <a href="#">
                    <i class="fa fa-circle-o"></i> Rujukan <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
                </a>
                <ul class="treeview-menu">
                    <li class="<?= seq(1, 'bpjs') && seq(2, 'rujukan') && (seq(3, 'index') || seq(3, 'create')) ? 'active' : ''; ?>"><a href="<?= base_url();?>bpjs/rujukan/index"><i class="fa fa-circle-o"></i> Pembuatan </a></li>
                    <li class="<?= seq(1, 'bpjs') && seq(2, 'rujukan') && seq(3, 'daftar') ? 'active' : ''; ?>"><a href="<?= base_url();?>bpjs/rujukan/daftar"><i class="fa fa-circle-o"></i> Daftar </a></li>
                </ul>
            </li>
            <li class="treeview <?= seq(1, 'bpjs') && seq(2, 'rujukan_khusus') ? 'active menu-open' : ''; ?>">
                <a href="#">
                    <i class="fa fa-circle-o"></i> Rujukan Khusus <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
                </a>
                <ul class="treeview-menu">
                    <li class="<?= seq(1, 'bpjs') && seq(2, 'rujukan_khusus') && (seq(3, 'index') || seq(3, 'create')) ? 'active' : ''; ?>"><a href="<?= base_url();?>bpjs/rujukan_khusus/index"><i class="fa fa-circle-o"></i> Pembuatan </a></li>
                    <li class="<?= seq(1, 'bpjs') && seq(2, 'rujukan_khusus') && seq(3, 'daftar') ? 'active' : ''; ?>"><a href="<?= base_url();?>bpjs/rujukan_khusus/daftar"><i class="fa fa-circle-o"></i> Daftar </a></li>
                </ul>
            </li>
            <li class="treeview <?= seq(1, 'bpjs') && seq(2, 'prb') ? 'active menu-open' : ''; ?>">
                <a href="#">
                    <i class="fa fa-circle-o"></i> Rujuk Balik (PRB) <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
                </a>
                <ul class="treeview-menu">
                    <li class="<?= seq(1, 'bpjs') && seq(2, 'prb') && (seq(3, 'index') || seq(3, 'create')) ? 'active' : ''; ?>"><a href="<?= base_url();?>bpjs/prb/index"><i class="fa fa-circle-o"></i> Pembuatan </a></li>
                    <li class="<?= seq(1, 'bpjs') && seq(2, 'prb') && seq(3, 'daftar') ? 'active' : ''; ?>"><a href="<?= base_url();?>bpjs/prb/daftar"><i class="fa fa-circle-o"></i> Daftar </a></li>
                </ul>
            </li>
            <li class="treeview <?= seq(1, 'bpjs') && seq(2, 'rencana_kontrol') ? 'active menu-open' : ''; ?>">
                <a href="#">
                    <i class="fa fa-circle-o"></i> Rencana Kontrol / Inap <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
                </a>
                <ul class="treeview-menu">
                    <li class="<?= seq(1, 'bpjs') && seq(2, 'rencana_kontrol') && seq(3, 'index') ? 'active' : ''; ?>"><a href="<?= base_url();?>bpjs/rencana_kontrol/index"><i class="fa fa-circle-o"></i> Pembuatan </a></li>
                    <li class="<?= seq(1, 'bpjs') && seq(2, 'rencana_kontrol') && seq(3, 'daftar') ? 'active' : ''; ?>"><a href="<?= base_url();?>bpjs/rencana_kontrol/daftar"><i class="fa fa-circle-o"></i> Daftar </a></li>
                </ul>
            </li>
            <li class="treeview <?= seq(1, 'bpjs') && seq(2, 'laporan') ? 'active menu-open' : ''; ?>">
                <a href="#">
                    <i class="fa fa-circle-o"></i> Laporan <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
                </a>
                <ul class="treeview-menu">
                    <li class="<?= seq(1, 'bpjs') && seq(2, 'laporan') && seq(3, 'kunjungan') ? 'active' : ''; ?>"><a href="<?= base_url();?>bpjs/laporan/kunjungan"><i class="fa fa-circle-o"></i> Kunjungan </a></li>
                    <li class="<?= seq(1, 'bpjs') && seq(2, 'laporan') && seq(3, 'klaim') ? 'active' : ''; ?>"><a href="<?= base_url();?>bpjs/laporan/klaim"><i class="fa fa-circle-o"></i> Klaim </a></li>
                    <li class="<?= seq(1, 'bpjs') && seq(2, 'laporan') && seq(3, 'history_pelayanan_peserta') ? 'active' : ''; ?>"><a href="<?= base_url();?>bpjs/laporan/history_pelayanan_peserta"><i class="fa fa-circle-o"></i> History Pelayanan Peserta </a></li>
                    <li class="<?= seq(1, 'bpjs') && seq(2, 'laporan') && seq(3, 'klaim_jaminan_jasa_raharja') ? 'active' : ''; ?>"><a href="<?= base_url();?>bpjs/laporan/klaim_jaminan_jasa_raharja"><i class="fa fa-circle-o"></i> Klaim Jaminan Jasa Raharja </a></li>
                </ul>
            </li>
            <li class="<?= seq(1, 'bpjs') && seq(2, 'jadwal_dokter') ? 'active' : ''; ?>"><a href="<?= base_url();?>bpjs/jadwal_dokter"><i class="fa fa-circle-o"></i> Jadwal Dokter </a></li>
        </ul>
    </li>
<?php } ?>

<?php function transfer_pasien() { ?>
    <li class="treeview <?= (seq(1, 'transferpasien') && sneq(2, 'showList') && sneq(2, 'daftar')) ? 'active' : ''; ?>">
        <a href="#">
            <i class="fa fa-random"></i> <span>Transfer Pasien</span>
            <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
        </a>
        <ul class="treeview-menu">

            <li class="treeview <?= seq(1, 'TransferPasien') && seq(2, 'add') && seq_array(3, [1,7,8,14,9]) ? 'active menu-open' : ''; ?>">
                <a href="#">
                    <i class="fa fa-circle-o"></i> Dari Poli <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
                </a>
                <ul class="treeview-menu">
                    <li class="<?= seq(2, 'add') && seq(3, 1) ? 'active' : ''; ?>"><a href="<?= base_url();?>TransferPasien/add/1"><i class="fa fa-circle-o"></i> Poli Ke Ranap</a></li>
                    <li class="<?= seq(2, 'add') && seq(3, 7) ? 'active' : ''; ?>"><a href="<?= base_url();?>TransferPasien/add/7"><i class="fa fa-circle-o"></i> Poli Ke Ruang Operasi</a></li>
                    <li class="<?= seq(2, 'add') && seq(3, 8) ? 'active' : ''; ?>"><a href="<?= base_url();?>TransferPasien/add/8"><i class="fa fa-circle-o"></i> Poli Ke Ruang Bersalin</a></li>
                    <li class="<?= seq(2, 'add') && seq(3, 14) ? 'active' : ''; ?>"><a href="<?= base_url();?>TransferPasien/add/14"><i class="fa fa-circle-o"></i> Poli Ke HCU</a></li>
                    <li class="<?= seq(2, 'add') && seq(3, 9) ? 'active' : ''; ?>"><a href="<?= base_url();?>TransferPasien/add/9"><i class="fa fa-circle-o"></i> Poli Ke Perinatologi</a></li>
                </ul>
            </li>

            <li class="treeview <?= seq(1, 'TransferPasien') && (seq(2, 'listInap') || seq(2, 'add')) && seq_array(3, [2,4,15,10,3]) ? 'active menu-open' : ''; ?>">
                <a href="#">
                    <i class="fa fa-circle-o"></i> Dari Ranap <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
                </a>
                <ul class="treeview-menu">
                    <li class="<?= (seq(2, 'listInap') || seq(2, 'add')) && seq(3, 2) ? 'active' : ''; ?>"><a href="<?= base_url();?>TransferPasien/listInap/2"><i class="fa fa-circle-o"></i> Ranap Ke Ruang Operasi</a></li>
                    <li class="<?= (seq(2, 'listInap') || seq(2, 'add')) && seq(3, 4) ? 'active' : ''; ?>"><a href="<?= base_url();?>TransferPasien/listInap/4"><i class="fa fa-circle-o"></i> Ranap Ke Ruang Bersalin</a></li>
                    <li class="<?= (seq(2, 'listInap') || seq(2, 'add')) && seq(3, 15) ? 'active' : ''; ?>"><a href="<?= base_url();?>TransferPasien/listInap/15"><i class="fa fa-circle-o"></i> Ranap Ke HCU</a></li>
                    <li class="<?= (seq(2, 'listInap') || seq(2, 'add')) && seq(3, 10) ? 'active' : ''; ?>"><a href="<?= base_url();?>TransferPasien/listInap/10"><i class="fa fa-circle-o"></i> Ranap Ke Perinatologi</a></li>
                    <li class="<?= (seq(2, 'listInap') || seq(2, 'add')) && seq(3, 3) ? 'active' : ''; ?>"><a href="<?= base_url();?>TransferPasien/listInap/3"><i class="fa fa-circle-o"></i> Ranap Ke Ranap</a></li>
                </ul>
            </li>

            <li class="treeview <?= seq(1, 'TransferPasien') && (seq(2, 'listInap') || seq(2, 'add')) && seq_array(3, [16,11]) ? 'active menu-open' : ''; ?>">
                <a href="#">
                    <i class="fa fa-circle-o"></i> Dari Ruang Operasi <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
                </a>
                <ul class="treeview-menu">
                    <li class="<?= (seq(2, 'listInap') || seq(2, 'add')) && seq(3, 16) ? 'active' : ''; ?>"><a href="<?= base_url();?>TransferPasien/listInap/16"><i class="fa fa-circle-o"></i> Ke HCU</a></li>
                    <li class="<?= (seq(2, 'listInap') || seq(2, 'add')) && seq(3, 11) ? 'active' : ''; ?>"><a href="<?= base_url();?>TransferPasien/listInap/11"><i class="fa fa-circle-o"></i> Ke Perinatologi</a></li>
                </ul>
            </li>

            <li class="treeview <?= seq(1, 'TransferPasien') && (seq(2, 'listInap') || seq(2, 'add')) && seq_array(3, [17,12,19]) ? 'active menu-open' : ''; ?>">
                <a href="#">
                    <i class="fa fa-circle-o"></i> Dari Ruang Bersalin <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
                </a>
                <ul class="treeview-menu">
                    <li class="<?= (seq(2, 'listInap') || seq(2, 'add')) && seq(3, 19) ? 'active' : ''; ?>"><a href="<?= base_url();?>TransferPasien/listInap/19"><i class="fa fa-circle-o"></i> Ke Ruang Operasi</a></li>
                    <li class="<?= (seq(2, 'listInap') || seq(2, 'add')) && seq(3, 17) ? 'active' : ''; ?>"><a href="<?= base_url();?>TransferPasien/listInap/17"><i class="fa fa-circle-o"></i> Ke HCU</a></li>
                    <li class="<?= (seq(2, 'listInap') || seq(2, 'add')) && seq(3, 12) ? 'active' : ''; ?>"><a href="<?= base_url();?>TransferPasien/listInap/12"><i class="fa fa-circle-o"></i> Ke Perinatologi</a></li>
                </ul>
            </li>

        </ul>
    </li>
<?php } ?>

<?php function pemeriksaan_awal() { ?>
    <li class="<?= seq(1, 'PemeriksaanAwal') ? 'active' : ''; ?>">
        <a  href="<?= base_url();?>PemeriksaanAwal">
            <i class="fa fa-stethoscope"></i> <span>Pemeriksaan Awal</span>
        </a>
    </li>
<?php } ?>

<?php function pemeriksaan_pasien() { ?>
    <li  class="treeview <?= seq(1, 'pemeriksaan') ? 'active menu-open' : ''; ?>">
        <a href="#">
            <i class="fa fa-stethoscope"></i> <span>Pemeriksaan Pasien</span>
            <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
        </a>
        <ul class="treeview-menu">
            <li class="<?= seq(2, 'listpemeriksaanPasien') ? 'active' : ''; ?>"><a href="<?= base_url();?>pemeriksaan/listpemeriksaanPasien"><i class="fa fa-circle-o"></i> List Pasien daftar</a></li>
            <li class="<?= seq(2, 'listPasienSelesaiPeriksa') ? 'active' : ''; ?>"><a href="<?= base_url();?>pemeriksaan/listPasienSelesaiPeriksa"><i class="fa fa-circle-o"></i>List Pasien sudah diperiksa</a></li>
        </ul>
    </li>
<?php } ?>

<?php function igd() { ?>
    <li class="treeview <?= seq(1, 'Igd') ? 'active menu-open' : ''; ?>">
        <a href="#">
            <i class="fa fa-first-aid"></i> <span>IGD</span>
            <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
        </a>
        <ul class="treeview-menu">
<!--            <li class="--><?php //= seq(1, 'Igd') && (seq(2, 'pendaftaran') || seq(2, 'daftarPasienBaru') || seq(2, 'daftarPasienLama')) ? 'active' : ''; ?><!--">-->
<!--                <a href="--><?php //= base_url();?><!--Igd/pendaftaran"><i class="fa fa-circle-o"></i> Pendaftaran Pasien</a>-->
<!--            </li>-->
            <li class="<?= seq(1, 'Igd') && (seq(2, 'pemeriksaanAwal') || seq(2, 'periksaAwal')) ? 'active' : ''; ?>">
                <a href="<?= base_url();?>Igd/pemeriksaanAwal"><i class="fa fa-circle-o"></i> Pemeriksaan Awal</a>
            </li>
            <li class="<?= seq(1, 'Igd') && (seq(2, 'pemeriksaan') || seq(2, 'periksa')) ? 'active' : ''; ?>">
                <a href="<?= base_url();?>Igd/pemeriksaan"><i class="fa fa-circle-o"></i> Pemeriksaan</a>
            </li>
            <li class="<?= seq(1, 'Igd') && seq(2, 'selesai') ? 'active' : ''; ?>">
                <a href="<?= base_url();?>Igd/selesai"><i class="fa fa-circle-o"></i> List Pasien Sudah Periksa</a>
            </li>
        </ul>
    </li>
<?php } ?>

<?php function rawat_inap($u) { ?>
    <li class="treeview <?= seq(1, 'RawatInap') ? 'active menu-open' : ''; ?>">
        <a href="#">
            <i class="fa fa-bed"></i> <span>Rawat Inap</span>
            <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
        </a>
        <ul class="treeview-menu">
            <li class="<?= seq(1, 'RawatInap') && (seq(2, 'showList') || seq(2, 'detail')) ? 'active' : ''; ?>"><a href="<?= base_url();?>RawatInap/showList"><i class="fa fa-circle-o"></i> Pasien Rawat Inap</a></li>
            <?php if (!pimpinan($u)) : ?>
            <li class="<?= seq(1, 'RawatInap') && seq(2, 'listBolehPulang') ? 'active' : ''; ?>"><a href="<?= base_url();?>RawatInap/listBolehPulang"><i class="fa fa-circle-o"></i> Pasien Boleh Pulang</a></li>
            <li class="<?= seq(1, 'RawatInap') && (seq(2, 'listBilling') || seq(2, 'bayar')) ? 'active' : ''; ?>"><a href="<?= base_url();?>RawatInap/listBilling"><i class="fa fa-circle-o"></i> Kasir</a></li>
            <?php endif; ?>
            <li class="<?= seq(1, 'RawatInap') && seq(2, 'listRekapitulasi') ? 'active' : ''; ?>"><a href="<?= base_url();?>RawatInap/listRekapitulasi"><i class="fa fa-circle-o"></i> Rekapitulasi</a></li>
            <?php if (!pimpinan($u)) : ?>
            <li class="treeview <?= seq(1, 'RawatInap') && seq(2, 'dataTransfer') ? 'active menu-open' : ''; ?>">
                <a href="#">
                    <i class="fa fa-circle-o"></i> Data Transfer <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
                </a>
                <ul class="treeview-menu">
                    <li class="<?= seq(1, 'RawatInap') && seq(2, 'dataTransfer') && seq(3, 'masuk') ? 'active' : ''; ?>"><a href="<?= base_url();?>RawatInap/dataTransfer/masuk"><i class="fa fa-circle-o"></i> Masuk</a></li>
                    <li class="<?= seq(1, 'RawatInap') && seq(2, 'dataTransfer') && seq(3, 'keluar') ? 'active' : ''; ?>"><a href="<?= base_url();?>RawatInap/dataTransfer/keluar"><i class="fa fa-circle-o"></i> Keluar</a></li>
                </ul>
            </li>
            <?php endif; ?>
        </ul>
    </li>
<?php } ?>

<?php function ruang_bersalin($u) { ?>
    <li class="treeview <?= seq(1, 'RuangBersalin') ? 'active menu-open' : ''; ?>">
        <a href="#">
            <i class="fa fa-baby-carriage"></i> <span>Ruang Bersalin</span>
            <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
        </a>
        <ul class="treeview-menu">
            <li class="<?= seq(1, 'RuangBersalin') && (seq(2, 'showList') || seq(2, 'detail') || seq(2, 'selesai')) ? 'active' : ''; ?>"><a href="<?= base_url();?>RuangBersalin/showList"><i class="fa fa-circle-o"></i> Pasien Ruang Bersalin</a></li>
            <?php if (!pimpinan($u)) : ?>
            <li class="<?= seq(1, 'RuangBersalin') && (seq(2, 'listBilling') || seq(2, 'bayar')) ? 'active' : ''; ?>"><a href="<?= base_url();?>RuangBersalin/listBilling"><i class="fa fa-circle-o"></i> Kasir</a></li>
            <?php endif; ?>
            <li class="<?= seq(1, 'RuangBersalin') && seq(2, 'listRekapitulasi') ? 'active' : ''; ?>"><a href="<?= base_url();?>RuangBersalin/listRekapitulasi"><i class="fa fa-circle-o"></i> Rekapitulasi</a></li>
            <?php if (!pimpinan($u)) : ?>
            <li class="treeview <?= seq(1, 'RuangBersalin') && seq(2, 'dataTransfer') ? 'active menu-open' : ''; ?>">
                <a href="#">
                    <i class="fa fa-circle-o"></i> Data Transfer <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
                </a>
                <ul class="treeview-menu">
                    <li class="<?= seq(1, 'RuangBersalin') && seq(2, 'dataTransfer') && seq(3, 'masuk') ? 'active' : ''; ?>"><a href="<?= base_url();?>RuangBersalin/dataTransfer/masuk"><i class="fa fa-circle-o"></i> Masuk</a></li>
                    <li class="<?= seq(1, 'RuangBersalin') && seq(2, 'dataTransfer') && seq(3, 'keluar') ? 'active' : ''; ?>"><a href="<?= base_url();?>RuangBersalin/dataTransfer/keluar"><i class="fa fa-circle-o"></i> Keluar</a></li>
                </ul>
            </li>
            <?php endif; ?>
        </ul>
    </li>
<?php } ?>

<?php function ruang_operasi($u) { ?>
    <li class="treeview <?= seq(1, 'RuangOperasi') ? 'active menu-open' : ''; ?>">
        <a href="#">
            <i class="fa fa-cut"></i> <span>Ruang Operasi</span>
            <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
        </a>
        <ul class="treeview-menu">
            <li class="<?= seq(1, 'RuangOperasi') && (seq(2, 'showList') || seq(2, 'detail') || seq(2, 'selesai')) ? 'active' : ''; ?>"><a href="<?= base_url();?>RuangOperasi/showList"><i class="fa fa-circle-o"></i> Pasien Ruang Operasi</a></li>
            <?php if (!pimpinan($u)) : ?>
            <li class="<?= seq(1, 'RuangOperasi') && (seq(2, 'listBilling') || seq(2, 'bayar')) ? 'active' : ''; ?>"><a href="<?= base_url();?>RuangOperasi/listBilling"><i class="fa fa-circle-o"></i> Kasir</a></li>
            <?php endif; ?>
            <li class="<?= seq(1, 'RuangOperasi') && seq(2, 'listRekapitulasi') ? 'active' : ''; ?>"><a href="<?= base_url();?>RuangOperasi/listRekapitulasi"><i class="fa fa-circle-o"></i> Rekapitulasi</a></li>
            <?php if (!pimpinan($u)) : ?>
            <li class="treeview <?= seq(1, 'RuangOperasi') && seq(2, 'dataTransfer') ? 'active menu-open' : ''; ?>">
                <a href="#">
                    <i class="fa fa-circle-o"></i> Data Transfer <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
                </a>
                <ul class="treeview-menu">
                    <li class="<?= seq(1, 'RuangOperasi') && seq(2, 'dataTransfer') && seq(3, 'masuk') ? 'active' : ''; ?>"><a href="<?= base_url();?>RuangOperasi/dataTransfer/masuk"><i class="fa fa-circle-o"></i> Masuk</a></li>
                    <li class="<?= seq(1, 'RuangOperasi') && seq(2, 'dataTransfer') && seq(3, 'keluar') ? 'active' : ''; ?>"><a href="<?= base_url();?>RuangOperasi/dataTransfer/keluar"><i class="fa fa-circle-o"></i> Keluar</a></li>
                </ul>
            </li>
            <?php endif; ?>
        </ul>
    </li>
<?php } ?>

<?php function hcu($u) { ?>
    <li class="treeview <?= seq(1, 'HCU') ? 'active menu-open' : ''; ?>">
        <a href="#">
            <i class="fa fa-hand-holding-medical"></i> <span>HCU</span>
            <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
        </a>
        <ul class="treeview-menu">
            <li class="<?= seq(1, 'HCU') && (seq(2, 'showList') || seq(2, 'detail') || seq(2, 'selesai')) ? 'active' : ''; ?>"><a href="<?= base_url();?>HCU/showList"><i class="fa fa-circle-o"></i> Pasien HCU</a></li>
            <?php if (!pimpinan($u)) : ?>
            <li class="<?= seq(1, 'HCU') && (seq(2, 'listBilling') || seq(2, 'bayar')) ? 'active' : ''; ?>"><a href="<?= base_url();?>HCU/listBilling"><i class="fa fa-circle-o"></i> Kasir</a></li>
            <?php endif; ?>
            <li class="<?= seq(1, 'HCU') && seq(2, 'listRekapitulasi') ? 'active' : ''; ?>"><a href="<?= base_url();?>HCU/listRekapitulasi"><i class="fa fa-circle-o"></i> Rekapitulasi</a></li>
            <?php if (!pimpinan($u)) : ?>
            <li class="treeview <?= seq(1, 'HCU') && seq(2, 'dataTransfer') ? 'active menu-open' : ''; ?>">
                <a href="#">
                    <i class="fa fa-circle-o"></i> Data Transfer <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
                </a>
                <ul class="treeview-menu">
                    <li class="<?= seq(1, 'HCU') && seq(2, 'dataTransfer') && seq(3, 'masuk') ? 'active' : ''; ?>"><a href="<?= base_url();?>HCU/dataTransfer/masuk"><i class="fa fa-circle-o"></i> Masuk</a></li>
                    <li class="<?= seq(1, 'HCU') && seq(2, 'dataTransfer') && seq(3, 'keluar') ? 'active' : ''; ?>"><a href="<?= base_url();?>HCU/dataTransfer/keluar"><i class="fa fa-circle-o"></i> Keluar</a></li>
                </ul>
            </li>
            <?php endif; ?>
        </ul>
    </li>
<?php } ?>

<?php function perinatologi($u) { ?>
    <li class="treeview <?= seq(1, 'Perinatologi') ? 'active menu-open' : ''; ?>">
        <a href="#">
            <i class="fa fa-baby"></i> <span>Perinatologi</span>
            <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
        </a>
        <ul class="treeview-menu">
            <li class="<?= seq(1, 'Perinatologi') && (seq(2, 'showList') || seq(2, 'detail') || seq(2, 'selesai')) ? 'active' : ''; ?>"><a href="<?= base_url();?>Perinatologi/showList"><i class="fa fa-circle-o"></i> Pasien Perinatologi</a></li>
            <?php if (!pimpinan($u)) : ?>
            <li class="<?= seq(1, 'Perinatologi') && (seq(2, 'listBilling') || seq(2, 'bayar')) ? 'active' : ''; ?>"><a href="<?= base_url();?>Perinatologi/listBilling"><i class="fa fa-circle-o"></i> Kasir</a></li>
            <?php endif; ?>
            <li class="<?= seq(1, 'Perinatologi') && seq(2, 'listRekapitulasi') ? 'active' : ''; ?>"><a href="<?= base_url();?>Perinatologi/listRekapitulasi"><i class="fa fa-circle-o"></i> Rekapitulasi</a></li>
            <?php if (!pimpinan($u)) : ?>
            <li class="treeview <?= seq(1, 'Perinatologi') && seq(2, 'dataTransfer') ? 'active menu-open' : ''; ?>">
                <a href="#">
                    <i class="fa fa-circle-o"></i> Data Transfer <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
                </a>
                <ul class="treeview-menu">
                    <li class="<?= seq(1, 'Perinatologi') && seq(2, 'dataTransfer') && seq(3, 'masuk') ? 'active' : ''; ?>"><a href="<?= base_url();?>Perinatologi/dataTransfer/masuk"><i class="fa fa-circle-o"></i> Masuk</a></li>
                    <li class="<?= seq(1, 'Perinatologi') && seq(2, 'dataTransfer') && seq(3, 'keluar') ? 'active' : ''; ?>"><a href="<?= base_url();?>Perinatologi/dataTransfer/keluar"><i class="fa fa-circle-o"></i> Keluar</a></li>
                </ul>
            </li>
            <?php endif; ?>
        </ul>
    </li>
<?php } ?>

<?php function radiologi() { $ci =& get_instance(); ?>
    <li  class="treeview <?= seq(1, 'Radio') ? 'active menu-open' : ''; ?>">
        <a href="#">
            <i class="fa fas fa-x-ray"></i> <span>Radiologi</span>
            <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
        </a>
        <ul class="treeview-menu">
            <li class="<?= seq(1, 'Radio') && seq(2, 'search') ? 'active' : ''; ?>"><a href="<?= base_url();?>Radio/search"><i class="fa fa-circle-o"></i> List Pasien daftar</a></li>
            <li class="<?= seq(1, 'Radio') && seq(2, 'sudahPeriksa') ? 'active' : ''; ?>"><a href="<?= base_url();?>Radio/sudahPeriksa"><i class="fa fa-circle-o"></i>List Pasien sudah diperiksa</a></li>
        </ul>
    </li>
<?php } ?>

<?php function pemeriksaan_lab() { $ci =& get_instance(); ?>
    <li  class="treeview <?= ($ci->uri->segment(1) == 'laboratorium') ? 'active menu-open' : ''; ?>">
        <a href="#">
            <i class="fa fas fa-flask"></i> <span>Laboratorium</span>
            <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
        </a>
        <ul class="treeview-menu">
            <li class="<?= ($ci->uri->segment(2) == 'listpemeriksaanPasien') ? 'active' : ''; ?>"><a href="<?= base_url();?>laboratorium/listpemeriksaanPasien"><i class="fa fa-circle-o"></i> List Pasien daftar</a></li>
<!--            <li class="--><?//= ($ci->uri->segment(2) == 'pengecekanAwal') ? 'active' : ''; ?><!--"><a href="--><?//= base_url();?><!--laboratorium/pengecekanAwal"><i class="fa fa-circle-o"></i>Pengecekan Awal</a></li>-->
            <li class="<?= ($ci->uri->segment(2) == 'pemeriksaanLab') ? 'active' : ''; ?>"><a href="<?= base_url();?>laboratorium/pemeriksaanLab"><i class="fa fa-circle-o"></i>Pemeriksaan Lab</a></li>
            <li class="<?= ($ci->uri->segment(2) == 'rekapitulasiLab') ? 'active' : ''; ?>"><a href="<?= base_url();?>laboratorium/rekapitulasiLab"><i class="fa fa-circle-o"></i>Rekapitulasi Lab</a></li>
            <li class="<?= ($ci->uri->segment(2) == 'tarifDanLayanan') ? 'active' : ''; ?>"><a href="<?= base_url();?>laboratorium/tarifDanLayanan"><i class="fa fa-circle-o"></i>Tarif dan Layanan</a></li>
        </ul>
    </li>
<?php } ?>

<?php function ambulance() { $ci =& get_instance(); ?>
    <li  class="treeview <?= ($ci->uri->segment(1) == 'Ambulance') ? 'active menu-open' : ''; ?>">
        <a href="#">
            <i class="fa fa-ambulance"></i> <span>Ambulance</span>
            <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
        </a>
        <ul class="treeview-menu">
            <li class="<?= !$ci->uri->segment(2) ? 'active' : ''; ?>"><a href="<?= base_url();?>Ambulance"><i class="fa fa-circle-o"></i>Daftar Ambulance</a></li>
            <li class="<?= seq(2, 'getcallambulance') ? 'active' : ''; ?>"><a href="<?= base_url();?>Ambulance/getcallambulance"><i class="fa fa-circle-o"></i>Daftar Panggilan Ambulance</a></li>
            <li class="<?= seq(2, 'rekapAmbulance') ? 'active' : ''; ?>"><a href="<?= base_url();?>Ambulance/rekapAmbulance"><i class="fa fa-circle-o"></i>Rekapitulasi</a></li>
        </ul>
    </li>
<?php } ?>

<?php function inventory() { $ci =& get_instance(); ?>
    <li class="<?= $ci->uri->segment(1) == 'Inventory' ? 'active' : ''; ?>">
        <a  href="<?= base_url();?>Inventory">
            <i class="fas fa-luggage-cart"></i> <span>Inventory</span>
        </a>
    </li>
<?php } ?>

<?php function master_data($u) { $ci =& get_instance(); ?>
    <li class="treeview <?= (
        $ci->uri->segment(1) == 'mastergigi' ||
        $ci->uri->segment(1) == 'MasterPoli' ||
        $ci->uri->segment(1) == 'Apotek' ||
        $ci->uri->segment(1) == 'Pasien' ||
        $ci->uri->segment(1) == 'Dokter' ||
        $ci->uri->segment(1) == 'KabagKeuangan' ||
        $ci->uri->segment(1) == 'KabagLayananMedis' ||
        $ci->uri->segment(1) == 'KabagFarmasi' ||
        $ci->uri->segment(1) == 'AdminObat' ||
        $ci->uri->segment(1) == 'Admin' ||
        $ci->uri->segment(1) == 'AdminKeuangan' ||
        $ci->uri->segment(1) == 'Laboratorium' ||
        $ci->uri->segment(1) == 'Ekg' ||
        $ci->uri->segment(1) == 'Spirometri' ||
        $ci->uri->segment(1) == 'Perawat' ||
        $ci->uri->segment(1) == 'Apoteker' ||
        $ci->uri->segment(1) == 'Kasir' ||
        $ci->uri->segment(1) == 'FrontOffice' ||
        $ci->uri->segment(1) == 'Obat' ||
        $ci->uri->segment(2) == 'detailklinik' ||
        $ci->uri->segment(1) == 'JenisPendaftaran' ||
        $ci->uri->segment(1) == 'TarifTindakan' ||
        $ci->uri->segment(1) == 'Penyakit' ||
        ($ci->uri->segment(1) == 'User' && $ci->uri->segment(2) == 'adduser') ||
        $ci->uri->segment(1) == 'master' ||
        (seq(1, 'Pengeluaran') && (seq(2, 'list_item') || seq(2, 'create_item') || seq(2, 'edit_item')))
    ) ? 'active menu-open' : ''; ?>">
        <a href="#">
            <i class="fa fa-list"></i> <span>Master Data</span>
            <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
        </a>
        <ul class="treeview-menu">
            <?php
            if (superadmin($u) || admin($u)) master_data_poli();
            if (superadmin($u) || admin($u)) master_data_ranap();
            if (superadmin($u) || admin($u)) master_data_ambulan();
            if (superadmin($u) || admin($u)) master_data_hak_akes();
            if (superadmin($u) || admin($u)) master_data_other();
            if (superadmin($u) || admin($u)) master_data_hospital();
            if (frontOffice($u)) master_data_pasien();
            if (perawat($u)) master_data_tarif_tindakan();
            if (laborat($u)) master_data_laboratorium();
            if (ekg($u)) master_data_ekg();
            if (spirometri($u)) master_data_spirometri();
            ?>
        </ul>
    </li>
<?php } ?>

<?php function master_data_poli() { $ci =& get_instance(); ?>
  <?php $data_poli = $ci->config->item('poli'); ?>
    <?php foreach ($data_poli as $key => $value) { if (true) { ?>
        <li class="treeview <?= (in_array($ci->uri->segment(2), ['listPenyakit', 'listTarifTindakan']) && $ci->uri->segment(3) == $key) ? 'menu-open' : ''; ?>">
            <a href="#">
                <i class="fa fa-circle-o"></i> <?=$value['label']?> <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
            </a>
            <ul class="treeview-menu" <?= (in_array($ci->uri->segment(2), ['listPenyakit', 'listTarifTindakan']) && $ci->uri->segment(3) == $key) ? 'style="display:block;"' : ''; ?>>
                <li class="<?= ($ci->uri->segment(2) == 'listPenyakit' && $ci->uri->segment(3) == $key) ? 'active' : ''; ?>">
                    <a href="<?= base_url();?>MasterPoli/listPenyakit/<?=$key?>"><i class="fa fa-circle-o"></i>
                        <?=$key == 'ekg' || $key == 'spirometri' || $key == 'laboratorium' ? 'Jenis Layanan' : 'Jenis Penyakit'?>
                    </a>
                </li>
                <li class="<?= ($ci->uri->segment(2) == 'listTarifTindakan' && $ci->uri->segment(3) == $key) ? 'active' : ''; ?>"><a href="<?= base_url();?>MasterPoli/listTarifTindakan/<?=$key?>"><i class="fa fa-circle-o"></i>Tarif Tindakan</a></li>
                <?php if (in_array($key, ['ekg', 'spirometri', 'pemeriksaan-laborat'])) { ?>
                    <li class="<?= ($ci->uri->segment(2) == 'transaksi' && $ci->uri->segment(3) == $key) ? 'active' : ''; ?>"><a href="<?= base_url();?>MasterPoli/transaksi/<?=$key?>"><i class="fa fa-circle-o"></i>Rekapitulasi Transaksi</a></li>
                <?php } ?>
            </ul>
        </li>
    <?php } } ?>
<?php } ?>

<?php function master_data_ranap() { $ci =& get_instance(); ?>
    <li class="treeview  <?= (in_array($ci->uri->segment(2), ['listPenyakit', 'listTarifTindakan']) && $ci->uri->segment(3) == 'rawat_inap') ? 'menu-open' : ''; ?>">
        <a href="#">
            <i class="fa fa-circle-o"></i> Rawat Inap <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
        </a>
        <ul class="treeview-menu" <?= (in_array($ci->uri->segment(2), ['listPenyakit', 'listTarifTindakan']) && $ci->uri->segment(3) == 'rawat_inap') ? 'style="display:block;"' : ''; ?>>
            <li class="<?= ($ci->uri->segment(2) == 'listPenyakit' && $ci->uri->segment(3) == 'rawat_inap') ? 'active' : ''; ?>"><a href="<?= base_url();?>MasterPoli/listPenyakit/rawat_inap"><i class="fa fa-circle-o"></i>Jenis Penyakit</a></li>
            <li class="<?= ($ci->uri->segment(2) == 'listTarifTindakan' && $ci->uri->segment(3) == 'rawat_inap') ? 'active' : ''; ?>"><a href="<?= base_url();?>MasterPoli/listTarifTindakan/rawat_inap"><i class="fa fa-circle-o"></i>Tarif Tindakan</a></li>
        </ul>
    </li>
    <li class="treeview  <?= (in_array($ci->uri->segment(2), ['listPenyakit', 'listTarifTindakan']) && $ci->uri->segment(3) == 'ruang_bersalin') ? 'menu-open' : ''; ?>">
        <a href="#">
            <i class="fa fa-circle-o"></i> Ruang Bersalin <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
        </a>
        <ul class="treeview-menu" <?= (in_array($ci->uri->segment(2), ['listPenyakit', 'listTarifTindakan']) && $ci->uri->segment(3) == 'ruang_bersalin') ? 'style="display:block;"' : ''; ?>>
            <li class="<?= ($ci->uri->segment(2) == 'listPenyakit' && $ci->uri->segment(3) == 'ruang_bersalin') ? 'active' : ''; ?>"><a href="<?= base_url();?>MasterPoli/listPenyakit/ruang_bersalin"><i class="fa fa-circle-o"></i>Jenis Penyakit</a></li>
            <li class="<?= ($ci->uri->segment(2) == 'listTarifTindakan' && $ci->uri->segment(3) == 'ruang_bersalin') ? 'active' : ''; ?>"><a href="<?= base_url();?>MasterPoli/listTarifTindakan/ruang_bersalin"><i class="fa fa-circle-o"></i>Tarif Tindakan</a></li>
        </ul>
    </li>
    <li class="treeview  <?= (in_array($ci->uri->segment(2), ['listPenyakit', 'listTarifTindakan']) && $ci->uri->segment(3) == 'ruang_operasi') ? 'menu-open' : ''; ?>">
        <a href="#">
            <i class="fa fa-circle-o"></i> Ruang Operasi <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
        </a>
        <ul class="treeview-menu" <?= (in_array($ci->uri->segment(2), ['listPenyakit', 'listTarifTindakan']) && $ci->uri->segment(3) == 'ruang_operasi') ? 'style="display:block;"' : ''; ?>>
            <li class="<?= ($ci->uri->segment(2) == 'listPenyakit' && $ci->uri->segment(3) == 'ruang_operasi') ? 'active' : ''; ?>"><a href="<?= base_url();?>MasterPoli/listPenyakit/ruang_operasi"><i class="fa fa-circle-o"></i>Jenis Penyakit</a></li>
            <li class="<?= ($ci->uri->segment(2) == 'listTarifTindakan' && $ci->uri->segment(3) == 'ruang_operasi') ? 'active' : ''; ?>"><a href="<?= base_url();?>MasterPoli/listTarifTindakan/ruang_operasi"><i class="fa fa-circle-o"></i>Tarif Tindakan</a></li>
        </ul>
    </li>
    <li class="treeview  <?= (in_array($ci->uri->segment(2), ['listPenyakit', 'listTarifTindakan']) && $ci->uri->segment(3) == 'icd9') ? 'menu-open' : ''; ?>">
        <a href="#">
            <i class="fa fa-circle-o"></i> ICD-9 <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
        </a>
        <ul class="treeview-menu" <?= (in_array($ci->uri->segment(2), ['listPenyakit', 'listTarifTindakan']) && $ci->uri->segment(3) == 'ruang_operasi') ? 'style="display:block;"' : ''; ?>>
            <li class="<?= ($ci->uri->segment(2) == 'listPenyakit' && $ci->uri->segment(3) == 'icd9') ? 'active' : ''; ?>"><a href="<?= base_url();?>MasterPoli/listPenyakit/icd9"><i class="fa fa-circle-o"></i>Jenis Tindakan</a></li>
        </ul>
    </li>
<?php } ?>

<?php function master_data_ambulan() { $ci =& get_instance(); ?>
    <li class="<?= $ci->uri->segment(1) == 'TarifAmbulance' ? 'active' : ''; ?>"><a href="<?= base_url();?>TarifAmbulance"><i class="fa fa-circle-o"></i> Tarif Ambulance</a></li>
<?php } ?>

<?php function master_data_laboratorium() { $ci =& get_instance(); ?>
    <li class="treeview  <?= (in_array($ci->uri->segment(2), ['listPenyakit', 'listTarifTindakan']) && $ci->uri->segment(3) == 'laboratorium') ? 'menu-open' : ''; ?>">
        <a href="#">
            <i class="fa fa-circle-o"></i> Pemeriksaan Laboratorium <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
        </a>
        <ul class="treeview-menu" <?= (in_array($ci->uri->segment(2), ['listPenyakit', 'listTarifTindakan']) && $ci->uri->segment(3) == 'laboratorium') ? 'style="display:block;"' : ''; ?>>
            <li class="<?= ($ci->uri->segment(2) == 'listPenyakit' && $ci->uri->segment(3) == 'laboratorium') ? 'active' : ''; ?>"><a href="<?= base_url();?>MasterPoli/listPenyakit/laboratorium"><i class="fa fa-circle-o"></i>List Layanan</a></li>
            <li class="<?= ($ci->uri->segment(2) == 'listTarifTindakan' && $ci->uri->segment(3) == 'laboratorium') ? 'active' : ''; ?>"><a href="<?= base_url();?>MasterPoli/listTarifTindakan/laboratorium"><i class="fa fa-circle-o"></i>Tarif Tindakan</a></li>
        </ul>
    </li>
<?php } ?>

<?php function master_data_ekg() { $ci =& get_instance(); ?>
    <li class="treeview  <?= (in_array($ci->uri->segment(2), ['listPenyakit', 'listTarifTindakan']) && $ci->uri->segment(3) == 'ekg') ? 'menu-open' : ''; ?>">
        <a href="#">
            <i class="fa fa-circle-o"></i> Pemeriksaan Ekg <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
        </a>
        <ul class="treeview-menu" <?= (in_array($ci->uri->segment(2), ['listPenyakit', 'listTarifTindakan']) && $ci->uri->segment(3) == 'ekg') ? 'style="display:block;"' : ''; ?>>
            <li class="<?= ($ci->uri->segment(2) == 'listPenyakit' && $ci->uri->segment(3) == 'ekg') ? 'active' : ''; ?>"><a href="<?= base_url();?>MasterPoli/listPenyakit/ekg"><i class="fa fa-circle-o"></i>List Layanan</a></li>
            <li class="<?= ($ci->uri->segment(2) == 'listTarifTindakan' && $ci->uri->segment(3) == 'ekg') ? 'active' : ''; ?>"><a href="<?= base_url();?>MasterPoli/listTarifTindakan/ekg"><i class="fa fa-circle-o"></i>Tarif Tindakan</a></li>
        </ul>
    </li>
<?php } ?>

<?php function master_data_spirometri() { $ci =& get_instance(); ?>
    <li class="treeview  <?= (in_array($ci->uri->segment(2), ['listPenyakit', 'listTarifTindakan']) && $ci->uri->segment(3) == 'spirometri') ? 'menu-open' : ''; ?>">
        <a href="#">
            <i class="fa fa-circle-o"></i> Pemeriksaan Spirometri <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
        </a>
        <ul class="treeview-menu" <?= (in_array($ci->uri->segment(2), ['listPenyakit', 'listTarifTindakan']) && $ci->uri->segment(3) == 'spirometri') ? 'style="display:block;"' : ''; ?>>
            <li class="<?= ($ci->uri->segment(2) == 'listPenyakit' && $ci->uri->segment(3) == 'spirometri') ? 'active' : ''; ?>"><a href="<?= base_url();?>MasterPoli/listPenyakit/spirometri"><i class="fa fa-circle-o"></i>List Layanan</a></li>
            <li class="<?= ($ci->uri->segment(2) == 'listTarifTindakan' && $ci->uri->segment(3) == 'spirometri') ? 'active' : ''; ?>"><a href="<?= base_url();?>MasterPoli/listTarifTindakan/spirometri"><i class="fa fa-circle-o"></i>Tarif Tindakan</a></li>
        </ul>
    </li>
<?php } ?>

<?php function master_data_apotek() { $ci =& get_instance(); ?>
    <li class="treeview  <?= (in_array($ci->uri->segment(1), ['apotek']) ? 'menu-open' : '') ?>">
        <a href="#">
            <i class="fa fa-circle-o"></i> Apotek <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
        </a>
        <ul class="treeview-menu" <?= (in_array($ci->uri->segment(1), ['apotek']) ? 'style="display:block"' : '') ?>>
            <li class="<?= ($ci->uri->segment(2) == 'stokObat') ? 'active' : ''; ?>"><a href="<?= base_url();?>apotek/stokObat"><i class="fa fa-circle-o"></i> Stok Obat</a></li>
<!--            <li class="--><?//= ''; ?><!--"><a href="#"><i class="fa fa-circle-o"></i> Retur Pembelian Obat</a></li>-->
<!--            <li class="--><?//= ''; ?><!--"><a href="#"><i class="fa fa-circle-o"></i> Retur Penjualan Obat</a></li>-->
<!--            <li class="--><?//= ''; ?><!--"><a href="#"><i class="fa fa-circle-o"></i> HPP Obat</a></li>-->
            <li class="<?= (
                    $ci->uri->segment(2) == 'pembelian' ||
                    $ci->uri->segment(2) == 'editPembelianObat' ||
                    $ci->uri->segment(2) == 'tambahPembelianObat'
            ) ? 'active' : ''; ?>"><a href="<?= base_url();?>apotek/pembelian"><i class="fa fa-circle-o"></i> Pembelian</a></li>
            <li class="<?= ($ci->uri->segment(2) == 'gudang') ? 'active' : ''; ?>"><a href="<?= base_url();?>apotek/gudang"><i class="fa fa-circle-o"></i> Gudang</a></li>
            <li class="<?= ($ci->uri->segment(2) == 'mutasi') ? 'active' : ''; ?>"><a href="<?= base_url();?>apotek/mutasi"><i class="fa fa-circle-o"></i> Mutasi</a></li>
            <li class="<?= ($ci->uri->segment(2) == 'resep') ? 'active' : ''; ?>"><a href="<?= base_url();?>apotek/resep"><i class="fa fa-circle-o"></i> Rekapitulasi Resep</a></li>
            <li class="<?= (
                    $ci->uri->segment(2) == 'resep_n' ||
                    $ci->uri->segment(2) == 'resep_nota'
            ) ? 'active' : ''; ?>"><a href="<?= base_url();?>apotek/resep_n"><i class="fa fa-circle-o"></i> Resep</a></li>
        </ul>
    </li>
    <li class="treeview  <?= (in_array($ci->uri->segment(1), ['BahanHabisPakai']) ? 'menu-open' : '') ?>">
            <a href="#">
                <i class="fa fa-circle-o"></i> Bahan Habis Pakai <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
            </a>
            <ul class="treeview-menu" <?= (in_array($ci->uri->segment(1), ['BahanHabisPakai']) ? 'style="display:block"' : '') ?>>
                <li class="<?= ($ci->uri->segment(2) == 'stok') ? 'active' : ''; ?>"><a href="<?= base_url();?>BahanHabisPakai/stok"><i class="fa fa-circle-o"></i> Stok</a></li>
                <li class="<?= (
                        $ci->uri->segment(2) == 'pembelian' ||
                        $ci->uri->segment(2) == 'editPembelian' ||
                        $ci->uri->segment(2) == 'tambahPembelian'
                ) ? 'active' : ''; ?>"><a href="<?= base_url();?>BahanHabisPakai/pembelian"><i class="fa fa-circle-o"></i> Pembelian</a></li>
                <li class="<?= ($ci->uri->segment(2) == 'gudang') ? 'active' : ''; ?>"><a href="<?= base_url();?>BahanHabisPakai/gudang"><i class="fa fa-circle-o"></i> Gudang</a></li>
                <li class="<?= ($ci->uri->segment(2) == 'mutasi') ? 'active' : ''; ?>"><a href="<?= base_url();?>BahanHabisPakai/mutasi"><i class="fa fa-circle-o"></i> Mutasi</a></li>
            </ul>
        </li>
<?php } ?>

<?php function master_data_hak_akes() { $ci =& get_instance(); ?>
    <li class="treeview <?= (
        $ci->uri->segment(1) == 'Dokter' ||
        $ci->uri->segment(1) == 'KabagFarmasi' ||
        $ci->uri->segment(1) == 'KabagLayananMedis' ||
        $ci->uri->segment(1) == 'KabagKeuangan' ||
        $ci->uri->segment(1) == 'AdminObat' ||
        $ci->uri->segment(1) == 'Admin' ||
        $ci->uri->segment(1) == 'AdminKeuangan' ||
        $ci->uri->segment(1) == 'Laboratorium' ||
        $ci->uri->segment(1) == 'Ekg' ||
        $ci->uri->segment(1) == 'Spirometri' ||
        $ci->uri->segment(1) == 'Perawat' ||
        $ci->uri->segment(1) == 'Apoteker' ||
        $ci->uri->segment(1) == 'Kasir' ||
        $ci->uri->segment(1) == 'FrontOffice' ||
        ($ci->uri->segment(1) == 'User' && $ci->uri->segment(2) == 'adduser')

    ) ? 'active menu-open' : ''; ?>">
        <a href="#">
            <i class="fa fa-circle-o"></i> Hak Akses <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
        </a>
        <ul class="treeview-menu">
            <li class="<?= $ci->uri->segment(1) == 'Admin' ? 'active' : ''; ?>"><a href="<?= base_url();?>Admin"><i class="fa fa-circle-o"></i> Admin</a></li>
            <li class="<?= $ci->uri->segment(1) == 'Pimpinan' ? 'active' : ''; ?>"><a href="<?= base_url();?>Pimpinan"><i class="fa fa-circle-o"></i> Pimpinan</a></li>
            <li class="<?= $ci->uri->segment(1) == 'Dokter' ? 'active' : ''; ?>"><a href="<?= base_url();?>Dokter"><i class="fa fa-circle-o"></i> Dokter</a></li>
            <li class="<?= $ci->uri->segment(1) == 'Perawat' ? 'active' : ''; ?>"><a href="<?= base_url();?>Perawat"><i class="fa fa-circle-o"></i> Perawat</a></li>
            <li class="<?= $ci->uri->segment(1) == 'KabagFarmasi' ? 'active' : ''; ?>"><a href="<?= base_url();?>KabagFarmasi"><i class="fa fa-circle-o"></i> Kabag Farmasi</a></li>
            <li class="<?= $ci->uri->segment(1) == 'KabagLayananMedis' ? 'active' : ''; ?>"><a href="<?= base_url();?>KabagLayananMedis"><i class="fa fa-circle-o"></i> Kabag Layanan Medis</a></li>
            <li class="<?= $ci->uri->segment(1) == 'KabagKeuangan' ? 'active' : ''; ?>"><a href="<?= base_url();?>KabagKeuangan"><i class="fa fa-circle-o"></i> Kabag Keuangan</a></li>
            <li class="<?= $ci->uri->segment(1) == 'Apoteker' ? 'active' : ''; ?>"><a href="<?= base_url();?>Apoteker"><i class="fa fa-circle-o"></i> Apoteker</a></li>
            <li class="<?= $ci->uri->segment(1) == 'Gizi' ? 'active' : ''; ?>"><a href="<?= base_url();?>Gizi"><i class="fa fa-circle-o"></i> Gizi</a></li>
            <li class="<?= $ci->uri->segment(1) == 'AdminObat' ? 'active' : ''; ?>"><a href="<?= base_url();?>AdminObat"><i class="fa fa-circle-o"></i> Admin Obat</a></li>
            <li class="<?= $ci->uri->segment(1) == 'FrontOffice' ? 'active' : ''; ?>"><a href="<?= base_url();?>FrontOffice"><i class="fa fa-circle-o"></i> Front Office</a></li>
            <li class="<?= $ci->uri->segment(1) == 'AdminKeuangan' ? 'active' : ''; ?>"><a href="<?= base_url();?>AdminKeuangan"><i class="fa fa-circle-o"></i> Administrasi Keuangan</a></li>
            <li class="<?= $ci->uri->segment(1) == 'Kasir' ? 'active' : ''; ?>"><a href="<?= base_url();?>Kasir"><i class="fa fa-circle-o"></i> Kasir</a></li>
            <li class="<?= $ci->uri->segment(1) == 'Laboratorium' ? 'active' : ''; ?>"><a href="<?= base_url();?>Laboratorium"><i class="fa fa-circle-o"></i> Laboratorium</a></li>
            <li class="<?= $ci->uri->segment(1) == 'Radio' ? 'active' : ''; ?>"><a href="<?= base_url();?>Radio"><i class="fa fa-circle-o"></i> Radiologi</a></li>
            <li class="<?= $ci->uri->segment(1) == 'Ekg' ? 'active' : ''; ?>"><a href="<?= base_url();?>Ekg"><i class="fa fa-circle-o"></i> EKG</a></li>
            <li class="<?= $ci->uri->segment(1) == 'Spirometri' ? 'active' : ''; ?>"><a href="<?= base_url();?>Spirometri"><i class="fa fa-circle-o"></i> Spirometri</a></li>
        </ul>
    </li>
<?php } ?>

<?php function master_data_pasien() { $ci =& get_instance(); ?>
    <li class="<?= $ci->uri->segment(1) == 'Pasien' ? 'active' : ''; ?>"><a href="<?= base_url();?>Pasien"><i class="fa fa-circle-o"></i> Pasien</a></li>
<?php } ?>

<?php function master_data_tarif_tindakan() { $ci =& get_instance(); ?>
    <li class="<?= $ci->uri->segment(1) == 'TarifTindakan' ? 'active' : ''; ?>"><a href="<?= base_url();?>TarifTindakan/listTarifTindakan"><i class="fa fa-circle-o"></i> Tarif Tindakan</a></li>
<?php } ?>

<?php function master_data_other() { $ci =& get_instance(); ?>
    <li class="<?= $ci->uri->segment(1) == 'Pasien' ? 'active' : ''; ?>"><a href="<?= base_url();?>Pasien"><i class="fa fa-circle-o"></i> Pasien</a></li>
    <li class="<?= $ci->uri->segment(1) == 'JenisPendaftaran' ? 'active' : ''; ?>"><a href="<?= base_url();?>JenisPendaftaran"><i class="fa fa-circle-o"></i> Jenis Pendaftaran</a></li>
    <li class="<?= seq(1, 'Pengeluaran') && (seq(2, 'list_item') || seq(2, 'create_item') || seq(2, 'edit_item')) ? 'active' : ''; ?>"><a href="<?= base_url();?>Pengeluaran/list_item"><i class="fa fa-circle-o"></i> Item Pengeluaran</a></li>
    <li class="<?= $ci->uri->segment(2) == 'detailklinik' ? 'active' : ''; ?>"><a href="<?= base_url();?>User/detailklinik"><i class="fa fa-circle-o"></i> Data Klinik</a></li>
<?php } ?>

<?php function master_data_hospital() { $ci =& get_instance(); ?>
    <li class="treeview <?= ($ci->uri->segment(1) == 'master' && starts_with($ci->uri->segment(2), 'Bed')) ? 'active menu-open' : ''; ?>">
        <a href="#">
            <i class="fa fa-circle-o"></i> Rawat Inap <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
        </a>
        <ul class="treeview-menu">
            <li class="<?= $ci->uri->segment(2) == 'Bed' ? 'active' : ''; ?>"><a href="<?= base_url();?>master/Bed/status"><i class="fa fa-circle-o"></i> Bed</a></li>
            <li class="<?= $ci->uri->segment(2) == 'Gizi' ? 'active' : ''; ?>"><a href="<?= base_url();?>master/Gizi"><i class="fa fa-circle-o"></i> Gizi</a></li>
        </ul>
    </li>
    <li class="<?= ($ci->uri->segment(1) == 'master' && starts_with($ci->uri->segment(2), 'item')) ? 'active' : ''; ?>">
        <a href="<?= base_url();?>master/itemcategory"><i class="fa fa-circle-o"></i> Inventori</a>
    </li>
<?php } ?>

<?php function apotek($u) { $ci =& get_instance(); ?>
    <?php if (apoteker($u)): ?>
        <li class="treeview <?= seq(1, 'apotek') && (seq(2, 'list_input_resep') || seq(2, 'input_resep') || seq(2, 'resep_n') || seq(2, 'resep_nota')) ? 'active menu-open' : ''; ?>">
            <a href="#">
                <i class="fa fa-circle-o"></i> Resep Rawat Jalan <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
            </a>
            <ul class="treeview-menu">
                <li class="<?= (
                    $ci->uri->segment(2) == 'list_input_resep' ||
                    $ci->uri->segment(2) == 'input_resep'
                ) ? 'active' : ''; ?>"><a href="<?= base_url();?>apotek/list_input_resep"><i class="fa fa-circle-o"></i> Input Resep</a></li>
                <li class="<?= ($ci->uri->segment(1) == 'apotek' && (
                        $ci->uri->segment(2) == 'resep_n' ||
                        $ci->uri->segment(2) == 'resep_nota'
                    )) ? 'active' : ''; ?>"><a href="<?= base_url();?>apotek/resep_n"><i class="fa fa-circle-o"></i> Resep</a></li>
            </ul>
        </li>

        <li class="treeview <?= seq(1, 'apotek') && (seq(2, 'list_resep_ranap') || seq(2, 'input_resep_ranap') || seq(2, 'list_resep_transfer') || seq(2, 'input_resep_transfer') || seq(2, 'detail_resep_ranap')) ? 'active menu-open' : ''; ?>">
            <a href="#">
                <i class="fa fa-circle-o"></i> Resep Non Rawat Jalan <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
            </a>
            <ul class="treeview-menu">
                <li class="<?= (
                    $ci->uri->segment(2) == 'list_resep_ranap' ||
                    $ci->uri->segment(2) == 'input_resep_ranap'
                ) ? 'active' : ''; ?>"><a href="<?= base_url();?>apotek/list_resep_ranap"><i class="fa fa-circle-o"></i> Resep Ranap</a></li>
                <li class="<?= (
                    $ci->uri->segment(2) == 'list_resep_transfer' ||
                    $ci->uri->segment(2) == 'input_resep_transfer' ||
                    $ci->uri->segment(2) == 'detail_resep_ranap'
                ) && seq(3, 'OK') ? 'active' : ''; ?>"><a href="<?= base_url();?>apotek/list_resep_transfer/OK"><i class="fa fa-circle-o"></i> Resep OK</a></li>
                <li class="<?= (
                    $ci->uri->segment(2) == 'list_resep_transfer' ||
                    $ci->uri->segment(2) == 'input_resep_transfer' ||
                    $ci->uri->segment(2) == 'detail_resep_ranap'
                ) && seq(3, 'VK') ? 'active' : ''; ?>"><a href="<?= base_url();?>apotek/list_resep_transfer/VK"><i class="fa fa-circle-o"></i> Resep VK</a></li>
                <li class="<?= (
                    $ci->uri->segment(2) == 'list_resep_transfer' ||
                    $ci->uri->segment(2) == 'input_resep_transfer' ||
                    $ci->uri->segment(2) == 'detail_resep_ranap'
                ) && seq(3, 'HCU') ? 'active' : ''; ?>"><a href="<?= base_url();?>apotek/list_resep_transfer/HCU"><i class="fa fa-circle-o"></i> Resep HCU</a></li>
                <li class="<?= (
                    $ci->uri->segment(2) == 'list_resep_transfer' ||
                    $ci->uri->segment(2) == 'input_resep_transfer' ||
                    $ci->uri->segment(2) == 'detail_resep_ranap'
                ) && seq(3, 'Perinatologi') ? 'active' : ''; ?>"><a href="<?= base_url();?>apotek/list_resep_transfer/Perinatologi"><i class="fa fa-circle-o"></i> Resep Perinatologi</a></li>
            </ul>
        </li>

        <li class="treeview <?= seq(1, 'apotek') && (seq(2, 'resep') || seq(2, 'rekap_resep_ranap') || seq(2, 'rekap_resep_transfer')) ? 'active menu-open' : ''; ?>">
            <a href="#">
                <i class="fa fa-circle-o"></i> Rekapitulasi Resep <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
            </a>
            <ul class="treeview-menu">
                <li class="<?= ($ci->uri->segment(1) == 'apotek' && $ci->uri->segment(2) == 'resep') ? 'active' : ''; ?>"><a href="<?= base_url();?>apotek/resep"><i class="fa fa-circle-o"></i> Rekapitulasi Resep Rajal</a></li>
                <li class="<?= seq(2, 'rekap_resep_ranap') ? 'active' : ''; ?>"><a href="<?= base_url();?>apotek/rekap_resep_ranap"><i class="fa fa-circle-o"></i> Rekapitulasi Resep Ranap</a></li>
                <li class="<?= seq(2, 'rekap_resep_transfer') && seq(3, 'OK') ? 'active' : ''; ?>"><a href="<?= base_url();?>apotek/rekap_resep_transfer/OK"><i class="fa fa-circle-o"></i> Rekapitulasi Resep OK</a></li>
                <li class="<?= seq(2, 'rekap_resep_transfer') && seq(3, 'VK') ? 'active' : ''; ?>"><a href="<?= base_url();?>apotek/rekap_resep_transfer/VK"><i class="fa fa-circle-o"></i> Rekapitulasi Resep VK</a></li>
                <li class="<?= seq(2, 'rekap_resep_transfer') && seq(3, 'HCU') ? 'active' : ''; ?>"><a href="<?= base_url();?>apotek/rekap_resep_transfer/HCU"><i class="fa fa-circle-o"></i> Rekapitulasi Resep HCU</a></li>
                <li class="<?= seq(2, 'rekap_resep_transfer') && seq(3, 'Perinatologi') ? 'active' : ''; ?>"><a href="<?= base_url();?>apotek/rekap_resep_transfer/Perinatologi"><i class="fa fa-circle-o"></i> Rekap Resep Perinatologi</a></li>
            </ul>
        </li>

        <li class="<?= (
            $ci->uri->segment(2) == 'resep_n' ||
            $ci->uri->segment(2) == 'resep_nota'
        ) ? 'active' : ''; ?>"><a href="<?= base_url();?>apotek/resep_n"><i class="fa fa-circle-o"></i> Resep</a>
        </li>
        <li class="<?= $ci->uri->segment(2) == 'resep_luar' ? 'active' : ''; ?>"><a href="<?= base_url();?>apotek/resep_luar"><i class="fa fa-circle-o"></i> Resep Luar</a></li>
        <li class="<?= $ci->uri->segment(2) == 'rekapitulasi_resep_luar' ? 'active' : ''; ?>"><a href="<?= base_url();?>apotek/rekapitulasi_resep_luar"><i class="fa fa-circle-o"></i> Rekapitulasi Resep Luar</a></li>
        <li class="<?= $ci->uri->segment(2) == 'obat_bebas' ? 'active' : ''; ?>"><a href="<?=base_url();?>apotek/obat_bebas"><i class="fa fa-circle-o"></i> Obat Bebas</a></li>
        <!--<li class="<? //$ci->uri->segment(2) == 'rekapitulasi_obat_bebas' ? 'active' : ''; ?>"><a href="<?//= base_url();?>apotek/rekapitulasi_obat_bebas"><i class="fa fa-circle-o"></i> Rekapitulasi Obat Bebas</a></li>-->
        <!-- <li class="<?// $ci->uri->segment(2) == 'obat_internal' ? 'active' : ''; ?>"><a href="<?//= base_url();?>apotek/obat_internal"><i class="fa fa-circle-o"></i> Penj. Obat Internal</a></li> -->
        <!-- <li class"--><?// $ci->uri->segment(2) == 'rekapitulasi_obat_internal' ? 'active' : ''; ?><!--"><a href="--><?//= base_url();?><!--apotek/rekapitulasi_obat_internal"><i class="fa fa-circle-o"></i> Rekap. Penj. Obat Internal</a></li> -->
        <!--<li class="--><?//= seq(1, 'bpjs') && seq(2, 'sep') && seq(3, 'update_pulang') ? 'active' : ''; ?><!--"><a href="--><?//= base_url();?><!--bpjs/sep/update_pulang"><i class="fa fa-circle-o"></i> Update Pulang SEP</a></li>-->
    <?php elseif (admin_obat($u)): ?>
        <li class="treeview  <?= (in_array($ci->uri->segment(1), ['apotek']) ? 'menu-open' : '') ?>">
            <a href="#">
                <i class="fa fa-circle-o"></i> Obat <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
            </a>
            <ul class="treeview-menu" <?= (in_array($ci->uri->segment(1), ['apotek']) ? 'style="display:block"' : '') ?>>
                <li class="<?= ($ci->uri->segment(1) == 'apotek' && $ci->uri->segment(2) == 'stokObat') ? 'active' : ''; ?>"><a href="<?= base_url();?>apotek/stokObat"><i class="fa fa-circle-o"></i> Stok Obat</a></li>
                <li class="<?= ($ci->uri->segment(1) == 'apotek' && (
                        $ci->uri->segment(2) == 'pembelian' ||
                        $ci->uri->segment(2) == 'editPembelianObat' ||
                        $ci->uri->segment(2) == 'tambahPembelianObat'
                    )) ? 'active' : ''; ?>"><a href="<?= base_url();?>apotek/pembelian"><i class="fa fa-circle-o"></i> Pembelian</a></li>
                <li class="<?= ($ci->uri->segment(1) == 'apotek' && $ci->uri->segment(2) == 'gudang') ? 'active' : ''; ?>"><a href="<?= base_url();?>apotek/gudang"><i class="fa fa-circle-o"></i> Gudang</a></li>
                <li class="<?= ($ci->uri->segment(1) == 'apotek' && $ci->uri->segment(2) == 'mutasi') ? 'active' : ''; ?>"><a href="<?= base_url();?>apotek/mutasi"><i class="fa fa-circle-o"></i> Mutasi</a></li>
            </ul>
        </li>
        <li class="treeview  <?= (in_array($ci->uri->segment(1), ['BahanHabisPakai']) ? 'menu-open' : '') ?>">
            <a href="#">
                <i class="fa fa-circle-o"></i> Bahan Habis Pakai <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
            </a>
            <ul class="treeview-menu" <?= (in_array($ci->uri->segment(1), ['BahanHabisPakai']) ? 'style="display:block"' : '') ?>>
                <li class="<?= ($ci->uri->segment(1) == 'BahanHabisPakai' && $ci->uri->segment(2) == 'stok') ? 'active' : ''; ?>"><a href="<?= base_url();?>BahanHabisPakai/stok"><i class="fa fa-circle-o"></i> Stok</a></li>
                <li class="<?= ($ci->uri->segment(1) == 'BahanHabisPakai' && (
                        $ci->uri->segment(2) == 'pembelian' ||
                        $ci->uri->segment(2) == 'editPembelian' ||
                        $ci->uri->segment(2) == 'tambahPembelian'
                    )) ? 'active' : ''; ?>"><a href="<?= base_url();?>BahanHabisPakai/pembelian"><i class="fa fa-circle-o"></i> Pembelian</a></li>
                <li class="<?= ($ci->uri->segment(1) == 'BahanHabisPakai' && $ci->uri->segment(2) == 'gudang') ? 'active' : ''; ?>"><a href="<?= base_url();?>BahanHabisPakai/gudang"><i class="fa fa-circle-o"></i> Gudang</a></li>
                <li class="<?= ($ci->uri->segment(1) == 'BahanHabisPakai' && $ci->uri->segment(2) == 'mutasi') ? 'active' : ''; ?>"><a href="<?= base_url();?>BahanHabisPakai/mutasi"><i class="fa fa-circle-o"></i> Mutasi</a></li>
            </ul>
        </li>
    <?php else: ?>
        <li class="treeview <?= ($ci->uri->segment(1) == 'apotek'
            || $ci->uri->segment(1) == 'BahanHabisPakai'
            || $ci->uri->segment(1) == 'HistoryObat'
            || $ci->uri->segment(1) == 'HistoryBahanHabisPakai') ? 'active menu-open' : ''; ?>">
            <a href="#">
                <i class="fa fa-medkit"></i> <span>Apotek</span>
                <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
            </a>
            <ul class="treeview-menu">
                <li class="treeview  <?= (in_array($ci->uri->segment(1), ['apotek', 'HistoryObat']) ? 'menu-open' : '') ?>">
                    <a href="#">
                        <i class="fa fa-circle-o"></i> Obat <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
                    </a>
                    <ul class="treeview-menu" <?= (in_array($ci->uri->segment(1), ['apotek', 'HistoryObat']) ? 'style="display:block"' : '') ?>>
                        <li class="<?= ($ci->uri->segment(1) == 'apotek' && $ci->uri->segment(2) == 'stokObat') ? 'active' : ''; ?>"><a href="<?= base_url();?>apotek/stokObat"><i class="fa fa-circle-o"></i> Stok Obat</a></li>
                        <li class="<?= ($ci->uri->segment(1) == 'apotek' && (
                            $ci->uri->segment(2) == 'pembelian' ||
                            $ci->uri->segment(2) == 'editPembelianObat' ||
                            $ci->uri->segment(2) == 'tambahPembelianObat'
                        )) ? 'active' : ''; ?>"><a href="<?= base_url();?>apotek/pembelian"><i class="fa fa-circle-o"></i> Pembelian</a></li>
                        <li class="<?= ($ci->uri->segment(1) == 'apotek' && $ci->uri->segment(2) == 'gudang') ? 'active' : ''; ?>"><a href="<?= base_url();?>apotek/gudang"><i class="fa fa-circle-o"></i> Gudang</a></li>
                        <li class="<?= ($ci->uri->segment(1) == 'apotek' && $ci->uri->segment(2) == 'mutasi') ? 'active' : ''; ?>"><a href="<?= base_url();?>apotek/mutasi"><i class="fa fa-circle-o"></i> Mutasi</a></li>
                        <li class="<?= ($ci->uri->segment(1) == 'apotek' && $ci->uri->segment(2) == 'resep') ? 'active' : ''; ?>"><a href="<?= base_url();?>apotek/resep"><i class="fa fa-circle-o"></i> Rekapitulasi Resep</a></li>
                        <li class="<?= ($ci->uri->segment(1) == 'apotek' && (
                            $ci->uri->segment(2) == 'resep_n' ||
                            $ci->uri->segment(2) == 'resep_nota'
                        )) ? 'active' : ''; ?>"><a href="<?= base_url();?>apotek/resep_n"><i class="fa fa-circle-o"></i> Resep</a></li>
                        <li class="treeview  <?= (in_array($ci->uri->segment(1), ['HistoryObat']) ? 'menu-open' : '') ?>">
                            <a href="#">
                                <i class="fa fa-circle-o"></i> Persediaan <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
                            </a>
                            <ul class="treeview-menu" <?= (in_array($ci->uri->segment(1), ['HistoryObat']) ? 'style="display:block"' : '') ?>>
                                <li class="<?= ($ci->uri->segment(2) == 'stokAwal') ? 'active' : ''; ?>"><a href="<?= base_url();?>HistoryObat/stokAwal"><i class="fa fa-circle-o"></i> Stok Awal</a></li>
                                <li class="<?= ($ci->uri->segment(2) == 'stokAkhir') ? 'active' : ''; ?>"><a href="<?= base_url();?>HistoryObat/stokAkhir"><i class="fa fa-circle-o"></i> Stok Akhir</a></li>
                                <li class="<?= ($ci->uri->segment(2) == 'riwayat') ? 'active' : ''; ?>"><a href="<?= base_url();?>HistoryObat/riwayat"><i class="fa fa-circle-o"></i> Riwayat Transaksi</a></li>
                            </ul>
                        </li>
                    </ul>
                </li>
                <li class="treeview  <?= (in_array($ci->uri->segment(1), ['BahanHabisPakai', 'HistoryBahanHabisPakai']) ? 'menu-open' : '') ?>">
                    <a href="#">
                        <i class="fa fa-circle-o"></i> Bahan Habis Pakai <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
                    </a>
                    <ul class="treeview-menu" <?= (in_array($ci->uri->segment(1), ['BahanHabisPakai', 'HistoryBahanHabisPakai']) ? 'style="display:block"' : '') ?>>
                        <li class="<?= ($ci->uri->segment(1) == 'BahanHabisPakai' && $ci->uri->segment(2) == 'stok') ? 'active' : ''; ?>"><a href="<?= base_url();?>BahanHabisPakai/stok"><i class="fa fa-circle-o"></i> Stok</a></li>
                        <li class="<?= ($ci->uri->segment(1) == 'BahanHabisPakai' && (
                            $ci->uri->segment(2) == 'pembelian' ||
                            $ci->uri->segment(2) == 'editPembelian' ||
                            $ci->uri->segment(2) == 'tambahPembelian'
                        )) ? 'active' : ''; ?>"><a href="<?= base_url();?>BahanHabisPakai/pembelian"><i class="fa fa-circle-o"></i> Pembelian</a></li>
                        <li class="<?= ($ci->uri->segment(1) == 'BahanHabisPakai' && $ci->uri->segment(2) == 'gudang') ? 'active' : ''; ?>"><a href="<?= base_url();?>BahanHabisPakai/gudang"><i class="fa fa-circle-o"></i> Gudang</a></li>
                        <li class="<?= ($ci->uri->segment(1) == 'BahanHabisPakai' && $ci->uri->segment(2) == 'mutasi') ? 'active' : ''; ?>"><a href="<?= base_url();?>BahanHabisPakai/mutasi"><i class="fa fa-circle-o"></i> Mutasi</a></li>
                        <li class="treeview  <?= (in_array($ci->uri->segment(1), ['HistoryBahanHabisPakai']) ? 'menu-open' : '') ?>">
                            <a href="#">
                                <i class="fa fa-circle-o"></i> Persediaan <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
                            </a>
                            <ul class="treeview-menu" <?= (in_array($ci->uri->segment(1), ['HistoryBahanHabisPakai']) ? 'style="display:block"' : '') ?>>
                                <li class="<?= ($ci->uri->segment(2) == 'stokAwal') ? 'active' : ''; ?>"><a href="<?= base_url();?>HistoryBahanHabisPakai/stokAwal"><i class="fa fa-circle-o"></i> Stok Awal</a></li>
                                <li class="<?= ($ci->uri->segment(2) == 'stokAkhir') ? 'active' : ''; ?>"><a href="<?= base_url();?>HistoryBahanHabisPakai/stokAkhir"><i class="fa fa-circle-o"></i> Stok Akhir</a></li>
                                <li class="<?= ($ci->uri->segment(2) == 'riwayat') ? 'active' : ''; ?>"><a href="<?= base_url();?>HistoryBahanHabisPakai/riwayat"><i class="fa fa-circle-o"></i> Riwayat Transaksi</a></li>
                            </ul>
                        </li>
                    </ul>
                </li>
            </ul>
        </li>
    <?php endif; ?>
<?php } ?>

<?php function administrasi_periksa($u) { $ci =& get_instance(); ?>
    <li class="treeview <?= ($ci->uri->segment(1) == 'Administrasi' || $ci->uri->segment(1) == 'apotek' || $ci->uri->segment(1) == 'Rekapitulasi') ? 'active menu-open' : ''; ?>">
        <a href="#">
            <i class="fa fa-dollar"></i> Billing <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
        </a>
        <ul class="treeview-menu">
            <?php
            if (superadmin($u) || admin($u) || kasir($u) || kabag_farmasi($u) || kabag_keuangan($u)) {
                billing_kasir_rekap_resep();
            }
            billing_obat_non_periksa();
            ?>
        </ul>
    </li>
<?php } ?>

<?php function billing_kasir_rekap_resep() { $ci =& get_instance(); ?>
<!--    <li class="--><?//= (
//        $ci->uri->segment(2) == 'listPasienSelesaiPeriksa' ||
//        $ci->uri->segment(2) == 'nota' ||
//        $ci->uri->segment(2) == 'nota_submit'
//    ) ? 'active' : ''; ?><!--"><a href="--><?//= base_url();?><!--Administrasi/listPasienSelesaiPeriksa"><i class="fa fa-circle-o"></i> Kasir</a></li>-->
    <li class="<?= $ci->uri->segment(1) == 'Administrasi' && (
        $ci->uri->segment(2) == 'kasir_all' ||
        $ci->uri->segment(2) == 'nota' ||
        $ci->uri->segment(2) == 'nota_submit'
    ) ? 'active' : ''; ?>"><a href="<?= base_url();?>Administrasi/kasir_all"><i class="fa fa-circle-o"></i> Kasir All</a></li>
    <li class="<?= $ci->uri->segment(2) == 'rekapitulasi' ? 'active' : ''; ?>"><a href="<?= base_url();?>Administrasi/rekapitulasi"><i class="fa fa-circle-o"></i> Rekapitulasi Billing</a></li>
    <li class="<?= $ci->uri->segment(1) == 'Rekapitulasi' && $ci->uri->segment(2) == 'kasir_all' ? 'active' : ''; ?>"><a href="<?= base_url();?>Rekapitulasi/kasir_all"><i class="fa fa-circle-o"></i> Rekapitulasi Kasir All</a></li>

    <li class="treeview <?= seq(1, 'apotek') && (seq(2, 'list_input_resep') || seq(2, 'input_resep') || seq(2, 'resep_n') || seq(2, 'resep_nota')) ? 'active menu-open' : ''; ?>">
        <a href="#">
            <i class="fa fa-circle-o"></i> Resep Rawat Jalan <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
        </a>
        <ul class="treeview-menu">
            <li class="<?= (
                $ci->uri->segment(2) == 'list_input_resep' ||
                $ci->uri->segment(2) == 'input_resep'
            ) ? 'active' : ''; ?>"><a href="<?= base_url();?>apotek/list_input_resep"><i class="fa fa-circle-o"></i> Input Resep</a></li>
            <li class="<?= ($ci->uri->segment(1) == 'apotek' && (
                    $ci->uri->segment(2) == 'resep_n' ||
                    $ci->uri->segment(2) == 'resep_nota'
                )) ? 'active' : ''; ?>"><a href="<?= base_url();?>apotek/resep_n"><i class="fa fa-circle-o"></i> Resep</a></li>
        </ul>
    </li>

    <li class="treeview <?= seq(1, 'apotek') && (seq(2, 'list_resep_ranap') || seq(2, 'input_resep_ranap') || seq(2, 'list_resep_transfer') || seq(2, 'input_resep_transfer') || seq(2, 'detail_resep_ranap')) ? 'active menu-open' : ''; ?>">
        <a href="#">
            <i class="fa fa-circle-o"></i> Resep Non Rawat Jalan <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
        </a>
        <ul class="treeview-menu">
            <li class="<?= (
                $ci->uri->segment(2) == 'list_resep_ranap' ||
                $ci->uri->segment(2) == 'input_resep_ranap'
            ) ? 'active' : ''; ?>"><a href="<?= base_url();?>apotek/list_resep_ranap"><i class="fa fa-circle-o"></i> Resep Ranap</a></li>
            <li class="<?= (
                $ci->uri->segment(2) == 'list_resep_transfer' ||
                $ci->uri->segment(2) == 'input_resep_transfer' ||
                $ci->uri->segment(2) == 'detail_resep_ranap'
            ) && seq(3, 'OK') ? 'active' : ''; ?>"><a href="<?= base_url();?>apotek/list_resep_transfer/OK"><i class="fa fa-circle-o"></i> Resep OK</a></li>
            <li class="<?= (
                $ci->uri->segment(2) == 'list_resep_transfer' ||
                $ci->uri->segment(2) == 'input_resep_transfer' ||
                $ci->uri->segment(2) == 'detail_resep_ranap'
            ) && seq(3, 'VK') ? 'active' : ''; ?>"><a href="<?= base_url();?>apotek/list_resep_transfer/VK"><i class="fa fa-circle-o"></i> Resep VK</a></li>
            <li class="<?= (
                $ci->uri->segment(2) == 'list_resep_transfer' ||
                $ci->uri->segment(2) == 'input_resep_transfer' ||
                $ci->uri->segment(2) == 'detail_resep_ranap'
            ) && seq(3, 'HCU') ? 'active' : ''; ?>"><a href="<?= base_url();?>apotek/list_resep_transfer/HCU"><i class="fa fa-circle-o"></i> Resep HCU</a></li>
            <li class="<?= (
                $ci->uri->segment(2) == 'list_resep_transfer' ||
                $ci->uri->segment(2) == 'input_resep_transfer' ||
                $ci->uri->segment(2) == 'detail_resep_ranap'
            ) && seq(3, 'Perinatologi') ? 'active' : ''; ?>"><a href="<?= base_url();?>apotek/list_resep_transfer/Perinatologi"><i class="fa fa-circle-o"></i> Resep Perinatologi</a></li>
        </ul>
    </li>

    <li class="treeview <?= seq(1, 'apotek') && (seq(2, 'resep') || seq(2, 'rekap_resep_ranap') || seq(2, 'rekap_resep_transfer')) ? 'active menu-open' : ''; ?>">
        <a href="#">
            <i class="fa fa-circle-o"></i> Rekapitulasi Resep <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
        </a>
        <ul class="treeview-menu">
            <li class="<?= ($ci->uri->segment(1) == 'apotek' && $ci->uri->segment(2) == 'resep') ? 'active' : ''; ?>"><a href="<?= base_url();?>apotek/resep"><i class="fa fa-circle-o"></i> Rekapitulasi Resep Rajal</a></li>
            <li class="<?= seq(2, 'rekap_resep_ranap') ? 'active' : ''; ?>"><a href="<?= base_url();?>apotek/rekap_resep_ranap"><i class="fa fa-circle-o"></i> Rekapitulasi Resep Ranap</a></li>
            <li class="<?= seq(2, 'rekap_resep_transfer') && seq(3, 'OK') ? 'active' : ''; ?>"><a href="<?= base_url();?>apotek/rekap_resep_transfer/?current_place_type=OK"><i class="fa fa-circle-o"></i> Rekapitulasi Resep OK</a></li>
            <li class="<?= seq(2, 'rekap_resep_transfer') && seq(3, 'VK') ? 'active' : ''; ?>"><a href="<?= base_url();?>apotek/rekap_resep_transfer/?current_place_type=VK"><i class="fa fa-circle-o"></i> Rekapitulasi Resep VK</a></li>
            <li class="<?= seq(2, 'rekap_resep_transfer') && seq(3, 'HCU') ? 'active' : ''; ?>"><a href="<?= base_url();?>apotek/rekap_resep_transfer/?current_place_type=HCU"><i class="fa fa-circle-o"></i> Rekapitulasi Resep HCU</a></li>
            <li class="<?= seq(2, 'rekap_resep_transfer') && seq(3, 'Perinatologi') ? 'active' : ''; ?>"><a href="<?= base_url();?>apotek/rekap_resep_transfer/?current_place_type=Perinatologi"><i class="fa fa-circle-o"></i> Rekap Resep Perinatologi</a></li>
        </ul>
    </li>

    <li class="<?= (
        $ci->uri->segment(2) == 'resep_n' ||
        $ci->uri->segment(2) == 'resep_nota'
    ) ? 'active' : ''; ?>"><a href="<?= base_url();?>apotek/resep_n"><i class="fa fa-circle-o"></i> Resep</a></li>
<?php } ?>

<?php function billing_obat_non_periksa() { $ci =& get_instance(); ?>
    <li class="<?= $ci->uri->segment(2) == 'resep_luar' ? 'active' : ''; ?>"><a href="<?= base_url();?>apotek/resep_luar"><i class="fa fa-circle-o"></i> Resep Luar</a></li>
    <li class="<?= $ci->uri->segment(2) == 'rekapitulasi_resep_luar' ? 'active' : ''; ?>"><a href="<?= base_url();?>apotek/rekapitulasi_resep_luar"><i class="fa fa-circle-o"></i> Rekapitulasi Resep Luar</a></li>
    
<?php } ?>

<?php function laporan_harian_shift() { $ci =& get_instance(); ?>
    <li class="treeview <?= seq(1, 'LaporanHarianShift') ? 'active menu-open' : ''; ?>">
        <a href="#">
            <i class="fa fa-first-aid"></i> <span>Laporan Harian Shift</span>
            <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
        </a>
        <ul class="treeview-menu">
            <li class="<?= $ci->uri->segment(2) == 'laporan_harian' ? 'active' : ''; ?>"><a href="<?= base_url();?>LaporanHarianShift/laporan_harian"><i class="fa fa-circle-o"></i> Laporan Harian</a></li>
        </ul>
    </li>
<?php } ?>

<?php function surat_ket_sehat() { $ci =& get_instance(); ?>
    <li class="<?= ($ci->uri->segment(2) == 'listPasienSelesai_sehat') ? 'active' : ''; ?>">
        <a href="<?= base_url();?>administrasi/listPasienSelesai_sehat">
            <i class="fa fa-newspaper-o"></i> <span>Surat Ket. Sehat</span>
        </a>
    </li>
<?php } ?>

<?php function surat_ket_sakit() { $ci =& get_instance(); ?>
    <li class="<?= ($ci->uri->segment(2) == 'listPasienSelesaiPeriksa_sakit') ? 'active' : ''; ?>">
        <a href="<?= base_url();?>administrasi/listPasienSelesaiPeriksa_sakit">
            <i class="fa fa-newspaper-o"></i> <span>Surat Ket. Sakit</span>
        </a>
    </li>
<?php } ?>

<?php function surat_ket_rujuk() { $ci =& get_instance(); ?>
    <li class="<?= ($ci->uri->segment(2) == 'listPasienSelesaiPeriksa_rujuk') ? 'active' : ''; ?>">
        <a href="<?= base_url();?>administrasi/listPasienSelesaiPeriksa_rujuk">
            <i class="fa fa-newspaper-o"></i> <span>Surat Ket. Rujuk</span>
        </a>
    </li>
<?php } ?>

<?php function surat_ranap() { $ci =& get_instance(); ?>
    <li class="<?= ($ci->uri->segment(2) == 'listPasienSelesaiPeriksa_ranap') ? 'active' : ''; ?>">
        <a href="<?= base_url();?>administrasi/listPasienSelesaiPeriksa_ranap">
            <i class="fa fa-newspaper-o"></i> <span>Surat Pengantar Ranap</span>
        </a>
    </li>
<?php } ?>

<?php function intensif_karyawan() { $ci =& get_instance(); ?>
    <li  class="treeview <?= ($ci->uri->segment(1) == 'dokter' || $ci->uri->segment(1) == 'perawat' || $ci->uri->segment(1) == 'Insentif') ? 'active menu-open' : ''; ?>">
        <a href="#">
            <i class="fa fa-suitcase"></i><span>Insentif Karyawan</span><span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
        </a>
        <ul class="treeview-menu">
            <li class="<?= $ci->uri->segment(2) == 'listInsentifShift' ? 'active' : ''; ?>"><a href="<?= base_url();?>Insentif/listInsentifShift"><i class="fa fa-circle-o"></i> Insentif Shift/Kehadiran</a></li>
            <li class="treeview  <?= (
                seq(2, 'listInsentifDokter') ||
                seq(2, 'listInsentifPerawat') ||
                seq(2, 'listInsentifApoteker') ||
                seq(2, 'DetailInsentifDokter') ||
                seq(2, 'DetailInsentifPerawat') ||
                seq(2, 'DetailInsentifApoteker')
            ) ? 'menu-open' : '' ?>">
                <a href="#">
                    <i class="fa fa-circle-o"></i> Insentif Jasa Medis <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
                </a>
                <ul class="treeview-menu" <?= ((
                    seq(2, 'listInsentifDokter') ||
                    seq(2, 'listInsentifPerawat') ||
                    seq(2, 'listInsentifApoteker') ||
                    seq(2, 'DetailInsentifDokter') ||
                    seq(2, 'DetailInsentifPerawat') ||
                    seq(2, 'DetailInsentifApoteker')
                ) ? 'style="display:block"' : '') ?>>
                    <li class="<?= (seq(2, 'listInsentifDokter') || seq(2, 'DetailInsentifDokter')) ? 'active' : ''; ?>"><a href="<?= base_url();?>Insentif/listInsentifDokter"><i class="fa fa-circle-o"></i>Insentif Dokter</a></li>
                    <li class="<?= (seq(2, 'listInsentifPerawat') || seq(2, 'DetailInsentifPerawat')) ? 'active' : ''; ?>"><a href="<?= base_url();?>Insentif/listInsentifPerawat"><i class="fa fa-circle-o"></i>Insentif Perawat</a></li>
                </ul>
            </li>
            <li class="treeview  <?= ((
                $ci->uri->segment(2) == 'listInsentifDokter' ||
                $ci->uri->segment(2) == 'listInsentifPerawat' ||
                $ci->uri->segment(2) == 'listInsentifApoteker'
            ) ? 'menu-open' : '') ?>">
                <a href="#">
                    <i class="fa fa-circle-o"></i> Insentif Jasa Obat <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
                </a>
                <ul class="treeview-menu" <?= ((
                    $ci->uri->segment(2) == 'listInsentifObatDokter' ||
                    $ci->uri->segment(2) == 'listInsentifObatApoteker' ||
                    $ci->uri->segment(2) == 'listInsentifObatBebasApoteker' ||
                    $ci->uri->segment(2) == 'listInsentifObatKlinik'
                ) ? 'style="display:block"' : '') ?>>
                    <li class="<?= ($ci->uri->segment(2) == 'listInsentifObatDokter') ? 'active' : ''; ?>"><a href="<?= base_url();?>Insentif/listInsentifObatDokter"><i class="fa fa-circle-o"></i>Insentif Dokter</a></li>
                    <li class="<?= ($ci->uri->segment(2) == 'listInsentifObatApoteker') ? 'active' : ''; ?>"><a href="<?= base_url();?>Insentif/listInsentifObatApoteker"><i class="fa fa-circle-o"></i>Insentif Apoteker</a></li>
                    <li class="<?= ($ci->uri->segment(2) == 'listInsentifObatBebasApoteker') ? 'active' : ''; ?>"><a href="<?= base_url();?>Insentif/listInsentifObatBebasApoteker"><i class="fa fa-circle-o"></i>Insentif Obat Bebas Apoteker</a></li>
                    <li class="<?= ($ci->uri->segment(2) == 'listInsentifObatKlinik') ? 'active' : ''; ?>"><a href="<?= base_url();?>Insentif/listInsentifObatKlinik"><i class="fa fa-circle-o"></i>Klinik</a></li>
                </ul>
            </li>
            <li class="<?= $ci->uri->segment(2) == 'listInsentifJasaRacik' ? 'active' : ''; ?>"><a href="<?= base_url();?>Insentif/listInsentifJasaRacik"><i class="fa fa-circle-o"></i> Insentif Jasa Racik</a></li>
        </ul>
    </li>
<?php } ?>

<?php function executive_summary($u) { $ci =& get_instance(); ?>
    <li class="treeview <?= ($ci->uri->segment(1) == 'Laporan') ? 'active menu-open' : ''; ?>">
        <a href="#">
            <i class="fa fa-list"></i> <span>Executive Summary</span><span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
        </a>
        <?php if (superadmin($u)): ?>
            <ul class="treeview-menu">
                <li class="treeview <?= (
                    $ci->uri->segment(2) == 'jumlahKunjungan' ||
                    $ci->uri->segment(2) == 'jumlahPasien' ||
                    $ci->uri->segment(2) == 'jumlahPasienBaru' ||
                    $ci->uri->segment(2) == 'jumlahPasien20' ||
                    $ci->uri->segment(2) == 'rata2Kunjungan' ||
                    $ci->uri->segment(2) == 'rata2Pasien'
                ) ? 'active menu-open' : ''; ?>">
                    <a href="#">
                        <i class="fa fa-circle-o"></i> <span>Pasien</span><span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
                    </a>
                    <ul class="treeview-menu">
                        <li class="<?= $ci->uri->segment(2) == 'jumlahKunjungan' ? 'active' : ''; ?>"><a href="<?= base_url();?>Laporan"><i class="fa fa-caret-right"></i> Kunjungan Pasien</a></li>
                        <li class="<?= $ci->uri->segment(2) == 'jumlahPasien' ? 'active' : ''; ?>"><a href="<?= base_url();?>Laporan/jumlahPasien"><i class="fa fa-caret-right"></i> Jumlah Pasien</a></li>
                        <li class="<?= $ci->uri->segment(2) == 'jumlahPasienBaru' ? 'active' : ''; ?>"><a href="<?= base_url();?>Laporan/jumlahPasienBaru"><i class="fa fa-caret-right"></i> Jumlah Pasien Baru</a></li>
                        <li class="<?= $ci->uri->segment(2) == 'jumlahPasien20' ? 'active' : ''; ?>"><a href="<?= base_url();?>Laporan/jumlahPasien20"><i class="fa fa-caret-right"></i> Top 20 Pasien</a></li>
                        <li class="<?= $ci->uri->segment(2) == 'rata2Kunjungan' ? 'active' : ''; ?>"><a href="<?= base_url();?>Laporan/rata2Kunjungan"><i class="fa fa-caret-right"></i> Rata-Rata Kunjungan</a></li>
                        <li class="<?= $ci->uri->segment(2) == 'rata2Pasien' ? 'active' : ''; ?>"><a href="<?= base_url();?>Laporan/rata2Pasien"><i class="fa fa-caret-right"></i> Rata-Rata Pasien</a></li>
                    </ul>
                </li>

                <li class="<?= $ci->uri->segment(2) == 'performaDokter' ? 'active' : ''; ?>"><a href="<?= base_url();?>Laporan/performaDokter"><i class="fa fa-circle-o"></i> Performa Dokter</a></li>
                <li class="<?= $ci->uri->segment(2) == 'performaPerawat' ? 'active' : ''; ?>"><a href="<?= base_url();?>Laporan/performaPerawat"><i class="fa fa-circle-o"></i> Performa Perawat</a></li>
                <li class="<?= $ci->uri->segment(2) == 'Penyakit' ? 'active' : ''; ?>"><a href="<?= base_url();?>Laporan/Penyakit"><i class="fa fa-circle-o"></i> Jenis Penyakit</a></li>

                <li class="treeview <?= (
                    $ci->uri->segment(2) == 'Obat' ||
                    $ci->uri->segment(2) == 'obat_resep_luar' ||
                    $ci->uri->segment(2) == 'obat_obat_bebas' ||
                    $ci->uri->segment(2) == 'obat_obat_internal'
                ) ? 'active menu-open' : ''; ?>">
                    <a href="#">
                        <i class="fa fa-circle-o"></i> <span>Apotek</span><span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
                    </a>
                    <ul class="treeview-menu">
                        <li class="<?= $ci->uri->segment(2) == 'Obat' ? 'active' : ''; ?>"><a href="<?= base_url();?>Laporan/Obat"><i class="fa fa-caret-right"></i> Resep Internal</a></li>
                        <li class="<?= $ci->uri->segment(2) == 'obat_resep_luar' ? 'active' : ''; ?>"><a href="<?= base_url();?>Laporan/obat_resep_luar"><i class="fa fa-caret-right"></i> Resep Luar</a></li>
                        <li class="<?= $ci->uri->segment(2) == 'obat_obat_bebas' ? 'active' : ''; ?>"><a href="<?= base_url();?>Laporan/obat_obat_bebas"><i class="fa fa-caret-right"></i> Obat Bebas</a></li>
                        <li class="<?= $ci->uri->segment(2) == 'obat_obat_internal' ? 'active' : ''; ?>"><a href="<?= base_url();?>Laporan/obat_obat_internal"><i class="fa fa-caret-right"></i> Obat Internal</a></li>
                    </ul>
                </li>

                <li class="<?= $ci->uri->segment(2) == 'RekamMedis' ? 'active' : ''; ?>"><a href="<?= base_url();?>Laporan/RekamMedis"><i class="fa fa-circle-o"></i> Rekam Medis</a></li>
                <li class="<?= $ci->uri->segment(2) == 'VisitRate' ? 'active' : ''; ?>"><a href="<?= base_url();?>Laporan/VisitRate"><i class="fa fa-circle-o"></i> Visit Rate Pasien</a></li>

            </ul>
        <?php elseif(kabag_farmasi($u)): ?>
            <ul class="treeview-menu">
                <li class="treeview <?= (
                    $ci->uri->segment(2) == 'Obat' ||
                    $ci->uri->segment(2) == 'obat_resep_luar' ||
                    $ci->uri->segment(2) == 'obat_obat_bebas' ||
                    $ci->uri->segment(2) == 'obat_obat_internal'
                ) ? 'active menu-open' : ''; ?>">
                    <a href="#">
                        <i class="fa fa-circle-o"></i> <span>Apotek</span><span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
                    </a>
                    <ul class="treeview-menu">
                        <li class="<?= $ci->uri->segment(2) == 'Obat' ? 'active' : ''; ?>"><a href="<?= base_url();?>Laporan/Obat"><i class="fa fa-caret-right"></i> Resep Internal</a></li>
                        <li class="<?= $ci->uri->segment(2) == 'obat_resep_luar' ? 'active' : ''; ?>"><a href="<?= base_url();?>Laporan/obat_resep_luar"><i class="fa fa-caret-right"></i> Resep Luar</a></li>
                        <li class="<?= $ci->uri->segment(2) == 'obat_obat_bebas' ? 'active' : ''; ?>"><a href="<?= base_url();?>Laporan/obat_obat_bebas"><i class="fa fa-caret-right"></i> Obat Bebas</a></li>
                        <li class="<?= $ci->uri->segment(2) == 'obat_obat_internal' ? 'active' : ''; ?>"><a href="<?= base_url();?>Laporan/obat_obat_internal"><i class="fa fa-caret-right"></i> Obat Internal</a></li>
                    </ul>
                </li>
            </ul>
        <?php elseif(kabag_keuangan($u)): ?>
            <ul class="treeview-menu">
                <li class="<?= $ci->uri->segment(2) == 'performaDokter' ? 'active' : ''; ?>"><a href="<?= base_url();?>Laporan/performaDokter"><i class="fa fa-circle-o"></i> Performa Dokter</a></li>
                <li class="<?= $ci->uri->segment(2) == 'performaPerawat' ? 'active' : ''; ?>"><a href="<?= base_url();?>Laporan/performaPerawat"><i class="fa fa-circle-o"></i> Performa Perawat</a></li>
            </ul>
        <?php elseif(kabag_layanan_medis($u)): ?>
            <ul class="treeview-menu">
                <li class="treeview <?= (
                    $ci->uri->segment(2) == 'jumlahKunjungan' ||
                    $ci->uri->segment(2) == 'jumlahPasien' ||
                    $ci->uri->segment(2) == 'jumlahPasienBaru' ||
                    $ci->uri->segment(2) == 'jumlahPasien20' ||
                    $ci->uri->segment(2) == 'rata2Kunjungan' ||
                    $ci->uri->segment(2) == 'rata2Pasien'
                ) ? 'active menu-open' : ''; ?>">
                    <a href="#">
                        <i class="fa fa-circle-o"></i> <span>Pasien</span><span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
                    </a>
                    <ul class="treeview-menu">
                        <li class="<?= $ci->uri->segment(2) == 'jumlahKunjungan' ? 'active' : ''; ?>"><a href="<?= base_url();?>Laporan"><i class="fa fa-caret-right"></i> Kunjungan Pasien</a></li>
                        <li class="<?= $ci->uri->segment(2) == 'jumlahPasien' ? 'active' : ''; ?>"><a href="<?= base_url();?>Laporan/jumlahPasien"><i class="fa fa-caret-right"></i> Jumlah Pasien</a></li>
                        <li class="<?= $ci->uri->segment(2) == 'jumlahPasienBaru' ? 'active' : ''; ?>"><a href="<?= base_url();?>Laporan/jumlahPasienBaru"><i class="fa fa-caret-right"></i> Jumlah Pasien Baru</a></li>
                        <li class="<?= $ci->uri->segment(2) == 'jumlahPasien20' ? 'active' : ''; ?>"><a href="<?= base_url();?>Laporan/jumlahPasien20"><i class="fa fa-caret-right"></i> Top 20 Pasien</a></li>
                        <li class="<?= $ci->uri->segment(2) == 'rata2Kunjungan' ? 'active' : ''; ?>"><a href="<?= base_url();?>Laporan/rata2Kunjungan"><i class="fa fa-caret-right"></i> Rata-Rata Kunjungan</a></li>
                        <li class="<?= $ci->uri->segment(2) == 'rata2Pasien' ? 'active' : ''; ?>"><a href="<?= base_url();?>Laporan/rata2Pasien"><i class="fa fa-caret-right"></i> Rata-Rata Pasien</a></li>
                    </ul>
                </li>
                <li class="<?= $ci->uri->segment(2) == 'Penyakit' ? 'active' : ''; ?>"><a href="<?= base_url();?>Laporan/Penyakit"><i class="fa fa-circle-o"></i> Jenis Penyakit</a></li>
                <li class="<?= $ci->uri->segment(2) == 'RekamMedis' ? 'active' : ''; ?>"><a href="<?= base_url();?>Laporan/RekamMedis"><i class="fa fa-circle-o"></i> Rekam Medis</a></li>
            </ul>
            <?php elseif(perawat_rawat_inap($u)): ?>
            <ul class="treeview-menu">
                <li class="treeview <?= (
                    $ci->uri->segment(2) == 'jumlahKunjungan' ||
                    $ci->uri->segment(2) == 'jumlahPasien' ||
                    $ci->uri->segment(2) == 'jumlahPasienBaru' ||
                    $ci->uri->segment(2) == 'jumlahPasien20' ||
                    $ci->uri->segment(2) == 'rata2Kunjungan' ||
                    $ci->uri->segment(2) == 'rata2Pasien'
                ) ? 'active menu-open' : ''; ?>">
                    <a href="#">
                        <i class="fa fa-circle-o"></i> <span>Pasien</span><span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
                    </a>
                    <ul class="treeview-menu">
                        <li class="<?= $ci->uri->segment(2) == 'jumlahKunjungan' ? 'active' : ''; ?>"><a href="<?= base_url();?>Laporan"><i class="fa fa-caret-right"></i> Kunjungan Pasien</a></li>
                        <li class="<?= $ci->uri->segment(2) == 'jumlahPasien' ? 'active' : ''; ?>"><a href="<?= base_url();?>Laporan/jumlahPasien"><i class="fa fa-caret-right"></i> Jumlah Pasien</a></li>
                        <li class="<?= $ci->uri->segment(2) == 'jumlahPasienBaru' ? 'active' : ''; ?>"><a href="<?= base_url();?>Laporan/jumlahPasienBaru"><i class="fa fa-caret-right"></i> Jumlah Pasien Baru</a></li>
                        <li class="<?= $ci->uri->segment(2) == 'jumlahPasien20' ? 'active' : ''; ?>"><a href="<?= base_url();?>Laporan/jumlahPasien20"><i class="fa fa-caret-right"></i> Top 20 Pasien</a></li>
                        <li class="<?= $ci->uri->segment(2) == 'rata2Kunjungan' ? 'active' : ''; ?>"><a href="<?= base_url();?>Laporan/rata2Kunjungan"><i class="fa fa-caret-right"></i> Rata-Rata Kunjungan</a></li>
                        <li class="<?= $ci->uri->segment(2) == 'rata2Pasien' ? 'active' : ''; ?>"><a href="<?= base_url();?>Laporan/rata2Pasien"><i class="fa fa-caret-right"></i> Rata-Rata Pasien</a></li>
                    </ul>
                </li>
                <li class="<?= $ci->uri->segment(2) == 'Penyakit' ? 'active' : ''; ?>"><a href="<?= base_url();?>Laporan/Penyakit"><i class="fa fa-circle-o"></i> Jenis Penyakit</a></li>
                <li class="<?= $ci->uri->segment(2) == 'RekamMedis' ? 'active' : ''; ?>"><a href="<?= base_url();?>Laporan/RekamMedis"><i class="fa fa-circle-o"></i> Rekam Medis</a></li>
            </ul>
        <?php else: ?>
            <ul class="treeview-menu">
                <li class="<?= $ci->uri->segment(2) == 'RekamMedis' ? 'active' : ''; ?>"><a href="<?= base_url();?>Laporan/RekamMedis"><i class="fa fa-circle-o"></i> Rekam Medis</a></li>
            </ul>
        <?php endif; ?>
    </li>
<?php } ?>

<?php function laporan_keuangan($u) { $ci =& get_instance(); ?>
    <li class="treeview <?= ($ci->uri->segment(1) == 'Keuangan' ) ? 'active menu-open' : ''; ?>" >
        <a href="#">
            <i class="fa fa-files-o"></i><span>Laporan Keuangan</span><span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
        </a>
        <?php if (kabag_farmasi($u)): ?>
        <ul class="treeview-menu">
            <li class="treeview <?= ($ci->uri->segment(2) == 'resume_pemasukan_' || $ci->uri->segment(2) == 'penjualan_obat_global_' || $ci->uri->segment(2) == 'resume_frekuensi_tindakan') ? 'active menu-open' : ''; ?>">
                <a href="#"><i class="fa fa-circle-o"></i> <span>Pemasukan</span><span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span></a>
                <ul class="treeview-menu">
                    <li class="<?= $ci->uri->segment(2) == 'resume_pemasukan_' ? 'active' : ''; ?>" ><a href="<?= base_url(); ?>Keuangan/resume_pemasukan_"><i class="fa fa-caret-right"></i> Resume Pemasukan</a></li>
                    <li class="<?= $ci->uri->segment(2) == 'resume_frekuensi_tindakan' ? 'active' : ''; ?>" ><a href="<?= base_url(); ?>Keuangan/resume_frekuensi_tindakan"><i class="fa fa-caret-right"></i> Resume Frekuensi Tindakan</a></li>
                    <li class="<?= $ci->uri->segment(2) == 'penjualan_obat_global_' ? 'active' : ''; ?>"><a href="<?= base_url();?>Keuangan/penjualan_obat_global_"><i class="fa fa-caret-right"></i> Penjualan Obat Global</a></li>
                    <li class="<?= $ci->uri->segment(2) == 'rekap_ambulance' ? 'active' : ''; ?>"><a href="<?= base_url();?>Keuangan/rekap_ambulance"><i class="fa fa-caret-right"></i> Rekap Ambulance</a></li>
                </ul>
            </li>
        </ul>
        <?php else: ?>
        <ul class="treeview-menu">
            <li class="<?= $ci->uri->segment(2) == 'printNota' ? 'active' : ''; ?>" ><a href="<?= base_url(); ?>Keuangan/printNota"><i class="fa fa-circle-o"></i> Print Nota</a></li>
            <li class="<?= $ci->uri->segment(2) == 'printNotaAll' ? 'active' : ''; ?>" ><a href="<?= base_url(); ?>Keuangan/printNotaAll"><i class="fa fa-circle-o"></i> Print Nota All</a></li>
            <li class="treeview <?= ($ci->uri->segment(2) == 'resume_pemasukan_' || $ci->uri->segment(2) == 'penjualan_obat_global_' || $ci->uri->segment(2) == 'resume_frekuensi_tindakan') ? 'active menu-open' : ''; ?>">
                <a href="#"><i class="fa fa-circle-o"></i> <span>Pemasukan</span><span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span></a>
                <ul class="treeview-menu">
                    <li class="<?= $ci->uri->segment(2) == 'resume_pemasukan_' ? 'active' : ''; ?>" ><a href="<?= base_url(); ?>Keuangan/resume_pemasukan_"><i class="fa fa-caret-right"></i> Resume Pemasukan</a></li>
                    <li class="<?= $ci->uri->segment(2) == 'resume_frekuensi_tindakan' ? 'active' : ''; ?>" ><a href="<?= base_url(); ?>Keuangan/resume_frekuensi_tindakan"><i class="fa fa-caret-right"></i> Resume Frekuensi Tindakan</a></li>
                    <li class="<?= $ci->uri->segment(2) == 'penjualan_obat_global_' ? 'active' : ''; ?>"><a href="<?= base_url();?>Keuangan/penjualan_obat_global_"><i class="fa fa-caret-right"></i> Penjualan Obat Global</a></li>
                    <li class="<?= $ci->uri->segment(2) == 'rekap_ambulance' ? 'active' : ''; ?>"><a href="<?= base_url();?>Keuangan/rekap_ambulance"><i class="fa fa-caret-right"></i> Rekap Ambulance</a></li>
                </ul>
            </li>
            <li class="<?= $ci->uri->segment(2) == 'listTotalPiutang' ? 'active' : ''; ?>" ><a href="<?= base_url(); ?>Keuangan/listTotalPiutang"><i class="fa fa-circle-o"></i> Piutang</a></li>

            <li class="treeview <?= (seq(2, 'listTotalPengeluaran') || seq(2, 'listTotalPengeluaran') || seq(2, 'pengeluaran_rutin_harian') || seq(2, 'resume_pengeluaran')) ? 'active menu-open' : ''; ?>">
                <a href="#"><i class="fa fa-circle-o"></i> <span>Pengeluaran</span><span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span></a>
                <ul class="treeview-menu">
                    <li class="<?= seq(2, 'pengeluaran_rutin_harian') ? 'active' : ''; ?>"><a href="<?= base_url(); ?>Keuangan/pengeluaran_rutin_harian"><i class="fa fa-circle-o"></i> Pengeluaran Rutin Harian</a></li>
                    <li class="<?= seq(2, 'resume_pengeluaran') ? 'active' : ''; ?>"><a href="<?= base_url(); ?>Keuangan/resume_pengeluaran"><i class="fa fa-circle-o"></i> Resume Pengeluaran</a></li>
                    <li class="<?= seq(2, 'listTotalPengeluaran') ? 'active' : ''; ?>"><a href="<?= base_url(); ?>Keuangan/listTotalPengeluaran"><i class="fa fa-circle-o"></i> Pengeluaran Insentif</a></li>
                </ul>
            </li>

            <li class="<?= $ci->uri->segment(2) == 'labaRugi' ? 'active' : ''; ?>" ><a href="<?= base_url(); ?>Keuangan/labaRugi"><i class="fa fa-circle-o"></i> Laba Rugi</a></li>
        </ul>
        <?php endif; ?>
    </li>
<?php } ?>

<?php function pengaturan_akun() { $ci =& get_instance(); ?>
    <li class="<?= $ci->uri->segment(1) == 'User' ? 'active' : ''; ?>">
        <a href="<?= base_url(); ?>User">
            <i class="fa  fa-user-plus"></i> <span>Pengaturan Akun</span>
        </a>
    </li>
<?php } ?>

<?php function logout() { $ci =& get_instance(); ?>
    <li>
        <a href="<?= base_url(); ?>login/logout">
            <i class="fa  fa-sign-out"></i>
            <span>Log Out</span><span class="pull-right-container"></span>
        </a>
    </li>
<?php } ?>
