<link rel="stylesheet"
      href="<?php echo base_url() ?>assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Data List insentif Obat Farmasi
            <small></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a href="#">List insentif Obat Farmasi</a></li>
            <li class="active">Data List insentif Obat Farmasi</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">

                <div class="box">
                    <div class="box-body">
                        <h5><i class='fa fa-file-text-o fa-fw'></i> Insensif Obat Farmasi</h5>
                        <hr/>

                        <form class="form-horizontal" method="get"
                              action="<?php echo base_url() ?>Insentif/listInsentifObatApoteker">
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="form-horizontal">
                                        <div class="form-group">
                                            <label class="col-sm-4 control-label">Dari Tanggal</label>
                                            <div class="col-sm-8">
                                                <input type='date' name='from' class='form-control' id='tanggal_dari'
                                                       value="<?php echo ($this->input->get('from')) ? $this->input->get('from') : date('Y-m-01') ?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-horizontal">
                                        <div class="form-group">
                                            <label class="col-sm-4 control-label">Sampai Tanggal</label>
                                            <div class="col-sm-8">
                                                <input type='date' name='to' class='form-control' id='tanggal_sampai'
                                                       value="<?php echo ($this->input->get('to')) ? $this->input->get('to') : date('Y-m-d') ?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-horizontal">
                                        <div class="form-group">
                                            <label class="col-sm-4 control-label">Jenis Poli</label>
                                            <div class="col-sm-8">
                                                <select class="form-control" id="jenis_pendaftaran"
                                                        name="jenis_pendaftaran">
                                                    <option value="">-- Jenis --</option>
                                                    <option value="6" <?php if (isset($jenis) && $jenis == "6") echo 'selected'; ?>>Rawat Jalan</option>
                                                    <option value="1" <?php if (isset($jenis) && $jenis == "1") echo 'selected'; ?>>Rawat Inap</option>
                                                    <option value="3" <?php if (isset($jenis) && $jenis == "3") echo 'selected'; ?>>OK</option>
                                                    <option value="2" <?php if (isset($jenis) && $jenis == "2") echo 'selected'; ?>>VK</option>
                                                    <option value="5" <?php if (isset($jenis) && $jenis == "5") echo 'selected'; ?>>HCU</option>
                                                    <option value="4" <?php if (isset($jenis) && $jenis == "4") echo 'selected'; ?>>Perinatologi</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-horizontal">
                                        <div class="form-group">
                                            <div class="col-sm-8 col-sm-offset-4">
                                                <input type="checkbox" id="resep_luar" name="resep_luar" style="position: relative; top: 2px" value="true" <?php if ($this->input->get('resep_luar')) echo "checked"; ?>>
                                                <label class="form-check-label" for="resep_luar" >Resep Luar</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class='row'>
                                <div class="col-sm-3">
                                    <div class="form-horizontal">
                                        <div class="form-group">
                                            <div class="col-sm-4"></div>
                                            <div class="col-sm-8">
                                                <button type="submit" class="btn btn-primary" style='margin-left: 0px;'>
                                                    Tampilkan
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Data List Insentif Obat Farmasi</h3>&nbsp;&nbsp;

                    </div>
                    <!-- /.box-header -->
                    <?php $warning = $this->session->flashdata('warning');
                    if (!empty($warning)){ ?>
                        <div class="alert alert-warning alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <h4><i class="icon fa fa-warning"></i> Alert!</h4>
                            <?php echo $warning ?>
                        </div>
                    <?php } ?>
                    <?php $success = $this->session->flashdata('success');
                    if (!empty($success)){ ?>
                        <div class="alert alert-success alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <h4><i class="icon fa fa-success"></i> Success!</h4>
                            <?php echo $success ?>
                        </div>
                    <?php } ?>
                    <div class="box-body">
                        <table id="printable_table_e" class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>No</th>
                                <th>No RM</th>
                                <th>Nama Pasien</th>
                                <th>Tanggal Pemeriksaan</th>
                                <th>Nama Obat</th>
                                <th>Jumlah</th>
                                <th>Harga Jual</th>
                                <th>Harga Beli</th>
                                <th>Laba</th>
                                <th>Akumulasi</th>
                                <th>Insentif</th>

                            </tr>
                            </thead>

                            <tbody>
                            <?php $no=1; foreach ($listInsentif->result() as $row) { ?>

                                <tr>
                                    <td> <?php echo $no; ?></td>
                                    <td style="width:100px;">
                                        <?php echo $row->no_rm; ?><br>
                                        <small>
                                            <span class="label <?=$jaminan[$row->jaminan]['class']?>"><?=$jaminan[$row->jaminan]['label']?></span>
                                            <?php if (!isset($jaminan[$row->jaminan])) { ?>
                                                <span class="label label-warning">Umum</span>
                                            <?php } ?>
                                        </small>
                                    </td>
                                    <td> <?php echo $row->nama_pasien; ?></td>
                                    <td> <?php echo $row->waktu_pemeriksaan; ?></td>
                                    <td> <?php echo $row->nama_obat; ?></td>
                                    <td> <?php echo $row->jumlah; ?></td>
                                    <td nowrap style="text-align: right">Rp  <?php echo number_format($row->harga_jual, 2,',','.'); ?></td>
                                    <td nowrap style="text-align: right">Rp  <?php echo number_format($row->harga_beli, 2,',','.'); ?></td>
                                    <td nowrap style="text-align: right">Rp  <?php echo number_format($row->laba, 2,',','.'); ?></td>
                                    <td nowrap style="text-align: right">Rp  <?php echo number_format($row->akumulasi, 2,',','.'); ?></td>
                                    <td nowrap style="text-align: right">Rp <?php echo number_format($row->insentif, 2,',','.'); ?></td>

                                </tr>

                                <?php $no++; } ?>


                            </tbody>

                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>

            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->

</div>

<script>
    $(document).ready(function(){
        var table = $('#printable_table_e').DataTable({
            dom: "Bfrtip",
            buttons: [
                {
                    extend: 'copyHtml5',
                    text: '<i class="fa fa-files-o"></i>',
                    titleAttr: 'Copy',
                    exportOptions: {
                        columns: ':visible'
                    }
                },
                {
                    extend: 'excelHtml5',
                    text: '<i class="fa fa-file-excel-o"></i>',
                    titleAttr: 'Excel',
                    title: 'Data Insentif Obat Farmasi',
                    exportOptions: {
                        columns: ':visible'
                    }
                },
                {
                    extend: 'pdfHtml5',
                    text: '<i class="fa fa-file-pdf-o"></i>',
                    titleAttr: 'PDF',
                    exportOptions: {
                        columns: ':visible'
                    }
                },
                {
                    extend: 'print',
                    text: '<i class="fa fa-print"></i>',
                    titleAttr: 'Print',
                    customize: function ( win ) {
                        $(win.document.body)
                            .css( 'font-size', '10pt' );

                        $(win.document.body).find( 'table' )
                            .addClass( 'compact' )
                            .css( 'font-size','inherit');
                    },
                    exportOptions: {
                        columns: ':visible'
                    }
                },
            ]
        });
        table.buttons().container().appendTo( '#printable_table_wrapper .col-md-6:eq(0)' );
    });
</script>