<link rel="stylesheet" href="<?= base_url() ?>assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Update Jadwal Dokter
            <small></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="active"> Update Jadwal Dokter</li>
        </ol>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Daftar Update Jadwal Dokter</h3>&nbsp;&nbsp;
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-12 col-md-6 col-lg-4">
                                <div class="form-group">
                                    <label for="nama" class="control-label">Dokter</label>
                                    <select class="form-control select2" id="dokter">
                                        <?php foreach($dokter as $v) : ?>
                                            <option value="<?=$v['kodedokter']?>"><?=$v['namadokter']?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-12 col-md-6 col-lg-4">
                                <div class="form-group">
                                    <label for="nama" class="control-label">Tanggal</label>
                                    <input type="date" class="form-control" id="tanggal" value="<?=date('Y-m-d')?>">
                                </div>
                            </div>
                            <div class="col-12 col-md-6 col-lg-4">
                                <div style="height: 25px"></div>
                                <button class="btn btn-success" id="b-cari">
                                    <i class="fa fa-search"></i> Cari
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="box-body">
                        <table id="example2" class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>No</th>
                                <th>Dokter</th>
                                <th>Sub Spesialis</th>
                                <th>Hari</th>
                                <th>Jadwal</th>
                                <th>Kapasitas Pasien</th>
                                <th>Libur</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<script>

    $(function () {
        $('#example1').DataTable()
        $('#example2').DataTable({
            rowReorder: {
                selector: 'td:nth-child(2)'
            },
            'paging'      : true,
            'lengthChange': true,
            'searching'   : true,
            'ordering'    : true,
            'info'        : true,
            'autoWidth'   : true
        })

        $('#b-cari').click(function () {
            ajax_search_jadwal(r => {

            })
        })
    })

    const ajax_search_jadwal = (f) => {
        $.ajax({
            url: "<?= site_url('bpjs/jadwal_dokter/raw'); ?>",
            type: "POST",
            data: JSON.stringify({
                'method': 'GET',
                'url': `https://apijkn-dev.bpjs-kesehatan.go.id/vclaim-rest-dev/jadwaldokter/kodepoli/${$('#poli').val()}/tanggal/${$('#tanggal').val()}`
            }),
            success: function (json) {
                const data = json.success ? json.response.list : [
                    {
                        "kodesubspesialis": "ANA",
                        "hari": 4,
                        "kapasitaspasien": 54,
                        "libur": 0,
                        "namahari": "KAMIS",
                        "jadwal": "08:00 - 12:00",
                        "namasubspesialis": "ANAK",
                        "namadokter": "DR. OKTORA WAHYU WIJAYANTO, SP.A",
                        "kodepoli": "ANA",
                        "namapoli": "Anak",
                        "kodedokter": 33690
                    }, {
                        "kodesubspesialis": "ANA",
                        "hari": 4,
                        "kapasitaspasien": 20,
                        "libur": 0,
                        "namahari": "KAMIS",
                        "jadwal": "13:00 - 17:00",
                        "namasubspesialis": "ANAK",
                        "namadokter": "DR. OKTORA WAHYU WIJAYANTO, SP.A",
                        "kodepoli": "ANA",
                        "namapoli": "Anak",
                        "kodedokter": 33690
                    }
                ]

                $('#example2 tbody').empty()
                data.forEach((v, k) => {
                    $('#example2 tbody').append(`
                        <tr>
                            <td>${k + 1}</td>
                            <td>${v.namadokter}</td>
                            <td>${v.namasubspesialis}</td>
                            <td>${v.namahari}</td>
                            <td>${v.jadwal}</td>
                            <td>${v.kapasitaspasien}</td>
                            <td>${v.libur ? 'YA' : 'TIDAK'}</td>
                        </tr>
                    `)
                })
                toastr.success('Berhasil mendapatkan data')

                // if (json.success) {
                //     toastr.success('Berhasil mendapatkan data')
                // }
                // else {
                //     toastr.error('Gagal mendapatkan data')
                // }
            },
            error: (e) => {
                toastr.error('Gagal mendapatkan data')
            }
        })
    }
</script>

<script>
    $(function () {
        <?php $warning = $this->session->flashdata('warning');
        if (!empty($warning)) { ?>
        Swal.fire({
            icon: 'error',
            title: '<?=$warning?>',
        })
        <?php } ?>
        <?php $success = $this->session->flashdata('success');
        if (!empty($success)) { ?>
        Swal.fire({
            icon: 'success',
            title: '<?=$success?>',
        })
        <?php } ?>
    })
</script>
