<link rel="stylesheet" href="<?= base_url() ?>assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Jadwal Dokter
            <small></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="active"> Jadwal Dokter</li>
        </ol>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Daftar Jadwal Dokter</h3>&nbsp;&nbsp;
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-12 col-md-6 col-lg-4">
                                <div class="form-group">
                                    <label for="nama" class="control-label">Poli</label>
                                    <select class="form-control select2" id="poli">
                                        <?php foreach($poli as $v) : ?>
                                        <option value="<?=$v->kdpoli?>"><?=$v->nmpoli?> - <?=$v->nmsubspesialis?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-12 col-md-6 col-lg-4">
                                <div class="form-group">
                                    <label for="nama" class="control-label">Tanggal</label>
                                    <input type="date" class="form-control" id="tanggal" value="<?=date('Y-m-d')?>">
                                </div>
                            </div>
                            <div class="col-12 col-md-6 col-lg-4">
                                <div style="height: 25px"></div>
                                <button class="btn btn-success" id="b-cari">
                                    <i class="fa fa-search"></i> Cari
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="box-body">
                        <table id="example2" class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>No</th>
                                <th>Dokter</th>
                                <th>Sub Spesialis</th>
                                <th>Hari</th>
                                <th>Jadwal</th>
                                <th>Kapasitas Pasien</th>
                                <th>Libur</th>
                                <th>Aksi</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<div class="modal fade" data-backdrop="static" id="modal-edit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" style="font-size: 16px" id="modal-title">Edit Jadwal</h5>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label>Hari</label>
                    <select name="hari" id="hari" class="form-control">
                        <option value="1">Senin</option>
                        <option value="2">Selasa</option>
                        <option value="3">Rabu</option>
                        <option value="4">Kamis</option>
                        <option value="5">Jumat</option>
                        <option value="6">Sabtu</option>
                        <option value="7">Minggu</option>
                    </select>
                </div>
                <div class="form-group">
                    <label>Buka</label>
                    <input class="form-control" type="time" name="buka" id="buka">
                </div>
                <div class="form-group">
                    <label>Tutup</label>
                    <input class="form-control" type="time" name="tutup" id="tutup">
                </div>
                <input type="hidden" id="dokter">
                <input type="hidden" id="kodepoli">
                <input type="hidden" id="kodesubspesialis">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" onclick="simpan_jadwal()">Simpan</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
            </div>
        </div>
    </div>
</div>

<script>

    $(function () {
        $('.select2').select2()
        $('#example1').DataTable()
        $('#example2').DataTable({
            rowReorder: {
                selector: 'td:nth-child(2)'
            },
            'paging'      : true,
            'lengthChange': true,
            'searching'   : true,
            'ordering'    : true,
            'info'        : true,
            'autoWidth'   : true
        })

        $('#b-cari').click(function () {
            $('#example2 tbody').empty()
            ajax_search_jadwal(r => {

            })
        })
    })

    const ajax_search_jadwal = (f) => {
        $.ajax({
            url: "<?= site_url('bpjs/jadwal_dokter/raw'); ?>",
            type: "POST",
            data: JSON.stringify({
                'method': 'GET',
                'url': `<?=$base_url_antrean?>jadwaldokter/kodepoli/${$('#poli').val()}/tanggal/${$('#tanggal').val()}`
            }),
            success: function (r) {
                const json = JSON.parse(r)
                if (json.success) {
                    if (json.data.length) {
                        const data = json.success ? json.data : []
                        all_jadwal = data

                        data.forEach((v, k) => {
                            $('#example2 tbody').append(`
                        <tr>
                            <td>${k + 1}</td>
                            <td>${v.namadokter}</td>
                            <td>${v.namasubspesialis}</td>
                            <td>${v.namahari}</td>
                            <td>${v.jadwal}</td>
                            <td>${v.kapasitaspasien}</td>
                            <td>${v.libur ? 'YA' : 'TIDAK'}</td>
                            <td>
                                <button class="btn btn-warning btn-sm btn-block" onclick="edit_jadwal(${k})"><i class="fa fa-edit"></i> Edit</button>
                            </td>
                        </tr>
                    `)
                        })

                        toastr.success('Berhasil mendapatkan data')
                    }
                    else {
                        toastr.success('Data tidak ada')
                    }
                }
                else {
                    toastr.error(json.server_output.metadata.message ?? 'Gagal mendapatkan data')
                }
            },
            error: (e) => {
                toastr.error('Gagal mendapatkan data')
            }
        })
    }

    let all_jadwal = ''

    function edit_jadwal(k) {
        const jadwal = all_jadwal[k]
        const jam = jadwal.jadwal.replace(/ /g,'').split('-')

        $('#hari').val(jadwal.hari)
        $('#buka').val(jam[0])
        $('#tutup').val(jam[1])
        $('#dokter').val(jadwal.kodedokter)
        $('#kodepoli').val(jadwal.kodepoli)
        $('#kodesubspesialis').val(jadwal.kodesubspesialis)

        $('#modal-edit').modal('show')
    }

    function simpan_jadwal() {
        $.ajax({
            url: "<?= site_url('bpjs/jadwal_dokter/raw'); ?>",
            type: "POST",
            data: JSON.stringify({
                'method': 'POST',
                'url': `<?=$base_url_antrean?>jadwaldokter/updatejadwaldokter`,
                'body': {
                    "kodepoli": $('#kodepoli').val(),
                    "kodesubspesialis": $('#kodesubspesialis').val(),
                    "kodedokter": +$('#dokter').val(),
                    "jadwal": [
                        {
                            "hari": $('#hari').val(),
                            "buka": $('#buka').val(),
                            "tutup": $('#tutup').val()
                        }
                    ]
                }
            }),
            success: function (r) {
                $('#modal-edit').modal('hide')
                const json = JSON.parse(r)
                if (json.success) {
                    toastr.success('Berhasil mengedit data')
                }
                else {
                    toastr.error(json.server_output.metadata.message ?? 'Gagal mengedit data')
                }
            },
            error: (e) => {
                toastr.error('Gagal mengedit data')
            }
        })
    }
</script>

<script>
    $(function () {
        <?php $warning = $this->session->flashdata('warning');
        if (!empty($warning)) { ?>
        Swal.fire({
            icon: 'error',
            title: '<?=$warning?>',
        })
        <?php } ?>
        <?php $success = $this->session->flashdata('success');
        if (!empty($success)) { ?>
        Swal.fire({
            icon: 'success',
            title: '<?=$success?>',
        })
        <?php } ?>
    })
</script>
