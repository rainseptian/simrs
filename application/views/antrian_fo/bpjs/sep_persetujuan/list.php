<link rel="stylesheet" href="<?= base_url() ?>assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Persetujuan SEP
            <small></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="active"> Persetujuan SEP</li>
        </ol>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Persetujuan SEP</h3>&nbsp;&nbsp;
                        <button type="button" class="btn btn-primary pull-right" id="b-add-pengajuan">
                            <span class="glyphicon glyphicon-plus"></span>
                            Tambah Pengajuan
                        </button>
                    </div>
                    <div class="box-body">
                        <table id="example2" class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>No</th>
                                <th>No Kartu</th>
                                <th>Nama Peserta</th>
                                <th>Tgl SEP</th>
                                <th>RI / RJ</th>
                                <th>Persetujuan</th>
                                <th>Status</th>
                                <th>Aksi</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $no=1; foreach ($list as $row) { ?>
                                <?php $res = json_decode($row->response); ?>
                                <tr>
                                    <td><?= $no; ?></td>
                                    <td><?= $row->no_kartu; ?></td>
                                    <td><?= $row->nama_peserta; ?></td>
                                    <td><?= date('d-F-Y', strtotime($row->tgl_sep)); ?></td>
                                    <td><?= $row->ri_rj; ?></td>
                                    <td><?= $row->persetujuan == 1 ? 'Backdate' : 'Fingerprint'; ?></td>
                                    <td>
                                        <?php if ($row->status == 'Baru') : ?>
                                            <span class="label label-primary">Baru</span>
                                        <?php elseif ($row->status == 'Gagal') : ?>
                                            <span class="label label-danger">Gagal</span>
                                        <?php else : ?>
                                            <span class="label label-success">Disetujui</span>
                                        <?php endif; ?>

                                        <?php if ($row->status == 'Gagal') : ?>
                                            <br><small><?=$res->metaData->message ?? ''?></small>
                                        <?php endif; ?>
                                    </td>
                                    <td> 
                                        <?php if ($row->status == 'Baru') : ?>
                                        <a href="<?php echo base_url(); ?>bpjs/sep/persetujuan_setuju/<?= $row->id; ?>" onclick="return confirm('Setujui pengajuan SEP ini?')">
                                            <button type="button" class="btn btn-success btn-sm"><i class="fa fa-check"></i> Setuju</button>
                                        </a>
                                        <?php endif; ?>
                                    </td>
                                </tr>
                                <?php $no++; } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<div class="modal fade" id="modal-add-pengajuan" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <form method="post" action="<?=base_url()?>bpjs/sep/persetujuan">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 style="display: inline-block" class="modal-title" id="exampleModalLabel">Pengajuan SEP</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" id="f">
                    <div class="form-group">
                        <label for="nama" class="control-label">Tgl SEP</label>
                        <input type="date" class="form-control" id="tgl_sep" name="tgl_sep" required value="<?=date('Y-m-d')?>">
                    </div>
                    <div style="display: flex">
                        <div class="form-group" style="flex: 1">
                            <label for="nama" class="control-label">No Kartu</label>
                            <input type="text" class="form-control" name="no_kartu" id="no_kartu" placeholder="No Kartu">
                        </div>
                        <div style="margin-left: 8px; margin-top: 24px">
                            <button type="button" class="btn btn-success" onclick="searchPeserta()"><i class="fa fa-search"></i> </button>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="nama" class="control-label">Nama</label>
                        <input type="text" class="form-control" id="nama_peserta" name="nama_peserta" readonly required>
                    </div>
                    <div class="form-group">
                        <label for="nama" class="control-label">Pelayanan</label>
                        <br>
                        <label style="font-weight: normal">
                            <input type="radio" name="jns_pelayanan" value="1" required checked> Rawat Inap
                        </label>
                        <label style="font-weight: normal; margin-left: 8px">
                            <input type="radio" name="jns_pelayanan" value="2" required> Rawat Jalan
                        </label>
                    </div>
                    <div class="form-group">
                        <label for="nama" class="control-label">Pilih</label>
                        <br>
                        <label style="font-weight: normal">
                            <input type="radio" name="jns_pengajuan" value="1" required checked> Persetujuan tanggal SEP Backdate
                        </label>
                        <label style="font-weight: normal; margin-left: 8px">
                            <input type="radio" name="jns_pengajuan" value="2" required> Persetujuan Fingerprint
                        </label>
                    </div>
                    <div class="form-group">
                        <label for="nama" class="control-label">Keterangan</label>
                        <textarea class="form-control" name="keterangan" id="keterangan" placeholder="Ketik keterangan" required></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success">
                        <i class="fa fa-save"></i>
                        Simpan
                    </button>
                    <button type="button" class="btn btn-warning" data-dismiss="modal">Batal</button>
                </div>
            </div>
        </form>
    </div>
</div>

<script>
    $(function () {
        $('#example1').DataTable()
        $('#example2').DataTable({
            rowReorder: {
                selector: 'td:nth-child(2)'
            },
            'paging'      : true,
            'lengthChange': true,
            'searching'   : true,
            'ordering'    : true,
            'info'        : true,
            'autoWidth'   : true
        })

        $('#b-add-pengajuan').click(function () {
            $('#modal-add-pengajuan').modal('show')
        })
    })

    function searchPeserta() {
        if (!$('#no_kartu').val()) {
            toastr.error('Masukkan no kartu')
            return
        }

        $.ajax({
            url: "<?= site_url('Bpjs/ajax_search_peserta_by_no_bpjs'); ?>",
            type: "POST",
            cache: false,
            data: `keyword=${$('#no_kartu').val()}&tgl=${$('#tgl_sep').val()}`,
            dataType: 'json',
            success: function (json) {
                if (json.status === 1) {
                    $('#nama_peserta').val(json.datanya.peserta.nama)
                    toastr.success('Berhasil mendapatkan peserta')
                }
                if (json.status === 0) {
                    toastr.error(json.server_output?.metaData?.message ?? 'Gagal mendapatkan data')
                }
            }
        })
    }
</script>

<script>
    $(function () {
        <?php $warning = $this->session->flashdata('warning');
        if (!empty($warning)) { ?>
        Swal.fire({
            icon: 'error',
            title: '<?=$warning?>',
        })
        <?php } ?>
        <?php $success = $this->session->flashdata('success');
        if (!empty($success)) { ?>
        Swal.fire({
            icon: 'success',
            title: '<?=$success?>',
        })
        <?php } ?>
    })
</script>
