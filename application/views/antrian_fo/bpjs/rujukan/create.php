<link rel="stylesheet" href="<?= base_url() ?>assets/plugins/iCheck/all.css">
<!--<link rel="stylesheet" href="--><?//= base_url() ?><!--assets/bower_components/select2/dist/css/select2.min.css">-->
<link rel="stylesheet" href="<?= base_url() ?>assets/plugins/autocomplete/style-gue.css">
<style media="screen">
    .select2-container {
        width: 100% !important;
    }
    @media (min-width: 768px) {
        .modal-xl {
            width: 90%;
            max-width:1200px;
        }
    }
</style>

<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Pembuatan Rujukan 
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?= base_url() ?>Dashbord"><i class="fa fa-dashboard"></i> Dashbord</a></li>
            <li class="active">Pembuatan Rujukan</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">

            <div class="col-md-3">
                <div id="divKontrolPanel" style="">
                    <div class="box box-solid box-primary">
                        <div class="box-header with-border">
                            <span><i class="fa fa-envelope"> SEP</i> </span>
                            <div class="box-tools">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse">
                                    <i class="fa fa-minus"></i>
                                </button>
                            </div>
                        </div>
                        <div class="box-body no-padding">
                            <ul class="nav nav-pills nav-stacked">
                                <li><a title="No.SEP"><i class="fa fa-sort-numeric-asc"></i> <label id="lblnosep"><?=$sep->noSep?></label></a></li>
                                <li><a title="Tgl.SEP"><i class="fa fa-calendar"></i> <label id="lbltglsep"><?=$sep->tglSep?></label></a></li>
                                <li><a title="Jns.Pelayanan"><i class="fa fa-medkit"></i> <label id="lbljenpel"><?=$sep->jnsPelayanan?></label></a></li>
                                <li><a title="Poli"><i class="fa fa-bookmark-o"></i> <label id="lblpoli"><?=$poli->kode.' - '.$sep->poli?></label></a></li>
                                <li><a title="Diagnosa"><i class="fa fa-heartbeat"></i> <label id="lbldiagnosa"><?=$sep->diagnosa?></label></a></li>
                            </ul>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <div class="box box-solid box-success">
                        <div class="box-header with-border">
                            <span><i class="fa fa-hospital-o"> Asal Rujukan SEP</i> </span>
                            <div class="box-tools">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse">
                                    <i class="fa fa-minus"></i>
                                </button>
                            </div>
                        </div>
                        <div class="box-body no-padding">
                            <ul class="nav nav-pills nav-stacked">
                                <li><a title="No.Rujukan"><i class="fa fa-sort-numeric-asc"></i> <label id="lblnorujukan"><?=$sep->noRujukan?></label></a></li>
                                <li><a title="Masa Aktif Rujukan"><i class="fa fa-calendar"></i> <label id="lbltglrujukan"><?=$sep->tglRujukan?></label></a></li>
                                <li><a title="Faskes Asal Rujukan"><i class="fa fa-search"></i> <label id="lblfaskesasalrujukan"><?=$sep->faskesAsal?></label></a></li>
                            </ul>
                        </div>
                        <!-- /.box-body -->
                    </div>
                </div>

                <!-- /. box -->
                <div class="box box-solid box-warning">
                    <div class="box-header with-border">
                        <span><i class="fa fa-user"> Peserta</i> </span>
                        <div class="box-tools">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse">
                                <i class="fa fa-minus"></i>
                            </button>
                        </div>
                    </div>
                    <div class="box-body no-padding">
                        <ul class="nav nav-pills nav-stacked">
                            <li><a title="No.Kartu"><i class="fa fa-sort-numeric-asc text-blue"></i> <label id="lblnokartu"><?=$sep->peserta->noKartu?></label></a></li>
                            <li><a title="Nama Peserta"><i class="fa fa-user text-light-blue"></i> <label id="lblnmpeserta"><?=$sep->peserta->nama?></label></a></li>
                            <li><a title="Tgl.Lahir"><i class="fa fa-calendar text-blue"></i> <label id="lbltgllhrpst"><?=$sep->peserta->tglLahir?></label></a></li>
                            <li><a title="Kelamin"><i class="fa fa-intersex  text-blue"></i> <label id="lbljkpst"><?=$sep->peserta->kelamin == 'L' ? 'Laki-laki' : 'Perempuan'?></label></a></li>
                            <li><a title="Kelas Peserta"><i class="fa fa-user  text-blue"></i> <label id="lblklpst"><?=$sep->peserta->hakKelas?></label></a></li>
                            <li><a title="PPK Asal Peserta"><i class="fa fa-user-md  text-blue"></i> <label id="lblppkpst"></label></a></li>
                        </ul>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>

            <div class="col-md-9">
                <form method="post" id="fff" action="<?=base_url()?>bpjs/rujukan/insert">
                    <input type="hidden" name="noSEP" value="<?=$sep->noSep?>">
                    <input type="hidden" name="kodeDokter" value="<?=$sep->dpjp->kdDPJP?>">
                    <input type="hidden" name="poliKontrol" value="<?=$poli->kode?>">

                    <div class="box box-danger">
                        <div class="box-header with-border">
                            <h3 class="box-title">Rujukan</h3>
                        </div>
                        <div class="box-body">
                            <div class="row">
                                <div class="col-12 col-md-6">
                                    <div class="form-group">
                                        <label for="nama" class="control-label">Tgl Rujukan</label>
                                        <input type="date" class="form-control" id="tgl_rujukan" name="tglRujukan" placeholder="Masukkan Tgl Rujukan" required value="<?=date('Y-m-d')?>">
                                    </div>
                                    <div class="form-group">
                                        <label for="nama" class="control-label">Pelayanan</label>
                                        <select class="form-control laka_lantas" name="jnsPelayanan" id="jnsPelayanan">
                                            <option value="2">Rawat Jalan</option>
                                            <option value="1">Rawat Inap</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="nama" class="control-label">Tipe Rujukan</label>
                                        <br>
                                        <label style="font-weight: normal">
                                            <input type="radio" name="tipeRujukan" value="0" required checked> Penuh
                                        </label>
                                        <label style="font-weight: normal; margin-left: 8px">
                                            <input type="radio" name="tipeRujukan" value="1" required> Partial
                                        </label>
                                        <label style="font-weight: normal; margin-left: 8px">
                                            <input type="radio" name="tipeRujukan" value="2" required> Rujuk Balik (Non PRB)
                                        </label>
                                    </div>
                                    <div class="form-group">
                                        <label for="nama" class="control-label">Diagnosa Rujukan</label>
                                        <select class="form-control select2" required
                                                style="width: 100%;"
                                                name="diagRujukan"
                                                id="diag_awal"
                                                data-placeholder="Masukkan Diagnosa Rujukan">
                                        </select>
                                    </div>
                                </div>
                                <div class="col-12 col-md-6">
                                    <div style="display: flex">
                                        <div class="form-group" style="flex: 1">
                                            <label for="nama" class="control-label">Dirujuk Ke</label>
                                            <input type="text" class="form-control" name="haha" id="ppk_dirujuk_name" required readonly/>
                                            <input type="hidden" name="ppkDirujuk" id='ppk_dirujuk'>
                                        </div>
                                        <div style="margin-left: 8px; margin-top: 24px">
                                            <button type="button" class="btn btn-primary" id="bb"><i class="fa fa-search"></i> </button>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="nama" class="control-label">Spesialis/SubSpesialis</label>
                                        <input type="text" class="form-control" id="spesialis-txt" name="spesialis-txt" placeholder="Masukkan Spesialis/SubSpesialis">
                                        <input type="hidden" id="spesialis" name="spesialis">
                                    </div>
                                    <div class="form-group">
                                        <label for="nama" class="control-label">Catatan</label>
                                        <textarea class="form-control" name="catatan" id="catatan" placeholder="Catatan Tidak Boleh Kosong (Minimal 5 alpahnumeric)" required></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="box-footer">
                            <button type="submit" class="btn btn-success pull-right">Simpan</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </section>
</div>

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel">Jadwal Praktek dan Sarana Rumah Sakit Rujukan</h4>
            </div>
            <div class="modal-body">
                <div style="margin-left: 10px; margin-right: 10px">
                    <div class="row">
                        <div class="col-6">
                            <div class="form-group">
                                <label for="nama" class="control-label">PPK Rujuk</label>
                                <input type="text" class="form-control" id="cari_ppk_rujukan"
                                       name="cari_ppk_rujukan"
                                       placeholder="Masukkan PPK Rujuk" required>
                                <input type="hidden" name="ppkDirujuk" id='ppk_rujukan'>
                                <div id='hasil_pencarian_ppk_rujukan' class='hasil_pencarian'></div>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <label for="nama" class="control-label">Tgl Rencana Kunjungan</label>
                                <input form="fff" type="date" class="form-control" id="tgl_renc" name="tglRencanaKunjungan" placeholder="Masukkan Tgl Rencana Kunjungan" required value="<?=date('Y-m-d')?>">
                            </div>
                        </div>
                        <div class="col-6">
                            <button type="button" class="btn btn-success" id="b-cari">Cari</button>
                        </div>
                        <div class="col-12" style="margin-top: 10px">
                            <div class="alert alert-info alert-dismissible">
                                <p>
                                    Untuk Melihat Jadwal Praktek Dokter klik Nama Spesialis/SubSpesialis
                                </p>
                            </div>
                        </div>
                        <div class="col-12" style="margin-top: 10px">
                            <table class="table table-bordered" id="tbl-sp" style="margin-top: 10px">
                                <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Nama Spesialis/Sub</th>
                                    <th>Kapasitas</th>
                                    <th>Jml Rujukan</th>
                                    <th>Prosentase</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
            </div>
        </div>
    </div>
</div>

<div id='ResponseInput'></div>

<script src="<?= base_url() ?>assets/plugins/input-mask/jquery.inputmask.js"></script>
<script src="<?= base_url() ?>assets/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="<?= base_url() ?>assets/plugins/input-mask/jquery.inputmask.extensions.js"></script>
<script src="<?= base_url() ?>assets/bower_components/select2/dist/js/select2.full.min.js"></script>
<script src="<?php echo base_url(); ?>assets/bower_components/toast-alert/toastr.js"></script>

<script>
    function select_spesialistik(kodeSpesialis, namaSpesialis) {
        $('#exampleModal').modal('hide')
        $('#spesialis').val(kodeSpesialis)
        $('#spesialis-txt').val(`${kodeSpesialis} - ${namaSpesialis}`)
    }

    $(function () {
        $('#tbl-sp').DataTable()
        $('#b-cari').click(function () {
            if ($('#ppk_dirujuk').val() === '') {
                alert('Masukkan ppk rujukan dulu')
                return
            }

            $('#tbl-sp tbody').empty()
            $.ajax({
                url: "<?= site_url('Bpjs/ajax_search_spesialistik_rujukan'); ?>",
                type: "POST",
                cache: false,
                data: `ppk=${$('#ppk_dirujuk').val()}&tgl=${$('#tgl_renc').val()}`,
                dataType: 'json',
                success: function (json) {
                    if (json.status === 1) {
                        json.datanya.list.forEach((v, k) => {
                            $('#tbl-sp tbody').append(`
                                <tr>
                                    <td>${k + 1}</td>
                                    <td>
                                        <button type="button" class="btn btn-secondary btn-sm" onclick="select_spesialistik('${v.kodeSpesialis}', '${v.namaSpesialis}')">${v.namaSpesialis}</button>
                                    </td>
                                    <td>${v.kapasitas}</td>
                                    <td>${v.jumlahRujukan}</td>
                                    <td>${v.persentase}</td>
                                    <td>
                                        <button type="button" class="btn btn-success btn-sm" onclick="select_spesialistik('${v.kodeSpesialis}', '${v.namaSpesialis}')"><i class="fa fa-check"></i> </button>
                                    </td>
                                </tr>
                            `)
                        })
                    }
                    if (json.status === 0) {

                    }
                }
            })
        })

        $('#bb').click(function () {
            $('#exampleModal').modal('show')
        })

        $('#diag_awal').select2({
            ajax: {
                url: "<?= site_url('Bpjs/ajax_search_diagnosa'); ?>",
                type: "POST",
                data: function (params) {
                    return { keyword: params.term }
                },
                processResults: function (data) {
                    return {
                        results: JSON.parse(data).datanya.map(v => ({id: v.kode, text: v.nama}))
                    }
                }
            },
            minimumInputLength: 3
        })

        set_search(
            $('#cari_ppk_rujukan'),
            (width, keyword) => {
                if (keyword.length > 2) {
                    ajax_search_faskes_2(width, keyword)
                }
            },
            $('#hasil_pencarian_ppk_rujukan'),
            $li => {
                const kode = $li.find('span#kode').html();
                const nama = $li.find('span#nama').html();
                if (kode && nama) {
                    $('#ppk_dirujuk_name').val(kode + ' - ' + nama);
                    $('#cari_ppk_rujukan').val(kode + ' - ' + nama);
                    $('#ppk_dirujuk').val(kode);
                } else {
                    alert('data tidak ada! Pilih pilihan yang ada sebelum di enter.');
                }
                $('div#hasil_pencarian_ppk_rujukan').hide();
            }
        )

        const ajax_search_faskes_2 = (width, s) => {
            $('div#hasil_pencarian_ppk_rujukan').hide()
            const Lebar = width + 25
            $.ajax({
                url: "<?= site_url('Bpjs/ajax_search_faskes'); ?>",
                type: "POST",
                cache: false,
                data: 'keyword=' + s,
                dataType: 'json',
                success: function (json) {
                    if (json.status === 1) {
                        $('div#hasil_pencarian_ppk_rujukan').css({'width': Lebar + 'px'});
                        $('div#hasil_pencarian_ppk_rujukan').show('fast');
                        $('div#hasil_pencarian_ppk_rujukan').html(json.datanya);
                    }
                    if (json.status === 0) {
                        $('div#hasil_pencarian_ppk_rujukan').html('');
                    }
                }
            })
        }

    })
</script>

<script>
    const set_search = ($input, search_ajax_fn, $display, on_selected) => {
        let timer = null
        const display_id = $display.attr('id')

        $input.on('keyup', function (e) {
            const s = $(this).val()
            if (s !== '') {
                clearTimeout(timer)
                timer = setTimeout(() => {
                    const charCode = ( e.which ) ? e.which : e.keyCode
                    if (charCode === 40) {
                        if ($('.form-group').find(`div#${display_id} li.autocomplete_active`).length > 0) {
                            var selanjutnya = $('.form-group').find(`div#${display_id} li.autocomplete_active`).next();
                            $('.form-group').find(`div#${display_id} li.autocomplete_active`).removeClass('autocomplete_active');
                            selanjutnya.addClass('autocomplete_active');
                        } else {
                            $('.form-group').find(`div#${display_id} li:first`).addClass('autocomplete_active');
                        }
                    }
                    else if (charCode === 38) {
                        if ($('.form-group').find(`div#${display_id} li.autocomplete_active`).length > 0) {
                            var sebelumnya = $('.form-group').find(`div#${display_id} li.autocomplete_active`).prev();
                            $('.form-group').find(`div#${display_id} li.autocomplete_active`).removeClass('autocomplete_active');
                            sebelumnya.addClass('autocomplete_active');
                        } else {
                            $('.form-group').find(`div#${display_id} li:first`).addClass('autocomplete_active');
                        }
                    }
                    else if (charCode === 13) {
                        on_selected($('.form-group').find(`div#${display_id} li.autocomplete_active`))
                    }
                    else {
                        search_ajax_fn($input.width(), s);
                    }
                }, 100)
            }
            else {
                $display.hide();
            }
        })

        $(document).on('click', `div#${display_id} .daftar-autocomplete li`, function () {
            on_selected($(this))
            $display.hide();
        })
    }
</script>
