<link rel="stylesheet" href="<?= base_url() ?>assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Rujukan
            <small></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="active"> Rujukan</li>
        </ol>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Daftar Rujukan</h3>&nbsp;&nbsp;
<!--                        <a href="--><?//= base_url(); ?><!--bpjs/rujukan/create"><button type="button" class="btn btn-primary"><span class="glyphicon glyphicon-plus"></span></button></a>-->
                    </div>
                    <?php $warning = $this->session->flashdata('warning');
                    if (!empty($warning)){ ?>
                        <div class="alert alert-warning alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <h4><i class="icon fa fa-warning"></i> Alert!</h4>
                            <?= $warning ?>
                        </div>
                    <?php } ?>
                    <?php $success = $this->session->flashdata('success');
                    if (!empty($success)){ ?>
                        <div class="alert alert-success alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <h4><i class="icon fa fa-success"></i> Success!</h4>
                            <?= $success ?>
                        </div>
                    <?php } ?>
                    <div class="box-body">
                        <table id="example2" class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>No</th>
                                <th>Tgl Dibuat</th>
                                <th>No SEP</th>
                                <th>Tgl Rujukan</th>
                                <th>PPK Rujuk</th>
                                <th>Peserta</th>
                                <th>Status</th>
                                <th>Response</th>
                                <th>Aksi</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $no=1; foreach ($list as $row) { ?>
                                <?php $res = json_decode($row->response); ?>
                                <tr>
                                    <td><?= $no; ?></td>
                                    <td><?= date('d-M-Y H:i', strtotime($row->created_at)); ?></td>
                                    <td><?= $row->no_sep; ?></td>
                                    <td>
                                        <?php if ($row->success): ?>
                                            <?= date('d-M-Y', strtotime($res->rujukan->tglRujukan)); ?>
                                        <?php endif; ?>
                                    </td>
                                    <td>
                                        <?php if ($row->success): ?>
                                            <?= $res->rujukan->tujuanRujukan->kode . ' - ' . $res->rujukan->tujuanRujukan->nama ?>
                                        <?php endif; ?>
                                    </td>
                                    <td>
                                        <?php if ($row->success): ?>
                                            <?= $res->rujukan->peserta->nama; ?>
                                            <br>
                                            <small><i>No Kartu: <?= $res->rujukan->peserta->noKartu; ?></i></small>
                                        <?php endif; ?>
                                    </td>
                                    <td>
                                        <?php if ($row->success): ?>
                                            <span class="label label-success">Berhasil</span>
                                        <?php else: ?>
                                            <span class="label label-danger">Gagal</span>
                                        <?php endif; ?>
                                    </td>
                                    <td>
                                        <?php if ($res->rujukan->noRujukan): ?>
                                            No Rujukan: <?= $res->rujukan->noRujukan; ?>
                                        <?php else: ?>
                                            <?= $res->metaData->message ?? ''; ?>
                                        <?php endif; ?>
                                    </td>
                                    <td>
                                        <?php if ($row->success): ?>
                                            <a href="<?php echo base_url(); ?>bpjs/rujukan/detail/<?= $row->id; ?>">
                                                <button type="button" class="btn btn-primary btn-sm btn-block"><i class="fa fa-arrows"></i> Detail</button>
                                            </a>
                                            <div style="height: 4px"></div>
                                            <a href="<?php echo base_url(); ?>bpjs/rujukan/print_rujukan/<?= $row->id; ?>" target="_blank">
                                                <button type="button" class="btn btn-success btn-sm btn-block"><i class="fa fa-print"></i> Cetak Surat</button>
                                            </a>
                                        <?php endif; ?>
                                        <?php if (!$row->success): ?>
<!--                                            <div style="height: 4px"></div>-->
<!--                                            <a href="--><?php //echo base_url(); ?><!--Bpjs/edit_sep/--><?//= $row->id; ?><!--">-->
<!--                                                <button type="button" class="btn btn-warning btn-block btn-sm"><i class="fa fa-pencil"></i> Edit</button>-->
<!--                                            </a>-->
                                        <?php endif; ?>
                                    </td>
                                </tr>
                                <?php $no++; } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<div class="modal fade" id="modal-detail" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 style="display: inline-block" class="modal-title" id="exampleModalLabel">Modal title</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" id="f">
                <table class="table table-bordered" id="tbl-detail">
                    <thead>
                    <tr>
                        <th>Key</th>
                        <th>Value</th>
                    </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<script>

    const parse = (obj, parent_name = '') => {
        for (let key in obj) {
            if (Array.isArray(obj[key])) {
                obj[key].forEach((v, k) => {
                    parse(v, key + '-' + (k + 1))
                })
            }
            else if (typeof obj[key] === 'object') {
                parse(obj[key], key)
            }
            else {
                $('#tbl-detail > tbody').append(`
                    <tr>
                        <td>${parent_name ? parent_name + ' -> ' + key : key}</td>
                        <td>${obj[key]}</td>
                    </tr>
                `)
            }
        }
    }

    function open_detail(id) {
        const list = <?=json_encode($list);?>;
        const v = list.find(v => parseInt(v.id) === parseInt(id))
        if (v) {
            $('#tbl-detail > tbody').html('')
            const request = JSON.parse(v.request).request.t_rujukan
            parse(request)

            $('#exampleModalLabel').html(`Detail Rujukan ${v.nama}`)
            $('#modal-detail').modal('show')
        }
    }

    $(function () {
        $('#example1').DataTable()
        $('#example2').DataTable({
            rowReorder: {
                selector: 'td:nth-child(2)'
            },
            'paging'      : true,
            'lengthChange': true,
            'searching'   : true,
            'ordering'    : true,
            'info'        : true,
            'autoWidth'   : true
        })
    })
</script>