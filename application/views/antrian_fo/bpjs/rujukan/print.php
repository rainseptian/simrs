<?php

function DateToIndo($date) {
    $BulanIndo = array("Januari", "Februari", "Maret", "April",
        "Mei", "Juni", "Juli", "Agustus", "September", "Oktober",
        "November", "Desember");
    $e = explode('-', $date);
    $tgl = $e[2];
    $bulan = $e[1];
    $tahun = $e[0];
    $result = $tgl . " " . $BulanIndo[(int)$bulan-1] . " ". $tahun;
    return($result);
}

?>

<html>
<head>
    <script
            src="https://code.jquery.com/jquery-3.4.1.min.js"
            integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
            crossorigin="anonymous"></script>
    <title>Rujukan</title>
    <style>
        .container {
            width: 100%;
            margin: auto;
        }

        .one1 {
            width: 65%;
            float: left;
        }

        .two1 {
            margin-left: 45%;
        }

        .one {
            width: 55%;
            float: left;
        }

        .two {
            margin-left: 55%;
        }
    </style>
</head>
<body>
    <div class="container">
        <div class="one1">
            <div style="display: flex; margin-bottom: 16px">
                <div style="width: 200px">
                    <img src="<?=base_url('/assets/img/logo-bpjs.png')?>" style="display: block; width: 100%; height: auto;">
                </div>
                <p style="padding: 0; margin: 0 0 0 16px"><b>SURAT RUJUKAN<br>KLINIK UTAMA SUKMA WIJAYA</b></p>
            </div>
        </div>
        <div class="two1">
            <p>No. <?=$response->noRujukan?></p>
            <small>Tgl. <?=DateToIndo($response->tglRujukan)?></small>
        </div>
    </div>
    <div class="container">
        <div class="one">
            <table>
                <tr>
                    <td>Kepada Yth</td>
                    <td>:</td>
                    <td><?=$response->poliTujuan->nama ? "{$response->poliTujuan->nama}<br>" : ''?><?=$response->tujuanRujukan->nama?></td>
                </tr>
                <tr>
                    <td colspan="3">Mohon Pemeriksaan dan Penanganan Lebih Lanjut :</td>
                </tr>
                <tr>
                    <td>No Kartu</td>
                    <td>:</td>
                    <td><?=$response->peserta->noKartu?></td>
                </tr>
                <tr>
                    <td>Nama Peserta</td>
                    <td>:</td>
                    <td><?=strtoupper($response->peserta->nama)?> (<?=$response->peserta->kelamin?>)</td>
                </tr>
                <tr>
                    <td>Tgl Lahir</td>
                    <td>:</td>
                    <td><?=$response->peserta->tglLahir?></td>
                </tr>
                <tr>
                    <td>Diagnosa</td>
                    <td>:</td>
                    <td><?=$response->diagnosa->nama?></td>
                </tr>
                <tr>
                    <td>Keterangan</td>
                    <td>:</td>
                    <td></td>
                </tr>
            </table>
        </div>
        <div class="two">
            <table>
                <tr>
                    <td colspan="3"></td>
                </tr>
                <tr>
                    <td colspan="3"></td>
                </tr>
                <tr>
                    <td colspan="3">==<?=$request->tipeRujukan == 0 ? 'Rujukan Penuh' : ($request->tipeRujukan == 1 ? 'Rujukan Partial' : 'Rujukan Balik') ?>==</td>
                </tr>
                <tr>
                    <td colspan="3"><?=$request->jnsPelayanan == 1 ? 'Rawat Inap' : 'Rawat Jalan' ?></td>
                </tr>
            </table>
        </div>
    </div>
    <div style="clear: both"></div>
    <div class="container" style="margin-top: 16px">
        <div class="one">
            <p>Demikian atas bantuannya,diucapkan banyak terima kasih.</p>
            <small style="font-size: 11px">*Rujukan Berlaku Sampai Dengan <?=DateToIndo(date($response->tglBerlakuKunjungan))?></small><br>
            <small style="font-size: 11px">*Tgl.Rencana Berkunjung <?=DateToIndo(date($response->tglRencanaKunjungan))?></small>
            <br>
            <br>
            <small style="font-size: 11px">Tgl. Cetak <?=DateToIndo(date('Y-m-d'))?></small>
        </div>
        <div class="two" style="margin-top: 16px; text-align: center">
            <p style="margin: 0">
                Mengetahui,<br><br>
                ____________________
            </p>
        </div>
    </div>
    <script>
        $( document ).ready(function() {
            window.print();
        });
    </script>
</body>
</html>