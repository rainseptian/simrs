<link rel="stylesheet" href="<?= base_url() ?>assets/plugins/iCheck/all.css">
<!--<link rel="stylesheet" href="--><?//= base_url() ?><!--assets/bower_components/select2/dist/css/select2.min.css">-->
<link rel="stylesheet" href="<?= base_url() ?>assets/plugins/autocomplete/style-gue.css">
<style media="screen">
    .select2-container {
        width: 100% !important;
    }
    @media (min-width: 768px) {
        .modal-xl {
            width: 90%;
            max-width:1200px;
        }
    }
    #hasil_pencarian_nama_obat {
        z-index: 1000;
    }
</style>

<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Pembuatan Rujuk Balik (PRM)
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?= base_url() ?>Dashbord"><i class="fa fa-dashboard"></i> Dashbord</a></li>
            <li class="active">Pembuatan Rujuk Balik (PRM)</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">

            <div class="col-md-3">
                <div id="divKontrolPanel" style="">
                    <div class="box box-solid box-primary">
                        <div class="box-header with-border">
                            <span><i class="fa fa-envelope"> SEP</i> </span>
                            <div class="box-tools">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse">
                                    <i class="fa fa-minus"></i>
                                </button>
                            </div>
                        </div>
                        <div class="box-body no-padding">
                            <ul class="nav nav-pills nav-stacked">
                                <li><a title="No.SEP"><i class="fa fa-sort-numeric-asc"></i> <label id="lblnosep"><?=$sep->noSep?></label></a></li>
                                <li><a title="Tgl.SEP"><i class="fa fa-calendar"></i> <label id="lbltglsep"><?=$sep->tglSep?></label></a></li>
                                <li><a title="Jns.Pelayanan"><i class="fa fa-medkit"></i> <label id="lbljenpel"><?=$sep->jnsPelayanan?></label></a></li>
                                <li><a title="Poli"><i class="fa fa-bookmark-o"></i> <label id="lblpoli"><?=$poli->kode.' - '.$sep->poli?></label></a></li>
                                <li><a title="Diagnosa"><i class="fa fa-heartbeat"></i> <label id="lbldiagnosa"><?=$sep->diagnosa?></label></a></li>
                            </ul>
                        </div>
                        <!-- /.box-body -->
                    </div>
                </div>

                <!-- /. box -->
                <div class="box box-solid box-warning">
                    <div class="box-header with-border">
                        <span><i class="fa fa-user"> Peserta</i> </span>
                        <div class="box-tools">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse">
                                <i class="fa fa-minus"></i>
                            </button>
                        </div>
                    </div>
                    <div class="box-body no-padding">
                        <ul class="nav nav-pills nav-stacked">
                            <li><a title="No.Kartu"><i class="fa fa-sort-numeric-asc text-blue"></i> <label id="lblnokartu"><?=$sep->peserta->noKartu?></label></a></li>
                            <li><a title="Nama Peserta"><i class="fa fa-user text-light-blue"></i> <label id="lblnmpeserta"><?=$sep->peserta->nama?></label></a></li>
                            <li><a title="Tgl.Lahir"><i class="fa fa-calendar text-blue"></i> <label id="lbltgllhrpst"><?=$sep->peserta->tglLahir?></label></a></li>
                            <li><a title="Kelamin"><i class="fa fa-intersex  text-blue"></i> <label id="lbljkpst"><?=$sep->peserta->kelamin == 'L' ? 'Laki-laki' : 'Perempuan'?></label></a></li>
                            <li><a title="Kelas Peserta"><i class="fa fa-user  text-blue"></i> <label id="lblklpst"><?=$sep->peserta->hakKelas?></label></a></li>
                            <li><a title="PPK Asal Peserta"><i class="fa fa-user-md  text-blue"></i> <label id="lblppkpst"></label></a></li>
                        </ul>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>

            <div class="col-md-9">
                <form method="post" action="<?=base_url()?>bpjs/prb/insert">
                    <input type="hidden" name="noSEP" value="<?=$sep->noSep?>">
                    <input type="hidden" name="noKartu" value="<?=$sep->peserta->noKartu?>">
                    <input type="hidden" name="poliKontrol" value="<?=$poli->kode?>">
                    <input type="hidden" name="obat" id="obat">

                    <div class="box box-danger">
                        <div class="box-header with-border">
                            <h3 class="box-title">Rujuk Balik (PRB)</h3>
                        </div>
                        <div class="box-body">
                            <div class="row">
                                <div class="col-12 col-md-6">
                                    <div class="form-group">
                                        <label for="nama" class="control-label">Tgl Rujuk Balik</label>
                                        <input type="date" class="form-control" id="tgl_rujuk_balik" name="tgl_rujuk_balik" placeholder="Masukkan Tgl Rujuk Balik" required value="<?=date('Y-m-d')?>" readonly>
                                    </div>
                                    <div class="form-group">
                                        <label for="nama" class="control-label">Alamat Peserta</label>
                                        <input type="text" class="form-control" name="alamat" id="alamat" placeholder="Alamat Peserta" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="nama" class="control-label">Kontak/Email Peserta/Keluarga </label>
                                        <input type="text" class="form-control" name="email" id="email" placeholder="Kontak/Email Peserta/Keluarga" required>
                                    </div>
                                </div>
                                <div class="col-12 col-md-6">
                                    <div class="form-group">
                                        <label for="nama" class="control-label">Program PRB</label>
                                        <select class="form-control laka_lantas" name="programPRB" id="programPRB" required>
                                            <option value="0">--Silahkan Pilih--</option>
                                            <?php foreach ($diag_prb as $v) : ?>
                                                <option value="<?=$v->kode?>"><?=$v->nama?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="nama" class="control-label">DPJP Pemberi Pelayanan</label>
                                        <select class="form-control laka_lantas" name="dpjp" id="dpjp" required>
                                            <option value="0">--Silahkan Pilih--</option>
                                            <?php foreach ($dpjp as $v) : ?>
                                                <option value="<?=$v['kode']?>"><?=$v['nama']?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="nama" class="control-label">Keterangan</label>
                                        <input type="text" class="form-control" name="keterangan" id="keterangan" placeholder="Ketik keterangan maksimal 150 karakter">
                                    </div>
                                    <div class="form-group">
                                        <label for="nama" class="control-label">Saran</label>
                                        <input type="text" class="form-control" name="saran" id="saran" placeholder="Ketik saran maksimal 150 karakter">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="box-header with-border">
                            <h3 class="box-title">Obat</h3>
                        </div>
                        <div class="box-body">
                            <div class="row">
                                <div class="col-12 col-md-6">
                                    <div class="form-group">
                                        <label for="nama" class="control-label">Nama Obat</label>
                                        <input type="text" class="form-control" name="nama_obat" id="cari_nama_obat" placeholder="Ketik kode atau nama obat min 3 karakter">
                                        <input type="hidden" name="nama_obat" id='nama_obat'>
                                        <div id='hasil_pencarian_nama_obat' class='hasil_pencarian'></div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12 col-md-6">
                                    <label for="nama" class="control-label">Signa</label>
                                    <div class="input-group">
                                        <input type="number" class="form-control" id="signa1" value="0" max="9" min="0" placeholder="Signa 1" onkeyup="if(this.value>99){this.value='9';}else if(this.value<0){this.value='0';}">
                                        <span class="input-group-addon">
                                            x
                                        </span>
                                        <input type="number" class="form-control" id="signa2" value="0" max="9" min="0" placeholder="Signa 2" onkeyup="if(this.value>99){this.value='9';}else if(this.value<0){this.value='0';}">
                                    </div>
                                </div>
                                <div class="col-12 col-md-6">
                                    <div class="input-group">
                                        <label for="nama" class="control-label">Jumlah</label>
                                        <input type="number" class="form-control" id="jumlah" value="0" min="0" placeholder="Jumlah">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12 col-md-6" style="margin-top: 10px">
                                    <button type="button" class="btn btn-success" id="b-add-obat"><i class="fa fa-plus"></i> Tambah Obat</button>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12 col-md-12" style="margin-top: 10px">
                                    <table class="table table-bordered" id="tbl-obat">
                                        <thead>
                                        <tr>
                                            <td>#</td>
                                            <td>Nama Obat</td>
                                            <td>S1</td>
                                            <td>S2</td>
                                            <td>Jumlah</td>
                                            <td></td>
                                        </tr>
                                        </thead>
                                        <tbody>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="box-footer">
                            <button type="submit" class="btn btn-success pull-right">Simpan</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </section>
</div>

<div id='ResponseInput'></div>

<script src="<?= base_url() ?>assets/plugins/input-mask/jquery.inputmask.js"></script>
<script src="<?= base_url() ?>assets/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="<?= base_url() ?>assets/plugins/input-mask/jquery.inputmask.extensions.js"></script>
<script src="<?= base_url() ?>assets/bower_components/select2/dist/js/select2.full.min.js"></script>
<script src="<?php echo base_url(); ?>assets/bower_components/toast-alert/toastr.js"></script>

<script>
    let kode, nama

    $(function () {
        $('#b-add-obat').click(function () {
            if (!$('#nama_obat').val()) {
                toastr.error('Masukkan nama obat dulu')
                return
            }
            if (+$('#signa1').val() === 0) {
                toastr.error('Masukkan signa 1 dulu')
                return
            }
            if ($('#signa1').val().length > 1) {
                toastr.error('Signa 1 maksimal 1 karakter')
                return
            }
            if (+$('#signa2').val() === 0) {
                toastr.error('Masukkan signa 2 dulu')
                return
            }
            if ($('#signa2').val().length > 1) {
                toastr.error('Signa 2 maksimal 1 karakter')
                return
            }
            if (+$('#jumlah').val() === 0) {
                toastr.error('Masukkan jumlah dulu')
                return
            }

            add_obat_to_table()
        })

        set_search(
            $('#cari_nama_obat'),
            (width, keyword) => {
                ajax_search_obat(width, keyword)
            },
            $('#hasil_pencarian_nama_obat'),
            $li => {
                kode = $li.find('span#kode').html();
                nama = $li.find('span#nama').html();
                if (kode && nama) {
                    $('#cari_nama_obat').val(kode + ' - ' + nama);
                    $('#nama_obat').val(kode);
                } else {
                    alert('data tidak ada! Pilih pilihan yang ada sebelum di enter.');
                }
                $('div#hasil_pencarian_nama_obat').hide();
            }
        )

        const ajax_search_obat = (width, s) => {
            $('div#hasil_pencarian_nama_obat').hide()
            const Lebar = width + 25
            $.ajax({
                url: "<?= site_url('bpjs/prb/ajax_search_obat_list'); ?>",
                type: "POST",
                cache: false,
                data: 'keyword=' + s,
                dataType: 'json',
                success: function (json) {
                    if (json.status === 1) {
                        $('div#hasil_pencarian_nama_obat').css({'width': Lebar + 'px'});
                        $('div#hasil_pencarian_nama_obat').show('fast');
                        $('div#hasil_pencarian_nama_obat').html(json.datanya);
                    }
                    if (json.status === 0) {
                        $('div#hasil_pencarian_nama_obat').html('');
                    }
                }
            })
        }
    })

    let obat = []

    function add_obat_to_table() {
        $('#tbl-obat tbody').append(`
            <tr id="row-obat-${kode}">
                <td>${kode}</td>
                <td>${nama}</td>
                <td>${$('#signa1').val()}</td>
                <td>${$('#signa2').val()}</td>
                <td>${$('#jumlah').val()}</td>
                <td>
                    <button class="btn btn-sm btn-danger" onclick="delete_obat('${kode}')"><i class="fa fa-trash"></i> </button>
                </td>
            </tr>
        `)

        obat.push({
            kdObat: kode,
            signa1: $('#signa1').val(),
            signa2: $('#signa2').val(),
            jmlObat: $('#jumlah').val(),
        })
        $('#obat').val(JSON.stringify(obat))

        kode = ''
        nama = ''
        $('#cari_nama_obat').val('')
        $('#nama_obat').val('')
        $('#signa1').val('0')
        $('#signa2').val('0')
        $('#jumlah').val('0')
    }

    function delete_obat(kode) {
        obat = obat.filter(v => v.kdObat !== kode)
        $('#obat').val(JSON.stringify(obat))
        $(`#row-obat-${kode}`).remove()
    }

    const set_search = ($input, search_ajax_fn, $display, on_selected) => {
        let timer = null
        const display_id = $display.attr('id')

        $input.on('keyup', function (e) {
            const s = $(this).val()
            if (s !== '') {
                clearTimeout(timer)
                timer = setTimeout(() => {
                    const charCode = ( e.which ) ? e.which : e.keyCode
                    if (charCode === 40) {
                        if ($('.form-group').find(`div#${display_id} li.autocomplete_active`).length > 0) {
                            var selanjutnya = $('.form-group').find(`div#${display_id} li.autocomplete_active`).next();
                            $('.form-group').find(`div#${display_id} li.autocomplete_active`).removeClass('autocomplete_active');
                            selanjutnya.addClass('autocomplete_active');
                        } else {
                            $('.form-group').find(`div#${display_id} li:first`).addClass('autocomplete_active');
                        }
                    }
                    else if (charCode === 38) {
                        if ($('.form-group').find(`div#${display_id} li.autocomplete_active`).length > 0) {
                            var sebelumnya = $('.form-group').find(`div#${display_id} li.autocomplete_active`).prev();
                            $('.form-group').find(`div#${display_id} li.autocomplete_active`).removeClass('autocomplete_active');
                            sebelumnya.addClass('autocomplete_active');
                        } else {
                            $('.form-group').find(`div#${display_id} li:first`).addClass('autocomplete_active');
                        }
                    }
                    else if (charCode === 13) {
                        on_selected($('.form-group').find(`div#${display_id} li.autocomplete_active`))
                    }
                    else {
                        search_ajax_fn($input.width(), s);
                    }
                }, 100)
            }
            else {
                $display.hide();
            }
        })

        $(document).on('click', `div#${display_id} .daftar-autocomplete li`, function () {
            on_selected($(this))
            $display.hide();
        })
    }
</script>
