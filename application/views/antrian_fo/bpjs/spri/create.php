<link rel="stylesheet" href="<?= base_url() ?>assets/plugins/iCheck/all.css">
<!--<link rel="stylesheet" href="--><?//= base_url() ?><!--assets/bower_components/select2/dist/css/select2.min.css">-->
<link rel="stylesheet" href="<?= base_url() ?>assets/plugins/autocomplete/style-gue.css">
<style media="screen">
    .select2-container {
        width: 100% !important;
    }
</style>

<div class="content-wrapper">
    <section class="content-header">
        <h1>
            SPRI
            <small>Preview</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?= base_url() ?>Dashbord"><i class="fa fa-dashboard"></i> Dashbord</a></li>
            <li class="active">Tambah SPRI</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-danger">
                    <div class="box-header">
                        <h3 class="box-title">Tambah SPRI</h3>
                    </div>
                    <div class="box-header" id="err-box" style="display: none">
                        <div class="alert alert-warning alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <h4><i class="icon fa fa-warning"></i> Alert!</h4>
                            <p id="err">kajdhkajhs</p>
                        </div>
                    </div>
                    <?php $warning = $this->session->flashdata('warning');
                    if (!empty($warning)) { ?>
                        <div class="alert alert-warning alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <h4><i class="icon fa fa-warning"></i> Alert!</h4>
                            <?= $warning ?>
                        </div>
                    <?php } ?>
                    <?php $success = $this->session->flashdata('success');
                    if (!empty($success)) { ?>
                        <div class="alert alert-success alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <h4><i class="icon fa fa-check"></i> Success!</h4>
                            <?= $success ?>
                        </div>
                    <?php } ?>
                    <div class="box-body">
                        <form class="form" method="post" action="<?= base_url() ?>bpjs/spri/create">
                            <div class="box-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="nama" class="control-label">Nomor Kartu BPJS</label>
                                            <input type="text" class="form-control" id="cari_nomor_sep"
                                                   name="cari_nomor_sep"
                                                   placeholder="Masukkan nomor SEP / nomor BPJS / nama pasien" required>
                                            <input type="hidden" name="nomor_sep" id='nomor_sep'>
                                            <input type="hidden" name="nomor_bjps" id='nomor_bjps'>
                                            <input type="hidden" name="id_pasien" id='id_pasien'>
                                            <div id='hasil_pencarian' class='hasil_pencarian'></div>
                                        </div>
                                        <div class="form-group">
                                            <label for="nama" class="control-label">Tgl SPRI</label>
                                            <input type="date" class="form-control" name="tgl_spri" id="tgl_spri" placeholder="Masukkan SPRI" required>
                                        </div>
                                        <div class="form-group">
                                            <label for="nama" class="control-label">Poli Kontrol</label>
                                            <select class="form-control select2" required
                                                    style="width: 100%;"
                                                    name="poli_kontrol"
                                                    id="poli_kontrol"
                                                    data-placeholder="Masukkan Poli Kontrol">
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="nama" class="control-label">Dokter</label>
                                            <select type="text" class="form-control select2" id="kode_dokter" name="kode_dokter" required>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">

                                    </div>
                                </div>
                                <div class="box-footer">
                                    <button type="submit" name="submit" value="1" id="elementID"
                                            class="btn btn-primary btn-lg btn-flat pull-right">Simpan
                                    </button>
                                    <a href="javascript:history.back()"
                                       class="btn btn-default btn-lg btn-flat pull-right">Batal</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>


<div id='ResponseInput'></div>
<!-- InputMask -->
<script src="<?= base_url() ?>assets/plugins/input-mask/jquery.inputmask.js"></script>
<script src="<?= base_url() ?>assets/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="<?= base_url() ?>assets/plugins/input-mask/jquery.inputmask.extensions.js"></script>
<script src="<?= base_url() ?>assets/bower_components/select2/dist/js/select2.full.min.js"></script>

<script>

    $(function () {
        set_search(
            $('#cari_nomor_sep'),
            (width, keyword) => { ajax_no_sep(width, keyword) },
            $('#hasil_pencarian'),
            $li => {
                const pasien_id = $li.find('span#pasien_id').html();
                const no_rm = $li.find('span#no_rm').html();
                const nama = $li.find('span#nama').html();
                const no_sep = $li.find('span#no_sep').html();
                const nomor_bjps = $li.find('span#no_bpjs').html();
                const alamat = $li.find('span#alamat').html();

                if (no_sep) {
                    $('#cari_nomor_sep').val(no_sep + ' - ' + nama);
                    $('#id_pasien').val(pasien_id);
                    $('#nomor_sep').val(no_sep).change();
                    $('#nomor_bjps').val(nomor_bjps);
                    $('#s_alamat').val(alamat);
                    $('#no_mr').val(no_rm)
                } else {
                    alert('data tidak ada! Pilih pilihan yang ada sebelum di enter.');
                }
                $('div#hasil_pencarian').hide();
            }
        )
    })

    const set_search = ($input, search_ajax_fn, $display, on_selected) => {
        let timer = null
        const display_id = $display.attr('id')

        $input.on('keyup', function (e) {
            const s = $(this).val()
            if (s !== '') {
                clearTimeout(timer)
                timer = setTimeout(() => {
                    const charCode = ( e.which ) ? e.which : e.keyCode
                    if (charCode === 40) {
                        if ($('.form-group').find(`div#${display_id} li.autocomplete_active`).length > 0) {
                            var selanjutnya = $('.form-group').find(`div#${display_id} li.autocomplete_active`).next();
                            $('.form-group').find(`div#${display_id} li.autocomplete_active`).removeClass('autocomplete_active');
                            selanjutnya.addClass('autocomplete_active');
                        } else {
                            $('.form-group').find(`div#${display_id} li:first`).addClass('autocomplete_active');
                        }
                    }
                    else if (charCode === 38) {
                        if ($('.form-group').find(`div#${display_id} li.autocomplete_active`).length > 0) {
                            var sebelumnya = $('.form-group').find(`div#${display_id} li.autocomplete_active`).prev();
                            $('.form-group').find(`div#${display_id} li.autocomplete_active`).removeClass('autocomplete_active');
                            sebelumnya.addClass('autocomplete_active');
                        } else {
                            $('.form-group').find(`div#${display_id} li:first`).addClass('autocomplete_active');
                        }
                    }
                    else if (charCode === 13) {
                        on_selected($('.form-group').find(`div#${display_id} li.autocomplete_active`))
                    }
                    else {
                        search_ajax_fn($input.width(), s);
                    }
                }, 100)
            }
            else {
                $display.hide();
            }
        })

        $(document).on('click', `div#${display_id} .daftar-autocomplete li`, function () {
            on_selected($(this))
            $display.hide();
        })
    }

    const ajax_no_sep = (width, s) => {
        $('div#hasil_pencarian').hide()
        const Lebar = width + 25
        $.ajax({
            url: "<?= site_url('bpjs/prb/ajax_nomor_sep'); ?>",
            type: "POST",
            cache: false,
            data: 'keyword=' + s,
            dataType: 'json',
            success: function (json) {
                if (json.status === 1) {
                    $('div#hasil_pencarian').css({'width': Lebar + 'px'});
                    $('div#hasil_pencarian').show('fast');
                    $('div#hasil_pencarian').html(json.datanya);
                }
                if (json.status === 0) {
                    $('div#hasil_pencarian').html('');
                }
            },
            error: function (e) {
                console.log(e)
            }
        })
    }
</script>

<script>
    const refreshPoli = () => {
        $('#err-box').hide()
        $("#elementID").removeAttr('disabled')
        const nomor = $('#nomor_bjps').val()
        const tgl = $('#tgl_spri').val()

        if (!(nomor && nomor.length > 5 && tgl)) {
            return
        }

        $.ajax({
            url: "<?= site_url('bpjs/spri/get_poli'); ?>",
            type: "POST",
            cache: false,
            data: `nomor=${nomor}&tgl=${tgl}`,
            dataType: 'json',
            success: function (json) {
                if (json.status === 1) {
                    $('#poli_kontrol').empty()
                    json.datanya.forEach(v => {
                        $('#poli_kontrol').append(`<option value="${v.kodePoli}">${v.namaPoli}</option>`)
                    })
                    $('#poli_kontrol').change()
                }
                else if (json.status === 0) {
                    $('#err').html(json.server_output.metaData.message)
                    $('#err-box').show()
                    $("#elementID").prop("disabled", true)
                }
            },
            error: function (e) {
                console.log(e)
            }
        })
    }

    const refreshDokter = () => {
        $('#err-box').hide()
        $("#elementID").removeAttr('disabled')
        const poli_kontrol = $('#poli_kontrol').val()
        const tgl = $('#tgl_spri').val()

        if (!(poli_kontrol && tgl)) {
            return
        }

        $.ajax({
            url: "<?= site_url('bpjs/spri/get_dokter'); ?>",
            type: "POST",
            cache: false,
            data: `poli_kontrol=${poli_kontrol}&tgl=${tgl}`,
            dataType: 'json',
            success: function (json) {
                if (json.status === 1) {
                    $('#kode_dokter').empty()
                    json.datanya.forEach(v => {
                        $('#kode_dokter').append(`<option value="${v.kodeDokter}">${v.namaDokter}</option>`)
                    })
                }
                else if (json.status === 0) {
                    $('#err').html(json.server_output.metaData.message)
                    $('#err-box').show()
                    $("#elementID").prop("disabled", true)
                }
            },
            error: function (e) {
                console.log(e)
            }
        })
    }

    $(function () {
        $('#nomor_sep').change(refreshPoli)
        $('#tgl_spri').change(refreshPoli)
    })
    $(function () {
        $('#poli_kontrol').change(refreshDokter)
        $('#tgl_spri').change(refreshDokter)
    })
</script>