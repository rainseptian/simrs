<link rel="stylesheet" href="<?= base_url() ?>assets/plugins/iCheck/all.css">
<!--<link rel="stylesheet" href="--><?//= base_url() ?><!--assets/bower_components/select2/dist/css/select2.min.css">-->
<link rel="stylesheet" href="<?= base_url() ?>assets/plugins/autocomplete/style-gue.css">
<style media="screen">
    .select2-container {
        width: 100% !important;
    }
</style>

<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Laporan History Pelayanan Peserta
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?= base_url() ?>Dashbord"><i class="fa fa-dashboard"></i> Dashbord</a></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-danger">
                    <div class="box-header with-border">
                        <h3 class="box-title" style="padding-top: 6px">Laporan History Pelayanan Peserta</h3>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-12">
                                <div class="col-12 col-md-6 col-lg-4">
                                    <div class="form-group">
                                        <label class="control-label">Dari Tanggal</label>
                                        <div class="input-group date">
                                            <input type="date" class="form-control" id="tgl_start" value="<?=date('Y-m-d')?>" placeholder="yyyy-MM-dd" maxlength="10">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-md-6 col-lg-4">
                                    <div class="form-group">
                                        <label class="control-label">Sampai Tanggal</label>
                                        <div class="input-group date">
                                            <input type="date" class="form-control" id="tgl_end" value="<?=date('Y-m-d')?>" placeholder="yyyy-MM-dd" maxlength="10">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-md-6 col-lg-4">
                                    <div class="form-group">
                                        <label class="control-label">No Kartu</label>
                                        <div class="input-group date">
                                            <input type="text" class="form-control" id="no_kartu" placeholder="Nomor Kartu Peserta">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="box-footer">
                        <button type="button" id="b-cari" value="1" class="btn btn-primary">
                            Cari
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="box box-danger">
                    <div class="box-body">
                        <table class="table table-bordered" id="printable_table">
                            <thead>
                            <tr>
                                <td>No</td>
                                <td>Diagnosa</td>
                                <td>Jenis Pelayanan</td>
                                <td>Kelas Rawat</td>
                                <td>Nama Peserta</td>
                                <td>No Kartu</td>
                                <td>No SEP</td>
                                <td>No Rujukan</td>
                                <td>Poli</td>
                                <td>PPK Pelayanan</td>
                                <td>Tgl Pulang SEP</td>
                                <td>Tgl SEP</td>
                            </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<div id='ResponseInput'></div>

<script src="<?= base_url() ?>assets/plugins/input-mask/jquery.inputmask.js"></script>
<script src="<?= base_url() ?>assets/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="<?= base_url() ?>assets/plugins/input-mask/jquery.inputmask.extensions.js"></script>
<script src="<?= base_url() ?>assets/bower_components/select2/dist/js/select2.full.min.js"></script>
<script src="<?php echo base_url(); ?>assets/bower_components/toast-alert/toastr.js"></script>

<script>
    $(function () {
        $('#b-cari').click(function () {
            ajax_search()
        })
    })

    function format(n) {
        return n?.toString()?.replace(/\B(?=(\d{3})+(?!\d))/g, ".") ?? '0';
    }

    const ajax_search = () => {
        $.ajax({
            url: "<?= site_url('bpjs/laporan/ajax_laporan_history_pelayanan_peserta'); ?>",
            type: "POST",
            cache: false,
            data: `from=${$('#tgl_start').val()}&to=${$('#tgl_end').val()}&no_kartu=${$('#no_kartu').val()}`,
            dataType: 'json',
            success: function (json) {
                if (json.status === 1) {
                    toastr.success('Berhasil mendapatkan data')

                    $('#printable_table tbody').empty()
                    json.datanya.forEach((v, k) => {
                        $('#printable_table tbody').append(`
                            <tr>
                                <td>${k + 1}</td>
                                <td>${v.diagnosa}</td>
                                <td>${v.jnsPelayanan}</td>
                                <td>${v.kelasRawat}</td>
                                <td>${v.namaPeserta}</td>
                                <td>${v.noKartu}</td>
                                <td>${v.noSep}</td>
                                <td>${v.noRujukan ?? ''}</td>
                                <td>${v.poli}</td>
                                <td>${v.ppkPelayanan}</td>
                                <td>${v.tglPlgSep ?? ''}</td>
                                <td>${v.tglSep}</td>
                            </tr>
                        `)
                    })
                }
                if (json.status === 0) {
                    toastr.error(json.server_output.metaData.message ?? 'Gagal mendapatkan data')
                }
            }
        })
    }
</script>