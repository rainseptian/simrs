<link rel="stylesheet" href="<?= base_url() ?>assets/plugins/iCheck/all.css">
<!--<link rel="stylesheet" href="--><?//= base_url() ?><!--assets/bower_components/select2/dist/css/select2.min.css">-->
<link rel="stylesheet" href="<?= base_url() ?>assets/plugins/autocomplete/style-gue.css">
<style media="screen">
    .select2-container {
        width: 100% !important;
    }
</style>

<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Laporan Klaim Jaminan Jasa Raharja
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?= base_url() ?>Dashbord"><i class="fa fa-dashboard"></i> Dashbord</a></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-danger">
                    <div class="box-header with-border">
                        <h3 class="box-title" style="padding-top: 6px">Laporan Klaim Jaminan Jasa Raharja</h3>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-12 col-md-6 col-lg-4">
                                <div class="form-group">
                                    <label for="nama" class="control-label">Pelayanan</label>
                                    <br>
                                    <label style="font-weight: normal">
                                        <input type="radio" name="rc_kontrol" value="2" required checked> Rawat Jalan
                                    </label>
                                    <label style="font-weight: normal; margin-left: 8px">
                                        <input type="radio" name="rc_kontrol" value="1" required> Rawat Inap
                                    </label>
                                </div>
                            </div>
                            <div class="col-12 col-md-6 col-lg-4">
                                <div class="form-group">
                                    <label class="control-label">Dari Tanggal</label>
                                    <div class="input-group date">
                                        <input type="date" class="form-control" id="tgl_start" value="<?=date('Y-m-d')?>" placeholder="yyyy-MM-dd" maxlength="10">
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-md-6 col-lg-4">
                                <div class="form-group">
                                    <label class="control-label">Sampai Tanggal</label>
                                    <div class="input-group date">
                                        <input type="date" class="form-control" id="tgl_end" value="<?=date('Y-m-d')?>" placeholder="yyyy-MM-dd" maxlength="10">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="box-footer">
                        <button type="button" id="b-cari" value="1" class="btn btn-primary">
                            Cari
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="box box-danger">
                    <div class="box-body">
                        <table class="table table-bordered" id="printable_table">
                            <thead>
                            <tr>
                                <td>No</td>
                                <td>SEP</td>
                                <td>Jasa Raharja</td>
                            </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<div id='ResponseInput'></div>

<script src="<?= base_url() ?>assets/plugins/input-mask/jquery.inputmask.js"></script>
<script src="<?= base_url() ?>assets/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="<?= base_url() ?>assets/plugins/input-mask/jquery.inputmask.extensions.js"></script>
<script src="<?= base_url() ?>assets/bower_components/select2/dist/js/select2.full.min.js"></script>
<script src="<?php echo base_url(); ?>assets/bower_components/toast-alert/toastr.js"></script>

<script>
    $(function () {
        $('#b-cari').click(function () {
            ajax_search()
        })
    })

    function format(n) {
        return n?.toString()?.replace(/\B(?=(\d{3})+(?!\d))/g, ".") ?? '0';
    }

    const ajax_search = () => {
        $.ajax({
            url: "<?= site_url('bpjs/laporan/ajax_laporan_klaim_jaminan_jasa_raharja'); ?>",
            type: "POST",
            cache: false,
            data: `from=${$('#tgl_start').val()}&to=${$('#tgl_end').val()}&jns=${$("input[name='rc_kontrol']:checked").val()}`,
            dataType: 'json',
            success: function (json) {
                if (json.status === 1) {
                    toastr.success('Berhasil mendapatkan data')

                    $('#printable_table tbody').empty()
                    json.datanya.forEach((v, k) => {
                        $('#printable_table tbody').append(`
                            <tr>
                                <td>${k + 1}</td>
                                <td>
                                    <table class="tbl tbl-border">
                                        <tr>
                                            <td>Nama Peserta</td>
                                            <td>${v.sep.peserta.nama}</td>
                                        </tr>
                                        <tr>
                                            <td>No SEP</td>
                                            <td>${v.sep.noSEP}</td>
                                        </tr>
                                        <tr>
                                            <td>Tgl SEP</td>
                                            <td>${v.sep.tglSEP}</td>
                                        </tr>
                                        <tr>
                                            <td>Tgl Pulang SEP</td>
                                            <td>${v.sep.tglPlgSEP}</td>
                                        </tr>
                                        <tr>
                                            <td>No RM</td>
                                            <td>${v.sep.noMr}</td>
                                        </tr>
                                        <tr>
                                            <td>Jenis Pelayanan</td>
                                            <td>${+v.sep.jnsPelayanan === 1 ? 'Rawat Inap' : 'Rawat Jalan'}</td>
                                        </tr>
                                        <tr>
                                            <td>Poli</td>
                                            <td>${v.sep.poli}</td>
                                        </tr>
                                        <tr>
                                            <td>Diagnosa</td>
                                            <td>${v.sep.diagnosa}</td>
                                        </tr>
                                    </table>
                                </td>
                                <td>
                                    <table class="tbl tbl-border">
                                        <tr>
                                            <td>Tgl Kejadian</td>
                                            <td>${v.jasaRaharja.tglKejadian}</td>
                                        </tr>
                                        <tr>
                                            <td>No Register</td>
                                            <td>${v.jasaRaharja.noRegister}</td>
                                        </tr>
                                        <tr>
                                            <td>Status Dijamin</td>
                                            <td>${v.jasaRaharja.ketStatusDijamin}</td>
                                        </tr>
                                        <tr>
                                            <td>Status Dikirim</td>
                                            <td>${v.jasaRaharja.ketStatusDikirim}</td>
                                        </tr>
                                        <tr>
                                            <td>Biaya Dijamin</td>
                                            <td>Rp ${format(v.jasaRaharja.biayaDijamin)}</td>
                                        </tr>
                                        <tr>
                                            <td>Plafon</td>
                                            <td>Rp ${format(v.jasaRaharja.plafon)}</td>
                                        </tr>
                                        <tr>
                                            <td>Jumlah Dibayar</td>
                                            <td>Rp ${format(v.jasaRaharja.jmlDibayar)}</td>
                                        </tr>
                                        <tr>
                                            <td>Result</td>
                                            <td>${v.jasaRaharja.resultsJasaRaharja}</td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        `)
                    })
                }
                if (json.status === 0) {
                    toastr.error(json.server_output.metaData.message ?? 'Gagal mendapatkan data')
                }
            }
        })
    }
</script>