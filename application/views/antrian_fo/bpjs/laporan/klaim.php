<link rel="stylesheet" href="<?= base_url() ?>assets/plugins/iCheck/all.css">
<!--<link rel="stylesheet" href="--><?//= base_url() ?><!--assets/bower_components/select2/dist/css/select2.min.css">-->
<link rel="stylesheet" href="<?= base_url() ?>assets/plugins/autocomplete/style-gue.css">
<style media="screen">
    .select2-container {
        width: 100% !important;
    }
</style>

<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Laporan Klaim
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?= base_url() ?>Dashbord"><i class="fa fa-dashboard"></i> Dashbord</a></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-danger">
                    <div class="box-header with-border">
                        <h3 class="box-title" style="padding-top: 6px">Laporan Klaim</h3>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-12 col-md-6 col-lg-4">
                                <div class="form-group">
                                    <label for="nama" class="control-label">Pelayanan</label>
                                    <br>
                                    <label style="font-weight: normal">
                                        <input type="radio" name="rc_kontrol" value="2" required checked> Rawat Jalan
                                    </label>
                                    <label style="font-weight: normal; margin-left: 8px">
                                        <input type="radio" name="rc_kontrol" value="1" required> Rawat Inap
                                    </label>
                                </div>
                            </div>
                            <div class="col-12 col-md-6 col-lg-4">
                                <div class="form-group">
                                    <label class="control-label">Tanggal</label>
                                    <div class="input-group date">
                                        <input type="date" class="form-control" id="txtTgl1" placeholder="yyyy-MM-dd" maxlength="10">
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-md-6 col-lg-4">
                                <div class="form-group">
                                    <label for="nama" class="control-label">Status Klaim</label>
                                    <br>
                                    <label style="font-weight: normal">
                                        <input type="radio" name="status" value="1" required checked> Proses Verifikasi
                                    </label>
                                    <label style="font-weight: normal; margin-left: 8px">
                                        <input type="radio" name="status" value="2" required> Pending Verifikasi
                                    </label>
                                    <label style="font-weight: normal; margin-left: 8px">
                                        <input type="radio" name="status" value="3" required> Klaim
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="box-footer">
                        <button type="button" id="b-cari" value="1" class="btn btn-primary">
                            Cari
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="box box-danger">
                    <div class="box-body">
                        <table class="table table-bordered" id="printable_table">
                            <thead>
                            <tr>
                                <td>No</td>
                                <td>Inacbg</td>
                                <td>Biaya</td>
                                <td>Kelas Rawat</td>
                                <td>No SEP</td>
                                <td>Peserta</td>
                                <td>Poli</td>
                                <td>Tgl SEP</td>
                                <td>Tgl Pulang</td>
                                <td>Status</td>
                            </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<div id='ResponseInput'></div>

<script src="<?= base_url() ?>assets/plugins/input-mask/jquery.inputmask.js"></script>
<script src="<?= base_url() ?>assets/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="<?= base_url() ?>assets/plugins/input-mask/jquery.inputmask.extensions.js"></script>
<script src="<?= base_url() ?>assets/bower_components/select2/dist/js/select2.full.min.js"></script>
<script src="<?php echo base_url(); ?>assets/bower_components/toast-alert/toastr.js"></script>

<script>
    $(function () {
        $('#b-cari').click(function () {
            ajax_search()
        })
    })

    function format(n) {
        return n?.toString()?.replace(/\B(?=(\d{3})+(?!\d))/g, ".") ?? '0';
    }

    const ajax_search = () => {
        $.ajax({
            url: "<?= site_url('bpjs/laporan/ajax_laporan_klaim'); ?>",
            type: "POST",
            cache: false,
            data: `tgl=${$('#txtTgl1').val()}&jns=${$("input[name='rc_kontrol']:checked").val()}&status=${$("input[name='status']:checked").val()}`,
            dataType: 'json',
            success: function (json) {
                if (json.status === 1) {
                    toastr.success('Berhasil mendapatkan data')

                    $('#printable_table tbody').empty()
                    json.datanya.forEach((v, k) => {
                        $('#printable_table tbody').append(`
                            <tr>
                                <td>${k + 1}</td>
                                <td>${v.noSep}</td>
                                <td>
                                    ${v.Inacbg.nama}<br>
                                    <i>${v.Inacbg.kode}</i>
                                </td>
                                <td>
                                    Biaya Pengajuan: Rp ${format(v.biaya.byPengajuan)}<br>
                                    Biaya Disetujui: Rp ${format(v.biaya.bySetujui)}<br>
                                    Biaya Tarif Gruper: Rp ${format(v.biaya.byTarifGruper)}<br>
                                    Biaya Tarif RS: Rp ${format(v.biaya.byTarifRS)}<br>
                                    Biaya Topup: Rp ${format(v.biaya.byTopup)}
                                </td>
                                <td>${v.kelasRawat}</td>
                                <td>${v.noSEP}</td>
                                <td>
                                    Nama: ${v.peserta.nama}<br>
                                    No Kartu: ${v.peserta.noKartu}<br>
                                    No RM: ${v.peserta.noMr}<br>
                                </td>
                                <td>${v.poli}</td>
                                <td>${v.tglSep}</td>
                                <td>${v.tglPulang}</td>
                                <td>${v.status}</td>
                            </tr>
                        `)
                    })
                }
                if (json.status === 0) {
                    toastr.error(json.server_output.metaData.message ?? 'Gagal mendapatkan data')
                }
            }
        })
    }
</script>