<link rel="stylesheet" href="<?= base_url() ?>assets/plugins/iCheck/all.css">
<!--<link rel="stylesheet" href="--><?//= base_url() ?><!--assets/bower_components/select2/dist/css/select2.min.css">-->
<link rel="stylesheet" href="<?= base_url() ?>assets/plugins/autocomplete/style-gue.css">
<style media="screen">
    .select2-container {
        width: 100% !important;
    }
    .daftar-autocomplete {
        z-index: 100000000;
    }
    #hasil_pencarian_ppk_rujukan {
        z-index: 100000000;
    }
</style>

<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Pembuatan SEP
            <small>Surat Eligibilitas Peserta</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?= base_url() ?>Dashbord"><i class="fa fa-dashboard"></i> Dashbord</a></li>
            <li class="active">Pembuatan SEP</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-danger">
                    <div class="box-header with-border">
                        <h3 class="box-title" style="padding-top: 6px">Pembuatan SEP</h3>
                        <div class="pull-right col-md-3 col-sm-3 col-xs-12">
                            <div class="input-group">
                                <input type="text" class="form-control" id="txtNoSepCari" placeholder="ketik nomor sep" maxlength="19">
                                <span class="input-group-btn">
                                    <button type="button" id="btnCariNoSEP" class="btn btn-success">
                                        Cari SEP
                                    </button>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="box-body">
<!--                        <form class="form" method="post" action="--><?//= base_url() ?><!--Bpjs/create_sep">-->
                            <div class="box-body">
                                <div class="row">
                                    <div class="col-12 col-md-6 col-lg-4">
                                        <div class="form-group">
                                            <label for="nama" class="control-label">Pilih</label>
                                            <br>
                                            <label style="font-weight: normal">
                                                <input type="radio" name="jns_sep" value="1" required checked> Rujukan
                                            </label>
                                            <label style="font-weight: normal; margin-left: 8px">
                                                <input type="radio" name="jns_sep" value="2" required> Rujukan Manual / IGD
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12 col-md-6 col-lg-4" id="c-1">
                                        <form id="asw" method="post" action="<?=base_url()?>bpjs/sep/create_detail">
                                            <input type="hidden" name="rujukan" id="rujukan">
                                            <input type="hidden" name="jenis" value="1">
                                            <input type="hidden" name="jns_pelayanan" value="2">
                                            <div class="form-group">
                                                <label for="nama" class="control-label">Jenis Rujukan</label>
                                                <br>
                                                <label style="font-weight: normal">
                                                    <input type="radio" name="jns_rujukan" value="1" required checked> Standar
                                                </label>
                                                <label style="font-weight: normal; margin-left: 8px">
                                                    <input type="radio" name="jns_rujukan" value="2" required> Kontrol
                                                </label>
                                            </div>
                                            <div class="form-group">
                                                <label for="nama" class="control-label">Tgl SEP</label>
                                                <input type="date" class="form-control" name="tgl_sep" placeholder="Masukkan Tgl SEP" required value="<?=date('Y-m-d')?>">
                                            </div>
                                            <div class="form-group">
                                                <label for="nama" class="control-label">Asal Rujukan</label>
                                                <br>
                                                <label style="font-weight: normal">
                                                    <input type="radio" name="asal_rujukan" value="1" required checked> Faskes 1
                                                </label>
                                                <label style="font-weight: normal; margin-left: 8px">
                                                    <input type="radio" name="asal_rujukan" value="2" required> Faskes 2 (RS)
                                                </label>
                                            </div>
                                            <div class="form-group">
                                                <label for="nama" class="control-label">No Rujukan</label>
                                                <input type="text" class="form-control" name="no_rujukan" id="no_rujukan" placeholder="Masukkan No Rujukan" required>
                                            </div>
                                            <button type="button" class="btn btn-warning btn-sm" onclick="no_identitas()">No. Identitas</button>
                                        </form>
                                    </div>
                                    <div class="col-12 col-md-6 col-lg-4" id="c-2" style="display: none">
                                        <form id="f-2" method="post" action="<?=base_url()?>bpjs/sep/create_detail">
                                            <input type="hidden" name="peserta" id="peserta">
                                            <input type="hidden" name="jenis" value="2">
                                            <div class="form-group">
                                                <label for="nama" class="control-label">Jenis Pelayanan</label>
                                                <br>
                                                <label style="font-weight: normal">
                                                    <input type="radio" name="jns_pelayanan" value="1" required> Rawat Inap
                                                </label>
                                                <label style="font-weight: normal; margin-left: 8px">
                                                    <input type="radio" name="jns_pelayanan" value="2" required checked> Rawat Jalan
                                                </label>
                                            </div>
                                            <div class="form-group">
                                                <label for="nama" class="control-label">Tgl SEP</label>
                                                <input type="date" class="form-control" id="tgl_sep_" name="tgl_sep" placeholder="Masukkan Tgl SEP" required value="<?=date('Y-m-d')?>">
                                            </div>
                                            <div class="form-group">
                                                <label for="nama" class="control-label">Asal Rujukan</label>
                                                <br>
                                                <label style="font-weight: normal">
                                                    <input type="radio" name="asal_rujukan" value="1" required checked> Faskes 1
                                                </label>
                                                <label style="font-weight: normal; margin-left: 8px">
                                                    <input type="radio" name="asal_rujukan" value="2" required> Faskes 2 (RS)
                                                </label>
                                            </div>
                                            <div class="form-group">
                                                <label for="nama" class="control-label">PPK Asal Peserta</label>
                                                <input type="text" class="form-control" id="cari_ppk_rujukan"
                                                       name="cari_ppk_rujukan"
                                                       placeholder="Masukkan PPK Asal Peserta" required>
                                                <input type="hidden" name="ppk_rujukan" id='ppk_rujukan'>
                                                <div id='hasil_pencarian_ppk_rujukan' class='hasil_pencarian'></div>
                                            </div>
                                            <div class="form-group">
                                                <label for="nama" class="control-label">Nomor</label>
                                                <div class="input-group">
                                                    <input type="text" class="form-control" id="nomor" name="nomor" placeholder="Ketik Nomor">
                                                    <span class="input-group-addon">
                                                        <label><input type="radio" name="rbnomor" value="0" id="rbkartubpjs" checked=""> BPJS</label>
                                                        <label><input type="radio" name="rbnomor" value="1" id="rbkartunik"> NIK</label>
                                                    </span>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
<!--                        </form>-->
                    </div>
                    <div class="box-footer">
                        <button type="button" id="b-cari" value="1" class="btn btn-primary">
                            Cari
                        </button>
<!--                        <button type="button" id="b-cari-2" value="1" class="btn btn-primary">-->
<!--                            JANGAN DIKLIK YES-->
<!--                        </button>-->
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<div class="modal fade" data-backdrop="static" id="modal-no-id" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" style="font-size: 16px" id="modal-title">Rujukan Faskes</h5>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="nama" class="control-label">Nomor</label>
                            <div class="input-group">
                                <input type="text" class="form-control" id="n" name="n" placeholder="Ketik Nomor">
                                <span class="input-group-addon">
                                    <label><input type="radio" name="rbnomor2" value="0" id="rbkartubpjs" checked=""> BPJS</label>
                                    <label><input type="radio" name="rbnomor2" value="1" id="rbkartunik"> NIK</label>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <button style="margin-top: 24px" class="btn btn-warning" type="button" onclick="cari_by_no()">Cari</button>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <table class="table table-bordered" id="tbl-rujukan">
                            <thead>
                            <tr>
                                <th>No</th>
                                <th>No Rujukan</th>
                                <th>Tgl Rujukan</th>
                                <th>No Kartu</th>
                                <th>Nama</th>
                                <th>PPK Perujuk</th>
                                <th>Sub/Spesialis</th>
                                <th>Aksi</th>
                            </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
            </div>
        </div>
    </div>
</div>


<div id='ResponseInput'></div>

<script src="<?= base_url() ?>assets/plugins/input-mask/jquery.inputmask.js"></script>
<script src="<?= base_url() ?>assets/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="<?= base_url() ?>assets/plugins/input-mask/jquery.inputmask.extensions.js"></script>
<script src="<?= base_url() ?>assets/bower_components/select2/dist/js/select2.full.min.js"></script>
<script src="<?php echo base_url(); ?>assets/bower_components/toast-alert/toastr.js"></script>

<script>
    $(function () {
        $('#b-cari-2').click(function () {
            window.open('file:///System/Applications/TextEdit.app');
        })
    })
</script>

<script>
    $(function () {
        <?php $warning = $this->session->flashdata('warning');
        if (!empty($warning)) { ?>
            Swal.fire({
                icon: 'error',
                title: '<?=$warning?>',
            }).then((result) => {

            })
        <?php } ?>
        <?php $success = $this->session->flashdata('success');
        if (!empty($success)) { ?>
            const no_print = '<?=$this->session->flashdata('no-print')?>';
            Swal.fire({
                icon: 'success',
                title: '<?=$success?>',
                showCancelButton: true,
                confirmButtonText: no_print ? 'Oke' : 'Cetak SEP',
            }).then((result) => {
                if (result.isConfirmed && !no_print) {
                    const url = `<?php echo base_url(); ?>bpjs/sep/print_sep/<?=$this->session->flashdata('id')?>`
                    window.open(url, '_blank').focus();
                }
            })
        <?php } ?>
    })
</script>

<script>
    const set_search = ($input, search_ajax_fn, $display, on_selected) => {
        let timer = null
        const display_id = $display.attr('id')

        $input.on('keyup', function (e) {
            const s = $(this).val()
            if (s !== '') {
                clearTimeout(timer)
                timer = setTimeout(() => {
                    const charCode = ( e.which ) ? e.which : e.keyCode
                    if (charCode === 40) {
                        if ($('.form-group').find(`div#${display_id} li.autocomplete_active`).length > 0) {
                            var selanjutnya = $('.form-group').find(`div#${display_id} li.autocomplete_active`).next();
                            $('.form-group').find(`div#${display_id} li.autocomplete_active`).removeClass('autocomplete_active');
                            selanjutnya.addClass('autocomplete_active');
                        } else {
                            $('.form-group').find(`div#${display_id} li:first`).addClass('autocomplete_active');
                        }
                    }
                    else if (charCode === 38) {
                        if ($('.form-group').find(`div#${display_id} li.autocomplete_active`).length > 0) {
                            var sebelumnya = $('.form-group').find(`div#${display_id} li.autocomplete_active`).prev();
                            $('.form-group').find(`div#${display_id} li.autocomplete_active`).removeClass('autocomplete_active');
                            sebelumnya.addClass('autocomplete_active');
                        } else {
                            $('.form-group').find(`div#${display_id} li:first`).addClass('autocomplete_active');
                        }
                    }
                    else if (charCode === 13) {
                        on_selected($('.form-group').find(`div#${display_id} li.autocomplete_active`))
                    }
                    else {
                        search_ajax_fn($input.width(), s);
                    }
                }, 100)
            }
            else {
                $display.hide();
            }
        })

        $(document).on('click', `div#${display_id} .daftar-autocomplete li`, function () {
            on_selected($(this))
            $display.hide();
        })
    }
</script>

<script>
    $(function () {
        $('input[type=radio][name=jns_sep]').change(function() {
            if (this.value === '1') {
                $('#c-1').css('display', 'block')
                $('#c-2').css('display', 'none')
            }
            else {
                $('#c-1').css('display', 'none')
                $('#c-2').css('display', 'block')
            }
        })

        set_search(
            $('#cari_ppk_rujukan'),
            (width, keyword) => {
                if (keyword.length > 2) {
                    ajax_search_faskes_2(width, keyword)
                }
            },
            $('#hasil_pencarian_ppk_rujukan'),
            $li => {
                const kode = $li.find('span#kode').html();
                const nama = $li.find('span#nama').html();
                if (kode && nama) {
                    $('#cari_ppk_rujukan').val(kode + ' - ' + nama);
                    $('#ppk_rujukan').val(kode);
                } else {
                    alert('data tidak ada! Pilih pilihan yang ada sebelum di enter.');
                }
                $('div#hasil_pencarian_ppk_rujukan').hide();
            }
        )

        $('#b-cari').click(function () {
            if ($('input[type=radio][name=jns_sep]:checked').val() === '1') {
                const nmr = $('#no_rujukan').val()
                if (!nmr) {
                    toastr.error('Masukkan nomor rujukan dulu')
                    return
                }

                const onSuccess = (rujukan) => {
                    $('#rujukan').val(JSON.stringify(rujukan))
                    $('#asw').submit()
                }

                search_rujukan(nmr, onSuccess)
            }
            else {
                const nmr = $('#nomor').val()
                if (!nmr) {
                    toastr.error('Masukkan nomor dulu')
                    return
                }

                const rbnomor = $('input[type=radio][name=rbnomor]:checked').val()
                const onSuccess = (peserta) => {
                    $('#peserta').val(JSON.stringify(peserta))
                    $('#f-2').submit()
                }

                if (rbnomor === '0') {
                    search_by_bpjs(nmr, onSuccess)
                }
                else if (rbnomor === '1') {
                    search_by_nik(nmr, onSuccess)
                }
            }
        })
    })

    const search_rujukan = (nmr, f) => {
        $.ajax({
            url: "<?= site_url('Bpjs/ajax_search_rujukan'); ?>",
            type: "POST",
            cache: false,
            data: `keyword=${nmr}`,
            dataType: 'json',
            success: function (json) {
                if (json.status === 1) {
                    f(json.datanya)
                }
                else if (json.status === 0) {
                    toastr.error(json.server_output?.metaData?.message ?? 'Gagal mendapatkan data')
                }
            }
        })
    }

    const search_by_bpjs = (nmr, f) => {
        $.ajax({
            url: "<?= site_url('Bpjs/ajax_search_peserta_by_no_bpjs'); ?>",
            type: "POST",
            cache: false,
            data: `keyword=${nmr}&tgl=${$('#tgl_sep_').val()}`,
            dataType: 'json',
            success: function (json) {
                if (json.status === 1) {
                    if (+json.datanya.peserta.statusPeserta.kode === 0) {
                        is_fingerprint(json.datanya.peserta.noKartu, (b) => {
                            if (b) {
                                f(json.datanya.peserta)
                            }
                        })
                    }
                    else {
                        Swal.fire({
                            icon: 'error',
                            title: json.datanya.peserta.statusPeserta.keterangan ?? 'Pasien tidak aktif',
                        })
                    }
                }
                else if (json.status === 0) {
                    Swal.fire({
                        icon: 'error',
                        title: json.server_output?.metaData?.message ?? 'Gagal mendapatkan data',
                    })
                }
            }
        })
    }

    const search_by_nik = (nmr, f) => {
        $.ajax({
            url: "<?= site_url('Bpjs/ajax_search_peserta_by_nik'); ?>",
            type: "POST",
            cache: false,
            data: `keyword=${nmr}&tgl=${$('#tgl_sep_').val()}`,
            dataType: 'json',
            success: function (json) {
                if (json.status === 1) {
                    if (+json.datanya.peserta.statusPeserta.kode === 0) {
                        is_fingerprint(json.datanya.peserta.noKartu, (b) => {
                            if (b) {
                                f(json.datanya.peserta)
                            }
                        })
                    }
                    else {
                        Swal.fire({
                            icon: 'error',
                            title: json.datanya.peserta.statusPeserta.keterangan ?? 'Pasien tidak aktif',
                        })
                    }
                }
                else if (json.status === 0) {
                    Swal.fire({
                        icon: 'error',
                        title: json.server_output?.metaData?.message ?? 'Gagal mendapatkan data',
                    })
                }
            }
        })
    }

    const is_fingerprint = (nmr_bpjs, f) => {
        f(1)
        //$.ajax({
        //    url: "<?//= site_url('bpjs/ajax/raw'); ?>//",
        //    type: "POST",
        //    data: JSON.stringify({
        //        'method': 'GET',
        //        'url': `<?//=$base_url_vclaim?>//SEP/FingerPrint/Peserta/${nmr_bpjs}/TglPelayanan/<?//=date('Y-m-d')?>//`
        //    }),
        //    success: function (json) {
        //        const data = JSON.parse(json)
        //        if (data.success) {
        //            if (+data.data.kode === 0) {
        //                Swal.fire({
        //                    icon: 'error',
        //                    title: data.data.status ?? 'Belum fingerprint',
        //                    confirmButtonText: 'Buka App Fingerprint'
        //                }).then(r => {
        //                    if (r.isConfirmed) {
        //                        window.open('file:///C:"Buziol Games"/"Mario Forever"/"Mario Forever.exe"');
        //                    }
        //                    f(0)
        //                })
        //            }
        //            else {
        //                f(1)
        //            }
        //        }
        //        else {
        //            toastr.error(data.server_output.metaData.message ?? 'Gagal mendapatkan data')
        //        }
        //    },
        //    error: (e) => {
        //        toastr.error('Gagal mendapatkan data')
        //    }
        //})
    }

    const ajax_search_faskes_2 = (width, s) => {
        $('div#hasil_pencarian_ppk_rujukan').hide()
        const Lebar = width + 25
        $.ajax({
            url: "<?= site_url('Bpjs/ajax_search_faskes'); ?>",
            type: "POST",
            cache: false,
            data: 'keyword=' + s,
            dataType: 'json',
            success: function (json) {
                if (json.status === 1) {
                    $('div#hasil_pencarian_ppk_rujukan').css({'width': Lebar + 'px'});
                    $('div#hasil_pencarian_ppk_rujukan').show('fast');
                    $('div#hasil_pencarian_ppk_rujukan').html(json.datanya);
                }
                if (json.status === 0) {
                    $('div#hasil_pencarian_ppk_rujukan').html('');
                }
            }
        })
    }

</script>

<script>
    $(function () {
        $('#btnCariNoSEP').click(function () {
            const sep = $('#txtNoSepCari').val()
            if (sep) {
                window.open(`<?php echo base_url(); ?>bpjs/sep/detail/${sep}`,"_self")
            }
        })
    })

    function no_identitas() {
        $('#modal-no-id').modal('show')
    }

    let rujukan_list = []

    async function cari_by_no() {
        if (!$('#n').val()) {
            toastr.error('masukkan Nomor dulu')
            return
        }

        let no_bpjs = $('#n').val()
        if ($('input[name="rbnomor2"]:checked').val() === '1') { // 0: bpjs; 1: nik
            const res = await search_peserta_by_nik(no_bpjs)
            if (!res.success) {
                toastr.error(res.message)
                return
            }
            no_bpjs = res.data.noKartu
        }

        rujukan_list = []

        ajax_search_jadwal(no_bpjs, r => {
            if (r.success) {
                $('#tbl-rujukan tbody').empty()
                rujukan_list = r.data.rujukan
                r.data.rujukan.forEach((v, k) => {
                    $('#tbl-rujukan tbody').append(`
                        <tr>
                            <td>${k + 1}</td>
                            <td>${v.noKunjungan}</td>
                            <td>${v.tglKunjungan}</td>
                            <td>${v.peserta.noKartu}</td>
                            <td>${v.peserta.nama}</td>
                            <td>${v.provPerujuk.nama}</td>
                            <td>${v.poliRujukan.nama}</td>
                            <td>
                                <button class="btn btn-sm btn-success" onclick="select_no_rujukan('${v.noKunjungan}', ${k})">
                                    <i class="fa fa-check"></i>
                                </button>
                            </td>
                        </tr>
                    `)
                })
                $('#modal-pilih-rujukan').modal('show')
            }
            else {
                toastr.error(r.server_output?.metaData?.message ?? 'Gagal mendapatkan data')
            }
        })
    }

    const select_no_rujukan = (no, i) => {
        $('#no_rujukan').val(no)
        $('#modal-no-id').modal('hide')
        $('#n').val('')
        $('#tbl-rujukan tbody').empty()

        $('#rujukan').val(JSON.stringify({rujukan: rujukan_list[i]}))
        $('#asw').submit()
    }

    const search_peserta_by_nik = async (no) => {
        return new Promise(resolve => {
            $.ajax({
                url: "<?= site_url('Bpjs/ajax_search_peserta_by_nik'); ?>",
                type: "POST",
                cache: false,
                data: `keyword=${no.trim()}&tgl=<?=date('Y-m-d')?>`,
                dataType: 'json',
                success: function (json) {
                    if (json.status === 1) {
                        resolve({
                            success: true,
                            data: json.datanya.peserta
                        })
                    }
                    if (json.status === 0) {
                        resolve({
                            success: false,
                            message: json.server_output?.metaData?.message ?? 'Gagal mendapatkan data'
                        })
                    }
                }
            })
        })
    }

    const ajax_search_jadwal = (no_bpjs, f) => {
        $.ajax({
            url: "<?= site_url('bpjs/ajax/raw'); ?>",
            type: "POST",
            data: JSON.stringify({
                'method': 'GET',
                'url': `<?=$base_url_vclaim?>Rujukan/List/Peserta/${no_bpjs.trim()}`
            }),
            success: function (json) {
                f(JSON.parse(json))
            },
            error: (e) => {
                toastr.error('Gagal mendapatkan data')
            }
        })
    }

</script>