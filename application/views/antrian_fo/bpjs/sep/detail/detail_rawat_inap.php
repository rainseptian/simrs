<div class="row">
    <div class="col-12 col-md-6">
        <div class="form-group">
            <label for="nama" class="control-label">Asal Rujukan</label>
            <input type="text" class="form-control" name="asal_rujukan_txt" id="asal_rujukan_txt" value="Faskes Tingkat 2" readonly>
            <input type="hidden" name="asal_rujukan" value="2">
        </div>
        <div class="form-group">
            <label for="nama" class="control-label">PPK Asal Rujukan</label>
            <input type="text" class="form-control" name="ppk_asal_rujukan_txt" id="ppk_asal_rujukan_txt" value="KLINIK UTAMA SUKMA WIJAYA" readonly>
            <input type="hidden" name="ppk_rujukan" value="0207S001">
        </div>
        <div class="form-group">
            <label for="nama" class="control-label">Tgl Rujukan</label>
            <input type="date" class="form-control" id="tgl_rujukan" name="tgl_rujukan" required value="<?=$request->rujukan->tglRujukan?>">
        </div>
        <div class="form-group">
            <label for="nama" class="control-label">No Rujukan</label>
            <input type="text" class="form-control" name="no_rujukan" id="no_rujukan" placeholder="Masukkan No Rujukan" value="<?=$sep->noRujukan?>">
        </div>
        <div class="form-group">
            <label for="nama" class="control-label">No SPRI</label>
            <input type="text" class="form-control" name="no_spri" id="no_spri" placeholder="Masukkan No SPRI" value="<?=$sep->kontrol->noSurat?>" disabled>
        </div>
        <div class="form-group">
            <label for="nama" class="control-label">DPJP Pemberi Surat SKDP/SPRI</label>
            <input type="text" class="form-control" name="dpjp_pemberi_spri_txt" id="dpjp_pemberi_spri_txt" placeholder="Masukkan DPJP Pemberi Surat SKDP/SPRI" readonly value="<?=$sep->kontrol->nmDokter?>">
            <input type="hidden" name="dpjp_pemberi_spri" id="dpjp_pemberi_spri" value="<?=$sep->kontrol->kdDokter?>">
        </div>
        <div class="form-group">
            <label for="nama" class="control-label">Tgl SEP*</label>
            <input type="date" class="form-control" id="tgl_sep_" name="tgl_sep" readonly required value="<?=$sep->tglSep?>">
        </div>
        <div class="form-group">
            <label for="nama" class="control-label">No RM*</label>
            <div class="input-group">
                <input type="text" class="form-control editable" name="no_mr" id="no_mr" value="<?=$sep->peserta->noMr?>" placeholder="Masukkan No RM" required>
                <span class="input-group-addon">
                    <label><input type="checkbox" name="cob" id="cob" value="1" <?=$peserta->cob->noAsuransi ? 'checked' : ''?> disabled> COB</label>
                </span>
            </div>
        </div>
    </div>
    <div class="col-12 col-md-6">
        <div class="form-group">
            <label for="nama" class="control-label">Kelas rawat</label>
            <div class="input-group">
                <select class="form-control" name="kelas_rawat" id="kelas_rawat" disabled readonly>
                    <option value="1" <?=$sep->klsRawat->klsRawatHak == 1 ? 'selected' : ''?>>Kelas 1</option>
                    <option value="2" <?=$sep->klsRawat->klsRawatHak == 2 ? 'selected' : ''?>>Kelas 2</option>
                    <option value="3" <?=$sep->klsRawat->klsRawatHak == 3 ? 'selected' : ''?>>Kelas 3</option>
                </select>
                <span class="input-group-addon">
                    <label><input type="checkbox" class="editable" name="naik_kelas" id="naik_kelas" value="1" disabled> Naik kelas rawat inap</label>
                </span>
            </div>
        </div>
        <div id="div-naik-kelas" class="text-muted well well-sm no-shadow" style="display: none">
            <div class="form-group">
                <label for="nama" class="control-label">Kelas Rawat Inap</label>
                <select class="form-control" name="kelas_rawat_naik" id="kelas_rawat_naik">
                    <option value="">-- Silahkan Pilih --</option>
                    <option value="1">VVIP</option>
                    <option value="2">VIP</option>
                    <option value="3">Kelas 1</option>
                    <option value="4">Kelas 2</option>
                    <option value="5">Kelas 3</option>
                    <option value="6">ICCU</option>
                    <option value="7">ICU</option>
                </select>
            </div>
            <div class="form-group">
                <label for="nama" class="control-label">Pembiayaan</label>
                <select class="form-control" name="pembiayaan" id="pembiayaan">
                    <option value="">-- Silahkan Pilih --</option>
                    <option value="1">Pribadi</option>
                    <option value="2">Pemberi Kerja</option>
                    <option value="3">Asuransi Kesehatan Tambahan</option>
                </select>
            </div>
            <div class="form-group">
                <label for="nama" class="control-label">Nama Penanggungjawab</label>
                <input type="text" class="form-control" name="penanggungjawab" id="penanggungjawab" placeholder="Isi jika pembiayaan oleh 'Pemberi Kerja' atau 'Asuransi Kesehatan Tambahan'">
            </div>
        </div>
        <div class="form-group">
            <label for="nama" class="control-label">Diagnosa</label>
            <select class="form-control select2 editable" required
                    style="width: 100%;"
                    name="diag_awal"
                    id="diag_awal"
                    data-placeholder="Masukkan Diagnosa Awal ICD10">
            </select>
        </div>
        <div class="form-group">
            <label for="nama" class="control-label">No Telp</label>
            <input type="text" class="form-control editable" name="no_telp" id="no_telp" placeholder="Ketik no telp yang dapat dihubungi" required value="<?=$peserta->mr->noTelepon ?? ($pasien ? $pasien->telepon : '')?>">
        </div>
        <div class="form-group">
            <label for="nama" class="control-label">Catatan</label>
            <textarea class="form-control editable" name="catatan" id="catatan" placeholder="Ketik catatan apabila ada"></textarea>
        </div>
        <div class="form-group">
            <label for="nama" class="control-label">Katarak <input type="checkbox" class="editable" name="katarak" id="katarak" value="1"> </label>
            <input type="text" class="form-control" placeholder="Centang Katarak Jika Peserta Mendapat Surat Perintah Operasi katarak" readonly>
        </div>
        <div class="form-group">
            <label for="nama" class="control-label">Status Kecelakaan*</label>
            <select class="form-control laka_lantas editable" name="laka_lantas" id="laka_lantas">
                <option value="-">-- Silahkan Pilih --</option>
                <option value="0">Bukan Kecelakaan</option>
                <option value="1">Kecelakaan lalulintas dan bukan kecelakaan kerja</option>
                <option value="2">Kecelakaan lalulintas dan bukan kecelakaan kerja</option>
                <option value="3">Kecelakaan kerja</option>
            </select>
        </div>
        <?php $this->load->view('bpjs/sep/jaminan'); ?>
    </div>
</div>

<script>
    $(function () {
        $('#naik_kelas').change(function () {
            if ($(this).is(":checked")) {
                $('#div-naik-kelas').css('display', 'block')
            }
            else {
                $('#div-naik-kelas').css('display', 'none')
            }
        })

        $('#no_spri').keyup(function () {
            if ($(this).val().length === 19) {
                ajax_search_spri($(this).val(), r => {
                    if (r.jnsKontrol === '2') {
                        toastr.error('Bukan No SPRI')
                        return
                    }

                    toastr.success('No SPRI ditemukan')
                    $('#dpjp_pemberi_spri').val(r.kodeDokter)
                    $('#dpjp_pemberi_spri_txt').val(r.namaDokter)
                })
            }
        })
    })

    const ajax_search_spri = (v, f) => {
        $.ajax({
            url: "<?= site_url('Bpjs/ajax_search_surat_kontrol'); ?>",
            type: "POST",
            cache: false,
            data: 'keyword=' + v,
            dataType: 'json',
            success: function (json) {
                if (json.status === 1) {
                    f(json.datanya)
                }
                else {
                    toastr.error('Gagal mendapatkan data')
                }
            }
        })
    }

</script>