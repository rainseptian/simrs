<div class="row">
    <div class="col-12 col-md-6">
        <div class="form-group">
            <label for="nama" class="control-label">Spesialis/SubSpesialis*</label>
            <input type="text" class="form-control" id="nama_poli_tujuan" value="<?=$rujukan->poliRujukan->nama?>" readonly>
            <input type="hidden" name="poli_tujuan" id="poli_tujuan" value="<?=$rujukan->poliRujukan->kode?>">
            <input type="hidden" name="poli_eksekutif" value="0">
        </div>
        <div class="form-group">
            <label for="nama" class="control-label">DPJP yang Melayani*</label>
            <select type="text" class="form-control select2" id="dpjp_layan_select"
                    name="dpjp_layan_select"
                    placeholder="Masukkan Dokter DPJP" required>
                <option value="">--Pilih Dokter DPJP--</option>
                <?php foreach ($dpjp as $v) : ?>
                    <option value="<?=$v['kode']?>"><?=$v['nama']?></option>
                <?php endforeach; ?>
            </select>
            <input type="hidden" name="dpjp_layan" id="dpjp_layan" value="0">
        </div>
        <div class="form-group">
            <label for="nama" class="control-label">Asal Rujukan</label>
            <br>
            <label style="font-weight: normal">
                <input type="radio" name="asal_rujukan" value="1" required <?=$asalFaskes == 1 ? 'checked' : ''?> disabled> Faskes 1
            </label>
            <label style="font-weight: normal; margin-left: 8px">
                <input type="radio" name="asal_rujukan" value="2" required <?=$asalFaskes == 2 ? 'checked' : ''?> disabled> Faskes 2 (RS)
            </label>
        </div>
        <div class="form-group">
            <label for="nama" class="control-label">PPK Asal Rujukan</label>
            <input type="text" class="form-control" id="cari_ppk_rujukan"
                   name="cari_ppk_rujukan"
                   value="<?=$rujukan->provPerujuk->nama?>"
                   placeholder="Masukkan Faskes Rujukan" required readonly>
            <input type="hidden" name="ppk_rujukan" id='ppk_rujukan' value="<?=$rujukan->provPerujuk->kode?>">
        </div>
        <div class="form-group">
            <label for="nama" class="control-label">Tgl Rujukan</label>
            <input type="date" class="form-control" name="tgl_rujukan" id="tgl_rujukan" placeholder="Masukkan Tgl Rujukan" readonly value="<?=$rujukan->tglKunjungan?>">
        </div>
        <div class="form-group">
            <label for="nama" class="control-label">No Rujukan</label>
            <input type="text" class="form-control" name="no_rujukan" id="no_rujukan" placeholder="Masukkan No Rujukan" readonly value="<?=$rujukan->noKunjungan?>">
        </div>
        <div class="form-group">
            <label for="nama" class="control-label">No Surat SKDP</label>
            <input type="text" class="form-control" name="no_surat" placeholder="Masukkan No Surat SKDP">
        </div>
        <div class="form-group">
            <label for="nama" class="control-label">DPJP Pemberi Surat SKDP/SPRI</label>
            <select type="text" class="form-control select2" id="kode_dpjp" disabled
                    name="kode_dpjp"
                    placeholder="Masukkan Dokter DPJP">
                <option value="">--Pilih Dokter DPJP--</option>
                <?php foreach ($dpjp as $v) : ?>
                    <option value="<?=$v['kode']?>"><?=$v['nama']?></option>
                <?php endforeach; ?>
            </select>
        </div>
    </div>
    <div class="col-12 col-md-6">
        <div class="form-group">
            <label for="nama" class="control-label">Tgl SEP*</label>
            <input type="date" class="form-control" id="tgl_sep_" name="tgl_sep" readonly required value="<?=$tgl_sep?>">
        </div>
        <div class="form-group">
            <label for="nama" class="control-label">No RM*</label>
            <input type="text" class="form-control editable" name="no_mr" id="no_mr" value="<?=$peserta->mr->noMR?>" placeholder="Masukkan No RM" required>
        </div>
        <div class="form-group">
            <label for="nama" class="control-label">Diagnosa*</label>
            <select class="form-control select2 editable" required
                    style="width: 100%;"
                    name="diag_awal"
                    id="diag_awal"
                    data-placeholder="Masukkan Diagnosa Awal ICD10">
                <option value="<?=$rujukan->diagnosa->kode?>"><?=$rujukan->diagnosa->nama?></option>
            </select>
        </div>
        <div class="form-group">
            <label for="nama" class="control-label">No Telp*</label>
            <input type="text" class="form-control editable" name="no_telp" id="no_telp" placeholder="Ketik no telp yang dapat dihubungi" required value="<?=$peserta->mr->noTelepon?>">
        </div>
        <div class="form-group">
            <label for="nama" class="control-label">Catatan</label>
            <textarea class="form-control editable" name="catatan" id="catatan" placeholder="Ketik catatan apabila ada"></textarea>
        </div>
        <div class="form-group">
            <label for="nama" class="control-label">Status Kecelakaan*</label>
            <select class="form-control laka_lantas editable" name="laka_lantas" id="laka_lantas">
                <option value="-">-- Silahkan Pilih --</option>
                <option value="0">Bukan Kecelakaan</option>
                <option value="1">Kecelakaan lalulintas dan bukan kecelakaan kerja</option>
                <option value="2">Kecelakaan lalulintas dan bukan kecelakaan kerja</option>
                <option value="3">Kecelakaan kerja</option>
            </select>
        </div>
        <?php $this->load->view('bpjs/sep/jaminan'); ?>
    </div>
</div>