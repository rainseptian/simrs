<div class="row">
    <div class="col-12 col-md-6">
        <div class="form-group">
            <label for="nama" class="control-label">Spesialis/SubSpesialis*</label>
            <input type="text" class="form-control" value="Instalasi Gawat Darurat" readonly>
            <input type="hidden" name="poli_tujuan" value="IGD">
            <input type="hidden" name="poli_eksekutif" value="0">
        </div>
        <div class="form-group">
            <label for="nama" class="control-label">DPJP yang Melayani*</label>
            <select type="text" class="form-control select2 editable" id="dpjp_layan"
                    name="dpjp_layan"
                    placeholder="Masukkan Dokter DPJP" required>
                <option value="">--Pilih Dokter DPJP--</option>
                <?php foreach ($dpjp as $v) : ?>
                    <option value="<?=$v['kode']?>"><?=$v['nama']?></option>
                <?php endforeach; ?>
            </select>
        </div>
        <div class="form-group">
            <label for="nama" class="control-label">Tgl SEP*</label>
            <input type="date" class="form-control" id="tgl_sep_" name="tgl_sep" readonly required value="<?=$tgl_sep?>">
        </div>
        <div class="form-group">
            <label for="nama" class="control-label">No RM*</label>
            <input type="text" class="form-control editable" name="no_mr" id="no_mr" value="<?=$pasien ? $pasien->no_rm : ''?>" placeholder="Masukkan No RM" required>
        </div>
        <div class="form-group">
            <label for="nama" class="control-label">Diagnosa*</label>
            <select class="form-control select2 editable" required
                    style="width: 100%;"
                    name="diag_awal"
                    id="diag_awal"
                    data-placeholder="Masukkan Diagnosa Awal ICD10">
            </select>
        </div>
        <div class="form-group">
            <label for="nama" class="control-label">No Telp*</label>
            <input type="text" class="form-control editable" name="no_telp" id="no_telp" placeholder="Ketik no telp yang dapat dihubungi" required <?=$pasien ? $pasien->telepon : ''?>>
        </div>
    </div>
    <div class="col-12 col-md-6">
        <div class="form-group">
            <label for="nama" class="control-label">Catatan</label>
            <textarea class="form-control editable" name="catatan" id="catatan" placeholder="Ketik catatan apabila ada"></textarea>
        </div>
        <div class="form-group">
            <label for="nama" class="control-label">Status Kecelakaan*</label>
            <select class="form-control laka_lantas editable" name="laka_lantas" id="laka_lantas">
                <option value="-">-- Silahkan Pilih --</option>
                <option value="0">Bukan Kecelakaan</option>
                <option value="1">Kecelakaan lalulintas dan bukan kecelakaan kerja</option>
                <option value="2">Kecelakaan lalulintas dan bukan kecelakaan kerja</option>
                <option value="3">Kecelakaan kerja</option>
            </select>
        </div>
        <?php $this->load->view('bpjs/sep/jaminan'); ?>
    </div>
</div>