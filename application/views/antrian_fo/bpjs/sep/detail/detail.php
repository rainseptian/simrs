<link rel="stylesheet" href="<?= base_url() ?>assets/plugins/iCheck/all.css">
<!--<link rel="stylesheet" href="--><?//= base_url() ?><!--assets/bower_components/select2/dist/css/select2.min.css">-->
<link rel="stylesheet" href="<?= base_url() ?>assets/plugins/autocomplete/style-gue.css">
<style media="screen">
    .select2-container {
        width: 100% !important;
    }
    @media (min-width: 768px) {
        .modal-xl {
            width: 90%;
            max-width:1200px;
        }
    }
</style>

<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Detail SEP
            <small>Surat Eligibilitas Peserta</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?= base_url() ?>Dashbord"><i class="fa fa-dashboard"></i> Dashbord</a></li>
            <li class="active">Detail SEP</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-3">
                <!-- Profile Image -->

                <div class="box box-widget widget-user-2">
                    <div class="widget-user-header bg-success">
                        <div class="widget-user-image">
                            <img class="img-circle" src="<?=$peserta->sex == 'L' ? base_url().'assets/img/man.png' : base_url().'assets/img/woman.png'?>">
                        </div>
                        <h5 class="widget-user-username" id="lblnama" style="font-size: 18px; font-weight: bold;"><?=$peserta->nama?></h5>
                        <p class="widget-user-desc" id="lblnoka"><?=$peserta->noKartu?></p>
                        <input type="hidden" id="txtkelamin" value="L">
                        <input type="hidden" id="txtkdstatuspst" value="0">
                    </div>

                    <!-- /.box-body -->

                    <!-- /.box -->
                    <!-- About Me Box -->

                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="nav-tabs-custom">
                            <ul class="nav nav-tabs">
                                <li class="active"><a title="Profile Peserta" href="#tab_1" data-toggle="tab" aria-expanded="true"><span class="fa fa-user"></span></a></li>
                                <li class=""><a href="#tab_3" title="Histori" data-toggle="tab" id="tabHistori" aria-expanded="false"><span class="fa fa-list"></span></a></li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane active" id="tab_1">
                                    <ul class="list-group list-group-unbordered">
                                        <li class="list-group-item">
                                            <span class="fa fa-sort-numeric-asc"></span> <a title="NIK" class="pull-right-container" id="lblnik"><?=$peserta->nik?></a>
                                        </li>
                                        <li class="list-group-item">
                                            <span class="fa fa-calendar"></span> <a title="Tanggal Lahir" class="pull-right-container" id="lbltgllahir"><?=$peserta->tglLahir?></a>
                                        </li>
                                        <li class="list-group-item">
                                            <span class="fa fa-hospital-o"></span> <a title="Hak Kelas Rawat" class="pull-right-container" id="lblhakkelas"><?=$peserta->hakKelas->keterangan?></a>
                                            <input type="hidden" name="klsRawatHak" value="<?=$peserta->hakKelas->kode?>">
                                        </li>
                                        <li class="list-group-item">
                                            <span class="fa fa-stethoscope"></span>  <a title="Faskes Tingkat 1" class="pull-right-container" id="lblfktp"><?=$peserta->provUmum->kdProvider?> - <?=$peserta->provUmum->nmProvider?></a>
                                        </li>
                                        <li class="list-group-item">
                                            <span class="fa fa-calendar"></span>  <a title="TMT dan TAT Peserta" class="pull-right-container" id="lbltmt_tat"><?=$peserta->tglTMT?> s.d <?=$peserta->tglTAT?></a>
                                        </li>
                                        <li class="list-group-item">
                                            <span class="fa fa-calendar"></span>  <a title="Jenis Peserta" class="pull-right-container" id="lblpeserta"><?=$peserta->jenisPeserta->keterangan?></a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="tab-pane" id="tab_3">
                                    <div id="divHistori" class="list-group">
                                        <?php foreach($history as $v) : ?>
                                            <a href="<?=base_url('')?>bpjs/sep/detail/<?=$v->noSep?>" target="_blank" class="list-group-item">
                                                <h5 class="list-group-item-heading"><b><u><?=$v->noSep?></u></b></h5>
                                                <small>Rawat <?=$v->jnsPelayanan == 1 ? 'Inap' : 'Jalan'?></small>
                                                <p class="list-group-item-text"><small></small></p>
                                                <p class="list-group-item-text"><small><?=$v->poli?></small></p>
                                                <p class="list-group-item-text"><small>Tgl.SEP: <?=date('d-F-Y', strtotime($v->tglSep))?></small></p>
                                                <p class="list-group-item-text"><small><?=$v->noRujukan?></small></p>
                                                <p class="list-group-item-text"><small><?=$v->diagnosa?></small></p>
                                                <p class="list-group-item-text"><small><?=$v->ppkPelayanan?></small></p>
                                            </a>
                                        <?php endforeach; ?>
                                    </div>
                                    <div>
                                        <button type="button" id="btnHistori" class="btn btn-xs btn-default btn-block"><span class="fa fa-cubes"></span> Histori</button>
                                    </div>
                                </div>
                            </div>
                            <!-- /.tab-content -->
                        </div>
                        <div id="divriwayatKK" style="display: none;">
                            <button type="button" id="btnRiwayatKK" class="btn btn-danger btn-block"><span class="fa fa-th-list"></span> Pasien Memiliki Riwayat KLL/KK/PAK <br><i>(klik lihat data)</i></button>
                        </div>
                    </div>
                </div>

            </div>
            <div class="col-md-9">
                <form method="post" action="<?=base_url()?>bpjs/sep/update">
                    <input type="hidden" name="nomor_sep" value="<?=$sep->noSep?>">
                    <input type="hidden" name="nomor_kartu" value="<?=$peserta->noKartu?>">
                    <input type="hidden" name="jns_pelayanan" value="<?=$jenis_sep?>">
                    <input type="hidden" name="kelas_rawat_hak" value="<?=$peserta->hakKelas->kode?>">
                    <input type="hidden" name="asal_rujukan" value="<?=$asal_rujukan?>">
                    <input type="hidden" name="ppk_rujukan" value="<?=$ppk_rujukan?>">
                    <input type="hidden" name="id_pasien" value="<?=$pasien ? $pasien->id : 0?>">
                    <input type="hidden" name="type" value="<?=$bpjs_sep->type?>">

                    <div class="box box-danger">
                        <div class="box-header with-border">
                            <h3 class="box-title"><?=$jenis_sep?> - <?=$sep->noSep?></h3>
                        </div>
                        <?php if ($peserta->informasi->prolanisPRB || $peserta->cob->noAsuransi) : ?>
                            <div class="box-body">
                                <?php if ($peserta->informasi->prolanisPRB) : ?>
                                    <div class="callout callout-info">
                                        <p>
                                            <b>Peserta Prolanis PRB</b><br>
                                            Dinsos: <?=$peserta->informasi->dinsos?><br>
                                            No SKTM: <?=$peserta->informasi->noSKTM?><br>
                                            Prolanis PRB: <?=$peserta->informasi->prolanisPRB?><br>
                                        </p>
                                    </div>
                                <?php endif; ?>
                                <?php if ($peserta->cob->noAsuransi) : ?>
                                    <div class="callout callout-info">
                                        <p>
                                            <b>Peserta COB</b><br>
                                        </p>
                                    </div>
                                <?php endif; ?>
                            </div>
                        <?php endif; ?>
                        <div class="box-body">
                            <?php $this->load->view('bpjs/sep/detail/detail_'.$detail_view); ?>
                        </div>
                        <div class="box-footer">
                            <button type="button" id="b-edit" class="btn btn-warning wk"><i class="fa fa-pencil"></i> Edit</button>
                            <button type="button" id="b-delete" class="btn btn-danger wk"><i class="fa fa-trash"></i> Hapus</button>
                            <button type="button" id="b-cetak" class="btn btn-success wk"><i class="fa fa-print"></i> Cetak</button>
                            <button type="submit" id="b-simpan" class="btn btn-success" style="display: none"><i class="fa fa-save"></i> Simpan</button>

                            <button type="button" class="btn btn-danger pull-right" onclick="window.history.go(-1); return false;">
                                <i class="fa fa-close"></i> Batal
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </section>
</div>

<?php $this->load->view('bpjs/sep/modal_jaminan'); ?>
<?php $this->load->view('bpjs/sep/modal_history'); ?>
<div id='ResponseInput'></div>

<script src="<?= base_url() ?>assets/plugins/input-mask/jquery.inputmask.js"></script>
<script src="<?= base_url() ?>assets/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="<?= base_url() ?>assets/plugins/input-mask/jquery.inputmask.extensions.js"></script>
<script src="<?= base_url() ?>assets/bower_components/select2/dist/js/select2.full.min.js"></script>
<script src="<?php echo base_url(); ?>assets/bower_components/toast-alert/toastr.js"></script>

<script>
    const set_search = ($input, search_ajax_fn, $display, on_selected) => {
        let timer = null
        const display_id = $display.attr('id')

        $input.on('keyup', function (e) {
            const s = $(this).val()
            if (s !== '') {
                clearTimeout(timer)
                timer = setTimeout(() => {
                    const charCode = ( e.which ) ? e.which : e.keyCode
                    if (charCode === 40) {
                        if ($('.form-group').find(`div#${display_id} li.autocomplete_active`).length > 0) {
                            var selanjutnya = $('.form-group').find(`div#${display_id} li.autocomplete_active`).next();
                            $('.form-group').find(`div#${display_id} li.autocomplete_active`).removeClass('autocomplete_active');
                            selanjutnya.addClass('autocomplete_active');
                        } else {
                            $('.form-group').find(`div#${display_id} li:first`).addClass('autocomplete_active');
                        }
                    }
                    else if (charCode === 38) {
                        if ($('.form-group').find(`div#${display_id} li.autocomplete_active`).length > 0) {
                            var sebelumnya = $('.form-group').find(`div#${display_id} li.autocomplete_active`).prev();
                            $('.form-group').find(`div#${display_id} li.autocomplete_active`).removeClass('autocomplete_active');
                            sebelumnya.addClass('autocomplete_active');
                        } else {
                            $('.form-group').find(`div#${display_id} li:first`).addClass('autocomplete_active');
                        }
                    }
                    else if (charCode === 13) {
                        on_selected($('.form-group').find(`div#${display_id} li.autocomplete_active`))
                    }
                    else {
                        search_ajax_fn($input.width(), s);
                    }
                }, 100)
            }
            else {
                $display.hide();
            }
        })

        $(document).on('click', `div#${display_id} .daftar-autocomplete li`, function () {
            on_selected($(this))
            $display.hide();
        })
    }
</script>

<script>
    $('#diag_awal').select2({
        ajax: {
            url: "<?= site_url('Bpjs/ajax_search_diagnosa'); ?>",
            type: "POST",
            data: function (params) {
                return { keyword: params.term }
            },
            processResults: function (data) {
                return {
                    results: JSON.parse(data).datanya.map(v => ({id: v.kode, text: v.nama}))
                }
            }
        },
        minimumInputLength: 3
    })
</script>

<!--jaminan-->
<script>
    $(function () {
        $('.laka_lantas').change(function () {
            if ($(this).val() !== '-' && $(this).val() !== '0') {
                $('#modal-jaminan').modal('show')
            }
            else {
                $('#div-jaminan').css('display', 'none')
            }
        })

        $('#kd_propinsi').select2().change(function () {
            $('#kd_kabupaten').empty()
            ajax_search_kabupaten($(this).val(), (val) => {
                $('#kd_kecamatan').empty()
                ajax_search_kecamatan(val)
            })
        })

        $('#kd_kabupaten').select2().change(function () {
            $('#kd_kecamatan').empty()
            ajax_search_kecamatan($(this).val())
        })

        $('#kd_kecamatan').select2()
    })

    const onModalJaminanDismiss = (isNew) => {
        if (isNew) {
            $('#div-jaminan').css('display', 'block')
        }
        else {
            $('.laka_lantas').val('-').change()
        }
    }

    const ajax_search_kabupaten = (kode_provinsi, f = null) => {
        $.ajax({
            url: "<?= site_url('Bpjs/ajax_search_kabupaten'); ?>",
            type: "POST",
            cache: false,
            data: 'keyword=' + kode_provinsi,
            dataType: 'json',
            success: function (json) {
                if (json.status === 1) {
                    const data = json.datanya.map(v => ({id: v.kode, text: v.nama}))
                    $('#kd_kabupaten').select2({ data })
                    if (f) {
                        f($('#kd_kabupaten').val())
                    }
                }
            }
        })
    }

    const ajax_search_kecamatan = kode_kabupaten => {
        $.ajax({
            url: "<?= site_url('Bpjs/ajax_search_kecamatan'); ?>",
            type: "POST",
            cache: false,
            data: 'keyword=' + kode_kabupaten,
            dataType: 'json',
            success: function (json) {
                if (json.status === 1) {
                    const data = json.datanya.map(v => ({id: v.kode, text: v.nama}))
                    $('#kd_kecamatan').select2({ data })
                }
            }
        })
    }

</script>

<!--detail-->
<script>
    $(function () {
        const sep = <?=json_encode($sep)?>;
        const bpjs_sep = <?=json_encode($bpjs_sep)?>;
        const request = <?=json_encode($request)?>;
        const rujukan = <?=json_encode($rujukan)?>;

        try {
            if (bpjs_sep?.no_rujukan || sep.noRujukan) {
                $('#nama_poli_tujuan').val(sep.poli).prop('readonly', true)
                if (request) {
                    $('#poli_tujuan').val(request.poli.tujuan).prop('readonly', true)
                    $(`input[name=asal_rujukan][value=${request.rujukan.asalRujukan}]`).attr('checked', true)
                    $('#cari_ppk_rujukan').val(request.rujukan.ppkRujukan).prop('readonly', true)
                    $('#ppk_rujukan').val(request.rujukan.ppkRujukan).prop('readonly', true)
                    $('#tgl_rujukan').val(request.rujukan.tglRujukan).prop('readonly', true)
                    $('#no_rujukan').val(request.rujukan.noRujukan).prop('readonly', true)
                }
                else if (rujukan) {
                    $('#poli_tujuan').val(rujukan.rujukan.poliRujukan.nama).prop('readonly', true)
                    $(`input[name=asal_rujukan][value=${rujukan.asalFaskes}]`).attr('checked', true)
                    $('#cari_ppk_rujukan').val(`${rujukan.rujukan.provPerujuk.kode} - ${rujukan.rujukan.provPerujuk.nama}`).prop('readonly', true)
                    $('#ppk_rujukan').val(rujukan.rujukan.provPerujuk.kode).prop('readonly', true)
                    $('#tgl_rujukan').val(rujukan.rujukan.tglKunjungan).prop('readonly', true)
                    $('#no_rujukan').val(rujukan.rujukan.noKunjungan).prop('readonly', true)
                }
            }

            $('#dpjp_layan_select').val(sep.dpjp.kdDPJP).attr("disabled", true)
            $('#dpjp_layan').val(sep.dpjp.kdDPJP).attr("disabled", true)
            $('#tgl_sep_').val(sep.tglSep)
            $('#no_mr').val(sep.peserta.noMr).prop('readonly', true)
            if (request)
                $('#diag_awal').append(`<option value="${request.diagAwal}">${request.diagAwal} - ${sep.diagnosa}</option>`).val(request.diagAwal).attr("disabled", true)
            else
                $('#diag_awal').append(`<option value="${sep.diagnosa}">${sep.diagnosa}</option>`).val(sep.diagnosa).attr("disabled", true)
            $('#no_telp').val(request?.noTelp).prop('readonly', true)
            $('#catatan').val(sep.catatan).prop('readonly', true)
            $('#katarak').attr('checked', sep.katarak.katarak ? true : false).prop('disabled', true)
            $('#laka_lantas').val(sep.kdStatusKecelakaan).attr("disabled", true)
        }
        catch (e) {
            console.log(e)
        }

        $('#b-cetak').click(function () {
            const url = `<?php echo base_url(); ?>bpjs/sep/print_sep/${bpjs_sep?.id ?? 0}/${sep.noSep}`
            window.open(url, '_blank').focus();
        })
        $('#b-delete').click(function () {
            if (confirm('Yakin hapus SEP ini?')) {
                const url = `<?php echo base_url(); ?>bpjs/sep/delete_sep/${sep.noSep}`
                window.open(url, '_self')
            }
        })
        $('#b-edit').click(function () {
            $('.editable').prop('readonly', false).attr("disabled", false)
            $('#b-simpan').css('display', 'inline-block')
            $('.wk').css('display', 'none')
        })
    })
</script>

<script>
    $(function () {
        $('#btnHistori').click(function () {
            $('#modal-history').modal('show')
        })

        const sep = <?=json_encode($sep)?>;

        $('#b-cari-history').click(function () {
            $.ajax({
                url: "<?= site_url('bpjs/ajax/raw'); ?>",
                type: "POST",
                data: JSON.stringify({
                    'method': 'GET',
                    'url': `<?=$base_url_vclaim?>monitoring/HistoriPelayanan/NoKartu/${sep.peserta.noKartu}/tglMulai/${$('#history-start').val()}/tglAkhir/${$('#history-end').val()}`
                }),
                success: function (json) {
                    const data = JSON.parse(json)
                    if (data.success) {
                        $('#tbl-history tbody').empty()
                        data.data.histori.forEach((v, k) => {
                            $('#tbl-history tbody').append(`
                                <tr>
                                    <td>${k + 1}</td>
                                    <td>${v.tglSep}</td>
                                    <td>${v.noSep}</td>
                                    <td>${v.noRujukan}</td>
                                    <td>${v.ppkPelayanan}</td>
                                    <td>${v.diagnosa}</td>
                                    <td>${v.poli}</td>
                                    <td>Rawat ${+v.jnsPelayanan === 1 ? 'Inap' : 'Jalan'}</td>
                                    <td>${v.kelasRawat}</td>
                                </tr>
                            `)
                        })
                    }
                    else {
                        toastr.error(data.server_output.metaData.message ?? 'Gagal mendapatkan data')
                    }
                },
                error: (e) => {
                    toastr.error('Gagal mendapatkan data')
                }
            })
        })
    })
</script>