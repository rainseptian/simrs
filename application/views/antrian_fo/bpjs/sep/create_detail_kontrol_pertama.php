<div class="row">
    <div class="col-12 col-md-6">
        <input type="hidden" name="ppk_rujukan" value="<?=$ppk_rujukan?>">
        <div class="form-group">
            <label for="nama" class="control-label">Spesialis/SubSpesialis*</label>
            <select class="form-control select2" id="poli_tujuan"
                    name="poli_tujuan"
                    placeholder="Masukkan Spesialis/SubSpesialis" required>
                <option value="">--Pilih Spesialis/SubSpesialis--</option>
                <?php foreach($poli as $v) : ?>
                    <option value="<?=$v->kdpoli?>"><?=$v->nmpoli?> - <?=$v->nmsubspesialis?></option>
                <?php endforeach; ?>
            </select>
            <input type="hidden" name="poli_eksekutif" value="0">
        </div>
        <div class="form-group">
            <label for="nama" class="control-label">DPJP yang Melayani*</label>
            <select type="text" class="form-control select2" id="dpjp_layan"
                    name="dpjp_layan"
                    placeholder="Masukkan Dokter DPJP" required>
                <option value="">--Pilih DPJP--</option>
            </select>
        </div>
        <div class="form-group">
            <label for="nama" class="control-label">Asal Rujukan</label>
            <br>
            <label style="font-weight: normal">
                <input type="radio" name="asal_rujukan" value="1" required disabled> Faskes 1
            </label>
            <label style="font-weight: normal; margin-left: 8px">
                <input type="radio" name="asal_rujukan" value="2" required checked disabled> Faskes 2 (RS)
            </label>
        </div>
        <div class="form-group">
            <label for="nama" class="control-label">PPK Asal Rujukan</label>
            <input type="text" class="form-control"
                   name="cari_ppk_rujukan"
                   value="KLINIK UTAMA SUKMA WIJAYA"
                   placeholder="Masukkan Faskes Rujukan" required readonly>
            <input type="hidden" name="ppk_rujukan" id='ppk_rujukan' value="0207S001">
        </div>
        <div class="form-group">
            <label for="nama" class="control-label">Tgl Rujukan</label>
            <input type="date" class="form-control" name="tgl_rujukan" placeholder="Masukkan Tgl Rujukan" value="<?=date('Y-m-d')?>">
        </div>
        <div class="form-group">
            <label for="nama" class="control-label">No Rujukan</label>
            <input type="text" class="form-control" name="no_rujukan" placeholder="Masukkan No Rujukan" readonly value="<?=$no_rujukan?>">
        </div>
        <div class="form-group">
            <label for="nama" class="control-label">No Surat Kontrol/SKDP</label>
            <input type="text" class="form-control" name="no_skdp" id="no_skdp" placeholder="Masukkan No Surat SKDP" required>
        </div>
        <div class="form-group">
            <label for="nama" class="control-label">DPJP Pemberi Surat SKDP/SPRI</label>
            <input type="text" class="form-control" name="dpjp_pemberi_skdp_txt" id="dpjp_pemberi_skdp_txt" placeholder="Masukkan DPJP Pemberi Surat SKDP/SPRI" readonly>
            <input type="hidden" name="dpjp_pemberi_skdp" id="dpjp_pemberi_skdp">
        </div>
    </div>
    <div class="col-12 col-md-6">
        <div class="form-group">
            <label for="nama" class="control-label">Tgl SEP*</label>
            <input type="date" class="form-control" id="tgl_sep_" name="tgl_sep" readonly required value="<?=$tgl_sep?>">
        </div>
        <div class="form-group">
            <label for="nama" class="control-label">No RM*</label>
            <input type="text" class="form-control" name="no_mr" id="no_mr" value="<?=$pasien ? $pasien->no_rm : ''?>" placeholder="Masukkan No RM" required>
        </div>
        <div class="form-group">
            <label for="nama" class="control-label">Diagnosa*</label>
            <select class="form-control select2" required
                    style="width: 100%;"
                    name="diag_awal"
                    id="diag_awal"
                    data-placeholder="Masukkan Diagnosa Awal ICD10">
            </select>
        </div>
        <div class="form-group">
            <label for="nama" class="control-label">No Telp*</label>
            <input type="text" class="form-control" name="no_telp" id="no_telp" placeholder="Ketik no telp yang dapat dihubungi" required <?=$pasien ? $pasien->telepon : ''?>>
        </div>
        <div class="form-group">
            <label for="nama" class="control-label">Catatan</label>
            <textarea class="form-control" name="catatan" id="catatan" placeholder="Ketik catatan apabila ada"></textarea>
        </div>
        <div class="form-group">
            <label for="nama" class="control-label">Status Kecelakaan*</label>
            <select class="form-control laka_lantas" name="laka_lantas" id="laka_lantas">
                <option value="-">-- Silahkan Pilih --</option>
                <option value="0">Bukan Kecelakaan</option>
                <option value="1">Kecelakaan lalulintas dan bukan kecelakaan kerja</option>
                <option value="2">Kecelakaan lalulintas dan bukan kecelakaan kerja</option>
                <option value="3">Kecelakaan kerja</option>
            </select>
        </div>
        <?php $this->load->view('bpjs/sep/jaminan'); ?>
    </div>
</div>
<script>
    $(function () {
        $('.select2').select2()

        $('#poli_tujuan').change(function () {
            let pad = function(num) { return ('00'+num).slice(-2) };
            let date = new Date();
            date = date.getUTCFullYear() + '-' + pad(date.getUTCMonth() + 1)  + '-' + pad(date.getUTCDate())

            $.ajax({
                url: "<?= site_url('bpjs/ajax/raw'); ?>",
                type: "POST",
                data: JSON.stringify({
                    'method': 'GET',
                    'url': `<?=$base_url_vclaim?>referensi/dokter/pelayanan/2/tglPelayanan/${date}/Spesialis/${$('#poli_tujuan').val()}`
                }),
                success: function (json) {
                    const data = JSON.parse(json)
                    if (data.success) {
                        $('#dpjp_layan').find('option').remove()
                        $('#dpjp_layan').append(`<option value="">--Pilih DPJP--</option>`)
                        data.data.list.forEach(v => {
                            $('#dpjp_layan').append(`<option value="${v.kode}">${v.nama}</option>`)
                        })
                    }
                    else {
                        toastr.error(data.server_output.metaData.message ?? 'Gagal mendapatkan data')
                    }
                },
                error: (e) => {
                    toastr.error('Gagal mendapatkan data')
                }
            })
        })

        $('#no_skdp').on('change keyup', function () {
            $.ajax({
                url: "<?= site_url('bpjs/ajax/raw'); ?>",
                type: "POST",
                data: JSON.stringify({
                    'method': 'GET',
                    'url': `<?=$base_url_vclaim?>RencanaKontrol/noSuratKontrol/${$('#no_skdp').val().trim()}`
                }),
                success: function (json) {
                    const data = JSON.parse(json)
                    if (data.success) {
                        $('#dpjp_pemberi_skdp_txt').val(data.data.namaDokter)
                        $('#dpjp_pemberi_skdp').val(data.data.kodeDokter)
                    }
                    else {
                        toastr.error(data.server_output.metaData.message ?? 'Gagal mendapatkan data')
                    }
                },
                error: (e) => {
                    toastr.error('Gagal mendapatkan data')
                }
            })
        })

        $('#diag_awal').select2({
            ajax: {
                url: "<?= site_url('Bpjs/ajax_search_diagnosa'); ?>",
                type: "POST",
                data: function (params) {
                    return { keyword: params.term }
                },
                processResults: function (data) {
                    return {
                        results: JSON.parse(data).datanya.map(v => ({id: v.kode, text: v.nama}))
                    }
                }
            },
            minimumInputLength: 3
        })
    })
</script>