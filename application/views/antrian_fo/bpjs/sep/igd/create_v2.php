<link rel="stylesheet" href="<?= base_url() ?>assets/plugins/iCheck/all.css">
<!--<link rel="stylesheet" href="--><?//= base_url() ?><!--assets/bower_components/select2/dist/css/select2.min.css">-->
<link rel="stylesheet" href="<?= base_url() ?>assets/plugins/autocomplete/style-gue.css">
<style media="screen">
    .select2-container {
        width: 100% !important;
    }
</style>

<div class="content-wrapper">
    <section class="content-header">
        <h1>
            SEP IGD
            <small>Preview</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?= base_url() ?>Dashbord"><i class="fa fa-dashboard"></i> Dashbord</a></li>
            <li class="active">Tambah SEP IGD</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-danger">
                    <div class="box-header">
                        <h3 class="box-title">Tambah SEP IGD</h3>
                    </div>
                    <?php $warning = $this->session->flashdata('warning');
                    if (!empty($warning)) { ?>
                        <div class="alert alert-warning alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <h4><i class="icon fa fa-warning"></i> Alert!</h4>
                            <?= $warning ?>
                        </div>
                    <?php } ?>
                    <?php $success = $this->session->flashdata('success');
                    if (!empty($success)) { ?>
                        <div class="alert alert-success alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <h4><i class="icon fa fa-check"></i> Success!</h4>
                            <?= $success ?>
                        </div>
                    <?php } ?>
                    <div class="box-body">
                        <form class="form" method="post" action="<?= base_url() ?>bpjs/sep/igd/create">
                            <div class="box-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="nama" class="control-label">Nomor Kartu</label>
                                            <input type="text" class="form-control" id="cari_nomor_kartu"
                                                   name="cari_nomor_kartu"
                                                   placeholder="Masukkan nama pasien / nomor kartu BPJS" required>
                                            <input type="hidden" name="nomor_kartu" id='nomor_kartu'>
                                            <input type="hidden" name="id_pasien" id='id_pasien'>
                                            <div id='hasil_pencarian' class='hasil_pencarian'></div>
                                        </div>
                                        <div class="form-group">
                                            <label for="nama" class="control-label">Tgl SEP</label>
                                            <input type="date" class="form-control" name="tgl_sep" id="tgl_sep" placeholder="Masukkan Tgl SEP" required>
                                        </div>
                                        <div class="form-group">
                                            <label for="nama" class="control-label">Faskes Pemberi Pelayanan</label>
                                            <input type="text" class="form-control" id="cari_ppk_pelayanan"
                                                   name="cari_ppk_pelayanan"
                                                   placeholder="Masukkan Faskes Pemberi Pelayanan" required>
                                            <input type="hidden" name="ppk_pelayanan" id='ppk_pelayanan'>
                                            <div id='hasil_pencarian_ppk_pelayanan' class='hasil_pencarian'></div>
                                        </div>
                                        <div class="form-group">
                                            <label for="nama" class="control-label">Jenis Pelayanan</label>
                                            <br>
                                            <label style="font-weight: normal">
                                                <input type="radio" name="jns_pelayanan" value="1" required> Rawat Inap
                                            </label>
                                            <label style="font-weight: normal; margin-left: 8px">
                                                <input type="radio" name="jns_pelayanan" value="2" required checked> Rawat Jalan
                                            </label>
                                        </div>
                                        <hr>
                                        <div class="form-group">
                                            <label for="nama" class="control-label">No RM</label>
                                            <input type="text" class="form-control" name="no_mr" id="no_mr" placeholder="Masukkan No RM">
                                        </div>
                                        <div class="form-group">
                                            <label for="nama" class="control-label">Asal Rujukan</label>
                                            <br>
                                            <label style="font-weight: normal">
                                                <input type="radio" name="asal_rujukan" value="1" required> Faskes 1
                                            </label>
                                            <label style="font-weight: normal; margin-left: 8px">
                                                <input type="radio" name="asal_rujukan" value="2" required> Faskes 2 (RS)
                                            </label>
                                        </div>
                                        <div class="form-group">
                                            <label for="nama" class="control-label">Tgl Rujukan</label>
                                            <input type="date" class="form-control" name="tgl_rujukan" placeholder="Masukkan Tgl Rujukan">
                                        </div>
                                        <div class="form-group">
                                            <label for="nama" class="control-label">No Rujukan</label>
                                            <input type="text" class="form-control" name="no_rujukan" placeholder="Masukkan No Rujukan">
                                        </div>
                                        <div class="form-group">
                                            <label for="nama" class="control-label">Faskes Rujukan</label>
                                            <input type="text" class="form-control" id="cari_ppk_rujukan"
                                                   name="cari_ppk_rujukan"
                                                   placeholder="Masukkan Faskes Rujukan">
                                            <input type="hidden" name="ppk_rujukan" id='ppk_rujukan'>
                                            <div id='hasil_pencarian_ppk_rujukan' class='hasil_pencarian'></div>
                                        </div>
                                        <hr>
                                        <div class="form-group">
                                            <label for="nama" class="control-label">Dokter DPJP</label>
                                            <select type="text" class="form-control select2" id="kode_dpjp" name="kode_dpjp" required>
                                                <option value="">--Pilih Dokter DPJP--</option>
                                                <?php foreach ($dpjp as $v) : ?>
                                                    <option value="<?=$v['kode']?>"><?=$v['nama']?></option>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="nama" class="control-label">Diagnosa Awal ICD10</label>
                                            <select class="form-control select2" required
                                                    style="width: 100%;"
                                                    name="diag_awal"
                                                    id="diag_awal"
                                                    data-placeholder="Masukkan Diagnosa Awal ICD10">
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="nama" class="control-label">Poli Tujuan</label>
                                            <select class="form-control select2" required
                                                    style="width: 100%;"
                                                    name="poli_tujuan"
                                                    id="poli_tujuan"
                                                    data-placeholder="Masukkan Poli Tujuan">
                                                <option value="<?=$req->poli->tujuan?>" selected><?=$req->poli->tujuan?></option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="nama" class="control-label">Poli Eksekutif</label>
                                            <br>
                                            <label style="font-weight: normal">
                                                <input type="radio" name="poli_eksekutif" value="0" required <?=$req->rujukan->eksekutif == 0 ? 'checked' : ''?>> Tidak
                                            </label>
                                            <label style="font-weight: normal; margin-left: 8px">
                                                <input type="radio" name="poli_eksekutif" value="1" required <?=$req->rujukan->eksekutif == 1 ? 'checked' : ''?>> Iya
                                            </label>
                                        </div>
                                        <div class="form-group">
                                            <label for="nama" class="control-label">Catatan</label>
                                            <input type="text" class="form-control" name="catatan" placeholder="Masukkan Catatan">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="nama" class="control-label">Laka Lantas</label>
                                            <select class="form-control" name="laka_lantas" id="laka_lantas">
                                                <option value="0">Bukan Kecelakaan lalu lintas [BKLL]</option>
                                                <option value="1">KLL dan bukan kecelakaan Kerja [BKK]</option>
                                                <option value="2">KLL dan KK</option>
                                                <option value="3">KK</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="nama" class="control-label">Tgl Kejadian KLL</label>
                                            <input type="date" class="form-control" name="tgl_kejadian">
                                        </div>
                                        <div class="form-group">
                                            <label for="nama" class="control-label">Keterangan</label>
                                            <input type="text" class="form-control" name="keterangan" placeholder="Masukkan Keterangan">
                                        </div>
                                        <div class="form-group">
                                            <label for="nama" class="control-label">Suplesi</label>
                                            <br>
                                            <label style="font-weight: normal">
                                                <input type="radio" name="suplesi" value="0" required checked> Tidak
                                            </label>
                                            <label style="font-weight: normal; margin-left: 8px">
                                                <input type="radio" name="suplesi" value="1" required> Iya
                                            </label>
                                        </div>
                                        <div class="form-group">
                                            <label for="nama" class="control-label">No SEP Suplesi (diisi jika terdapat suplesi)</label>
                                            <input type="text" class="form-control" name="no_sep_suplesi" placeholder="Masukkan No SEP Suplesi">
                                        </div>
                                        <div class="form-group">
                                            <label for="nama" class="control-label">Provinsi Laka</label>
                                            <select class="form-control select2"
                                                    style="width: 100%;"
                                                    name="kd_propinsi"
                                                    id="kd_propinsi"
                                                    data-placeholder="Pilih Provinsi Laka">
                                                <option value="">--Pilih Provinsi Laka--</option>
                                                <?php foreach ($provinsi as $v) : ?>
                                                    <option value="<?=$v->kode?>"><?=$v->nama?></option>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="nama" class="control-label">Kabupaten Laka</label>
                                            <select class="form-control select2"
                                                    style="width: 100%;"
                                                    name="kd_kabupaten"
                                                    id="kd_kabupaten"
                                                    data-placeholder="Pilih Kabupaten Laka">
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="nama" class="control-label">Kecamatan Laka</label>
                                            <select class="form-control select2"
                                                    style="width: 100%;"
                                                    name="kd_kecamatan"
                                                    id="kd_kecamatan"
                                                    data-placeholder="Pilih Kecamatan Laka">
                                            </select>
                                        </div>
                                        <hr>
                                        <div class="form-group">
                                            <label for="nama" class="control-label">DPJP Layanan (tidak diisi jika jenis pelayanan adalah Ranap)</label>
                                            <select type="text" class="form-control select2" id="dpjp_layan" name="dpjp_layan">
                                                <option value="">--Pilih DPJP Layanan--</option>
                                                <?php foreach ($dpjp as $v) : ?>
                                                    <option value="<?=$v['kode']?>"><?=$v['nama']?></option>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="nama" class="control-label">No Telp</label>
                                            <input type="number" class="form-control" id="no_telp" name="no_telp" placeholder="Masukkan No Telp" required>
                                        </div>
                                        <div class="form-group">
                                            <label for="nama" class="control-label">Nama User (pembuat SEP)</label>
                                            <input type="text" class="form-control" name="user" placeholder="Masukkan Nama User" value="<?=$nama?>" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="box-footer">
                                    <button type="submit" name="submit" value="1"
                                            class="btn btn-primary btn-lg btn-flat pull-right">Simpan
                                    </button>
                                    <a href="javascript:history.back()"
                                       class="btn btn-default btn-lg btn-flat pull-right">Batal</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>


<div id='ResponseInput'></div>
<!-- InputMask -->
<script src="<?= base_url() ?>assets/plugins/input-mask/jquery.inputmask.js"></script>
<script src="<?= base_url() ?>assets/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="<?= base_url() ?>assets/plugins/input-mask/jquery.inputmask.extensions.js"></script>
<script src="<?= base_url() ?>assets/bower_components/select2/dist/js/select2.full.min.js"></script>

<script>
    $(function () {
        $('#kd_propinsi').select2().change(function () {
            $('#kd_kabupaten').empty()
            ajax_search_kabupaten($(this).val())
        })

        $('#kd_kabupaten').select2().change(function () {
            $('#kd_kecamatan').empty()
            ajax_search_kecamatan($(this).val())
        })

        $('#kd_kecamatan').select2()

        $('#diag_awal').select2({
            ajax: {
                url: "<?= site_url('Bpjs/ajax_search_diagnosa'); ?>",
                type: "POST",
                data: function (params) {
                    return { keyword: params.term }
                },
                processResults: function (data) {
                    return {
                        results: JSON.parse(data).datanya.map(v => ({id: v.kode, text: v.nama}))
                    }
                }
            },
            minimumInputLength: 3
        })

        $('#poli_tujuan').select2({
            ajax: {
                url: "<?= site_url('Bpjs/ajax_search_poli'); ?>",
                type: "POST",
                data: function (params) {
                    return { keyword: params.term }
                },
                processResults: function (data) {
                    return {
                        results: JSON.parse(data).datanya.map(v => ({id: v.kode, text: v.nama}))
                    }
                }
            },
            minimumInputLength: 3
        })

        $('#pembiayaan').change(function () {
            if ($(this).val()) {
                const txt = $("#pembiayaan option:selected").text()
                $('#penanggung_jawab').val(txt)
            }
            else {
                $('#penanggung_jawab').val('')
            }
        })

    })

    const ajax_search_kabupaten = kode_provinsi => {
        $.ajax({
            url: "<?= site_url('Bpjs/ajax_search_kabupaten'); ?>",
            type: "POST",
            cache: false,
            data: 'keyword=' + kode_provinsi,
            dataType: 'json',
            success: function (json) {
                if (json.status === 1) {
                    const data = json.datanya.map(v => ({id: v.kode, text: v.nama}))
                    $('#kd_kabupaten').select2({ data })
                }
            }
        })
    }

    const ajax_search_kecamatan = kode_kabupaten => {
        $.ajax({
            url: "<?= site_url('Bpjs/ajax_search_kecamatan'); ?>",
            type: "POST",
            cache: false,
            data: 'keyword=' + kode_kabupaten,
            dataType: 'json',
            success: function (json) {
                if (json.status === 1) {
                    const data = json.datanya.map(v => ({id: v.kode, text: v.nama}))
                    $('#kd_kecamatan').select2({ data })
                }
            }
        })
    }

</script>

<script>

    $(function () {
        set_search(
            $('#cari_nomor_kartu'),
            (width, keyword) => { ajax_no_bpjs(width, keyword) },
            $('#hasil_pencarian'),
            $li => {
                const pasien_id = $li.find('span#pasien_id').html();
                const telepon = $li.find('span#telepon').html();
                const no_rm = $li.find('span#no_rm').html();
                const nama = $li.find('span#nama').html();
                const no_jaminan = $li.find('span#no_jaminan').html();
                if (no_jaminan) {
                    $('#cari_nomor_kartu').val(no_jaminan + ' - ' + nama);
                    $('#id_pasien').val(pasien_id);
                    $('#nomor_kartu').val(no_jaminan);
                    $('#no_mr').val(no_rm)
                    $('#no_telp').val(telepon)
                } else {
                    alert('data tidak ada! Pilih pilihan yang ada sebelum di enter.');
                }
                $('div#hasil_pencarian').hide();
            }
        )

        set_search(
            $('#cari_ppk_pelayanan'),
            (width, keyword) => {
                if (keyword.length > 2) {
                    ajax_search_faskes(width, keyword)
                }
            },
            $('#hasil_pencarian_ppk_pelayanan'),
            $li => {
                const kode = $li.find('span#kode').html();
                const nama = $li.find('span#nama').html();
                if (kode && nama) {
                    $('#cari_ppk_pelayanan').val(kode + ' - ' + nama);
                    $('#ppk_pelayanan').val(kode);
                } else {
                    alert('data tidak ada! Pilih pilihan yang ada sebelum di enter.');
                }
                $('div#hasil_pencarian_ppk_pelayanan').hide();
            }
        )

        set_search(
            $('#cari_ppk_rujukan'),
            (width, keyword) => {
                if (keyword.length > 2) {
                    ajax_search_faskes_2(width, keyword)
                }
            },
            $('#hasil_pencarian_ppk_rujukan'),
            $li => {
                const kode = $li.find('span#kode').html();
                const nama = $li.find('span#nama').html();
                if (kode && nama) {
                    $('#cari_ppk_rujukan').val(kode + ' - ' + nama);
                    $('#ppk_rujukan').val(kode);
                } else {
                    alert('data tidak ada! Pilih pilihan yang ada sebelum di enter.');
                }
                $('div#hasil_pencarian_ppk_rujukan').hide();
            }
        )
    })

    const set_search = ($input, search_ajax_fn, $display, on_selected) => {
        let timer = null
        const display_id = $display.attr('id')

        $input.on('keyup', function (e) {
            const s = $(this).val()
            if (s !== '') {
                clearTimeout(timer)
                timer = setTimeout(() => {
                    const charCode = ( e.which ) ? e.which : e.keyCode
                    if (charCode === 40) {
                        if ($('.form-group').find(`div#${display_id} li.autocomplete_active`).length > 0) {
                            var selanjutnya = $('.form-group').find(`div#${display_id} li.autocomplete_active`).next();
                            $('.form-group').find(`div#${display_id} li.autocomplete_active`).removeClass('autocomplete_active');
                            selanjutnya.addClass('autocomplete_active');
                        } else {
                            $('.form-group').find(`div#${display_id} li:first`).addClass('autocomplete_active');
                        }
                    }
                    else if (charCode === 38) {
                        if ($('.form-group').find(`div#${display_id} li.autocomplete_active`).length > 0) {
                            var sebelumnya = $('.form-group').find(`div#${display_id} li.autocomplete_active`).prev();
                            $('.form-group').find(`div#${display_id} li.autocomplete_active`).removeClass('autocomplete_active');
                            sebelumnya.addClass('autocomplete_active');
                        } else {
                            $('.form-group').find(`div#${display_id} li:first`).addClass('autocomplete_active');
                        }
                    }
                    else if (charCode === 13) {
                        on_selected($('.form-group').find(`div#${display_id} li.autocomplete_active`))
                    }
                    else {
                        search_ajax_fn($input.width(), s);
                    }
                }, 100)
            }
            else {
                $display.hide();
            }
        })

        $(document).on('click', `div#${display_id} .daftar-autocomplete li`, function () {
            on_selected($(this))
            $display.hide();
        })
    }

    const ajax_no_bpjs = (width, s) => {
        $('div#hasil_pencarian').hide()
        const Lebar = width + 25
        $.ajax({
            url: "<?= site_url('Bpjs/ajax_nomor_kartu'); ?>",
            type: "POST",
            cache: false,
            data: 'keyword=' + s,
            dataType: 'json',
            success: function (json) {
                if (json.status === 1) {
                    $('div#hasil_pencarian').css({'width': Lebar + 'px'});
                    $('div#hasil_pencarian').show('fast');
                    $('div#hasil_pencarian').html(json.datanya);
                }
                if (json.status === 0) {
                    $('div#hasil_pencarian').html('');
                }
            }
        })
    }

    const ajax_search_faskes = (width, s) => {
        $('div#hasil_pencarian_ppk_pelayanan').hide()
        const Lebar = width + 25
        $.ajax({
            url: "<?= site_url('Bpjs/ajax_search_faskes'); ?>",
            type: "POST",
            cache: false,
            data: 'keyword=' + s,
            dataType: 'json',
            success: function (json) {
                if (json.status === 1) {
                    $('div#hasil_pencarian_ppk_pelayanan').css({'width': Lebar + 'px'});
                    $('div#hasil_pencarian_ppk_pelayanan').show('fast');
                    $('div#hasil_pencarian_ppk_pelayanan').html(json.datanya);
                }
                if (json.status === 0) {
                    $('div#hasil_pencarian_ppk_pelayanan').html('');
                }
            }
        })
    }

    const ajax_search_faskes_2 = (width, s) => {
        $('div#hasil_pencarian_ppk_rujukan').hide()
        const Lebar = width + 25
        $.ajax({
            url: "<?= site_url('Bpjs/ajax_search_faskes'); ?>",
            type: "POST",
            cache: false,
            data: 'keyword=' + s,
            dataType: 'json',
            success: function (json) {
                if (json.status === 1) {
                    $('div#hasil_pencarian_ppk_rujukan').css({'width': Lebar + 'px'});
                    $('div#hasil_pencarian_ppk_rujukan').show('fast');
                    $('div#hasil_pencarian_ppk_rujukan').html(json.datanya);
                }
                if (json.status === 0) {
                    $('div#hasil_pencarian_ppk_rujukan').html('');
                }
            }
        })
    }

</script>
