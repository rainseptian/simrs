<div class="row">
    <div class="col-12 col-md-6">
        <div class="form-group">
            <label for="nama" class="control-label">Spesialis/SubSpesialis*</label>
            <input type="text" class="form-control" value="<?=$rujukan->poliRujukan->nama?>" readonly>
            <input type="hidden" name="poli_tujuan" value="<?=$rujukan->poliRujukan->kode?>">
            <input type="hidden" name="poli_eksekutif" value="0">
        </div>
        <div class="form-group">
            <label for="nama" class="control-label">DPJP yang Melayani*</label>
            <select type="text" class="form-control select2" id="dpjp_layan"
                    name="dpjp_layan"
                    placeholder="Masukkan Dokter DPJP" required>
                <option value="">--Pilih Dokter DPJP--</option>
                <?php foreach ($dpjp as $v) : ?>
                    <option value="<?=$v['kode']?>"><?=$v['nama']?></option>
                <?php endforeach; ?>
            </select>
        </div>
        <div class="form-group">
            <label for="nama" class="control-label">Asal Rujukan</label>
            <br>
            <label style="font-weight: normal">
                <input type="radio" name="asal_rujukan" value="1" required <?=$asalFaskes == 1 ? 'checked' : ''?> disabled> Faskes 1
            </label>
            <label style="font-weight: normal; margin-left: 8px">
                <input type="radio" name="asal_rujukan" value="2" required <?=$asalFaskes == 2 ? 'checked' : ''?> disabled> Faskes 2 (RS)
            </label>
        </div>
        <div class="form-group">
            <label for="nama" class="control-label">PPK Asal Rujukan</label>
            <input type="text" class="form-control" id="cari_ppk_rujukan"
                   name="cari_ppk_rujukan"
                   value="<?=$rujukan->provPerujuk->nama?>"
                   placeholder="Masukkan Faskes Rujukan" required readonly>
            <input type="hidden" name="ppk_rujukan" id='ppk_rujukan' value="<?=$rujukan->provPerujuk->kode?>">
        </div>
        <div class="form-group">
            <label for="nama" class="control-label">Tgl Rujukan</label>
            <input type="date" class="form-control" name="tgl_rujukan" placeholder="Masukkan Tgl Rujukan" readonly value="<?=$rujukan->tglKunjungan?>">
        </div>
        <div class="form-group">
            <label for="nama" class="control-label">No Rujukan</label>
            <input type="text" class="form-control" name="no_rujukan" placeholder="Masukkan No Rujukan" readonly value="<?=$rujukan->noKunjungan?>">
        </div>
        <div class="form-group">
            <label for="nama" class="control-label">No Surat SKDP</label>
            <input type="text" class="form-control" name="no_skdp" id="no_skdp" placeholder="Masukkan No Surat SKDP">
        </div>
        <div class="form-group">
            <label for="nama" class="control-label">DPJP Pemberi Surat SKDP</label>
            <input type="text" class="form-control" name="dpjp_pemberi_skdp_txt" id="dpjp_pemberi_skdp_txt" placeholder="Masukkan DPJP Pemberi Surat SKDP" readonly>
            <input type="hidden" name="dpjp_pemberi_skdp" id="dpjp_pemberi_skdp">
        </div>
        <div class="form-group">
            <label for="nama" class="control-label">Tujuan Kunjungan</label>
            <br>
            <label style="font-weight: normal">
                <input type="radio" name="tujuan_kunj" value="1" required checked> Prosedur
            </label>
            <label style="font-weight: normal; margin-left: 8px">
                <input type="radio" name="tujuan_kunj" value="2" required> Konsul Dokter
            </label>
        </div>
        <div class="form-group" id="c-flag-procedure">
            <label for="nama" class="control-label">Prosedur</label>
            <br>
            <label style="font-weight: normal">
                <input type="radio" name="flag_procedure" value="0" checked> Prosedur Tidak Berkelanjutan
            </label>
            <br>
            <label style="font-weight: normal;">
                <input type="radio" name="flag_procedure" value="1"> Prosedur dan Terapi Berkelanjutan
            </label>
        </div>
        <div class="form-group" id="c-kd-penunjang">
            <label for="nama" class="control-label">Penunjang</label>
            <select class="form-control" name="kd_penunjang" id="kd_penunjang">
                <option value="7">Laboratorium</option>
                <option value="8">USG</option>
                <option value="9">Farmasi</option>
                <option value="10">Lain-Lain</option>
                <option value="11">MRI</option>
            </select>
        </div>
        <div class="form-group" id="c-assesment-pel" style="display: none">
            <label for="nama" class="control-label">Asesmen Pelayanan</label>
            <select class="form-control" name="assesment_pel" id="assesment_pel">
                <option value="1">Poli spesialis tidak tersedia pada hari sebelumnya</option>
                <option value="2">Jam Poli telah berakhir pada hari sebelumnya</option>
                <option value="3">Dokter Spesialis yang dimaksud tidak praktek pada hari sebelumnya</option>
                <option value="4">Atas Instruksi RS</option>
                <option value="5">Tujuan Kontrol</option>
            </select>
        </div>
    </div>
    <div class="col-12 col-md-6">
        <div class="form-group">
            <label for="nama" class="control-label">Tgl SEP*</label>
            <input type="date" class="form-control" id="tgl_sep_" name="tgl_sep" readonly required value="<?=$tgl_sep?>">
        </div>
        <div class="form-group">
            <label for="nama" class="control-label">No RM*</label>
            <input type="text" class="form-control" name="no_mr" id="no_mr" value="<?=$peserta->mr->noMR?>" placeholder="Masukkan No RM" required>
        </div>
        <div class="form-group">
            <label for="nama" class="control-label">Diagnosa*</label>
            <select class="form-control select2" required
                    style="width: 100%;"
                    name="diag_awal"
                    id="diag_awal"
                    data-placeholder="Masukkan Diagnosa Awal ICD10">
                <option value="<?=$rujukan->diagnosa->kode?>"><?=$rujukan->diagnosa->nama?></option>
            </select>
        </div>
        <div class="form-group">
            <label for="nama" class="control-label">No Telp*</label>
            <input type="text" class="form-control" name="no_telp" id="no_telp" placeholder="Ketik no telp yang dapat dihubungi" required value="<?=$peserta->mr->noTelepon?>">
        </div>
        <div class="form-group">
            <label for="nama" class="control-label">Catatan</label>
            <textarea class="form-control" name="catatan" id="catatan" placeholder="Ketik catatan apabila ada"></textarea>
        </div>
        <div class="form-group">
            <label for="nama" class="control-label">Status Kecelakaan*</label>
            <select class="form-control laka_lantas" name="laka_lantas" id="laka_lantas">
                <option value="-">-- Silahkan Pilih --</option>
                <option value="0">Bukan Kecelakaan</option>
                <option value="1">Kecelakaan lalulintas dan bukan kecelakaan kerja</option>
                <option value="2">Kecelakaan lalulintas dan bukan kecelakaan kerja</option>
                <option value="3">Kecelakaan kerja</option>
            </select>
        </div>
        <?php $this->load->view('bpjs/sep/jaminan'); ?>
    </div>
</div>

<script>
    $(function () {
        $('#no_skdp').keyup(function () {
            if ($(this).val().length === 19) {
                ajax_search_skdp($(this).val(), r => {
                    if (+r.jnsKontrol !== 2) {
                        toastr.error('Bukan No Surat Kontrol')
                        return
                    }

                    toastr.success('No surat kontrol ditemukan')
                    $('#dpjp_pemberi_skdp').val(r.kodeDokter)
                    $('#dpjp_pemberi_skdp_txt').val(r.namaDokter)
                })
            }
        })
    })

    const ajax_search_skdp = (v, f) => {
        $.ajax({
            url: "<?= site_url('Bpjs/ajax_search_surat_kontrol'); ?>",
            type: "POST",
            cache: false,
            data: 'keyword=' + v,
            dataType: 'json',
            success: function (json) {
                if (json.status === 1) {
                    f(json.datanya)
                }
                else {
                    toastr.error('Gagal mendapatkan data')
                }
            }
        })
    }

</script>

<script>
    $(function () {
        $(`input[type=radio][name=tujuan_kunj]`).change(function () {
            if (+$(this).val() === 1) {
                $('#c-flag-procedure').css('display', 'block')
                $('#c-kd-penunjang').css('display', 'block')
                $('#c-assesment-pel').css('display', 'none')
            }
            else {
                $('#c-flag-procedure').css('display', 'none')
                $('#c-kd-penunjang').css('display', 'none')
                $('#c-assesment-pel').css('display', 'block')
            }
        })

        $('input[type=radio][name=flag_procedure]').change(function () {
            console.log('oiooioi')
            const select = $('#kd_penunjang')
            select.empty()
            console.log(+$(this).val())
            if (+$(this).val() === 0) {
                select.append(`<option value="7">Laboratorium</option>`)
                select.append(`<option value="8">USG</option>`)
                select.append(`<option value="9">Farmasi</option>`)
                select.append(`<option value="10">Lain-Lain</option>`)
                select.append(`<option value="11">MRI</option>`)
            }
            else {
                select.append(`<option value="1">Radioterapi</option>`)
                select.append(`<option value="2">Kemoterapi</option>`)
                select.append(`<option value="3">Rehabilitasi Medik</option>`)
                select.append(`<option value="4">Rehabilitasi Psikososial</option>`)
                select.append(`<option value="5">Transfusi Darah</option>`)
                select.append(`<option value="6">Pelayanan Gigi</option>`)
                select.append(`<option value="12">HEMODIALISA</option>`)
            }
        })
    })
</script>