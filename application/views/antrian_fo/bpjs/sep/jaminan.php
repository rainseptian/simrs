<div id="div-jaminan" class="text-muted well well-sm no-shadow" style="display: none">
    <div class="form-group">
        <label for="nama" class="control-label">Tgl Kejadian</label>
        <input type="date" class="form-control" name="tgl_kejadian">
    </div>
    <div class="form-group">
        <label for="nama" class="control-label">Provinsi Laka</label>
        <select class="form-control select2"
                style="width: 100%;"
                name="kd_propinsi"
                id="kd_propinsi"
                data-placeholder="Pilih Provinsi Laka">
            <option value="">--Pilih Provinsi Laka--</option>
            <?php foreach ($provinsi as $v) : ?>
                <option value="<?=$v->kode?>"><?=$v->nama?></option>
            <?php endforeach; ?>
        </select>
    </div>
    <div class="form-group">
        <label for="nama" class="control-label">Kabupaten Laka</label>
        <select class="form-control select2"
                style="width: 100%;"
                name="kd_kabupaten"
                id="kd_kabupaten"
                data-placeholder="Pilih Kabupaten Laka">
        </select>
    </div>
    <div class="form-group">
        <label for="nama" class="control-label">Kecamatan Laka</label>
        <select class="form-control select2"
                style="width: 100%;"
                name="kd_kecamatan"
                id="kd_kecamatan"
                data-placeholder="Pilih Kecamatan Laka">
        </select>
    </div>
    <div class="form-group">
        <label for="nama" class="control-label">Keterangan Kejadian</label>
        <textarea class="form-control" name="keterangan" placeholder="Masukkan Keterangan"></textarea>
        <input type="hidden" name="suplesi" value="0">
        <input type="hidden" name="no_sep_suplesi">
    </div>
</div>