<div class="modal fade" data-backdrop="static" id="modal-history" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" style="font-size: 16px">History</h5>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Tanggal Mulai</label>
                            <input type="date" class="form-control" id="history-start" value="<?=date('Y-m-d')?>">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Tanggal Akhir</label>
                            <input type="date" class="form-control" id="history-end" value="<?=date('Y-m-d')?>">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <button class="btn btn-success" style="margin-top: 23px" id="b-cari-history">Cari</button>
                    </div>
                </div>
                <table class="table table-bordered" id="tbl-history">
                    <thead>
                    <tr>
                        <th>No</th>
                        <th>Tgl SEP</th>
                        <th>No SEP</th>
                        <th>No Rujukan</th>
                        <th>PPK Pelayanan</th>
                        <th>Diagnosa</th>
                        <th>Poli</th>
                        <th>Jenis Pelayanan</th>
                        <th>Kelas Rawat</th>
                    </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Tutup</button>
            </div>
        </div>
    </div>
</div>