<div class="modal fade" data-backdrop="static" id="modal-jaminan" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" style="font-size: 16px" id="modal-title">Data Penjaminan an. <?=$peserta->nama?></h5>
            </div>
            <div class="modal-body">
                <div class="form-horizontal">
                    <div class="callout callout-info">
                        <p style="margin-top: 14px;">
                            Daftar ini merupakan penjaminan kecelakaan lalu lintas dari PT. Jasa Raharja.
                            <br><b>Silahkan Pilih Data tersebut sesuai dengan berkas kasus sebelumnya(SUPLESI) dengan klik <i class="fa fa-check-circle"></i> warna hijau.</b>
                        </p>
                        <p style="margin-top: 12px;">
                            <b>
                                1. Jika Kasus Kecelakaan baru, Silahkan Klik tombol kasus KLL Baru.<br>
                                2. Jika Kasus Kecelakaan lama, Silahkan Pilih No.SEP Sebagai SEP Kasus Sebelumnya.<br>
                                3. Untuk Melihat Detail Jumlah Dibayar. Silahkan Pilih dengan klik NO.SEP.
                            </b>
                        </p>
                    </div>
                    <div id="tblJaminanHistori_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer"><div class="row"><div class="col-sm-6"></div><div class="col-sm-6"></div></div><div class="row"><div class="col-sm-12"><table class="table table-bordered table-striped dataTable no-footer" cellpadding="0" id="tblJaminanHistori" style="width: 100%; font-size: small;" role="grid" aria-describedby="tblJaminanHistori_info">
                        <thead>
                        <tr role="row"><th class="sorting_asc" tabindex="0" aria-controls="tblJaminanHistori" rowspan="1" colspan="1" aria-label="Action: activate to sort column descending" style="width: 37px;" aria-sort="ascending">Action</th><th class="sorting" tabindex="0" aria-controls="tblJaminanHistori" rowspan="1" colspan="1" aria-label="No.SEP: activate to sort column ascending" style="width: 43px;">No.SEP</th><th class="sorting" tabindex="0" aria-controls="tblJaminanHistori" rowspan="1" colspan="1" aria-label="No.SEP AWAL: activate to sort column ascending" style="width: 42px;">No.SEP AWAL</th><th class="sorting" tabindex="0" aria-controls="tblJaminanHistori" rowspan="1" colspan="1" aria-label="Tgl.SEP: activate to sort column ascending" style="width: 44px;">Tgl.SEP</th><th class="sorting" tabindex="0" aria-controls="tblJaminanHistori" rowspan="1" colspan="1" aria-label="Tgl.Kejadian: activate to sort column ascending" style="width: 72px;">Tgl.Kejadian</th><th class="sorting" tabindex="0" aria-controls="tblJaminanHistori" rowspan="1" colspan="1" aria-label="No.Register: activate to sort column ascending" style="width: 68px;">No.Register</th><th class="sorting" tabindex="0" aria-controls="tblJaminanHistori" rowspan="1" colspan="1" aria-label="Surat Jaminan: activate to sort column ascending" style="width: 50px;">Surat Jaminan</th></tr>
                        </thead>
                        <tbody><tr class="odd"><td valign="top" colspan="7" class="dataTables_empty">No data available in table</td></tr></tbody></table><div id="tblJaminanHistori_processing" class="dataTables_processing" style="display: none;">Processing...</div></div></div><div class="row"><div class="col-sm-5"><div class="dataTables_info" id="tblJaminanHistori_info" role="status" aria-live="polite">Showing 0 to 0 of 0 entries</div></div><div class="col-sm-7"><div class="dataTables_paginate paging_simple_numbers" id="tblJaminanHistori_paginate"><ul class="pagination"><li class="paginate_button previous disabled" id="tblJaminanHistori_previous"><a href="#" aria-controls="tblJaminanHistori" data-dt-idx="0" tabindex="0">Previous</a></li><li class="paginate_button next disabled" id="tblJaminanHistori_next"><a href="#" aria-controls="tblJaminanHistori" data-dt-idx="1" tabindex="0">Next</a></li></ul></div></div></div></div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal" onclick="onModalJaminanDismiss(false)">Batal</button>
                <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="onModalJaminanDismiss(true)">Kasus KLL Baru</button>
            </div>
        </div>
    </div>
</div>