<link rel="stylesheet" href="<?= base_url() ?>assets/plugins/iCheck/all.css">
<!--<link rel="stylesheet" href="--><?//= base_url() ?><!--assets/bower_components/select2/dist/css/select2.min.css">-->
<link rel="stylesheet" href="<?= base_url() ?>assets/plugins/autocomplete/style-gue.css">
<style media="screen">
    .select2-container {
        width: 100% !important;
    }
    @media (min-width: 768px) {
        .modal-xl {
            width: 90%;
            max-width:1200px;
        }
    }
</style>

<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Pembuatan SEP
            <small>Surat Eligibilitas Peserta</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?= base_url() ?>Dashbord"><i class="fa fa-dashboard"></i> Dashbord</a></li>
            <li class="active">Pembuatan SEP</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-3">
                <!-- Profile Image -->

                <div class="box box-widget widget-user-2">
                    <div class="widget-user-header bg-success">
                        <div class="widget-user-image">
                            <img class="img-circle" src="<?=$peserta->sex == 'L' ? base_url().'assets/img/man.png' : base_url().'assets/img/woman.png'?>">
                        </div>
                        <h5 class="widget-user-username" id="lblnama" style="font-size: 18px; font-weight: bold;"><?=$peserta->nama?></h5>
                        <p class="widget-user-desc" id="lblnoka"><?=$peserta->noKartu?></p>
                        <input type="hidden" id="txtkelamin" value="L">
                        <input type="hidden" id="txtkdstatuspst" value="0">
                    </div>

                    <!-- /.box-body -->

                    <!-- /.box -->
                    <!-- About Me Box -->

                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="nav-tabs-custom">
                            <ul class="nav nav-tabs">
                                <li class="active"><a title="Profile Peserta" href="#tab_1" data-toggle="tab" aria-expanded="true"><span class="fa fa-user"></span></a></li>
                                <li class=""><a href="#tab_3" title="Histori" data-toggle="tab" id="tabHistori" aria-expanded="false"><span class="fa fa-list"></span></a></li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane active" id="tab_1">
                                    <ul class="list-group list-group-unbordered">
                                        <li class="list-group-item">
                                            <span class="fa fa-sort-numeric-asc"></span> <a title="NIK" class="pull-right-container" id="lblnik"><?=$peserta->nik?></a>
                                        </li>
                                        <!--                                        <li class="list-group-item">-->
                                        <!--                                            <span class="fa fa-credit-card"></span> <a title="No.Kartu Bapel JKK" class="pull-right-container" id="lblnokartubapel"></a>-->
                                        <!--                                        </li>-->
                                        <li class="list-group-item">
                                            <span class="fa fa-calendar"></span> <a title="Tanggal Lahir" class="pull-right-container" id="lbltgllahir"><?=$peserta->tglLahir?></a>
                                        </li>
                                        <!--                                        <li class="list-group-item">-->
                                        <!--                                            <span class="fa fa-user"></span> <a title="PISA" class="pull-right-container" id="lblpisa">--><?//=$peserta->noKartu?><!--</a>-->
                                        <!--                                        </li>-->
                                        <li class="list-group-item">
                                            <span class="fa fa-hospital-o"></span> <a title="Hak Kelas Rawat" class="pull-right-container" id="lblhakkelas"><?=$peserta->hakKelas->keterangan?></a>
                                            <input type="hidden" name="klsRawatHak" value="<?=$peserta->hakKelas->kode?>">
                                        </li>
                                        <li class="list-group-item">
                                            <span class="fa fa-stethoscope"></span>  <a title="Faskes Tingkat 1" class="pull-right-container" id="lblfktp"><?=$peserta->provUmum->kdProvider?> - <?=$peserta->provUmum->nmProvider?></a>
                                        </li>
                                        <li class="list-group-item">
                                            <span class="fa fa-calendar"></span>  <a title="TMT dan TAT Peserta" class="pull-right-container" id="lbltmt_tat"><?=$peserta->tglTMT?> s.d <?=$peserta->tglTAT?></a>
                                        </li>
                                        <li class="list-group-item">
                                            <span class="fa fa-calendar"></span>  <a title="Jenis Peserta" class="pull-right-container" id="lblpeserta"><?=$peserta->jenisPeserta->keterangan?></a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="tab-pane" id="tab_3">
                                    <div id="divHistori" class="list-group">
                                        <?php foreach($history as $v) : ?>
                                            <a href="<?=base_url('')?>bpjs/sep/detail/<?=$v->noSep?>" target="_blank" class="list-group-item">
                                                <h5 class="list-group-item-heading"><b><u><?=$v->noSep?></u></b></h5>
                                                <small>Rawat <?=$v->jnsPelayanan == 1 ? 'Inap' : 'Jalan'?></small>
                                                <p class="list-group-item-text"><small></small></p>
                                                <p class="list-group-item-text"><small><?=$v->poli?></small></p>
                                                <p class="list-group-item-text"><small>Tgl.SEP: <?=date('d-F-Y', strtotime($v->tglSep))?></small></p>
                                                <p class="list-group-item-text"><small><?=$v->noRujukan?></small></p>
                                                <p class="list-group-item-text"><small><?=$v->diagnosa?></small></p>
                                                <p class="list-group-item-text"><small><?=$v->ppkPelayanan?></small></p>
                                            </a>
                                        <?php endforeach; ?>
                                    </div>
                                    <div>
                                        <button type="button" id="btnHistori" class="btn btn-xs btn-default btn-block"><span class="fa fa-cubes"></span> Histori</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="divriwayatKK" style="display: none;">
                            <button type="button" id="btnRiwayatKK" class="btn btn-danger btn-block"><span class="fa fa-th-list"></span> Pasien Memiliki Riwayat KLL/KK/PAK <br><i>(klik lihat data)</i></button>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->

            </div>
            <div class="col-md-9">
                <form method="post" id="form-insert" action="<?=base_url()?>bpjs/sep/insert">
                    <input type="hidden" name="nomor_kartu" value="<?=$peserta->noKartu?>">
                    <input type="hidden" name="jns_pelayanan" value="<?=$jns_pelayanan?>">
                    <input type="hidden" name="kelas_rawat_hak" value="<?=$peserta->hakKelas->kode?>">
                    <input type="hidden" name="asal_rujukan" value="<?=$asal_rujukan?>">
                    <input type="hidden" name="id_pasien" value="<?=$pasien ? $pasien->id : 0?>">
                    <input type="hidden" name="jenis" value="<?=$jenis?>"><!-- 2: igd atau; 1: rujukan-->
                    <input type="hidden" name="jns_rujukan" value="<?=$jns_rujukan?>"><!-- 1: rujukan normal; 2: rujukan kontrol-->

                    <div class="box box-danger">
                        <div class="box-header with-border">
                            <h3 class="box-title"><?=$jenis_sep?></h3>
                        </div>
                        <?php if ($peserta->informasi->prolanisPRB || $peserta->cob->noAsuransi) : ?>
                            <div class="box-body">
                                <?php if ($peserta->informasi->prolanisPRB) : ?>
                                    <div class="callout callout-info">
                                        <p>
                                            <b>Peserta Prolanis PRB</b><br>
                                            Dinsos: <?=$peserta->informasi->dinsos?><br>
                                            No SKTM: <?=$peserta->informasi->noSKTM?><br>
                                            Prolanis PRB: <?=$peserta->informasi->prolanisPRB?><br>
                                        </p>
                                    </div>
                                <?php endif; ?>
                                <?php if ($peserta->cob->noAsuransi) : ?>
                                    <div class="callout callout-info">
                                        <p>
                                            <b>Peserta COB</b><br>
                                        </p>
                                    </div>
                                <?php endif; ?>
                            </div>
                        <?php endif; ?>
                        <div class="box-body">
                            <?php $this->load->view('bpjs/sep/create_detail_'.$detail_view); ?>
                        </div>
                        <div class="box-footer">
                            <button type="button" id="b-simpans" class="btn btn-success pull-right">Simpan</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </section>
</div>

<?php $this->load->view('bpjs/sep/modal_jaminan'); ?>
<?php $this->load->view('bpjs/sep/modal_history'); ?>
<div id='ResponseInput'></div>

<script src="<?= base_url() ?>assets/plugins/input-mask/jquery.inputmask.js"></script>
<script src="<?= base_url() ?>assets/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="<?= base_url() ?>assets/plugins/input-mask/jquery.inputmask.extensions.js"></script>
<script src="<?= base_url() ?>assets/bower_components/select2/dist/js/select2.full.min.js"></script>
<script src="<?php echo base_url(); ?>assets/bower_components/toast-alert/toastr.js"></script>

<script>
    const is_fingerprint = (nmr_bpjs, f) => {
        // f(1)
        $.ajax({
            url: "<?= site_url('bpjs/ajax/raw'); ?>",
            type: "POST",
            data: JSON.stringify({
                'method': 'GET',
                'url': `<?=$base_url_vclaim?>SEP/FingerPrint/Peserta/<?=$peserta->noKartu?>/TglPelayanan/<?=date('Y-m-d')?>`
            }),
            success: function (json) {
                const data = JSON.parse(json)
                if (data.success) {
                    if (+data.data.kode === 0) {
                        Swal.fire({
                            icon: 'error',
                            title: data.data.status ?? 'Belum fingerprint',
                            confirmButtonText: 'Buka App Fingerprint'
                        }).then(r => {
                            if (r.isConfirmed) {
                                window.open('fpbpjs://open');
                            }
                            f(0)
                        })
                    }
                    else {
                        f(1)
                    }
                }
                else {
                    toastr.error(data.server_output?.metaData?.message ?? 'Gagal mendapatkan data')
                }
            },
            error: (e) => {
                toastr.error('Gagal mendapatkan data')
            }
        })
    }

    $(function () {
        $('#b-simpans').click(function () {
            is_fingerprint('', (r) => {
                if (r) {
                    // toastr.error('masuk')
                    $('#form-insert').submit()
                }
            })
        })

        $('#btnHistori').click(function () {
            $('#modal-history').modal('show')
        })

        $('#b-cari-history').click(function () {
            $.ajax({
                url: "<?= site_url('bpjs/ajax/raw'); ?>",
                type: "POST",
                data: JSON.stringify({
                    'method': 'GET',
                    'url': `<?=$base_url_vclaim?>monitoring/HistoriPelayanan/NoKartu/<?=$noKartu?>/tglMulai/${$('#history-start').val()}/tglAkhir/${$('#history-end').val()}`
                }),
                success: function (json) {
                    const data = JSON.parse(json)
                    if (data.success) {
                        $('#tbl-history tbody').empty()
                        data.data.histori.forEach((v, k) => {
                            $('#tbl-history tbody').append(`
                                <tr>
                                    <td>${k + 1}</td>
                                    <td>${v.tglSep}</td>
                                    <td>${v.noSep}</td>
                                    <td>${v.noRujukan}</td>
                                    <td>${v.ppkPelayanan}</td>
                                    <td>${v.diagnosa}</td>
                                    <td>${v.poli}</td>
                                    <td>Rawat ${+v.jnsPelayanan === 1 ? 'Inap' : 'Jalan'}</td>
                                    <td>${v.kelasRawat}</td>
                                </tr>
                            `)
                        })
                    }
                    else {
                        toastr.error(data.server_output.metaData.message ?? 'Gagal mendapatkan data')
                    }
                },
                error: (e) => {
                    toastr.error('Gagal mendapatkan data')
                }
            })
        })
    })
</script>

<script>
    const set_search = ($input, search_ajax_fn, $display, on_selected) => {
        let timer = null
        const display_id = $display.attr('id')

        $input.on('keyup', function (e) {
            const s = $(this).val()
            if (s !== '') {
                clearTimeout(timer)
                timer = setTimeout(() => {
                    const charCode = ( e.which ) ? e.which : e.keyCode
                    if (charCode === 40) {
                        if ($('.form-group').find(`div#${display_id} li.autocomplete_active`).length > 0) {
                            var selanjutnya = $('.form-group').find(`div#${display_id} li.autocomplete_active`).next();
                            $('.form-group').find(`div#${display_id} li.autocomplete_active`).removeClass('autocomplete_active');
                            selanjutnya.addClass('autocomplete_active');
                        } else {
                            $('.form-group').find(`div#${display_id} li:first`).addClass('autocomplete_active');
                        }
                    }
                    else if (charCode === 38) {
                        if ($('.form-group').find(`div#${display_id} li.autocomplete_active`).length > 0) {
                            var sebelumnya = $('.form-group').find(`div#${display_id} li.autocomplete_active`).prev();
                            $('.form-group').find(`div#${display_id} li.autocomplete_active`).removeClass('autocomplete_active');
                            sebelumnya.addClass('autocomplete_active');
                        } else {
                            $('.form-group').find(`div#${display_id} li:first`).addClass('autocomplete_active');
                        }
                    }
                    else if (charCode === 13) {
                        on_selected($('.form-group').find(`div#${display_id} li.autocomplete_active`))
                    }
                    else {
                        search_ajax_fn($input.width(), s);
                    }
                }, 100)
            }
            else {
                $display.hide();
            }
        })

        $(document).on('click', `div#${display_id} .daftar-autocomplete li`, function () {
            on_selected($(this))
            $display.hide();
        })
    }
</script>

<script>
    $('#diag_awal').select2({
        ajax: {
            url: "<?= site_url('Bpjs/ajax_search_diagnosa'); ?>",
            type: "POST",
            data: function (params) {
                return { keyword: params.term }
            },
            processResults: function (data) {
                return {
                    results: JSON.parse(data).datanya.map(v => ({id: v.kode, text: v.nama}))
                }
            }
        },
        minimumInputLength: 3
    })
</script>

<!--jaminan-->
<script>
    $(function () {
        $('.laka_lantas').change(function () {
            if ($(this).val() !== '-' && $(this).val() !== '0') {
                $('#modal-jaminan').modal('show')
            }
            else {
                $('#div-jaminan').css('display', 'none')
            }
        })

        $('#kd_propinsi').select2().change(function () {
            $('#kd_kabupaten').empty()
            ajax_search_kabupaten($(this).val(), (val) => {
                $('#kd_kecamatan').empty()
                ajax_search_kecamatan(val)
            })
        })

        $('#kd_kabupaten').select2().change(function () {
            $('#kd_kecamatan').empty()
            ajax_search_kecamatan($(this).val())
        })

        $('#kd_kecamatan').select2()
    })

    const onModalJaminanDismiss = (isNew) => {
        if (isNew) {
            $('#div-jaminan').css('display', 'block')
        }
        else {
            $('.laka_lantas').val('-').change()
        }
    }

    const ajax_search_kabupaten = (kode_provinsi, f = null) => {
        $.ajax({
            url: "<?= site_url('Bpjs/ajax_search_kabupaten'); ?>",
            type: "POST",
            cache: false,
            data: 'keyword=' + kode_provinsi,
            dataType: 'json',
            success: function (json) {
                if (json.status === 1) {
                    const data = json.datanya.map(v => ({id: v.kode, text: v.nama}))
                    $('#kd_kabupaten').select2({ data })
                    if (f) {
                        f($('#kd_kabupaten').val())
                    }
                }
            }
        })
    }

    const ajax_search_kecamatan = kode_kabupaten => {
        $.ajax({
            url: "<?= site_url('Bpjs/ajax_search_kecamatan'); ?>",
            type: "POST",
            cache: false,
            data: 'keyword=' + kode_kabupaten,
            dataType: 'json',
            success: function (json) {
                if (json.status === 1) {
                    const data = json.datanya.map(v => ({id: v.kode, text: v.nama}))
                    $('#kd_kecamatan').select2({ data })
                }
            }
        })
    }

</script>