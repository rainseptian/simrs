<link rel="stylesheet" href="<?= base_url() ?>assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            SEP
            <small></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="active"> SEP</li>
        </ol>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Daftar SEP</h3>&nbsp;&nbsp;
                    </div>
                    <div class="box-body">
                        <table id="example2" class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>No</th>
                                <th>Tgl Dibuat</th>
                                <th>NO BPJS</th>
                                <th>Pasien</th>
                                <th>No Rujukan</th>
                                <th>Response</th>
                                <th>Aksi</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $no=1; foreach ($list as $row) { ?>
                                <?php $res = json_decode($row->response); ?>
                                <tr>
                                    <td><?= $no; ?></td>
                                    <td><?= date('d-M-Y H:i', strtotime($row->created_at)); ?></td>
                                    <td><?= $row->no_bpjs; ?></td>
                                    <td>
                                        <?= $row->nama_pasien; ?>
                                        <br>
                                        <small><i>No RM: <?= $row->no_rm; ?></i></small>
                                    </td>
                                    <td>
                                        <?= $row->no_rujukan; ?>
                                    </td>
                                    <td>
                                        <?php if ($res->sep): ?>
                                            No SEP: <?= $res->sep->noSep; ?><br>
                                            Tipe SEP: <b><?= ucwords($row->type); ?></b>
                                        <?php else: ?>
                                            <?= $res->metaData->message ?? ''; ?>
                                        <?php endif; ?>
                                        <?php if ($row->type == 'rawat inap'): ?>
                                            <?php if ($row->pulang_success): ?>
                                                <span class="label label-success">Sudah Pulang</span>
                                                <div style="height: 4px"></div>
                                                <button onclick="lihat_pulang(<?=$row->id?>)" class="btn btn-sm btn-danger">
                                                    <i class="fa fa-arrows"></i> Detail Kepulangan
                                                </button>
                                            <?php else: ?>
                                                <span class="label label-danger">Belum Pulang</span>
                                            <?php endif; ?>
                                        <?php endif; ?>
                                    </td>
                                    <td>
                                        <div class="btn-group" role="group">
                                            <button id="btnGroupDrop1" type="button" class="btn btn-sm btn-success dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="fa fa-bars"></i>
                                            </button>
                                            <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                                                <a href="<?php echo base_url(); ?>bpjs/sep/detail/<?= $res->sep->noSep; ?>">
                                                    <button type="button" class="btn btn-primary btn-block btn-sm"><i class="fa fa-arrows"></i> Detail</button>
                                                </a>
                                                <?php if ($res->sep): ?>
                                                    <div style="height: 4px"></div>
                                                    <a href="<?php echo base_url(); ?>bpjs/sep/print_sep/<?= $row->id; ?>" target="_blank">
                                                        <button type="button" class="btn btn-success btn-block btn-sm"><i class="fa fa-print"></i> Cetak Surat</button>
                                                    </a>
                                                <?php endif; ?>
                                                <?php if (!$res->sep): ?>
                                                    <div style="height: 4px"></div>
                                                    <a href="<?php echo base_url(); ?>Bpjs/edit_sep/<?= $row->id; ?>">
                                                        <button type="button" class="btn btn-warning btn-block btn-sm"><i class="fa fa-pencil"></i> Edit</button>
                                                    </a>
                                                <?php endif; ?>
                                                <?php if ($row->type == 'rawat inap'): ?>
                                                    <div style="height: 4px"></div>
                                                    <button onclick="pulang(<?=$row->id?>)" class="btn btn-sm btn-warning btn-block">
                                                        <i class="fa fa-bed"></i> Update Pulang
                                                    </button>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <?php $no++; } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<div class="modal fade" id="modal-detail" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 style="display: inline-block" class="modal-title" id="exampleModalLabel">Modal title</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" id="f">
                <table class="table table-bordered" id="tbl-detail">
                    <thead>
                    <tr>
                        <th>Key</th>
                        <th>Value</th>
                    </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-pulang" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <form method="post" action="<?=base_url()?>bpjs/sep/update_pulang">
            <input type="hidden" name="id" id="id">
            <input type="hidden" name="no_sep" id="no_sep">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 style="display: inline-block" class="modal-title" id="update_pulang_title">Pengajuan SEP</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" id="f">
                    <div class="form-group">
                        <label for="nama" class="control-label">Tgl Pulang</label>
                        <input type="date" class="form-control" id="tgl_pulang" name="tgl_pulang" required value="<?=date('Y-m-d')?>">
                    </div>
                    <div class="form-group">
                        <label for="nama" class="control-label">Status Pulang</label>
                        <select class="form-control laka_lantas" name="status_pulang" id="status_pulang">
                            <option value="1">Atas Persetujuan Dokter</option>
                            <option value="3">Atas Permintaan Sendiri</option>
                            <option value="4">Meninggal</option>
                            <option value="5">Lain-lain</option>
                        </select>
                    </div>
                    <div class="form-group mng" style="display: none">
                        <label for="nama" class="control-label">No Surat Ket Meninggal</label>
                        <input type="text" class="form-control" id="no_surat" name="no_surat" placeholder="No Surat Ket Meninggal">
                    </div>
                    <div class="form-group mng" style="display: none">
                        <label for="nama" class="control-label">Tgl Meninggal</label>
                        <input type="date" class="form-control" id="tgl_meninggal" name="tgl_meninggal" value="<?=date('Y-m-d')?>">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success">
                        <i class="fa fa-save"></i>
                        Simpan
                    </button>
                    <button type="button" class="btn btn-warning" data-dismiss="modal">Batal</button>
                </div>
            </div>
        </form>
    </div>
</div>

<div class="modal fade" id="modal-lihat-pulang" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <form method="post" action="<?=base_url()?>bpjs/sep/update_pulang">
            <input type="hidden" name="id" id="id">
            <input type="hidden" name="no_sep" id="no_sep">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 style="display: inline-block" class="modal-title" id="modal-lihat-pulang-title">Pengajuan SEP</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <table class="table table-bordered" id="tbl-pulang">
                        <tbody>

                        </tbody>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-warning" data-dismiss="modal">Tutup</button>
                </div>
            </div>
        </form>
    </div>
</div>

<script>

    const parse = obj => {
        for (let key in obj) {
            if (typeof obj[key] === 'object') {
                parse(obj[key])
            }
            else {
                $('#tbl-detail > tbody').append(`
                    <tr>
                        <td>${key}</td>
                        <td>${obj[key]}</td>
                    </tr>
                `)
            }
        }
    }

    function open_detail(id) {
        const list = <?=json_encode($list);?>;
        const v = list.find(v => parseInt(v.id) === parseInt(id))
        if (v) {
            $('#tbl-detail > tbody').html('')
            const request = JSON.parse(v.request).request.t_sep
            parse(request)

            $('#exampleModalLabel').html(`Detail SEP ${v.nama}`)
            $('#modal-detail').modal('show')
        }
    }

    $(function () {
        $('#example1').DataTable()
        $('#example2').DataTable({
            rowReorder: {
                selector: 'td:nth-child(2)'
            },
            'paging'      : true,
            'lengthChange': true,
            'searching'   : true,
            'ordering'    : true,
            'info'        : true,
            'autoWidth'   : true
        })
    })

    const all = <?=json_encode($list)?>;
    function pulang(id) {
        const v = all.find(v => +v.id === id)
        if (v) {
            $('#id').val(v.id)
            $('#no_sep').val(v.no_sep)
            $('#update_pulang_title').html(`Update pulang ${v.no_sep}`)
            $('#modal-pulang').modal('show')
        }
    }

    $(function () {
        $('#status_pulang').change(function () {
            $('.mng').css('display', +$(this).val() === 4 ? 'block' : 'none')
        })
    })

    function lihat_pulang(id) {
        const v = all.find(v => +v.id === id)
        if (v) {
            const request = (JSON.parse(v.pulang_request)).request.t_sep
            $('#tbl-pulang tbody')
                .empty()
                .append(`
                    <tr>
                        <td>Tanggal Pulang</td>
                        <td>${request.tglPulang}</td>
                    </tr>
                    <tr>
                        <td>Status Pulang</td>
                        <td>${request.statusPulang === '1' ? 'Atas Persetujuan Dokter' : request.statusPulang === '3' ? 'Atas Permintaan Sendiri' : request.statusPulang === '4' ? 'Meninggal' : 'Lain-lain'}</td>
                    </tr>
                `)
                .append(request.statusPulang !== '4' ? '' : `
                    <tr>
                        <td>Tanggal Meninggal</td>
                        <td>${request.tglMeninggal}</td>
                    </tr>
                    <tr>
                        <td>No Surat Ket Meninggal</td>
                        <td>${request.noSuratMeninggal}</td>
                    </tr>
                `)

            $('#modal-lihat-pulang-title').html(`Detail kepulangan ${v.no_sep}`)
            $('#modal-lihat-pulang').modal('show')
        }
    }
</script>

<script>
    $(function () {
        <?php $warning = $this->session->flashdata('warning');
        if (!empty($warning)) { ?>
        Swal.fire({
            icon: 'error',
            title: '<?=$warning?>',
        })
        <?php } ?>
        <?php $success = $this->session->flashdata('success');
        if (!empty($success)) { ?>
        Swal.fire({
            icon: 'success',
            title: '<?=$success?>',
        })
        <?php } ?>
    })
</script>
