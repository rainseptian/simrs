<link rel="stylesheet" href="<?= base_url() ?>assets/plugins/iCheck/all.css">
<!--<link rel="stylesheet" href="--><?//= base_url() ?><!--assets/bower_components/select2/dist/css/select2.min.css">-->
<link rel="stylesheet" href="<?= base_url() ?>assets/plugins/autocomplete/style-gue.css">
<style media="screen">
    .select2-container {
        width: 100% !important;
    }
</style>

<div class="content-wrapper">
    <section class="content-header">
        <h1>
            SEP
            <small>Preview</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?= base_url() ?>Dashbord"><i class="fa fa-dashboard"></i> Dashbord</a></li>
            <li class="active">Edit SEP</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-danger">
                    <div class="box-header">
                        <h3 class="box-title">Edit SEP</h3>
                    </div>
                    <?php $warning = $this->session->flashdata('warning');
                    if (!empty($warning)) { ?>
                        <div class="alert alert-warning alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <h4><i class="icon fa fa-warning"></i> Alert!</h4>
                            <?= $warning ?>
                        </div>
                    <?php } ?>
                    <?php $success = $this->session->flashdata('success');
                    if (!empty($success)) { ?>
                        <div class="alert alert-success alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <h4><i class="icon fa fa-check"></i> Success!</h4>
                            <?= $success ?>
                        </div>
                    <?php } ?>
                    <div class="box-body">
                        <form class="form" method="post" action="<?= base_url() ?>Bpjs/edit_sep/<?=$id?>">
                            <div class="box-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="nama" class="control-label">Nomor Kartu</label>
                                            <input type="text" class="form-control" id="cari_nomor_kartu"
                                                   name="cari_nomor_kartu"
                                                   value="<?=$data->nama.' - '.$data->no_bpjs?>"
                                                   placeholder="Masukkan nama pasien / nomor kartu BPJS" required>
                                            <input type="hidden" name="nomor_kartu" id='nomor_kartu' value="<?=$data->no_bpjs?>">
                                            <input type="hidden" name="id_pasien" id='id_pasien' value="<?=$data->pasien_id?>">
                                            <div id='hasil_pencarian' class='hasil_pencarian'></div>
                                        </div>
                                        <div class="form-group">
                                            <label for="nama" class="control-label">Tgl SEP</label>
                                            <input type="date" class="form-control" name="tgl_sep" id="tgl_sep" value="<?=$req->tglSep?>" placeholder="Masukkan Tgl SEP" required>
                                        </div>
                                        <div class="form-group">
                                            <label for="nama" class="control-label">Faskes Pemberi Pelayanan</label>
                                            <input type="text" class="form-control" id="cari_ppk_pelayanan"
                                                   name="cari_ppk_pelayanan"
                                                   value="<?=$req->ppkPelayanan?>"
                                                   placeholder="Masukkan Faskes Pemberi Pelayanan" required>
                                            <input type="hidden" name="ppk_pelayanan" id='ppk_pelayanan' value="<?=$req->ppkPelayanan?>">
                                            <div id='hasil_pencarian_ppk_pelayanan' class='hasil_pencarian'></div>
                                        </div>
                                        <div class="form-group">
                                            <label for="nama" class="control-label">Jenis Pelayanan</label>
                                            <br>
                                            <label style="font-weight: normal">
                                                <input type="radio" name="jns_pelayanan" value="1" required <?=$req->jnsPelayanan == 1 ? 'checked' : ''?>> Rawat Inap
                                            </label>
                                            <label style="font-weight: normal; margin-left: 8px">
                                                <input type="radio" name="jns_pelayanan" value="2" required <?=$req->jnsPelayanan == 2 ? 'checked' : ''?>> Rawat Jalan
                                            </label>
                                        </div>
                                        <hr>
                                        <div class="form-group">
                                            <label for="nama" class="control-label">Kelas Rawat Hak (sesuai kelas rawat peserta)</label>
                                            <select class="form-control" name="kls_rawat_hak" required>
                                                <option value="">--Pilih Kelas Rawat Hak--</option>
                                                <option value="1" <?=$req->klsRawat->klsRawatHak == 1 ? 'selected' : ''?>>Kelas 1</option>
                                                <option value="2" <?=$req->klsRawat->klsRawatHak == 2 ? 'selected' : ''?>>Kelas 2</option>
                                                <option value="3" <?=$req->klsRawat->klsRawatHak == 3 ? 'selected' : ''?>>Kelas 3</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="nama" class="control-label">Kelas Rawat Naik (diisi jika naik kelas rawat)</label>
                                            <select class="form-control" name="kls_rawat_naik">
                                                <option value="">--Pilih Kelas Rawat Naik--</option>
                                                <option value="1" <?=$req->klsRawat->klsRawatNaik == 1 ? 'selected' : ''?>>VVIP</option>
                                                <option value="2" <?=$req->klsRawat->klsRawatNaik == 2 ? 'selected' : ''?>>VIP</option>
                                                <option value="3" <?=$req->klsRawat->klsRawatNaik == 3 ? 'selected' : ''?>>Kelas 1</option>
                                                <option value="4" <?=$req->klsRawat->klsRawatNaik == 4 ? 'selected' : ''?>>Kelas 2</option>
                                                <option value="5" <?=$req->klsRawat->klsRawatNaik == 5 ? 'selected' : ''?>>Kelas 3</option>
                                                <option value="6" <?=$req->klsRawat->klsRawatNaik == 6 ? 'selected' : ''?>>ICCU</option>
                                                <option value="7" <?=$req->klsRawat->klsRawatNaik == 7 ? 'selected' : ''?>>ICU</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="nama" class="control-label">Pembiayaan (diisi jika naik kelas rawat)</label>
                                            <select class="form-control" name="pembiayaan" id="pembiayaan">
                                                <option value="">--Pilih Pembiayaan--</option>
                                                <option value="1" <?=$req->klsRawat->pembiayaan == 1 ? 'selected' : ''?>>Pribadi</option>
                                                <option value="2" <?=$req->klsRawat->pembiayaan == 2 ? 'selected' : ''?>>Pemberi Kerja</option>
                                                <option value="3" <?=$req->klsRawat->pembiayaan == 3 ? 'selected' : ''?>>Asuransi Kesehatan Tambahan</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="nama" class="control-label">Penanggung Jawab (diisi jika naik kelas rawat)</label>
                                            <input type="text" class="form-control" name="penanggung_jawab" id="penanggung_jawab"
                                                   value="<?=$req->klsRawat->penanggungJawab?>"
                                                   placeholder="Masukkan Penanggung Jawab">
                                        </div>
                                        <hr>
                                        <div class="form-group">
                                            <label for="nama" class="control-label">No RM</label>
                                            <input type="text" class="form-control" name="no_mr" id="no_mr"
                                                   value="<?=$req->noMR?>"
                                                   placeholder="Masukkan No RM">
                                        </div>
                                        <div class="form-group">
                                            <label for="nama" class="control-label">Asal Rujukan</label>
                                            <br>
                                            <label style="font-weight: normal">
                                                <input type="radio" name="asal_rujukan" value="1" required <?=$req->rujukan->asalRujukan == 1 ? 'checked' : ''?>> Faskes 1
                                            </label>
                                            <label style="font-weight: normal; margin-left: 8px">
                                                <input type="radio" name="asal_rujukan" value="2" required <?=$req->rujukan->asalRujukan == 2 ? 'checked' : ''?>> Faskes 2 (RS)
                                            </label>
                                        </div>
                                        <div class="form-group">
                                            <label for="nama" class="control-label">Tgl Rujukan</label>
                                            <input type="date" class="form-control" name="tgl_rujukan"
                                                   value="<?=$req->rujukan->tglRujukan?>"
                                                   placeholder="Masukkan Tgl Rujukan">
                                        </div>
                                        <div class="form-group">
                                            <label for="nama" class="control-label">No Rujukan</label>
                                            <input type="text" class="form-control" name="no_rujukan"
                                                   value="<?=$req->rujukan->noRujukan?>"
                                                   placeholder="Masukkan No Rujukan">
                                        </div>
                                        <div class="form-group">
                                            <label for="nama" class="control-label">Faskes Rujukan</label>
                                            <input type="text" class="form-control" id="cari_ppk_rujukan"
                                                   name="cari_ppk_rujukan"
                                                   value="<?=$req->rujukan->ppkRujukan?>"
                                                   placeholder="Masukkan Faskes Rujukan">
                                            <input type="hidden" name="ppk_rujukan" id='ppk_rujukan' value="<?=$req->rujukan->ppkRujukan?>">
                                            <div id='hasil_pencarian_ppk_rujukan' class='hasil_pencarian'></div>
                                        </div>
                                        <hr>
                                        <div class="form-group">
                                            <label for="nama" class="control-label">Catatan</label>
                                            <input type="text" class="form-control" name="catatan" placeholder="Masukkan Catatan" value="<?=$req->catatan?>">
                                        </div>
                                        <div class="form-group">
                                            <label for="nama" class="control-label">Diagnosa Awal ICD10</label>
                                            <select class="form-control select2" required
                                                   style="width: 100%;"
                                                   name="diag_awal"
                                                   id="diag_awal"
                                                   data-placeholder="Masukkan Diagnosa Awal ICD10">
                                                <option value="<?=$req->diagAwal?>" selected><?=$req->diagAwal?></option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="nama" class="control-label">Poli Tujuan</label>
                                            <select class="form-control select2" required
                                                   style="width: 100%;"
                                                   name="poli_tujuan"
                                                   id="poli_tujuan"
                                                   data-placeholder="Masukkan Poli Tujuan">
                                                <option value="<?=$req->poli->tujuan?>" selected><?=$req->poli->tujuan?></option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="nama" class="control-label">Poli Eksekutif</label>
                                            <br>
                                            <label style="font-weight: normal">
                                                <input type="radio" name="poli_eksekutif" value="0" required <?=$req->rujukan->eksekutif == 0 ? 'checked' : ''?>> Tidak
                                            </label>
                                            <label style="font-weight: normal; margin-left: 8px">
                                                <input type="radio" name="poli_eksekutif" value="1" required <?=$req->rujukan->eksekutif == 1 ? 'checked' : ''?>> Iya
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="nama" class="control-label">COB</label>
                                            <br>
                                            <label style="font-weight: normal">
                                                <input type="radio" name="cob" value="0" required <?=$req->cob->cob == 0 ? 'checked' : ''?>> Tidak
                                            </label>
                                            <label style="font-weight: normal; margin-left: 8px">
                                                <input type="radio" name="cob" value="1" required <?=$req->cob->cob == 1 ? 'checked' : ''?>> Iya
                                            </label>
                                        </div>
                                        <div class="form-group">
                                            <label for="nama" class="control-label">Katarak</label>
                                            <br>
                                            <label style="font-weight: normal">
                                                <input type="radio" name="katarak" value="0" required <?=$req->katarak->katarak == 0 ? 'checked' : ''?>> Tidak
                                            </label>
                                            <label style="font-weight: normal; margin-left: 8px">
                                                <input type="radio" name="katarak" value="1" required <?=$req->katarak->katarak == 1 ? 'checked' : ''?>> Iya
                                            </label>
                                        </div>
                                        <hr>
                                        <div class="form-group">
                                            <label for="nama" class="control-label">Laka Lantas</label>
                                            <select class="form-control" name="laka_lantas" id="laka_lantas">
                                                <option value="0" <?=$req->jaminan->lakaLantas == 0 ? 'checked' : ''?>>Bukan Kecelakaan lalu lintas [BKLL]</option>
                                                <option value="1" <?=$req->jaminan->lakaLantas == 1 ? 'checked' : ''?>>KLL dan bukan kecelakaan Kerja [BKK]</option>
                                                <option value="2" <?=$req->jaminan->lakaLantas == 2 ? 'checked' : ''?>>KLL dan KK</option>
                                                <option value="3" <?=$req->jaminan->lakaLantas == 3 ? 'checked' : ''?>>KK</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="nama" class="control-label">Tgl Kejadian KLL</label>
                                            <input type="date" class="form-control" name="tgl_kejadian" value="<?=$req->jaminan->penjamin->tglKejadian?>">
                                        </div>
                                        <div class="form-group">
                                            <label for="nama" class="control-label">Keterangan</label>
                                            <input type="text" class="form-control" name="keterangan" placeholder="Masukkan Keterangan" value="<?=$req->jaminan->penjamin->keterangan?>">
                                        </div>
                                        <div class="form-group">
                                            <label for="nama" class="control-label">Suplesi</label>
                                            <br>
                                            <label style="font-weight: normal">
                                                <input type="radio" name="suplesi" value="0" required <?=$req->jaminan->penjamin->suplesi->suplesi == 0 ? 'checked' : ''?>> Tidak
                                            </label>
                                            <label style="font-weight: normal; margin-left: 8px">
                                                <input type="radio" name="suplesi" value="1" required <?=$req->jaminan->penjamin->suplesi->suplesi == 1 ? 'checked' : ''?>> Iya
                                            </label>
                                        </div>
                                        <div class="form-group">
                                            <label for="nama" class="control-label">No SEP Suplesi (diisi jika terdapat suplesi)</label>
                                            <input type="text" class="form-control" name="no_sep_suplesi" placeholder="Masukkan No SEP Suplesi" value="<?=$req->jaminan->penjamin->suplesi->noSepSuplesi?>">
                                        </div>
                                        <div class="form-group">
                                            <label for="nama" class="control-label">Provinsi Laka</label>
                                            <select class="form-control select2"
                                                    style="width: 100%;"
                                                    name="kd_propinsi"
                                                    id="kd_propinsi"
                                                    data-placeholder="Pilih Provinsi Laka">
                                                <option value="">--Pilih Provinsi Laka--</option>
                                                <?php foreach ($provinsi as $v) : ?>
                                                    <option value="<?=$v->kode?>" <?=$req->jaminan->penjamin->suplesi->lokasiLaka->kdPropinsi == $v->kode ? 'selected' : ''?>><?=$v->nama?></option>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="nama" class="control-label">Kabupaten Laka</label>
                                            <select class="form-control select2"
                                                    style="width: 100%;"
                                                    name="kd_kabupaten"
                                                    id="kd_kabupaten"
                                                    data-placeholder="Pilih Kabupaten Laka">
                                                <?php if ($req->jaminan->penjamin->suplesi->lokasiLaka->kdKabupaten) : ?>
                                                    <option value="<?=$req->jaminan->penjamin->suplesi->lokasiLaka->kdKabupaten?>" selected>
                                                        <?=$req->jaminan->penjamin->suplesi->lokasiLaka->kdKabupaten?>
                                                    </option>
                                                <?php endif; ?>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="nama" class="control-label">Kecamatan Laka</label>
                                            <select class="form-control select2"
                                                    style="width: 100%;"
                                                    name="kd_kecamatan"
                                                    id="kd_kecamatan"
                                                    data-placeholder="Pilih Kecamatan Laka">
                                                <?php if ($req->jaminan->penjamin->suplesi->lokasiLaka->kdKecamatan) : ?>
                                                    <option value="<?=$req->jaminan->penjamin->suplesi->lokasiLaka->kdKecamatan?>" selected>
                                                        <?=$req->jaminan->penjamin->suplesi->lokasiLaka->kdKecamatan?>
                                                    </option>
                                                <?php endif; ?>
                                            </select>
                                        </div>
                                        <hr>
                                        <div class="form-group">
                                            <label for="nama" class="control-label">Tujuan Kunjungan</label>
                                            <br>
                                            <label style="font-weight: normal">
                                                <input type="radio" name="tujuan_kunj" value="0" required <?=$req->tujuanKunj == 0 ? 'checked' : ''?>> Normal
                                            </label>
                                            <label style="font-weight: normal; margin-left: 8px">
                                                <input type="radio" name="tujuan_kunj" value="1" required <?=$req->tujuanKunj == 1 ? 'checked' : ''?>> Prosedur
                                            </label>
                                            <label style="font-weight: normal; margin-left: 8px">
                                                <input type="radio" name="tujuan_kunj" value="2" required <?=$req->tujuanKunj == 2 ? 'checked' : ''?>> Konsul Dokter
                                            </label>
                                        </div>
                                        <div class="form-group">
                                            <label for="nama" class="control-label">Prosedur (diisi jika tujuan kunjungan bukan 'Normal')</label>
                                            <select class="form-control" name="flag_procedure" id="flag_procedure">
                                                <option value="">--Pilih Prosedur--</option>
                                                <option value="0" <?=$req->flagProcedure === 0 ? 'selected' : ''?>>Prosedur Tidak Berkelanjutan</option>
                                                <option value="1" <?=$req->flagProcedure == 1 ? 'selected' : ''?>>Prosedur dan Terapi Berkelanjutan</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="nama" class="control-label">Penunjang (diisi jika tujuan kunjungan bukan 'Normal')</label>
                                            <select class="form-control" name="kd_penunjang" id="kd_penunjang">
                                                <option value="">--Pilih Penunjang--</option>
                                                <option value="1" <?=$req->kdPenunjang == 1 ? 'selected' : ''?>>Radioterapi</option>
                                                <option value="2" <?=$req->kdPenunjang == 2 ? 'selected' : ''?>>Kemoterapi</option>
                                                <option value="3" <?=$req->kdPenunjang == 3 ? 'selected' : ''?>>Rehabilitasi Medik</option>
                                                <option value="4" <?=$req->kdPenunjang == 4 ? 'selected' : ''?>>Rehabilitasi Psikososial</option>
                                                <option value="5" <?=$req->kdPenunjang == 5 ? 'selected' : ''?>>Transfusi Darah</option>
                                                <option value="6" <?=$req->kdPenunjang == 6 ? 'selected' : ''?>>Pelayanan Gigi</option>
                                                <option value="7" <?=$req->kdPenunjang == 7 ? 'selected' : ''?>>Laboratorium</option>
                                                <option value="8" <?=$req->kdPenunjang == 8 ? 'selected' : ''?>>USG</option>
                                                <option value="9" <?=$req->kdPenunjang == 9 ? 'selected' : ''?>>Farmasi</option>
                                                <option value="10" <?=$req->kdPenunjang == 10 ? 'selected' : ''?>>Lain-Lain</option>
                                                <option value="11" <?=$req->kdPenunjang == 11 ? 'selected' : ''?>>MRI</option>
                                                <option value="12" <?=$req->kdPenunjang == 12 ? 'selected' : ''?>>HEMODIALISA</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="nama" class="control-label">Assesment</label>
                                            <select class="form-control" name="assesment_pel" id="assesment_pel">
                                                <option value="">--Pilih Assesment--</option>
                                                <option value="1" <?=$req->assesmentPel == 1 ? 'selected' : ''?>>Poli spesialis tidak tersedia pada hari sebelumnya</option>
                                                <option value="2" <?=$req->assesmentPel == 2 ? 'selected' : ''?>>Jam Poli telah berakhir pada hari sebelumnya</option>
                                                <option value="3" <?=$req->assesmentPel == 3 ? 'selected' : ''?>>Dokter Spesialis yang dimaksud tidak praktek pada hari sebelumnya</option>
                                                <option value="4" <?=$req->assesmentPel == 4 ? 'selected' : ''?>>Atas Instruksi RS</option>
                                                <option value="5" <?=$req->assesmentPel == 5 ? 'selected' : ''?>>Tujuan Kontrol</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="nama" class="control-label">No Surat SKDP</label>
                                            <input type="text" class="form-control" name="no_surat" placeholder="Masukkan No Surat SKDP" value="<?=$req->skdp->noSurat?>">
                                        </div>
                                        <div class="form-group">
                                            <label for="nama" class="control-label">Dokter DPJP</label>
                                            <select type="text" class="form-control select2" id="kode_dpjp" name="kode_dpjp" required>
                                                <option value="">--Pilih Dokter DPJP--</option>
                                                <?php foreach ($dpjp as $v) : ?>
                                                    <option value="<?=$v['kode']?>" <?=$req->skdp->kodeDPJP == $v['kode'] ? 'selected' : ''?>><?=$v['nama']?></option>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="nama" class="control-label">DPJP Layanan (tidak diisi jika jenis pelayanan adalah Ranap)</label>
                                            <select type="text" class="form-control select2" id="dpjp_layan" name="dpjp_layan">
                                                <option value="">--Pilih DPJP Layanan--</option>
                                                <?php foreach ($dpjp as $v) : ?>
                                                    <option value="<?=$v['kode']?>" <?=$req->dpjpLayan == $v['kode'] ? 'selected' : ''?>><?=$v['nama']?></option>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="nama" class="control-label">No Telp</label>
                                            <input type="text" class="form-control" id="no_telp" name="no_telp" placeholder="Masukkan No Telp" value="<?=$req->noTelp?>">
                                        </div>
                                        <div class="form-group">
                                            <label for="nama" class="control-label">Nama User (pembuat SEP)</label>
                                            <input type="text" class="form-control" name="user" placeholder="Masukkan Nama User" value="<?=$nama?>" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="box-footer">
                                    <button type="submit" name="submit" value="1"
                                            class="btn btn-primary btn-lg btn-flat pull-right">Simpan
                                    </button>
                                    <a href="javascript:history.back()"
                                       class="btn btn-default btn-lg btn-flat pull-right">Batal</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>


<div id='ResponseInput'></div>
<!-- InputMask -->
<script src="<?= base_url() ?>assets/plugins/input-mask/jquery.inputmask.js"></script>
<script src="<?= base_url() ?>assets/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="<?= base_url() ?>assets/plugins/input-mask/jquery.inputmask.extensions.js"></script>
<script src="<?= base_url() ?>assets/bower_components/select2/dist/js/select2.full.min.js"></script>

<script>
    $(function () {
        $('#kd_propinsi').select2().change(function () {
            $('#kd_kabupaten').empty()
            ajax_search_kabupaten($(this).val())
        })

        $('#kd_kabupaten').select2().change(function () {
            $('#kd_kecamatan').empty()
            ajax_search_kecamatan($(this).val())
        })

        $('#kd_kecamatan').select2()

        $('#diag_awal').select2({
            ajax: {
                url: "<?= site_url('Bpjs/ajax_search_diagnosa'); ?>",
                type: "POST",
                data: function (params) {
                    return { keyword: params.term }
                },
                processResults: function (data) {
                    return {
                        results: JSON.parse(data).datanya.map(v => ({id: v.kode, text: v.nama}))
                    }
                }
            },
            minimumInputLength: 3
        })

        $('#poli_tujuan').select2({
            ajax: {
                url: "<?= site_url('Bpjs/ajax_search_poli'); ?>",
                type: "POST",
                data: function (params) {
                    return { keyword: params.term }
                },
                processResults: function (data) {
                    return {
                        results: JSON.parse(data).datanya.map(v => ({id: v.kode, text: v.nama}))
                    }
                }
            },
            minimumInputLength: 3
        })

        $('#pembiayaan').change(function () {
            if ($(this).val()) {
                const txt = $("#pembiayaan option:selected").text()
                $('#penanggung_jawab').val(txt)
            }
            else {
                $('#penanggung_jawab').val('')
            }
        })

    })

    const ajax_search_kabupaten = kode_provinsi => {
        $.ajax({
            url: "<?= site_url('Bpjs/ajax_search_kabupaten'); ?>",
            type: "POST",
            cache: false,
            data: 'keyword=' + kode_provinsi,
            dataType: 'json',
            success: function (json) {
                if (json.status === 1) {
                    const data = json.datanya.map(v => ({id: v.kode, text: v.nama}))
                    $('#kd_kabupaten').select2({ data })
                }
            }
        })
    }

    const ajax_search_kecamatan = kode_kabupaten => {
        $.ajax({
            url: "<?= site_url('Bpjs/ajax_search_kecamatan'); ?>",
            type: "POST",
            cache: false,
            data: 'keyword=' + kode_kabupaten,
            dataType: 'json',
            success: function (json) {
                if (json.status === 1) {
                    const data = json.datanya.map(v => ({id: v.kode, text: v.nama}))
                    $('#kd_kecamatan').select2({ data })
                }
            }
        })
    }

</script>

<script>

    $(function () {
        set_search(
            $('#cari_nomor_kartu'),
            (width, keyword) => { ajax_no_bpjs(width, keyword) },
            $('#hasil_pencarian'),
            $li => {
                const pasien_id = $li.find('span#pasien_id').html();
                const telepon = $li.find('span#telepon').html();
                const no_rm = $li.find('span#no_rm').html();
                const nama = $li.find('span#nama').html();
                const no_jaminan = $li.find('span#no_jaminan').html();
                if (no_jaminan) {
                    $('#cari_nomor_kartu').val(no_jaminan + ' - ' + nama);
                    $('#id_pasien').val(pasien_id);
                    $('#nomor_kartu').val(no_jaminan);
                    $('#no_mr').val(no_rm)
                    $('#no_telp').val(telepon)
                } else {
                    alert('data tidak ada! Pilih pilihan yang ada sebelum di enter.');
                }
                $('div#hasil_pencarian').hide();
            }
        )

        set_search(
            $('#cari_ppk_pelayanan'),
            (width, keyword) => {
                if (keyword.length > 2) {
                    ajax_search_faskes(width, keyword)
                }
            },
            $('#hasil_pencarian_ppk_pelayanan'),
            $li => {
                const kode = $li.find('span#kode').html();
                const nama = $li.find('span#nama').html();
                if (kode && nama) {
                    $('#cari_ppk_pelayanan').val(kode + ' - ' + nama);
                    $('#ppk_pelayanan').val(kode);
                } else {
                    alert('data tidak ada! Pilih pilihan yang ada sebelum di enter.');
                }
                $('div#hasil_pencarian_ppk_pelayanan').hide();
            }
        )

        set_search(
            $('#cari_ppk_rujukan'),
            (width, keyword) => {
                if (keyword.length > 2) {
                    ajax_search_faskes_2(width, keyword)
                }
            },
            $('#hasil_pencarian_ppk_rujukan'),
            $li => {
                const kode = $li.find('span#kode').html();
                const nama = $li.find('span#nama').html();
                if (kode && nama) {
                    $('#cari_ppk_rujukan').val(kode + ' - ' + nama);
                    $('#ppk_rujukan').val(kode);
                } else {
                    alert('data tidak ada! Pilih pilihan yang ada sebelum di enter.');
                }
                $('div#hasil_pencarian_ppk_rujukan').hide();
            }
        )
    })

    const set_search = ($input, search_ajax_fn, $display, on_selected) => {
        let timer = null
        const display_id = $display.attr('id')

        $input.on('keyup', function (e) {
            const s = $(this).val()
            if (s !== '') {
                clearTimeout(timer)
                timer = setTimeout(() => {
                    const charCode = ( e.which ) ? e.which : e.keyCode
                    if (charCode === 40) {
                        if ($('.form-group').find(`div#${display_id} li.autocomplete_active`).length > 0) {
                            var selanjutnya = $('.form-group').find(`div#${display_id} li.autocomplete_active`).next();
                            $('.form-group').find(`div#${display_id} li.autocomplete_active`).removeClass('autocomplete_active');
                            selanjutnya.addClass('autocomplete_active');
                        } else {
                            $('.form-group').find(`div#${display_id} li:first`).addClass('autocomplete_active');
                        }
                    }
                    else if (charCode === 38) {
                        if ($('.form-group').find(`div#${display_id} li.autocomplete_active`).length > 0) {
                            var sebelumnya = $('.form-group').find(`div#${display_id} li.autocomplete_active`).prev();
                            $('.form-group').find(`div#${display_id} li.autocomplete_active`).removeClass('autocomplete_active');
                            sebelumnya.addClass('autocomplete_active');
                        } else {
                            $('.form-group').find(`div#${display_id} li:first`).addClass('autocomplete_active');
                        }
                    }
                    else if (charCode === 13) {
                        on_selected($('.form-group').find(`div#${display_id} li.autocomplete_active`))
                    }
                    else {
                        search_ajax_fn($input.width(), s);
                    }
                }, 100)
            }
            else {
                $display.hide();
            }
        })

        $(document).on('click', `div#${display_id} .daftar-autocomplete li`, function () {
            on_selected($(this))
            $display.hide();
        })
    }

    const ajax_no_bpjs = (width, s) => {
        $('div#hasil_pencarian').hide()
        const Lebar = width + 25
        $.ajax({
            url: "<?= site_url('Bpjs/ajax_nomor_kartu'); ?>",
            type: "POST",
            cache: false,
            data: 'keyword=' + s,
            dataType: 'json',
            success: function (json) {
                if (json.status === 1) {
                    $('div#hasil_pencarian').css({'width': Lebar + 'px'});
                    $('div#hasil_pencarian').show('fast');
                    $('div#hasil_pencarian').html(json.datanya);
                }
                if (json.status === 0) {
                    $('div#hasil_pencarian').html('');
                }
            }
        })
    }

    const ajax_search_faskes = (width, s) => {
        $('div#hasil_pencarian_ppk_pelayanan').hide()
        const Lebar = width + 25
        $.ajax({
            url: "<?= site_url('Bpjs/ajax_search_faskes'); ?>",
            type: "POST",
            cache: false,
            data: 'keyword=' + s,
            dataType: 'json',
            success: function (json) {
                if (json.status === 1) {
                    $('div#hasil_pencarian_ppk_pelayanan').css({'width': Lebar + 'px'});
                    $('div#hasil_pencarian_ppk_pelayanan').show('fast');
                    $('div#hasil_pencarian_ppk_pelayanan').html(json.datanya);
                }
                if (json.status === 0) {
                    $('div#hasil_pencarian_ppk_pelayanan').html('');
                }
            }
        })
    }

    const ajax_search_faskes_2 = (width, s) => {
        $('div#hasil_pencarian_ppk_rujukan').hide()
        const Lebar = width + 25
        $.ajax({
            url: "<?= site_url('Bpjs/ajax_search_faskes'); ?>",
            type: "POST",
            cache: false,
            data: 'keyword=' + s,
            dataType: 'json',
            success: function (json) {
                if (json.status === 1) {
                    $('div#hasil_pencarian_ppk_rujukan').css({'width': Lebar + 'px'});
                    $('div#hasil_pencarian_ppk_rujukan').show('fast');
                    $('div#hasil_pencarian_ppk_rujukan').html(json.datanya);
                }
                if (json.status === 0) {
                    $('div#hasil_pencarian_ppk_rujukan').html('');
                }
            }
        })
    }

</script>
