<?php

function DateToIndo($date) {
    $BulanIndo = array("Januari", "Februari", "Maret", "April",
        "Mei", "Juni", "Juli", "Agustus", "September", "Oktober",
        "November", "Desember");
    $tgl = substr($date, 0, 2);
    $bulan = substr($date, 3, 2);
    $tahun = substr($date, 6, 4);
    $result = $tgl . " " . $BulanIndo[(int)$bulan-1] . " ". $tahun;
    $r = explode(' ', $date);
    return($result.' '.$r[1].' WIB');
}

?>

<html>
<head>
    <script
            src="https://code.jquery.com/jquery-3.4.1.min.js"
            integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
            crossorigin="anonymous"></script>
    <title>SEP</title>
    <style>
        .container {
            width: 100%;
            margin: auto;
        }

        .one {
            width: 55%;
            float: left;
        }

        .two {
            margin-left: 55%;
        }
    </style>
</head>
<body>
    <div style="display: flex; margin-bottom: 16px">
        <div style="width: 200px">
            <img src="<?=base_url('/assets/img/logo-bpjs.png')?>" style="display: block; width: 100%; height: auto;">
        </div>
        <p style="padding: 0; margin: 0 0 0 16px"><b>SURAT ELEGIBILITAS PESERTA<br>KLINIK UTAMA SUKMA WIJAYA</b></p>
    </div>
    <div class="container">
        <div class="one">
            <table>
                <tr>
                    <td>No SEP</td>
                    <td>:</td>
                    <td><?=$sep->noSep?></td>
                </tr>
                <tr>
                    <td>Tgl SEP</td>
                    <td>:</td>
                    <td><?=$sep->tglSep?></td>
                </tr>
                <tr>
                    <td>No Kartu</td>
                    <td>:</td>
                    <td><?=$sep->peserta->noKartu?> (MR <?=$sep->peserta->noMr?>)</td>
                </tr>
                <tr>
                    <td>Nama Peserta</td>
                    <td>:</td>
                    <td><?=strtoupper($sep->peserta->nama)?></td>
                </tr>
                <tr>
                    <td>Tgl Lahir</td>
                    <td>:</td>
                    <td><?=$sep->peserta->tglLahir . ' Kelamin: ' . ($sep->peserta->kelamin == 'P' ? 'Perempuan' : 'Laki-laki')?></td>
                </tr>
                <tr>
                    <td>No Telepon</td>
                    <td>:</td>
                    <td><?=$request ? $request->noTelp : $pasien->telepon?></td>
                </tr>
                <tr>
                    <td>Sub/Spesialis</td>
                    <td>:</td>
                    <td><?=$sep->poli?></td>
                </tr>
                <tr>
                    <td>Dokter</td>
                    <td>:</td>
                    <td><?=strtoupper($dpjp)?></td>
                </tr>
                <tr>
                    <td>Faskes Perujuk</td>
                    <td>:</td>
                    <td>KLINIK UTAMA SUKMA WIJAYA</td>
                </tr>
                <tr>
                    <td>Diagnosa Awal</td>
                    <td>:</td>
                    <td><?=$diagnosa?></td>
                </tr>
                <tr>
                    <td>Catatan</td>
                    <td>:</td>
                    <td><?=$sep->catatan?></td>
                </tr>
            </table>
        </div>
        <div class="two">
            <table>
                <tr>
                    <td colspan="3"></td>
                </tr>
                <tr>
                    <td colspan="3"></td>
                </tr>
                <tr>
                    <td>Peserta</td>
                    <td>:</td>
                    <td><?=$sep->peserta->jnsPeserta?></td>
                </tr>
                <tr>
                    <td colspan="3"></td>
                </tr>
                <tr>
                    <td colspan="3"></td>
                </tr>
                <tr>
                    <td>Jns Rawat</td>
                    <td>:</td>
                    <td><?=$sep->jnsPelayanan?></td>
                </tr>
                <tr>
                    <td>Jns Kunjungan</td>
                    <td>:</td>
                    <td>
                        <?php if ($request) : ?>
                            <?=$request->tujuanKunj == 0 ? 'Normal' : ($request->tujuanKunj == 1 ? 'Prosedur' : 'Konsul Dokter')?>
                        <?php else: ?>
                        -
                        <?php endif; ?>
                    </td>
                </tr>
                <tr>
                    <td colspan="3"></td>
                </tr>
                <tr>
                    <td colspan="3"></td>
                </tr>
                <tr>
                    <td>Poli Perujuk</td>
                    <td>:</td>
                    <td><?=$sep->poli?></td>
                </tr>
                <tr>
                    <td>Kls Hak</td>
                    <td>:</td>
                    <td><?=$sep->peserta->hakKelas?></td>
                </tr>
                <tr>
                    <td>Kls Rawat</td>
                    <td>:</td>
                    <td><?=$sep->kelasRawat?></td>
                </tr>
                <tr>
                    <td>Penjamin</td>
                    <td></td>
                    <td></td>
                </tr>
            </table>
        </div>
    </div>
    <div style="clear: both"></div>
    <div class="container" style="margin-top: 16px">
        <div class="one">
            <small style="font-size: 11px">*Saya menyetujui BPJS Kesehatan menggunakan infomasi medis pasien jika diperlukan.</small><br>
            <small style="font-size: 11px">*SEP Bukan sebagai bukti penjaminan peserta.</small><br>
            <small style="font-size: 11px">** Dengan diterbitkannya SEP ini, Peserta rawat inap telah mendapatkan informasi dan menempati kelas rawat sesuai hak kelasnya (terkecuali kelas penuh atau naik kelas sesuai aturan yang berlaku)</small>
            <br>
            <br>
            <small style="font-size: 11px">Cetakan ke <?=$data->print_ke?> <?=DateToIndo(date('d-m-Y H:i'))?></small>
        </div>
        <div class="two" style="margin-top: 16px; text-align: center">
            <p style="margin: 0">
                Pasien/Keluarga Pasien<br>
                ____________________
            </p>
        </div>
    </div>
    <script>
        $( document ).ready(function() {
            window.print();
        });
    </script>
</body>
</html>