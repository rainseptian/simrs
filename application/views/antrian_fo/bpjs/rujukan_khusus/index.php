<link rel="stylesheet" href="<?= base_url() ?>assets/plugins/iCheck/all.css">
<!--<link rel="stylesheet" href="--><?//= base_url() ?><!--assets/bower_components/select2/dist/css/select2.min.css">-->
<link rel="stylesheet" href="<?= base_url() ?>assets/plugins/autocomplete/style-gue.css">
<style media="screen">
    .select2-container {
        width: 100% !important;
    }
</style>

<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Rujukan Khusus
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?= base_url() ?>Dashbord"><i class="fa fa-dashboard"></i> Dashbord</a></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-danger">
                    <div class="box-header with-border">
                        <h3 class="box-title" style="padding-top: 6px">Pembuatan Rujukan</h3>
                    </div>
                    <div class="box-body">
                        <div class="box-body">
                            <div class="row">
                                <div class="col-12 col-md-6 col-lg-4" id="c-1">
                                    <form id="f-1" method="post" action="<?=base_url()?>bpjs/rujukan/create">
                                        <div class="form-group">
                                            <label for="nama" class="control-label">Pelayanan</label>
                                            <select class="form-control select2" required
                                                    style="width: 100%;"
                                                    name="pelayanan_rujuk_khusus"
                                                    id="pelayanan_rujuk_khusus"
                                                    data-placeholder="Pelayanan">
                                                <option value="0">Pilih Pelayanan</option>
                                                <option value="1">Hemodialisa (HDL)</option>
                                                <option value="2">Thalasemia - Hemofilia</option>
                                                <option value="3">Pelayanan CAPD</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="nama" class="control-label">No Rujukan (FKTP)</label>
                                            <input type="text" class="form-control" name="no_rujukan" id="no_rujukan" placeholder="Masukkan No Rujukan (FKTP)" required>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="box-footer">
                        <button type="button" id="b-cari" value="1" class="btn btn-primary">
                            Cari
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<div id='ResponseInput'></div>

<script src="<?= base_url() ?>assets/plugins/input-mask/jquery.inputmask.js"></script>
<script src="<?= base_url() ?>assets/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="<?= base_url() ?>assets/plugins/input-mask/jquery.inputmask.extensions.js"></script>
<script src="<?= base_url() ?>assets/bower_components/select2/dist/js/select2.full.min.js"></script>
<script src="<?php echo base_url(); ?>assets/bower_components/toast-alert/toastr.js"></script>

<script>
    $(function () {
        <?php $warning = $this->session->flashdata('warning');
        if (!empty($warning)) { ?>
        Swal.fire({
            icon: 'error',
            title: '<?=$warning?>',
        }).then((result) => {

        })
        <?php } ?>
        <?php $success = $this->session->flashdata('success');
        if (!empty($success)) { ?>
        Swal.fire({
            icon: 'success',
            title: '<?=$success?>',
            showCancelButton: true,
            // confirmButtonText: 'Cetak SEP',
        }).then((result) => {
            if (result.isConfirmed) {
                const url = `<?php echo base_url(); ?>bpjs/sep/print_sep`
                window.open(url, '_blank').focus();
            }
        })
        <?php } ?>
    })
</script>

<script>
    $(function () {
        $('#b-cari').click(function () {
            const nmr = $('#no_rujukan').val()
            if (!nmr) {
                toastr.error('Masukkan nomor rujukan dulu')
                return
            }

            const onSuccess = (sep) => {
                $('#sep').val(JSON.stringify(sep))
                $('#f-1').submit()
            }

            toastr.error('Data tidak ditemukan')
            // search_sep(nmr, onSuccess)
        })
    })

    const search_sep = (nmr, f) => {
        $.ajax({
            url: "<?= site_url('Bpjs/ajax_search_sep'); ?>",
            type: "POST",
            cache: false,
            data: `keyword=${nmr}`,
            dataType: 'json',
            success: function (json) {
                if (json.status === 1) {
                    f(json.datanya)
                }
                else if (json.status === 0) {
                    toastr.error(json.server_output?.metaData?.message ?? 'Gagal mendapatkan data')
                }
            }
        })
    }

</script>
