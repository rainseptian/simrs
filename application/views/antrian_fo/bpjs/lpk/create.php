<link rel="stylesheet" href="<?= base_url() ?>assets/plugins/iCheck/all.css">
<!--<link rel="stylesheet" href="--><?//= base_url() ?><!--assets/bower_components/select2/dist/css/select2.min.css">-->
<link rel="stylesheet" href="<?= base_url() ?>assets/plugins/autocomplete/style-gue.css">
<style media="screen">
    .select2-container {
        width: 100% !important;
    }
</style>

<div class="content-wrapper">
    <section class="content-header">
        <h1>
            LPK
            <small>Preview</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?= base_url() ?>Dashbord"><i class="fa fa-dashboard"></i> Dashbord</a></li>
            <li class="active">Tambah LPK</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-danger">
                    <div class="box-header">
                        <h3 class="box-title">Tambah LPK</h3>
                    </div>
                    <?php $warning = $this->session->flashdata('warning');
                    if (!empty($warning)) { ?>
                        <div class="alert alert-warning alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <h4><i class="icon fa fa-warning"></i> Alert!</h4>
                            <?= $warning ?>
                        </div>
                    <?php } ?>
                    <?php $success = $this->session->flashdata('success');
                    if (!empty($success)) { ?>
                        <div class="alert alert-success alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <h4><i class="icon fa fa-check"></i> Success!</h4>
                            <?= $success ?>
                        </div>
                    <?php } ?>
                    <div class="box-body">
                        <form class="form" method="post" action="<?= base_url() ?>bpjs/lpk/create">
                            <div class="box-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="nama" class="control-label">Nomor SEP</label>
                                            <input type="text" class="form-control" id="cari_nomor_sep"
                                                   name="cari_nomor_sep"
                                                   placeholder="Masukkan nomor SEP / nama pasien" required>
                                            <input type="hidden" name="nomor_sep" id='nomor_sep'>
                                            <input type="hidden" name="id_pasien" id='id_pasien'>
                                            <div id='hasil_pencarian' class='hasil_pencarian'></div>
                                        </div>
                                        <div class="form-group">
                                            <label for="nama" class="control-label">Tgl Masuk</label>
                                            <input type="date" class="form-control" name="tgl_masuk" id="tgl_masuk" placeholder="Masukkan Tgl Masuk" required>
                                        </div>
                                        <div class="form-group">
                                            <label for="nama" class="control-label">Tgl Keluar</label>
                                            <input type="date" class="form-control" name="tgl_keluar" id="tgl_keluar" placeholder="Masukkan Tgl Keluar" required>
                                        </div>
                                        <div class="form-group">
                                            <label for="nama" class="control-label">Jaminan</label>
                                            <select class="form-control" name="jaminan" required>
                                                <option value="1">JKN</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="nama" class="control-label">Poli</label>
                                            <select class="form-control select2" required
                                                   style="width: 100%;"
                                                   name="poli"
                                                   id="poli"
                                                   data-placeholder="Masukkan Poli">
                                            </select>
                                        </div>
                                        <hr>
                                        <div class="form-group">
                                            <label for="nama" class="control-label">Ruang Rawat</label>
                                            <select class="form-control select2"
                                                    style="width: 100%;"
                                                    name="ruang_rawat"
                                                    id="ruang_rawat"
                                                    data-placeholder="Pilih Ruang Rawat">
                                                <option value="">--Pilih Ruang Rawat--</option>
                                                <?php foreach ($ruang_rawat as $v) : ?>
                                                    <option value="<?=$v->kode?>"><?=$v->nama?></option>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="nama" class="control-label">Kelas Rawat <i>(Tidak Boleh Diisi Jika Pelayanan Rawat Jalan)</i></label>
                                            <select class="form-control select2"
                                                    style="width: 100%;"
                                                    name="kelas_rawat"
                                                    id="kelas_rawat"
                                                    data-placeholder="Pilih Kelas Rawat">
                                                <option value="">--Pilih Kelas Rawat--</option>
                                                <?php foreach ($kelas_rawat as $v) : ?>
                                                    <option value="<?=$v->kode?>"><?=$v->nama?></option>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="nama" class="control-label">Spesialistik <i>(Tidak Boleh Diisi Jika Pelayanan Rawat Jalan)</i></label>
                                            <select class="form-control select2"
                                                    style="width: 100%;"
                                                    name="spesialistik"
                                                    id="spesialistik"
                                                    data-placeholder="Pilih Spesialistik">
                                                <option value="">--Pilih Spesialistik--</option>
                                                <?php foreach ($spesialistik as $v) : ?>
                                                    <option value="<?=$v->kode?>"><?=$v->nama?></option>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="nama" class="control-label">Cara Keluar <i>(Tidak Boleh Diisi Jika Pelayanan Rawat Jalan)</i></label>
                                            <select class="form-control select2"
                                                    style="width: 100%;"
                                                    name="cara_keluar"
                                                    id="cara_keluar"
                                                    data-placeholder="Pilih Cara Keluar">
                                                <option value="">--Pilih Cara Keluar--</option>
                                                <?php foreach ($cara_keluar as $v) : ?>
                                                    <option value="<?=$v->kode?>"><?=$v->nama?></option>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="nama" class="control-label">Kondisi Pulang <i>(Tidak Boleh Diisi Jika Pelayanan Rawat Jalan)</i></label>
                                            <select class="form-control select2"
                                                    style="width: 100%;"
                                                    name="kondisi_pulang"
                                                    id="kondisi_pulang"
                                                    data-placeholder="Pilih Kondisi Pulang">
                                                <option value="">--Pilih Kondisi Pulang--</option>
                                                <?php foreach ($kondisi_pulang as $v) : ?>
                                                    <option value="<?=$v->kode?>"><?=$v->nama?></option>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="diagnosa-ctr">
                                            <div class="form-group">
                                                <label for="nama" class="control-label">Diagnosa</label>
                                                <select class="form-control select2" required
                                                        style="width: 100%;"
                                                        name="diagnosa_1"
                                                        id="diagnosa_1"
                                                        data-placeholder="Masukkan Diagnosa">
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label for="nama" class="control-label">Level</label>
                                                <br>
                                                <label style="font-weight: normal">
                                                    <input type="radio" name="level_1" value="1" required checked> Primer
                                                </label>
                                                <label style="font-weight: normal; margin-left: 8px">
                                                    <input type="radio" name="level_1" value="2" required> Sekunder
                                                </label>
                                            </div>
                                        </div>
                                        <input type="hidden" name="diagnosa_count" id="diagnosa_count" value="1">
                                        <div class="text-center">
                                            <button type="button" id="btn-add-diagnosa" class="btn btn-sm btn-success">Tambah Diagnosa</button>
                                        </div>
                                        <hr>
                                        <div class="form-group">
                                            <label for="nama" class="control-label">Procedure/Tindakan</label>
                                            <select class="form-control select2"
                                                    multiple
                                                    style="width: 100%;"
                                                    name="procedure[]"
                                                    id="procedure"
                                                    data-placeholder="Pilih Procedure/Tindakan">
                                                <option value="">--Pilih Procedure/Tindakan--</option>
                                            </select>
                                        </div>
                                        <hr>
                                        <div class="form-group">
                                            <label for="nama" class="control-label">Rencana Tindak Lanjut</label>
                                            <br>
                                            <label style="font-weight: normal">
                                                <input type="radio" name="tindakLanjut" value="1" required checked> Diperbolehkan Pulang
                                            </label>
                                            <label style="font-weight: normal; margin-left: 8px">
                                                <input type="radio" name="tindakLanjut" value="2" required> Pemeriksaan Penunjang
                                            </label>
                                            <label style="font-weight: normal; margin-left: 8px">
                                                <input type="radio" name="tindakLanjut" value="3" required> Dirujuk Ke
                                            </label>
                                            <label style="font-weight: normal; margin-left: 8px">
                                                <input type="radio" name="tindakLanjut" value="4" required> Kontrol Kembali
                                            </label>
                                        </div>
                                        <div class="form-group" id="dirujuk-ke" style="display: none">
                                            <label for="nama" class="control-label">Dirujuk Ke</label>
                                            <input type="text" class="form-control" id="cari_ppk_rujukan"
                                                   name="cari_ppk_rujukan"
                                                   placeholder="Masukkan Faskes Rujukan">
                                            <input type="hidden" name="ppk_rujukan" id='ppk_rujukan'>
                                            <div id='hasil_pencarian_ppk_rujukan' class='hasil_pencarian'></div>
                                        </div>
                                        <div class="form-group control-kembali" style="display: none">
                                            <label for="nama" class="control-label">Tgl Kontrol</label>
                                            <input type="date" class="form-control" name="tgl_kontrol" id="tgl_kontrol" placeholder="Masukkan Tgl Kontrol">
                                        </div>
                                        <div class="form-group control-kembali" style="display: none">
                                            <label for="nama" class="control-label">Poli</label>
                                            <select class="form-control select2"
                                                    style="width: 100%;"
                                                    name="poli_kontrol"
                                                    id="poli_kontrol"
                                                    data-placeholder="Masukkan Poli">
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="nama" class="control-label">DPJP</label>
                                            <select type="text" class="form-control select2" id="dpjp_layan" name="dpjp_layan" required>
                                                <option value="">--Pilih DPJP--</option>
                                                <?php foreach ($dpjp as $v) : ?>
                                                    <option value="<?=$v['kode']?>"><?=$v['nama']?></option>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="nama" class="control-label">Nama User (pembuat LPK)</label>
                                            <input type="text" class="form-control" name="user" placeholder="Masukkan Nama User" value="<?=$nama?>" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="box-footer">
                                    <button type="submit" name="submit" value="1"
                                            class="btn btn-primary btn-lg btn-flat pull-right">Simpan
                                    </button>
                                    <a href="javascript:history.back()"
                                       class="btn btn-default btn-lg btn-flat pull-right">Batal</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>


<div id='ResponseInput'></div>
<!-- InputMask -->
<script src="<?= base_url() ?>assets/plugins/input-mask/jquery.inputmask.js"></script>
<script src="<?= base_url() ?>assets/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="<?= base_url() ?>assets/plugins/input-mask/jquery.inputmask.extensions.js"></script>
<script src="<?= base_url() ?>assets/bower_components/select2/dist/js/select2.full.min.js"></script>

<script>
    $(function () {

        $('#poli').select2({
            ajax: {
                url: "<?= site_url('Bpjs/ajax_search_poli'); ?>",
                type: "POST",
                data: function (params) {
                    return { keyword: params.term }
                },
                processResults: function (data) {
                    return {
                        results: JSON.parse(data).datanya.map(v => ({id: v.kode, text: v.nama}))
                    }
                }
            },
            minimumInputLength: 3
        })

        $('#ruang_rawat').select2()
        $('#kelas_rawat').select2()
        $('#spesialistik').select2()
        $('#cara_keluar').select2()
        $('#kondisi_pulang').select2()

        //const default_processor = data => ({
        //    results: JSON.parse(data).datanya.map(v => ({id: v.kode, text: v.nama}))
        //})
        //
        //const set_async_option = ($opt, url, process = default_processor) => {
        //    $opt.select2({
        //        ajax: {
        //            url,
        //            type: "POST",
        //            data: function (params) {
        //                return { keyword: params.term }
        //            },
        //            processResults: process
        //        },
        //        minimumInputLength: 1
        //    })
        //}
        //
        //set_async_option($('#spesialistik'), "<?//= site_url('bpjs/ajax/search_spesialistik'); ?>//")
        //set_async_option($('#cara_keluar'), "<?//= site_url('bpjs/ajax/search_cara_keluar'); ?>//")
    })
</script>

<script>

    $(function () {
        set_search(
            $('#cari_nomor_sep'),
            (width, keyword) => { ajax_no_sep(width, keyword) },
            $('#hasil_pencarian'),
            $li => {
                const pasien_id = $li.find('span#pasien_id').html();
                const no_rm = $li.find('span#no_rm').html();
                const nama = $li.find('span#nama').html();
                const no_sep = $li.find('span#no_sep').html();
                if (no_sep) {
                    $('#cari_nomor_sep').val(no_sep + ' - ' + nama);
                    $('#id_pasien').val(pasien_id);
                    $('#nomor_sep').val(no_sep);
                    $('#no_mr').val(no_rm)
                } else {
                    alert('data tidak ada! Pilih pilihan yang ada sebelum di enter.');
                }
                $('div#hasil_pencarian').hide();
            }
        )

        set_search(
            $('#cari_ppk_rujukan'),
            (width, keyword) => {
                if (keyword.length > 2) {
                    ajax_search_faskes_2(width, keyword)
                }
            },
            $('#hasil_pencarian_ppk_rujukan'),
            $li => {
                const kode = $li.find('span#kode').html();
                const nama = $li.find('span#nama').html();
                if (kode && nama) {
                    $('#cari_ppk_rujukan').val(kode + ' - ' + nama);
                    $('#ppk_rujukan').val(kode);
                } else {
                    alert('data tidak ada! Pilih pilihan yang ada sebelum di enter.');
                }
                $('div#hasil_pencarian_ppk_rujukan').hide();
            }
        )
    })

    const set_search = ($input, search_ajax_fn, $display, on_selected) => {
        let timer = null
        const display_id = $display.attr('id')

        $input.on('keyup', function (e) {
            const s = $(this).val()
            if (s !== '') {
                clearTimeout(timer)
                timer = setTimeout(() => {
                    const charCode = ( e.which ) ? e.which : e.keyCode
                    if (charCode === 40) {
                        if ($('.form-group').find(`div#${display_id} li.autocomplete_active`).length > 0) {
                            var selanjutnya = $('.form-group').find(`div#${display_id} li.autocomplete_active`).next();
                            $('.form-group').find(`div#${display_id} li.autocomplete_active`).removeClass('autocomplete_active');
                            selanjutnya.addClass('autocomplete_active');
                        } else {
                            $('.form-group').find(`div#${display_id} li:first`).addClass('autocomplete_active');
                        }
                    }
                    else if (charCode === 38) {
                        if ($('.form-group').find(`div#${display_id} li.autocomplete_active`).length > 0) {
                            var sebelumnya = $('.form-group').find(`div#${display_id} li.autocomplete_active`).prev();
                            $('.form-group').find(`div#${display_id} li.autocomplete_active`).removeClass('autocomplete_active');
                            sebelumnya.addClass('autocomplete_active');
                        } else {
                            $('.form-group').find(`div#${display_id} li:first`).addClass('autocomplete_active');
                        }
                    }
                    else if (charCode === 13) {
                        on_selected($('.form-group').find(`div#${display_id} li.autocomplete_active`))
                    }
                    else {
                        search_ajax_fn($input.width(), s);
                    }
                }, 100)
            }
            else {
                $display.hide();
            }
        })

        $(document).on('click', `div#${display_id} .daftar-autocomplete li`, function () {
            on_selected($(this))
            $display.hide();
        })
    }

    const ajax_no_sep = (width, s) => {
        $('div#hasil_pencarian').hide()
        const Lebar = width + 25
        console.log('oioioioi', s)
        $.ajax({
            url: "<?= site_url('bpjs/lpk/ajax_nomor_sep'); ?>",
            type: "POST",
            cache: false,
            data: 'keyword=' + s,
            dataType: 'json',
            success: function (json) {
                console.log(json)
                if (json.status === 1) {
                    $('div#hasil_pencarian').css({'width': Lebar + 'px'});
                    $('div#hasil_pencarian').show('fast');
                    $('div#hasil_pencarian').html(json.datanya);
                }
                if (json.status === 0) {
                    $('div#hasil_pencarian').html('');
                }
            },
            error: function (e) {
                console.log(e)
            }
        })
    }

    const ajax_search_faskes_2 = (width, s) => {
        $('div#hasil_pencarian_ppk_rujukan').hide()
        const Lebar = width + 25
        $.ajax({
            url: "<?= site_url('Bpjs/ajax_search_faskes'); ?>",
            type: "POST",
            cache: false,
            data: 'keyword=' + s,
            dataType: 'json',
            success: function (json) {
                if (json.status === 1) {
                    $('div#hasil_pencarian_ppk_rujukan').css({'width': Lebar + 'px'});
                    $('div#hasil_pencarian_ppk_rujukan').show('fast');
                    $('div#hasil_pencarian_ppk_rujukan').html(json.datanya);
                }
                if (json.status === 0) {
                    $('div#hasil_pencarian_ppk_rujukan').html('');
                }
            }
        })
    }
</script>

<script>
    const diagnosa_select2_opt = {
        ajax: {
            url: "<?= site_url('Bpjs/ajax_search_diagnosa'); ?>",
            type: "POST",
            data: function (params) {
                return { keyword: params.term }
            },
            processResults: function (data) {
                return {
                    results: JSON.parse(data).datanya.map(v => ({id: v.kode, text: v.nama}))
                }
            }
        },
        minimumInputLength: 3
    }

    $(function () {
        $(`#diagnosa_1`).select2(diagnosa_select2_opt)

        let diagnosa_count = 1

        const build_diagnosa = index => `
            <hr>
            <div class="form-group">
                <label for="nama" class="control-label">Diagnosa</label>
                <select class="form-control select2" required
                        style="width: 100%;"
                        name="diagnosa_${index}"
                        id="diagnosa_${index}"
                        data-placeholder="Masukkan Diagnosa">
                </select>
            </div>
            <div class="form-group">
                <label for="nama" class="control-label">Level</label>
                <br>
                <label style="font-weight: normal">
                    <input type="radio" name="level_${index}" value="1" required checked> Primer
                </label>
                <label style="font-weight: normal; margin-left: 8px">
                    <input type="radio" name="level_${index}" value="2" required> Sekunder
                </label>
            </div>
        `

        $('#btn-add-diagnosa').click(function () {
            $('.diagnosa-ctr').append(build_diagnosa(++diagnosa_count))
            $('#diagnosa_count').val(diagnosa_count)

            $(`#diagnosa_${diagnosa_count}`).select2(diagnosa_select2_opt)
        })
    })
</script>

<script>
    $(function () {
        $('#procedure').select2({
            ajax: {
                url: "<?= site_url('Bpjs/ajax_search_procedure'); ?>",
                type: "POST",
                data: function (params) {
                    return { keyword: params.term }
                },
                processResults: function (data) {
                    return {
                        results: JSON.parse(data).datanya.map(v => ({id: v.kode, text: v.nama}))
                    }
                }
            },
            minimumInputLength: 3
        })

        $('input[type=radio][name=tindakLanjut]').change(function() {
            $('#dirujuk-ke').css('display', 'none')
            $('.control-kembali').css('display', 'none')

            if (this.value === '3') {
                $('#dirujuk-ke').css('display', 'block')
            }
            else if (this.value === '4') {
                $('.control-kembali').css('display', 'block')
            }
            else {
                $('#dirujuk-ke').css('display', 'none')
                $('.control-kembali').css('display', 'none')
            }
        })

        $('#poli_kontrol').select2({
            ajax: {
                url: "<?= site_url('Bpjs/ajax_search_poli'); ?>",
                type: "POST",
                data: function (params) {
                    return { keyword: params.term }
                },
                processResults: function (data) {
                    return {
                        results: JSON.parse(data).datanya.map(v => ({id: v.kode, text: v.nama}))
                    }
                }
            },
            minimumInputLength: 3
        })

    })
</script>