<?php

function DateToIndo($date) {
    $BulanIndo = array("Januari", "Februari", "Maret", "April",
        "Mei", "Juni", "Juli", "Agustus", "September", "Oktober",
        "November", "Desember");
    $tgl = substr($date, 0, 2);
    $bulan = substr($date, 3, 2);
    $tahun = substr($date, 6, 4);
    $result = $tgl . " " . $BulanIndo[(int)$bulan-1] . " ". $tahun;
    $r = explode(' ', $date);
    return($result.' '.$r[1].' WIB');
}

?>

<html>
<head>
    <script
            src="https://code.jquery.com/jquery-3.4.1.min.js"
            integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
            crossorigin="anonymous"></script>
    <title>Rencana Kontrol</title>
    <style>
        .container {
            width: 100%;
            margin: auto;
        }

        .one {
            width: 55%;
            float: left;
        }

        .two {
            margin-left: 55%;
        }
    </style>
</head>
<body>
    <div style="display: flex; margin-bottom: 16px">
        <div style="width: 200px">
            <img src="<?=base_url('/assets/img/logo-bpjs.png')?>" style="display: block; width: 100%; height: auto;">
        </div>
        <p style="padding: 0; margin: 0 0 0 16px"><b>SURAT RENCANA <?=$data->type == 'spri' ? 'INAP' : 'KONTROL'?><br>KLINIK UTAMA SUKMA WIJAYA</b></p>
        <div style="flex: 1"></div>
        <p style="padding: 0; margin: 0 0 0 16px">No. <?=$data->type == 'spri' ? $response->noSPRI : $response->noSuratKontrol?><br></p>
    </div>
    <div class="container">
        <div>
            <p>Kepada Yth <?= $response->namaDokter ?></p>
            <p>Mohon Pemeriksaan dan Penanganan Lebih Lanjut :</p>
            <table>
                <tr>
                    <td>No.Kartu</td>
                    <td>:</td>
                    <td><?=$response->noKartu?></td>
                </tr>
                <tr>
                    <td>Nama Peserta</td>
                    <td>:</td>
                    <td><?=$response->nama?> (<?=$response->kelamin?>)</td>
                </tr>
                <tr>
                    <td>Tgl.Lahir</td>
                    <td>:</td>
                    <td><?=date('d-F-Y', strtotime($response->tglLahir))?></td>
                </tr>
                <tr>
                    <td>Diagnosa</td>
                    <td>:</td>
                    <td><?=$response->namaDiagnosa?></td>
                </tr>
                <tr>
                    <td>Rencana Kontrol</td>
                    <td>:</td>
                    <td><?=date('d-F-Y', strtotime($response->tglRencanaKontrol))?></td>
                </tr>
            </table>
        </div>
        <p>Demikian atas bantuannya,diucapkan banyak terima kasih.</p>
    </div>
    <div style="clear: both"></div>
    <div class="container" style="margin-top: 16px">
        <div class="one">
            <br>
            <br>
            <br>
            <small style="font-size: 11px">Tgl.Entri <?=date('Y-m-d', strtotime($data->created_at))?> Tgl.Cetak <?=DateToIndo(date('d-m-Y H:i'))?></small>
        </div>
        <div class="two" style="margin-top: 16px; text-align: center">
            <p style="margin: 0">
                Mengetahui DPJP,
                <br>
                <br>
                <br>
                <?= $response->namaDokter ?>
            </p>
        </div>
    </div>
    <script>
        $( document ).ready(function() {
            window.print();
        });
    </script>
</body>
</html>