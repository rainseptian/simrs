<link rel="stylesheet" href="<?= base_url() ?>assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Rencana Kontrol / Inap
            <small></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="active"> Rencana Kontrol / Inap</li>
        </ol>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Daftar Rencana Kontrol / Inap</h3>&nbsp;&nbsp;
                    </div>
                    <div class="box-body">
                        <table id="example2" class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>No</th>
                                <th>Tgl Dibuat</th>
                                <th>NO</th>
                                <th>Tipe</th>
                                <th>Status</th>
                                <th>Response</th>
                                <th>Aksi</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $no=1; foreach ($list as $row) { ?>
                                <?php $req = json_decode($row->request); ?>
                                <?php $res = json_decode($row->response); ?>
                                <tr>
                                    <td><?= $no; ?></td>
                                    <td><?= date('d-M-Y H:i', strtotime($row->created_at)); ?></td>
                                    <td><?= $row->type == 'spri' ? "No Kartu: $row->no_bpjs" : "No SEP: {$req->request->noSEP}"; ?></td>
                                    <td><?= $row->type == 'spri' ? 'Inap' : 'Kontrol' ?></td>
                                    <td>
                                        <?php if ($row->success): ?>
                                            <span class="label label-success">Berhasil</span>
                                        <?php else: ?>
                                            <?= ($res->metaData->code ?? '-1') != '200' ? '<span class="label label-danger">Gagal</span>' : '<span class="label label-success">Berhasil</span>'; ?>
                                        <?php endif; ?>
                                    </td>
                                    <td>
                                        <?php if ($row->success): ?>
                                            <?php if ($res->noSPRI): ?>
                                                No SPRI: <?= $res->noSPRI; ?>
                                            <?php else: ?>
                                                No Rencana Kontrol: <?= $res->noSuratKontrol; ?>
                                            <?php endif; ?>
                                        <?php else: ?>
                                            <?= $res->metaData->message ?? ''; ?>
                                        <?php endif; ?>
                                    </td>
                                    <td>
                                        <?php if ($row->success): ?>
                                            <a href="<?php echo base_url(); ?>bpjs/rencana_kontrol/detail_<?= $row->type == 'spri' ? 'inap' : 'kontrol' ?>/<?= $row->id; ?>">
                                                <button type="button" class="btn btn-primary btn-block btn-sm"><i class="fa fa-arrows"></i> Detail</button>
                                            </a>
                                            <div style="height: 4px"></div>
                                            <a href="<?php echo base_url(); ?>bpjs/rencana_kontrol/print_rencana_kontrol/<?= $row->id; ?>" target="_blank">
                                                <button type="button" class="btn btn-success btn-block btn-sm"><i class="fa fa-print"></i> Cetak Surat</button>
                                            </a>
                                        <?php endif; ?>
                                    </td>
                                </tr>
                                <?php $no++; } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<div class="modal fade" id="modal-detail" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 style="display: inline-block" class="modal-title" id="exampleModalLabel">Modal title</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" id="f">
                <table class="table table-bordered" id="tbl-detail">
                    <thead>
                    <tr>
                        <th>Key</th>
                        <th>Value</th>
                    </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<script>

    const parse = (obj, parent_name = '') => {
        for (let key in obj) {
            if (Array.isArray(obj[key])) {
                obj[key].forEach((v, k) => {
                    parse(v, key + '-' + (k + 1))
                })
            }
            else if (typeof obj[key] === 'object') {
                parse(obj[key], key)
            }
            else {
                $('#tbl-detail > tbody').append(`
                    <tr>
                        <td>${parent_name ? parent_name + ' -> ' + key : key}</td>
                        <td>${obj[key]}</td>
                    </tr>
                `)
            }
        }
    }

    function open_detail(id) {
        const list = <?=json_encode($list);?>;
        const v = list.find(v => parseInt(v.id) === parseInt(id))
        if (v) {
            $('#tbl-detail > tbody').html('')
            const request = JSON.parse(v.request).request.t_rencana_kontrol
            parse(request)

            $('#exampleModalLabel').html(`Detail Rencana Kontrol ${v.nama}`)
            $('#modal-detail').modal('show')
        }
    }

    $(function () {
        $('#example1').DataTable()
        $('#example2').DataTable({
            rowReorder: {
                selector: 'td:nth-child(2)'
            },
            'paging'      : true,
            'lengthChange': true,
            'searching'   : true,
            'ordering'    : true,
            'info'        : true,
            'autoWidth'   : true
        })
    })
</script>

<script>
    $(function () {
        <?php $warning = $this->session->flashdata('warning');
        if (!empty($warning)) { ?>
        Swal.fire({
            icon: 'error',
            title: '<?=$warning?>',
        }).then((result) => {

        })
        <?php } ?>
        <?php $success = $this->session->flashdata('success');
        if (!empty($success)) { ?>
        Swal.fire({
            icon: 'success',
            title: '<?=$success?>',
            // showCancelButton: true,
            // confirmButtonText: 'Cetak SEP',
        }).then((result) => {
            //if (result.isConfirmed) {
            //    const url = `<?php //echo base_url(); ?>//bpjs/sep/print_sep`
            //    window.open(url, '_blank').focus();
            //}
        })
        <?php } ?>
    })
</script>
