<link rel="stylesheet" href="<?= base_url() ?>assets/plugins/iCheck/all.css">
<!--<link rel="stylesheet" href="--><?//= base_url() ?><!--assets/bower_components/select2/dist/css/select2.min.css">-->
<link rel="stylesheet" href="<?= base_url() ?>assets/plugins/autocomplete/style-gue.css">
<style media="screen">
    .select2-container {
        width: 100% !important;
    }
    @media (min-width: 768px) {
        .modal-xl {
            width: 90%;
            max-width:1200px;
        }
    }
</style>

<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Detail Rencana Kontrol / Rencana Inap
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?= base_url() ?>Dashbord"><i class="fa fa-dashboard"></i> Dashbord</a></li>
            <li class="active">Detail Rencana Kontrol / Rencana Inap</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">

            <div class="col-md-3">
                <div id="divKontrolPanel" style="">
                    <div class="box box-solid box-primary">
                        <div class="box-header with-border">
                            <span><i class="fa fa-envelope"> SEP</i> </span>
                            <div class="box-tools">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse">
                                    <i class="fa fa-minus"></i>
                                </button>
                            </div>
                        </div>
                        <div class="box-body no-padding">
                            <ul class="nav nav-pills nav-stacked">
                                <li><a title="No.SEP"><i class="fa fa-sort-numeric-asc"></i> <label id="lblnosep"><?=$sep->noSep ?? ''?></label></a></li>
                                <li><a title="Tgl.SEP"><i class="fa fa-calendar"></i> <label id="lbltglsep"><?=$sep->tglSep ?? ''?></label></a></li>
                                <li><a title="Jns.Pelayanan"><i class="fa fa-medkit"></i> <label id="lbljenpel"><?=$sep->jnsPelayanan ?? ''?></label></a></li>
                                <li><a title="Poli"><i class="fa fa-bookmark-o"></i> <label id="lblpoli"><?=($poli->kode ?? '').' - '.($sep->poli ?? '')?></label></a></li>
                                <li><a title="Diagnosa"><i class="fa fa-heartbeat"></i> <label id="lbldiagnosa"><?=$sep->diagnosa ?? ''?></label></a></li>
                            </ul>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <div class="box box-solid box-success">
                        <div class="box-header with-border">
                            <span><i class="fa fa-hospital-o"> Asal Rujukan SEP</i> </span>
                            <div class="box-tools">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse">
                                    <i class="fa fa-minus"></i>
                                </button>
                            </div>
                        </div>
                        <div class="box-body no-padding">
                            <ul class="nav nav-pills nav-stacked">
                                <li><a title="No.Rujukan"><i class="fa fa-sort-numeric-asc"></i> <label id="lblnorujukan"><?=$sep->noRujukan ?? ''?></label></a></li>
                                <li><a title="Masa Aktif Rujukan"><i class="fa fa-calendar"></i> <label id="lbltglrujukan"><?=$sep->tglRujukan ?? ''?></label></a></li>
                                <li><a title="Faskes Asal Rujukan"><i class="fa fa-search"></i> <label id="lblfaskesasalrujukan"><?=$sep->faskesAsal ?? ''?></label></a></li>
                            </ul>
                        </div>
                        <!-- /.box-body -->
                    </div>
                </div>

                <!-- /. box -->
                <div class="box box-solid box-warning">
                    <div class="box-header with-border">
                        <span><i class="fa fa-user"> Peserta</i> </span>
                        <div class="box-tools">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse">
                                <i class="fa fa-minus"></i>
                            </button>
                        </div>
                    </div>
                    <div class="box-body no-padding">
                        <ul class="nav nav-pills nav-stacked">
                            <li><a title="No.Kartu"><i class="fa fa-sort-numeric-asc text-blue"></i> <label id="lblnokartu"><?=$sep->peserta->noKartu ?? ''?></label></a></li>
                            <li><a title="Nama Peserta"><i class="fa fa-user text-light-blue"></i> <label id="lblnmpeserta"><?=$sep->peserta->nama ?? ''?></label></a></li>
                            <li><a title="Tgl.Lahir"><i class="fa fa-calendar text-blue"></i> <label id="lbltgllhrpst"><?=$sep->peserta->tglLahir ?? ''?></label></a></li>
                            <li><a title="Kelamin"><i class="fa fa-intersex  text-blue"></i> <label id="lbljkpst"><?=$sep->peserta->kelamin ? ($sep->peserta->kelamin == 'L' ? 'Laki-laki' : 'Perempuan') : ''?></label></a></li>
                            <li><a title="Kelas Peserta"><i class="fa fa-user  text-blue"></i> <label id="lblklpst"><?=$sep->peserta->hakKelas ?? ''?></label></a></li>
                            <li><a title="PPK Asal Peserta"><i class="fa fa-user-md  text-blue"></i> <label id="lblppkpst"></label></a></li>
                        </ul>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>

            <div class="col-md-9">
                <form method="post" action="<?=base_url()?>bpjs/rencana_kontrol/update_kontrol">
                    <input type="hidden" name="noSEP" value="<?=$nomor?>">
                    <input type="hidden" name="data_id" value="<?=$data->id?>">

                    <div class="box box-danger">
                        <div class="box-header with-border">
                            <h3 class="box-title"></h3>
                        </div>
                        <div class="box-body">
                            <div class="row">
                                <div class="col-12 col-md-6">
                                    <div class="form-group">
                                        <label for="nama" class="control-label">Tgl Rencana Kontrol</label>
                                        <input type="date" class="form-control editable" id="tgl_rencana" name="tglRencanaKontrol" placeholder="Masukkan Tgl Rencana Kontrol" required readonly value="<?=$request->tglRencanaKontrol?>">
                                    </div>
                                    <div class="form-group">
                                        <label for="nama" class="control-label">Pelayanan</label>
                                        <select class="form-control laka_lantas" name="pelayanan" id="pelayanan" disabled>
                                            <option value="0">Rawat Jalan</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-12 col-md-6">
                                    <div class="form-group">
                                        <label for="nama" class="control-label">No. Surat Kontrol</label>
                                        <input type="text" class="form-control" name="noSuratKontrol" id="noSuratKontrol" placeholder="No. Surat Kontrol" readonly value="<?=$response->noSuratKontrol?>">
                                    </div>
                                    <div style="display: flex">
                                        <div class="form-group" style="flex: 1">
                                            <label for="nama" class="control-label">Spesialis/SubSpesialis</label>
                                            <input type="text" class="form-control" name="poli_kontrol_name" id="poli_kontrol_name" placeholder="Spesialis/SubSpesialis" readonly value="<?=$poli->nama?>">
                                            <input type="hidden" name="poli_kontrol" id='poli_kontrol' value="<?=$request->poliKontrol?>">
                                        </div>
                                        <div style="margin-left: 8px; margin-top: 24px">
                                            <button type="button" class="btn btn-primary editable" disabled id="bb"><i class="fa fa-search"></i> </button>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="nama" class="control-label">DPJP Tujuan Kontrol / Inap</label>
                                        <input type="text" class="form-control" name="kode_dokter_name" id="kode_dokter_name" placeholder="Spesialis/SubSpesialis" readonly value="<?=$response->namaDokter?>">
                                        <input type="hidden" name="kode_dokter" id="kode_dokter" value="<?=$request->kodeDokter?>">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="box-footer">
                            <button type="button" id="b-edit" class="btn btn-warning wk"><i class="fa fa-pencil"></i> Edit</button>
                            <button type="button" id="b-delete" class="btn btn-danger wk"><i class="fa fa-trash"></i> Hapus</button>
                            <button type="button" id="b-cetak" class="btn btn-success wk"><i class="fa fa-print"></i> Cetak</button>
                            <button type="submit" id="b-simpan" class="btn btn-success" style="display: none"><i class="fa fa-save"></i> Simpan</button>

                            <button type="button" class="btn btn-danger pull-right" onclick="window.history.go(-1); return false;">
                                <i class="fa fa-close"></i> Batal
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </section>
</div>

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel">Jadwal Praktek Rumah Sakit</h4>
            </div>
            <div class="modal-body">
                <div style="margin-left: 10px; margin-right: 10px">
                    <div class="row">
                        <div class="col-12" style="margin-top: 10px">
                            <div class="alert alert-info alert-dismissible" style="padding-left: 0 !important; padding-bottom: 4px !important;">
                                <ol>
                                    <li>Untuk Melihat Jadwal Praktek Dokter klik Nama Spesialis/SubSpesialis</li>
                                    <li>Jumlah Rencana Kontrol Merupakan Penjumlahan dari Rencana Kontrol Spesialis/Subspesialis Per Tanggal</li>
                                    <li>Kapasitas Merupakan Jumlah Maksimal Layanan yang Dapat dilayani Oleh Spesialis/Subspesialis</li>
                                </ol>
                            </div>
                        </div>
                        <div class="col-12" style="margin-top: 10px">
                            <table class="table table-bordered" id="tbl-sp" style="margin-top: 10px">
                                <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Nama Spesialis/Sub</th>
                                    <th>Kapasitas</th>
                                    <th>Jml Rencana Kontrol & Rujukan</th>
                                    <th>Prosentase</th>
                                </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel">Jadwal Praktek Dokter</h4>
            </div>
            <div class="modal-body">
                <div style="margin-left: 10px; margin-right: 10px">
                    <div class="row">
                        <div class="col-12" style="margin-top: 10px">
                            <table class="table table-bordered" id="tbl-dokter" style="margin-top: 10px">
                                <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Nama Dokter</th>
                                    <th>Jadwal Praktek</th>
                                    <th>Kapasitas</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
            </div>
        </div>
    </div>
</div>

<div id='ResponseInput'></div>

<script src="<?= base_url() ?>assets/plugins/input-mask/jquery.inputmask.js"></script>
<script src="<?= base_url() ?>assets/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="<?= base_url() ?>assets/plugins/input-mask/jquery.inputmask.extensions.js"></script>
<script src="<?= base_url() ?>assets/bower_components/select2/dist/js/select2.full.min.js"></script>
<script src="<?php echo base_url(); ?>assets/bower_components/toast-alert/toastr.js"></script>

<script>
    $(function () {
        const data = <?= json_encode($data); ?>;
        const response = <?= json_encode($response); ?>;
        $('#b-cetak').click(function () {
            const url = `<?php echo base_url(); ?>bpjs/rencana_kontrol/print_rencana_kontrol/${data.id}`
            window.open(url, '_blank').focus();
        })
        $('#b-delete').click(function () {
            if (confirm('Yakin hapus rencana kontrol ini?')) {
                const url = `<?php echo base_url(); ?>bpjs/rencana_kontrol/delete_kontrol/${data.id}`
                window.open(url, '_self')
            }
        })
        $('#b-edit').click(function () {
            $('.editable').prop('readonly', false).attr("disabled", false)
            $('#b-simpan').css('display', 'inline-block')
            $('.wk').css('display', 'none')
        })
    })
</script>

<script>
    $(function () {
        $('#bb').click(function () {
            $('#tbl-sp tbody').empty()
            $('#exampleModal').modal('show')

            search_spesialistik(r => {
                r.forEach((v, k) => {
                    $('#tbl-sp tbody').append(`
                        <tr>
                            <td>${k + 1}</td>
                            <td>
                                <button type="button" class="btn btn-secondary btn-sm" onclick="select_spesialistik('${v.kodePoli}', '${v.namaPoli}')">${v.namaPoli}</button>
                            </td>
                            <td>${v.kapasitas}</td>
                            <td>${v.jmlRencanaKontroldanRujukan}</td>
                            <td>${v.persentase}</td>
                        </tr>
                    `)
                })
            })
        })
    })

    function select_spesialistik(kodeSpesialis, namaSpesialis) {
        $('#poli_kontrol').val(kodeSpesialis)
        $('#poli_kontrol_name').val(`${kodeSpesialis} - ${namaSpesialis}`)

        search_jadwal_dokter(r => {
            $('#exampleModal').modal('hide')

            if (r && r.length) {
                $('#tbl-dokter tbody').empty()
                r.forEach((v, k) => {
                    $('#tbl-dokter tbody').append(`
                        <tr>
                            <td>${k + 1}</td>
                            <td>${v.namaDokter}</td>
                            <td>${v.jadwalPraktek}</td>
                            <td>${v.kapasitas}</td>
                            <td>
                                <button class="btn btn-success btn-sm" onclick="select_dokter('${v.kodeDokter}', '${v.namaDokter}')"><i class="fa fa-check"></i> </button>
                            </td>
                        </tr>
                    `)
                })
                $('#exampleModal2').modal('show')
            }
            else {
                toastr.error(`Tidak ada jadwal dokter ${namaSpesialis} di tanggal ${$('#tgl_rencana').val()}`)
            }
        })
    }

    function select_dokter(kodeDokter, namaDokter) {
        $('#exampleModal2').modal('hide')
        $('#kode_dokter').val(kodeDokter)
        $('#kode_dokter_name').val(namaDokter)
    }

    const search_spesialistik = (f) => {
        $.ajax({
            url: "<?= site_url('bpjs/rencana_kontrol/ajax_search_spesialistik_rencana_kontrol'); ?>",
            type: "POST",
            cache: false,
            dataType: 'json',
            data: `jenis_kontrol=<?=$jenis_kontrol?>&nomor=<?=$nomor?>&tgl=${$('#tgl_rencana').val()}`,
            success: function (json) {
                if (json.status === 1) {
                    f(json.datanya.list)
                }
                if (json.status === 0) {
                    toastr.error(json.server_output?.metaData?.message ?? 'Gagal mendapatkan data')
                }
            }
        })
    }

    const search_jadwal_dokter = (f) => {
        $.ajax({
            url: "<?= site_url('bpjs/rencana_kontrol/ajax_search_jadwal_dokter_rencana_kontrol'); ?>",
            type: "POST",
            cache: false,
            dataType: 'json',
            data: `jenis_kontrol=<?=$jenis_kontrol?>&kode_poli=${$('#poli_kontrol').val()}&tgl=${$('#tgl_rencana').val()}`,
            success: function (json) {
                if (json.status === 1) {
                    f(json.datanya.list)
                }
                if (json.status === 0) {
                    toastr.error(json.server_output?.metaData?.message ?? 'Gagal mendapatkan data')
                }
            }
        })
    }
</script>