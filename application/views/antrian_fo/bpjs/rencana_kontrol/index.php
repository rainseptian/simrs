<link rel="stylesheet" href="<?= base_url() ?>assets/plugins/iCheck/all.css">
<!--<link rel="stylesheet" href="--><?//= base_url() ?><!--assets/bower_components/select2/dist/css/select2.min.css">-->
<link rel="stylesheet" href="<?= base_url() ?>assets/plugins/autocomplete/style-gue.css">
<style media="screen">
    .select2-container {
        width: 100% !important;
    }
</style>

<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Rencana Kontrol / Rencana Inap
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?= base_url() ?>Dashbord"><i class="fa fa-dashboard"></i> Dashbord</a></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-danger">
                    <div class="box-header with-border">
                        <h3 class="box-title" style="padding-top: 6px">Pembuatan Rencana Kontrol / Rencana Inap</h3>
                    </div>
                    <div class="box-body">
                        <div class="box-body">
                            <div class="row">
                                <div class="col-12 col-md-6 col-lg-4">
                                    <div class="form-group">
                                        <label for="nama" class="control-label">Pilih</label>
                                        <br>
                                        <label style="font-weight: normal">
                                            <input type="radio" name="rc_kontrol" value="1" required checked> Rencana Kontrol
                                        </label>
                                        <label style="font-weight: normal; margin-left: 8px">
                                            <input type="radio" name="rc_kontrol" value="2" required> Rencana Rawat Inap (SPRI)
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12 col-md-6 col-lg-4" id="c-1">
                                    <form id="f-1" method="post" action="<?=base_url()?>bpjs/rencana_kontrol/create_kontrol">
                                        <input type="hidden" name="sep" id="sep">
                                        <input type="hidden" name="spesialistik" id="spesialistik">
                                        <div class="form-group">
                                            <label for="nama" class="control-label">No SEP</label>
                                            <input type="text" class="form-control" name="no_sep" id="no_sep" placeholder="Masukkan No SEP" required>
                                        </div>
                                    </form>
                                </div>
                                <div class="col-12 col-md-6 col-lg-4" id="c-2" style="display: none">
                                    <form id="f-2" method="post" action="<?=base_url()?>bpjs/rencana_kontrol/create_ranap">
                                        <input type="hidden" name="peserta" id="peserta">
                                        <div class="form-group">
                                            <label for="nama" class="control-label">Tgl Rencana Inap</label>
                                            <input type="date" class="form-control" id="tgl_rancana" name="tgl_rancana" placeholder="Masukkan Tgl Rencana Inap" required value="<?=date('Y-m-d')?>">
                                        </div>
                                        <div class="form-group">
                                            <label for="nama" class="control-label">No Kartu</label>
                                            <input type="text" class="form-control" name="no_kartu" id="no_kartu" placeholder="Masukkan No Kartu" required>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="box-footer">
                        <button type="button" id="b-cari" value="1" class="btn btn-primary">
                            Cari
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<div id='ResponseInput'></div>

<script src="<?= base_url() ?>assets/plugins/input-mask/jquery.inputmask.js"></script>
<script src="<?= base_url() ?>assets/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="<?= base_url() ?>assets/plugins/input-mask/jquery.inputmask.extensions.js"></script>
<script src="<?= base_url() ?>assets/bower_components/select2/dist/js/select2.full.min.js"></script>
<script src="<?php echo base_url(); ?>assets/bower_components/toast-alert/toastr.js"></script>

<script>
    $(function () {
        <?php $warning = $this->session->flashdata('warning');
        if (!empty($warning)) { ?>
        Swal.fire({
            icon: 'error',
            title: '<?=$warning?>',
        }).then((result) => {

        })
        <?php } ?>
        <?php $success = $this->session->flashdata('success');
        if (!empty($success)) { ?>
        Swal.fire({
            icon: 'success',
            title: '<?=$success?>',
            // showCancelButton: true,
            // confirmButtonText: 'Cetak SEP',
        }).then((result) => {
            if (result.isConfirmed) {
                //const url = `<?php //echo base_url(); ?>//bpjs/sep/print_sep`
                //window.open(url, '_blank').focus();
            }
        })
        <?php } ?>
    })
</script>

<script>
    $(function () {
        $('input[type=radio][name=rc_kontrol]').change(function() {
            if (this.value === '1') {
                $('#c-1').css('display', 'block')
                $('#c-2').css('display', 'none')
            }
            else {
                $('#c-1').css('display', 'none')
                $('#c-2').css('display', 'block')
            }
        })

        $('#b-cari').click(function () {
            if ($('input[type=radio][name=rc_kontrol]:checked').val() === '1') { // rencana kontrol
                const nmr = $('#no_sep').val()
                if (!nmr) {
                    toastr.error('Masukkan nomor rujukan dulu')
                    return
                }

                search_sep(nmr, (sep) => {
                    $('#sep').val(JSON.stringify(sep))
                    search_spesialistik(2, nmr, '<?=date("Y-m-d", strtotime('tomorrow'))?>', r => {
                        $('#spesialistik').val(JSON.stringify(r))
                        $('#f-1').submit()
                    })
                })
            }
            else { // SPRI
                const nmr = $('#no_kartu').val()
                if (!nmr) {
                    toastr.error('Masukkan nomor dulu')
                    return
                }

                const onSuccess = (peserta) => {
                    $('#peserta').val(JSON.stringify(peserta))
                    $('#f-2').submit()
                }

                search_peserta_by_no_bpjs(nmr, onSuccess)
            }
        })
    })

    const search_sep = (nmr, f) => {
        $.ajax({
            url: "<?= site_url('Bpjs/ajax_search_sep'); ?>",
            type: "POST",
            cache: false,
            data: `keyword=${nmr}`,
            dataType: 'json',
            success: function (json) {
                if (json.status === 1) {
                    f(json.datanya)
                }
                else if (json.status === 0) {
                    toastr.error(json.server_output?.metaData?.message ?? 'Gagal mendapatkan data')
                }
            }
        })
    }

    const search_spesialistik = (jns_kontrol, nomor, tgl, f) => {
        $.ajax({
            url: "<?= site_url('bpjs/rencana_kontrol/ajax_search_spesialistik_rencana_kontrol'); ?>",
            type: "POST",
            cache: false,
            dataType: 'json',
            data: `jenis_kontrol=${jns_kontrol}&nomor=${nomor}&tgl=${tgl}`,
            success: function (json) {
                console.log({json})
                if (json.status === 1) {
                    f(json.datanya.list)
                }
                else if (json.status === 0) {
                    toastr.error(json.server_output?.metaData?.message ?? 'Gagal mendapatkan data')
                }
            }
        })
    }

    const search_peserta_by_no_bpjs = (nmr, f) => {
        $.ajax({
            url: "<?= site_url('Bpjs/ajax_search_peserta_by_no_bpjs'); ?>",
            type: "POST",
            cache: false,
            data: `keyword=${nmr}&tgl=${$('#tgl_rancana').val()}`,
            dataType: 'json',
            success: function (json) {
                if (json.status === 1) {
                    f(json.datanya.peserta)
                }
                else if (json.status === 0) {
                    toastr.error(json.server_output?.metaData?.message ?? 'Gagal mendapatkan data')
                }
            }
        })
    }

</script>
