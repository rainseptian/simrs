<link rel="stylesheet" href="<?= base_url() ?>assets/plugins/iCheck/all.css">
<!--<link rel="stylesheet" href="--><?//= base_url() ?><!--assets/bower_components/select2/dist/css/select2.min.css">-->
<link rel="stylesheet" href="<?= base_url() ?>assets/plugins/autocomplete/style-gue.css">
<style media="screen">
    .select2-container {
        width: 100% !important;
    }
    @media (min-width: 768px) {
        .modal-xl {
            width: 90%;
            max-width:1200px;
        }
    }
</style>

<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Pembuatan Rencana Kontrol / Rencana Inap
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?= base_url() ?>Dashbord"><i class="fa fa-dashboard"></i> Dashbord</a></li>
            <li class="active">Pembuatan Rencana Kontrol / Rencana Inap</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">

            <div class="col-md-3">
                <div class="box box-solid box-warning">
                    <div class="box-header with-border">
                        <span><i class="fa fa-user"> Peserta</i> </span>
                        <div class="box-tools">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse">
                                <i class="fa fa-minus"></i>
                            </button>
                        </div>
                    </div>
                    <div class="box-body no-padding">
                        <ul class="nav nav-pills nav-stacked">
                            <li><a title="No.Kartu"><i class="fa fa-sort-numeric-asc text-blue"></i> <label id="lblnokartu"><?=$peserta->noKartu?></label></a></li>
                            <li><a title="Nama Peserta"><i class="fa fa-user text-light-blue"></i> <label id="lblnmpeserta"><?=$peserta->nama?></label></a></li>
                            <li><a title="Tgl.Lahir"><i class="fa fa-calendar text-blue"></i> <label id="lbltgllhrpst"><?=$peserta->tglLahir?></label></a></li>
                            <li><a title="Kelamin"><i class="fa fa-intersex  text-blue"></i> <label id="lbljkpst"><?=$peserta->kelamin == 'L' ? 'Laki-laki' : 'Perempuan'?></label></a></li>
                            <li><a title="Kelas Peserta"><i class="fa fa-user  text-blue"></i> <label id="lblklpst"><?=$peserta->hakKelas?></label></a></li>
                            <li><a title="PPK Asal Peserta"><i class="fa fa-user-md  text-blue"></i> <label id="lblppkpst"></label></a></li>
                        </ul>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>

            <div class="col-md-9">
                <form method="post" action="<?=base_url()?>bpjs/rencana_kontrol/insert_ranap">
                    <input type="hidden" name="noKartu" value="<?=$peserta->noKartu?>">
                    <input type="hidden" name="poliKontrol" value="BED">

                    <div class="box box-danger">
                        <div class="box-header with-border">
                            <h3 class="box-title"><?=$jenis_sep?></h3>
                        </div>
                        <div class="box-body">
                            <div class="row">
                                <div class="col-12 col-md-6">
                                    <div class="form-group">
                                        <label for="nama" class="control-label">Tgl Rencana Kontrol</label>
                                        <input type="date" class="form-control" id="tgl_rencana" name="tglRencanaKontrol" placeholder="Masukkan Tgl Rencana Kontrol" readonly value="<?=$tgl_rencana?>">
                                    </div>
                                    <div class="form-group">
                                        <label for="nama" class="control-label">Pelayanan</label>
                                        <select class="form-control laka_lantas" name="pelayanan" id="pelayanan" disabled>
                                            <option value="0">Rawat Inap</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-12 col-md-6">
                                    <div class="form-group">
                                        <label for="nama" class="control-label">No. Surat Kontrol</label>
                                        <input type="text" class="form-control" name="no_surat_kontrol" id="no_surat_kontrol" placeholder="No. Surat Kontrol" readonly>
                                    </div>
                                    <div style="display: flex">
                                        <div class="form-group" style="flex: 1">
                                            <label for="nama" class="control-label">Spesialis/SubSpesialis</label>
                                            <input type="text" class="form-control" name="poli_kontrol" id="poli_kontrol" placeholder="Spesialis/SubSpesialis" readonly>
                                            <input type="hidden" name="poli_kontrol_name" id="poli_kontrol_name">
                                        </div>
                                        <div style="margin-left: 8px; margin-top: 24px">
                                            <button type="button" class="btn btn-success" onclick="showModal()"><i class="fa fa-search"></i> </button>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="nama" class="control-label">DPJP Tujuan Kontrol / Inap</label>
                                        <input type="text" class="form-control" name="kode_dokter_name" id="kode_dokter_name" placeholder="Spesialis/SubSpesialis" readonly>
                                        <input type="hidden" name="kode_dokter" id="kode_dokter">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="box-footer">
                            <button type="submit" class="btn btn-success pull-right">Simpan</button>
                        </div>
                    </div>
                </form>
            </div>

        </div>
    </section>
</div>

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel">Jadwal Praktek Rumah Sakit</h4>
            </div>
            <div class="modal-body">
                <div style="margin-left: 10px; margin-right: 10px">
                    <div class="row">
                        <div class="col-12" style="margin-top: 10px">
                            <div class="alert alert-info alert-dismissible" style="padding-left: 0 !important; padding-bottom: 4px !important;">
                                <ol>
                                    <li>Untuk Melihat Jadwal Praktek Dokter klik Nama Spesialis/SubSpesialis</li>
                                    <li>Jumlah Rencana Kontrol Merupakan Penjumlahan dari Rencana Kontrol Spesialis/Subspesialis Per Tanggal</li>
                                    <li>Kapasitas Merupakan Jumlah Maksimal Layanan yang Dapat dilayani Oleh Spesialis/Subspesialis</li>
                                </ol>
                            </div>
                        </div>
                        <div class="col-12">
                            <table class="table table-bordered" id="tbl-sp">
                                <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Nama Spesialis/Sub</th>
                                    <th>Kapasitas</th>
                                    <th>Jml Rencana Kontrol & Rujukan</th>
                                    <th>Prosentase</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach($poli as $k => $v) : ?>
                                    <tr>
                                        <td><?=$k + 1?></td>
                                        <td>
                                            <button type="button" class="btn btn-secondary btn-sm" onclick="select_spesialistik('<?=$v->kodePoli?>', '<?=$v->namaPoli?>')"><?=$v->namaPoli?></button>
                                        </td>
                                        <td><?=$v->kapasitas?></td>
                                        <td><?=$v->jmlRencanaKontroldanRujukan?></td>
                                        <td><?=$v->persentase?></td>
                                    </tr>
                                <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel">Jadwal Praktek Dokter</h4>
            </div>
            <div class="modal-body">
                <div style="margin-left: 10px; margin-right: 10px">
                    <div class="row">
                        <div class="col-12">
                            <table class="table table-bordered" id="tbl-jadwal">
                                <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Nama Dokter</th>
                                    <th>Jadwal Praktek</th>
                                    <th>Kapasitas</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
            </div>
        </div>
    </div>
</div>

<div id='ResponseInput'></div>

<script src="<?= base_url() ?>assets/plugins/input-mask/jquery.inputmask.js"></script>
<script src="<?= base_url() ?>assets/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="<?= base_url() ?>assets/plugins/input-mask/jquery.inputmask.extensions.js"></script>
<script src="<?= base_url() ?>assets/bower_components/select2/dist/js/select2.full.min.js"></script>
<script src="<?php echo base_url(); ?>assets/bower_components/toast-alert/toastr.js"></script>

<script>
    function showModal() {
        $('#exampleModal').modal('show')
    }

    function select_spesialistik(kodeSpesialis, namaSpesialis) {
        $('#poli_kontrol').val(kodeSpesialis)
        $('#poli_kontrol_name').val(`${kodeSpesialis} - ${namaSpesialis}`)

        search_jadwal_dokter(r => {
            $('#exampleModal').modal('hide')

            if (r && r.length) {
                $('#tbl-jadwal tbody').empty()
                r.forEach((v, k) => {
                    $('#tbl-jadwal tbody').append(`
                        <tr>
                            <td>${k + 1}</td>
                            <td>${v.namaDokter}</td>
                            <td>${v.jadwalPraktek}</td>
                            <td>${v.kapasitas}</td>
                            <td>
                                <button class="btn btn-success btn-sm" onclick="select_dokter('${v.kodeDokter}', '${v.namaDokter}')"><i class="fa fa-check"></i> </button>
                            </td>
                        </tr>
                    `)
                })
                $('#exampleModal2').modal('show')
            }
            else {
                toastr.error(`Tidak ada jadwal dokter ${namaSpesialis} di tanggal ${$('#tgl_rencana').val()}`)
            }
        })
    }

    const search_jadwal_dokter = (f) => {
        $.ajax({
            url: "<?= site_url('bpjs/rencana_kontrol/ajax_search_jadwal_dokter_rencana_kontrol'); ?>",
            type: "POST",
            cache: false,
            dataType: 'json',
            data: `jenis_kontrol=1&kode_poli=${$('#poli_kontrol').val()}&tgl=<?=$p3?>`,
            success: function (json) {
                if (json.status === 1) {
                    f(json.datanya.list)
                }
                if (json.status === 0) {
                    toastr.error(json.server_output?.metaData?.message ?? 'Gagal mendapatkan data')
                }
            }
        })
    }

    function select_dokter(kodeDokter, namaDokter) {
        $('#exampleModal2').modal('hide')
        $('#kode_dokter').val(kodeDokter)
        $('#kode_dokter_name').val(namaDokter)
    }

</script>