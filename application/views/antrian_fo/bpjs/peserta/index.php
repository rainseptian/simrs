<link rel="stylesheet" href="<?= base_url() ?>assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Cek Peserta
            <small></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="active"> Cek Peserta</li>
        </ol>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-success">
                    <div class="box-header">
                        <h3 class="box-title">Cek Peserta</h3>&nbsp;&nbsp;
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-12 col-md-4">
                                <div class="form-group">
                                    <label>Berdasarkan NIK</label>
                                    <input type="text" class="form-control" id="nik">
                                </div>
                            </div>
                            <div class="col-12 col-md-4">
                                <div class="form-group">
                                    <label>Tanggal Pelayanan/SEP</label>
                                    <input type="date" class="form-control" id="tgl-nik" value="<?=date('Y-m-d')?>">
                                </div>
                            </div>
                            <div class="col-12 col-md-4" style="padding-top: 24px">
                                <button type="button" class="btn btn-success" id="cari-nik">
                                    <i class="fa fa-search"></i> Cari
                                </button>
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-12 col-md-4">
                                <div class="form-group">
                                    <label>Berdasarkan No BPJS</label>
                                    <input type="text" class="form-control" id="no-bpjs">
                                </div>
                            </div>
                            <div class="col-12 col-md-4">
                                <div class="form-group">
                                    <label>Tanggal Pelayanan/SEP</label>
                                    <input type="date" class="form-control" id="tgl-no-bpjs" value="<?=date('Y-m-d')?>">
                                </div>
                            </div>
                            <div class="col-12 col-md-4" style="padding-top: 24px">
                                <button type="button" class="btn btn-success" id="cari-no-bpjs">
                                    <i class="fa fa-search"></i> Cari
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box box-success">
                    <div class="box-header">
                        <h3 class="box-title" id="t-res">Hasil Pengecekan</h3>&nbsp;&nbsp;
                    </div>
                    <div class="box-body">
                        <p style="font-style: italic" id="t-result"></p>
                        <table class="table table-bordered" id="tbl-detail">
                            <tbody>
                            <tr>
                                <th>No Kartu</th>
                                <td id="no_kartu"></td>
                            </tr>
                            <tr>
                                <th>NIK</th>
                                <td id="nik_"></td>
                            </tr>
                            <tr>
                                <th>Nama</th>
                                <td id="nama"></td>
                            </tr>
                            <tr>
                                <th>Pisa</th>
                                <td id="pisa"></td>
                            </tr>
                            <tr>
                                <th>Kelamin</th>
                                <td id="kelamin"></td>
                            </tr>
                            <tr>
                                <th>No MR</th>
                                <td id="no_mr"></td>
                            </tr>
                            <tr>
                                <th>No Telp</th>
                                <td id="no_telp"></td>
                            </tr>
                            <tr>
                                <th>Tanggal Lahir</th>
                                <td id="tgl_lahir"></td>
                            </tr>
                            <tr>
                                <th>Tanggal Cetak Kartu</th>
                                <td id="tgl_cetak_kartu"></td>
                            </tr>
                            <tr>
                                <th>Tanggal TAT</th>
                                <td id="tgl_tat"></td>
                            </tr>
                            <tr>
                                <th>Tanggal TMT</th>
                                <td id="tgl_tmt"></td>
                            </tr>
                            <tr>
                                <th>Status Peserta</th>
                                <td id="status_peserta"></td>
                            </tr>
                            <tr>
                                <th>Prov Umum</th>
                                <td id="prov_umum"></td>
                            </tr>
                            <tr>
                                <th>Jenis Peserta</th>
                                <td id="jenis_peserta"></td>
                            </tr>
                            <tr>
                                <th>Hak Kelas</th>
                                <td id="hak_kelas"></td>
                            </tr>
                            <tr>
                                <th>Umur Sekarang</th>
                                <td id="umur_sekarang"></td>
                            </tr>
                            <tr>
                                <th>Umur Saat Pelayanan</th>
                                <td id="umur_saat_pelayanan"></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<script>

    const parse = (obj) => {
        console.log({obj})
        $('#no_kartu').html(obj.noKartu)
        $('#nik_').html(obj.nik)
        $('#nama').html(obj.nama)
        $('#pisa').html(obj.pisa)
        $('#kelamin').html(obj.sex === 'L' ? 'Laki-Laki' : 'Perempuan')
        $('#no_mr').html(obj.mr.noMR)
        $('#no_telp').html(obj.mr.noTelepon)
        $('#tgl_lahir').html(obj.tglLahir)
        $('#tgl_cetak_kartu').html(obj.tglCetakKartu)
        $('#tgl_tat').html(obj.tglTAT)
        $('#tgl_tmt').html(obj.tglTMT)
        $('#status_peserta').html(obj.statusPeserta.keterangan)
        $('#prov_umum').html(`${obj.provUmum.kdProvider} - ${obj.provUmum.nmProvider}`)
        $('#jenis_peserta').html(`${obj.jenisPeserta.kode} - ${obj.jenisPeserta.keterangan}`)
        $('#hak_kelas').html(obj.hakKelas.keterangan)
        $('#umur_sekarang').html(obj.umur.umurSekarang)
        $('#umur_saat_pelayanan').html(obj.umur.umurSaatPelayanan)
    }

    $(function () {
        $('#cari-nik').click(function () {
            $('#t-res').html(`Hasil Pengecekan NIK: ${$('#nik').val()}`)
            $.ajax({
                url: "<?= site_url('Bpjs/ajax_search_peserta_by_nik'); ?>",
                type: "POST",
                cache: false,
                data: `keyword=${$('#nik').val()}&tgl=${$('#tgl-nik').val()}`,
                dataType: 'json',
                success: function (json) {
                    $('#nik').val('')
                    $('#tgl-nik').val('')

                    if (json.status === 1) {
                        // $('#t-result').hide()
                        // $('#tbl-detail').show()
                        // $('#tbl-detail > tbody').html('')
                        parse(json.datanya.peserta)
                    }
                    if (json.status === 0) {
                        $('#tbl-detail').hide()
                        $('#t-result').show().html(json.server_output?.metaData?.message ?? 'Gagal mendapatkan data')
                    }
                }
            })
        })

        $('#cari-no-bpjs').click(function () {
            $('#t-res').html(`Hasil Pengecekan No BPJS: ${$('#no-bpjs').val()}`)
            $.ajax({
                url: "<?= site_url('Bpjs/ajax_search_peserta_by_no_bpjs'); ?>",
                type: "POST",
                cache: false,
                data: `keyword=${$('#no-bpjs').val()}&tgl=${$('#tgl-no-bpjs').val()}`,
                dataType: 'json',
                success: function (json) {
                    $('#no-bpjs').val('')
                    $('#tgl-no-bpjs').val('')

                    if (json.status === 1) {
                        // $('#t-result').hide()
                        // $('#tbl-detail').show()
                        // $('#tbl-detail > tbody').html('')
                        parse(json.datanya.peserta)
                    }
                    if (json.status === 0) {
                        $('#tbl-detail').hide()
                        $('#t-result').show().html(json.server_output?.metaData?.message ?? 'Gagal mendapatkan data')
                    }
                }
            })
        })
    })
</script>