<?php
$res = unserialize($pemeriksaan['meta']);
?>

<div class="form-group">
    <label for="hpht" class="col-sm-3 control-label">HPHT</label>
    <div class="col-sm-9">
        <input type="date" class="form-control" name="meta[hpht]" id="hpht" value="<?= $res['hpht'] ?>">
    </div>
</div>
<div class="form-group">
    <label for="hpl" class="col-sm-3 control-label">HPL</label>
    <div class="col-sm-9">
        <input type="date" class="form-control" name="meta[hpl]" id="hpl" value="<?= $res['hpl'] ?>">
    </div>
</div>
<div class="form-group">
    <label for="g_p_a" class="col-sm-3 control-label">G/P/A</label>
    <div class="col-sm-9">
        <textarea class="form-control" name="meta[g_p_a]" id="g_p_a" rows="2"><?= $res['g_p_a'] ?></textarea>
    </div>
</div>
<div class="form-group">
    <label for="uk" class="col-sm-3 control-label">UK</label>
    <div class="col-sm-9">
        <textarea class="form-control" name="meta[uk]" id="uk" rows="2"><?= $res['uk'] ?></textarea>
    </div>
</div>
<div class="form-group">
    <label for="hasil_usg" class="col-sm-3 control-label">Bacaan Hasil USG</label>
    <div class="col-sm-9">
        <textarea class="form-control" name="meta[hasil_usg]" id="hasil_usg" rows="2"><?= $res['hasil_usg'] ?></textarea>
    </div>
</div>
<div class="form-group">
    <label for="status_obsetri" class="col-sm-3 control-label">Status Obsetri</label>
    <div class="col-sm-9">
        <textarea class="form-control" name="meta[status_obsetri]" id="status_obsetri" rows="2"><?= $res['status_obsetri'] ?></textarea>
    </div>
</div>
<div class="form-group">
    <label for="status_gynekologi" class="col-sm-3 control-label">Status Gynekologi</label>
    <div class="col-sm-9">
        <textarea class="form-control" name="meta[status_gynekologi]" id="status_gynekologi" rows="2"><?= $res['status_gynekologi'] ?></textarea>
    </div>
</div>
<div class="form-group">
    <label for="pemeriksaan_fisik" class="col-sm-3 control-label">Pemeriksaan Fisik</label>
    <div class="col-sm-9">
        <textarea class="form-control" name="meta[pemeriksaan_fisik]" id="pemeriksaan_fisik" rows="2"><?= $res['pemeriksaan_fisik'] ?></textarea>
    </div>
</div>
<!--<div class="form-group">-->
<!--    <label for="abdomen" class="col-sm-3 control-label">Abdomen</label>-->
<!--    <div class="col-sm-9">-->
<!--        <textarea type="text" class="form-control" name="meta[abdomen]" id="abdomen">--><?//= $res['abdomen'] ?><!--</textarea>-->
<!--    </div>-->
<!--</div>-->
<!--<div class="form-group">-->
<!--    <label for="auskultasi" class="col-sm-3 control-label">Auskultasi</label>-->
<!--    <div class="col-sm-9">-->
<!--        <textarea type="text" class="form-control" name="meta[auskultasi]" id="auskultasi">--><?//= $res['auskultasi'] ?><!--</textarea>-->
<!--    </div>-->
<!--</div>-->

<script>
    Date.prototype.add = function(num, what = 'day') {
        let date = new Date(this.valueOf());
        if (what === 'day')
            date.setDate(date.getDate() + num);
        else if (what === 'month')
            date.setMonth(date.getMonth() + num);
        else if (what === 'year')
            date.setFullYear(date.getFullYear() + num);
        return date;
    }

    Date.prototype.sub = function(num, what = 'day') {
        let date = new Date(this.valueOf());
        if (what === 'day')
            date.setDate(date.getDate() - num);
        else if (what === 'month')
            date.setMonth(date.getMonth() - num);
        else if (what === 'year')
            date.setFullYear(date.getFullYear() - num);
        return date;
    }

    Date.prototype.toSqlDate = function() {
        let pad = function(num) { return ('00'+num).slice(-2) };
        let date = new Date(this.valueOf());
        date = date.getUTCFullYear() + '-' +
            pad(date.getUTCMonth() + 1) + '-' +
            pad(date.getUTCDate());
        return date;
    }

    Date.prototype.toSqlDateTime = function() {
        let pad = function(num) { return ('00'+num).slice(-2) };
        let date = new Date(this.valueOf());
        date = date.getUTCFullYear()         + '-' +
            pad(date.getUTCMonth() + 1)  + '-' +
            pad(date.getUTCDate())       + ' ' +
            pad(date.getUTCHours())      + ':' +
            pad(date.getUTCMinutes())    + ':' +
            pad(date.getUTCSeconds());
        return date;
    }

    $(function () {
        $('#hpht').change(function () {
            let tgl = new Date($(this).val())
            if (tgl.getUTCMonth() + 1 >= 1 && tgl.getUTCMonth() + 1 <= 3) {
                tgl = tgl.add(7, 'day')
                tgl = tgl.add(9, 'month')
            }
            else {
                tgl = tgl.add(7, 'day')
                tgl = tgl.sub(3, 'month')
                tgl = tgl.add(1, 'year')
            }
            $('#hpl').val(tgl.toSqlDate())
        })
    })
</script>