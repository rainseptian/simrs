<style media="screen">
    .hd {
        border: 1px solid #eee;
        text-align: left;
    }

    .hd input {
        width: 100%;
        border: 0;
        border-bottom: 1px dotted #333;
        background-color: #fff0;
    }

    .hd input:focus {
        border-style: solid;
        outline: none !important;
    }

    #area-pemeriksaan{
        padding: 0;
        cursor: pointer;
    }

    #area-pemeriksaan .marker{
        width:40px;
        height:23px;
        text-align:right;
        position:absolute;
        color: red;
        font-weight: bold;
    }

    .italic {
        font-style: italic;
    }
    .bold {
        font-weight: bold !important;
    }
    .not-bold label {
        font-weight: normal;
    }
    .small-text {
        font-size: 12px;
    }

    [type="checkbox"] {
        vertical-align:middle;
    }
    .my-checkbox input {
        width: 14px;
        margin-top: 0;
        margin-bottom: 3px;
    }

    .inner-table {
        font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
        border-collapse: collapse;
        width: 100%;
    }
    .inner-table td, .inner-table th {
        border: 1px solid #ddd;
        padding: 8px;
    }
    .inner-table tr:nth-child(even){background-color: #f2f2f2;}
    .inner-table th {
        padding-top: 12px;
        padding-bottom: 12px;
        text-align: left;
        background-color: #4CAF50;
        color: white;
    }

    .tbl-resiko-jatuh td {
        padding: 4px;
    }

    .my-input {
        width: calc(70% - 10px) !important;
        margin-left: 5px;
        margin-right: 5px;
        box-sizing: border-box;
    }

    .my-input-half {
        width: 40% !important;
        margin-left: 5px;
        margin-right: 5px;
        box-sizing: border-box;
    }

    .inline-input {
        display: inline-block !important;
    }
    .inline-input-next {
        display: inline-block !important;
        margin-left: 10px;
    }

    .tbl-terpadu td, .tbl-terpadu th {
        padding: 4px 4px !important;
        border: 1px solid #ddd;
        text-align: center !important;
        vertical-align: middle !important;
    }
    .tbl-terpadu tr:nth-child(even){background-color: #f6f6f6;}
    .tbl-terpadu th {
        padding-top: 12px;
        padding-bottom: 12px;
        text-align: left;
        background-color: #4CAF50;
        color: white;
    }
    .tbl-terpadu input, textarea {
        height: 100%;
        width: 40px;
        resize: none;
    }
    .tr-pre-hd td {
        height: 60px;
    }
    .tr-intra-hd td {
        height: 130px;
    }
    .tr-post-hd td {
        height: 100px;
    }

    .new-label-kb {
        width: 100px;
        float: left;
        margin-top: 0 !important;
        padding-top: 0 !important;
    }
    .new-label-kb-div {
        width: calc(100% - 110px);
        margin-left: 5px;
        float: left;
    }
    .new-label {
        width: 80px;
        float: left;
        margin-top: 0 !important;
        padding-top: 0 !important;
    }
    .new-label-div {
        width: calc(100% - 90px);
        margin-left: 5px;
        float: left;
    }
    .padding-sm {
        padding: 5px !important;
    }
</style>

<?php
$kia = unserialize($pemeriksaan['meta']);
$ada_hamil = in_array('hamil', $kia['jenis']);
$ada_kb = in_array('kb', $kia['jenis']);
//die(json_encode($kia));
?>

<div class="form-group">
    <label class="col-sm-3 control-label"></label>
</div>
<div class="row">
    <div class="col-xs-12">
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <?php if ($ada_hamil) : ?>
                <li class="active">
                    <a href="#tab_1" data-toggle="tab" class="padding-sm">
                        <span style="font-size: 18px;">Ibu Hamil</span>
                    </a>
                </li>
                <?php endif; ?>
                <?php if ($ada_kb) : ?>
                <li>
                    <a href="#tab_2" data-toggle="tab" class="padding-sm">
                        <span style="font-size: 18px;">KB</span>
                    </a>
                </li>
                <?php endif; ?>
            </ul>
            <div class="tab-content">
                <?php if ($ada_hamil) : ?>
                    <div class="tab-pane active" id="tab_1">
                    <h4 class="box-title text-center"><strong>IBU HAMIL</strong></h4>
                    <div class="col-sm-6">
                        <div class="table-responsive">
                            <table class="table table-striped hd">
                                <tbody>
                                <tr>
                                    <td><label class="control-label">A. IDENTITAS IBU</label></td>
                                </tr>
                                <tr class="not-bold">
                                    <td>
                                        <label class="new-label">
                                            <span style="float: left">Nama</span>
                                        </label>
                                        <div class="new-label-div">
                                            <span style="width:5%; float: left">:</span>
                                            <input type="text" style="width: 95%;" name="meta[hamil][a][nama]" value="<?=$kia['hamil']['a']['nama'] ?? $pendaftaran['nama_pasien'] ?? ''?>">
                                        </div>
                                    </td>
                                </tr>
                                <tr class="not-bold">
                                    <td>
                                        <label class="new-label">
                                            <span style="float: left">Umur</span>
                                        </label>
                                        <div class="new-label-div">
                                            <span style="width:5%; float: left">:</span>
                                            <input type="text" style="width: 50%;" name="meta[hamil][a][umur]" value="<?=$kia['hamil']['a']['umur'] ?? $pendaftaran['usia'] ?? ''?>">
                                            <span style="width:5%;">tahun</span>
                                        </div>
                                    </td>
                                </tr>
                                <tr class="not-bold">
                                    <td>
                                        <label class="new-label">
                                            <span style="float: left">Agama</span>
                                        </label>
                                        <div class="new-label-div">
                                            <span style="width:5%; float: left">:</span>
                                            <input type="text" style="width: 95%;" name="meta[hamil][a][agama]" value="<?=$kia['hamil']['a']['agama'] ?? $pendaftaran['agama'] ?? ''?>">
                                        </div>
                                    </td>
                                </tr>
                                <tr class="not-bold">
                                    <td>
                                        <label class="new-label">
                                            <span style="float: left">Pekerjaan</span>
                                        </label>
                                        <div class="new-label-div">
                                            <span style="width:5%; float: left">:</span>
                                            <input type="text" style="width: 95%;" name="meta[hamil][a][pekerjaan]" value="<?=$kia['hamil']['a']['pekerjaan'] ?? $pendaftaran['pekerjaan'] ?? ''?>">
                                        </div>
                                    </td>
                                </tr>
                                <tr class="not-bold">
                                    <td>
                                        <label class="new-label">
                                            <span style="float: left">Pendidikan</span>
                                        </label>
                                        <div class="new-label-div">
                                            <span style="width:5%; float: left">:</span>
                                            <input type="text" style="width: 95%;" name="meta[hamil][a][pendidikan]" value="<?=$kia['hamil']['a']['pendidikan'] ?? ''?>">
                                        </div>
                                    </td>
                                </tr>
                                <tr class="not-bold">
                                    <td>
                                        <label class="new-label">
                                            <span style="float: left">Alamat</span>
                                        </label>
                                        <div class="new-label-div">
                                            <span style="width:5%; float: left">:</span>
                                            <input type="text" style="width: 95%;" name="meta[hamil][a][alamat]" value="<?=$kia['hamil']['a']['alamat'] ?? $pendaftaran['alamat'] ?? ''?>">
                                        </div>
                                    </td>
                                </tr>
                                <tr class="not-bold">
                                    <td>
                                        <label class="new-label">
                                            <span style="float: left">No Telp / HP</span>
                                        </label>
                                        <div class="new-label-div">
                                            <span style="width:5%; float: left">:</span>
                                            <input type="text" style="width: 95%;" name="meta[hamil][a][no_telp]" value="<?=$kia['hamil']['a']['no_telp'] ?? $pendaftaran['telepon'] ?? ''?>">
                                        </div>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="table-responsive">
                            <table class="table table-striped hd">
                                <tbody>
                                <tr>
                                    <td><label class="control-label">E. IDENTITAS SUAMI</label></td>
                                </tr>
                                <tr class="not-bold">
                                    <td>
                                        <label class="new-label">
                                            <span style="float: left">Nama</span>
                                        </label>
                                        <div class="new-label-div">
                                            <span style="width:5%; float: left">:</span>
                                            <input type="text" style="width: 95%;" name="meta[hamil][e][nama]" value="<?=isset($kia) ? $kia['hamil']['e']['nama'] : ''?>">
                                        </div>
                                    </td>
                                </tr>
                                <tr class="not-bold">
                                    <td>
                                        <label class="new-label">
                                            <span style="float: left">Umur</span>
                                        </label>
                                        <div class="new-label-div">
                                            <span style="width:5%; float: left">:</span>
                                            <input type="text" style="width: 50%;" name="meta[hamil][e][umur]" value="<?=isset($kia) ? $kia['hamil']['e']['umur'] : ''?>">
                                            <span style="width:5%;">tahun</span>
                                        </div>
                                    </td>
                                </tr>
                                <tr class="not-bold">
                                    <td>
                                        <label class="new-label">
                                            <span style="float: left">Agama</span>
                                        </label>
                                        <div class="new-label-div">
                                            <span style="width:5%; float: left">:</span>
                                            <input type="text" style="width: 95%;" name="meta[hamil][e][agama]" value="<?=isset($kia) ? $kia['hamil']['e']['agama'] : ''?>">
                                        </div>
                                    </td>
                                </tr>
                                <tr class="not-bold">
                                    <td>
                                        <label class="new-label">
                                            <span style="float: left">Pekerjaan</span>
                                        </label>
                                        <div class="new-label-div">
                                            <span style="width:5%; float: left">:</span>
                                            <input type="text" style="width: 95%;" name="meta[hamil][e][pekerjaan]" value="<?=isset($kia) ? $kia['hamil']['e']['pekerjaan'] : ''?>">
                                        </div>
                                    </td>
                                </tr>
                                <tr class="not-bold">
                                    <td>
                                        <label class="new-label">
                                            <span style="float: left">Pendidikan</span>
                                        </label>
                                        <div class="new-label-div">
                                            <span style="width:5%; float: left">:</span>
                                            <input type="text" style="width: 95%;" name="meta[hamil][e][pendidikan]" value="<?=isset($kia) ? $kia['hamil']['e']['pendidikan'] : ''?>">
                                        </div>
                                    </td>
                                </tr>
                                <tr class="not-bold">
                                    <td>
                                        <label class="new-label">
                                            <span style="float: left">Alamat</span>
                                        </label>
                                        <div class="new-label-div">
                                            <span style="width:5%; float: left">:</span>
                                            <input type="text" style="width: 95%;" name="meta[hamil][e][alamat]" value="<?=isset($kia) ? $kia['hamil']['e']['alamat'] : ''?>">
                                        </div>
                                    </td>
                                </tr>
                                <tr class="not-bold">
                                    <td>
                                        <label class="new-label">
                                            <span style="float: left">No Telp / HP</span>
                                        </label>
                                        <div class="new-label-div">
                                            <span style="width:5%; float: left">:</span>
                                            <input type="text" style="width: 95%;" name="meta[hamil][e][no_telp]" value="<?=isset($kia) ? $kia['hamil']['e']['no_telp'] : ''?>">
                                        </div>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="table-responsive">
                            <table class="table table-striped hd">
                                <tbody>
                                <tr>
                                    <td><label class="control-label">B. RIWAYAT HAID</label></td>
                                </tr>
                                <tr class="not-bold">
                                    <td>
                                        <label class="new-label">
                                            <span style="float: left">Menarche</span>
                                        </label>
                                        <div class="new-label-div">
                                            <span style="width:5%; float: left">:</span>
                                            <input type="text" style="width: 50%;" name="meta[hamil][b][menarche]" value="<?=isset($kia) ? $kia['hamil']['b']['menarche'] : ''?>">
                                            <span style="width:5%;">tahun</span>
                                        </div>
                                    </td>
                                </tr>
                                <tr class="not-bold">
                                    <td>
                                        <label class="new-label">
                                            <span style="float: left">Lamanya</span>
                                        </label>
                                        <div class="new-label-div">
                                            <span style="width:5%; float: left">:</span>
                                            <input type="text" style="width: 50%;" name="meta[hamil][b][lamanya]" value="<?=isset($kia) ? $kia['hamil']['b']['lamanya'] : ''?>">
                                            <span style="width:5%;">hari</span>
                                        </div>
                                    </td>
                                </tr>
                                <tr class="not-bold">
                                    <td>
                                        <label class="new-label">
                                            <span style="float: left">Banyaknya</span>
                                        </label>
                                        <div class="new-label-div">
                                            <span style="width:5%; float: left">:</span>
                                            <input type="text" style="width: 95%;" name="meta[hamil][b][banyaknya]" value="<?=isset($kia) ? $kia['hamil']['b']['banyaknya'] : ''?>">
                                        </div>
                                    </td>
                                </tr>
                                <tr class="not-bold">
                                    <td>
                                        <label class="new-label">
                                            <span style="float: left">Disminorhea</span>
                                        </label>
                                        <div class="new-label-div">
                                            <span style="width:5%; float: left">:</span>
                                            <input type="text" style="width: 95%;" name="meta[hamil][b][disminorhea]" value="<?=isset($kia) ? $kia['hamil']['b']['disminorhea'] : ''?>">
                                        </div>
                                    </td>
                                </tr>
                                <tr class="not-bold">
                                    <td>
                                        <label class="new-label">
                                            <span style="float: left">HPHT</span>
                                        </label>
                                        <div class="new-label-div">
                                            <span style="width:5%; float: left">:</span>
                                            <input type="text" style="width: 95%;" name="meta[hamil][b][hpht]" value="<?=isset($kia) ? $kia['hamil']['b']['hpht'] : ''?>">
                                        </div>
                                    </td>
                                </tr>
                                <tr class="not-bold">
                                    <td>
                                        <label class="new-label">
                                            <span style="float: left">HPL</span>
                                        </label>
                                        <div class="new-label-div">
                                            <span style="width:5%; float: left">:</span>
                                            <input type="text" style="width: 95%;" name="meta[hamil][b][hpl]" value="<?=isset($kia) ? $kia['hamil']['b']['hpl'] : ''?>">
                                        </div>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="table-responsive">
                            <table class="table table-striped hd">
                                <tbody>
                                <tr>
                                    <td><label class="control-label">F. RIWAYAT PERKAWINAN</label></td>
                                </tr>

                                <tr class="not-bold">
                                    <td>
                                        <label class="new-label">
                                            <span style="float: left">Usia Ibu</span>
                                        </label>
                                        <div class="new-label-div">
                                            <span style="width:5%; float: left">:</span>
                                            <input type="text" style="width: 50%;" name="meta[hamil][f][usia_ibu]" value="<?=isset($kia) ? $kia['hamil']['f']['usia_ibu'] : ''?>">
                                            <span style="width:5%;">tahun</span>
                                        </div>
                                    </td>
                                </tr>
                                <tr class="not-bold">
                                    <td>
                                        <label class="new-label">
                                            <span style="float: left">Perkawinan ke</span>
                                        </label>
                                        <div class="new-label-div">
                                            <span style="width:5%; float: left">:</span>
                                            <input type="text" style="width: 95%;" name="meta[hamil][f][perkawinan_ke]" value="<?=isset($kia) ? $kia['hamil']['f']['perkawinan_ke'] : ''?>">
                                        </div>
                                    </td>
                                </tr>
                                <tr class="not-bold">
                                    <td>
                                        <label class="new-label">
                                            <span style="float: left">Lama Nikah</span>
                                        </label>
                                        <div class="new-label-div">
                                            <span style="width:5%; float: left">:</span>
                                            <input type="text" style="width: 50%;" name="meta[hamil][f][lama_nikah]" value="<?=isset($kia) ? $kia['hamil']['f']['lama_nikah'] : ''?>">
                                            <span style="width:5%;">tahun/bulan</span>
                                        </div>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="table-responsive">
                            <table class="table table-striped hd">
                                <tbody>
                                <tr>
                                    <td><label class="control-label">G. RIWAYAT KB</label></td>
                                </tr>

                                <tr class="not-bold">
                                    <td>
                                        <label class="new-label">
                                            <span style="float: left">Penggunaan Alokon</span>
                                        </label>
                                        <div class="new-label-div">
                                            <span style="width:5%; float: left">:</span>
                                            <div class="form-check my-checkbox inline-input-next" style="padding-top: 8px;">
                                                <input type="radio" class="form-check-input"
                                                       name="meta[hamil][g][penggunaan_alokon]"
                                                       value="ya"
                                                    <?=isset($kia) && $kia['hamil']['g']['penggunaan_alokon'] == 'ya' ? 'checked' : ''?>>
                                                <label class="form-check-label">Ya</label>
                                            </div>
                                            <div class="form-check my-checkbox inline-input-next" style="padding-top: 8px;">
                                                <input type="radio" class="form-check-input"
                                                       name="meta[hamil][g][penggunaan_alokon]"
                                                       value="tidak"
                                                    <?=isset($kia) && $kia['hamil']['g']['penggunaan_alokon'] == 'tidak' ? 'checked' : ''?>>
                                                <label class="form-check-label">Tidak</label>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr class="not-bold">
                                    <td>
                                        <label class="new-label">
                                            <span style="float: left">Lama KB</span>
                                        </label>
                                        <div class="new-label-div">
                                            <span style="width:5%; float: left">:</span>
                                            <input type="text" style="width: 50%;" name="meta[hamil][g][lama_kb]" value="<?=isset($kia) ? $kia['hamil']['g']['lama_kb'] : ''?>">
                                            <span style="width:5%;">tahun/bulan</span>
                                        </div>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="table-responsive">
                            <table class="table table-striped hd">
                                <tbody>
                                <tr>
                                    <td><label class="control-label">C. RIWAYAT KESEHATAN IBU</label></td>
                                </tr>

                                <tr class="not-bold">
                                    <td>
                                        <label class="new-label">
                                            <span style="float: left">Jantung</span>
                                        </label>
                                        <div class="new-label-div">
                                            <span style="width:5%; float: left">:</span>
                                            <div class="form-check my-checkbox inline-input-next" style="padding-top: 8px;">
                                                <input type="radio" class="form-check-input"
                                                       name="meta[hamil][c][jantung]"
                                                       value="ya"
                                                    <?=isset($kia) && $kia['hamil']['c']['jantung'] == 'ya' ? 'checked' : ''?>>
                                                <label class="form-check-label">Ya</label>
                                            </div>
                                            <div class="form-check my-checkbox inline-input-next" style="padding-top: 8px;">
                                                <input type="radio" class="form-check-input"
                                                       name="meta[hamil][c][jantung]"
                                                       value="tidak"
                                                    <?=isset($kia) && $kia['hamil']['c']['jantung'] == 'tidak' ? 'checked' : ''?>>
                                                <label class="form-check-label">Tidak</label>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr class="not-bold">
                                    <td>
                                        <label class="new-label">
                                            <span style="float: left">Paru</span>
                                        </label>
                                        <div class="new-label-div">
                                            <span style="width:5%; float: left">:</span>
                                            <div class="form-check my-checkbox inline-input-next" style="padding-top: 8px;">
                                                <input type="radio" class="form-check-input"
                                                       name="meta[hamil][c][paru]"
                                                       value="ya"
                                                    <?=isset($kia) && $kia['hamil']['c']['paru'] == 'ya' ? 'checked' : ''?>>
                                                <label class="form-check-label">Ya</label>
                                            </div>
                                            <div class="form-check my-checkbox inline-input-next" style="padding-top: 8px;">
                                                <input type="radio" class="form-check-input"
                                                       name="meta[hamil][c][paru]"
                                                       value="tidak"
                                                    <?=isset($kia) && $kia['hamil']['c']['paru'] == 'tidak' ? 'checked' : ''?>>
                                                <label class="form-check-label">Tidak</label>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr class="not-bold">
                                    <td>
                                        <label class="new-label">
                                            <span style="float: left">TD Tinggi</span>
                                        </label>
                                        <div class="new-label-div">
                                            <span style="width:5%; float: left">:</span>
                                            <div class="form-check my-checkbox inline-input-next" style="padding-top: 8px;">
                                                <input type="radio" class="form-check-input"
                                                       name="meta[hamil][c][td_tinggi]"
                                                       value="ya"
                                                    <?=isset($kia) && $kia['hamil']['c']['td_tinggi'] == 'ya' ? 'checked' : ''?>>
                                                <label class="form-check-label">Ya</label>
                                            </div>
                                            <div class="form-check my-checkbox inline-input-next" style="padding-top: 8px;">
                                                <input type="radio" class="form-check-input"
                                                       name="meta[hamil][c][td_tinggi]"
                                                       value="tidak"
                                                    <?=isset($kia) && $kia['hamil']['c']['td_tinggi'] == 'tidak' ? 'checked' : ''?>>
                                                <label class="form-check-label">Tidak</label>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr class="not-bold">
                                    <td>
                                        <label class="new-label">
                                            <span style="float: left">DM</span>
                                        </label>
                                        <div class="new-label-div">
                                            <span style="width:5%; float: left">:</span>
                                            <div class="form-check my-checkbox inline-input-next" style="padding-top: 8px;">
                                                <input type="radio" class="form-check-input"
                                                       name="meta[hamil][c][dm]"
                                                       value="ya"
                                                    <?=isset($kia) && $kia['hamil']['c']['dm'] == 'ya' ? 'checked' : ''?>>
                                                <label class="form-check-label">Ya</label>
                                            </div>
                                            <div class="form-check my-checkbox inline-input-next" style="padding-top: 8px;">
                                                <input type="radio" class="form-check-input"
                                                       name="meta[hamil][c][dm]"
                                                       value="tidak"
                                                    <?=isset($kia) && $kia['hamil']['c']['dm'] == 'tidak' ? 'checked' : ''?>>
                                                <label class="form-check-label">Tidak</label>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr class="not-bold">
                                    <td>
                                        <label class="new-label">
                                            <span style="float: left">Lever</span>
                                        </label>
                                        <div class="new-label-div">
                                            <span style="width:5%; float: left">:</span>
                                            <div class="form-check my-checkbox inline-input-next" style="padding-top: 8px;">
                                                <input type="radio" class="form-check-input"
                                                       name="meta[hamil][c][lever]"
                                                       value="ya"
                                                    <?=isset($kia) && $kia['hamil']['c']['lever'] == 'ya' ? 'checked' : ''?>>
                                                <label class="form-check-label">Ya</label>
                                            </div>
                                            <div class="form-check my-checkbox inline-input-next" style="padding-top: 8px;">
                                                <input type="radio" class="form-check-input"
                                                       name="meta[hamil][c][lever]"
                                                       value="tidak"
                                                    <?=isset($kia) && $kia['hamil']['c']['lever'] == 'tidak' ? 'checked' : ''?>>
                                                <label class="form-check-label">Tidak</label>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr class="not-bold">
                                    <td>
                                        <label class="new-label">
                                            <span style="float: left">PMS</span>
                                        </label>
                                        <div class="new-label-div">
                                            <span style="width:5%; float: left">:</span>
                                            <div class="form-check my-checkbox inline-input-next" style="padding-top: 8px;">
                                                <input type="radio" class="form-check-input"
                                                       name="meta[hamil][c][pms]"
                                                       value="ya"
                                                    <?=isset($kia) && $kia['hamil']['c']['pms'] == 'ya' ? 'checked' : ''?>>
                                                <label class="form-check-label">Ya</label>
                                            </div>
                                            <div class="form-check my-checkbox inline-input-next" style="padding-top: 8px;">
                                                <input type="radio" class="form-check-input"
                                                       name="meta[hamil][c][pms]"
                                                       value="tidak"
                                                    <?=isset($kia) && $kia['hamil']['c']['pms'] == 'tidak' ? 'checked' : ''?>>
                                                <label class="form-check-label">Tidak</label>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr class="not-bold">
                                    <td>
                                        <label class="new-label">
                                            <span style="float: left">Alergi</span>
                                        </label>
                                        <div class="new-label-div">
                                            <span style="width:5%; float: left">:</span>
                                            <input type="text" style="width: 95%;" name="meta[hamil][c][alergi]" value="<?=isset($kia) ? $kia['hamil']['c']['alergi'] : ''?>">
                                        </div>
                                    </td>
                                </tr>
                                <tr class="not-bold">
                                    <td>
                                        <label class="new-label">
                                            <span style="float: left">Malaria</span>
                                        </label>
                                        <div class="new-label-div">
                                            <span style="width:5%; float: left">:</span>
                                            <div class="form-check my-checkbox inline-input-next" style="padding-top: 8px;">
                                                <input type="radio" class="form-check-input"
                                                       name="meta[hamil][c][malaria]"
                                                       value="ya"
                                                    <?=isset($kia) && $kia['hamil']['c']['malaria'] == 'ya' ? 'checked' : ''?>>
                                                <label class="form-check-label">Ya</label>
                                            </div>
                                            <div class="form-check my-checkbox inline-input-next" style="padding-top: 8px;">
                                                <input type="radio" class="form-check-input"
                                                       name="meta[hamil][c][malaria]"
                                                       value="tidak"
                                                    <?=isset($kia) && $kia['hamil']['c']['malaria'] == 'tidak' ? 'checked' : ''?>>
                                                <label class="form-check-label">Tidak</label>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="table-responsive">
                            <table class="table table-striped hd">
                                <tbody>
                                <tr>
                                    <td><label class="control-label">H. RIWAYAT KESEHATAN KELUARGA</label></td>
                                </tr>

                                <tr class="not-bold">
                                    <td>
                                        <label class="new-label">
                                            <span style="float: left">Jantung</span>
                                        </label>
                                        <div class="new-label-div">
                                            <span style="width:5%; float: left">:</span>
                                            <div class="form-check my-checkbox inline-input-next" style="padding-top: 8px;">
                                                <input type="radio" class="form-check-input"
                                                       name="meta[hamil][h][jantung]"
                                                       value="ya"
                                                    <?=isset($kia) && $kia['hamil']['h']['jantung'] == 'ya' ? 'checked' : ''?>>
                                                <label class="form-check-label">Ya</label>
                                            </div>
                                            <div class="form-check my-checkbox inline-input-next" style="padding-top: 8px;">
                                                <input type="radio" class="form-check-input"
                                                       name="meta[hamil][h][jantung]"
                                                       value="tidak"
                                                    <?=isset($kia) && $kia['hamil']['h']['jantung'] == 'tidak' ? 'checked' : ''?>>
                                                <label class="form-check-label">Tidak</label>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr class="not-bold">
                                    <td>
                                        <label class="new-label">
                                            <span style="float: left">Paru</span>
                                        </label>
                                        <div class="new-label-div">
                                            <span style="width:5%; float: left">:</span>
                                            <div class="form-check my-checkbox inline-input-next" style="padding-top: 8px;">
                                                <input type="radio" class="form-check-input"
                                                       name="meta[hamil][h][paru]"
                                                       value="ya"
                                                    <?=isset($kia) && $kia['hamil']['h']['paru'] == 'ya' ? 'checked' : ''?>>
                                                <label class="form-check-label">Ya</label>
                                            </div>
                                            <div class="form-check my-checkbox inline-input-next" style="padding-top: 8px;">
                                                <input type="radio" class="form-check-input"
                                                       name="meta[hamil][h][paru]"
                                                       value="tidak"
                                                    <?=isset($kia) && $kia['hamil']['h']['paru'] == 'tidak' ? 'checked' : ''?>>
                                                <label class="form-check-label">Tidak</label>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr class="not-bold">
                                    <td>
                                        <label class="new-label">
                                            <span style="float: left">TD Tinggi</span>
                                        </label>
                                        <div class="new-label-div">
                                            <span style="width:5%; float: left">:</span>
                                            <div class="form-check my-checkbox inline-input-next" style="padding-top: 8px;">
                                                <input type="radio" class="form-check-input"
                                                       name="meta[hamil][h][td_tinggi]"
                                                       value="ya"
                                                    <?=isset($kia) && $kia['hamil']['h']['td_tinggi'] == 'ya' ? 'checked' : ''?>>
                                                <label class="form-check-label">Ya</label>
                                            </div>
                                            <div class="form-check my-checkbox inline-input-next" style="padding-top: 8px;">
                                                <input type="radio" class="form-check-input"
                                                       name="meta[hamil][h][td_tinggi]"
                                                       value="tidak"
                                                    <?=isset($kia) && $kia['hamil']['h']['td_tinggi'] == 'tidak' ? 'checked' : ''?>>
                                                <label class="form-check-label">Tidak</label>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr class="not-bold">
                                    <td>
                                        <label class="new-label">
                                            <span style="float: left">DM</span>
                                        </label>
                                        <div class="new-label-div">
                                            <span style="width:5%; float: left">:</span>
                                            <div class="form-check my-checkbox inline-input-next" style="padding-top: 8px;">
                                                <input type="radio" class="form-check-input"
                                                       name="meta[hamil][h][dm]"
                                                       value="ya"
                                                    <?=isset($kia) && $kia['hamil']['h']['dm'] == 'ya' ? 'checked' : ''?>>
                                                <label class="form-check-label">Ya</label>
                                            </div>
                                            <div class="form-check my-checkbox inline-input-next" style="padding-top: 8px;">
                                                <input type="radio" class="form-check-input"
                                                       name="meta[hamil][h][dm]"
                                                       value="tidak"
                                                    <?=isset($kia) && $kia['hamil']['h']['dm'] == 'tidak' ? 'checked' : ''?>>
                                                <label class="form-check-label">Tidak</label>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr class="not-bold">
                                    <td>
                                        <label class="new-label">
                                            <span style="float: left">Lever</span>
                                        </label>
                                        <div class="new-label-div">
                                            <span style="width:5%; float: left">:</span>
                                            <div class="form-check my-checkbox inline-input-next" style="padding-top: 8px;">
                                                <input type="radio" class="form-check-input"
                                                       name="meta[hamil][h][lever]"
                                                       value="ya"
                                                    <?=isset($kia) && $kia['hamil']['h']['lever'] == 'ya' ? 'checked' : ''?>>
                                                <label class="form-check-label">Ya</label>
                                            </div>
                                            <div class="form-check my-checkbox inline-input-next" style="padding-top: 8px;">
                                                <input type="radio" class="form-check-input"
                                                       name="meta[hamil][h][lever]"
                                                       value="tidak"
                                                    <?=isset($kia) && $kia['hamil']['h']['lever'] == 'tidak' ? 'checked' : ''?>>
                                                <label class="form-check-label">Tidak</label>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr class="not-bold">
                                    <td>
                                        <label class="new-label">
                                            <span style="float: left">PMS</span>
                                        </label>
                                        <div class="new-label-div">
                                            <span style="width:5%; float: left">:</span>
                                            <div class="form-check my-checkbox inline-input-next" style="padding-top: 8px;">
                                                <input type="radio" class="form-check-input"
                                                       name="meta[hamil][h][pms]"
                                                       value="ya"
                                                    <?=isset($kia) && $kia['hamil']['h']['pms'] == 'ya' ? 'checked' : ''?>>
                                                <label class="form-check-label">Ya</label>
                                            </div>
                                            <div class="form-check my-checkbox inline-input-next" style="padding-top: 8px;">
                                                <input type="radio" class="form-check-input"
                                                       name="meta[hamil][h][pms]"
                                                       value="tidak"
                                                    <?=isset($kia) && $kia['hamil']['h']['pms'] == 'tidak' ? 'checked' : ''?>>
                                                <label class="form-check-label">Tidak</label>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr class="not-bold">
                                    <td>
                                        <label class="new-label">
                                            <span style="float: left">Keturunan Kembar</span>
                                        </label>
                                        <div class="new-label-div">
                                            <span style="width:5%; float: left">:</span>
                                            <div class="form-check my-checkbox inline-input-next" style="padding-top: 8px;">
                                                <input type="radio" class="form-check-input"
                                                       name="meta[hamil][h][keturunan_kembar]"
                                                       value="ya"
                                                    <?=isset($kia) && $kia['hamil']['h']['keturunan_kembar'] == 'ya' ? 'checked' : ''?>>
                                                <label class="form-check-label">Ya</label>
                                            </div>
                                            <div class="form-check my-checkbox inline-input-next" style="padding-top: 8px;">
                                                <input type="radio" class="form-check-input"
                                                       name="meta[hamil][h][keturunan_kembar]"
                                                       value="tidak"
                                                    <?=isset($kia) && $kia['hamil']['h']['keturunan_kembar'] == 'tidak' ? 'checked' : ''?>>
                                                <label class="form-check-label">Tidak</label>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr class="not-bold">
                                    <td>
                                        <label class="new-label">
                                            <span style="float: left">Kel. Conginetal</span>
                                        </label>
                                        <div class="new-label-div">
                                            <span style="width:5%; float: left">:</span>
                                            <div class="form-check my-checkbox inline-input-next" style="padding-top: 8px;">
                                                <input type="radio" class="form-check-input"
                                                       name="meta[hamil][h][kel_conginetal]"
                                                       value="ya"
                                                    <?=isset($kia) && $kia['hamil']['h']['kel_conginetal'] == 'ya' ? 'checked' : ''?>>
                                                <label class="form-check-label">Ya</label>
                                            </div>
                                            <div class="form-check my-checkbox inline-input-next" style="padding-top: 8px;">
                                                <input type="radio" class="form-check-input"
                                                       name="meta[hamil][h][kel_conginetal]"
                                                       value="tidak"
                                                    <?=isset($kia) && $kia['hamil']['h']['kel_conginetal'] == 'tidak' ? 'checked' : ''?>>
                                                <label class="form-check-label">Tidak</label>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="table-responsive">
                            <table class="table table-striped hd">
                                <tbody>
                                <tr>
                                    <td><label class="control-label">D. RIWAYAT KEHAMILAN SEKARANG</label></td>
                                </tr>

                                <tr class="not-bold">
                                    <td>
                                        <label class="new-label">
                                            <span style="float: left">Imunisasi TT</span>
                                        </label>
                                        <div class="new-label-div">
                                            <span style="width:5%; float: left">:</span>
                                            <input type="text" style="width: 95%;" name="meta[hamil][d][imunisasi_tt]" value="<?=isset($kia) ? $kia['hamil']['d']['imunisasi_tt'] : ''?>">
                                        </div>
                                    </td>
                                </tr>
                                <tr class="not-bold">
                                    <td>
                                        <label class="new-label">
                                            <span style="float: left">Merokok</span>
                                        </label>
                                        <div class="new-label-div">
                                            <span style="width:5%; float: left">:</span>
                                            <div class="form-check my-checkbox inline-input-next" style="padding-top: 8px;">
                                                <input type="radio" class="form-check-input"
                                                       name="meta[hamil][d][merokok]"
                                                       value="ya"
                                                    <?=isset($kia) && $kia['hamil']['d']['merokok'] == 'ya' ? 'checked' : ''?>>
                                                <label class="form-check-label">Ya</label>
                                            </div>
                                            <div class="form-check my-checkbox inline-input-next" style="padding-top: 8px;">
                                                <input type="radio" class="form-check-input"
                                                       name="meta[hamil][d][merokok]"
                                                       value="tidak"
                                                    <?=isset($kia) && $kia['hamil']['d']['merokok'] == 'tidak' ? 'checked' : ''?>>
                                                <label class="form-check-label">Tidak</label>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr class="not-bold">
                                    <td>
                                        <label class="new-label">
                                            <span style="float: left">Jamu</span>
                                        </label>
                                        <div class="new-label-div">
                                            <span style="width:5%; float: left">:</span>
                                            <div class="form-check my-checkbox inline-input-next" style="padding-top: 8px;">
                                                <input type="radio" class="form-check-input"
                                                       name="meta[hamil][d][jamu]"
                                                       value="ya"
                                                    <?=isset($kia) && $kia['hamil']['d']['jamu'] == 'ya' ? 'checked' : ''?>>
                                                <label class="form-check-label">Ya</label>
                                            </div>
                                            <div class="form-check my-checkbox inline-input-next" style="padding-top: 8px;">
                                                <input type="radio" class="form-check-input"
                                                       name="meta[hamil][d][jamu]"
                                                       value="tidak"
                                                    <?=isset($kia) && $kia['hamil']['d']['jamu'] == 'tidak' ? 'checked' : ''?>>
                                                <label class="form-check-label">Tidak</label>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr class="not-bold">
                                    <td>
                                        <label class="new-label">
                                            <span style="float: left">ANC Sebelumnya</span>
                                        </label>
                                        <div class="new-label-div">
                                            <span style="width:5%; float: left">:</span>
                                            <input type="text" style="width: 95%;" name="meta[hamil][d][anc_sebelumnya]" value="<?=isset($kia) ? $kia['hamil']['d']['anc_sebelumnya'] : ''?>">
                                        </div>
                                    </td>
                                </tr>
                                <tr class="not-bold">
                                    <td>
                                        <label class="new-label">
                                            <span style="float: left">Tinggi Badan</span>
                                        </label>
                                        <div class="new-label-div">
                                            <span style="width:5%; float: left">:</span>
                                            <input type="text" style="width: 50%;" name="meta[hamil][d][tinggi_badan]" value="<?=isset($kia) ? $kia['hamil']['d']['tinggi_badan'] : ''?>">
                                            <span style="width:5%;">Cm</span>
                                        </div>
                                    </td>
                                </tr>
                                <tr class="not-bold">
                                    <td>
                                        <label class="new-label">
                                            <span style="float: left">Lila</span>
                                        </label>
                                        <div class="new-label-div">
                                            <span style="width:5%; float: left">:</span>
                                            <input type="text" style="width: 50%;" name="meta[hamil][d][lila]" value="<?=isset($kia) ? $kia['hamil']['d']['lila'] : ''?>">
                                            <span style="width:5%;">Cm</span>
                                        </div>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="table-responsive">
                            <table class="table table-striped hd">
                                <tbody>
                                <tr>
                                    <td><label class="control-label">I. RIWAYAT OBSTETRI (G&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;P&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;A&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;)</label></td>
                                </tr>

                                <tr class="not-bold">
                                    <td>
                                        <label class="new-label">
                                            <span style="float: left">Jml. Persalinan</span>
                                        </label>
                                        <div class="new-label-div">
                                            <span style="width:5%; float: left">:</span>
                                            <input type="text" style="width: 95%;" name="meta[hamil][i][jml_persalinan]" value="<?=isset($kia) ? $kia['hamil']['i']['jml_persalinan'] : ''?>">
                                        </div>
                                    </td>
                                </tr>
                                <tr class="not-bold">
                                    <td>
                                        <label class="new-label">
                                            <span style="float: left">Jml Pers Krg Bulan</span>
                                        </label>
                                        <div class="new-label-div">
                                            <span style="width:5%; float: left">:</span>
                                            <input type="text" style="width: 95%;" name="meta[hamil][i][jml_pers_krg_bulan]" value="<?=isset($kia) ? $kia['hamil']['i']['jml_pers_krg_bulan'] : ''?>">
                                        </div>
                                    </td>
                                </tr>
                                <tr class="not-bold">
                                    <td>
                                        <label class="new-label">
                                            <span style="float: left">Jml Abortus</span>
                                        </label>
                                        <div class="new-label-div">
                                            <span style="width:5%; float: left">:</span>
                                            <input type="text" style="width: 95%;" name="meta[hamil][i][jml_abortus]" value="<?=isset($kia) ? $kia['hamil']['i']['jml_abortus'] : ''?>">
                                        </div>
                                    </td>
                                </tr>
                                <tr class="not-bold">
                                    <td>
                                        <label class="new-label">
                                            <span style="float: left">Jarak Pers Terakhir</span>
                                        </label>
                                        <div class="new-label-div">
                                            <span style="width:5%; float: left">:</span>
                                            <input type="text" style="width: 95%;" name="meta[hamil][i][jarak_pers_terakhir]" value="<?=isset($kia) ? $kia['hamil']['i']['jarak_pers_terakhir'] : ''?>">
                                        </div>
                                    </td>
                                </tr>
                                <tr class="not-bold">
                                    <td>
                                        <label class="new-label">
                                            <span style="float: left">Cara Persalinan</span>
                                        </label>
                                        <div class="new-label-div">
                                            <span style="width:5%; float: left">:</span>
                                            <input type="text" style="width: 95%;" name="meta[hamil][i][cara_persalinan]" value="<?=isset($kia) ? $kia['hamil']['i']['cara_persalinan'] : ''?>">
                                        </div>
                                    </td>
                                </tr>
                                <tr class="not-bold">
                                    <td>
                                        <label class="new-label">
                                            <span style="float: left">Penolong Persalinan</span>
                                        </label>
                                        <div class="new-label-div">
                                            <span style="width:5%; float: left">:</span>
                                            <input type="text" style="width: 95%;" name="meta[hamil][i][penolong_persalinan]" value="<?=isset($kia) ? $kia['hamil']['i']['penolong_persalinan'] : ''?>">
                                        </div>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="table-responsive">
                            <table class="table table-striped hd">
                                <tbody>
                                <tr>
                                    <td><label class="control-label">CATATAN</label></td>
                                </tr>

                                <tr class="not-bold">
                                    <td>
                                        <label class="new-label">
                                            <span style="float: left">Catatan</span>
                                        </label>
                                        <div class="new-label-div">
                                            <span style="width:5%; float: left">:</span>
                                            <input type="text" style="width: 95%;" name="catatan" value="<?=isset($kia) ? $kia['catatan'] : ''?>">
                                        </div>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <?php endif; ?>
                <?php if ($ada_kb) : ?>
                    <div class="tab-pane" id="tab_2">
                    <h4 class="box-title text-center"><strong>KARTU STATUS PESERTA KB</strong></h4>
                    <div class="col-sm-12">
                        <div class="table-responsive">
                            <table class="table table-striped hd">
                                <tbody>
                                <tr class="not-bold">
                                    <td>
                                        <label class="new-label-kb">
                                            <span style="font-size: 12px;">I. Kode Faskes KB/Jaringan/Jejaring</span>
                                        </label>
                                        <div class="new-label-kb-div">
                                            <span style="width:5%; float: left">:</span>
                                            <input type="text" style="width: 95%;" name="meta[kb][kode_faskes]" value="<?=isset($kia) ? $kia['kb']['kode_faskes'] : ''?>">
                                        </div>
                                    </td>
                                    <td>
                                        <label class="new-label-kb">
                                            <span style="font-size: 12px;">II. Kode Keluarga Indonesia</span>
                                        </label>
                                        <div class="new-label-kb-div">
                                            <span style="width:5%; float: left">:</span>
                                            <input type="text" style="width: 95%;" name="meta[kb][kode_keluarga_indonesia]" value="<?=isset($kia) ? $kia['kb']['kode_keluarga_indonesia'] : ''?>">
                                        </div>
                                    </td>
                                </tr>
                                <tr class="not-bold">
                                    <td style="width: 50%">
                                        <label class="new-label-kb">
                                            <span style="font-size: 12px;">III. Nama Peserta KB</span>
                                        </label>
                                        <div class="new-label-kb-div">
                                            <span style="width:5%; float: left">:</span>
                                            <input type="text" style="width: 95%;" name="meta[kb][nama_peserta_kb]" value="<?=isset($kia) ? $kia['kb']['nama_peserta_kb'] : ''?>">
                                        </div>
                                    </td>
                                    <td style="width: 50%">
                                        <label class="new-label-kb">
                                            <span style="font-size: 12px;">IV. Tgl Lahir/Umur Istri</span>
                                        </label>
                                        <div class="new-label-kb-div">
                                            <span style="width:5%; float: left">:</span>
                                            <input type="date" style="width: 70%; float: left" id="tgl_lahir_istri" name="meta[kb][tgl_lahir_istri]" value="<?=isset($kia) ? $kia['kb']['tgl_lahir_istri'] : ''?>">
                                            <span style="width:5%; float: left">/</span>
                                            <input type="text" style="width: 20%; float: left" id="umur_istri" name="meta[kb][umur_istri]" value="<?=isset($kia) ? $kia['kb']['umur_istri'] : ''?>">
                                        </div>
                                    </td>
                                </tr>
                                <tr class="not-bold">
                                    <td>
                                        <label class="new-label-kb">
                                            <span style="font-size: 12px;">V. Nama Suami/Istri</span>
                                        </label>
                                        <div class="new-label-kb-div">
                                            <span style="width:5%; float: left">:</span>
                                            <input type="text" style="width: 95%;" name="meta[kb][nama_suami_istri]" value="<?=isset($kia) ? $kia['kb']['nama_suami_istri'] : ''?>">
                                        </div>
                                    </td>
                                    <td>
                                        <span style="font-size: 12px;">VI. Pendidikan </span>
                                        <div>
                                            <label class="new-label-kb">
                                                <span style="font-size: 12px;">Pendidikan Suami</span>
                                            </label>
                                            <div class="new-label-kb-div">
                                                <span style="width:5%; float: left">:</span>
                                                <div class="col-sm-11">
                                                    <select name="meta[kb][pendidikan_suami]" style="font-size: 12px;">
                                                        <option value="">--Pilih Pendidikan--</option>
                                                        <option value="1" <?=isset($kia) && $kia['kb']['pendidikan_suami'] == 1 ? 'selected' : ''?>>Tidak Tamat SD/MI</option>
                                                        <option value="2" <?=isset($kia) && $kia['kb']['pendidikan_suami'] == 2 ? 'selected' : ''?>>Tamat SD/MI</option>
                                                        <option value="3" <?=isset($kia) && $kia['kb']['pendidikan_suami'] == 3 ? 'selected' : ''?>>Tamat SLTP/MTsN</option>
                                                        <option value="4" <?=isset($kia) && $kia['kb']['pendidikan_suami'] == 4 ? 'selected' : ''?>>Tamat SLTA/MA</option>
                                                        <option value="5" <?=isset($kia) && $kia['kb']['pendidikan_suami'] == 5 ? 'selected' : ''?>>Tamat PT</option>
                                                        <option value="6" <?=isset($kia) && $kia['kb']['pendidikan_suami'] == 6 ? 'selected' : ''?>>Tidak Sekolah</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div style="clear: both">
                                            <label class="new-label-kb">
                                                <span style="font-size: 12px;">Pendidikan Istri</span>
                                            </label>
                                            <div class="new-label-kb-div">
                                                <span style="width:5%; float: left">:</span>
                                                <div class="col-sm-11">
                                                    <select name="meta[kb][pendidikan_istri]" style="font-size: 12px;">
                                                        <option value="">--Pilih Pendidikan--</option>
                                                        <option value="1" <?=isset($kia) && $kia['kb']['pendidikan_istri'] == 1 ? 'selected' : ''?>>Tidak Tamat SD/MI</option>
                                                        <option value="2" <?=isset($kia) && $kia['kb']['pendidikan_istri'] == 2 ? 'selected' : ''?>>Tamat SD/MI</option>
                                                        <option value="3" <?=isset($kia) && $kia['kb']['pendidikan_istri'] == 3 ? 'selected' : ''?>>Tamat SLTP/MTsN</option>
                                                        <option value="4" <?=isset($kia) && $kia['kb']['pendidikan_istri'] == 4 ? 'selected' : ''?>>Tamat SLTA/MA</option>
                                                        <option value="5" <?=isset($kia) && $kia['kb']['pendidikan_istri'] == 5 ? 'selected' : ''?>>Tamat PT</option>
                                                        <option value="6" <?=isset($kia) && $kia['kb']['pendidikan_istri'] == 6 ? 'selected' : ''?>>Tidak Sekolah</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr class="not-bold">
                                    <td style="width: 50%">
                                        <label class="new-label-kb">
                                            <span style="font-size: 12px;">VII. Alamat Peserta KB</span>
                                        </label>
                                        <div class="new-label-kb-div">
                                            <span style="width:5%; float: left">:</span>
                                            <input type="text" style="width: 95%;" name="meta[kb][alamat_peserta_kb]" value="<?=isset($kia) ? $kia['kb']['alamat_peserta_kb'] : ''?>">
                                        </div>
                                    </td>
                                    <td style="width: 50%">
                                        <span style="font-size: 12px;">VIII. Pekerjaan </span>
                                        <div>
                                            <label class="new-label-kb">
                                                <span style="font-size: 12px;">Pekerjaan Suami</span>
                                            </label>
                                            <div class="new-label-kb-div">
                                                <span style="width:5%; float: left">:</span>
                                                <div class="col-sm-11">
                                                    <select id="pekerjaan_suami" style="font-size: 12px; float: left" name="meta[kb][pekerjaan_suami]">
                                                        <option value="">--Pilih Pekerjaan--</option>
                                                        <option value="1" <?=isset($kia) && $kia['kb']['pekerjaan_suami'] == 1 ? 'selected' : ''?>>Petani</option>
                                                        <option value="2" <?=isset($kia) && $kia['kb']['pekerjaan_suami'] == 2 ? 'selected' : ''?>>Nelayan</option>
                                                        <option value="3" <?=isset($kia) && $kia['kb']['pekerjaan_suami'] == 3 ? 'selected' : ''?>>Pedagang</option>
                                                        <option value="4" <?=isset($kia) && $kia['kb']['pekerjaan_suami'] == 4 ? 'selected' : ''?>>PNS/TNI/POLRI</option>
                                                        <option value="5" <?=isset($kia) && $kia['kb']['pekerjaan_suami'] == 5 ? 'selected' : ''?>>Pegawai Swasta</option>
                                                        <option value="6" <?=isset($kia) && $kia['kb']['pekerjaan_suami'] == 6 ? 'selected' : ''?>>Wiraswasta</option>
                                                        <option value="7" <?=isset($kia) && $kia['kb']['pekerjaan_suami'] == 7 ? 'selected' : ''?>>Pensiunan</option>
                                                        <option value="8" <?=isset($kia) && $kia['kb']['pekerjaan_suami'] == 8 ? 'selected' : ''?>>Pekerja Lepas</option>
                                                        <option value="9" <?=isset($kia) && $kia['kb']['pekerjaan_suami'] == 9 ? 'selected' : ''?>>Lainnya</option>
                                                        <option value="10" <?=isset($kia) && $kia['kb']['pekerjaan_suami'] == 10 ? 'selected' : ''?>>Tidak Bekerja</option>
                                                    </select>
                                                    <input type="text"
                                                           style="width: 80%; margin: 5px; float: left"
                                                           id="pekerjaan_suami_lainnya"
                                                           name="meta[kb][pekerjaan_suami_lainnya]"
                                                           value="<?=isset($kia) ? $kia['kb']['pekerjaan_suami_lainnya'] : ''?>">
                                                </div>
                                            </div>
                                        </div>
                                        <div style="clear: both">
                                            <label class="new-label-kb">
                                                <span style="font-size: 12px;">Pekerjaan Istri</span>
                                            </label>
                                            <div class="new-label-kb-div">
                                                <span style="width:5%; float: left">:</span>
                                                <div class="col-sm-11">
                                                    <select id="pekerjaan_istri" name="meta[kb][pekerjaan_istri]" style="font-size: 12px;">
                                                        <option value="">--Pilih Pekerjaan--</option>
                                                        <option value="1" <?=isset($kia) && $kia['kb']['pekerjaan_istri'] == 1 ? 'selected' : ''?>>Petani</option>
                                                        <option value="2" <?=isset($kia) && $kia['kb']['pekerjaan_istri'] == 2 ? 'selected' : ''?>>Nelayan</option>
                                                        <option value="3" <?=isset($kia) && $kia['kb']['pekerjaan_istri'] == 3 ? 'selected' : ''?>>Pedagang</option>
                                                        <option value="4" <?=isset($kia) && $kia['kb']['pekerjaan_istri'] == 4 ? 'selected' : ''?>>PNS/TNI/POLRI</option>
                                                        <option value="5" <?=isset($kia) && $kia['kb']['pekerjaan_istri'] == 5 ? 'selected' : ''?>>Pegawai Swasta</option>
                                                        <option value="6" <?=isset($kia) && $kia['kb']['pekerjaan_istri'] == 6 ? 'selected' : ''?>>Wiraswasta</option>
                                                        <option value="7" <?=isset($kia) && $kia['kb']['pekerjaan_istri'] == 7 ? 'selected' : ''?>>Pensiunan</option>
                                                        <option value="8" <?=isset($kia) && $kia['kb']['pekerjaan_istri'] == 8 ? 'selected' : ''?>>Pekerja Lepas</option>
                                                        <option value="9" <?=isset($kia) && $kia['kb']['pekerjaan_istri'] == 9 ? 'selected' : ''?>>Lainnya</option>
                                                        <option value="10" <?=isset($kia) && $kia['kb']['pekerjaan_istri'] == 10 ? 'selected' : ''?>>Tidak Bekerja</option>
                                                    </select>
                                                    <input type="text"
                                                           style="width: 80%; margin: 5px; float: left"
                                                           id="pekerjaan_istri_lainnya"
                                                           name="meta[kb][pekerjaan_istri_lainnya]"
                                                           value="<?=isset($kia) ? $kia['kb']['pekerjaan_istri_lainnya'] : ''?>">
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr class="not-bold">
                                    <td colspan="2">
                                        <div>
                                            <label class="new-label-kb">
                                                <span style="font-size: 12px;">IX. Penggunaan Asuransi</span>
                                            </label>
                                            <div class="new-label-kb-div">
                                                <span style="width:5%; float: left">:</span>
                                                <div class="col-sm-11">
                                                    <select name="meta[kb][penggunaan_asuransi]"style="font-size: 12px;">
                                                        <option value="">--Pilih Asuransi--</option>
                                                        <option value="1" <?=isset($kia) && $kia['kb']['penggunaan_asuransi'] == 1 ? 'selected' : ''?>>BPJS Kesehatan</option>
                                                        <option value="2" <?=isset($kia) && $kia['kb']['penggunaan_asuransi'] == 2 ? 'selected' : ''?>>Lainnya</option>
                                                        <option value="3" <?=isset($kia) && $kia['kb']['penggunaan_asuransi'] == 3 ? 'selected' : ''?>>Tidak</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="table-responsive">
                            <table class="table table-striped hd">
                                <tbody>
                                <tr class="not-bold">
                                    <td style="width: 50%">
                                        <span style="font-size: 12px;">X. Jumlah Anak Hidup </span>
                                        <div>
                                            <label class="new-label-kb">
                                                <span style="font-size: 12px;">Laki-Laki</span>
                                            </label>
                                            <div class="new-label-kb-div">
                                                <span style="width:5%; float: left">:</span>
                                                <input type="text" style="width: 95%;" name="meta[kb][jumlah_anak_hidup][laki_laki]" value="<?=isset($kia) ? $kia['kb']['jumlah_anak_hidup']['laki_laki'] : ''?>">
                                            </div>
                                        </div>
                                        <div style="clear: both">
                                            <label class="new-label-kb">
                                                <span style="font-size: 12px;">Perempuan</span>
                                            </label>
                                            <div class="new-label-kb-div">
                                                <span style="width:5%; float: left">:</span>
                                                <input type="text" style="width: 95%;" name="meta[kb][jumlah_anak_hidup][perempuan]" value="<?=isset($kia) ? $kia['kb']['jumlah_anak_hidup']['perempuan'] : ''?>">
                                            </div>
                                        </div>
                                    </td>
                                    <td style="width: 50%">
                                        <span style="font-size: 12px;">XI. Umur anak terakhir yg masih hidup</span>
                                        <div>
                                            <label class="new-label-kb">
                                                <span style="font-size: 12px;">Tahun</span>
                                            </label>
                                            <div class="new-label-kb-div">
                                                <span style="width:5%; float: left">:</span>
                                                <input type="text" style="width: 95%;" name="meta[kb][umur_anak_terakhir][tahun]" value="<?=isset($kia) ? $kia['kb']['umur_anak_terakhir']['tahun'] : ''?>">
                                            </div>
                                        </div>
                                        <div style="clear: both">
                                            <label class="new-label-kb">
                                                <span style="font-size: 12px;">Bulan</span>
                                            </label>
                                            <div class="new-label-kb-div">
                                                <span style="width:5%; float: left">:</span>
                                                <input type="text" style="width: 95%;" name="meta[kb][umur_anak_terakhir][bulan]" value="<?=isset($kia) ? $kia['kb']['umur_anak_terakhir']['bulan'] : ''?>">
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr class="not-bold">
                                    <td style="width: 50%">
                                        <div>
                                            <label class="new-label-kb">
                                                <span style="font-size: 12px;">XII. Status Peserta KB</span>
                                            </label>
                                            <div class="new-label-kb-div">
                                                <span style="width:5%; float: left">:</span>
                                                <div class="col-sm-11">
                                                    <select name="meta[kb][status_peserta_kb]" style="width: 100%; font-size: 12px">
                                                        <option value="">--Pilih Status--</option>
                                                        <option value="1" <?=isset($kia) && $kia['kb']['status_peserta_kb'] == 1 ? 'selected' : ''?>>Baru pertama kali</option>
                                                        <option value="2" <?=isset($kia) && $kia['kb']['status_peserta_kb'] == 2 ? 'selected' : ''?>>Pernah pakai alat KB berhenti sesudah bersalin/keguguran</option>
                                                        <option value="3" <?=isset($kia) && $kia['kb']['status_peserta_kb'] == 3 ? 'selected' : ''?>>Pernah pakai alat KB</option>
                                                        <option value="4" <?=isset($kia) && $kia['kb']['status_peserta_kb'] == 4 ? 'selected' : ''?>>Sedang ber-KB</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                    <td style="width: 50%">
                                        <div>
                                            <label class="new-label-kb">
                                                <span style="font-size: 12px;">XIII. Alat/Obat/Cara KB Terakhir</span>
                                            </label>
                                            <div class="new-label-kb-div">
                                                <span style="width:5%; float: left">:</span>
                                                <div class="col-sm-11">
                                                    <select name="meta[kb][alat_kb_terakhir]" style="font-size: 12px;">
                                                        <option value="">--Pilih--</option>
                                                        <option value="1" <?=isset($kia) && $kia['kb']['alat_kb_terakhir'] == 1 ? 'selected' : ''?>>Suntikan satu bulanan</option>
                                                        <option value="2" <?=isset($kia) && $kia['kb']['alat_kb_terakhir'] == 2 ? 'selected' : ''?>>Suntikan 3 bulanan</option>
                                                        <option value="3" <?=isset($kia) && $kia['kb']['alat_kb_terakhir'] == 3 ? 'selected' : ''?>>Pil</option>
                                                        <option value="4" <?=isset($kia) && $kia['kb']['alat_kb_terakhir'] == 4 ? 'selected' : ''?>>Kondom</option>
                                                        <option value="5" <?=isset($kia) && $kia['kb']['alat_kb_terakhir'] == 5 ? 'selected' : ''?>>Implan 1 batang</option>
                                                        <option value="6" <?=isset($kia) && $kia['kb']['alat_kb_terakhir'] == 6 ? 'selected' : ''?>>Implan 2 batang</option>
                                                        <option value="7" <?=isset($kia) && $kia['kb']['alat_kb_terakhir'] == 7 ? 'selected' : ''?>>IUD CuT 380A</option>
                                                        <option value="8" <?=isset($kia) && $kia['kb']['alat_kb_terakhir'] == 8 ? 'selected' : ''?>>IUD Lain-lain</option>
                                                        <option value="9" <?=isset($kia) && $kia['kb']['alat_kb_terakhir'] == 9 ? 'selected' : ''?>>Tubektorni</option>
                                                        <option value="10" <?=isset($kia) && $kia['kb']['alat_kb_terakhir'] == 10 ? 'selected' : ''?>>Vasektomi</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="table-responsive">
                            <table class="table table-striped hd">
                                <tbody>
                                <tr>
                                    <td colspan="2">
                                        <label class="control-label" style="font-size: 12px;">XIV. Penapisan (Skrining) untuk menentukan alat kontrasepsi yang dapat digunakan calon peserta KB</label><br>
                                        <span style="font-size: 12px;">Petunjuk: Periksalah keadaan berikut ini dan hasilnya ditulis dengan angka atau tanda centang pada kotak yang tersedia.</span><br>
                                        <span style="font-size: 12px;">Penapisan (skrining) hanya boleh dilakukan oleh pelaksana yang telah dilatih dalam pelayanan KB.</span>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <label class="control-label" style="font-size: 12px;">Anamnese</label><br>
                                    </td>
                                </tr>
                                <tr class="not-bold">
                                    <td style="width: 50%">
                                        <label class="new-label-kb">
                                            <span style="font-size: 12px;">1. Haid terakhir tanggal</span>
                                        </label>
                                        <div class="new-label-kb-div">
                                            <span style="width:5%; float: left">:</span>
                                            <input type="date" style="width: 95%; float: left" id="haid_terakhir_tgl" name="meta[kb][haid_terakhir_tgl]" value="<?=isset($kia) ? $kia['kb']['haid_terakhir_tgl'] : ''?>">
                                        </div>
                                    </td>
                                    <td style="width: 50%; font-size: 12px">
                                        <label class="new-label-kb">
                                            <span style="font-size: 12px;">2. Hamil/ Diduga Hamil</span>
                                        </label>
                                        <div class="new-label-kb-div">
                                            <span style="width:5%; float: left">:</span>
                                            <div class="form-check my-checkbox inline-input-next" style="padding-top: 8px;">
                                                <input type="radio" class="form-check-input"
                                                       name="meta[kb][hamil_diduga_hamil]"
                                                       value="ya"
                                                    <?=isset($kia) && $kia['kb']['hamil_diduga_hamil'] == 'ya' ? 'checked' : ''?>>
                                                <label class="form-check-label">Ya</label>
                                            </div>
                                            <div class="form-check my-checkbox inline-input-next" style="padding-top: 8px;">
                                                <input type="radio" class="form-check-input"
                                                       name="meta[kb][hamil_diduga_hamil]"
                                                       value="tidak"
                                                    <?=isset($kia) && $kia['kb']['hamil_diduga_hamil'] == 'tidak' ? 'checked' : ''?>>
                                                <label class="form-check-label">Tidak</label>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr class="not-bold">
                                    <td colspan="2">
                                        <label class="new-label-kb">
                                            <span style="font-size: 12px;">3. Jumlah GPA</span>
                                        </label>
                                        <div class="row" style="clear: both; width: calc(100% - 10px)">
                                            <div class="col-sm-4">
                                                <label class="new-label-kb">
                                                    <span style="font-size: 12px;">Gravida (Kehamilan)</span>
                                                </label>
                                                <div class="new-label-kb-div">
                                                    <span style="width:5%; float: left">:</span>
                                                    <input type="text" style="width: 95%;" name="meta[kb][jumlah_gpa][gravida]" value="<?=isset($kia) ? $kia['kb']['jumlah_gpa']['gravida'] : ''?>">
                                                </div>
                                            </div>
                                            <div class="col-sm-4">
                                                <label class="new-label-kb">
                                                    <span style="font-size: 12px;">Partus (Persalinan)</span>
                                                </label>
                                                <div class="new-label-kb-div">
                                                    <span style="width:5%; float: left">:</span>
                                                    <input type="text" style="width: 95%;" name="meta[kb][jumlah_gpa][partus]" value="<?=isset($kia) ? $kia['kb']['jumlah_gpa']['partus'] : ''?>">
                                                </div>
                                            </div>
                                            <div class="col-sm-4">
                                                <label class="new-label-kb">
                                                    <span style="font-size: 12px;">Abortus (Keguguran)</span>
                                                </label>
                                                <div class="new-label-kb-div">
                                                    <span style="width:5%; float: left">:</span>
                                                    <input type="text" style="width: 95%;" name="meta[kb][jumlah_gpa][abortus]" value="<?=isset($kia) ? $kia['kb']['jumlah_gpa']['abortus'] : ''?>">
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr class="not-bold">
                                    <td style="font-size: 12px">
                                        <label class="new-label-kb">
                                            <span style="font-size: 12px;">4. Menyusui</span>
                                        </label>
                                        <div class="new-label-kb-div">
                                            <span style="width:5%; float: left">:</span>
                                            <div class="form-check my-checkbox inline-input-next" style="padding-top: 4px;">
                                                <input type="radio" class="form-check-input"
                                                       name="meta[kb][menyusui]"
                                                       value="ya"
                                                    <?=isset($kia) && $kia['kb']['menyusui'] == 'ya' ? 'checked' : ''?>>
                                                <label class="form-check-label">Ya</label>
                                            </div>
                                            <div class="form-check my-checkbox inline-input-next" style="padding-top: 4px;">
                                                <input type="radio" class="form-check-input"
                                                       name="meta[kb][menyusui]"
                                                       value="tidak"
                                                    <?=isset($kia) && $kia['kb']['menyusui'] == 'tidak' ? 'checked' : ''?>>
                                                <label class="form-check-label">Tidak</label>
                                            </div>
                                        </div>
                                    </td>
                                    <td></td>
                                </tr>
                                <tr class="not-bold">
                                    <td style="font-size: 12px" colspan="2">
                                        <label class="new-label-kb" style="width: 100% !important;">
                                            <span style="font-size: 12px;">5. Riwayat Penyakit Sebelumnya</span>
                                        </label>
                                        <div class="new-label-kb-div" style="clear: both">
                                            <label class="new-label">
                                                <span style="float: left">a. Sakit kuning :</span>
                                            </label>
                                            <div class="new-label-div">
                                                <div class="form-check my-checkbox inline-input-next" style="padding-top: 4px;">
                                                    <input type="radio" class="form-check-input"
                                                           name="meta[kb][sakit_kuning]"
                                                           value="ya"
                                                        <?=isset($kia) && $kia['kb']['sakit_kuning'] == 'ya' ? 'checked' : ''?>>
                                                    <label class="form-check-label">Ya</label>
                                                </div>
                                                <div class="form-check my-checkbox inline-input-next" style="padding-top: 4px;">
                                                    <input type="radio" class="form-check-input"
                                                           name="meta[kb][sakit_kuning]"
                                                           value="tidak"
                                                        <?=isset($kia) && $kia['kb']['sakit_kuning'] == 'tidak' ? 'checked' : ''?>>
                                                    <label class="form-check-label">Tidak</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="new-label-kb-div" style="clear: both">
                                            <label style="float: left">
                                                <span style="float: left">b. Perdarahan pervaginam yang tdk diketahui sebelumnya :</span>
                                            </label>
                                            <div class="new-label-div" style="width: auto !important;">
                                                <div class="form-check my-checkbox inline-input-next" style="padding-top: 4px;">
                                                    <input type="radio" class="form-check-input"
                                                           name="meta[kb][perdarahan]"
                                                           value="ya"
                                                        <?=isset($kia) && $kia['kb']['perdarahan'] == 'ya' ? 'checked' : ''?>>
                                                    <label class="form-check-label">Ya</label>
                                                </div>
                                                <div class="form-check my-checkbox inline-input-next" style="padding-top: 4px;">
                                                    <input type="radio" class="form-check-input"
                                                           name="meta[kb][perdarahan]"
                                                           value="tidak"
                                                        <?=isset($kia) && $kia['kb']['perdarahan'] == 'tidak' ? 'checked' : ''?>>
                                                    <label class="form-check-label">Tidak</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="new-label-kb-div" style="clear: both">
                                            <label style="float: left">
                                                <span style="float: left">c. Keputihan yang lama :</span>
                                            </label>
                                            <div class="new-label-div" style="width: auto !important;">
                                                <div class="form-check my-checkbox inline-input-next" style="padding-top: 4px;">
                                                    <input type="radio" class="form-check-input"
                                                           name="meta[kb][keputihan]"
                                                           value="ya"
                                                        <?=isset($kia) && $kia['kb']['keputihan'] == 'ya' ? 'checked' : ''?>>
                                                    <label class="form-check-label">Ya</label>
                                                </div>
                                                <div class="form-check my-checkbox inline-input-next" style="padding-top: 4px;">
                                                    <input type="radio" class="form-check-input"
                                                           name="meta[kb][keputihan]"
                                                           value="tidak"
                                                        <?=isset($kia) && $kia['kb']['keputihan'] == 'tidak' ? 'checked' : ''?>>
                                                    <label class="form-check-label">Tidak</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="new-label-kb-div" style="clear: both">
                                            <label style="float: left">
                                                <span style="float: left">d. Tumor</span>
                                            </label>
                                            <ul style="float: left"><li>Payudara</li><li>Rahim</li><li>Indung telur</li></ul>
                                            <div class="new-label-div" style="width: auto !important; float: left">
                                                <div class="form-check my-checkbox inline-input-next" style="padding-top: 4px; float: left">
                                                    <input type="radio" class="form-check-input"
                                                           style="float: left"
                                                           name="meta[kb][tumor]"
                                                           value="ya"
                                                        <?=isset($kia) && $kia['kb']['tumor'] == 'ya' ? 'checked' : ''?>>
                                                    <label class="form-check-label">Ya</label>
                                                </div>
                                                <div class="form-check my-checkbox inline-input-next" style="padding-top: 4px;">
                                                    <input type="radio" class="form-check-input"
                                                           name="meta[kb][tumor]"
                                                           value="tidak"
                                                        <?=isset($kia) && $kia['kb']['tumor'] == 'tidak' ? 'checked' : ''?>>
                                                    <label class="form-check-label">Tidak</label>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <label class="control-label" style="font-size: 12px;">Pemeriksaan</label><br>
                                    </td>
                                </tr>
                                <tr class="not-bold">
                                    <td style="width: 50%; font-size: 12px;">
                                        <div>
                                            <label class="new-label-kb">
                                                <span style="font-size: 12px;">6. Keadaan Umum</span>
                                            </label>
                                            <div class="new-label-kb-div">
                                                <span style="width:5%; float: left">:</span>
                                                <div class="col-sm-11">
                                                    <select name="meta[kb][keadaan_umum]">
                                                        <option value="">--Pilih--</option>
                                                        <option value="1" <?=isset($kia) && $kia['kb']['keadaan_umum'] == 1 ? 'selected' : ''?>>Baik</option>
                                                        <option value="2" <?=isset($kia) && $kia['kb']['keadaan_umum'] == 2 ? 'selected' : ''?>>Sedang</option>
                                                        <option value="3" <?=isset($kia) && $kia['kb']['keadaan_umum'] == 3 ? 'selected' : ''?>>Kurang</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                    <td style="width: 50%">
                                        <label class="new-label-kb">
                                            <span style="font-size: 12px;">7. Berat Badan</span>
                                        </label>
                                        <div class="new-label-kb-div">
                                            <span style="width:5%; float: left">:</span>
                                            <input type="text" style="width: 70px; float: left" id="berat_badan" name="meta[kb][berat_badan]" value="<?=isset($kia) ? $kia['kb']['berat_badan'] : ''?>">
                                            <span style="width:5%; float: left">Kg</span>
                                        </div>
                                    </td>
                                </tr>
                                <tr class="not-bold">
                                    <td colspan="2">
                                        <label class="new-label-kb">
                                            <span style="font-size: 12px;">8. Tekanan Darah</span>
                                        </label>
                                        <div class="new-label-kb-div">
                                            <span style="width: 10px; float: left">:</span>
                                            <input type="text" style="width: 150px;" name="meta[kb][tekanan_darah]" value="<?=isset($kia) ? $kia['kb']['tekanan_darah'] : ''?>">
                                        </div>
                                    </td>
                                </tr>
                                <tr class="not-bold">
                                    <td style="font-size: 12px; width: 50%">
                                        <label class="new-label-kb" style="width: 100% !important;">
                                            <span style="font-size: 12px;">9. Sebelum dilakukan pemasangan UID atau Tubektomi dilakukan pemeriksaan dalam</span>
                                        </label>
                                        <div class="new-label-kb-div" style="clear: both">
                                            <label style="float: left">
                                                <span style="float: left">a. Tanda-tanda radang :</span>
                                            </label>
                                            <div class="new-label-div" style="width: auto !important;">
                                                <div class="form-check my-checkbox inline-input-next" style="padding-top: 4px;">
                                                    <input type="radio" class="form-check-input"
                                                           name="meta[kb][tanda_tanda_radang]"
                                                           value="ya"
                                                        <?=isset($kia) && $kia['kb']['tanda_tanda_radang'] == 'ya' ? 'checked' : ''?>>
                                                    <label class="form-check-label">Ya</label>
                                                </div>
                                                <div class="form-check my-checkbox inline-input-next" style="padding-top: 4px;">
                                                    <input type="radio" class="form-check-input"
                                                           name="meta[kb][tanda_tanda_radang]"
                                                           value="tidak"
                                                        <?=isset($kia) && $kia['kb']['tanda_tanda_radang'] == 'tidak' ? 'checked' : ''?>>
                                                    <label class="form-check-label">Tidak</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="new-label-kb-div" style="clear: both">
                                            <label style="float: left">
                                                <span style="float: left">b. Tumor/keganasan ginekologi :</span>
                                            </label>
                                            <div class="new-label-div" style="width: auto !important;">
                                                <div class="form-check my-checkbox inline-input-next" style="padding-top: 4px;">
                                                    <input type="radio" class="form-check-input"
                                                           name="meta[kb][tumor_keganasan]"
                                                           value="ya"
                                                        <?=isset($kia) && $kia['kb']['tumor_keganasan'] == 'ya' ? 'checked' : ''?>>
                                                    <label class="form-check-label">Ya</label>
                                                </div>
                                                <div class="form-check my-checkbox inline-input-next" style="padding-top: 4px;">
                                                    <input type="radio" class="form-check-input"
                                                           name="meta[kb][tumor_keganasan]"
                                                           value="tidak"
                                                        <?=isset($kia) && $kia['kb']['tumor_keganasan'] == 'tidak' ? 'checked' : ''?>>
                                                    <label class="form-check-label">Tidak</label>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                    <td style="font-size: 12px; width: 50%">
                                        <div class="callout callout-info">
                                            <p>Bila semua jawaban <strong>TIDAK</strong>, pemasangan IUD atau tindakan Tubektomi dapat dilakukan. Bila salah satu jawaban <strong>YA</strong>, rujuk ke dokter.</p>
                                        </div>
                                    </td>
                                </tr>
                                <tr class="not-bold">
                                    <td style="width: 50%; font-size: 12px;">
                                        <div>
                                            <label class="new-label-kb">
                                                <span style="font-size: 12px;">10. Posisi Rahim</span>
                                            </label>
                                            <div class="new-label-kb-div">
                                                <span style="width:5%; float: left">:</span>
                                                <div class="col-sm-11">
                                                    <select name="meta[kb][posisi_rahim]">
                                                        <option value="">--Pilih--</option>
                                                        <option value="1" <?=isset($kia) && $kia['kb']['posisi_rahim'] == 1 ? 'selected' : ''?>>Retrofleksi</option>
                                                        <option value="2" <?=isset($kia) && $kia['kb']['posisi_rahim'] == 2 ? 'selected' : ''?>>Antefleksi</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                    <td style="width: 50%">
                                    </td>
                                </tr>
                                <tr class="not-bold">
                                    <td style="font-size: 12px; width: 50%">
                                        <label class="new-label-kb" style="width: 100% !important;">
                                            <span style="font-size: 12px;">11. Pemeriksaan tambahan (khusus untuk calon Vasektomi dan Tubektomi)</span>
                                        </label>
                                        <div class="new-label-kb-div" style="clear: both">
                                            <label style="float: left">
                                                <span style="float: left">a. Tanda-tanda diabetes :</span>
                                            </label>
                                            <div class="new-label-div" style="width: auto !important;">
                                                <div class="form-check my-checkbox inline-input-next" style="padding-top: 4px;">
                                                    <input type="radio" class="form-check-input"
                                                           name="meta[kb][tanda_tanda_diabetes]"
                                                           value="ya"
                                                        <?=isset($kia) && $kia['kb']['tanda_tanda_diabetes'] == 'ya' ? 'checked' : ''?>>
                                                    <label class="form-check-label">Ya</label>
                                                </div>
                                                <div class="form-check my-checkbox inline-input-next" style="padding-top: 4px;">
                                                    <input type="radio" class="form-check-input"
                                                           name="meta[kb][tanda_tanda_diabetes]"
                                                           value="tidak"
                                                        <?=isset($kia) && $kia['kb']['tanda_tanda_diabetes'] == 'tidak' ? 'checked' : ''?>>
                                                    <label class="form-check-label">Tidak</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="new-label-kb-div" style="clear: both">
                                            <label style="float: left">
                                                <span style="float: left">b. Kelainan pembekuan darah :</span>
                                            </label>
                                            <div class="new-label-div" style="width: auto !important;">
                                                <div class="form-check my-checkbox inline-input-next" style="padding-top: 4px;">
                                                    <input type="radio" class="form-check-input"
                                                           name="meta[kb][kelainan_pembekuan_darah]"
                                                           value="ya"
                                                        <?=isset($kia) && $kia['kb']['kelainan_pembekuan_darah'] == 'ya' ? 'checked' : ''?>>
                                                    <label class="form-check-label">Ya</label>
                                                </div>
                                                <div class="form-check my-checkbox inline-input-next" style="padding-top: 4px;">
                                                    <input type="radio" class="form-check-input"
                                                           name="meta[kb][kelainan_pembekuan_darah]"
                                                           value="tidak"
                                                        <?=isset($kia) && $kia['kb']['kelainan_pembekuan_darah'] == 'tidak' ? 'checked' : ''?>>
                                                    <label class="form-check-label">Tidak</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="new-label-kb-div" style="clear: both">
                                            <label style="float: left">
                                                <span style="float: left">c. Radang orchitis/epididymitis :</span>
                                            </label>
                                            <div class="new-label-div" style="width: auto !important;">
                                                <div class="form-check my-checkbox inline-input-next" style="padding-top: 4px;">
                                                    <input type="radio" class="form-check-input"
                                                           name="meta[kb][radang_orchitis_epididymitis]"
                                                           value="ya"
                                                        <?=isset($kia) && $kia['kb']['radang_orchitis_epididymitis'] == 'ya' ? 'checked' : ''?>>
                                                    <label class="form-check-label">Ya</label>
                                                </div>
                                                <div class="form-check my-checkbox inline-input-next" style="padding-top: 4px;">
                                                    <input type="radio" class="form-check-input"
                                                           name="meta[kb][radang_orchitis_epididymitis]"
                                                           value="tidak"
                                                        <?=isset($kia) && $kia['kb']['radang_orchitis_epididymitis'] == 'tidak' ? 'checked' : ''?>>
                                                    <label class="form-check-label">Tidak</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="new-label-kb-div" style="clear: both">
                                            <label style="float: left">
                                                <span style="float: left">d. Tumor/keganasan ginekologi :</span>
                                            </label>
                                            <div class="new-label-div" style="width: auto !important;">
                                                <div class="form-check my-checkbox inline-input-next" style="padding-top: 4px;">
                                                    <input type="radio" class="form-check-input"
                                                           name="meta[kb][11_tumor_keganasan_ginekologi]"
                                                           value="ya"
                                                        <?=isset($kia) && $kia['kb']['11_tumor_keganasan_ginekologi'] == 'ya' ? 'checked' : ''?>>
                                                    <label class="form-check-label">Ya</label>
                                                </div>
                                                <div class="form-check my-checkbox inline-input-next" style="padding-top: 4px;">
                                                    <input type="radio" class="form-check-input"
                                                           name="meta[kb][11_tumor_keganasan_ginekologi]"
                                                           value="tidak"
                                                        <?=isset($kia) && $kia['kb']['11_tumor_keganasan_ginekologi'] == 'tidak' ? 'checked' : ''?>>
                                                    <label class="form-check-label">Tidak</label>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                    <td style="font-size: 12px; width: 50%">
                                        <div class="callout callout-info">
                                            <p>Bila semua jawaban <strong>TIDAK</strong>, dapat dilakukan Vasektomi. Bila salah satu jawaban <strong>YA</strong>, maka rujuklah ke Faskes KB/Rumah Sakit yang lengkap.</p>
                                        </div>
                                    </td>
                                </tr>
                                <tr class="not-bold">
                                    <td style="font-size: 12px;" colspan="2">
                                        <label class="new-label-kb" style="width: 100% !important;">
                                            <span style="font-size: 12px;">12. Alat/obat/cara kontrasepsi yang boleh dipergunakan:</span>
                                        </label>
                                        <div class="new-label-kb-div">
                                            <div class="col-sm-6">
                                                <div class="checkbox">
                                                    <input type="checkbox"
                                                           name="meta[kb][boleh][suntikan_1_bulanan]"
                                                           style="width: 50px !important;"
                                                        <?=isset($kia) && $kia['kb']['boleh']['suntikan_1_bulanan'] ? 'checked' : ''?>>
                                                    <label>Suntikan 1 Bulanan</label>
                                                </div>
                                                <div class="checkbox">
                                                    <input type="checkbox"
                                                           name="meta[kb][boleh][suntikan_3_bulanan]"
                                                           style="width: 50px !important;"
                                                        <?=isset($kia) && $kia['kb']['boleh']['suntikan_3_bulanan'] ? 'checked' : ''?>>
                                                    <label>Suntikan 3 Bulanan</label>
                                                </div>
                                                <div class="checkbox">
                                                    <input type="checkbox"
                                                           name="meta[kb][boleh][pil]"
                                                           style="width: 50px !important;"
                                                        <?=isset($kia) && $kia['kb']['boleh']['pil'] ? 'checked' : ''?>>
                                                    <label>Pil</label>
                                                </div>
                                                <div class="checkbox">
                                                    <input type="checkbox"
                                                           name="meta[kb][boleh][kondom]"
                                                           style="width: 50px !important;"
                                                        <?=isset($kia) && $kia['kb']['boleh']['kondom'] ? 'checked' : ''?>>
                                                    <label>Kondom</label>
                                                </div>
                                                <div class="checkbox">
                                                    <input type="checkbox"
                                                           name="meta[kb][boleh][implan_1_batang]"
                                                           style="width: 50px !important;"
                                                        <?=isset($kia) && $kia['kb']['boleh']['implan_1_batang'] ? 'checked' : ''?>>
                                                    <label>Implan 1 Batang</label>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="checkbox">
                                                    <input type="checkbox"
                                                           name="meta[kb][boleh][implan_2_batang]"
                                                           style="width: 50px !important;"
                                                        <?=isset($kia) && $kia['kb']['boleh']['implan_2_batang'] ? 'checked' : ''?>>
                                                    <label>Implan 2 Batang</label>
                                                </div>
                                                <div class="checkbox">
                                                    <input type="checkbox"
                                                           name="meta[kb][boleh][iud_cut]"
                                                           style="width: 50px !important;"
                                                        <?=isset($kia) && $kia['kb']['boleh']['iud_cut'] ? 'checked' : ''?>>
                                                    <label>IUD CuT 380A</label>
                                                </div>
                                                <div class="checkbox">
                                                    <input type="checkbox"
                                                           name="meta[kb][boleh][iud_lain]"
                                                           style="width: 50px !important;"
                                                        <?=isset($kia) && $kia['kb']['boleh']['iud_lain'] ? 'checked' : ''?>>
                                                    <label>IUD Lain-Lain</label>
                                                </div>
                                                <div class="checkbox">
                                                    <input type="checkbox"
                                                           name="meta[kb][boleh][tubektomi]"
                                                           style="width: 50px !important;"
                                                        <?=isset($kia) && $kia['kb']['boleh']['tubektomi'] ? 'checked' : ''?>>
                                                    <label>Tubektomi</label>
                                                </div>
                                                <div class="checkbox">
                                                    <input type="checkbox"
                                                           name="meta[kb][boleh][vasektomi]"
                                                           style="width: 50px !important;"
                                                        <?=isset($kia) && $kia['kb']['boleh']['vasektomi'] ? 'checked' : ''?>>
                                                    <label>Vasektomi</label>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="table-responsive">
                            <table class="table table-striped hd">
                                <tbody>
                                <tr class="not-bold">
                                    <td style="width: 50%; font-size: 12px;">
                                        <div>
                                            <label class="new-label-kb">
                                                <span style="font-size: 12px;">XV. Alat/obat/cara kontrasepsi yang dipilih</span>
                                            </label>
                                            <div class="new-label-kb-div">
                                                <span style="width:5%; float: left">:</span>
                                                <div class="col-sm-11">
                                                    <select name="meta[kb][alat_kontrasepsi_dipilih]">
                                                        <option value="">--Pilih--</option>
                                                        <option value="1" <?=isset($kia) && $kia['kb']['alat_kontrasepsi_dipilih'] == 1 ? 'selected' : ''?>>Suntikan 1 Bulanan</option>
                                                        <option value="2" <?=isset($kia) && $kia['kb']['alat_kontrasepsi_dipilih'] == 2 ? 'selected' : ''?>>Suntikan 3 Bulanan</option>
                                                        <option value="3" <?=isset($kia) && $kia['kb']['alat_kontrasepsi_dipilih'] == 3 ? 'selected' : ''?>>Pil</option>
                                                        <option value="4" <?=isset($kia) && $kia['kb']['alat_kontrasepsi_dipilih'] == 4 ? 'selected' : ''?>>Kondom</option>
                                                        <option value="5" <?=isset($kia) && $kia['kb']['alat_kontrasepsi_dipilih'] == 5 ? 'selected' : ''?>>Implan 1 Batang</option>
                                                        <option value="6" <?=isset($kia) && $kia['kb']['alat_kontrasepsi_dipilih'] == 6 ? 'selected' : ''?>>Implan 2 Batang</option>
                                                        <option value="7" <?=isset($kia) && $kia['kb']['alat_kontrasepsi_dipilih'] == 7 ? 'selected' : ''?>>IUD CuT 380A</option>
                                                        <option value="8" <?=isset($kia) && $kia['kb']['alat_kontrasepsi_dipilih'] == 8 ? 'selected' : ''?>>IUD Lain-lain</option>
                                                        <option value="9" <?=isset($kia) && $kia['kb']['alat_kontrasepsi_dipilih'] == 9 ? 'selected' : ''?>>Tubektomi</option>
                                                        <option value="10" <?=isset($kia) && $kia['kb']['alat_kontrasepsi_dipilih'] == 10 ? 'selected' : ''?>>Vasektomi</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                    <td style="width: 50%">
                                        <label class="new-label-kb">
                                            <span style="font-size: 12px;">XVI. Tanggal dilayani</span>
                                        </label>
                                        <div class="new-label-kb-div">
                                            <span style="width:5%; float: left">:</span>
                                            <input type="date" style="width: 95%; float: left" id="tgl_dilayani" name="meta[kb][tgl_dilayani]" value="<?=isset($kia) ? $kia['kb']['tgl_dilayani'] : ''?>">
                                        </div>
                                    </td>
                                </tr>
                                <tr class="not-bold">
                                    <td style="width: 50%">
                                        <label class="new-label-kb">
                                            <span style="font-size: 12px;">XVII. Tanggal kunjungan ulang</span>
                                        </label>
                                        <div class="new-label-kb-div">
                                            <span style="width:5%; float: left">:</span>
                                            <input type="date" style="width: 95%; float: left" id="tgl_kunjungan_ulang" name="meta[kb][tgl_kunjungan_ulang]" value="<?=isset($kia) ? $kia['kb']['tgl_kunjungan_ulang'] : ''?>">
                                        </div>
                                    </td>
                                    <td style="width: 50%">
                                        <label class="new-label-kb">
                                            <span style="font-size: 12px;">XVIII. Tanggal dicabut <span style="font-size: 10px; font-style: italic">(khusus Implan/IUD)</span></span>
                                        </label>
                                        <div class="new-label-kb-div">
                                            <span style="width:5%; float: left">:</span>
                                            <input type="date" style="width: 95%; float: left" id="tgl_dicabut" name="meta[kb][tgl_dicabut]" value="<?=isset($kia) ? $kia['kb']['tgl_dicabut'] : ''?>">
                                        </div>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>

<script>
    function getAge(dateString) {
        const today = new Date();
        const birthDate = new Date(dateString);
        let age = today.getFullYear() - birthDate.getFullYear();
        let m = today.getMonth() - birthDate.getMonth();
        if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
            age--;
        }
        return age;
    }

    $(function () {
        const tgl_lahir_istri = $('#tgl_lahir_istri');
        tgl_lahir_istri.on('change', function (e) {
            const age = getAge(tgl_lahir_istri.val());
            $('#umur_istri').val(age);
        });

        const pekerjaan_suami = $('#pekerjaan_suami');
        $('#pekerjaan_suami_lainnya').hide();
        pekerjaan_suami.on('change', function () {
            if (pekerjaan_suami.val() === '9') {
                $('#pekerjaan_suami_lainnya').show();
            }
            else {
                $('#pekerjaan_suami_lainnya').hide();
            }
        });

        const pekerjaan_istri = $('#pekerjaan_istri');
        $('#pekerjaan_istri_lainnya').hide();
        pekerjaan_istri.on('change', function () {
            if (pekerjaan_istri.val() === '9') {
                $('#pekerjaan_istri_lainnya').show();
            }
            else {
                $('#pekerjaan_istri_lainnya').hide();
            }
        });
    });
</script>
