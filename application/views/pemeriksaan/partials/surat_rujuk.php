<div class="box box-success hidden" id="surat-rujuk">
    <div class="box-header">
        <h4>Surat Keterangan Rujuk</h4>
    </div>
    <div class="box-body">
        <div class="row">
            <div class="col-sm-12">
                <p>Kepada YTH : <input type="text" class="form-control" name="surat_form[rujuk][yth]"></p>
                <p>Dokter : <input type="text" class="form-control" name="surat_form[rujuk][dokter]"></p>
                <p>Di : <input type="text" class="form-control" name="surat_form[rujuk][di]"></p>
                <br>
                <p>Bersama ini kami konsultasikan untuk penanganan lebih lanjut penderita</p>
                <table class="table">
                    <tr>
                        <td>Nama</td>
                        <td>:</td>
                        <td><?= $pemeriksaan['nama_pasien'] ?></td>
                    </tr>
                    <tr>
                        <td>Umur</td>
                        <td>:</td>
                        <td><?= $pasien->usia?></td>
                    </tr>
                    <tr>
                        <td>Jenis Kelamin</td>
                        <td>:</td>
                        <td><?= $pasien->jk == 'L' ? 'Laki-Laki' : 'Perempuan'?></td>
                    </tr>
                    <tr>
                        <td>Alamat</td>
                        <td>:</td>
                        <td><?= $pasien->alamat?></td>
                    </tr>
                    <tr>
                        <td>Pekerjaan</td>
                        <td>:</td>
                        <td><?= $pasien->pekerjaan?></td>
                    </tr>
                </table>

                <div>
                    <p>Dengan keluhan </p>
                    <textarea class="form-control" name="surat_form[rujuk][keluhan]" rows="3" id="surat-rujuk-keluhan"><?=$pemeriksaan['keluhan_utama']?></textarea>
                    <p>Sementara kami diagnosa:  </p>
                    <textarea class="form-control" name="surat_form[rujuk][diagnosa]" rows="3" id="surat-rujuk-diagnosa"></textarea>
                    <p>Dan kami beri terapi </p>
                    <textarea class="form-control" name="surat_form[rujuk][terapi]" rows="3" id="surat-rujuk-terapi"></textarea>
                    <p>Atas bantuan dan kerjasamanya kami sampaikan terima kasih.</p>
                </div>
            </div>
        </div>
    </div>
</div>