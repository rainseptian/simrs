<script>
    function change_surat() {
        $('#surat-sehat').addClass('hidden')
        $('#surat-sakit').addClass('hidden')
        $('#surat-rujuk').addClass('hidden')
        $('#surat-ranap').addClass('hidden')

        const s = $('#surat').val()
        if (s === 'sehat') {
            $('#surat-sehat').removeClass('hidden')
        }
        else if (s === 'sakit') {
            $('#surat-sakit').removeClass('hidden')
        }
        else if (s === 'rujuk') {
            $('#surat-rujuk').removeClass('hidden')
        }
        else if (s === 'ranap') {
            $('#surat-ranap').removeClass('hidden')
        }
    }
</script>

<script>
    Date.prototype.addDays = function(days) {
        let date = new Date(this.valueOf());
        date.setDate(date.getDate() + days);
        return date;
    }

    $(function () {
        $('#jumlah_hari').on('keyup', function () {
            const start_date = $('#start_date').val()
            let parts = start_date.split('-')
            let myDate = new Date(parts[0], parts[1] - 1, parts[2]);
            myDate = myDate.addDays(+$(this).val() || 1)
            $('#end_date').val(
                myDate.getUTCFullYear() + '-' +
                ('00' + (myDate.getUTCMonth()+1)).slice(-2) + '-' +
                ('00' + myDate.getUTCDate()).slice(-2)
            )
        })

        on_diagnosis_change()
        on_tindakan_change()
    })

    const diags = <?=json_encode($penyakit->result())?>;
    const tindakans = <?=json_encode($tindakan->result())?>;

    function on_diagnosis_change(e) {
        const val = $('#diagnosis_jenis_penyakit').val()
        const icd = val.map(v => diags.find(d => +d.id === +v)).filter(v => v).map(v => `${v.kode} - ${v.nama.trim()}`).join(', ')
        const diagnosis = $('#diagnosis').val()
        let arr = []
        if (icd)
            arr.push(icd)
        if (diagnosis)
            arr.push(diagnosis)

        $('#surat-ranap-diagnosa').html(arr.join('\n'))
        $('#surat-rujuk-diagnosa').html(arr.join('\n'))

        return false
    }

    function on_tindakan_change(e) {
        const val = $('#tindakan').val()
        const icd = val.map(v => tindakans.find(d => +d.id === +v))
            .filter(v => v && v.nama !== 'Administrasi')
            .map(v => `${v.nama.trim()}`)
            .join(', ')
        $('#surat-rujuk-terapi').val(icd)

        return false
    }

</script>