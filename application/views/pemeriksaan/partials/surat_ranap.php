<div class="box box-success hidden" id="surat-ranap">
    <div class="box-header">
        <h4>Surat Pengantar Rawat Inap</h4>
    </div>
    <div class="box-body">
        <div class="row">
            <div class="col-sm-12">
                <p>Kepada Yang Terhormat</p>
                <p>Dokter / Bidan / Perawat jaga</p>
                <p><?=$klinik->nama?></p>
                <br>
                <p>Dengan Hormat,</p>
                <table class="table">
                    <tr>
                        <td>Mohon MRS</td>
                        <td>:</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>Nama</td>
                        <td>:</td>
                        <td><?= $pemeriksaan['nama_pasien'] ?></td>
                    </tr>
                    <tr>
                        <td>Diagnosa</td>
                        <td>:</td>
                        <td><textarea class="form-control" name="surat_form[ranap][diagnosa]" rows="3" id="surat-ranap-diagnosa"></textarea></td>
                    </tr>
                    <tr>
                        <td>Rencana</td>
                        <td>:</td>
                        <td><textarea class="form-control" name="surat_form[ranap][rencana]" rows="3"></textarea></td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>