<link rel="stylesheet"
      href="<?php echo base_url() ?>assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
<link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/autocomplete/style-gue.css">


<div class="col-sm-12 col-md-12 col-lg-5">
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">Permintaan Penunjang Medis</h3>
        </div>
        <div class="box-footer" style="display: flex">
            <div style="flex: 1; margin-right: 10px">
                <button type="submit" name="rujuk" value="lab" class="btn btn-success btn-block btn">
                    <i class="fa fas fa-flask"></i>
                    Laboratorium
                </button>
            </div>
            <div style="flex: 1">
                <button type="submit" name="rujuk" value="radio" class="btn btn-success btn-block btn">
                    <i class="fa fas fa-x-ray"></i>
                    Radiologi
                </button>
            </div>
        </div>
    </div>
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">Rujuk Antar Poli</h3>
        </div>
        <div class="box-body">
            <select class="form-control abdush-select" name="poli-rujuk" id="poli-rujuk">
                <option value="">--Pilih Poli Rujuk--</option>
                <?php foreach ($jenis_pendaftaran->result() as $key => $value) { ?>
                    <?php if ($value->id != 19 && $value->id != 59) : ?>
                        <option value="<?php echo $value->id ?>"><?php echo $value->nama ?></option>
                    <?php endif; ?>
                <?php } ?>
            </select>
        </div>
        <div class="box-footer text-right">
            <button type="submit" name="rujuk" value="poli" onclick="return rujuk_poli()" class="btn btn-success">Rujuk</button>
        </div>
    </div>
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">Riwayat Rawat Inap</h3>
        </div>
        <div class="box-body">
                        <form class="form-horizontal" method="get" action="<?php echo base_url() ?>Laporan/RekamMedis" >
                            <div class="row">
                                <div class="col-sm-6">
                                    <?php echo form_open('Pendaftaran/Pendaftaran_lama2', array('id' => 'form_cari')); ?>
                                    <div class="form-group">
                                        <label for="cari" class="col-sm-1 control-label">Cari</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control " name="pencarian_kode"
                                                id="pencarian_kode" placeholder="Masukkan Nama atau No RM"
                                                autocomplete="on">
                                            <input type="hidden" name="id_pasien" id='id_pasien'>
                                            <div id='hasil_pencarian'></div>
                                        </div>
                                        <div class="col-sm-2">
                                            <button type="button" class="btn btn-primary" id="detail_pasien" ><i
                                                        class="fa fa-pencil"></i> Detail
                                            </button>
                                        </div>
                                        <?php echo form_close(); ?>
                                    </div>
                                </div>
                            </div>
                        </form>
        </div>
    </div>
</div>
<script>

    $(function () {
        $('#example1').DataTable()
        $('#example2').DataTable({
            'paging': true,
            'lengthChange': false,
            'searching': false,
            'ordering': true,
            'info': true,
            'autoWidth': false
        })
    })

</script>

<script>

    $('#pencarian_kode').keypress((event) => {
        if (event.keyCode == 13) {
            event.preventDefault();
        }
    });
    $('#detail_pasien').click(() => {
        window.location.href = "<?=base_url()?>Laporan/RekamMedis/" + $('#id_pasien').val();
    });

    var timer = null;
    $(document).on('keyup', '#pencarian_kode', (e) => {
        clearTimeout(timer);
        timer = setTimeout(() => {
            if ($('#pencarian_kode').val() != '') {
                var charCode = (e.which) ? e.which : event.keyCode;

                if (charCode == 40) { //arrow down

                    if ($('.form-group').find('div#hasil_pencarian li.autocomplete_active').length > 0) {
                        var selanjutnya = $('.form-group').find('div#hasil_pencarian li.autocomplete_active').next();
                        $('.form-group').find('div#hasil_pencarian li.autocomplete_active').removeClass('autocomplete_active');
                        selanjutnya.addClass('autocomplete_active');
                    } else {
                        $('.form-group').find('div#hasil_pencarian li:first').addClass('autocomplete_active');
                    }
                } else if (charCode == 38) { //arrow up

                    if ($('.form-group').find('div#hasil_pencarian li.autocomplete_active').length > 0) {
                        var sebelumnya = $('.form-group').find('div#hasil_pencarian li.autocomplete_active').prev();
                        $('.form-group').find('div#hasil_pencarian li.autocomplete_active').removeClass('autocomplete_active');
                        sebelumnya.addClass('autocomplete_active');
                    } else {
                        $('.form-group').find('div#hasil_pencarian li:first').addClass('autocomplete_active');
                    }
                } else if (charCode == 13) { // enter

                    var Kodenya = $('.form-group').find('div#hasil_pencarian li.autocomplete_active span#kodenya').html();
                    var No_rmnya = $('.form-group').find('div#hasil_pencarian li.autocomplete_active span#no_rmnya').html();
                    var Namanya = $('.form-group').find('div#hasil_pencarian li.autocomplete_active span#namanya').html();
                    if (Kodenya) {
                        $('#pencarian_kode').val(No_rmnya + ' - ' + Namanya);
                        $('#id_pasien').val(Kodenya);
                    } else {
                        alert('data tidak ada! Pilih pilihan yang ada sebelum di enter.');
                    }

                    $('.form-group').find('div#hasil_pencarian').hide();
                } else {
                    var text = $('#form_cari').find('div#hasil_pencarian').html();
                    autoComplete($('#pencarian_kode').width(), $('#pencarian_kode').val());
                }
            } else {
                $('#form_cari').find('div#hasil_pencarian').hide();
            }
        }, 100);
    });

    $(document).on('click', '#daftar-autocomplete li', function () {
        $(this).parent().parent().parent().find('#pencarian_kode').val($(this).find('span#no_rmnya').html() + ' - ' + $(this).find('span#namanya').html());
        $(this).parent().parent().parent().find('#id_pasien').val($(this).find('span#kodenya').html());

        $('.form-group').find('#daftar-autocomplete').hide();
    });

    function autoComplete(Lebar, KataKunci) {
        let $hasil_pencarian = $('div#hasil_pencarian');
        $hasil_pencarian.hide();
        var Lebar = Lebar + 25;

        $.ajax({
            url: "<?php echo site_url('pendaftaran/ajax_kode'); ?>",
            type: "POST",
            cache: false,
            data: 'keyword=' + KataKunci,
            dataType: 'json',
            success: function (json) {
                if (json.status == 1) {
                    $hasil_pencarian.css({'width': Lebar + 'px'});
                    $hasil_pencarian.css({'display': 'block'});
                    $hasil_pencarian.show('fast');
                    $hasil_pencarian.html(json.datanya);
                }
                if (json.status == 0) {
                    $('#form_cari').find('div#hasil_pencarian').html('');
                    $hasil_pencarian.hide();
                }
            }
        });
    }
</script>

<script src="<?php echo base_url() ?>assets/bower_components/Flot/jquery.flot.js"></script>
<script src="<?php echo base_url() ?>assets/bower_components/Flot/jquery.flot.categories.js"></script>