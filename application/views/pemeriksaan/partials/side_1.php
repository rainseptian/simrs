<div class="col-sm-12 col-md-12 col-lg-5">
    <div class="box box-primary">
        <div class="box-body">
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#tab_3" data-toggle="tab" aria-expanded="false">Bahan Habis Pakai</a></li>
                    <li class=""><a href="#tab_4" data-toggle="tab" aria-expanded="false">Resep</a></li>
                    <li class=""><a href="#tab_5" data-toggle="tab" aria-expanded="false">Upload Hasil Pemeriksaan</a></li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="tab_3">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-sm-8">
                                        <select id="bahan-option">
                                            <option value="">-- Pilih Bahan --</option>
                                            <?php foreach ($bahan as $key => $value) { ?>
                                                <option value="<?= $value->id ?>"><?= $value->nama ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                    <div class="col-sm-4">
                                        <button type="button" name="button"
                                                class="btn btn-sm btn-block btn-primary"
                                                id="button-add-bahan"><i class="fa fa-plus"></i>
                                            Tambah Bahan
                                        </button>
                                        <input type="hidden" id="abdush-counter2" value="0">
                                    </div>
                                </div>
                                <div class="form-area" style="margin-top:15px;">
                                    <table class="table table-striped table-hover">
                                        <thead>
                                        <tr>
                                            <th>Nama Bahan</th>
                                            <th>Stok</th>
                                            <th>Jml</th>
                                            <th></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="tab_4">
                        <div class="row">
                            <div class="col-md-12">
                                <button type="submit" name="input_resep" value="1" class="btn btn-success btn-block">Input Resep</button>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="tab_5">
                        <div class="row">
                            <div class="col-md-12">
                                <?php $this->load->view('pemeriksaan/partials/tab_upload'); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="box-footer">
            <button type="submit" name="submit" value="1" class="btn btn-primary btn-lg btn-flat pull-right">
                Simpan
            </button>
            <a href="<?= base_url() ?>pemeriksaan/listpemeriksaanPasien" class="btn btn-default btn-lg btn-flat pull-right">
                Batal
            </a>
        </div>
    </div>
</div>