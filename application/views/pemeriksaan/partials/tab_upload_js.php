<script>

    let photo_galleries = <?=json_encode($images)?>;

    $(function () {
        resetPhoto(photo_galleries);

        $(document).on('click', '[data-toggle="lightbox"]', function(event) {
            event.preventDefault();
            $(this).ekkoLightbox({
                alwaysShowClose: true
            });
        });

        $('#input-photo').on('change', function () {
            if (this.files && this.files[0]) {

                const reader = new FileReader();
                reader.onload = function(e) {
                    resizeBase64Img(e.target.result, 1500, 1500).then(r => {
                        $('#new-image-preview').attr('src', r);
                        uploadImage(r);
                        $('#btn-plus').hide();
                        $('#loader').show();
                    });
                }
                reader.readAsDataURL(this.files[0]);
            }
        });

        function uploadImage(base64) {
            $.ajax({
                url: `<?=base_url()?>/Pemeriksaan/upload_image/<?= $pemeriksaan['id'] ?>`,
                type: 'POST',
                data: {
                    img: base64
                },
                dataType: 'JSON',
                success: function (data) {
                    resetPhoto(data.data)
                    Swal.fire({
                        icon: 'success',
                        title: 'Upload berhasil',
                        showConfirmButton: false,
                        timer: 1500
                    })
                },
                error: function (e) {
                    resetPhoto()
                    Swal.fire({
                        icon: 'error',
                        title: 'Upload gagal. Coba lagi',
                    })
                }
            })
        }
    })

    function resetPhoto(images = '') {
        if (images)
            photo_galleries = images;
        else
            images = photo_galleries

        function setHover(div, del) {
            del.hide(200);
            div.on({
                mouseenter: function () {
                    del.show(200);
                },
                mouseleave: function () {
                    del.hide(200);
                }
            });
        }

        function setDelete(del, id, file_name) {
            del.on('click', function () {
                if (confirm('Yakin hapus foto ini?')) {
                    try {
                        $.ajax({
                            url: `<?=base_url()?>/Pemeriksaan/delete_image/<?=$pemeriksaan['id']?>/${id}/${file_name}`,
                            type: 'GET',
                            dataType: 'JSON',
                            success: function (data) {
                                Swal.fire({
                                    icon: 'success',
                                    title: 'Hapus berhasil',
                                    showConfirmButton: false,
                                    timer: 1500
                                })
                                resetPhoto(data.data)
                            },
                            error: function (e) {
                                Swal.fire({
                                    icon: 'error',
                                    title: 'Hapus gagal. Coba lagi',
                                })
                            }
                        })
                    }
                    catch (e) {
                        Swal.fire({
                            icon: 'error',
                            title: 'Hapus gagal. Coba lagi',
                        })
                    }
                }
            })
        }

        const img = `<?php echo base_url() ?>assets/img/white-square.jpg`
        const addBox = `
            <div id="btn-add-photo" class="col-sm-4 add-box" style="font-size: 2rem; position: relative; margin-bottom: 12px">
                <img id="new-image-preview" src="${img}" class="img-fluid img-responsive rounded-corners foto" alt="Tambah gambar">
                <span id="btn-plus" class="fa fa-plus align-middle child-center" style="color: #3C8DBC"></span>
                <div class="loader" id="loader"></div>
            </div>
        `;

        const url = `<?=base_url()?>/assets/img/hasil_pemeriksaan/`
        const ga = $('#img-gallery');
        ga.empty();
        images.forEach((g, i) => {
            ga.append(`
                <div class="col-sm-4 mb-3" id="div-photo-${i}" style=" margin-bottom: 12px">
                    <a href="${url}${g.image}" target="_blank">
                        <img src="${url}${g.image}" class="img-fluid img-responsive rounded-corners foto" alt="Foto ${i + 1}"/>
                    </a>
                    <span class="fa fa-trash btn-delete-photo" id="del-photo-${i}"></span>
                </div>
            `);
            setHover($(`#div-photo-${i}`), $(`#del-photo-${i}`))
            setDelete($(`#del-photo-${i}`), g.id, g.image)
        });

        const is_detail = <?=isset($is_detail) && $is_detail ? 1 : 0?>;
        if (images.length < 12 && !is_detail)
            ga.append(addBox);

        $('#btn-plus').show();
        $('#loader').hide();

        $('#btn-add-photo').on('click', function () {
            $('#input-photo').click();
        });
    }
</script>

<script>

    function calculateAspectRatioFit(srcWidth, srcHeight, maxWidth, maxHeight) {

        let ratio = Math.min(maxWidth / srcWidth, maxHeight / srcHeight);

        return { width: srcWidth*ratio, height: srcHeight*ratio };
    }

    function resizeBase64Img(base64, newWidth, newHeight) {

        return new Promise((resolve, reject) => {
            let img_test = document.createElement("img");
            img_test.onload = function () {
                if (this.width < newWidth || this.height < newHeight) {
                    resolve(base64);
                }
                else {
                    let ratio = calculateAspectRatioFit(this.width, this.height, newWidth, newHeight);
                    let canvas = document.createElement("canvas");
                    canvas.width = ratio.width;
                    canvas.height = ratio.height;

                    let context = canvas.getContext("2d");
                    context.drawImage(img_test, 0, 0, canvas.width, canvas.height);
                    resolve(canvas.toDataURL());
                }
            };
            img_test.src = base64;
        });
    }

</script>