<style>

    .rounded-corners {
        border-radius: 8px;
        -moz-box-shadow: 0 0 4px #ddd;
        -webkit-box-shadow: 0 0 4px #ddd;
        box-shadow: 0 0 4px #ddd;

        /*transition: box-shadow 200s ease-in-out;*/
    }
    .rounded-corners:hover {
        -moz-box-shadow: 0 0 8px #999;
        -webkit-box-shadow: 0 0 8px #999;
        box-shadow: 0 0 8px #999;

        -webkit-transition: box-shadow 200ms ease-in-out;
        -ms-transition: box-shadow 200ms ease-in-out;
        transition: box-shadow 200ms ease-in-out;
    }
    .add-box {
        cursor: pointer;
    }
    .child-center {
        margin: 0;
        position: absolute;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%);
    }

    .btn-delete-photo {
        text-shadow: 1px 1px 2px #aaaaaa;
        color: #eeeeee;
        position: absolute;
        top: 8px;
        right: 24px;
        cursor: pointer;
    }
    .btn-delete-photo:hover {
        color: red;
        -webkit-transition: color 200ms ease-in-out;
        -ms-transition: color 200ms ease-in-out;
        transition: color 200ms ease-in-out;
    }

    .btn-upload-video {
        font-size: 4em;
        position: absolute;
        color: rgba(45, 135, 237, 0.3);
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%);
        cursor: pointer;
    }
    .btn-upload-video:hover {
        color: rgb(45, 135, 237);
        -webkit-transition: color 200ms ease-in-out;
        -ms-transition: color 200ms ease-in-out;
        transition: color 200ms ease-in-out;
    }

    .btn-delete-video {
        text-shadow: 1px 1px 2px #aaaaaa;
        color: #ffffff;
        position: absolute;
        top: 18px;
        right: 24px;
        cursor: pointer;
    }
    .btn-delete-video:hover {
        color: red;
        -webkit-transition: color 200ms ease-in-out;
        -ms-transition: color 200ms ease-in-out;
        transition: color 200ms ease-in-out;
    }

    .hidden-input {
        display: none;
    }

    .foto {
        width: 200px;
        height: 170px;
        object-fit: cover;
    }

    .video-view {
        width: 100%;
        height: 190px;
        object-fit: cover;
    }

    #new-video-preview {
        height: 204px;
        padding-top: 7px !important;
        padding-bottom: 7px !important;
        object-fit: cover;
    }

    /*loading*/
    .loader {
        margin: 0;
        position: absolute;
        top: 37.5%;
        left: 38.5%;
        /*transform: translate(-50%, -50%);*/

        border: 4px solid #f3f3f3;
        border-radius: 50%;
        border-top: 4px solid #3498db;
        width: 40px;
        height: 40px;
        -webkit-animation: spin 2s linear infinite; /* Safari */
        animation: spin 2s linear infinite;
    }

    /* Safari */
    @-webkit-keyframes spin {
        0% { -webkit-transform: rotate(0deg); }
        100% { -webkit-transform: rotate(360deg); }
    }

    @keyframes spin {
        0% { transform: rotate(0deg); }
        100% { transform: rotate(360deg); }
    }
</style>

<input type="file" class="hidden-input" id="input-photo" accept="image/*" style="display: none">
<div class="row row-eq-height" id="img-gallery">
</div>