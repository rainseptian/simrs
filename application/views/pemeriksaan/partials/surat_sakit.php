<div class="box box-success hidden" id="surat-sakit">
    <div class="box-header">
        <h4>Surat Keterangan Sakit</h4>
    </div>
    <div class="box-body">
        <div class="row">
            <div class="col-sm-12">
                <p>Yang bertanda tangan di bawah ini, menerangkan dengan sebenarnya bahwa:</p>
                <table class="table">
                    <tr>
                        <td>Nama Pasien</td>
                        <td>:</td>
                        <td><?php echo $pemeriksaan['nama_pasien'] ?></td>
                    </tr>
                    <address>
                        <tr>
                            <td>Alamat</td>
                            <td>:</td>
                            <td><?php echo $pasien->alamat ?></td>
                        </tr>
                        <tr>
                            <td>Umur</td>
                            <td>:</td>
                            <td><?php echo $pasien->usia ?></td>
                        </tr>
                        <tr>
                            <td>Pekerjaan</td>
                            <td>:</td>
                            <td><?php echo $pasien->pekerjaan ?></td>
                        </tr>
                    </address>
                </table>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <p>
                    Saat ini benar-benar memerlukan istirahat selama <input class="form-control" type="text" name="surat_form[sakit][jumlah_hari]" id="jumlah_hari"> hari,
                    terhitung mulai tanggal <input class="form-control" type="date" name="surat_form[sakit][start_date]" id="start_date" value="<?=date('Y-m-d')?>">
                    s/d <input class="form-control" type="date" name="surat_form[sakit][end_date]" id="end_date">
                </p>
                <p>
                    Dengan demikian keterangan ini dibuat untuk digunakan sebagaimana mestinya.
                </p>
            </div>
        </div>
    </div>
</div>