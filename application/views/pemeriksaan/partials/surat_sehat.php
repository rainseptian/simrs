<div class="box box-success hidden" id="surat-sehat">
    <div class="box-header">
        <h4>Surat Keterangan Sehat</h4>
    </div>
    <div class="box-body">
        <div class="row">
            <div class="col-sm-12">
                <p>Yang bertanda tangan dibawah ini, <?=ucwords($pendaftaran['nama_dokter'])?> selaku dokter pemeriksa di <?=$klinik->nama?>, dengan ini menerangkan</p>
                <table class="table">
                    <tr>
                        <td>Nama</td>
                        <td>:</td>
                        <td><?php echo $pemeriksaan['nama_pasien'] ?></td>
                    </tr>
                    <tr>
                        <td>Tempat tanggal lahir</td>
                        <td>:</td>
                        <td><?php echo $pasien->tempat_lahir ?>, <?php echo DateIndo($pasien->tanggal_lahir) ?></td>
                    </tr>
                    <tr>
                        <td>Jenis kelamin</td>
                        <td>:</td>
                        <td><?php echo $pasien->jk == 'L' ? 'Laki-laki' : 'Perempuan' ?></td>
                    </tr>
                    <tr>
                        <td>Pekerjaan</td>
                        <td>:</td>
                        <td><?php echo $pasien->pekerjaan ?></td>
                    </tr>
                    <tr>
                        <td>Alamat</td>
                        <td>:</td>
                        <td><?php echo $pasien->alamat ?></td>
                    </tr>
                    <tr>
                        <td>Pada pemeriksan tanggal</td>
                        <td>:</td>
                        <td><?php echo DateIndo(date('Y-m-d')) ?></td>
                    </tr>
                    <tr>
                        <td>Dinyatakan dalam keadaan</td>
                        <td>:</td>
                        <td>Sehat</td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <p>
                    Surat keterangan ini dibuat dipergunakan untuk:
                    <input type="text" name="surat_form[sehat][untuk]" class="form-control">
                </p>
                <p>
                    Demikian surat keterangan ini kami buat sebenarnya untuk dapat digunakan sebagaimana mestinya.
                </p>

                <table class="table">
                    <thead>
                    <tr>
                        <th colspan="3">Keterangan</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>Visus mata kanan VOD</td>
                        <td>:</td>
                        <td><input type="text" class="form-control" name="surat_form[sehat][vm_kanan]"></td>
                    </tr>
                    <tr>
                        <td>Visus mata kiri VOS</td>
                        <td>:</td>
                        <td><input type="text" class="form-control" name="surat_form[sehat][vm_kiri]"></td>
                    </tr>
                    <tr>
                        <td>Telinga Kanan</td>
                        <td>:</td>
                        <td><input type="text" class="form-control" name="surat_form[sehat][telinga_kanan]" value="dbn"></td>
                    </tr>
                    <tr>
                        <td>Telinga Kiri</td>
                        <td>:</td>
                        <td><input type="text" class="form-control" name="surat_form[sehat][telinga_kiri]" value="dbn"></td>
                    </tr>
                    <tr>
                        <td>Tes Buta Warna</td>
                        <td>:</td>
                        <td><input type="text" class="form-control" name="surat_form[sehat][tes_buta_warna]" value="Normal"></td>
                    </tr>
                    <tr>
                        <td>Golongan Darah</td>
                        <td>:</td>
                        <td><input type="text" class="form-control" name="surat_form[sehat][goldar]"></td>
                    </tr>
                    <tr>
                        <td>Tinggi Badan</td>
                        <td>:</td>
                        <td><?php echo $pemeriksaan['tb'] ?> cm</td>
                    </tr>
                    <tr>
                        <td>Berat Badan</td>
                        <td>:</td>
                        <td><?php echo $pemeriksaan['bb'] ?> Kg</td>
                    </tr>
                    <tr>
                        <td>Tensi</td>
                        <td>:</td>
                        <td><?php echo $pemeriksaan['td'] ?></td>
                    </tr>
                    <tr>
                        <td>Nadi</td>
                        <td>:</td>
                        <td><?php echo $pemeriksaan['n'] ?>x/mnt</td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>