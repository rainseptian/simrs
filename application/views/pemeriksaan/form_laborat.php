<?php
$hasil_lab = json_decode($pemeriksaan['hasil_lab'], true);
?>

<style>
    .padding-sm {
        padding: 5px !important;
    }
</style>

<br>

<div class="row">
    <div class="box box-success">
        <div class="box-body">
            <div class="col-xs-12">
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                        <li class="active">
                            <a href="#tab_1" data-toggle="tab" class="padding-sm">
                                <span style="font-size: 14px;">Hematologi</span>
                            </a>
                        </li>
                        <li>
                            <a href="#tab_2" data-toggle="tab" class="padding-sm">
                                <span style="font-size: 14px;">DL_Automatic</span>
                            </a>
                        </li>
                        <li>
                            <a href="#tab_3" data-toggle="tab" class="padding-sm">
                                <span style="font-size: 14px;">Faal_Hesmostatis</span>
                            </a>
                        </li>
                        <li>
                            <a href="#tab_4" data-toggle="tab" class="padding-sm">
                                <span style="font-size: 14px;">Fungsi Ginjal</span>
                            </a>
                        </li>
                        <li>
                            <a href="#tab_5" data-toggle="tab" class="padding-sm">
                                <span style="font-size: 14px;">Glukosa Darah</span>
                            </a>
                        </li>
                        <li>
                            <a href="#tab_6" data-toggle="tab" class="padding-sm">
                                <span style="font-size: 14px;">Faal Hati</span>
                            </a>
                        </li>
                        <li>
                            <a href="#tab_7" data-toggle="tab" class="padding-sm">
                                <span style="font-size: 14px;">Lemak Darah</span>
                            </a>
                        </li>
                        <li>
                            <a href="#tab_8" data-toggle="tab" class="padding-sm">
                                <span style="font-size: 14px;">Elektrolit</span>
                            </a>
                        </li>
                        <li>
                            <a href="#tab_9" data-toggle="tab" class="padding-sm">
                                <span style="font-size: 14px;">Imunoserologi</span>
                            </a>
                        </li>
                        <li>
                            <a href="#tab_10" data-toggle="tab" class="padding-sm">
                                <span style="font-size: 14px;">Widal</span>
                            </a>
                        </li>
                        <!-- SEPULUH-->
                        <li>
                            <a href="#tab_11" data-toggle="tab" class="padding-sm">
                                <span style="font-size: 14px;">Urin Lengkap</span>
                            </a>
                        </li>
                        <li>
                            <a href="#tab_12" data-toggle="tab" class="padding-sm">
                                <span style="font-size: 14px;">PPT_Test_Pack</span>
                            </a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab_1">

                            <h4 class="box-title text-center"><strong>Hematologi</strong></h4>

                            <div class="form-group">
                                <label for="inputtext3" class="col-sm-2 control-label">Hemoglobin</label>
                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">
                                    <input type="text" class="form-control" id="hemoglobin" name="hemoglobin" value="<?=isset($hasil_lab['hemoglobin']) ? $hasil_lab['hemoglobin'] : ''?>">
                                    <span class="input-group-addon">P:13.0 - 16.0 | W:12.0 - 14.0</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputtext3" class="col-sm-2 control-label">LED 1jam/2jam</label>
                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">
                                    <input type="text" class="form-control" id="led" name="led" value="<?=isset($hasil_lab['led']) ? $hasil_lab['led'] : ''?>">
                                    <span class="input-group-addon">P < 10 | W < 15</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputtext3" class="col-sm-2 control-label">Leukosit</label>
                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">
                                    <input type="text" class="form-control" id="leukosit" name="leukosit" value="<?=isset($hasil_lab['leukosit']) ? $hasil_lab['leukosit'] : ''?>">
                                    <span class="input-group-addon">5000-10000</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputtext3" class="col-sm-2 control-label">Hitung Jenis</label>
                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">
                                    <input type="text" class="form-control" id="hitung" name="hitung" value="<?=isset($hasil_lab['hitung']) ? $hasil_lab['hitung'] : ''?>">
                                    <span class="input-group-addon"> </span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputtext3" class="col-sm-2 control-label">Eosinophyl</label>
                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">
                                    <input type="text" class="form-control" id="eosinophyl" name="eosinophyl" value="<?=isset($hasil_lab['eosinophyl']) ? $hasil_lab['eosinophyl'] : ''?>">
                                    <span class="input-group-addon">1-3 </span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputtext3" class="col-sm-2 control-label">Basophyl</label>
                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">
                                    <input type="text" class="form-control" id="basophyl" name="basophyl" value="<?=isset($hasil_lab['basophyl']) ? $hasil_lab['basophyl'] : ''?>">
                                    <span class="input-group-addon">0-1</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputtext3" class="col-sm-2 control-label">Stab</label>
                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">
                                    <input type="text" class="form-control" id="stab" name="stab" value="<?=isset($hasil_lab['stab']) ? $hasil_lab['stab'] : ''?>">
                                    <span class="input-group-addon">2-6</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputtext3" class="col-sm-2 control-label">Segment</label>
                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">
                                    <input type="text" class="form-control" id="segment" name="segment" value="<?=isset($hasil_lab['segment']) ? $hasil_lab['segment'] : ''?>">
                                    <span class="input-group-addon">50-70</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputtext3" class="col-sm-2 control-label">Lymposit</label>
                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">
                                    <input type="text" class="form-control" id="lymposit" name="lymposit" value="<?=isset($hasil_lab['lymposit']) ? $hasil_lab['lymposit'] : ''?>">
                                    <span class="input-group-addon">20-40</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputtext3" class="col-sm-2 control-label">Monosit</label>
                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">
                                    <input type="text" class="form-control" id="monosit" name="monosit" value="<?=isset($hasil_lab['monosit']) ? $hasil_lab['monosit'] : ''?>">
                                    <span class="input-group-addon">2-8</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputtext3" class="col-sm-2 control-label">Sel Lainnya</label>
                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">
                                    <input type="text" class="form-control" id="sel_lainnya" name="sel_lainnya" value="<?=isset($hasil_lab['sel_lainnya']) ? $hasil_lab['sel_lainnya'] : ''?>">
                                    <span class="input-group-addon"> </span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputtext3" class="col-sm-2 control-label">Eosinofil</label>
                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">
                                    <input type="text" class="form-control" id="eosinofil" name="eosinofil" value="<?=isset($hasil_lab['eosinofil']) ? $hasil_lab['eosinofil'] : ''?>">
                                    <span class="input-group-addon">50-300</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputtext3" class="col-sm-2 control-label">Erytrosit</label>
                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">
                                    <input type="text" class="form-control" id="eritrosit" name="eritrosit" value="<?=isset($hasil_lab['eritrosit']) ? $hasil_lab['eritrosit'] : ''?>">
                                    <span class="input-group-addon">P:4.5 - 5.5 | W:4.0 - 5.0</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputtext3" class="col-sm-2 control-label">Trombocyt</label>
                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">
                                    <input type="text" class="form-control" id="trombocyt" name="trombocyt" value="<?=isset($hasil_lab['trombocyt']) ? $hasil_lab['trombocyt'] : ''?>">
                                    <span class="input-group-addon">150.000 - 500.000</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputtext3" class="col-sm-2 control-label">Reticulocyt</label>
                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">
                                    <input type="text" class="form-control" id="reticulocyt" name="reticulocyt" value="<?=isset($hasil_lab['reticulocyt']) ? $hasil_lab['reticulocyt'] : ''?>">
                                    <span class="input-group-addon">0.5-1.5</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputtext3" class="col-sm-2 control-label">Hematacrit</label>
                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">
                                    <input type="text" class="form-control" id="hematacrit" name="hematacrit" value="<?=isset($hasil_lab['hematacrit']) ? $hasil_lab['hematacrit'] : ''?>">
                                    <span class="input-group-addon">P:40-48% | W:37-43% | A:31-47%</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputtext3" class="col-sm-2 control-label">MCV</label>
                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">
                                    <input type="text" class="form-control" id="mcv" name="mcv" value="<?=isset($hasil_lab['mcv']) ? $hasil_lab['mcv'] : ''?>">
                                    <span class="input-group-addon">82-92</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputtext3" class="col-sm-2 control-label">MCH</label>
                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">
                                    <input type="text" class="form-control" id="mch" name="mch" value="<?=isset($hasil_lab['mch']) ? $hasil_lab['mch'] : ''?>">
                                    <span class="input-group-addon">27-31</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputtext3" class="col-sm-2 control-label">MCHC</label>
                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">
                                    <input type="text" class="form-control" id="mchc" name="mchc" value="<?=isset($hasil_lab['mchc']) ? $hasil_lab['mchc'] : ''?>">
                                    <span class="input-group-addon">32-36</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputtext3" class="col-sm-2 control-label">Waktu Pendarahan</label>
                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">
                                    <input type="text" class="form-control" id="waktu_pendarahan" name="waktu_pendarahan" value="<?=isset($hasil_lab['waktu_pendarahan']) ? $hasil_lab['waktu_pendarahan'] : ''?>">
                                    <span class="input-group-addon">1-3</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputtext3" class="col-sm-2 control-label">Waktu Pembekuan</label>
                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">
                                    <input type="text" class="form-control" id="waktu_pembekuan" name="waktu_pembekuan" value="<?=isset($hasil_lab['waktu_pembekuan']) ? $hasil_lab['waktu_pembekuan'] : ''?>">
                                    <span class="input-group-addon">10-15</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputtext3" class="col-sm-2 control-label">Waktu Prothombin</label>
                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">
                                    <input type="text" class="form-control" id="waktu_prothombin" name="waktu_prothombin" value="<?=isset($hasil_lab['waktu_prothombin']) ? $hasil_lab['waktu_prothombin'] : ''?>">
                                    <span class="input-group-addon">11-14</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputtext3" class="col-sm-2 control-label">Waktu Rekalsifikasi</label>
                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">
                                    <input type="text" class="form-control" id="waktu_rkalsifikasi" name="waktu_rekalsifikasi" value="<?=isset($hasil_lab['waktu_rekalsifikasi']) ? $hasil_lab['waktu_rekalsifikasi'] : ''?>">
                                    <span class="input-group-addon">100-250</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputtext3" class="col-sm-2 control-label">PTT</label>
                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">
                                    <input type="text" class="form-control" id="ptt" name="ptt" value="<?=isset($hasil_lab['ptt']) ? $hasil_lab['ptt'] : ''?>">
                                    <span class="input-group-addon">30-40</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputtext3" class="col-sm-2 control-label">Thrombotes Owren</label>
                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">
                                    <input type="text" class="form-control" id="thrombotes_owren" name="thrombotes_owren" value="<?=isset($hasil_lab['thrombotes_owren']) ? $hasil_lab['thrombotes_owren'] : ''?>">
                                    <span class="input-group-addon">70-100</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputtext3" class="col-sm-2 control-label">Fibrinogen</label>
                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">
                                    <input type="text" class="form-control" id="fibrinogen" name="fibrinogen" value="<?=isset($hasil_lab['fibrinogen']) ? $hasil_lab['fibrinogen'] : ''?>">
                                    <span class="input-group-addon">200-400</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputtext3" class="col-sm-2 control-label">Retraksi Bekuan</label>
                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">
                                    <input type="text" class="form-control" id="retraksi_bekuan" name="retraksi_bekuan" value="<?=isset($hasil_lab['retraksi_bekuan']) ? $hasil_lab['retraksi_bekuan'] : ''?>">
                                    <span class="input-group-addon">40-60</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputtext3" class="col-sm-2 control-label">Retraksi Osmotik</label>
                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">
                                    <input type="text" class="form-control" id="retraksi_osmotik" name="retraksi_osmotik" value="<?=isset($hasil_lab['retraksi_osmotik']) ? $hasil_lab['retraksi_osmotik'] : ''?>">
                                    <span class="input-group-addon">0.40-0.30</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputtext3" class="col-sm-2 control-label">Malaria</label>
                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">
                                    <input type="text" class="form-control" id="malaria" name="malaria" value="<?=isset($hasil_lab['malaria']) ? $hasil_lab['malaria'] : ''?>">
                                    <span class="input-group-addon"> </span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputtext3" class="col-sm-2 control-label">Plasmodium Falcifarum</label>
                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">
                                    <input type="text" class="form-control" id="plasmodium_falcifarum" name="plasmodium_falcifarum" value="<?=isset($hasil_lab['plasmodium_falcifarum']) ? $hasil_lab['plasmodium_falcifarum'] : ''?>">
                                    <span class="input-group-addon">Negatif</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputtext3" class="col-sm-2 control-label">Plasmodium Vivax</label>
                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">
                                    <input type="text" class="form-control" id="plasmodium_vivax" name="plasmodium_vivax" value="<?=isset($hasil_lab['plasmodium_vivax']) ? $hasil_lab['plasmodium_vivax'] : ''?>">
                                    <span class="input-group-addon">Negatif</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputtext3" class="col-sm-2 control-label">Prc Pembendungan</label>
                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">
                                    <input type="text" class="form-control" id="prc_pembendungan" name="prc_pembendungan" value="<?=isset($hasil_lab['prc_pembendungan']) ? $hasil_lab['prc_pembendungan'] : ''?>">
                                    <span class="input-group-addon">Pethecia < 10</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputtext3" class="col-sm-2 control-label">Darah Lengkap</label>
                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">
                                    <input type="text" class="form-control" id="darah_lengkap" name="darah_lengkap" value="<?=isset($hasil_lab['darah_lengkap']) ? $hasil_lab['darah_lengkap'] : ''?>">
                                    <span class="input-group-addon"> </span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputtext3" class="col-sm-2 control-label">RDW-CV</label>
                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">
                                    <input type="text" class="form-control" id="rdw_cv" name="rdw_cv" value="<?=isset($hasil_lab['rdw_cv']) ? $hasil_lab['rdw_cv'] : ''?>">
                                    <span class="input-group-addon">11.5-14.5</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputtext3" class="col-sm-2 control-label">RDW-SD</label>
                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">
                                    <input type="text" class="form-control" id="rdw_sd" name="rdw_sd" value="<?=isset($hasil_lab['rdw_sd']) ? $hasil_lab['rdw_sd'] : ''?>">
                                    <span class="input-group-addon">35-56</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputtext3" class="col-sm-2 control-label">MPV</label>
                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">
                                    <input type="text" class="form-control" id="mpv" name="mpv" value="<?=isset($hasil_lab['mpv']) ? $hasil_lab['mpv'] : ''?>">
                                    <span class="input-group-addon">7-11</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputtext3" class="col-sm-2 control-label">PDW</label>
                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">
                                    <input type="text" class="form-control" id="pdw" name="pdw" value="<?=isset($hasil_lab['pdw']) ? $hasil_lab['pdw'] : ''?>">
                                    <span class="input-group-addon">15-17</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputtext3" class="col-sm-2 control-label">PCT</label>
                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">
                                    <input type="text" class="form-control" id="pct" name="pct" value="<?=isset($hasil_lab['pct']) ? $hasil_lab['pct'] : ''?>">
                                    <span class="input-group-addon">0.108-0.282</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputtext3" class="col-sm-2 control-label">Limfosit</label>
                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">
                                    <input type="text" class="form-control" id="limfosit" name="limfosit" value="<?=isset($hasil_lab['limfosit']) ? $hasil_lab['limfosit'] : ''?>">
                                    <span class="input-group-addon"> </span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputtext3" class="col-sm-2 control-label">Analisa HB (HPLC)</label>
                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">
                                    <input type="text" class="form-control" id="analisa_hb" name="analisa_hb" value="<?=isset($hasil_lab['analisa_hb']) ? $hasil_lab['analisa_hb'] : ''?>">
                                    <span class="input-group-addon"> </span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputtext3" class="col-sm-2 control-label">Analisa HB (HPLC)</label>
                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">
                                    <input type="text" class="form-control" id="analisa_hb" name="analisa_hb" value="<?=isset($hasil_lab['analisa_hb']) ? $hasil_lab['analisa_hb'] : ''?>">
                                    <span class="input-group-addon"> </span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputtext3" class="col-sm-2 control-label">HbA2</label>
                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">
                                    <input type="text" class="form-control" id="hba2" name="hba2" value="<?=isset($hasil_lab['hba2']) ? $hasil_lab['hba2'] : ''?>">
                                    <span class="input-group-addon">2.0-3.6%</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputtext3" class="col-sm-2 control-label">HbF</label>
                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">
                                    <input type="text" class="form-control" id="hbf" name="hbf" value="<?=isset($hasil_lab['hbf']) ? $hasil_lab['hbf'] : ''?>">
                                    <span class="input-group-addon">< 1%</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputtext3" class="col-sm-2 control-label">Ferritin</label>
                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">
                                    <input type="text" class="form-control" id="ferritin" name="ferritin" value="<?=isset($hasil_lab['ferritin']) ? $hasil_lab['ferritin'] : ''?>">
                                    <span class="input-group-addon">13-150</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputtext3" class="col-sm-2 control-label">TIBC</label>
                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">
                                    <input type="text" class="form-control" id="tibc" name="tibc" value="<?=isset($hasil_lab['tibc']) ? $hasil_lab['tibc'] : ''?>">
                                    <span class="input-group-addon">260-389</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputtext3" class="col-sm-2 control-label">PT</label>
                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">
                                    <input type="text" class="form-control" id="pt" name="pt" value="<?=isset($hasil_lab['pt']) ? $hasil_lab['pt'] : ''?>">
                                    <span class="input-group-addon">10.70-14.30</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputtext3" class="col-sm-2 control-label">APTT</label>
                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">
                                    <input type="text" class="form-control" id="aptt" name="aptt" value="<?=isset($hasil_lab['aptt']) ? $hasil_lab['aptt'] : ''?>">
                                    <span class="input-group-addon">21.00-36.50</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputtext3" class="col-sm-2 control-label">INR</label>
                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">
                                    <input type="text" class="form-control" id="inr" name="inr" value="<?=isset($hasil_lab['inr']) ? $hasil_lab['inr'] : ''?>">
                                    <span class="input-group-addon">0.8-1.2</span>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab_2">

                            <h4 class="box-title text-center"><strong>DL_Automatic</strong></h4>

                            <div class="form-group">
                                <label for="inputtext3" class="col-sm-2 control-label">LEUCO_WBC</label>
                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">
                                    <input type="text" class="form-control" id="leuco_wbc" name="leuco_wbc" value="<?=isset($hasil_lab['leuco_wbc']) ? $hasil_lab['leuco_wbc'] : ''?>">
                                    <span class="input-group-addon">L:4.7-10.3  P:4.3-11.3</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputtext3" class="col-sm-2 control-label">ERY_RBC</label>
                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">
                                    <input type="text" class="form-control" id="ery_rbc" name="ery_rbc" value="<?=isset($hasil_lab['ery_rbc']) ? $hasil_lab['ery_rbc'] : ''?>">
                                    <span class="input-group-addon">L:4.33-5.95  P:3.9-4.5</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputtext3" class="col-sm-2 control-label">HB_HGB</label>
                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">
                                    <input type="text" class="form-control" id="hb_hgb" name="hb_hgb" value="<?=isset($hasil_lab['hb_hgb']) ? $hasil_lab['hb_hgb'] : ''?>">
                                    <span class="input-group-addon">L:13.4-17.7P:11.4-15.1</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputtext3" class="col-sm-2 control-label">PCV_HCT</label>
                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">
                                    <input type="text" class="form-control" id="pct_hcv" name="pct_hcv" value="<?=isset($hasil_lab['pct_hcv']) ? $hasil_lab['pct_hcv'] : ''?>">
                                    <span class="input-group-addon">L:40-47  P:38-42</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputtext3" class="col-sm-2 control-label">MCV</label>
                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">
                                    <input type="text" class="form-control" id="mcv" name="mcv" value="<?=isset($hasil_lab['mcv']) ? $hasil_lab['mcv'] : ''?>">
                                    <span class="input-group-addon">80-93</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputtext3" class="col-sm-2 control-label">MCH</label>
                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">
                                    <input type="text" class="form-control" id="mch" name="mch" value="<?=isset($hasil_lab['mch']) ? $hasil_lab['mch'] : ''?>">
                                    <span class="input-group-addon">27-31</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputtext3" class="col-sm-2 control-label">MCHC</label>
                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">
                                    <input type="text" class="form-control" id="mchc" name="mchc" value="<?=isset($hasil_lab['mchc']) ? $hasil_lab['mchc'] : ''?>">
                                    <span class="input-group-addon">32-36</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputtext3" class="col-sm-2 control-label">Trombo_PLT</label>
                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">
                                    <input type="text" class="form-control" id="trombo_plt" name="trombo_plt" value="<?=isset($hasil_lab['trombo_plt']) ? $hasil_lab['trombo_plt'] : ''?>">
                                    <span class="input-group-addon">x1000/ul (150-350)</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputtext3" class="col-sm-2 control-label">DIFF</label>
                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">
                                    <input type="text" class="form-control" id="diff" name="diff" value="<?=isset($hasil_lab['diff']) ? $hasil_lab['diff'] : ''?>">
                                    <span class="input-group-addon"> </span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputtext3" class="col-sm-2 control-label">Ensinofil</label>
                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">
                                    <input type="text" class="form-control" id="ensinofil" name="ensinofil" value="<?=isset($hasil_lab['ensinofil']) ? $hasil_lab['ensinofil'] : ''?>">
                                    <span class="input-group-addon"> 1-2% </span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputtext3" class="col-sm-2 control-label">Basofil</label>
                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">
                                    <input type="text" class="form-control" id="basofil" name="basofil" value="<?=isset($hasil_lab['basofil']) ? $hasil_lab['basofil'] : ''?>">
                                    <span class="input-group-addon">0-1%</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputtext3" class="col-sm-2 control-label">Stab</label>
                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">
                                    <input type="text" class="form-control" id="stab" name="stab" value="<?=isset($hasil_lab['stab']) ? $hasil_lab['stab'] : ''?>">
                                    <span class="input-group-addon">3-5%</span>
                                </div>
							</div>
							<div class="form-group">
                                <label for="inputtext3" class="col-sm-2 control-label">Segment</label>
                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">
                                    <input type="text" class="form-control" id="segment" name="stab" value="<?=isset($hasil_lab['segment']) ? $hasil_lab['segment'] : ''?>">
                                    <span class="input-group-addon">54-62% </span>
                                </div>
                            </div>
							<div class="form-group">
                                <label for="inputtext3" class="col-sm-2 control-label">Lymfosit</label>
                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">
                                    <input type="text" class="form-control" id="lymfosit" name="lymfosit" value="<?=isset($hasil_lab['lymfosit']) ? $hasil_lab['lymfosit'] : ''?>">
                                    <span class="input-group-addon">25-33% </span>
                                </div>
							 </div>
							<div class="form-group">
                                <label for="inputtext3" class="col-sm-2 control-label">Monosit</label>
                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">
                                    <input type="text" class="form-control" id="monosit" name="monosit" value="<?=isset($hasil_lab['monosit']) ? $hasil_lab['monosit'] : ''?>">
                                    <span class="input-group-addon">3-7% </span>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab_3">

                            <h4 class="box-title text-center"><strong>FAAL HEMOSTATIS</strong></h4>

                            <div class="form-group">
                                <label for="inputtext3" class="col-sm-3 control-label">PPT</label>
                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">
                                    <input type="text" class="form-control" id="ppt" name="ppt" value="<?=isset($hasil_lab['ppt']) ? $hasil_lab['ppt'] : ''?>">
                                    <span class="input-group-addon">C= 7.8	+/- 2 detikdari C</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputtext3" class="col-sm-3 control-label">APIT</label>
                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">
                                    <input type="text" class="form-control" id="apit" name="apit" value="<?=isset($hasil_lab['apit']) ? $hasil_lab['apit'] : ''?>">
                                    <span class="input-group-addon"> C= 31.8 +/- 7 detikdari C</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputtext3" class="col-sm-3 control-label">BT</label>
                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">
                                    <input type="text" class="form-control" id="bt" name="bt" value="<?=isset($hasil_lab['bt']) ? $hasil_lab['bt'] : ''?>">
                                    <span class="input-group-addon">2-3 menit</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputtext3" class="col-sm-3 control-label">CT</label>
                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">
                                    <input type="text" class="form-control" id="ct" name="ct" value="<?=isset($hasil_lab['ct']) ? $hasil_lab['ct'] : ''?>">
                                    <span class="input-group-addon">5-10 menit</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputtext3" class="col-sm-3 control-label">Laju Endap Darah</label>
                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">
                                    <input type="text" class="form-control" id="laju_endap_darah" name="laju_endap_darah" value="<?=isset($hasil_lab['laju_endap_darah']) ? $hasil_lab['albumin'] : ''?>">
                                    <span class="input-group-addon">L: 0-15;P :0-20mm/jam</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputtext3" class="col-sm-3 control-label">Gol Darah</label>
                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">
                                    <input type="text" class="form-control" id="gol_darah" name="gol_darah" value="<?=isset($hasil_lab['gol_darah']) ? $hasil_lab['gol_darah'] : ''?>">
                                    <span class="input-group-addon"> </span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputtext3" class="col-sm-3 control-label">Gol Darah Rhesus</label>
                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">
                                    <input type="text" class="form-control" id="gol_darah_rhesus" name="gol_darah_rhesus" value="<?=isset($hasil_lab['gol_darah_rhesus']) ? $hasil_lab['gol_darah_rhesus'] : ''?>">
                                    <span class="input-group-addon"> </span>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab_4">

                            <h4 class="box-title text-center"><strong>FUNGSI GINJAL</strong></h4>

                            <div class="form-group">
                                <label for="inputtext3" class="col-sm-3 control-label">Bun</label>
                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">
                                    <input type="text" class="form-control" id="bun" name="bun" value="<?=isset($hasil_lab['bun']) ? $hasil_lab['bun'] : ''?>">
                                    <span class="input-group-addon">mg/dl < 22 </span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputtext3" class="col-sm-3 control-label">Creatinine</label>
                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">
                                    <input type="text" class="form-control" id="creatinene" name="creatinene" value="<?=isset($hasil_lab['creatinene']) ? $hasil_lab['creatinene'] : ''?>">
                                    <span class="input-group-addon"> mg/dl L: <1.52 ; P: <1.19 </span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputtext3" class="col-sm-3 control-label">Uric Acid</label>
                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">
                                    <input type="text" class="form-control" id="uric_acid" name="uric_acid" value="<?=isset($hasil_lab['uric_acid']) ? $hasil_lab['uric_acid'] : ''?>">
                                    <span class="input-group-addon"> mg/dl L: 3.0 – 7.0 ; P: 2.3 – 6.0  </span>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab_5">

                            <h4 class="box-title text-center"><strong>GLUKOSA DARAH</strong></h4>

                            <div class="form-group">
                                <label for="inputtext3" class="col-sm-3 control-label">Glukosa Acak</label>
                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">
                                    <input type="text" class="form-control" id="glukosa_acak" name="glukosa_acak" value="<?=isset($hasil_lab['glukosa_acak']) ? $hasil_lab['glukosa_acak'] : ''?>">
                                    <span class="input-group-addon"> < 160 mg / dl </span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputtext3" class="col-sm-3 control-label">Glukosa Puasa</label>
                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">
                                    <input type="text" class="form-control" id="glukosa_puasa" name="glukosa_puasa" value="<?=isset($hasil_lab['glukosa_puasa']) ? $hasil_lab['glukosa_puasa'] : ''?>">
                                    <span class="input-group-addon"> <120 mg / dl</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputtext3" class="col-sm-3 control-label">Glukosa 2jp</label>
                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">
                                    <input type="text" class="form-control" id="glukosa_2jp" name="glukosa_2jp" value="<?=isset($hasil_lab['glukosa_2jp']) ? $hasil_lab['gglukosa_2jp'] : ''?>">
                                    <span class="input-group-addon"> < 160 mg / dl </span>
                                </div>
						    </div>
							<div class="form-group">
                                <label for="inputtext3" class="col-sm-3 control-label"> HbA-1c</label>
                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">
                                    <input type="text" class="form-control" id="hbA-1c" name="hbA-1c" value="<?=isset($hasil_lab['hbA-1c']) ? $hasil_lab['hbA-1c'] : ''?>">
                                    <span class="input-group-addon"> Normal : < 5.7 </span>
                                </div>
							</div>
							<div class="form-group">
                                <label for="inputtext3" class="col-sm-3 control-label"> HbA-1c＿a</label>
                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">
                                    <input type="text" class="form-control" id="hbA-1c_a" name="hbA-1c_a" value="<?=isset($hasil_lab['hbA-1c_a']) ? $hasil_lab['hbA-1c_a'] : ''?>">
                                    <span class="input-group-addon"> Normal : < 5.7 </span>
                                </div>
							</div>
							<div class="form-group">
                                <label for="inputtext3" class="col-sm-3 control-label"> HbA-1c＿b</label>
                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">
                                    <input type="text" class="form-control" id="hbA-1c_b" name="hbA-1c_b" value="<?=isset($hasil_lab['hbA-1c_b']) ? $hasil_lab['hbA-1c_b'] : ''?>">
                                    <span class="input-group-addon"> Pre Diabetes : 5.7 – 6.4 </span>
                                 </div>
							</div>
							<div class="form-group">
                                <label for="inputtext3" class="col-sm-3 control-label"> HbA-1c＿c</label>
                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">
                                    <input type="text" class="form-control" id="hbA-1c_c" name="hbA-1c_c" value="<?=isset($hasil_lab['hbA-1c_c']) ? $hasil_lab['hbA-1c_c'] : ''?>">
                                    <span class="input-group-addon"> Diabetes : >/= 6.5 </span>
                                </div>
							</div>
							<div class="form-group">
                                <label for="inputtext3" class="col-sm-3 control-label"> HbA-1c＿d</label>
                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">
                                    <input type="text" class="form-control" id="hbA-1c_d" name="hbA-1c_d" value="<?=isset($hasil_lab['hbA-1c_d']) ? $hasil_lab['hbA-1c_d'] : ''?>">
                                    <span class="input-group-addon"> Target Terapi : < 7.0 </span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputtext3" class="col-sm-3 control-label">Reduksi</label>
                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">
                                    <input type="text" class="form-control" id="reduksi_2" name="reduksi_2" value="<?=isset($hasil_lab['reduksi_2']) ? $hasil_lab['reduksi_2'] : ''?>">
                                    <span class="input-group-addon">Negatif</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputtext3" class="col-sm-3 control-label">Gula Darah Sewaktu</label>
                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">
                                    <input type="text" class="form-control" id="gula_darah_sewaktu" name="gula_darah_sewaktu" value="<?=isset($hasil_lab['gula_darah_sewaktu']) ? $hasil_lab['gula_darah_sewaktu'] : ''?>">
                                    <span class="input-group-addon"> <=180 </span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputtext3" class="col-sm-3 control-label">GTT:Puasa</label>
                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">
                                    <input type="text" class="form-control" id="gtt_puasa" name="gtt_puasa" value="<?=isset($hasil_lab['gtt_puasa']) ? $hasil_lab['gtt_puasa'] : ''?>">
                                    <span class="input-group-addon">70-100</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputtext3" class="col-sm-3 control-label">GTT:1/2jam</label>
                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">
                                    <input type="text" class="form-control" id="gtt_1/2jam" name="gtt_1/2jam" value="<?=isset($hasil_lab['gtt_1/2jam']) ? $hasil_lab['gtt_1/2jam'] : ''?>">
                                    <span class="input-group-addon">110-170</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputtext3" class="col-sm-3 control-label">GTT:1jam</label>
                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">
                                    <input type="text" class="form-control" id="gtt_1jam" name="gtt_1jam" value="<?=isset($hasil_lab['gtt_1jam']) ? $hasil_lab['gtt_1jam'] : ''?>">
                                    <span class="input-group-addon">120-170</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputtext3" class="col-sm-3 control-label">GTT:1 1/2jam</label>
                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">
                                    <input type="text" class="form-control" id="gtt_11/2jam" name="gtt_11/2jam" value="<?=isset($hasil_lab['gtt_11/2jam']) ? $hasil_lab['gtt_11/2jam'] : ''?>">
                                    <span class="input-group-addon">100-140</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputtext3" class="col-sm-3 control-label">GTT:2jam</label>
                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">
                                    <input type="text" class="form-control" id="gtt_2jam" name="gtt_2jam" value="<?=isset($hasil_lab['gtt_2jam']) ? $hasil_lab['gtt_2jam'] : ''?>">
                                    <span class="input-group-addon">20-120</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputtext3" class="col-sm-3 control-label">Hb A1-C</label>
                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">
                                    <input type="text" class="form-control" id="hb_A-1c" name="hb_A-1c" value="<?=isset($hasil_lab['hb_A-1c']) ? $hasil_lab['hb_A-1c'] : ''?>">
                                    <span class="input-group-addon">4.2-7.0</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputtext3" class="col-sm-3 control-label">II</label>
                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">
                                    <input type="text" class="form-control" id="ii" name="ii" value="<?=isset($hasil_lab['ii']) ? $hasil_lab['ii'] : ''?>">
                                    <span class="input-group-addon">4-7</span>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab_6">

                            <h4 class="box-title text-center"><strong>FAAL HATI</strong></h4>

                            <div class="form-group">
                                <label for="inputtext3" class="col-sm-3 control-label">SGOT</label>
                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">
                                    <input type="text" class="form-control" id="sgot" name="sgot" value="<?=isset($hasil_lab['sgot']) ? $hasil_lab['sgot'] : ''?>">
                                    <span class="input-group-addon">L: <37 ; P: <31u/l</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputtext3" class="col-sm-3 control-label">SGPT</label>
                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">
                                    <input type="text" class="form-control" id="sgpt" name="sgpt" value="<?=isset($hasil_lab['sgpt']) ? $hasil_lab['sgpt'] : ''?>">
                                    <span class="input-group-addon">L: <40 ; P: <31 u/l </span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputtext3" class="col-sm-3 control-label">S Albumin</label>
                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">
                                    <input type="text" class="form-control" id="s_albumin" name="s_albumin" value="<?=isset($hasil_lab['s_albumin']) ? $hasil_lab['s_albumin'] : ''?>">
                                    <span class="input-group-addon">3.5-5.0 gr %</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputtext3" class="col-sm-3 control-label">Bilirubin</label>
                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">
                                    <input type="text" class="form-control" id="bilirubin" name="bilirubin" value="<?=isset($hasil_lab['bilirubin']) ? $hasil_lab['bilirubin'] : ''?>">
                                    <span class="input-group-addon"> </span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputtext3" class="col-sm-3 control-label">Direct</label>
                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">
                                    <input type="text" class="form-control" id="direct" name="direct" value="<?=isset($hasil_lab['direct']) ? $hasil_lab['direct'] : ''?>">
                                    <span class="input-group-addon">< 0.25 mg/dl</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputtext3" class="col-sm-3 control-label">Indirect</label>
                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">
                                    <input type="text" class="form-control" id="indirect" name="indirect" value="<?=isset($hasil_lab['indirect']) ? $hasil_lab['indirect'] : ''?>">
                                    <span class="input-group-addon"> < 0.75 mg / dl </span>
                                </div>
                            </div>
							<div class="form-group">
                                <label for="inputtext3" class="col-sm-3 control-label">Total</label>
                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">
                                    <input type="text" class="form-control" id="total" name="total" value="<?=isset($hasil_lab['total']) ? $hasil_lab['total'] : ''?>">
                                    <span class="input-group-addon"> < 1.00 mg / dl </span>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab_7">

                            <h4 class="box-title text-center"><strong>LEMAK DARAH</strong></h4>

                            <div class="form-group">
                                <label for="inputtext3" class="col-sm-3 control-label">Cholesterol</label>
                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">
                                    <input type="text" class="form-control" id="cholesterol" name="cholesterol" value="<?=isset($hasil_lab['cholesterol']) ? $hasil_lab['cholesterol'] : ''?>">
                                    <span class="input-group-addon"> < 220 mg / dl</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputtext3" class="col-sm-3 control-label">Trigliserida</label>
                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">
                                    <input type="text" class="form-control" id="trigliserida" name="trigliserida" value="<?=isset($hasil_lab['trigliserida']) ? $hasil_lab['trigliserida'] : ''?>">
                                    <span class="input-group-addon"> < 150 mg / dl</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputtext3" class="col-sm-3 control-label">HDL Chol</label>
                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">
                                    <input type="text" class="form-control" id="hdl_chol" name="hdl_chol" value="<?=isset($hasil_lab['hdl_chol']) ? $hasil_lab['hdl_chol'] : ''?>">
                                    <span class="input-group-addon">L:> 55 ; P:> 60 mg/dl</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputtext3" class="col-sm-3 control-label">LDL Chol</label>
                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">
                                    <input type="text" class="form-control" id="ldl_chol" name="ldl_chol" value="<?=isset($hasil_lab['ldl_chol']) ? $hasil_lab['ldl_chol'] : ''?>">
                                    <span class="input-group-addon"> < 150 mg / dl </span>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab_8">

                            <h4 class="box-title text-center"><strong> ELEKTROLIT</strong></h4>

                            <div class="form-group">
                                <label for="inputtext3" class="col-sm-3 control-label">Na</label>
                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">
                                    <input type="text" class="form-control" id="na" name="pengecatan_gram" value="<?=isset($hasil_lab['na']) ? $hasil_lab['na'] : ''?>">
                                    <span class="input-group-addon"> 135-145 mEg/L </span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputtext3" class="col-sm-3 control-label">K</label>
                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">
                                    <input type="text" class="form-control" id="k" name="k" value="<?=isset($hasil_lab['k']) ? $hasil_lab['k'] : ''?>">
                                    <span class="input-group-addon"> 3.5- 5.5 mEg/L </span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputtext3" class="col-sm-3 control-label">Cl</label>
                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">
                                    <input type="text" class="form-control" id="cl" name="cl" value="<?=isset($hasil_lab['cl']) ? $hasil_lab['cl'] : ''?>">
                                    <span class="input-group-addon"> 90-110 mEg/L </span>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab_9">

                            <h4 class="box-title text-center"><strong>IMMUNOSEROLOGI</strong></h4>

                            <div class="form-group">
                                <label for="inputtext3" class="col-sm-3 control-label">HBsAg RPHA </label>
                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">
                                    <input type="text" class="form-control" id="hBsAg_rPHA" name="hBsAg_rPHA" value="<?=isset($hasil_lab['hBsAg_rPHA']) ? $hasil_lab['hBsAg_rPHA'] : ''?>">
                                    <span class="input-group-addon"> Non Reaktif </span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputtext3" class="col-sm-3 control-label">VDRL</label>
                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">
                                    <input type="text" class="form-control" id="vdrl" name="vdrl" value="<?=isset($hasil_lab['vdrl']) ? $hasil_lab['vdrl'] : ''?>">
                                    <span class="input-group-addon"> Non Reaktif </span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputtext3" class="col-sm-3 control-label"> IgG Anti DHF</label>
                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">
                                    <input type="text" class="form-control" id="igG_anti_dHF" name="igG_anti_dHF" value="<?=isset($hasil_lab['igG_anti_dHF']) ? $hasil_lab['igG_anti_dHF'] : ''?>">
                                    <span class="input-group-addon">Negatif </span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputtext3" class="col-sm-3 control-label"> IgM Anti DHF</label>
                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">
                                    <input type="text" class="form-control" id="igm_anti_dHF" name="igm_anti_dHF" value="<?=isset($hasil_lab['igm_anti_dHF']) ? $hasil_lab['igm_anti_dHF'] : ''?>">
                                    <span class="input-group-addon">Negatif </span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputtext3" class="col-sm-3 control-label">Anti HIV</label>
                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">
                                    <input type="text" class="form-control" id="anti_hiv" name="anti_hiv" value="<?=isset($hasil_lab['anti_hiv']) ? $hasil_lab['anti_hiv'] : ''?>">
                                    <span class="input-group-addon">Non Reaktif</span>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab_10">

                            <h4 class="box-title text-center"><strong>WIDAL</strong></h4>

                            <div class="form-group">
                                <label for="inputtext3" class="col-sm-3 control-label">Type O</label>
                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">
                                    <input type="text" class="form-control" id="type_o" name="type_o" value="<?=isset($hasil_lab['type_o']) ? $hasil_lab['type_o'] : ''?>">
                                    <span class="input-group-addon">Negatif: < 8</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputtext3" class="col-sm-3 control-label">Type H</label>
                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">
                                    <input type="text" class="form-control" id="type_h" name="type_h" value="<?=isset($hasil_lab['type_h']) ? $hasil_lab['type_h'] : ''?>">
                                    <span class="input-group-addon">Negatif: < 8</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputtext3" class="col-sm-3 control-label">Type A</label>
                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">
                                    <input type="text" class="form-control" id="type_a" name="type_a" value="<?=isset($hasil_lab['type_a']) ? $hasil_lab['type_a'] : ''?>">
                                    <span class="input-group-addon">Negatif: < 8</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputtext3" class="col-sm-3 control-label">Type B</label>
                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">
                                    <input type="text" class="form-control" id="type_b" name="type_b" value="<?=isset($hasil_lab['type_b']) ? $hasil_lab['type_b'] : ''?>">
                                    <span class="input-group-addon">Negatif: < 8</span>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab_11">

                            <h4 class="box-title text-center"><strong>URIN LENGKAP</strong></h4>

                            <div class="form-group">
                                <label for="inputtext3" class="col-sm-3 control-label">Albumin</label>
                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">
                                    <input type="text" class="form-control" id="albumin" name="albumin" value="<?=isset($hasil_lab['albumin']) ? $hasil_lab['albumin'] : ''?>">
                                    <span class="input-group-addon"> Negatif </span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputtext3" class="col-sm-3 control-label">Reduksi</label>
                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">
                                    <input type="text" class="form-control" id="Reduksi" name="Reduksi" value="<?=isset($hasil_lab['Reduksi']) ? $hasil_lab['fReduksi'] : ''?>">
                                    <span class="input-group-addon"> Negatif </span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputtext3" class="col-sm-3 control-label">Urobilin</label>
                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">
                                    <input type="text" class="form-control" id="urobilin" name="urobilin" value="<?=isset($hasil_lab['urobilin']) ? $hasil_lab['urobilin'] : ''?>">
                                    <span class="input-group-addon"> Negatif </span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputtext3" class="col-sm-3 control-label">Bilirubin</label>
                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">
                                    <input type="text" class="form-control" id="birilubin" name="bilirubin" value="<?=isset($hasil_lab['birilubin']) ? $hasil_lab['birilubin'] : ''?>">
                                    <span class="input-group-addon"> Negtif </span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputtext3" class="col-sm-3 control-label">PH</label>
                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">
                                    <input type="text" class="form-control" id="ph" name="ph" value="<?=isset($hasil_lab['ph']) ? $hasil_lab['ph'] : ''?>">
                                    <span class="input-group-addon"> 4.5 - 7.5 </span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputtext3" class="col-sm-3 control-label">Berat Jenis</label>
                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">
                                    <input type="text" class="form-control" id="berat_jenis" name="berat_jenis" value="<?=isset($hasil_lab['berat_jenis']) ? $hasil_lab['berat_jenis'] : ''?>">
                                    <span class="input-group-addon"> 1.010 - 1.025 </span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputtext3" class="col-sm-3 control-label">Blood</label>
                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">
                                    <input type="text" class="form-control" id="blood" name="blood" value="<?=isset($hasil_lab['blood']) ? $hasil_lab['blood'] : ''?>">
                                    <span class="input-group-addon">Negatif</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputtext3" class="col-sm-3 control-label">Leukosit</label>
                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">
                                    <input type="text" class="form-control" id="leukosit" name="leukosit" value="<?=isset($hasil_lab['leukosit']) ? $hasil_lab['leukosit'] : ''?>">
                                    <span class="input-group-addon">Negatif</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputtext3" class="col-sm-3 control-label">Nitrit</label>
                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">
                                    <input type="text" class="form-control" id="nitrit" name="nitrit" value="<?=isset($hasil_lab['nitrit']) ? $hasil_lab['nitrit'] : ''?>">
                                    <span class="input-group-addon">Negatif</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputtext3" class="col-sm-3 control-label">Keton</label>
                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">
                                    <input type="text" class="form-control" id="keton" name="keton" value="<?=isset($hasil_lab['keton']) ? $hasil_lab['keton'] : ''?>">
                                    <span class="input-group-addon">Negatif</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputtext3" class="col-sm-3 control-label">SEDIMENT</label>
                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">
                                    <input type="text" class="form-control" id="sediment" name="sediment" value="<?=isset($hasil_lab['sediment']) ? $hasil_lab['sediment'] : ''?>">
                                    <span class="input-group-addon"> </span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputtext3" class="col-sm-3 control-label">Eritrocyt</label>
                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">
                                    <input type="text" class="form-control" id="eritrocyt" name="eritrocyt" value="<?=isset($hasil_lab['eritrocyt']) ? $hasil_lab['eritrocyt'] : ''?>">
                                    <span class="input-group-addon"> 0 - 1 /lp </span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputtext3" class="col-sm-3 control-label">Leukosit</label>
                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">
                                    <input type="text" class="form-control" id="leukosit" name="leukosit" value="<?=isset($hasil_lab['leukosit']) ? $hasil_lab['leukosit'] : ''?>">
                                    <span class="input-group-addon">0 - 1 /lp</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputtext3" class="col-sm-3 control-label">Epitel</label>
                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">
                                    <input type="text" class="form-control" id="epitel" name="epitel" value="<?=isset($hasil_lab['epitel']) ? $hasil_lab['epitel'] : ''?>">
                                    <span class="input-group-addon"> </span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputtext3" class="col-sm-3 control-label">Silinder</label>
                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">
                                    <input type="text" class="form-control" id="silinder" name="silinder" value="<?=isset($hasil_lab['silinder']) ? $hasil_lab['silinder'] : ''?>">
                                    <span class="input-group-addon"> Negatif </span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputtext3" class="col-sm-3 control-label">Kristal</label>
                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">
                                    <input type="text" class="form-control" id="kristal" name="kristal" value="<?=isset($hasil_lab['kristal']) ? $hasil_lab['kristal'] : ''?>">
                                    <span class="input-group-addon"> Negatif </span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputtext3" class="col-sm-3 control-label">Lain - Lain</label>
                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">
                                    <input type="text" class="form-control" id="lain_lain" name="lain_lain" value="<?=isset($hasil_lab['lain_lain']) ? $hasil_lab['lain_lain'] : ''?>">
                                    <span class="input-group-addon"> Negatif </span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputtext3" class="col-sm-3 control-label">Mikroalbumin</label>
                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">
                                    <input type="text" class="form-control" id="mikroalbumin" name="mikroalbumin" value="<?=isset($hasil_lab['mikroalbumin']) ? $hasil_lab['mikroalbumin'] : ''?>">
                                    <span class="input-group-addon"> < 20 mg /L </span>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab_12">

                            <h4 class="box-title text-center"><strong>PPT Test Pack</strong></h4>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!--<style>-->
<!--    .padding-sm {-->
<!--        padding: 5px !important;-->
<!--    }-->
<!--</style>-->
<!---->
<!--<br>-->
<!---->
<!--<div class="row">-->
<!--    <div class="box box-success">-->
<!--        <div class="box-body">-->
<!--            <div class="col-xs-12">-->
<!--                <div class="nav-tabs-custom">-->
<!--                    <ul class="nav nav-tabs">-->
<!--                        <li class="active">-->
<!--                            <a href="#tab_1" data-toggle="tab" class="padding-sm">-->
<!--                                <span style="font-size: 14px;">Hematologi</span>-->
<!--                            </a>-->
<!--                        </li>-->
<!--                        <li>-->
<!--                            <a href="#tab_2" data-toggle="tab" class="padding-sm">-->
<!--                                <span style="font-size: 14px;">Fungsi Ginjal</span>-->
<!--                            </a>-->
<!--                        </li>-->
<!--                        <li>-->
<!--                            <a href="#tab_3" data-toggle="tab" class="padding-sm">-->
<!--                                <span style="font-size: 14px;">Fungsi Hati</span>-->
<!--                            </a>-->
<!--                        </li>-->
<!--                        <li>-->
<!--                            <a href="#tab_4" data-toggle="tab" class="padding-sm">-->
<!--                                <span style="font-size: 14px;">Fungsi Jantung</span>-->
<!--                            </a>-->
<!--                        </li>-->
<!--                        <li>-->
<!--                            <a href="#tab_5" data-toggle="tab" class="padding-sm">-->
<!--                                <span style="font-size: 14px;">Gula Darah</span>-->
<!--                            </a>-->
<!--                        </li>-->
<!--                        <li>-->
<!--                            <a href="#tab_6" data-toggle="tab" class="padding-sm">-->
<!--                                <span style="font-size: 14px;">Profil Lemak</span>-->
<!--                            </a>-->
<!--                        </li>-->
<!--                        <li>-->
<!--                            <a href="#tab_7" data-toggle="tab" class="padding-sm">-->
<!--                                <span style="font-size: 14px;">Elektrolit dan Gas Darah</span>-->
<!--                            </a>-->
<!--                        </li>-->
<!--                        <li>-->
<!--                            <a href="#tab_8" data-toggle="tab" class="padding-sm">-->
<!--                                <span style="font-size: 14px;">Mikrobiologi</span>-->
<!--                            </a>-->
<!--                        </li>-->
<!--                        <li>-->
<!--                            <a href="#tab_9" data-toggle="tab" class="padding-sm">-->
<!--                                <span style="font-size: 14px;">Seriomonologi</span>-->
<!--                            </a>-->
<!--                        </li>-->
<!--                        <li>-->
<!--                            <a href="#tab_10" data-toggle="tab" class="padding-sm">-->
<!--                                <span style="font-size: 14px;">CRP</span>-->
<!--                            </a>-->
<!--                        </li>-->
<!--                        <!-- SEPULUH-->-->
<!--                        <li>-->
<!--                            <a href="#tab_11" data-toggle="tab" class="padding-sm">-->
<!--                                <span style="font-size: 14px;">Urinalisa</span>-->
<!--                            </a>-->
<!--                        </li>-->
<!--                        <li>-->
<!--                            <a href="#tab_12" data-toggle="tab" class="padding-sm">-->
<!--                                <span style="font-size: 14px;">Feses</span>-->
<!--                            </a>-->
<!--                        </li>-->
<!--                        <li>-->
<!--                            <a href="#tab_13" data-toggle="tab" class="padding-sm">-->
<!--                                <span style="font-size: 14px;">Spermatozoa</span>-->
<!--                            </a>-->
<!--                        </li>-->
<!--                        <li>-->
<!--                            <a href="#tab_14" data-toggle="tab" class="padding-sm">-->
<!--                                <span style="font-size: 14px;">Hormon</span>-->
<!--                            </a>-->
<!--                        </li>-->
<!--                        <li>-->
<!--                            <a href="#tab_15" data-toggle="tab" class="padding-sm">-->
<!--                                <span style="font-size: 14px;">Petanda Tumor</span>-->
<!--                            </a>-->
<!--                        </li>-->
<!--                        <li>-->
<!--                            <a href="#tab_16" data-toggle="tab" class="padding-sm">-->
<!--                                <span style="font-size: 14px;">Administrasi</span>-->
<!--                            </a>-->
<!--                        </li>-->
<!--                        <li>-->
<!--                            <a href="#tab_17" data-toggle="tab" class="padding-sm">-->
<!--                                <span style="font-size: 14px;">Bahan</span>-->
<!--                            </a>-->
<!--                        </li>-->
<!--                        <li>-->
<!--                            <a href="#tab_18" data-toggle="tab" class="padding-sm">-->
<!--                                <span style="font-size: 14px;">Narkoba</span>-->
<!--                            </a>-->
<!--                        </li>-->
<!--                        <li>-->
<!--                            <a href="#tab_19" data-toggle="tab" class="padding-sm">-->
<!--                                <span style="font-size: 14px;">Kehamilan</span>-->
<!--                            </a>-->
<!--                        </li>-->
<!--                        <li>-->
<!--                            <a href="#tab_20" data-toggle="tab" class="padding-sm">-->
<!--                                <span style="font-size: 14px;">Golongan Darah</span>-->
<!--                            </a>-->
<!--                        </li>-->
<!--                    </ul>-->
<!--                    <div class="tab-content">-->
<!--                        <div class="tab-pane active" id="tab_1">-->
<!---->
<!--                            <h4 class="box-title text-center"><strong>Hematologi</strong></h4>-->
<!---->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-2 control-label">Hemoglobin</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="hemoglobin" name="hemoglobin" value="">-->
<!--                                    <span class="input-group-addon">P:13.0 - 16.0 | W:12.0 - 14.0</span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-2 control-label">LED 1jam/2jam</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="led" name="led" value="">-->
<!--                                    <span class="input-group-addon">P < 10 | W < 15</span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-2 control-label">Leukosit</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="leukosit" name="leukosit" value="">-->
<!--                                    <span class="input-group-addon">5000-10000</span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-2 control-label">Hitung Jenis</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="hitung" name="hitung" value="">-->
<!--                                    <span class="input-group-addon"> </span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-2 control-label">Eosinophyl</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="eosinophyl" name="eosinophyl" value="">-->
<!--                                    <span class="input-group-addon">1-3 </span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-2 control-label">Basophyl</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="basophyl" name="basophyl" value="">-->
<!--                                    <span class="input-group-addon">0-1</span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-2 control-label">Stab</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="stab" name="stab" value="">-->
<!--                                    <span class="input-group-addon">2-6</span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-2 control-label">Segment</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="segment" name="segment" value="">-->
<!--                                    <span class="input-group-addon">50-70</span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-2 control-label">Lymposit</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="lymposit" name="lymposit" value="">-->
<!--                                    <span class="input-group-addon">20-40</span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-2 control-label">Monosit</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="monosit" name="monosit" value="">-->
<!--                                    <span class="input-group-addon">2-8</span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-2 control-label">Sel Lainnya</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="sel_lainnya" name="sel_lainnya" value="">-->
<!--                                    <span class="input-group-addon"> </span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-2 control-label">Eosinofil</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="eosinofil" name="eosinofil" value="">-->
<!--                                    <span class="input-group-addon">50-300</span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-2 control-label">Erytrosit</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="eritrosit" name="eritrosit" value="">-->
<!--                                    <span class="input-group-addon">P:4.5 - 5.5 | W:4.0 - 5.0</span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-2 control-label">Trombocyt</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="trombocyt" name="trombocyt" value="">-->
<!--                                    <span class="input-group-addon">150.000 - 500.000</span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-2 control-label">Reticulocyt</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="reticulocyt" name="reticulocyt" value="">-->
<!--                                    <span class="input-group-addon">0.5-1.5</span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-2 control-label">Hematacrit</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="hematacrit" name="hematacrit" value="">-->
<!--                                    <span class="input-group-addon">P:40-48% | W:37-43% | A:31-47%</span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-2 control-label">MCV</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="mcv" name="mcv" value="">-->
<!--                                    <span class="input-group-addon">82-92</span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-2 control-label">MCH</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="mch" name="mch" value="">-->
<!--                                    <span class="input-group-addon">27-31</span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-2 control-label">MCHC</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="mchc" name="mchc" value="">-->
<!--                                    <span class="input-group-addon">32-36</span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-2 control-label">Waktu Pendarahan</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="waktu_pendarahan" name="waktu_pendarahan" value="">-->
<!--                                    <span class="input-group-addon">1-3</span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-2 control-label">Waktu Pembekuan</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="waktu_pembekuan" name="waktu_pembekuan" value="">-->
<!--                                    <span class="input-group-addon">10-15</span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-2 control-label">Waktu Prothombin</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="waktu_prothombin" name="waktu_prothombin" value="">-->
<!--                                    <span class="input-group-addon">11-14</span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-2 control-label">Waktu Rekalsifikasi</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="waktu_rkalsifikasi" name="waktu_rekalsifikasi" value="">-->
<!--                                    <span class="input-group-addon">100-250</span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-2 control-label">PTT</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="ptt" name="ptt" value="">-->
<!--                                    <span class="input-group-addon">30-40</span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-2 control-label">Thrombotes Owren</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="thrombotes_owren" name="thrombotes_owren" value="">-->
<!--                                    <span class="input-group-addon">70-100</span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-2 control-label">Fibrinogen</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="fibrinogen" name="fibrinogen" value="">-->
<!--                                    <span class="input-group-addon">200-400</span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-2 control-label">Retraksi Bekuan</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="retraksi_bekuan" name="retraksi_bekuan" value="">-->
<!--                                    <span class="input-group-addon">40-60</span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-2 control-label">Retraksi Osmotik</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="retraksi_osmotik" name="retraksi_osmotik" value="">-->
<!--                                    <span class="input-group-addon">0.40-0.30</span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-2 control-label">Malaria</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="malaria" name="malaria" value="">-->
<!--                                    <span class="input-group-addon"> </span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-2 control-label">Plasmodium Falcifarum</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="plasmodium_falcifarum" name="plasmodium_falcifarum" value="">-->
<!--                                    <span class="input-group-addon">Negatif</span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-2 control-label">Plasmodium Vivax</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="plasmodium_vivax" name="plasmodium_vivax" value="">-->
<!--                                    <span class="input-group-addon">Negatif</span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-2 control-label">Prc Pembendungan</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="prc_pembendungan" name="prc_pembendungan" value="">-->
<!--                                    <span class="input-group-addon">Pethecia < 10</span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-2 control-label">Darah Lengkap</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="darah_lengkap" name="darah_lengkap" value="">-->
<!--                                    <span class="input-group-addon"> </span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-2 control-label">RDW-CV</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="rdw_cv" name="rdw_cv" value="">-->
<!--                                    <span class="input-group-addon">11.5-14.5</span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-2 control-label">RDW-SD</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="rdw_sd" name="rdw_sd" value="">-->
<!--                                    <span class="input-group-addon">35-56</span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-2 control-label">MPV</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="mpv" name="mpv" value="">-->
<!--                                    <span class="input-group-addon">7-11</span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-2 control-label">PDW</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="pdw" name="pdw" value="">-->
<!--                                    <span class="input-group-addon">15-17</span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-2 control-label">PCT</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="pct" name="pct" value="">-->
<!--                                    <span class="input-group-addon">0.108-0.282</span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-2 control-label">Limfosit</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="limfosit" name="limfosit" value="">-->
<!--                                    <span class="input-group-addon"> </span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-2 control-label">Analisa HB (HPLC)</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="analisa_hb" name="analisa_hb" value="">-->
<!--                                    <span class="input-group-addon"> </span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-2 control-label">Analisa HB (HPLC)</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="analisa_hb" name="analisa_hb" value="">-->
<!--                                    <span class="input-group-addon"> </span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-2 control-label">HbA2</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="hba2" name="hba2" value="">-->
<!--                                    <span class="input-group-addon">2.0-3.6%</span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-2 control-label">HbF</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="hbf" name="hbf" value="">-->
<!--                                    <span class="input-group-addon">< 1%</span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-2 control-label">Ferritin</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="ferritin" name="ferritin" value="">-->
<!--                                    <span class="input-group-addon">13-150</span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-2 control-label">TIBC</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="tibc" name="tibc" value="">-->
<!--                                    <span class="input-group-addon">260-389</span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-2 control-label">PT</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="pt" name="pt" value="">-->
<!--                                    <span class="input-group-addon">10.70-14.30</span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-2 control-label">APTT</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="aptt" name="aptt" value="">-->
<!--                                    <span class="input-group-addon">21.00-36.50</span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-2 control-label">INR</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="inr" name="inr" value="">-->
<!--                                    <span class="input-group-addon">0.8-1.2</span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                        <div class="tab-pane" id="tab_2">-->
<!---->
<!--                            <h4 class="box-title text-center"><strong>FUNGSI GINJAL</strong></h4>-->
<!---->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-2 control-label">Ureum Darah</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="ureum_darah" name="ureum_darah" value="">-->
<!--                                    <span class="input-group-addon">10-50</span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-2 control-label">Ureum Urin</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="ureum_urin" name="ureum_urin" value="">-->
<!--                                    <span class="input-group-addon">20-35</span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-2 control-label">Creatine Darah</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="creatine_darah" name="creatin_darah" value="">-->
<!--                                    <span class="input-group-addon">0.7-1.7</span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-2 control-label">Creatine Urine</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="creatine_urine" name="creatin_urine" value="">-->
<!--                                    <span class="input-group-addon">1-3</span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-2 control-label">Creatine Clearence</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="creatine_clearence" name="creatin_clearence" value="">-->
<!--                                    <span class="input-group-addon">117+20</span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-2 control-label">Urea Clearence</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="urea_clearence" name="urea_clearence" value="">-->
<!--                                    <span class="input-group-addon">70-100</span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-2 control-label">Alkali Reserve</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="alkali_reserve" name="alkali_reserve" value="">-->
<!--                                    <span class="input-group-addon">24-31</span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-2 control-label">Fosfat Anorganik</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="fosfat_anorganik" name="fosfat_anorganik" value="">-->
<!--                                    <span class="input-group-addon">2-4 (dewasa) | 5-6 (anak) </span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-2 control-label">Uric Acid</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="uric_acid" name="uric_acid" value="">-->
<!--                                    <span class="input-group-addon">P:3.4-7.0 | W:2.4-5.7</span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-2 control-label">Serum Iron</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="serum_iron" name="serum_iron" value="">-->
<!--                                    <span class="input-group-addon">P:53-167 | W:49-151</span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-2 control-label">TIBC</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="tibc" name="tibc" value="">-->
<!--                                    <span class="input-group-addon">280-400</span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                        <div class="tab-pane" id="tab_3">-->
<!---->
<!--                            <h4 class="box-title text-center"><strong>FUNGSI HATI</strong></h4>-->
<!---->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">Bilirudin Total</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="bilirudin_total" name="bilirudin_total" value="">-->
<!--                                    <span class="input-group-addon">0.3-1.0</span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">Bilirudin Direk</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="bilirudin_direk" name="bilirudin_direk" value="">-->
<!--                                    <span class="input-group-addon">sd 0.4</span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">Bilirudin Indirek</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="bilirudin_indirek" name="bilirudin_indirek" value="">-->
<!--                                    <span class="input-group-addon">sd 0.6</span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">Protein Total</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="protein_total" name="protein_total" value="">-->
<!--                                    <span class="input-group-addon">6.8 - 8.7</span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">Albumin</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="albumin" name="albumin" value="">-->
<!--                                    <span class="input-group-addon">3.8 - 5.1</span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">SGOT</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="sgot" name="sgot" value="">-->
<!--                                    <span class="input-group-addon">P:s/d 37 | W:s/d 31</span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">SGPT</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="sgpt" name="sgpt" value="">-->
<!--                                    <span class="input-group-addon">P:s/d 40 | W:s/d 31</span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">Gamma GT</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="gamma_gt" name="gamma_gt" value="">-->
<!--                                    <span class="input-group-addon">11-61</span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">Askali Fosfatase</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="askali_fosfatase" name="askali_fosfatase" value="">-->
<!--                                    <span class="input-group-addon">34-114</span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">Chollinesterase (CHE)</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="chollinesterase" name="chollinesterase" value="">-->
<!--                                    <span class="input-group-addon">4620-11500</span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                        <div class="tab-pane" id="tab_4">-->
<!---->
<!--                            <h4 class="box-title text-center"><strong>FUNGSI JANTUNG</strong></h4>-->
<!---->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">CK</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="ck" name="ck" value="">-->
<!--                                    <span class="input-group-addon">W:24-170</span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">LDH</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="ldh" name="ldh" value="">-->
<!--                                    <span class="input-group-addon"> <480 </span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">Ck-M8</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="ck_m8" name="ck_m8" value="">-->
<!--                                    <span class="input-group-addon"> <25 </span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">Alpha HBDH</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="alpha_hbdh" name="alpha_hbdh" value="">-->
<!--                                    <span class="input-group-addon">65-165</span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">Globulin</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="globulin" name="globulin" value="">-->
<!--                                    <span class="input-group-addon">1.5-3.6</span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                        <div class="tab-pane" id="tab_5">-->
<!---->
<!--                            <h4 class="box-title text-center"><strong>GULA DARAH</strong></h4>-->
<!---->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">Gula Darah Puasa</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="gula_darah_puasa" name="gula_darah_puasa" value="">-->
<!--                                    <span class="input-group-addon">70-100</span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">Reduksi</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="reduksi" name="reduksi" value="">-->
<!--                                    <span class="input-group-addon">Negatif</span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">Gula Darah 2 jam PP</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="gula_darah_2jam" name="gula_darah_2jam" value="">-->
<!--                                    <span class="input-group-addon"> <=140 </span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">Reduksi</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="reduksi_2" name="reduksi_2" value="">-->
<!--                                    <span class="input-group-addon">Negatif</span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">Gula Darah Sewaktu</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="gula_darah_sewaktu" name="gula_darah_sewaktu" value="">-->
<!--                                    <span class="input-group-addon"> <=180 </span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">GTT:Puasa</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="gtt_puasa" name="gtt_puasa" value="">-->
<!--                                    <span class="input-group-addon">70-100</span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">GTT:1/2jam</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="gtt_1/2jam" name="gtt_1/2jam" value="">-->
<!--                                    <span class="input-group-addon">110-170</span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">GTT:1jam</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="gtt_1jam" name="gtt_1jam" value="">-->
<!--                                    <span class="input-group-addon">120-170</span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">GTT:1 1/2jam</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="gtt_11/2jam" name="gtt_11/2jam" value="">-->
<!--                                    <span class="input-group-addon">100-140</span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">GTT:2jam</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="gtt_2jam" name="gtt_2jam" value="">-->
<!--                                    <span class="input-group-addon">20-120</span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">Hb A1-C</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="hb_A-1c" name="hb_A-1c" value="">-->
<!--                                    <span class="input-group-addon">4.2-7.0</span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">II</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="ii" name="ii" value="">-->
<!--                                    <span class="input-group-addon">4-7</span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                        <div class="tab-pane" id="tab_6">-->
<!---->
<!--                            <h4 class="box-title text-center"><strong>PROFIL LEMAK</strong></h4>-->
<!---->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">Cholesterol Total</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="cholesterol_total" name="cholesterol_total" value="">-->
<!--                                    <span class="input-group-addon">150-200</span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">HDL Cholesterol</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="hdl_cholesterol" name="hdl_cholesterol" value="">-->
<!--                                    <span class="input-group-addon">P:35-55 | W:45-65</span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">LDL Cholesterol</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="ldl_cholesterol" name="ldl_cholesterol" value="">-->
<!--                                    <span class="input-group-addon">100-130</span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">Triglycerida</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="triglycerida" name="triglycerida" value="">-->
<!--                                    <span class="input-group-addon">40-155</span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">Lipid Total</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="lipid_total" name="lipid_total" value="">-->
<!--                                    <span class="input-group-addon">600-1000</span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">Cholesterol LDL Direk</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="cholesterol_ldl_direk" name="cholesterol_ldl_direk" value="">-->
<!--                                    <span class="input-group-addon"> <140 </span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                        <div class="tab-pane" id="tab_7">-->
<!---->
<!--                            <h4 class="box-title text-center"><strong>ELEKTROLIT dan GAS DARAH</strong></h4>-->
<!---->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">Natrium</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="natrium" name="natrium" value="">-->
<!--                                    <span class="input-group-addon">135-147</span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">Kalium</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="kalium" name="kalium" value="">-->
<!--                                    <span class="input-group-addon">3.5-5.5</span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">Chlorida</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="chlorida" name="chlorida" value="">-->
<!--                                    <span class="input-group-addon">96-106</span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">Calsium</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="calsium" name="calsium" value="">-->
<!--                                    <span class="input-group-addon">8.1-10.4</span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">Magnesium</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="magnesium" name="magnesium" value="">-->
<!--                                    <span class="input-group-addon">1.58 - 2.55</span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                        <div class="tab-pane" id="tab_8">-->
<!---->
<!--                            <h4 class="box-title text-center"><strong> MIKROBIOLOGI</strong></h4>-->
<!---->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">Pengecatan Gram</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="pengecatan_gram" name="pengecatan_gram" value="">-->
<!--                                    <span class="input-group-addon"> </span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">BTA</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="bta" name="bta" value="">-->
<!--                                    <span class="input-group-addon"> </span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">Mikroskopik Gonorhe</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="mikroskopik_gonore" name="mikroskopik_gonore" value="">-->
<!--                                    <span class="input-group-addon"> </span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">Trikomonas</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="trikomonas" name="trikomonas" value="">-->
<!--                                    <span class="input-group-addon"> </span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">Jamur</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="jamur" name="jamur" value="">-->
<!--                                    <span class="input-group-addon"> </span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">Kultur dan Sensitifitas Tes</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="kultur_sensitivitas" name="kultur_sensitivitas" value="">-->
<!--                                    <span class="input-group-addon"> </span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">Batang Gram-</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="batang_gram-" name="batang_gram-" value="">-->
<!--                                    <span class="input-group-addon"> </span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">Batang Gram+</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="batang_gram+" name="batang_gram+" value="">-->
<!--                                    <span class="input-group-addon"> </span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">Coccus Gram-</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="coccus_gram-" name="coccus_gram-" value="">-->
<!--                                    <span class="input-group-addon"> </span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">Coccus Gram+</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="coccus_gram+" name="coccus_gram+" value="">-->
<!--                                    <span class="input-group-addon"> </span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">Trichomonas</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="trichomonas" name="trichomonas" value="">-->
<!--                                    <span class="input-group-addon"> </span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">Mikroskopik Candida</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="mikroskopik_candida" name="mikroskopik_candida" value="">-->
<!--                                    <span class="input-group-addon"> </span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                        <div class="tab-pane" id="tab_9">-->
<!---->
<!--                            <h4 class="box-title text-center"><strong>SEROIMONOLOGI</strong></h4>-->
<!---->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">Widal</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="widal" name="widal" value="">-->
<!--                                    <span class="input-group-addon"> </span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">Salmonela Typhi O</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="salmonela_typhi_O" name="salmonela_typhi_O" value="">-->
<!--                                    <span class="input-group-addon">Negatif - </span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">Salmonela Typhi H</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="salmonela_typhi_h" name="salmonela_typhi_h" value="">-->
<!--                                    <span class="input-group-addon">Negatif - </span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">Salmonela Paratyphi A-H</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="salmonela_paratyphi_a_h" name="salmonela_paratyphi_a_h"-->
<!--                                           value="">-->
<!--                                    <span class="input-group-addon">Negatif - </span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">Salmonela Paratyphi AO</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="salmonela_paratyphi_ao" name="salmonela_paratyphi_ao" value="">-->
<!--                                    <span class="input-group-addon">Negatif - </span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">Salmonela Paratyphi BO</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="salmonela_paratyphi_bo" name="salmonela_paratyphi_bo" value="">-->
<!--                                    <span class="input-group-addon">Negatif - </span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">Salmonela Paratyphi CO</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="salmonela_paratyphi_co" name="salmonela_paratyphi_co" value="">-->
<!--                                    <span class="input-group-addon">Negatif - </span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">Salmonela Paratyphi BH</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="salmonela_paratyphi_bh" name="salmonela_paratyphi_bh" value="">-->
<!--                                    <span class="input-group-addon">Negatif - </span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">Salmonela Paratyphi CH</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="salmonela_paratyphi_ch" name="salmonela_paratyphi_ch" value="">-->
<!--                                    <span class="input-group-addon">Negatif - </span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">HBsAg</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="hbsag" name="hbsag" value="">-->
<!--                                    <span class="input-group-addon">0.13</span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">HIV</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="hiv" name="hiv" value="">-->
<!--                                    <span class="input-group-addon"> </span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">TPHA</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="tpha" name="tpha" value="">-->
<!--                                    <span class="input-group-addon"> </span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">Rhematoid Factor</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="rhematoid_factor" name="hbsag" value="">-->
<!--                                    <span class="input-group-addon"> </span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">Dengue Ig G</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="dengue_ig_g" name="dengue_ig_g" value="">-->
<!--                                    <span class="input-group-addon"> </span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">Dengue Ig M</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="dengue_ig_m" name="dengue_ig_m" value="">-->
<!--                                    <span class="input-group-addon"> </span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">Anti HBsAg</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="anti_hbsag" name="anti_hbsag" value="">-->
<!--                                    <span class="input-group-addon">Negatif < 8 | Positif > 12</span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">Anti-HBc Total</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="antihbc_total" name="antihbc_total" value="">-->
<!--                                    <span class="input-group-addon">Positif < 1.0 | Negatif => 1.40</span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">HBc</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="hbc" name="hbc" value="">-->
<!--                                    <span class="input-group-addon"> </span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">Anti TB Ig M</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="anti_tb_ig_m" name="anti_tb_ig_m" value="">-->
<!--                                    <span class="input-group-addon">Negatif</span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">Anti TB Ig G</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="anti_tb_ig_g" name="anti_tb_ig_g" value="">-->
<!--                                    <span class="input-group-addon">Negatif</span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">HCV</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="hcv" name="hcv" value="">-->
<!--                                    <span class="input-group-addon"> </span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">Anti HEV Ig M</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="anti_hev_ig_m" name="anti_hev_ig_m" value="">-->
<!--                                    <span class="input-group-addon"> </span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">Anti HEV Ig G</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="anti_hev_ig_g" name="anti_hev_ig_g" value="">-->
<!--                                    <span class="input-group-addon"> </span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">HBeAg</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="hbeag" name="hbeag" value="">-->
<!--                                    <span class="input-group-addon">Negativ < 0.10 | Positif => 0.10</span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">Anti HBe</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="anti_hbe" name="anti_hbe" value="">-->
<!--                                    <span class="input-group-addon"> </span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">VDRL</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="vdrl" name="vdrl" value="">-->
<!--                                    <span class="input-group-addon">non reaktif</span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">ASTO</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="asto" name="asto" value="">-->
<!--                                    <span class="input-group-addon"> </span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                        <div class="tab-pane" id="tab_10">-->
<!---->
<!--                            <h4 class="box-title text-center"><strong>CRP</strong></h4>-->
<!---->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">Titer Reumatoid Factor</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="titer_reumatoid_factor" name="titer_reumatoid_factor" value="">-->
<!--                                    <span class="input-group-addon">Negatif: < 8</span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">Anti HAV IgM</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="anti_hav_igm" name="anti_hav_igm" value="">-->
<!--                                    <span class="input-group-addon">Negatif < 0.4 | Positif =>0.5</span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">Anti HCV</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="anti_hcv" name="anti_hcv" value="">-->
<!--                                    <span class="input-group-addon"> </span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">Toxoplasma Ig A</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="toxoplasma_ig_a" name="toxoplasma_ig_a" value="">-->
<!--                                    <span class="input-group-addon"> </span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">Toxoplasma Ig G</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="toxoplasma_ig_g" name="toxoplasma_ig_g" value="">-->
<!--                                    <span class="input-group-addon">Negatif < 4 | Positif => 8 </span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">Toxoplasma Ig G</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="toxoplasma_ig_g" name="toxoplasma_ig_g" value="">-->
<!--                                    <span class="input-group-addon">Negatif < 4 | Positif => 8 </span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">Toxoplasma Ig M</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="toxoplasma_ig_m" name="toxoplasma_ig_m" value="">-->
<!--                                    <span class="input-group-addon">Negatif < 0.55 | Positif => 0.65</span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">Rubella Ig G</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="rubella_ig_g" name="rubella_ig_g" value="">-->
<!--                                    <span class="input-group-addon">Negatif < 10 | Positif => 15</span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">Rubella Ig M</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="rubella_ig_m" name="rubella_ig_m" value="">-->
<!--                                    <span class="input-group-addon">Negatif < 0.80 | Positif => 1.20</span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">Anti CMV Ig G</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="anti_cmv_ig_g" name="anti_cmv_ig_g" value="">-->
<!--                                    <span class="input-group-addon">Negatif < 4 | Positif => 6</span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">Anti CMV Ig M</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="anti_cmv_ig_m" name="anti_cmv_ig_m" value="">-->
<!--                                    <span class="input-group-addon">Negatif < 0.7 | Positif => 0.9</span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">Anti HSV2 Ig G</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="anti_hsv2_ig_g" name="anti_hsv2_ig_g" value="">-->
<!--                                    <span class="input-group-addon">Negatif < 0.8 | Positif => 1.1</span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">Anti HSV2 Ig M</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="anti_hsv2_ig_m" name="anti_hsv2_ig_m" value="">-->
<!--                                    <span class="input-group-addon">Negatif < 0.8 | Positif => 1.1</span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">TB ICT</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="tb_ict" name="tb_ict" value="">-->
<!--                                    <span class="input-group-addon">Negatif</span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">Tes Mantoux</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="tes_mantaoux" name="tes_mantoux" value="">-->
<!--                                    <span class="input-group-addon">Negatif</span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">Dengue NS1</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="dengue_ns1" name="dengue_ns1" value="">-->
<!--                                    <span class="input-group-addon">Negatif</span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">Anti HBsAg</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="anti_hbsag" name="anti_hbsag" value="">-->
<!--                                    <span class="input-group-addon">Negatif</span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">Chikungunya IgM</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="chikungunya_igm" name="chinkungunya_igm" value="">-->
<!--                                    <span class="input-group-addon">Negatif</span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">Salmonella IgG</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="salmonella_igg" name="salmonella_igg" value="">-->
<!--                                    <span class="input-group-addon">Negatif</span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">Salmonella IgM</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="salmonella_igm" name="salmonella_igm" value="">-->
<!--                                    <span class="input-group-addon">Negatif</span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">Serum Iron NS1</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="serum_iron" name="serum_iron" value="">-->
<!--                                    <span class="input-group-addon">62-173</span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">CA 125</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="ca_125" name="ca_125" value="">-->
<!--                                    <span class="input-group-addon"> < 35 </span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">Leptospira_IgM</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="leptospora_igm" name="leptospora_igm" value="">-->
<!--                                    <span class="input-group-addon">Negatif</span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">TPHA</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="tpha" name="tpha" value="">-->
<!--                                    <span class="input-group-addon">Non Reaktif</span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">HBsAg</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="hbsag" name="hbsag" value="">-->
<!--                                    <span class="input-group-addon">Negatif</span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">IgM Anti Salmonella Typhi</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="igm_anti_salmonella_typhi" name="igm_anti_salmonella_typhi"-->
<!--                                           value="">-->
<!--                                    <span class="input-group-addon">Negatif <= 2 | Borderline:3</span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">Anti Hbs Titer</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="anti_hbs_titer" name="anti_hbs_titer" value="">-->
<!--                                    <span class="input-group-addon">Negatif < 10 | Positif => 10</span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                        <div class="tab-pane" id="tab_11">-->
<!---->
<!--                            <h4 class="box-title text-center"><strong>URINALISA</strong></h4>-->
<!---->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">Urin Rutin</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="urin_rutin" name="urin_rutin" value="">-->
<!--                                    <span class="input-group-addon"> </span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">Fisis Warna</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="fisis_warna" name="fisis_warna" value="">-->
<!--                                    <span class="input-group-addon"> </span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">Kejernihan</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="kejernihan" name="kejernihan" value="">-->
<!--                                    <span class="input-group-addon"> </span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">Bau</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="bau" name="bau" value="">-->
<!--                                    <span class="input-group-addon"> </span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">Kimia PH</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="kimia_ph" name="kimia_ph" value="">-->
<!--                                    <span class="input-group-addon"> </span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">Berat Jenis</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="berat_jenis" name="berat_jenis" value="">-->
<!--                                    <span class="input-group-addon"> </span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">Protein</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="protein" name="protein" value="">-->
<!--                                    <span class="input-group-addon">Negatif</span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">Glukosa</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="glukosa" name="glukosa" value="">-->
<!--                                    <span class="input-group-addon">Negatif</span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">Urobillinogen</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="urobillinogen" name="urobillinogen" value="">-->
<!--                                    <span class="input-group-addon">Negatif</span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">Billirudin</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="billirudin" name="billirudin" value="">-->
<!--                                    <span class="input-group-addon">Negatif</span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">Keton</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="keton" name="keton" value="">-->
<!--                                    <span class="input-group-addon">Negatif</span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">Lekosit Esterase</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="lekosit_esterase" name="lekosit_esterase" value="">-->
<!--                                    <span class="input-group-addon">Negatif</span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">Nitrit</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="nitrit" name="nitrit" value="">-->
<!--                                    <span class="input-group-addon">Negatif</span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">Blood</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="blood" name="blood" value="">-->
<!--                                    <span class="input-group-addon">Negatif</span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">Sedimen Epitel</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="sedimen_epitel" name="sedimen_epitel" value="">-->
<!--                                    <span class="input-group-addon"> </span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">Lekosit</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="lekosit" name="lekosit" value="">-->
<!--                                    <span class="input-group-addon"> </span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">Erytrosit</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="erystrosit" name="erytrosit" value="">-->
<!--                                    <span class="input-group-addon"> </span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">Silinder Granula</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="silinder_granula" name="silinder_granula" value="">-->
<!--                                    <span class="input-group-addon"> </span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">Silinder Lekosit</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="silinder_lekosit" name="silinder_lekosit" value="">-->
<!--                                    <span class="input-group-addon"> </span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">Kristal</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="kristal" name="kristal" value="">-->
<!--                                    <span class="input-group-addon"> </span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">Bakteri</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="bakteri" name="bakteri" value="">-->
<!--                                    <span class="input-group-addon"> </span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">Trikomonas</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="trikomonas" name="trikomonas" value="">-->
<!--                                    <span class="input-group-addon"> </span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">Candida</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="candida" name="candida" value="">-->
<!--                                    <span class="input-group-addon"> </span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">Silinder Eritrosit</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="silinder_eritrosit" name="silinder_eritrosit" value="">-->
<!--                                    <span class="input-group-addon"> </span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">Silinder Hyalin</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="silinder_hyalin" name="silinder_hyalin" value="">-->
<!--                                    <span class="input-group-addon"> </span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                        <div class="tab-pane" id="tab_12">-->
<!---->
<!--                            <h4 class="box-title text-center"><strong>FESES</strong></h4>-->
<!---->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">Warna</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="warnar" name="warna" value="">-->
<!--                                    <span class="input-group-addon">Khas</span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">Bau</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="bau" name="bau" value="">-->
<!--                                    <span class="input-group-addon"> </span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">Konsistensi</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="konsistensi" name="konsistensi" value="">-->
<!--                                    <span class="input-group-addon"> </span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">Mikroskopis</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="mikroskopis" name="mikroskopis" value="">-->
<!--                                    <span class="input-group-addon"> </span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">Telur Cacing</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="telur_cacing" name="telur_cacing" value="">-->
<!--                                    <span class="input-group-addon">Negatif</span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">Amuba</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="amuba" name="amuba" value="">-->
<!--                                    <span class="input-group-addon">Negatif</span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">Sisa Pencernaan</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="sisa_pencernaan" name="sisa_pencernaan" value="">-->
<!--                                    <span class="input-group-addon"> </span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">Protein</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="Protein" name="protein" value="">-->
<!--                                    <span class="input-group-addon"> </span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">Lemak</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="lemak" name="lemak" value="">-->
<!--                                    <span class="input-group-addon"> </span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">Karbohidrat</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="karbohidrat" name="karbohidrat" value="">-->
<!--                                    <span class="input-group-addon"> </span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">Bensidin Test</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="bensidin_test" name="bensidin_test" value="">-->
<!--                                    <span class="input-group-addon"> </span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                        <div class="tab-pane" id="tab_13">-->
<!---->
<!--                            <h4 class="box-title text-center"><strong>SPERMATOZOA</strong></h4>-->
<!---->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">Metode</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="metode" name="metode" value="">-->
<!--                                    <span class="input-group-addon"> </span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">Abstinensia</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="abstinensia" name="abstinensia" value="">-->
<!--                                    <span class="input-group-addon"> </span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">Dikeluarkan Jam</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="dikeluarkan_jam" name="dikeluarkan_jam" value="">-->
<!--                                    <span class="input-group-addon"> </span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">Diterima di lab jam</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="diterima_di_lab_jam" name="diterima_di_lab_jam" value="">-->
<!--                                    <span class="input-group-addon"> </span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">Diperiksa jam</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="diperiksa_jam" name="diperiksa_jam" value="">-->
<!--                                    <span class="input-group-addon"> </span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">I MAKROSKOPIS</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="i_makroskopis" name="i_makroskopis" value="">-->
<!--                                    <span class="input-group-addon"> </span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">Warna</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="warna" name="warna" value="">-->
<!--                                    <span class="input-group-addon">Putih Abu-Abu</span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">Liquefaksi</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="liquefaksi" name="liquefaksi" value="">-->
<!--                                    <span class="input-group-addon"> < 60 menit</span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">Konsistensi</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="konsistensi" name="konsistensi" value="">-->
<!--                                    <span class="input-group-addon">Encer</span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">Bau</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="bau" name="bau" value="">-->
<!--                                    <span class="input-group-addon">Khas</span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">Volume</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="volume" name="volume" value="">-->
<!--                                    <span class="input-group-addon"> => 2ml</span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">PH</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="ph" name="ph" value="">-->
<!--                                    <span class="input-group-addon">7.2 - 7.8</span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">II Mikroskopis</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="ii_mikroskopis" name="ii_mikroskopis" value="">-->
<!--                                    <span class="input-group-addon"> </span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">Konsentrasi( x 10^6/ml)</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="konsentrasi" name="konsentrasi" value="">-->
<!--                                    <span class="input-group-addon">=>20 x ( 10^6/ml)</span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">Motilitas(%)</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="motilitas" name="motilitas" value="">-->
<!--                                    <span class="input-group-addon"> </span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">A Linier Cepat</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="a_linier_cepat" name="a_linier_cepat" value="">-->
<!--                                    <span class="input-group-addon">=>50% (A)+(B)</span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">B Linier Lambat</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="b_linier_lambat" name="b_linier_lambat" value="">-->
<!--                                    <span class="input-group-addon">atau</span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">C Tidak Progressif</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="c_tidak_progressif" name="c_tidak_progressif" value="">-->
<!--                                    <span class="input-group-addon">=>25%(A)</span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">D Tidak Motil</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="d_tidak_motil" name="d_tidak_motil" value="">-->
<!--                                    <span class="input-group-addon"> </span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">Viabilitas (%hidup)</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="viabilitas_(%hidup)" name="viabilitas_(%hidup)" value="">-->
<!--                                    <span class="input-group-addon">=>75%</span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">Morfologi (%Normal)</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="morfologi_(%normal)" name="morfologi_(%normal)" value="">-->
<!--                                    <span class="input-group-addon">=>30%</span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">Morfologi Abnormal(K,L,E, Cyt)</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="morfologi_abnormal" name="morfologi_abnormal" value="">-->
<!--                                    <span class="input-group-addon"> </span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">Sel Bulat( x10^6/ml)</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="sel_bulat" name="sel_bulat" value="">-->
<!--                                    <span class="input-group-addon">< 1x10^6/ml</span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">Sel Leukosit( x10^6/ml)</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="sel_leukosit" name="sel_leukosit" value="">-->
<!--                                    <span class="input-group-addon">< 1x10^6/ml</span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">Aglutinasi</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="aglutinasi" name="aglutinasi" value="">-->
<!--                                    <span class="input-group-addon">Tidak</span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">Fruktosa</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="fruktosa" name="fruktosa" value="">-->
<!--                                    <span class="input-group-addon">> 13 u mol/ejakulat</span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                        <div class="tab-pane" id="tab_14">-->
<!---->
<!--                            <h4 class="box-title text-center"><strong>HORMON</strong></h4>-->
<!---->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">T3</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="t3" name="t3" value="">-->
<!--                                    <span class="input-group-addon">0.92-2.33</span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">T4</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="t4" name="t4" value="">-->
<!--                                    <span class="input-group-addon">60-120</span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">TSH</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="tsh" name="tsh" value="">-->
<!--                                    <span class="input-group-addon">Hipertiroid < 0.15 | Euthyroid 0.25 - 5</span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">FT4</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="ft4" name="ft4" value="">-->
<!--                                    <span class="input-group-addon">1.6-19.4</span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">Egfr</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="egfr" name="egfr" value="">-->
<!--                                    <span class="input-group-addon">Normal =>90 | Ringan:60-89</span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">TSHs</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="tshs" name="tshs" value="">-->
<!--                                    <span class="input-group-addon">0.27 - 4.70</span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                        <div class="tab-pane" id="tab_15">-->
<!---->
<!--                            <h4 class="box-title text-center"><strong>PETANDA TUMOR</strong></h4>-->
<!---->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">CEA</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="cea" name="cea" value="">-->
<!--                                    <span class="input-group-addon"> </span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">AFP</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="afp" name="afp" value="">-->
<!--                                    <span class="input-group-addon"> </span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">PSA</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="psa" name="psa" value="">-->
<!--                                    <span class="input-group-addon"> </span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">CEA</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="cea" name="cea" value="">-->
<!--                                    <span class="input-group-addon"> </span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                        <div class="tab-pane" id="tab_16">-->
<!---->
<!--                            <h4 class="box-title text-center"><strong>ADMINISTRASI</strong></h4>-->
<!---->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">Administrasi</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="administrasi" name="administrasi" value="">-->
<!--                                    <span class="input-group-addon"> </span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                        <div class="tab-pane" id="tab_17">-->
<!---->
<!--                            <h4 class="box-title text-center"><strong>BAHAN</strong></h4>-->
<!---->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">Lancet</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="lancet" name="lancet" value="">-->
<!--                                    <span class="input-group-addon"> </span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">Spuit 3cc</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="spuit_3cc" name="spuit_3cc" value="">-->
<!--                                    <span class="input-group-addon"> </span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">Spuit 5cc</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="spuit_5cc" name="spuit_5cc" value="">-->
<!--                                    <span class="input-group-addon"> </span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">Vacutainer</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="vacutainer" name="vacutainer" value="">-->
<!--                                    <span class="input-group-addon"> </span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">Wing Needle</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="wing_needle" name="wing_needle" value="">-->
<!--                                    <span class="input-group-addon"> </span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">Spuit 1cc</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="spuit_1cc" name="spuit_1cc" value="">-->
<!--                                    <span class="input-group-addon"> </span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">Hand Scun</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="hand_scun" name="spuit_3cc" value="">-->
<!--                                    <span class="input-group-addon"> </span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                        <div class="tab-pane" id="tab_18">-->
<!---->
<!--                            <h4 class="box-title text-center"><strong>NARKOBA</strong></h4>-->
<!---->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">Amphetamine</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="amphetamine" name="amphetamine" value="">-->
<!--                                    <span class="input-group-addon">Negatif</span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">Morphine</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="spuit_3cc" name="spuit_3cc" value="">-->
<!--                                    <span class="input-group-addon">Negatif</span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">BZO (Benzodizepiner)</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="bzo_(benzodizepiner)" name="bzo_(benzodizepiner)" value="">-->
<!--                                    <span class="input-group-addon">Negatif</span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">THC (Marijuana)</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="thc_(marijuana)" name="thc_(marijuana)" value="">-->
<!--                                    <span class="input-group-addon">Negatif</span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">MET (Methamphetamine)</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="met_(methamphetamine)" name="met_(methamphetamine)" value="">-->
<!--                                    <span class="input-group-addon">Negatif</span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                        <div class="tab-pane" id="tab_19">-->
<!---->
<!--                            <h4 class="box-title text-center"><strong>KEHAMILAN</strong></h4>-->
<!---->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">Tes Kehamilan</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="tes_kehamilan" name="tes_kehamilan" value="">-->
<!--                                    <span class="input-group-addon"> </span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                        <div class="tab-pane" id="tab_20">-->
<!---->
<!--                            <h4 class="box-title text-center"><strong>GOLONGAN DARAH</strong></h4>-->
<!---->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">Rhesus</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="rhesus" name="rhesus" value="">-->
<!--                                    <span class="input-group-addon"> </span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label for="inputtext3" class="col-sm-3 control-label">Golongan Darah</label>-->
<!--                                <div class="input-group col-sm-8 col-md-8 col-sm-8 col-lg-8">-->
<!--                                    <input type="text" class="form-control" id="golongan_darah" name="golongan_darah" value="">-->
<!--                                    <span class="input-group-addon"> </span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->
<!--</div>-->
