<link rel="stylesheet" href="<?php echo base_url() ?>assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
<style>
    a.disabled {
        pointer-events: none !important;
        cursor: default !important;
    }
</style>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Data List Pasien
            <small></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a href="#">List Pasien</a></li>
            <li class="active">Data List Pasien</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <?php if (isset($jenis_pendaftaran)) : ?>
            <?php foreach ($jenis_pendaftaran as $jp) : ?>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="box">
                            <div class="box-header">
                                <h3 class="box-title">Data Pasien Belum Diperiksa - <?= $jp->jenis_pendaftaran ?></h3>&nbsp;&nbsp;

                            </div>
                            <!-- /.box-header -->
                            <?php $warning = $this->session->flashdata('warning');
                            if (!empty($warning)) { ?>
                                <div class="alert alert-warning alert-dismissible">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">
                                        &times;
                                    </button>
                                    <h4><i class="icon fa fa-warning"></i> Alert!</h4>
                                    <?php echo $warning ?>
                                </div>
                            <?php } ?>
                            <?php $success = $this->session->flashdata('success');
                            if (!empty($success)) { ?>
                                <div class="alert alert-success alert-dismissible">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">
                                        &times;
                                    </button>
                                    <h4><i class="icon fa fa-check"></i> Success!</h4>
                                    <?php echo $success ?>
                                </div>
                            <?php } ?>
                            <div class="box-body">
                                <table class="table table-bordered table-hover example2">
                                    <thead>
                                    <tr>
                                        <th>No Antrian</th>
                                        <th>NO RM</th>
                                        <th>Nama Pasien</th>
                                        <th>Alamat</th>
                                        <th>Tanggal Daftar</th>
                                        <th>Jenis Pendaftaran</th>
                                        <th>Nama Dokter</th>
                                        <th>Catatan Alergi/Lainnya</th>
                                        <th>Status</th>
                                        <th>Aksi</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php $no = 1;
                                    foreach ($jp->list as $row) { ?>
                                        <tr>
                                            <td>
                                                <?php echo $row->kode_antrian; ?>
                                                <?php if ($row->is_mobile_jkn) : ?>
                                                    <br>
                                                    <small>
                                                        <span class="label label-danger">Mobile JKN</span>
                                                        <?php if ($row->is_check_in) : ?>
                                                            <span class="label label-success">Check In</span>
                                                        <?php endif; ?>
                                                    </small>
                                                <?php endif; ?>
                                            </td>
                                            <td style="width: 100px;">
                                                <span class="norm"><?php echo $row->no_rm; ?></span>
                                                <br>
                                                <small>
                                                    <span class="label <?= $jaminan[$row->jaminan]['class'] ?>"><?= $jaminan[$row->jaminan]['label'] ?></span>
                                                    <?php if (!isset($jaminan[$row->jaminan])) { ?>
                                                        <span class="label label-warning">Umum</span>
                                                    <?php } ?>
                                                </small>
                                            </td>
                                            <td class="nama">
                                                <?php echo ucwords($row->nama_pasien); ?>
                                                <?php if ($row->pendaftaran_id) : ?>
                                                    <br>
                                                    <span class="label label-danger">Rujuk Internal</span>
                                                <?php endif; ?>
                                            </td>
                                            <td> <?php echo ucwords($row->alamat); ?></td>
                                            <td> <?php echo date('d-F-Y H:i', strtotime($row->waktu_pemeriksaan)); ?></td>
                                            <td> <?php echo ucwords($row->jenis_pendaftaran); ?></td>
                                            <td> <?php echo ucwords($row->nama_dokter ?? '-'); ?></td>
                                            <td> <?php echo ucwords($row->asuhan_keperawatan); ?></td>
                                            <td><span class="pull-right-container"><small
                                                            class="label pull-right bg-green text-uppercase"><?php echo ucwords(str_replace('_', ' ', $row->status)); ?></small></span>
                                            </td>
                                            <td>
                                                <div class="btn-group" role="group">
                                                    <button id="btnGroupDrop1" type="button" class="btn btn-sm btn-success dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                        <i class="fa fa-bars"></i>
                                                    </button>
                                                    <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                                                        <button onclick="panggil(<?=$row->antrian_id?>, '<?=$row->kode_antrian?>', '<?=$jp->jenis_pendaftaran?>', '<?=$row->id?>')"
                                                                class=" btn btn-sm btn-block btn-success" style="margin-bottom: 5px;">
                                                            <i class="fa fa-volume-up"></i> Panggil
                                                        </button>
                                                        <?php if($jp->jenis_pendaftaran_id == 58) : ?>
                                                            <a href="<?php echo base_url(); ?>Igd/periksa/<?php echo $row->id; ?>"
                                                               class="btn btn-sm btn-block btn-warning" style="margin-bottom: 5px;">
                                                                <i class="fa fa-pencil"></i> Periksa
                                                            </a>
                                                        <?php else : ?>
                                                            <a href="<?php echo base_url(); ?>pemeriksaan/periksa/<?php echo $row->id; ?>"
                                                               class="btn btn-sm btn-block btn-warning"
                                                               style="margin-bottom: 5px;">
                                                                <i class="fa fa-pencil"></i> Periksa
                                                            </a>
                                                        <?php endif; ?>
                                                        <button type="button"
                                                                class="btn btn-sm btn-primary btn-block btn-rekamedis"
                                                                data-pasien_id="<?= $row->pasien_id ?>">
                                                            <i class="fa fa-search"></i> Rekam Medis
                                                        </button>
                                                        <a href="<?php echo base_url(); ?>pemeriksaan/hapus/<?php echo $row->id; ?>/<?php echo $row->pdf_id; ?>"
                                                           onclick="return confirm('Yakin hapus data pemeriksaan ini?')"
                                                           class="btn btn-sm btn-block btn-danger" style="margin-bottom: 5px;">
                                                            <i class="fa fa-trash"></i> Hapus
                                                        </a>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <?php $no++;
                                    } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12" id="tess"></div>
                </div>
            <?php endforeach; ?>
        <?php else : ?>
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Data Pasien Belum Diperiksa</h3>&nbsp;&nbsp;

                        </div>
                        <!-- /.box-header -->
                        <?php $warning = $this->session->flashdata('warning');
                        if (!empty($warning)) { ?>
                            <div class="alert alert-warning alert-dismissible">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;
                                </button>
                                <h4><i class="icon fa fa-warning"></i> Alert!</h4>
                                <?php echo $warning ?>
                            </div>
                        <?php } ?>
                        <?php $success = $this->session->flashdata('success');
                        if (!empty($success)) { ?>
                            <div class="alert alert-success alert-dismissible">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;
                                </button>
                                <h4><i class="icon fa fa-check"></i> Success!</h4>
                                <?php echo $success ?>
                            </div>
                        <?php } ?>
                        <div class="box-body">
                            <table class="table table-bordered table-hover example2">
                                <thead>
                                <tr>
                                    <th>No Antrian</th>
                                    <th>NO RM</th>
                                    <th>Nama Pasien</th>
                                    <th>Alamat</th>
                                    <th>Tanggal Daftar</th>
                                    <th>Jenis Pendaftaran</th>
                                    <th>Nama Dokter</th>
                                    <th>Catatan Alergi/Lainnya</th>
                                    <th>Status</th>
                                    <th>Aksi</th>
                                </tr>
                                </thead>

                                <tbody>
                                <?php $no = 1;
                                foreach ($listPendaftaran->result() as $row) { ?>

                                    <tr>
                                        <td>
                                            <?php echo $row->kode_antrian; ?>
                                            <?php if ($row->is_mobile_jkn) : ?>
                                                <br>
                                                <small>
                                                    <span class="label label-danger">Mobile JKN</span>
                                                    <?php if ($row->is_check_in) : ?>
                                                        <span class="label label-success">Check In</span>
                                                    <?php endif; ?>
                                                </small>
                                            <?php endif; ?>
                                        </td>
                                        <td style="width: 100px;">
                                            <span class="norm"><?php echo $row->no_rm; ?></span>
                                            <br>
                                            <small>
                                                <span class="label <?= $jaminan[$row->jaminan]['class'] ?>"><?= $jaminan[$row->jaminan]['label'] ?></span>
                                                <?php if (!isset($jaminan[$row->jaminan])) { ?>
                                                    <span class="label label-warning">Umum</span>
                                                <?php } ?>
                                            </small>
                                        </td>
                                        <td class="nama">
                                            <?php echo ucwords($row->nama_pasien); ?>
                                            <?php if ($row->pendaftaran_id) : ?>
                                                <br>
                                                <span class="label label-danger">Rujuk Internal</span>
                                            <?php endif; ?>
                                        </td>
                                        <td> <?php echo ucwords($row->alamat); ?></td>
                                        <td> <?php echo date('d-F-Y H:i', strtotime($row->waktu_pemeriksaan)); ?></td>
                                        <td> <?php echo ucwords($row->jenis_pendaftaran); ?></td>
                                        <td> <?php echo ucwords($row->nama_dokter ?? '-'); ?></td>
                                        <td> <?php echo ucwords($row->asuhan_keperawatan); ?></td>
                                        <td><span class="pull-right-container"><small
                                                        class="label pull-right bg-green text-uppercase"><?php echo ucwords(str_replace('_', ' ', $row->status)); ?></small></span>
                                        </td>
                                        <td>
                                            <div class="btn-group" role="group">
                                                <button id="btnGroupDrop1" type="button" class="btn btn-sm btn-success dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    <i class="fa fa-bars"></i>
                                                </button>
                                                <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                                                    <button onclick="panggil(<?=$row->antrian_id?>, '<?=$row->kode_antrian?>', '<?=$row->jenis_pendaftaran?>', '<?=$row->id?>')"
                                                            class=" btn btn-sm btn-block btn-success" style="margin-bottom: 5px;">
                                                        <i class="fa fa-volume-up"></i> Panggil
                                                    </button>
                                                    <?php if($row->jenis_pendaftaran_id == 58) : ?>
                                                        <a href="<?php echo base_url(); ?>Igd/periksa/<?php echo $row->id; ?>"
                                                           class="btn btn-sm btn-block btn-warning" style="margin-bottom: 5px;"><i class="fa fa-pencil"></i> Periksa
                                                        </a>
                                                    <?php else : ?>
                                                        <a href="<?php echo base_url(); ?>pemeriksaan/periksa/<?php echo $row->id; ?>"
                                                           class="btn btn-sm btn-block btn-warning" style="margin-bottom: 5px;"><i class="fa fa-pencil"></i> Periksa
                                                        </a>
                                                    <?php endif; ?>
                                                    <button type="button"
                                                            class="btn btn-sm btn-primary btn-block btn-rekamedis"
                                                            data-pasien_id="<?= $row->pasien_id ?>"><i class="fa fa-search"></i> Rekam Medis
                                                    </button>
                                                    <a href="<?php echo base_url(); ?>pemeriksaan/hapus/<?php echo $row->id; ?>/<?php echo $row->pdf_id; ?>"
                                                       onclick="return confirm('Yakin hapus data pemeriksaan ini?')"
                                                       class="btn btn-sm btn-block btn-danger" style="margin-bottom: 5px;">
                                                        <i class="fa fa-trash"></i> Hapus
                                                    </a>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <?php $no++;
                                } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12" id="tess"></div>
            </div>
        <?php endif; ?>
    </section>
    <!-- /.content -->

</div>

<script src="https://cdn.socket.io/4.5.4/socket.io.min.js"></script>

<script>
    <?php if (isset($jenis_pendaftaran)) : ?>
        let list = []
        <?php foreach ($jenis_pendaftaran as $jp) : ?>
            <?php foreach ($jp->list as $row) : ?>
                list.push(<?=json_encode($row)?>)
            <?php endforeach; ?>
        <?php endforeach; ?>
    <?php else : ?>
        const list = <?=json_encode($listPendaftaran->result())?>;
    <?php endif; ?>

    let socket

    $(function () {
        socket = io("https://socket.pmibanyumasku.simklinik.net/", {
            // transports: ["websocket"],
            // forceNew: true
        });

        socket.on("connect", () => {
            console.log(`connected: ${socket.id}`);

            <?php if (!empty($success)) { ?>
            socket.emit('reload-farmasi')
            <?php } ?>
        });
        socket.on('connect_failed', function (data) {
            console.log(data || 'connect_failed');
        });
        socket.on('connect_error', function (data) {
            console.log(data || 'connect_error');
        });
    })

    let speech = new SpeechSynthesisUtterance();
    speech.lang = "id";
    speech.rate = 0.8

    function panggil(id, code, poli, row_id) {
        $.ajax({url: `<?=base_url()?>AntrianPoli/call_next/${id}`}).done(function() { })

        const row = list.find(v => +v.id === +row_id)
        if (row) {
            socket.emit('call', {
                jenis_pendaftaran_id: row.jenis_pendaftaran_id,
                antrian_id: id,
                kode_antrian: code,
                nama_poli: poli,
                nama_dokter: row.nama_dokter ?? '',
                nama_pasien: row.nama_pasien,
                no_rm: row.no_rm,
                nik: row.nik,
            });

            const kode = code.split('').join(' ... ')
            speech.text = `Nomor antrian.... ${kode} .... atas nama ${row.nama_pasien.replaceAll('.', '').toLowerCase().replaceAll('tn ', '').replaceAll('ny ', '').replaceAll('an ', '').replaceAll('sdr ', '')}. .... Silahkan ke ${poli}`
            window.speechSynthesis.speak(speech)
        }
        else {

            const kode = code.split('').join(' ... ')
            speech.text = `Nomor antrian.... ${kode} .... Silahkan ke ${poli}`
            window.speechSynthesis.speak(speech)
        }
    }

    $(function () {
        $('#example1').DataTable()
        $('.example2').DataTable({
            rowReorder: {
                selector: 'td:nth-child(2)'
            },
            'paging': true,
            'lengthChange': true,
            'searching': true,
            'ordering': true,
            'info': true,
            'autoWidth': true
        })
    });

</script>
