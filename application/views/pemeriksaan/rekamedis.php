<div style="display: flex">
    <div style="flex: 1">
        <table>
            <tbody>
            <tr>
                <td>NORM</td>
                <td>&nbsp;:&nbsp;</td>
                <td><?= $pasien->no_rm ?></td>
            </tr>
            <tr>
                <td>Nama</td>
                <td>&nbsp;:&nbsp;</td>
                <td><?= $pasien->nama ?></td>
            </tr>
            <tr>
                <td>Tempat Tanggal Lahir</td>
                <td>&nbsp;:&nbsp;</td>
                <td><?= $pasien->tempat_lahir ?>, <?= $pasien->tanggal_lahir ?></td>
            </tr>
            <tr>
                <td>Usia</td>
                <td>&nbsp;:&nbsp;</td>
                <td><?= $pasien->usia ?></td>
            </tr>
            <tr>
                <td>Jenis Kelamin</td>
                <td>&nbsp;:&nbsp;</td>
                <td><?= $pasien->jk ?></td>
            </tr>
            </tbody>
        </table>
    </div>
    <div style="flex: 1">
        <table>
            <tbody>
            <tr>
                <td>Alamat</td>
                <td>&nbsp;:&nbsp;</td>
                <td><?= $pasien->alamat ?></td>
            </tr>
            <tr>
                <td>Telepon</td>
                <td>&nbsp;:&nbsp;</td>
                <td><?= $pasien->telepon ?></td>
            </tr>
            <tr>
                <td>Pekerjaan</td>
                <td>&nbsp;:&nbsp;</td>
                <td><?= $pasien->pekerjaan ?></td>
            </tr>
            <tr>
                <td>Agama</td>
                <td>&nbsp;:&nbsp;</td>
                <td><?= $pasien->agama ?></td>
            </tr>
            <tr>
                <td>Penanggung Jawab</td>
                <td>&nbsp;:&nbsp;</td>
                <td><?= $pasien->penanggungjawab ?></td>
            </tr>
            </tbody>
        </table>
    </div>
</div>
<p>&nbsp;</p>
<div class="table-responsive">
    <table id="tbl" class="table table-bordered table-striped">
        <thead>
        <tr>
            <th>No</th>
            <th>Tanggal Periksa</th>
            <th>Nama Poli</th>
            <th>Nama Dokter</th>
            <th>Diagnosis Jenis Penyakit</th>
            <th>Anamnesis</th>
            <th>Tindakan</th>
            <th>Tata Laksana</th>
            <th>Obat</th>
            <th>Obat Racik</th>
        </tr>
        </thead>
        <tbody>
        <?php $no = 0;
        foreach ($pemeriksaan as $k => $row) {
            $no++;
            ?>
            <tr>
                <td><?=$k + 1?></td>
                <td> <?php echo date('d-F-Y', strtotime($row->waktu_pemeriksaan)); ?></td>
                <td><?= $row->nama_poli ?></td>
                <td><?= $row->nama_d ?></td>
                <td>
                    <table class="table table-bordered tbl-rekammedis">
                        <tbody>
                        <?php
                        if ($penyakit) {

                            foreach ($penyakit->result() as $row1) {
                                if ($row->id == $row1->pemeriksaan_id) {
                                    ?>
                                    <tr>
                                        <td><?php echo $row1->kode; ?> - <?php echo $row1->nama; ?></td>
                                    </tr>
                                <?php }
                            }
                        } else { ?>
                            <tr>
                                <td bgcolor="lightblue" colspan="3"> Data Tidak ada</td>
                            </tr>
                        <?php } ?>

                        </tbody>
                    </table>
                </td>
                <td><?= $row->amammesia ?></td>
                <td>
                    <table class="table table-bordered tbl-rekammedis">
                        <tbody>
                        <?php


                        if ($tindakan) {

                            foreach ($tindakan->result() as $row3) {
                                if ($row->id == $row3->pemeriksaan_id) {
                                    ?>
                                    <tr>

                                        <td> <?php echo $row3->nama; ?></td>

                                    </tr>
                                <?php }
                            }
                        } else {
                            ?>
                            <tr>
                                <td bgcolor="lightblue" colspan="3"> Data Tidak ada</td>
                            </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                </td>
                <td><?= $row->deskripsi_tindakan ?></td>
                <td>
                    <table class="table table-bordered tbl-rekammedis">
                        <thead>
                        <tr>
                            <th>Nama</th>
                            <th>Signa</th>
                            <th>Jumlah</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $o = 0; foreach ($obat->result() as $row2) {
                            if ($row->id == $row2->pemeriksaan_id) { ?>
                                <tr>
                                    <td> <?php echo $row2->nama; ?></td>
                                    <td> <?php echo $row2->signa_obat; ?></td>
                                    <td> <?php echo $row2->jumlah_satuan; ?></td>
                                </tr>
                            <?php $o++; }
                            $no++;
                        } ?>
                        <?php if (!$o) : ?>
                            <tr>
                                <td class="no-data text-center" bgcolor="#FEFFEF" colspan="3"> Data tidak ada</td>
                            </tr>
                        <?php endif; ?>
                        </tbody>
                    </table>
                </td>
                <td>
                    <table class="table table-bordered tbl-rekammedis">
                        <thead>
                        <tr>
                            <th class="bor">Nama</th>
                            <th class="bor">Signa</th>
                            <th class="bor">Obat</th>
                            <th class="bor">Catatan</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $or = 0; ?>
                        <?php if (!empty($row->racikans)) : ?>
                            <?php foreach ($row->racikans as $row2) : ?>
                                <?php if (count($row2->racikan)) : ?>
                                    <tr>
                                        <td class="bor" valign="top"> <?= $row2->nama_racikan; ?></td>
                                        <td class="bor" valign="top"> <?= $row2->signa; ?></td>
                                        <td class="bor">
                                            <table>
                                                <tbody>
                                                <?php foreach ($row2->racikan as $v) : ?>
                                                    <tr>
                                                        <td><?= $v->nama ?></td>
                                                        <td><?= $v->jumlah_satuan ?></td>
                                                    </tr>
                                                <?php endforeach; ?>
                                                </tbody>
                                            </table>
                                        </td>
                                        <td class="bor cat" valign="top"> <?= $row2->catatan; ?></td>
                                    </tr>
                                    <?php $or++; ?>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        <?php endif; ?>
                        <?php if (!$or) : ?>
                            <tr>
                                <td class="no-data text-center" bgcolor="#FEFFEF" colspan="4"> Data tidak ada</td>
                            </tr>
                        <?php endif; ?>
                        </tbody>
                    </table>
                </td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
</div>
<script>
    $('#tbl').dataTable({
        "pageLength": 5,
        "lengthMenu": [[5, 10, 25, 50, 100], [5, 10, 25, 50, 100]]
    })
</script>