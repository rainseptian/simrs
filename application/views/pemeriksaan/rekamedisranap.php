<div style="display: flex">
    <div style="flex: 1">
        <table>
            <tbody>
                <tr>
                    <td>NORM</td>
                    <td>&nbsp;:&nbsp;</td>
                    <td><?= $list_rawat_inap->no_rm ?></td>
                </tr>
                <tr>
                    <td>Nama</td>
                    <td>&nbsp;:&nbsp;</td>
                    <td><?= $list_rawat_inap->nama ?></td>
                </tr>
                <tr>
                    <td>Tempat Tanggal Lahir</td>
                    <td>&nbsp;:&nbsp;</td>
                    <td><?= $list_rawat_inap->tempat_lahir ?>, <?= $list_rawat_inap->tanggal_lahir ?></td>
                </tr>
                <tr>
                    <td>Usia</td>
                    <td>&nbsp;:&nbsp;</td>
                    <td><?= $list_rawat_inap->usia ?></td>
                </tr>
                <tr>
                    <td>Jenis Kelamin</td>
                    <td>&nbsp;:&nbsp;</td>
                    <td><?= $list_rawat_inap->jk ?></td>
                </tr>
            </tbody>
        </table>
    </div>
    <div style="flex: 1">
        <table>
            <tbody>
                <tr>
                    <td>Alamat</td>
                    <td>&nbsp;:&nbsp;</td>
                    <td><?= $list_rawat_inap->alamat ?></td>
                </tr>
                <tr>
                    <td>Telepon</td>
                    <td>&nbsp;:&nbsp;</td>
                    <td><?= $list_rawat_inap->telepon ?></td>
                </tr>
                <tr>
                    <td>Pekerjaan</td>
                    <td>&nbsp;:&nbsp;</td>
                    <td><?= $list_rawat_inap->pekerjaan ?></td>
                </tr>
                <tr>
                    <td>Agama</td>
                    <td>&nbsp;:&nbsp;</td>
                    <td><?= $list_rawat_inap->agama ?></td>
                </tr>
                <tr>
                    <td>Penanggung Jawab</td>
                    <td>&nbsp;:&nbsp;</td>
                    <td><?= $list_rawat_inap->penanggungjawab ?></td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
<p>&nbsp;</p>
<div class="table-responsive">
    <table id="tbl" class="table table-bordered table-striped">
        <thead>
            <tr>
                <th>No</th>
                <th>Tanggal MRS</th>
                <th>Tanggal Pulang</th>
                <th>Nama Dokter</th>
                <th>Poli</th>
                <th>Aksi</th>
            </tr>
        </thead>
        <tbody>
            <?php $no = 0;
            foreach ($ri_pemeriksaan as $k => $row) {
                $no++;
            ?>
                <tr>
                    <td><?= $k + 1 ?></td>
                    <td><?= date('d-F-Y H:i', strtotime($r['created_at'])) ?></td>
                    <td><?= date('d-F-Y H:i', strtotime($r['tgl_boleh_pulang'])) ?></td>
                    <td><?= $row->nama_d ?></td>
                    <td><?= $row->nama_poli ?></td>
                </tr>
            <?php } ?>
        </tbody>
    </table>
</div>
<script>
    $('#tbl').dataTable({
        "pageLength": 5,
        "lengthMenu": [
            [5, 10, 25, 50, 100],
            [5, 10, 25, 50, 100]
        ]
    })
</script>