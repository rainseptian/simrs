<?php

function isLab($pendaftaran)
{
    return $pendaftaran['kode_daftar'] == 'PL' || $pendaftaran['kode_daftar'] == 'BPJS-PL';
}

function getKodePoli($kode_daftar = '', $data_poli)
{
    foreach ($data_poli as $key => $value) {
        if (in_array($kode_daftar, $value['kode'])) return $key;
    }
    return 'umum';
}

function isPendukung($kode_daftar, $data_poli)
{
    foreach (['ekg', 'spirometri', 'pemeriksaan-laborat'] as $value) {
        if (isset($data_poli[$value]) && in_array($kode_daftar, $data_poli[$value]['kode'])) return true;
    }
    return false;
}

$data_poli = $this->config->item('poli');
$kode_poli = getKodePoli($pendaftaran['kode_daftar'], $data_poli);
$isDataPendukung = isPendukung($pendaftaran['kode_daftar'], $data_poli);

include BASEPATH . '../application/views/template/InputBuilder.php';
?>

<style media="screen">
    .select2-container {
        width: 100% !important;
    }
</style>
<link rel="stylesheet" href="<?= base_url() ?>assets/plugins/iCheck/all.css">



<form class="form-horizontal" method="post" action="<?= base_url() ?>pemeriksaan/periksa/<?= $pemeriksaan['id'] ?>">

    <input type="hidden" name="pendaftaran_id" id="pendaftran_id" value="<?= $pemeriksaan['pendaftaran_id']; ?>">
    <input type="hidden" name="dokter_id" id="dokter_id" value="<?= $pemeriksaan['dokter_id']; ?>">
    <input type="hidden" name="pasien_id" id="pasien_id" value="<?= $pendaftaran['pasien'] ?>">
    <input type="hidden" name="kode_daftar" id="kode_daftar" value="<?= $pendaftaran['kode_daftar']; ?>">
    <input type="hidden" name="is_rujuk_internal" id="is_rujuk_internal" value="<?= $pendaftaran['pendaftaran_id'] ? 1 : 0 ?>">

    <div class="content-wrapper">
        <section class="content-header">
            <h1>
                Pemeriksaan Pasien
                <small>Preview</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="<?= base_url() ?>Dashbord"><i class="fa fa-dashboard"></i> Dashbord</a></li>
                <li class="active">Pemeriksaan</li>
            </ol>
        </section>

        <?php $warning = $this->session->flashdata('warning');
        if (!empty($warning)) { ?>
            <div class="alert alert-warning alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-warning"></i> Alert!</h4>
                <?php echo $warning ?>
            </div>
        <?php } ?>
        <?php $success = $this->session->flashdata('success');
        if (!empty($success)) { ?>
            <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-check"></i> Success!</h4>
                <?php echo $success ?>
            </div>
        <?php } ?>

        <section class="content">
            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-7">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="box box-danger">
                                <div class="box-header">
                                    <h3 class="box-title"> No. Rekam Medis : <?= $pendaftaran['no_rm']; ?> </h3>
                                </div>
                                <div class="box-body">
                                    <div class="box-body">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <?php
                                                unit('td', 'TD')->val($pemeriksaan['td'])->required(!isLab($pendaftaran))->unit('mmHg')->build();
                                                unit('r', 'R')->val($pemeriksaan['r'])->required(!isLab($pendaftaran))->unit('K/Min')->build();
                                                unit('bb', 'BB')->val($pemeriksaan['bb'])->required(!isLab($pendaftaran))->unit('Kg')->onkeyup('set_bmi()')->build();
                                                unit('n', 'N')->val($pemeriksaan['n'])->required(!isLab($pendaftaran))->unit('K/Min')->build();
                                                unit('s', 'S')->val($pemeriksaan['s'])->required(!isLab($pendaftaran))->unit("'0")->build();
                                                unit('tb', 'TB')->val($pemeriksaan['tb'])->required(!isLab($pendaftaran))->unit("cm")->build();
                                                unit('spo2', 'Spo2')->val($pemeriksaan['spo2'])->required(!isLab($pendaftaran))->unit("%")->build();
                                                ?>
                                            </div>
                                        </div>

                                        <?php sm4('bmi', 'BMI')->val($pemeriksaan['bmi'])->build(); ?>
                                        <?php sm4('no_rm', 'No Rekam Medis')->val($pemeriksaan['no_rm'])->readonly()->build(); ?>
                                        <?php sm4('nama_pasien', 'Nama Pasien')->val($pemeriksaan['nama_pasien'])->readonly()->build(); ?>

                                        <div class="form-group">
                                            <label class="col-sm-3"></label>
                                            <div class="col-sm-9">
                                                <span class="label <?= $jaminan[$pemeriksaan['jaminan']]['class'] ?>"><?= $jaminan[$pemeriksaan['jaminan']]['label'] ?></span>
                                                <?php if (!isset($jaminan[$pemeriksaan['jaminan']])) : ?>
                                                    <span class="label label-warning">Umum</span>
                                                <?php endif; ?>
                                                <?php if ($pendaftaran['pendaftaran_id']) : ?>
                                                    <span class="label label-danger">Rujuk Internal</span>
                                                <?php endif; ?>
                                                <button type="button" name="button" class="btn btn-sm btn-primary btn-rekamedis" data-pasien_id="<?= $pendaftaran['pasien'] ?>">
                                                    <i class="fa fa-search"></i> Lihat Riwayat Poli
                                                </button>
                                                <!-- <button type="button" name="button" class="btn btn-sm btn-primary btn-rekamedisranap" data-pasien_id="<?= $pemeriksaan['pasien'] ?>">
                                                    <i class="fa fa-search"></i> Lihat Riwayat Rawat Inap
                                                </button> -->

                                            </div>
                                        </div>

                                        <?php if ($isDataPendukung) : ?>
                                            <?php $this->load->view('pemeriksaan/poli/' . $kode_poli, []); ?>
                                        <?php else : ?>
                                            <?php
                                            sm9('diagnosa_perawat', 'Diagnosa Perawat')->textarea(3)->val($pemeriksaan['diagnosa_perawat'])->build();
                                            sm9('keluhan_utama', 'Keluhan Utama')->textarea(3)->val($pemeriksaan['keluhan_utama'])->build();

                                            switch ($kode_poli) {
                                                case 'gigi':
                                                    odontogram();
                                                    br();
                                                    sm9('catatan_odontogram', 'Catatan Odontogram')->build();
                                                    break;

                                                case 'mata':
                                                    sm9('amammesia', 'Anamnesis')->textarea(3)->val($pemeriksaan['amammesia'] ?? '')->build();
                                                    $this->load->view('pemeriksaan/poli/mata', []);
                                                    break;

                                                case 'kulit':
                                                    sm9('amammesia', 'Anamnesis')->textarea(3)->val($pemeriksaan['amammesia'] ?? '')->build();
                                                    $this->load->view('pemeriksaan/poli/kulit', []);
                                                    break;

                                                case 'tht':
                                                    sm9('amammesia', 'Anamnesis')->textarea(3)->val($pemeriksaan['amammesia'] ?? '')->build();
                                                    $this->load->view('pemeriksaan/poli/tht', []);
                                                    break;

                                                case 'anak':
                                                    sm9('amammesia', 'Anamnesis')->textarea(3)->val($pemeriksaan['amammesia'] ?? '')->build();
                                                    sm9('pemeriksaan_fisik', 'Pemeriksaan Fisik')->textarea(3)->val($pemeriksaan['pemeriksaan_fisik'] ?? '')->build();
                                                    $this->load->view('pemeriksaan/poli/anak', []);
                                                    break;

                                                case 'obgyn':
                                                    sm9('amammesia', 'Anamnesis')->textarea(3)->val($pemeriksaan['amammesia'] ?? '')->build();
                                                    $this->load->view('pemeriksaan/poli/obgyn', []);
                                                    break;

                                                case 'jiwa':
                                                    sm9('amammesia', 'Anamnesis')->textarea(3)->val($pemeriksaan['amammesia'] ?? '')->build();
                                                    $this->load->view('pemeriksaan/poli/jiwa', []);
                                                    break;

                                                case 'syaraf':
                                                    sm9('amammesia', 'Anamnesis')->textarea(3)->val($pemeriksaan['amammesia'] ?? '')->build();
                                                    $this->load->view('pemeriksaan/poli/syaraf', []);
                                                    break;

                                                case 'hd':
                                                    sm9('amammesia', 'Anamnesis')->textarea(3)->val($pemeriksaan['amammesia'] ?? '')->build();
                                                    $this->load->view('pemeriksaan/poli/hemodialisis', []);
                                                    break;

                                                case 'kia':
                                                    sm9('amammesia', 'Anamnesis')->textarea(3)->val($pemeriksaan['amammesia'] ?? '')->build();
                                                    $this->load->view('pemeriksaan/poli/kia', []);
                                                    $kia = unserialize($pemeriksaan['meta']);
                                                    $kia_hamil = in_array('hamil', $kia['jenis']) ? 1 : 0;
                                                    $kia_kb = in_array('kb', $kia['jenis']) ? 1 : 0;
                                                    echo '<input type="hidden" class="form-control" name="kia_hamil" value="' . $kia_hamil . '">';
                                                    echo '<input type="hidden" class="form-control" name="kia_kb" value="' . $kia_kb . '">';
                                                    break;

                                                default:
                                                    sm9('amammesia', 'Anamnesis')->textarea(3)->val($pemeriksaan['amammesia'] ?? '')->build();
                                                    sm9('pemeriksaan_fisik', 'Pemeriksaan Fisik')->textarea(3)->val($pemeriksaan['pemeriksaan_fisik'] ?? '')->build();
                                                    break;
                                            }

                                            hasil_penunjang($pemeriksaan);
                                            sm9('laboratorium', 'Laboratorium')->textarea(3)->val($pemeriksaan['laboratorium'])->build();
                                            ?>
                                        <?php endif ?>

                                        <?php $excap = ['PE', 'BPJS-PE', 'P-Sirometri', 'BPJS-P-Sirometri', 'P-Laborat', 'BPJS-P-Laborat']; ?>
                                        <?php sel('diagnosis_jenis_penyakit[]', 'Diagnosis Jenis Penyakit')
                                            ->id('diagnosis_jenis_penyakit')
                                            ->onchange('on_diagnosis_change()')
                                            ->placeholder('Pilih penyakit untuk pasien')
                                            ->options($penyakit->result())
                                            ->selectedOptions($s_penyakit)
                                            ->display(function ($value) {
                                                return $value->kode . ' - ' . $value->nama;
                                            })
                                            ->build(); ?>
                                        <?php if ($pendaftaran['kode_daftar'] != 'PB' && $pendaftaran['kode_daftar'] != 'PA') : ?>
                                            <?php sm9('diagnosis', 'Diagnosis')
                                                ->id('diagnosis')
                                                ->onkeyup('on_diagnosis_change()')
                                                ->textarea(2)
                                                ->val($pemeriksaan['diagnosis'])
                                                ->build(); ?>
                                        <?php endif; ?>
                                        <?php sel('tindakan[]', 'Tarif / Tindakan')
                                            ->id('tindakan')
                                            ->onchange('on_tindakan_change()')
                                            ->placeholder('Pilih tindakan untuk pasien')
                                            ->options($tindakan->result())
                                            ->selectedOptions($s_tindakan)
                                            ->display(function ($value) use (&$pendaftaran) {
                                                return $value->nama . " - Rp." . number_format($value->tarif_pasien, 2, ',', '.');
                                            })
                                            ->build(); ?>
                                        <?php if ($pendaftaran['pendaftaran_id']) : ?>
                                            <?php sel('dokter', 'Dokter')
                                                ->single()
                                                ->placeholder('Pilih dokter')
                                                ->options($listDokter->result())
                                                ->display(function ($value) {
                                                    return ucwords($value->nama);
                                                })
                                                ->selectedOptionIds([$pemeriksaan['dokter_id']])
                                                ->required()
                                                ->build(); ?>
                                        <?php endif; ?>
                                        <?php sel('perawat[]', 'Perawat')
                                            ->placeholder('Pilih perawat')
                                            ->options($listPerawat->result())
                                            ->display(function ($value) {
                                                return ucwords($value->nama);
                                            })
                                            ->selectedOptionIds(explode(',', $pemeriksaan['detail_perawat_id']))
                                            ->required()
                                            ->build(); ?>
                                        <?php sm9('deskripsi_tindakan', 'Tata Laksana')->textarea(2)->val($pemeriksaan['deskripsi_tindakan'])->build(); ?>
                                        <?php sm9('saran_pemeriksaan', 'Rujukan')->val($pemeriksaan['saran_pemeriksaan'])->build(); ?>
                                        <?php sel('surat', 'Pilih Surat')
                                            ->placeholder('Pilih Surat')
                                            ->single()
                                            ->normal()
                                            ->id('surat')
                                            ->onchange('change_surat()')
                                            ->options([
                                                (object) ['id' => '', 'value' => '--Pilih Surat--'],
                                                (object) ['id' => 'sehat', 'value' => 'Surat Keterangan Sehat'],
                                                (object) ['id' => 'sakit', 'value' => 'Surat Keterangan Sakit'],
                                                (object) ['id' => 'rujuk', 'value' => 'Surat Keterangan Rujuk'],
                                                (object) ['id' => 'ranap', 'value' => 'Surat Pengantar Rawat Inap'],
                                            ])
                                            ->selectedOptionIds([$pendaftaran['surat']])
                                            ->display(function ($v) {
                                                return $v->value;
                                            })
                                            ->build(); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <?php $this->load->view('pemeriksaan/partials/surat_sehat'); ?>
                            <?php $this->load->view('pemeriksaan/partials/surat_sakit'); ?>
                            <?php $this->load->view('pemeriksaan/partials/surat_rujuk'); ?>
                            <?php $this->load->view('pemeriksaan/partials/surat_ranap'); ?>
                        </div>
                    </div>
                </div>
                <?php $this->load->view('pemeriksaan/partials/side_1'); ?>
                <?php $this->load->view('pemeriksaan/partials/side_2'); ?>
            </div>
        </section>
    </div>
</form>

<div id='ResponseInput'></div>

<script src="<?= base_url() ?>assets/bower_components/select2/dist/js/select2.full.min.js"></script>

<script type="text/javascript">
    $('.select2').select2();

    function set_bmi() {
        var tb = $('#tb').val();
        var bb = $('#bb').val();
        var tbm = tb / 100;
        var bmi = bb / (tbm * tbm);

        $('#bmi').val(bmi.toFixed(2));
    }

    function loadData(i) {
        var id = $('#obat' + i).val();
        var jumlah_satuan = $('#jumlah_satuan' + i).val();
        var stok = $('#stok' + i).val();
        var urls = "<?= base_url(); ?>obat/getStokObat";
        var datax = {
            "id": id
        };

        if (parseInt(stok) < parseInt(jumlah_satuan)) {
            alert('Stok obat tidak cukup. Silahkan kurangi jumlah atau ganti obat lain!');
            $('#jumlah_satuan' + i).val('');
        }
    }

    for (var i = 1; i < 11; i++) {
        for (var j = 1; j < 3; j++) {
            function loadDataRacikan(i, j) {
                var ij = (i.toString() + j.toString());


                var id = $('#obat_racikan' + ij).val();
                var jumlah_satuan = $('#jumlah_satuan' + ij).val();
                var urls = "<?= base_url(); ?>obat/getStokObat";
                var datax = {
                    "id": id
                };

                $.ajax({
                    type: 'GET',
                    url: urls,
                    data: datax,

                    success: function(stok) {
                        if (parseInt(stok) < parseInt(jumlah_satuan)) {
                            alert('Stok obat tidak cukup. Silahkan kurangi jumlah atau ganti obat lain!');
                            $('#jumlah_satuan' + ij).val('');
                        }


                    }
                });
            }
        }
    }
</script>

<script type="text/javascript">
    jQuery(function($) {
        var bahan = <?php echo json_encode($bahan); ?>;
        $('#bahan-option').select2();

        var obat = <?php echo json_encode($obat1); ?>;
        $('#obat-option').select2();

        $('#button-add-bahan').on('click', function(e) {
            var id_bahan = $('#bahan-option').val();
            if (id_bahan == '') {
                alert('Anda belum memilih bahan habis pakai !');
            } else {
                var theBahan = {};
                var counter = parseInt($('#abdush-counter2').val());
                $.each(bahan, function(i, v) {
                    if (v.id == id_bahan) {
                        theBahan = v;
                        return;
                    }
                });

                var html = `
                <tr>
                  <td>
                    ` + theBahan.nama + `
                    <input type="hidden" name="id[]" value="` + theBahan.id + `">
                  </td>
                  <td>` + theBahan.jumlah + ` ` + theBahan.satuan + `</td>
                  <td>
                    <input style="width:65px;" type="text" class="form-control" name="qty[]" id="bahan[` + counter + `][qty]">
                  </td>
                  <td>
                    <button class="btn btn-sm btn-danger btn-delete-row" type="button"> <i class="fa fa-trash"></i> </button>
                  </td>
                </tr>`;

                $('.form-area tbody').append(html);
                $('#abdush-counter').val(counter + 1);
                $('input[name="bahan[' + counter + '][qty]"]').focus();

                $('.btn-delete-row').unbind('click');
                $('.btn-delete-row').each(function() {
                    $(this).on('click', function() {
                        $(this).parents('tr').remove();
                    });
                });
            }

            $('#bahan-option').val('').trigger('change');
        });

        $('#button-add-obat').on('click', function(e) {
            var id_Obat = $('#obat-option').val();
            if (id_Obat == '') {
                alert('Anda belum memilih obat !');
            } else {
                var theObat = {};
                var counter = parseInt($('#abdush-counter2').val());
                $.each(obat, function(i, v) {
                    if (v.id == id_Obat) {
                        theObat = v;
                        return;
                    }
                });

                var html = `
                <tr>
                  <td>
                    ` + theObat.nama + `
                    <input type="hidden" name="nama_obat[]" value="` + theObat.id + `">
                  </td>
                  <td>` + theObat.stok_obat + ` item </td>
                  <input type="hidden" id="stok` + counter + `" value="` + theObat.stok_obat + `">
                  <td>
                    <input style="width:65px;" type="text" class="form-control" onchange="loadData(` + counter + `);" name="jumlah_satuan[]" id="jumlah_satuan` + counter + `">
                  </td>
                  <td>
                    <input style="width:100px;" type="text" class="form-control"  name="signa_obat[]" id="signa_obat` + counter + `">
                  </td>
                  <td>
                    <button class="btn btn-sm btn-danger btn-delete-row" type="button"> <i class="fa fa-trash"></i> </button>
                  </td>
                </tr>

                `;

                $('.form-area-obat tbody').append(html);
                $('#abdush-counter2').val(counter + 1);
                $('input[id="jumlah_satuan' + counter + '"]').focus();

                $('.btn-delete-row').unbind('click');
                $('.btn-delete-row').each(function() {
                    $(this).on('click', function() {
                        $(this).parents('tr').remove();
                    });
                });
            }

            $('#obat-option').val('').trigger('change');
        });

        let initializer = {
            init: function() {
                for (let i = 1; i < 9; i++) {
                    $(`#obat-racik-option-${i}`).select2();
                    $(`#button-add-obat-racik-${i}`).on('click', e => {
                        this.onBtnAddClick(i);
                    });
                }
            },
            checkHasObat: function(i) {
                let rowCount = $(`.form-area-obat-racik-${i} >table >tbody >tr`).length;
                if (rowCount === 0) {
                    $(`#signa-obat-racik-${i}`).prop('required', false);
                }
            },
            onBtnAddClick: function(i) {
                let that = this;
                let opt = $(`#obat-racik-option-${i}`);
                let id_Obat = opt.val();
                if (id_Obat === '') {
                    alert('Anda belum memilih obat !');
                } else {
                    let theObat = {};
                    let counter = parseInt($(`#abdush-counter-${i}`).val());
                    $.each(obat, function(i, v) {
                        if (v.id === id_Obat) {
                            theObat = v;
                            return;
                        }
                    });

                    $(`.form-area-obat-racik-${i} tbody`).append(this.getTableRow(i, theObat, counter));
                    $(`#signa-obat-racik-${i}`).prop('required', true);
                    $(`#abdush-counter-${i}`).val(counter + 1);
                    $('input[id="jumlah_satuan_racik' + counter + '"]').focus();

                    $('.btn-delete-row').unbind('click');
                    $('.btn-delete-row').each(function() {
                        $(this).on('click', function() {
                            $(this).parents('tr').remove();
                            that.checkHasObat(i);
                        });
                    });
                }
                opt.val('').trigger('change');
            },
            getTableRow: (i, obat, counter) => (`
                <tr>
                  <td>
                    ` + obat.nama + `
                    <input type="hidden" name="nama_obat_racikan${i}[]" value="` + obat.id + `">
                  </td>
                  <td>` + obat.stok_obat + ` item </td>
                  <input type="hidden" id="stok` + counter + `" value="` + obat.stok_obat + `">
                  <td>
                    <input style="width:65px;" type="text" class="form-control" onchange="loadData(` + counter + `);" name="jumlah_satuan_racikan${i}[]" id="jumlah_satuan_racikan${i}` + counter + `">
                  </td>
                  <td>
                    <button class="btn btn-sm btn-danger btn-delete-row" type="button"> <i class="fa fa-trash"></i> </button>
                  </td>
                </tr>
            `)
        };

        initializer.init();
    });
</script>

<script type="text/javascript">
    let bahan_all = <?php echo json_encode($bahan); ?>;
    let bahan = <?php echo json_encode($bahans); ?>;

    let renderBahan = (theBahan, counter, jumlah = '') => `
        <tr>
            <td>
            ${theBahan.nama}
                <input type="hidden" name="id[]" value="${theBahan.id}">
            </td>
            <td>${theBahan.jumlah} ${theBahan.satuan}</td>
            <input type="hidden" id="stok_bahan${counter}" value="${theBahan.jumlah}">
            <td>
                <input style="width:65px;"
                    type="text"
                    class="form-control"
                    onchange="cekBahan(${counter});"
                    name="qty[]"
                    id="bahan[${counter}][qty]"
                    value=${jumlah}>
            </td>
            <td>
                <button class="btn btn-sm btn-danger btn-delete-row" type="button"> <i class="fa fa-trash"></i> </button>
            </td>
        </tr>
    `;

    let addBahanView = (id_bahan, jumlah = '') => {
        let counter = parseInt($('#abdush-counter-bahan').val());
        let theBahan = bahan_all.find(v => v.id === id_bahan);

        $('.form-area tbody').append(renderBahan(theBahan, counter, jumlah));
        $('#abdush-counter').val(counter + 1);
        $('input[name="bahan[' + counter + '][qty]"]').focus();

        $('.btn-delete-row').unbind('click');
        $('.btn-delete-row').each(function() {
            $(this).on('click', function() {
                $(this).parents('tr').remove();
            });
        });
    };

    bahan.forEach(v => {
        addBahanView(v.bahan_id, v.jumlah);
    });

    function cekBahan(i) {
        var jumlah_satuan = $(`#bahan\\[${i}\\]\\[qty\\]`).val();
        var stok = $('#stok_bahan' + i).val();
        console.log(jumlah_satuan);
        console.log(stok);

        if (parseInt(stok) < parseInt(jumlah_satuan)) {
            alert('Stok bahan tidak cukup. Silahkan kurangi jumlah atau ganti bahan lain!');
            $(`#bahan\\[${i}\\]\\[qty\\]`).val('');
        }
    }
</script>

<script>
    function rujuk_poli() {
        if (!$('#poli-rujuk').val()) {
            alert('Pilih poli rujukan dulu')
            return false
        }
        return true
    }
</script>

<?php $this->load->view('pemeriksaan/partials/tab_upload_js'); ?>
<?php $this->load->view('pemeriksaan/partials/surat_js'); ?>