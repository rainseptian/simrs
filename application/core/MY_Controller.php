<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use LZCompressor\LZString;

class MY_Controller extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('setting_model');

        if ($this->uri->segment(1) == 'BpjsDev') {

        }
        else if ($this->uri->segment(1) == 'DaftarOnline') {

        }
        else if ($this->uri->segment(1) == 'pendaftaran' && $this->uri->segment(2) == 'ajax_kode') {

        }
        else {
            if(empty($this->session->userdata('logged_in')))
            {
                $this->session->set_flashdata('warning', 'Silahkan Login Dahulu');
                redirect('Login/index','refresh');
            }

            $StatusLogin = $this->cek_session();

            //print_r($StatusLogin);die();
            if(!$StatusLogin){
                $this->session->set_flashdata('error', 'Waktu Login Sudah Habis, Silahkan Login Dahulu');
                redirect('Login/index','refresh');
            }

            $this->load->helper('lang');
            $this->load->helper('language');
            $this->load->helper('directory');

            if ($this->session->has_userdata('hospitaladmin')) {
                $admin = $this->session->userdata('hospitaladmin');
                $language = ($admin['language']['language']);
            } else if ($this->session->has_userdata('patient')) {
                $student = $this->session->userdata('patient');
                $language = ($student['language']['language']);
            } else {
                $sss = $this->setting_model->get();
                $language = $sss[0]['language'];
            }

            $this->config->set_item('language', strtotime($language));
            $map = directory_map(APPPATH . "./language/" . 'english' . "/app_files");

            foreach ($map as $lang_key => $lang_value) {
                $lang_array[] = 'app_files/' . str_replace(".php", "", $lang_value);
            }

            $this->load->language($lang_array, strtolower($language));
        }
    }

    public function cek_session()
    {
        $session_cek = $this->session->userdata('logged_in');

        return $session_cek;
    }

    public function query_error($pesan = "Terjadi kesalahan, coba lagi !")
    {
        $json['status'] = 0;
        $json['pesan'] 	= $pesan;
        echo json_encode($json);
    }
}

class MY_BpjsController extends MY_Controller
{
    // Katalog bisa diakses di link berikut : https://dvlp.bpjs-kesehatan.go.id:8888/trust-mark/
    //dan dapat diakses dengan
    //username : putu.agus
    //password : Bpj$2020.

    // DEV
//    public static $base_url_vclaim_ = "https://apijkn-dev.bpjs-kesehatan.go.id/vclaim-rest-dev/";
//    public static $base_url_antrean_ = "https://apijkn-dev.bpjs-kesehatan.go.id/antreanrs_dev/";
//    public $base_url_vclaim = "https://apijkn-dev.bpjs-kesehatan.go.id/vclaim-rest-dev/";
//    public $base_url_antrean = "https://apijkn-dev.bpjs-kesehatan.go.id/antreanrs_dev/";
//
//    protected $salt = 'smart-clinic-and-hospital';
//    protected $cons_id = 28730;
//    protected $secret_key = '0lS92D5882';
//    protected $user_key = 'fe5fbae1ba7948d99170921bf17f6907';
//    protected $user_key_vclaim = 'd43504683bb350c66b90a4974d5ee8d0';
//    protected $timestamp = "";

    // PROD
    public static $base_url_vclaim_ = "https://apijkn.bpjs-kesehatan.go.id/vclaim-rest/";
    public static $base_url_antrean_ = "https://apijkn.bpjs-kesehatan.go.id/antreanrs/";
    public $base_url_vclaim = "https://apijkn.bpjs-kesehatan.go.id/vclaim-rest/";
    public $base_url_antrean = "https://apijkn.bpjs-kesehatan.go.id/antreanrs/";
    public $base_url_aplicare = "https://dvlp.bpjs-kesehatan.go.id:8888/aplicaresws/";

    protected $salt = 'smart-clinic-and-hospital';
    protected $cons_id = 30389;
    protected $secret_key = '2cJ24C23FC';
    protected $user_key = '83b9cf13a3ec7ea406a0f33c7c9892b9'; // vclaim
    protected $user_key_antrian = 'b940c5a76229a6baa02edbc9fc2edf1f'; // antrian
    protected $timestamp = "";

    public function __construct()
    {
        parent::__construct();

        $this->load->library('template');
        $this->load->Model('MainModel');
    }

    protected function signature($is_antrian = false)
    {
        $cons_id = $this->cons_id;
        $secret_key = $this->secret_key;
        $user_key = $is_antrian ? $this->user_key_antrian : $this->user_key;

        date_default_timezone_set('UTC');
        $this->timestamp = strval(time()-strtotime('1970-01-01 00:00:00'));
        $tStamp = $this->timestamp;
        $signature = hash_hmac('sha256', $cons_id."&".$tStamp, $secret_key, true);
        $encodedSignature = base64_encode($signature);
        date_default_timezone_set('Asia/Jakarta');

        return [
            "X-cons-id: {$cons_id}",
            "X-timestamp: {$tStamp}",
            "X-signature: {$encodedSignature}",
            "user_key: {$user_key}",
//            'Accept: application/json',
//            'Content-Type: application/json',
        ];
    }

    function stringDecrypt($string)
    {
        $tStamp = $this->timestamp;
        $key = "{$this->cons_id}{$this->secret_key}{$tStamp}";

        $encrypt_method = 'AES-256-CBC';
        $key_hash = hex2bin(hash('sha256', $key));
        $iv = substr(hex2bin(hash('sha256', $key)), 0, 16);

        return openssl_decrypt(base64_decode($string), $encrypt_method, $key_hash, OPENSSL_RAW_DATA, $iv);
    }

    function decompress($string)
    {
        return LZString::decompressFromEncodedURIComponent($string);
    }

    protected function get($url)
    {
        $header = $this->signature();

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        $server_output = curl_exec ($ch);
        curl_close ($ch);

        $server_output = json_decode($server_output);
        if ($server_output->metaData->code == 200) {
            $response = $server_output->response;
            $decrypted = $this->stringDecrypt($response);
            $decompressed = $this->decompress($decrypted);
            return (object) [
                'success' => true,
                'data' => json_decode($decompressed)
            ];
        }
        else {
            return (object) [
                'success' => false,
                'server_output' => $server_output
            ];
        }
    }

    protected function post($url, $body, $is_antrian = false)
    {
        $header = $this->signature($is_antrian);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $body);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $server_output = curl_exec ($ch);
        $error_exec = curl_errno($ch) ? curl_error($ch) : null;
        curl_close($ch);

        if ($error_exec) {
            return (object) [
                'success' => false,
                'server_output' => $error_exec
            ];
        }

        $server_output = json_decode($server_output);

        if ($server_output->metaData->code == 200 || $server_output->metadata->code == 200 || $server_output->metadata->code == 1) {
            $response = $server_output->response;
            $decrypted = $this->stringDecrypt($response);
            $decompressed = $this->decompress($decrypted);
            if (!$decompressed) {
                return (object) [
                    'success' => true,
                    'data' => $server_output
                ];
            }
            return (object) [
                'success' => true,
                'data' => json_decode($decompressed)
            ];
        }
        else {
            return (object) [
                'success' => false,
                'server_output' => $server_output
            ];
        }
    }

    protected function put($url, $body)
    {
        $header = $this->signature();

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $body);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $server_output = curl_exec ($ch);
        $error_exec = curl_errno($ch) ? curl_error($ch) : null;
        curl_close($ch);

        if ($error_exec) {
            return (object) [
                'success' => false,
                'server_output' => $error_exec
            ];
        }

        $server_output = json_decode($server_output);

        if ($server_output->metaData->code == 200) {
            $response = $server_output->response;
            $decrypted = $this->stringDecrypt($response);
            $decompressed = $this->decompress($decrypted);
            return (object) [
                'success' => true,
                'data' => json_decode($decompressed)
            ];
        }
        else {
            return (object) [
                'success' => false,
                'server_output' => $server_output
            ];
        }
    }

    protected function delete($url, $body)
    {
        $header = $this->signature();

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $body);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $server_output = curl_exec ($ch);
        $error_exec = curl_errno($ch) ? curl_error($ch) : null;
        curl_close($ch);

        if ($error_exec) {
            return (object) [
                'success' => false,
                'server_output' => $error_exec
            ];
        }

        $server_output = json_decode($server_output);

        if ($server_output->metaData->code == 200) {
            $response = $server_output->response;
            $decrypted = $this->stringDecrypt($response);
            $decompressed = $this->decompress($decrypted);
            return (object) [
                'success' => true,
                'data' => json_decode($decompressed)
            ];
        }
        else {
            return (object) [
                'success' => false,
                'server_output' => $server_output
            ];
        }
    }

    public function raw()
    {
        if ($this->input->post('method') == 'GET') {
            $res = $this->get($this->input->post('url'));
        }
        else {
            $res = $this->post($this->input->post('url'), json_encode($this->input->post('body')));
        }

        return json_encode($res);
    }

    protected function get_dpjp($kode = '')
    {
        $d = [
            [
                'kode' => '6517',
                'nama' => 'dr. Zakky Sukmajaya, Sp.OG'
            ],
            [
                'kode' => '265867',
                'nama' => 'DR. AIRLANGGA WITRA NANDA ABDILLAH'
            ],
            [
                'kode' => '6519',
                'nama' => 'dr. Fera Diastyarini, Sp. A'
            ],
            [
                'kode' => '250731',
                'nama' => 'DR.R.FX.BERJANTO TERAKUSUMA'
            ],
        ];

        if (!$kode) {
            return $d;
        }
        else {
            foreach ($d as $v) {
                if ($v['kode'] == $kode) {
                    return [$v];
                }
            }
            return [];
        }
    }

    protected function ajax_search_dpjp_param($p1, $p2, $p3 = '')
    {
        //Parameter 1 : Jenis Pelayanan (1. Rawat Inap, 2. Rawat Jalan)
        //Parameter 2 : Tgl.Pelayanan/SEP (yyyy-mm-dd)
        //Parameter 3 : Kode Spesialis/Subspesialis

//        $keyword = 'OBG';
//        $keyword = 'BEDAH';
//        $keyword = 'INT'; // penyakit dalam
//        $keyword = 'SAR';
//        $keyword = 'ANA';

        $res = $this->get("{$this->base_url_vclaim}referensi/dokter/pelayanan/$p1/tglPelayanan/$p2/Spesialis/$p3");
        if ($res->success) {
            $json['status'] = 1;
            $json['datanya'] = $res->data->list;
        }
        else {
            $json['status'] = 0;
            $json['server_output'] = $res->server_output;
        }

        return json_encode($json);
    }

    protected function ajax_nomor_kartu()
    {
        $keyword = $this->input->post('keyword');
        $pasien = $this->db->query("
            SELECT p.no_rm, p.no_bpjs as no_jaminan, p.id, p.nama, p.telepon
            FROM pasien p
            WHERE (p.no_bpjs LIKE '%$keyword%' OR p.nama LIKE '%$keyword%')
            AND p.is_active = 1
            AND p.no_bpjs != ''
            AND p.no_bpjs IS NOT NULL 
            LIMIT 10
        ")->result();

        if (count($pasien) > 0) {
            $json['status'] = 1;
            $json['datanya'] = "<ul class='daftar-autocomplete'>";
            foreach ($pasien as $b) {
                $json['datanya'] .= " 
						<li>
						    <span id='pasien_id' style='display: none;'>" . $b->id . "</span>
						    <span id='telepon' style='display: none;'>" . $b->telepon . "</span>
							<b>NO RM</b> :
							<span id='no_rm'>" . $b->no_rm . "</span> <br />
							<b>Nama</b> :
							<span id='nama'>" . $b->nama . "</span> <br />
							<b>No Kartu</b> :
							<span id='no_jaminan'>" . $b->no_jaminan . "</span> <br />
						</li>
					";
            }
            $json['datanya'] .= "</ul>";
        }
        else {
            $json['status'] = 0;
            $json['count'] = count($pasien);
        }

        return json_encode($json);
    }

    protected function ajax_search_faskes()
    {
        $keyword = urlencode($this->input->post('keyword'));
        $res = $this->get("{$this->base_url_vclaim}referensi/faskes/$keyword/1");
        $res_ = $this->get("{$this->base_url_vclaim}referensi/faskes/$keyword/2");
        if ($res->success || $res_->success) {
            $faskes = array_merge(
                $res->success ? array_slice($res->data->faskes, 0, 5) : [],
                $res_->success ? array_slice($res_->data->faskes, 0, 5) : []
            );
            $json['status'] = 1;
            $json['datanya'] = "<ul class='daftar-autocomplete'>";
            foreach ($faskes as $b) {
                $json['datanya'] .= " 
						<li> 
							<b>Kode</b> :
							<span id='kode'>" . $b->kode . "</span> <br />
							<b>Nama</b> :
							<span id='nama'>" . $b->nama . "</span> <br />
						</li>
					";
            }
            $json['datanya'] .= "</ul>";
        }
        else {
            $json['status'] = 0;
            $json['server_output'] = $res->server_output;
        }

        return json_encode($json);
    }

    protected function ajax_search_obat_list()
    {
        $keyword = urlencode($this->input->post('keyword'));
        $res = $this->get("{$this->base_url_vclaim}referensi/obatprb/$keyword");
        if ($res->success) {
            $json['status'] = 1;
            $json['datanya'] = "<ul class='daftar-autocomplete'>";
            foreach ($res->data->list as $b) {
                $json['datanya'] .= " 
						<li> 
							<b>Kode</b> :
							<span id='kode'>" . $b->kode . "</span> <br />
							<b>Nama</b> :
							<span id='nama'>" . $b->nama . "</span> <br />
						</li>
					";
            }
            $json['datanya'] .= "</ul>";
        }
        else {
            $json['status'] = 0;
            $json['server_output'] = $res->server_output;
        }

        return json_encode($json);
    }

    protected function ajax_search($url, $key = null)
    {
        $res = $this->get($url);
        if ($res->success) {
            $json['status'] = 1;
            $json['datanya'] = $key ? $res->data->{$key} : $res->data;
        }
        else {
            $json['status'] = 0;
            $json['server_output'] = $res->server_output;
        }

        return json_encode($json);
    }

    protected function ajax_search_diagnosa()
    {
        $keyword = urlencode($this->input->post('keyword'));
        $res = $this->get("{$this->base_url_vclaim}referensi/diagnosa/$keyword");
        if ($res->success) {
            $json['status'] = 1;
            $json['datanya'] = $res->data->diagnosa;
        }
        else {
            $json['status'] = 0;
            $json['server_output'] = $res->server_output;
        }

        return json_encode($json);
    }

    protected function ajax_search_obat()
    {
        $keyword = urlencode($this->input->post('keyword'));
        $res = $this->get("{$this->base_url_vclaim}referensi/obatprb/$keyword");
        if ($res->success) {
            $json['status'] = 1;
            $json['datanya'] = $res->data->list;
        }
        else {
            $json['status'] = 0;
            $json['server_output'] = $res->server_output;
        }

        return json_encode($json);
    }

    protected function ajax_search_poli($k = null)
    {
        $keyword = $k ?? urlencode($this->input->post('keyword'));
        $res = $this->get("{$this->base_url_vclaim}referensi/poli/$keyword");
        if ($res->success) {
            $json['status'] = 1;
            $json['datanya'] = $res->data->poli;
        }
        else {
            $json['status'] = 0;
            $json['server_output'] = $res->server_output;
        }

        return json_encode($json);
    }

    protected function ajax_search_provinsi()
    {
        $keyword = urlencode($this->input->post('keyword'));
        $res = $this->get("{$this->base_url_vclaim}referensi/propinsi");
        if ($res->success) {
            $json['status'] = 1;
            $json['datanya'] = $res->data->list;
        }
        else {
            $json['status'] = 0;
            $json['server_output'] = $res->server_output;
        }

        return json_encode($json);
    }

    protected function ajax_search_kabupaten()
    {
        $keyword = urlencode($this->input->post('keyword'));
        $res = $this->get("{$this->base_url_vclaim}referensi/kabupaten/propinsi/$keyword");
        if ($res->success) {
            $json['status'] = 1;
            $json['datanya'] = $res->data->list;
        }
        else {
            $json['status'] = 0;
            $json['server_output'] = $res->server_output;
        }

        return json_encode($json);
    }

    protected function ajax_search_kecamatan()
    {
        $keyword = urlencode($this->input->post('keyword'));
        $res = $this->get("{$this->base_url_vclaim}referensi/kecamatan/kabupaten/$keyword");
        if ($res->success) {
            $json['status'] = 1;
            $json['datanya'] = $res->data->list;
        }
        else {
            $json['status'] = 0;
            $json['server_output'] = $res->server_output;
        }

        return json_encode($json);
    }

    protected function ajax_search_dpjp()
    {
        $jns_pelayanan = $this->input->post('jns_pelayanan');
        $tgl_pelayanan = $this->input->post('tgl_pelayanan');
        $keyword = urlencode($this->input->post('keyword'));

        $jns_pelayanan = 2; // rawat jalan
        $tgl_pelayanan = date('Y-m-d');

//        $keyword = 'OBG';
//        $keyword = 'BEDAH';
//        $keyword = 'INT'; // penyakit dalam
//        $keyword = 'SAR';
//        $keyword = 'ANA';

        $res = $this->get("{$this->base_url_vclaim}referensi/dokter/pelayanan/$jns_pelayanan/tglPelayanan/$tgl_pelayanan/Spesialis/$keyword");
        if ($res->success) {
            $json['status'] = 1;
            $json['datanya'] = $res->data->list;
        }
        else {
            $json['status'] = 0;
            $json['url'] = "{$this->base_url_vclaim}referensi/dokter/pelayanan/$jns_pelayanan/tglPelayanan/$tgl_pelayanan/Spesialis/$keyword";
            $json['param'] = compact('jns_pelayanan', 'tgl_pelayanan', 'keyword');
            $json['server_output'] = $res->server_output;
        }

        return json_encode($json);
    }

    protected function ajax_search_ruang_rawat()
    {
        $res = $this->get("{$this->base_url_vclaim}referensi/ruangrawat");
        if ($res->success) {
            $json['status'] = 1;
            $json['datanya'] = $res->data->list;
        }
        else if ($res->server_output->metaData->message == 'Data Tidak Ada') {
            $json['status'] = 1;
            $json['datanya'] = [];
        }
        else {
            $json['status'] = 0;
            $json['server_output'] = $res->server_output;
        }

        return json_encode($json);
    }

    protected function ajax_search_kelas_rawat()
    {
        $res = $this->get("{$this->base_url_vclaim}referensi/kelasrawat");
        if ($res->success) {
            $json['status'] = 1;
            $json['datanya'] = $res->data->list;
        }
        else if ($res->server_output->metaData->message == 'Data Tidak Ada') {
            $json['status'] = 1;
            $json['datanya'] = [];
        }
        else {
            $json['status'] = 0;
            $json['server_output'] = $res->server_output;
        }

        return json_encode($json);
    }

    protected function ajax_search_spesialistik()
    {
        $res = $this->get("{$this->base_url_vclaim}referensi/spesialistik");
        if ($res->success) {
            $json['status'] = 1;
            $json['datanya'] = $res->data->list;
        }
        else if ($res->server_output->metaData->message == 'Data Tidak Ada') {
            $json['status'] = 1;
            $json['datanya'] = [];
        }
        else {
            $json['status'] = 0;
            $json['server_output'] = $res->server_output;
        }

        return json_encode($json);
    }

    protected function ajax_search_cara_keluar()
    {
        $res = $this->get("{$this->base_url_vclaim}referensi/carakeluar");
        if ($res->success) {
            $json['status'] = 1;
            $json['datanya'] = $res->data->list;
        }
        else if ($res->server_output->metaData->message == 'Data Tidak Ada') {
            $json['status'] = 1;
            $json['datanya'] = [];
        }
        else {
            $json['status'] = 0;
            $json['server_output'] = $res->server_output;
        }

        return json_encode($json);
    }

    protected function ajax_search_kondisi_pulang()
    {
        $res = $this->get("{$this->base_url_vclaim}referensi/pascapulang");
        if ($res->success) {
            $json['status'] = 1;
            $json['datanya'] = $res->data->list;
        }
        else if ($res->server_output->metaData->message == 'Data Tidak Ada') {
            $json['status'] = 1;
            $json['datanya'] = [];
        }
        else {
            $json['status'] = 0;
            $json['server_output'] = $res->server_output;
        }

        return json_encode($json);
    }

    protected function ajax_search_diagnosa_prb()
    {
        $res = $this->get("{$this->base_url_vclaim}referensi/diagnosaprb");
        if ($res->success) {
            $json['status'] = 1;
            $json['datanya'] = $res->data->list;
        }
        else if ($res->server_output->metaData->message == 'Data Tidak Ada') {
            $json['status'] = 1;
            $json['datanya'] = [];
        }
        else {
            $json['status'] = 0;
            $json['server_output'] = $res->server_output;
        }

        return json_encode($json);
    }

    protected function ajax_search_procedure()
    {
        $keyword = urlencode($this->input->post('keyword'));
        $res = $this->get("{$this->base_url_vclaim}referensi/procedure/$keyword");
        if ($res->success) {
            $json['status'] = 1;
            $json['datanya'] = $res->data->procedure;
        }
        else if ($res->server_output->metaData->message == 'Data Tidak Ada') {
            $json['status'] = 1;
            $json['datanya'] = [];
        }
        else {
            $json['status'] = 0;
            $json['server_output'] = $res->server_output;
        }

        return json_encode($json);
    }

    protected function ajax_search_peserta_by_nik()
    {
        $keyword = urlencode($this->input->post('keyword'));
        $tgl = urlencode($this->input->post('tgl'));
        $res = $this->get("{$this->base_url_vclaim}Peserta/nik/{$keyword}/tglSEP/{$tgl}");
        if ($res->success) {
            $json['status'] = 1;
            $json['datanya'] = $res->data;
        }
        else if ($res->server_output->metaData->message == 'Data Tidak Ada') {
            $json['status'] = 1;
            $json['datanya'] = [];
        }
        else {
            $json['status'] = 0;
            $json['server_output'] = $res->server_output;
        }

        return json_encode($json);
    }

    protected function ajax_search_peserta_by_no_bpjs()
    {
        $keyword = urlencode($this->input->post('keyword'));
        $tgl = urlencode($this->input->post('tgl'));
        $res = $this->get("{$this->base_url_vclaim}Peserta/nokartu/{$keyword}/tglSEP/{$tgl}");

        if ($res->success) {
            $json['status'] = 1;
            $json['datanya'] = $res->data;
        }
        else if ($res->server_output->metaData->message == 'Data Tidak Ada') {
            $json['status'] = 1;
            $json['datanya'] = [];
        }
        else {
            $json['status'] = 0;
            $json['server_output'] = $res->server_output;
        }

        return json_encode($json);
    }

    protected function search_peserta_by_no_bpjs($no, $tgl)
    {
        $res = $this->get("{$this->base_url_vclaim}Peserta/nokartu/{$no}/tglSEP/{$tgl}");

        if ($res->success) {
            $json['status'] = 1;
            $json['datanya'] = $res->data;
        }
        else if ($res->server_output->metaData->message == 'Data Tidak Ada') {
            $json['status'] = 1;
            $json['datanya'] = [];
        }
        else {
            $json['status'] = 0;
            $json['server_output'] = $res->server_output;
        }

        return $json;
    }

    protected function search_sep($sep)
    {
        $res = $this->get("{$this->base_url_vclaim}SEP/$sep");

        if ($res->success) {
            $json['status'] = 1;
            $json['datanya'] = $res->data;
        }
        else if ($res->server_output->metaData->message == 'Data Tidak Ada') {
            $json['status'] = 1;
            $json['datanya'] = [];
        }
        else {
            $json['status'] = 0;
            $json['server_output'] = $res->server_output;
        }

        return $json;
    }

    protected function search_sep_internal($sep)
    {
        $res = $this->get("{$this->base_url_vclaim}SEP/Internal/$sep");

        if ($res->success) {
            $json['status'] = 1;
            $json['datanya'] = $res->data;
        }
        else if ($res->server_output->metaData->message == 'Data Tidak Ada') {
            $json['status'] = 1;
            $json['datanya'] = [];
        }
        else {
            $json['status'] = 0;
            $json['server_output'] = $res->server_output;
        }

        return $json;
    }

    protected function destroy_sep($sep)
    {
        $req = [
            'request' => [
                't_sep' => [
                    'noSep' => $sep,
                    'user' => $this->session->userdata('logged_in')->nama
                ]
            ]
        ];

        $res = $this->delete("{$this->base_url_vclaim}SEP/2.0/delete", json_encode($req));

        if ($res->success) {
            $json['status'] = 1;
            $json['datanya'] = $res->data;
        }
        else if ($res->server_output->metaData->message == 'Data Tidak Ada') {
            $json['status'] = 1;
            $json['datanya'] = [];
        }
        else {
            $json['status'] = 0;
            $json['server_output'] = $res->server_output;
        }

        return $json;
    }

    protected function destroy_kontrol($noSuratKontrol)
    {
        $req = [
            'request' => [
                't_suratkontrol' => [
                    'noSuratKontrol' => $noSuratKontrol,
                    'user' => $this->session->userdata('logged_in')->nama
                ]
            ]
        ];

        $res = $this->delete("{$this->base_url_vclaim}RencanaKontrol/Delete", json_encode($req));

        if ($res->success) {
            $json['status'] = 1;
            $json['datanya'] = $res->data;
        }
        else if ($res->server_output->metaData->message == 'Data Tidak Ada') {
            $json['status'] = 1;
            $json['datanya'] = [];
        }
        else {
            $json['status'] = 0;
            $json['server_output'] = $res->server_output;
        }

        return $json;
    }

    protected function ajax_search_rujukan_by_no_bpjs()
    {
        $keyword = urlencode($this->input->post('keyword'));
        $res = $this->get("{$this->base_url_vclaim}Rujukan/Peserta/{$keyword}");

        if ($res->success) {
            $json['status'] = 1;
            $json['datanya'] = $res->data;
        }
        else if ($res->server_output->metaData->message == 'Data Tidak Ada') {
            $json['status'] = 1;
            $json['datanya'] = [];
        }
        else {
            $json['status'] = 0;
            $json['server_output'] = $res->server_output;
        }

        return json_encode($json);
    }

    protected function ajax_search_rujukan($no_rujukan = 0)
    {
        $keyword = urlencode($this->input->post('keyword'));
        $no = $keyword ?: $no_rujukan;
        $res = $this->get("{$this->base_url_vclaim}Rujukan/$no");

        if ($res->success) {
            $json['status'] = 1;
            $json['datanya'] = $res->data;
        }
        else if ($res->server_output->metaData->message == 'Data Tidak Ada') {
            $json['status'] = 1;
            $json['datanya'] = [];
        }
        else {
            $json['status'] = 0;
            $json['server_output'] = $res->server_output;
        }

        return json_encode($json);
    }

    protected function ajax_search_spesialistik_rujukan()
    {
        $ppk = urlencode($this->input->post('ppk'));
        $tgl = urlencode($this->input->post('tgl'));
        $res = $this->get("{$this->base_url_vclaim}Rujukan/ListSpesialistik/PPKRujukan/$ppk/TglRujukan/$tgl");

        if ($res->success) {
            $json['status'] = 1;
            $json['datanya'] = $res->data;
        }
        else if ($res->server_output->metaData->message == 'Data Tidak Ada') {
            $json['status'] = 1;
            $json['datanya'] = [];
        }
        else {
            $json['status'] = 0;
            $json['server_output'] = $res->server_output;
        }

        return json_encode($json);
    }

    protected function ajax_search_spesialistik_rencana_kontrol()
    {
        //Parameter 1: Jenis kontrol --> 1: SPRI, 2: Rencana Kontrol
        //Parameter 2: Nomor --> jika jenis kontrol = 1, maka diisi nomor kartu; jika jenis kontrol = 2, maka diisi nomor SEP
        //Parameter 3: Tanggal rencana kontrol --> format yyyy-MM-dd

        $jenis_kontrol = urlencode($this->input->post('jenis_kontrol'));
        $nomor = urlencode($this->input->post('nomor'));
        $tgl = urlencode($this->input->post('tgl'));
        $res = $this->get("{$this->base_url_vclaim}RencanaKontrol/ListSpesialistik/JnsKontrol/$jenis_kontrol/nomor/$nomor/TglRencanaKontrol/$tgl");

        if ($res->success) {
            $json['status'] = 1;
            $json['datanya'] = $res->data;
        }
        else if ($res->server_output->metaData->message == 'Data Tidak Ada') {
            $json['status'] = 1;
            $json['datanya'] = [];
        }
        else {
            $json['status'] = 0;
            $json['server_output'] = $res->server_output;
        }

        echo json_encode($json);
    }

    protected function ajax_search_jadwal_dokter_rencana_kontrol()
    {
        //Parameter 1: Jenis kontrol --> 1: SPRI, 2: Rencana Kontrol
        //Parameter 2: Kode poli
        //Parameter 3: Tanggal rencana kontrol --> format yyyy-MM-dd

        $jenis_kontrol = urlencode($this->input->post('jenis_kontrol'));
        $kode_poli = urlencode($this->input->post('kode_poli'));
        $tgl = urlencode($this->input->post('tgl'));
        $res = $this->get("{$this->base_url_vclaim}RencanaKontrol/JadwalPraktekDokter/JnsKontrol/$jenis_kontrol/KdPoli/$kode_poli/TglRencanaKontrol/$tgl");

        if ($res->success) {
            $json['status'] = 1;
            $json['datanya'] = $res->data;
        }
        else if ($res->server_output->metaData->message == 'Data Tidak Ada') {
            $json['status'] = 1;
            $json['datanya'] = [];
        }
        else {
            $json['status'] = 0;
            $json['server_output'] = $res->server_output;
        }

        echo json_encode($json);
    }

    protected function ajax_search_jadwal_dokter()
    {
        $p1 = urlencode($this->input->post('jenis_kontrol'));
        $p2 = urlencode($this->input->post('kode_poli'));
        $p3 = urlencode($this->input->post('tgl_rencana'));
        $res = $this->get("{$this->base_url_vclaim}RencanaKontrol/JadwalPraktekDokter/JnsKontrol/$p1/KdPoli/$p2/TglRencanaKontrol/$p3");

        if ($res->success) {
            $json['status'] = 1;
            $json['datanya'] = $res->data;
        }
        else if ($res->server_output->metaData->message == 'Data Tidak Ada') {
            $json['status'] = 1;
            $json['datanya'] = [];
        }
        else {
            $json['status'] = 0;
            $json['server_output'] = $res->server_output;
        }

        return json_encode($json);
    }

    protected function ajax_search_sep()
    {
        $keyword = urlencode($this->input->post('keyword'));
        return $this->ajax_search("{$this->base_url_vclaim}SEP/{$keyword}");
    }

    protected function ajax_search_surat_kontrol()
    {
        $keyword = urlencode($this->input->post('keyword'));
        return $this->ajax_search("{$this->base_url_vclaim}RencanaKontrol/noSuratKontrol/{$keyword}");
    }

    protected function ajax_laporan_kunjungan()
    {
        $tgl = urlencode($this->input->post('tgl'));
        $jns = urlencode($this->input->post('jns'));
        $res = $this->get("{$this->base_url_vclaim}Monitoring/Kunjungan/Tanggal/$tgl/JnsPelayanan/$jns");
        if ($res->success) {
            $json['status'] = 1;
            $json['datanya'] = $res->data->sep;
        }
        else {
            $json['status'] = 0;
            $json['server_output'] = $res->server_output;
        }

        return json_encode($json);
    }

    protected function ajax_laporan_klaim()
    {
        $tgl = urlencode($this->input->post('tgl'));
        $jns = urlencode($this->input->post('jns'));
        $status = urlencode($this->input->post('status'));
        $res = $this->get("{$this->base_url_vclaim}Monitoring/Klaim/Tanggal/$tgl/JnsPelayanan/$jns/Status/$status");
        if ($res->success) {
            $json['status'] = 1;
            $json['datanya'] = $res->data->klaim;
        }
        else {
            $json['status'] = 0;
            $json['server_output'] = $res->server_output;
        }

        return json_encode($json);
    }

    protected function ajax_laporan_history_pelayanan_peserta()
    {
        $no_kartu = urlencode($this->input->post('no_kartu'));
        $from = urlencode($this->input->post('from'));
        $to = urlencode($this->input->post('to'));
        $res = $this->get("{$this->base_url_vclaim}monitoring/HistoriPelayanan/NoKartu/$no_kartu/tglMulai/$from/tglAkhir/$to");
        if ($res->success) {
            $json['status'] = 1;
            $json['datanya'] = $res->data->histori;
        }
        else {
            $json['status'] = 0;
            $json['server_output'] = $res->server_output;
        }

        return json_encode($json);
    }

    protected function ajax_laporan_klaim_jaminan_jasa_raharja()
    {
        $jns = urlencode($this->input->post('jns'));
        $from = urlencode($this->input->post('from'));
        $to = urlencode($this->input->post('to'));
        $res = $this->get("{$this->base_url_vclaim}monitoring/JasaRaharja/JnsPelayanan/$jns/tglMulai/$from/tglAkhir/$to");
        if ($res->success) {
            $json['status'] = 1;
            $json['datanya'] = $res->data->jaminan;
        }
        else {
            $json['status'] = 0;
            $json['server_output'] = $res->server_output;
        }

        return json_encode($json);
    }
}

class MY_RestController extends CI_Controller
{
    protected $base_url_antrean = "https://apijkn.bpjs-kesehatan.go.id/antreanrs/";

    public function __construct()
    {
        parent::__construct();
        $_POST = json_decode($this->input->raw_input_stream, true);
        $this->load->helper('JWT');
        $this->load->Model('MainModel');
        $this->load->Model('PasienModel');
        $this->load->helper(['kode_booking', 'usia']);
        $this->load->library('template');
    }

    protected function send_success_response($data, $status = 200, $message = 'ok')
    {
        $response = $data ? ['response' => $data] : [];
        $response['metadata'] = [
            'message' => $message,
            'code' => $status
        ];

        return $this->output
            ->set_content_type('application/json')
            ->set_status_header($status)
            ->set_output(json_encode($response));
    }

    protected function send_failed_response($code, $msg)
    {
        $response = array(
            'metadata' => array(
                'message' => $msg,
                'code' => $code
            )
        );
        $this->output
            ->set_status_header($code)
            ->set_content_type('application/json');

        return $this->output
            ->set_content_type('application/json')
            ->set_status_header($code)
            ->set_output(json_encode($response));
    }

    protected function check_credential()
    {
        $token = $this->input->request_headers()['x-token'] ?? '';
        if (!$token) {
            return [
                'has_access' => false,
                'message' => 'Token tidak ditemukan',
                'code' => 201
            ];
        }

        $username = $this->input->request_headers()['x-username'] ?? '';
        if (!$username) {
            return [
                'has_access' => false,
                'message' => 'Username/Password tidak sesuai',
                'code' => 201
            ];
        }

        $res = $this->db->where('username', $username)->from('bpjs_user')->get()->result();
        if (!count($res)) {
            return [
                'has_access' => false,
                'message' => 'Username/Password tidak sesuai',
                'code' => 201
            ];
        }

        $CI = &get_instance();
        try {
            $token = JWT::decode($token, $CI->config->item('jwt_key'));
            if (!$token) {
                return [
                    'has_access' => false,
                    'message' => 'Anda tidak memiliki akses',
                    'code' => 201
                ];
            }
            if ($token->user != $username) {
                return [
                    'has_access' => false,
                    'message' => 'Token expired',
                    'code' => 201
                ];
            }
            if (!isset($token->timestamp) || !$token->timestamp) {
                return [
                    'has_access' => false,
                    'message' => 'Token expired',
                    'code' => 201
                ];
            }
            if (time() - $token->timestamp > ($CI->config->item('token_timeout') * 60)) {
                return [
                    'has_access' => false,
                    'message' => 'Token expired',
                    'code' => 201
                ];
            }
        }
        catch (Throwable $e) {
            return [
                'has_access' => false,
                'message' => 'Anda tidak memiliki akses',
                'code' => 201
            ];
        }

        return [
            'has_access' => true,
            'code' => 200
        ];
    }

    protected function get_error_credential()
    {
        try {
            $token = $this->input->request_headers()['x-token'] ?? '';
            $CI = &get_instance();
            JWT::decode($token, $CI->config->item('jwt_key'));
            return '';
        }
        catch (Throwable $e) {
            return $e->getMessage();
        }
    }

    protected function get_dpjp()
    {
        return $this->get("{$this->base_url_antrean}ref/dokter");
    }

    protected $salt = 'smart-clinic-and-hospital';
    protected $cons_id = 30389;
    protected $secret_key = '2cJ24C23FC';
    protected $user_key = 'b940c5a76229a6baa02edbc9fc2edf1f';
    protected $timestamp = "";

    protected function signature()
    {
        $cons_id = $this->cons_id;
        $secret_key = $this->secret_key;
        $user_key = $this->user_key;

        date_default_timezone_set('UTC');
        $this->timestamp = strval(time()-strtotime('1970-01-01 00:00:00'));
        $tStamp = $this->timestamp;
        $signature = hash_hmac('sha256', $cons_id."&".$tStamp, $secret_key, true);
        $encodedSignature = base64_encode($signature);
        date_default_timezone_set('Asia/Jakarta');

        return [
            "x-cons-id: {$cons_id}",
            "x-timestamp: {$tStamp}",
            "x-signature: {$encodedSignature}",
            "user_key: {$user_key}",
        ];
    }

    function stringDecrypt($string)
    {
        $tStamp = $this->timestamp;
        $key = "{$this->cons_id}{$this->secret_key}{$tStamp}";

        $encrypt_method = 'AES-256-CBC';
        $key_hash = hex2bin(hash('sha256', $key));
        $iv = substr(hex2bin(hash('sha256', $key)), 0, 16);

        return openssl_decrypt(base64_decode($string), $encrypt_method, $key_hash, OPENSSL_RAW_DATA, $iv);
    }

    function decompress($string)
    {
        return LZString::decompressFromEncodedURIComponent($string);
    }

    protected function get($url)
    {
        $header = $this->signature();

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        $server_output = curl_exec ($ch);
        curl_close ($ch);

        $server_output = json_decode($server_output);
        if ((isset($server_output->metaData) && $server_output->metaData->code == 200) || $server_output->metadata->code == 200 || $server_output->metadata->code == 1) {
            $response = $server_output->response;
            $decrypted = $this->stringDecrypt($response);
            $decompressed = $this->decompress($decrypted);
            return (object) [
                'success' => true,
                'data' => json_decode($decompressed)
            ];
        }
        else {
            return (object) [
                'success' => false,
                'server_output' => $server_output
            ];
        }
    }

    protected function post($url, $body)
    {
        $header = $this->signature();

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $body);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $server_output = curl_exec ($ch);
        $error_exec = curl_errno($ch) ? curl_error($ch) : null;
        curl_close($ch);

        if ($error_exec) {
            return (object) [
                'success' => false,
                'server_output' => $error_exec
            ];
        }

        $server_output = json_decode($server_output);

        if ($server_output->metaData->code == 200 || $server_output->metadata->code == 200 || $server_output->metadata->code == 1) {
            $response = $server_output->response;
            $decrypted = $this->stringDecrypt($response);
            $decompressed = $this->decompress($decrypted);
            return (object) [
                'success' => true,
                'data' => json_decode($decompressed)
            ];
        }
        else {
            return (object) [
                'success' => false,
                'server_output' => $server_output
            ];
        }
    }

    public function raw()
    {
        // $this->input->post('body') => array
        if ($this->input->post('method') == 'GET') {
            $res = $this->get($this->input->post('url'));
        }
        else {
            $res = $this->post($this->input->post('url'), json_encode($this->input->post('body')));
        }

        return json_encode($res);
    }

}

class Admin_Controller extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->load->library('rbac');
//        $this->auth->is_logged_in();
        $this->check_license();
    }

    public function check_license()
    {
        $license = $this->config->item('SHLK');

        if (!empty($license)) {

            $regex = "/^[A-Z0-9]{6}-[A-Z0-9]{6}-[A-Z0-9]{6}-/";

            if (preg_match($regex, $license)) {
                $valid_string = $this->aes->validchk('encrypt', base_url());

                if (strpos($license, $valid_string) !== false) {

                    true; //valid
                } else {
                    $this->update_ss_routine();
                }
            } else {

                $this->update_ss_routine();

            }

        }

    }
    public function update_ss_routine()
    {

        $license       = $this->config->item('SHLK');
        $fname         = APPPATH . 'config/license.php';
        $update_handle = fopen($fname, "r");
        $content       = fread($update_handle, filesize($fname));
        $file_contents = str_replace('$config[\'SHLK\'] = \'' . $license . '\'', '$config[\'SHLK\'] = \'\'', $content);
        $update_handle = fopen($fname, 'w') or die("can't open file");
        if (fwrite($update_handle, $file_contents)) {

        }
        fclose($update_handle);

        $this->config->set_item('SHLK', '');
    }

}
