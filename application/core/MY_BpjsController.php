<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class MY_BpjsController extends MY_Controller
{
    // Katalog bisa diakses di link berikut : https://dvlp.bpjs-kesehatan.go.id:8888/trust-mark/
    //dan dapat diakses dengan
    //username : putu.agus
    //password : Bpj$2020.

    protected $salt = 'smart-clinic-and-hospital';
    protected $cons_id = 28730;
    protected $secret_key = '0lS92D5882';
    protected $user_key = 'fe5fbae1ba7948d99170921bf17f6907';
    protected $timestamp = "";

    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('UTC');
        $this->timestamp = strval(time()-strtotime('1970-01-01 00:00:00'));

        $this->load->library('template');
        $this->load->Model('MainModel');
    }

    public function signature()
    {
        $cons_id = $this->cons_id;
        $secret_key = $this->secret_key;
        $user_key = $this->user_key;

        $tStamp = $this->timestamp;
        $signature = hash_hmac('sha256', $cons_id."&".$tStamp, $secret_key, true);
        $encodedSignature = base64_encode($signature);

        return [
            "X-cons-id: {$cons_id}",
            "X-timestamp: {$tStamp}",
            "X-signature: {$encodedSignature}",
            "user_key: {$user_key}",
            'Accept: application/json',
            'Content-Type: application/json',
        ];
    }

    function stringDecrypt($string)
    {
        $tStamp = $this->timestamp;
        $key = "{$this->cons_id}{$this->secret_key}{$tStamp}";

        $encrypt_method = 'AES-256-CBC';
        $key_hash = hex2bin(hash('sha256', $key));
        $iv = substr(hex2bin(hash('sha256', $key)), 0, 16);

        return openssl_decrypt(base64_decode($string), $encrypt_method, $key_hash, OPENSSL_RAW_DATA, $iv);
    }

    function decompress($string)
    {
        return LZString::decompressFromEncodedURIComponent($string);
    }

    public function test($what, $no)
    {
        $header = $this->signature();

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,"https://apijkn-dev.bpjs-kesehatan.go.id/vclaim-rest-dev/Peserta/$what/$no/tglSEP/2017-10-12");
//        curl_setopt($ch, CURLOPT_POST, 1);
//        curl_setopt($ch, CURLOPT_POSTFIELDS,$vars);  //Post Fields
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);

        $server_output = curl_exec ($ch);

        curl_close ($ch);

        $server_output = json_decode($server_output);
        if ($server_output->metaData->code == 200) {
            $response = $server_output->response;
            $decrypted = $this->stringDecrypt($response);
            $decompressed = $this->decompress($decrypted);
            die($decompressed);
        }
        else {
            die(json_encode($server_output));
        }
    }

    public function get($url)
    {
        $header = $this->signature();

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        $server_output = curl_exec ($ch);
        curl_close ($ch);

        $server_output = json_decode($server_output);
        if ($server_output->metaData->code == 200) {
            $response = $server_output->response;
            $decrypted = $this->stringDecrypt($response);
            $decompressed = $this->decompress($decrypted);
            return (object) [
                'success' => true,
                'data' => json_decode($decompressed)
            ];
        }
        else {
            return (object) [
                'success' => false,
                'server_output' => $server_output
            ];
        }
    }

    public function ajax_nomor_kartu()
    {
        $keyword = $this->input->post('keyword');
        $pasien = $this->db->query("
            SELECT pp.no_rm, pp.no_jaminan, p.id, p.nama
            FROM pendaftaran_pasien pp
            JOIN pasien p ON pp.pasien = p.id
            WHERE pp.jaminan = 'bpjs' 
            AND (pp.no_jaminan LIKE '%$keyword%' OR p.nama LIKE '%$keyword%')
            AND p.is_active = '1'
            GROUP BY p.id 
            LIMIT 10
        ")->result();

        if (count($pasien) > 0) {
            $json['status'] = 1;
            $json['datanya'] = "<ul id='daftar-autocomplete'>";
            foreach ($pasien as $b) {
                $json['datanya'] .= " 
						<li>
						    <span id='pasien_id' style='display: none;'>" . $b->id . "</span>
							<b>NO RM</b> :
							<span id='no_rm'>" . $b->no_rm . "</span> <br />
							<b>Nama</b> :
							<span id='nama'>" . $b->nama . "</span> <br />
							<b>No Kartu</b> :
							<span id='no_jaminan'>" . $b->no_jaminan . "</span> <br />
						</li>
					";
            }
            $json['datanya'] .= "</ul>";
        }
        else {
            $json['status'] = 0;
            $json['count'] = count($pasien);
        }

        echo json_encode($json);
    }

    public function ajax_search_faskes()
    {
        $keyword = urlencode($this->input->post('keyword'));
        $res = $this->get("https://apijkn-dev.bpjs-kesehatan.go.id/vclaim-rest-dev/referensi/faskes/$keyword/1");
        $res_ = $this->get("https://apijkn-dev.bpjs-kesehatan.go.id/vclaim-rest-dev/referensi/faskes/$keyword/2");
        if ($res->success || $res_->success) {
            $faskes = array_merge(
                $res->success ? array_slice($res->data->faskes, 0, 5) : [],
                $res_->success ? array_slice($res_->data->faskes, 0, 5) : []
            );
            $json['status'] = 1;
            $json['datanya'] = "<ul class='daftar-autocomplete'>";
            foreach ($faskes as $b) {
                $json['datanya'] .= " 
						<li> 
							<b>Kode</b> :
							<span id='kode'>" . $b->kode . "</span> <br />
							<b>Nama</b> :
							<span id='nama'>" . $b->nama . "</span> <br />
						</li>
					";
            }
            $json['datanya'] .= "</ul>";
        }
        else {
            $json['status'] = 0;
            $json['server_output'] = $res->server_output;
        }

        echo json_encode($json);
    }

    public function ajax_search_diagnosa()
    {
        $keyword = urlencode($this->input->post('keyword'));
        $res = $this->get("https://apijkn-dev.bpjs-kesehatan.go.id/vclaim-rest-dev/referensi/diagnosa/$keyword");
        if ($res->success) {
            $json['status'] = 1;
            $json['datanya'] = $res->data->diagnosa;
        }
        else {
            $json['status'] = 0;
            $json['server_output'] = $res->server_output;
        }

        echo json_encode($json);
    }

    public function ajax_search_poli()
    {
        $keyword = urlencode($this->input->post('keyword'));
        $res = $this->get("https://apijkn-dev.bpjs-kesehatan.go.id/vclaim-rest-dev/referensi/poli/$keyword");
        if ($res->success) {
            $json['status'] = 1;
            $json['datanya'] = $res->data->poli;
        }
        else {
            $json['status'] = 0;
            $json['server_output'] = $res->server_output;
        }

        echo json_encode($json);
    }

}